<?php

return [

    // These CSS rules will be applied after the regular template CSS

    /*
        'css' => [
            '.button-content .button { background: red }',
        ],
    */

    'colors' => [

        'highlight' => '#004ca3',
        'button'    => '#004cad',

    ],

    'view' => [
        'senderName'  => 'Domli',
        'reminder'    => null,
        // 'unsubscribe' => null,
        'address'     => 'hello@adjemin.com',

        'logo'        => [
            'path'   => 'http://localhost:8000/img/logo.png',
            // 'path'   => '%PUBLIC%/vendor/beautymail/assets/images/sunny/logo.png',
            'width'  => '50',
            'height' => '50',
        ],

        // 'twitter'  => 'twitter.com',
        // 'facebook' => 'facebook.com',
    ],

];
