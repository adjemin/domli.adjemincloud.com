<?php

namespace App\Utils;

use App\Models\Delivery;
use App\Models\CustomerDevices;

class DeliveryMessagingUtils {
    
    public static function notify($deliveryNotification){
        $agent = Delivery::where(["id" => $deliveryNotification->delivery_id])->first();

        // $defaultLanguage = $agent->default_language;
        // if($defaultLanguage == "FR"){
            $title = $deliveryNotification->title;
            $subtitle = $deliveryNotification->subtitle;
        // }else{
        //     $title = $agentNotification->title_en;
        //     $subtitle = $agentNotification->subtitle_en;
        // }

        $agentDevices = CustomerDevices::where([
            "customer_id" => $deliveryNotification->delivery_id,
            "deleted_at" => null
        ])->orderBy('updated_at', 'DESC')->get();

        $metadata = $deliveryNotification->toArray();


        if($agentDevices != null){

            $messages_sent = [];

            foreach($agentDevices as $agentDevice){
                $device = "".$agentDevice->firebase_id;
                $result = FirebaseMessagingUtils::sendNotification($title, $subtitle, $metadata, $device);
                array_push($messages_sent, $result);
            }


            return $messages_sent;
        }else{
            return "No Devices";
        }

    }
}