<?php

namespace App\Utils;

class FirebaseMessagingUtils{

    // const FCM_TOKEN = "AAAA66b9hlU:APA91bF1y9dQiRnF2yQJ53sPzCK2EWSQzo3UQpQ-YTpchpvxqa3RuftdYP3410P_dPuFErqLqP7yS9unPmWIkgwa9ZQI_dW9LSUCOzbIx8ALadZXgYpt22DifgcHtn46PmviYv78Vq4g";
    // const FCM_TOKEN = "AAAAIMuEEUE:APA91bFE7rzqfAsaD76cOAw3-hFugCEDuTYTE1Uc1S_80oklLiKtZvbfVp95b_C9cmlpcYmeQcrNnG8Wo3BlesiEMQcyTUgmRhZXvo298d__-oMa5ZSu2yNAgCweN3BDhYMOn5GkhA27";
    const FCM_TOKEN = "AAAA9zufmFA:APA91bH_L5RgqCYP-JYEeWktIYopK3ABHs5dOjL3IXCPCXYZ_Jh5DdTqQq6fv0LNqi-TmnCJAS6yvxW-Fq35sGipdk6SFbLFHVXBV8HJIXHlQY_vgZBxGoZ1Iw9MSNp_4c8L4UcZeBcj";

    public static function sendNotification($title, $body, $metadata, $id) {

        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array (
            'to' => $id,
            'notification' => array (
                "title" => $title,
                "body" => $body,
                "sound" => 'default'
            ),
            'priority' => 'high',
            'data' => array(
                'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
                'id' => '1',
                'status' => 'done',
                'meta_data_id' => "".$metadata["id"]
                //'metadata' => json_encode($metadata)
            )
        );
        $fields = json_encode ($fields);

        $headers = array (
            'Authorization: key=' .self::FCM_TOKEN,
            'Content-Type: application/json'
        );

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

        $result = curl_exec ( $ch );
        curl_close ( $ch );

        return $result;
    }

    public static function sendData($metadata, $id) {

        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array (
            'to' => $id,
            'priority' => 'high',
            'data' => array(
                'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
                'id' => '1',
                'status' => 'done',
                'metadata' => json_encode($metadata)
            )
        );
        $fields = json_encode ($fields);

        $headers = array (
            'Authorization: key=' .self::FCM_TOKEN,
            'Content-Type: application/json'
        );

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

        $result = curl_exec ( $ch );
        curl_close ( $ch );

        return $result;
    }
}
