<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models
 * @version July 27, 2020, 3:39 pm UTC
 *
 * @property string $title
 * @property string $description
 * @property string $photo
 * @property string $slug
 */
class Category extends Model
{
    use SoftDeletes;

    public $table = 'food_categories';


    protected $dates = ['deleted_at'];

    protected $appends = ['restaurants'];


    public $fillable = [
        'title',
        'description',
        'photo',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'photo' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getRestaurantsAttribute(){
        return RestaurantCategories::where(['category_id' => $this->id])->get();
    }
}
