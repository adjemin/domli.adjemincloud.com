<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Food
 * @package App\Models
 * @version January 9, 2021, 1:44 pm UTC
 *
 * @property integer $restaurant_id
 * @property integer $menu_id
 * @property integer $food_category_id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property integer $price
 * @property integer $currency_code
 * @property string $currency_name
 */
class Food extends Model
{

    use SoftDeletes;

    public $table = 'food';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'restaurant_id',
        'menu_id',
        'food_category_id',
        'uuid',
        'title',
        'description',
        'image',
        'price',
        'currency_code',
        'currency_name',
        'has_part',
        'quantity'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'restaurant_id' => 'integer',
        'menu_id' => 'integer',
        'food_category_id' => 'integer',
        'uuid' => 'string',
        'title' => 'string',
        'description' => 'string',
        'image' => 'string',
        'price' => 'string',
        'currency_code' => 'integer',
        'currency_name' => 'string',
        'quantity' => 'integer',
        'has_part'  =>  'boolean',
        'created_at'    =>  'string',
        'updated_at'    =>  'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getPrice(){
        $price = $this->price / 100;
        return '$ '.number_format($price, 2, '.', '');
    }

    public function menu(){
        return $this->belongsTo(RestaurantMenus::class, 'menu_id', 'id');
    }

    public function restaurant(){
        return $this->belongsTo(Restaurants::class, 'restaurant_id', 'id');
    }

    // public function food_category(){
    //     return $this->belongsTo(FoodCategories::class, 'food_category_id', 'id');
    // }

    public function food_category(){
        return $this->belongsTo(FoodCategories::class, 'food_category_id', 'id');
    }

    public function getPartsAttribute(){
        return  FoodParts::where(['food_id' => $this->id])->get();
    }
}
