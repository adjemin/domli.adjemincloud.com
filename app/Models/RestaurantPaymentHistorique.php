<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RestaurantPaymentHistorique
 * @package App\Models
 * @version March 29, 2021, 9:38 am UTC
 *
 * @property string $amount
 * @property integer $restaurant_id
 * @property string $reference_paiement
 */
class RestaurantPaymentHistorique extends Model
{
    use SoftDeletes;

    public $table = 'restaurant_payment_historiques';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'amount',
        'restaurant_id',
        'reference_paiement',
        'request_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'amount' => 'string',
        'restaurant_id' => 'integer',
        'reference_paiement' => 'string',
        'request_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function restaurant(){
        return $this->belongsTo(Restaurants::class, 'restaurant_id', 'id');
    }

    public function request_payment(){
        return $this->belongsTo(\App\Models\RequestRestaurantPayment::class, 'request_id', 'id');
    }
}
