<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderHistory
 * @package App\Models
 * @version August 3, 2020, 3:28 pm UTC
 *
 * @property integer $order_id
 * @property string $status
 * @property string $creator
 * @property integer $creator_id
 * @property string $creator_name
 */
class OrderHistory extends Model
{
    use SoftDeletes;

    public $table = 'order_histories';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'order_id',
        'status',
        'creator',
        'creator_id',
        'creator_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'status' => 'string',
        'creator' => 'string',
        'creator_id' => 'integer',
        'creator_name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function creator(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
