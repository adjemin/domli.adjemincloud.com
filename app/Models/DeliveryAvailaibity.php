<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DeliveryAvailaibity
 * @package App\Models
 * @version February 23, 2021, 10:15 am UTC
 *
 * @property integer $delivery_id
 * @property string $start_date
 * @property string $end_date
 * @property string $start_time
 * @property string $end_time
 */
class DeliveryAvailaibity extends Model
{
    use SoftDeletes;

    public $table = 'delivery_availaibities';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'delivery_id',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'is_active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'delivery_id' => 'integer',
        'start_date' => 'date',
        'end_date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
