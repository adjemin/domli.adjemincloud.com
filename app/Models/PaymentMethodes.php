<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PaymentMethodes
 * @package App\Models
 * @version January 8, 2021, 4:26 pm UTC
 *
 * @property string $title
 * @property string $slug
 * @property string $description
 */
class PaymentMethodes extends Model
{
    use SoftDeletes;

    public $table = 'payment_methodes';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
