<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DeliveryTypes
 * @package App\Models
 * @version January 8, 2021, 3:57 pm UTC
 *
 * @property string $title
 * @property string $description
 */
class DeliveryTypes extends Model
{
    use SoftDeletes;

    public $table = 'delivery_types';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
