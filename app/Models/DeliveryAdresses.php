<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DeliveryAdresses
 * @package App\Models
 * @version January 8, 2021, 5:44 pm UTC
 *
 * @property string $adress_name
 * @property string $adress_slug
 * @property integer $lng
 * @property string $town
 */
class DeliveryAdresses extends Model
{
    use SoftDeletes;

    public $table = 'delivery_adresses';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'adress_name',
        'adress_slug',
        'lng',
        'lat',
        'town'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'adress_name' => 'string',
        'adress_slug' => 'string',
        'lng' => 'integer',
        'town' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'lng' => 'lat integer:unsigned:nullable number'
    ];


}
