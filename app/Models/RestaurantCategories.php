<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RestaurantCategories
 * @package App\Models
 * @version January 9, 2021, 8:08 am UTC
 *
 * @property integer $restaurant_id
 * @property integer $food_categorie_id
 * @property string $slug
 */
class RestaurantCategories extends Model
{
    use SoftDeletes;

    public $table = 'restaurant_categories';


    protected $dates = ['deleted_at'];



    public $fillable = [
        // 'title',
        // 'slug',
        'restaurant_id',
        'category_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        // 'title' => 'string',
        // 'slug' => 'string'
        'restaurant_id' => 'integer',
        'category_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
    
    
    public function getFoodAttribute(){
        return Food::where('restaurant_id',  $this->id )->get();
    }

    public function category(){
        return $this->belongsTo(FoodCategories::class, 'category_id', 'id');
    }

    public function restaurant(){
        return $this->belongsTo(Restaurants::class, 'restaurant_id', 'id');
    }

}
