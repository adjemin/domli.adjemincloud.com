<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderDeliveries
 * @package App\Models
 * @version January 8, 2021, 5:22 pm UTC
 *
 * @property integer $order_code
 * @property integer $delivery_id
 */
class OrderDeliveries extends Model
{
    use SoftDeletes;

    public $table = 'order_deliveries';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'order_code',
        'delivery_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_code' => 'integer',
        'delivery_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
