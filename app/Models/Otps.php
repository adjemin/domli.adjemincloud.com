<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Otps
 * @package App\Models
 * @version January 8, 2021, 3:30 pm UTC
 *
 * @property integer $code
 * @property integer $dial_code
 * @property integer $phone_number
 * @property string $deadline
 * @property integer $is_used
 * @property integer $is_testing
 */
class Otps extends Model
{
    use SoftDeletes;

    public $table = 'otps';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'code',
        'dial_code',
        'phone_number',
        'deadline',
        'is_used',
        'is_testing'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'integer',
        'dial_code' => 'integer',
        'phone_number' => 'integer',
        'is_used' => 'integer',
        'is_testing' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
