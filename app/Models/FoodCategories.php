<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FoodCategories
 * @package App\Models
 * @version January 9, 2021, 8:02 am UTC
 *
 * @property string $title
 * @property string $description
 * @property string $photo
 * @property string $slug
 */
class FoodCategories extends Model
{
    use SoftDeletes;

    public $table = 'food_categories';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'description',
        'photo',
        'slug',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'photo' => 'string',
        'slug' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    // public function foods(){
    //     return $this->hasMany(Food::class, 'food_category_id', 'id');
    // }

    public function foods(){
        return $this->hasMany(Food::class, 'food_category_id', 'id');
    }

}
