<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderDelivery
 * @package App\Models
 * @version July 28, 2020, 1:01 pm UTC
 *
 * @property integer $order_id
 * @property integer $delivery_id
 */
class OrderDelivery extends Model
{
    use SoftDeletes;

    public $table = 'order_deliveries';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'order_id',
        'delivery_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'delivery_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function delivery(){
        return $this->belongsTo(DeliveryAddress::class, 'delivery_id');
    }
}
