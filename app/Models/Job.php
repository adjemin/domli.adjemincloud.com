<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Job
 * @package App\Models
 * @version February 25, 2021, 12:36 pm UTC
 *
 * @property string $location_name
 * @property string $location_description
 * @property integer $deliverer_id
 * @property number $location_lat
 * @property number $location_lng
 * @property boolean $is_pickup
 * @property string $status
 * @property string $signature_url
 * @property json $images
 * @property string $notes
 * @property json $note_audio
 * @property string $before_date
 */
class Job extends Model
{
    use SoftDeletes;

    const STARTED = 'started';
    const PENDING = 'pending';
    const FAILED = 'failed';
    const CANCELED = 'canceled';
    const SUCCESSFULLY = 'successfully';
    const ARRIVED = 'arrived';

    const PENDING_CODE = 1;
    const STARTED_CODE = 2;
    const ARRIVED_CODE = 3;
    const FAILED_CODE = 4;
    const CANCELED_CODE = 4;
    const SUCCESSFULLY_CODE = 4;

    public $table = 'jobs';
    

    protected $dates = ['deleted_at'];

    public $fillable = [
        'order_id',
        'job_code',
        'location_name',
        'location_description',
        'deliverer_id',
        'location_lat',
        'location_lng',
        'is_pickup',
        'status',
        'signature_url',
        'images',
        'notes',
        'note_audio',
        'before_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'job_code' => 'integer',
        'location_name' => 'string',
        'location_description' => 'string',
        'deliverer_id' => 'integer',
        'location_lat' => 'string',
        'location_lng' => 'string',
        'is_pickup' => 'boolean',
        'status' => 'string',
        'signature_url' => 'string',
        'notes' => 'string',
        'before_date' => 'string',
        'created_at' => 'string',
        'updated_at' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function delivery(){
        return $this->belongsTo(Delivery::class, 'deliverer_id', 'id');
    }
    
    public function order(){
        return $this->belongsTo(Orders::class, 'order_id', 'id');
    }
}
