<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RequestRestaurantPayment
 * @package App\Models
 * @version March 29, 2021, 9:43 am UTC
 *
 * @property string $amount
 * @property string $date
 * @property string $paiement_method
 * @property string $account_reference
 */
class RequestRestaurantPayment extends Model
{
    use SoftDeletes;

    public $table = 'request_restaurant_payments';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'amount',
        'date',
        'paiement_method',
        'account_reference',
        'restaurant_id',
        'is_paid'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'amount' => 'string',
        'date' => 'string',
        'paiement_method' => 'string',
        'account_reference' => 'string',
        'restaurant_id' =>  'integer',
        'is_paid'   =>  'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function restaurant(){
        return $this->belongsTo(Restaurants::class, 'restaurant_id', 'id');
    }
}
