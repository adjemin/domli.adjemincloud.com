<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AgentConnexion
 * @package App\Models
 * @version February 25, 2021, 12:49 pm UTC
 *
 * @property integer $deliverer_id
 * @property number $location_lat
 * @property number $location_lng
 * @property string $location_name
 * @property boolean $is_active
 */
class AgentConnexion extends Model
{
    use SoftDeletes;

    public $table = 'agent_connexions';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'deliverer_id',
        'location_lat',
        'location_lng',
        'location_name',
        'is_active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'deliverer_id' => 'integer',
        'location_lat' => 'double',
        'location_lng' => 'double',
        'location_name' => 'string',
        'is_active' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
