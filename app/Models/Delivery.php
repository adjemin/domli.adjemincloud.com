<?php

namespace App\Models;

use Eloquent as Model;
// use App\Models\Geographical;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;


/**
 * Class Delivery
 * @package App\Models
 * @version January 8, 2021, 5:30 pm UTC
 *
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $password
 * @property integer $dial_code
 * @property integer $phone_number
 * @property integer $phone
 * @property integer $delivery_type_id
 * @property integer $vehicle_type_id
 * @property integer $lng
 * @property integer $lat
 */
class Delivery extends Authenticatable implements JWTSubject
{
    use SoftDeletes, Geographical, Notifiable;

    public $table = 'deliveries';


    protected $dates = ['deleted_at'];

    // protected $appends = [
    //     'distance'
    // ];

    protected static $kilometers = true;

    const LATITUDE  = 'lat';
    const LONGITUDE = 'lng';

    public $fillable = [
        'first_name',
        'last_name',
        'name',
        // 'surname',
        'email',
        'email_verified',
        'email_verified_at',
        'password',
        'dial_code',
        'phone_number',
        'phone',
        'photo',
        // 'delivery_type_id',
        // 'vehicle_type_id',
        'lng',
        'lat',
        'is_active',
        'is_connected'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'first_name' => 'string',
        'last_name' => 'string',
        'name' => 'string',
        // 'surname' => 'string',
        'email_verified' => 'bool',
        'email_verified_at' => 'string',
        'email' => 'string',
        // 'password' => 'string',
        'dial_code' => 'string',
        'phone_number' => 'string',
        'phone' => 'string',
        'photo' => 'string',
        // 'delivery_type_id' => 'integer',
        // 'vehicle_type_id' => 'integer',
        'lng' => 'string',
        'lat' => 'string',
        'is_active' =>  'bool',
        'is_connected' =>  'bool'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    protected $hidden = [
        'password'
    ];

    public function verifyDeliverer(){
        // return \App\VerifyDeliverer::where(['deliverer_id' => $this->id])->first();
        return $this->hasOne('\\App\\VerifyDeliverer', 'deliverer_id', 'id');
    }

    public function vehicle(){
        return \App\Models\Vehicle::where(['deliverer_id' => $this->id])->first();
        // return $this->hasOne('App\\Models\\Vehicle', 'deliverer_id', 'id');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->id;
        // return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getAuthPassword()
    {
      return $this->password;
    }

}
