<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class JobHistorique
 * @package App\Models
 * @version February 25, 2021, 12:46 pm UTC
 *
 * @property integer $job_id
 * @property string $status
 */
class JobHistorique extends Model
{
    use SoftDeletes;

    public $table = 'job_historiques';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'job_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'job_id' => 'integer',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
