<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderHistories
 * @package App\Models
 * @version January 8, 2021, 6:17 pm UTC
 *
 * @property integer $order_id
 * @property string $status
 * @property integer $customer_id
 */
class OrderHistories extends Model
{
    use SoftDeletes;

    public $table = 'order_histories';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'order_id',
        'status',
        // 'customer_id'
        'creator',
        'creator_id',
        'creator_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'status' => 'string',
        'customer_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
