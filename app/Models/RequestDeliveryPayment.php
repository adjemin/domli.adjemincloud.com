<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RequestDeliveryPayment
 * @package App\Models
 * @version March 29, 2021, 9:45 am UTC
 *
 * @property string $amount
 * @property string $paiement_method
 * @property string $date
 * @property string $account_reference
 */
class RequestDeliveryPayment extends Model
{
    use SoftDeletes;

    public $table = 'request_delivery_payments';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'amount',
        'paiement_method',
        'date',
        'account_reference',
        'delivery_id',
        'is_paid'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'amount' => 'string',
        'paiement_method' => 'string',
        'date' => 'string',
        'account_reference' => 'string',
        'delivery_id'   => 'integer',
        'is_paid'   => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function deliverer(){
        return $this->belongsTo(\App\Models\Delivery::class, 'delivery_id', 'id');
    }
}
