<?php

namespace App\Models;

use Eloquent as Model;
// use App\Models\Geographical;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class Restaurants
 * @package App\Models
 * @version January 8, 2021, 3:53 pm UTC
 *
 * @property string $name
 * @property string $surname
 * @property string $cover
 * @property string $logo
 * @property integer $tel1
 * @property integer $tel2
 * @property integer $tel3
 * @property integer $lng
 * @property integer $lat
 * @property string $email
 */
class Restaurants extends Authenticatable
{
    use SoftDeletes, Notifiable, Geographical;

    public $table = 'restaurants';


    protected $dates = ['deleted_at'];

    // protected $appends = [
    //     'distance'
    // ];

    protected static $kilometers = true;

    const LATITUDE  = 'lat';
    const LONGITUDE = 'lng';


    public $fillable = [
        'uuid',
        'name',
        // 'lastname',
        'cover',
        'logo',
        'lng',
        'lat',
        'email',
        'categorie_id',
        'responsable_name',
        'descritption',
        'responsable_photo',
        'responsable_paper_id_number',
        'restaurant_paper_id_type',
        'restaurant_contact',
        'restaurant_region_name',
        'restaurant_adress_name',
        'restaurant_registration_number',
        'restaurant_city',
        'verified',
        'password',
        'is_active',
        'activation_date',
        'location_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'phone_number' => 'integer',
        'uuid' => 'string',
        'name' => 'string',
        'restaurant_adress_name' => 'string',
        'description' => 'string',
        'responsable_name' => 'string',
        'cover' => 'string',
        'logo' => 'string',
        'lng' => 'string',
        'lat' => 'string',
        'categorie_id' => 'integer',
        'responsable_name' => 'string',
        'responsable_photo' => 'string',
        'responsable_paper_id_number' => 'string',
        'restaurant_paper_id_type' => 'string',
        'restaurant_contact' => 'string',
        'restaurant_adress_name' => 'string',
        'restaurant_registration_number' => 'string',
        'restaurant_city' => 'string',
        'verified' => 'boolean',
        'is_active' => 'boolean',
        'email' => 'string',
        'activation_date' => 'string',
        'created_at' => 'string',
        'updated_at' => 'string',
        'location_name' => 'string'
        // 'restaurant_contact' => 'string',
        // 'password' => 'string',
        // 'verified'  => 'boolean',
        // 'is_active' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:255',
        'restaurant_adress_name' => 'required|max:255',
        'responsable_name' => 'required|max:255',
        'phone_number' => 'required',
        'email' => 'required',
        'password', 'required|confirmed|min:8',
    ];


    public function orders(){
        return $this->hasMany(Orders::class, 'restaurant_id', 'id');
    }

    public function getMenusAttribute(){
        return RestaurantMenus::where(['restaurant_id' => $this->id])->get();
    }

    public function getCategoriesAttribute(){
        return RestaurantCategories::where(['restaurant_id' => $this->id])->get();
    }
    // public function categories(){
    //     return $this->hasMany(RestaurantCatgories::class, 'restaurant_id', 'id');
    // }

    public function verifyRestaurant()
    {
        return $this->hasOne('App\\VerifyRestaurant', 'restaurant_id', 'id');
    }

    // public function category(){
    //     return $this->belongsTo(App\Models\RestaurantCategories::class, 'categorie_id', 'id');
    // }
}
