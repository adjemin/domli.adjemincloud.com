<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vehicle
 * @package App\Models
 * @version February 22, 2021, 3:38 pm UTC
 *
 * @property integer $vehicleType_id
 * @property integer $deliverer_id
 * @property boolean $is_active
 * @property string $numero_police
 * @property string $date_start
 * @property string $date_end
 * @property string $vehicule_name
 * @property string $numero_serie
 * @property string $immatriculation
 * @property boolean $is_verified
 */
class Vehicle extends Model
{
    use SoftDeletes;

    public $table = 'vehicles';

    public $appends = ['vehicule'];   

    protected $dates = ['deleted_at'];



    public $fillable = [
        'vehicleType_id',
        'deliverer_id',
        'is_active',
        'numero_police',
        'date_start',
        'date_end',
        'vehicule_name',
        'numero_serie',
        'immatriculation',
        'is_verified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'vehicleType_id' => 'integer',
        'deliverer_id' => 'integer',
        'is_active' => 'boolean',
        'numero_police' => 'string',
        'date_start' => 'string',
        'date_end' => 'string',
        'vehicule_name' => 'string',
        'numero_serie' => 'string',
        'immatriculation' => 'string',
        'is_verified' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function deliverer(){
        return $this->belongsTo(Delivery::class, 'deliverer_id', 'id');
    }

    public function vehicleType(){
        return $this->belongsTo(DeliveryTypes::class, 'vehicleType_id', 'id');
    }

    public function getVehiculeAttribute(){
        return VehicleTypes::find($this->vehicleType_id);
    }

    public function is_expired(){
        if(isset($this->date_start) || is_null($this->date_end)){
            if(\Carbon\Carbon::parse($this->date_start) <= \Carbon\Carbon::now() && \Carbon\Carbon::now() <= \Carbon\Carbon::parse($this->date_end)){
                return false;
            }
        }
        
        return true;
    }
}
