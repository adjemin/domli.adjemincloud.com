<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class InvoicePayment
 * @package App\Models
 * @version July 27, 2020, 5:03 pm UTC
 *
 * @property integer $invoice_id
 * @property string $payment_method
 * @property string $payment_reference
 * @property string $amount
 * @property string $currency_code
 * @property string $currency_name
 * @property string $coupon
 * @property string $creator_id
 * @property string $creator_name
 * @property string $creator
 * @property string $status
 * @property boolean $is_waiting
 * @property boolean $is_completed
 * @property string $payment_gateway_trans_id
 * @property string $payment_gateway_custom
 * @property string $payment_gateway_currency
 * @property string $payment_gateway_amount
 * @property string $payment_gateway_payid
 * @property string $payment_gateway_payment_date
 * @property string $payment_gateway_payment_time
 * @property string $payment_gateway_error_message
 * @property string $payment_gateway_payment_method
 * @property string $payment_gateway_phone_prefixe
 * @property string $payment_gateway_cel_phone_num
 * @property string $payment_gateway_ipn_ack
 * @property string $payment_gateway_created_at
 * @property string $payment_gateway_updated_at
 * @property string $payment_gateway_cpm_result
 * @property string $payment_gateway_trans_status
 * @property string $payment_gateway_designation
 * @property string $payment_gateway_buyer_name
 * @property string $payment_gateway_signature
 */
class InvoicePayment extends Model
{
    use SoftDeletes;

    public $table = 'invoice_payments';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'invoice_id',
        'payment_method',
        'payment_reference',
        'amount',
        'currency_code',
        'currency_name',
        'coupon',
        'creator_id',
        'creator_name',
        'creator',
        'status',
        'is_waiting',
        'is_completed',
        'payment_gateway_trans_id',
        'payment_gateway_custom',
        'payment_gateway_currency',
        'payment_gateway_amount',
        'payment_gateway_payid',
        'payment_gateway_payment_date',
        'payment_gateway_payment_time',
        'payment_gateway_error_message',
        'payment_gateway_payment_method',
        'payment_gateway_phone_prefixe',
        'payment_gateway_cel_phone_num',
        'payment_gateway_ipn_ack',
        'payment_gateway_created_at',
        'payment_gateway_updated_at',
        'payment_gateway_cpm_result',
        'payment_gateway_trans_status',
        'payment_gateway_designation',
        'payment_gateway_buyer_name',
        'payment_gateway_signature'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'invoice_id' => 'integer',
        'payment_method' => 'string',
        'payment_reference' => 'string',
        'amount' => 'string',
        'currency_code' => 'string',
        'currency_name' => 'string',
        'coupon' => 'string',
        'creator_id' => 'string',
        'creator_name' => 'string',
        'creator' => 'string',
        'status' => 'string',
        'is_waiting' => 'boolean',
        'is_completed' => 'boolean',
        'payment_gateway_trans_id' => 'string',
        'payment_gateway_custom' => 'string',
        'payment_gateway_currency' => 'string',
        'payment_gateway_amount' => 'string',
        'payment_gateway_payid' => 'string',
        'payment_gateway_payment_date' => 'string',
        'payment_gateway_payment_time' => 'string',
        'payment_gateway_error_message' => 'string',
        'payment_gateway_payment_method' => 'string',
        'payment_gateway_phone_prefixe' => 'string',
        'payment_gateway_cel_phone_num' => 'string',
        'payment_gateway_ipn_ack' => 'string',
        'payment_gateway_created_at' => 'string',
        'payment_gateway_updated_at' => 'string',
        'payment_gateway_cpm_result' => 'string',
        'payment_gateway_trans_status' => 'string',
        'payment_gateway_designation' => 'string',
        'payment_gateway_buyer_name' => 'string',
        'payment_gateway_signature' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function creator(){
        return $this->belongsTo(Customer::class, 'creator_id');
    }

    public function invoice(){
        return $this->belongsTo(Invoice::class, 'invoice_id');
    }
}
