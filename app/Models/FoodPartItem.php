<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FoodPartItem
 * @package App\Models
 * @version July 27, 2020, 3:52 pm UTC
 *
 * @property integer $food_part_id
 * @property integer $food_id
 * @property string $title
 * @property string $price
 * @property string $inputType
 * @property boolean $is_add_on
 * @property boolean $has_display_price
 * @property boolean $has_price
 * @property string $currency_code
 * @property string $currency_name
 * @property boolean $is_essential
 */
class FoodPartItem extends Model
{
    use SoftDeletes;

    public $table = 'food_part_items';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'food_part_id',
        'food_id',
        'title',
        'price',
        'inputType',
        'is_add_on',
        'has_display_price',
        'has_price',
        'currency_code',
        'currency_name',
        'quantity'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'food_part_id' => 'integer',
        'food_id' => 'integer',
        'title' => 'string',
        'price' => 'string',
        // 'inputType' => 'string',
        'is_add_on' => 'boolean',
        'has_display_price' => 'boolean',
        'has_price' => 'boolean',
        'currency_code' => 'string',
        'currency_name' => 'string',
        'quantity' => 'integer'
        // 'is_essential' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function food(){
        return $this->belongsTo(Food::class, 'food_id');
    }

    public function foodPart(){
        return $this->belongsTo(FoodParts::class, 'food_part_id');
    }
}
