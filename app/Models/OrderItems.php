<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderItems
 * @package App\Models
 * @version January 8, 2021, 6:24 pm UTC
 *
 * @property integer $order_id
 * @property integer $quantity
 * @property integer $unit_price
 * @property integer $total_amount
 * @property integer $currency_code
 * @property integer $currency_name
 */
class OrderItems extends Model
{
    use SoftDeletes;

    public $table = 'order_items';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'order_id',
        'quantity',
        'unit_price',
        'total_amount',
        'currency_code',
        'currency_name',
        'meta_data_id',
        'meta_data',
        'data',
        'data_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'quantity' => 'integer',
        'unit_price' => 'string',
        'total_amount' => 'string',
        'currency_code' => 'string',
        'currency_name' => 'string',
        'meta_data_id'  =>  'integer',
        'meta_data' =>  'string',
        'data'  =>  'string',
        'data_id'   =>  'integer',
        'created_at'   =>  'string',
        'updated_at'   =>  'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
