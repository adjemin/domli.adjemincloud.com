<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Order;
/**
 * Class Invoice
 * @package App\Models
 * @version July 27, 2020, 4:40 pm UTC
 *
 * @property integer $order_id
 * @property integer $customer_id
 * @property string $reference
 * @property string $link
 * @property string $subtotal
 * @property string $tax
 * @property string $fees_delivery
 * @property string $discount
 * @property string $extra
 * @property string $service_fees
 * @property string $total
 * @property string $status
 * @property string $is_paid
 * @property string $currency_code
 * @property string $currency_name
 * @property string $service_slug
 * @property boolean $is_paid_to_business
 */
class Invoice extends Model
{
    use SoftDeletes;

    public $table = 'invoices';
    

    protected $dates = ['deleted_at'];

    const TAXES = 0.0;



    public $fillable = [
        'order_id',
        'customer_id',
        'reference',
        'link',
        'subtotal',
        'tax',
        'fees_delivery',
        'discount',
        'extra',
        'service_fees',
        'total',
        'status',
        'is_paid',
        'currency_code',
        'currency_name',
        'service_slug',
        'is_paid_to_business'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'customer_id' => 'integer',
        'reference' => 'string',
        'link' => 'string',
        'subtotal' => 'string',
        'tax' => 'string',
        'fees_delivery' => 'string',
        'discount' => 'string',
        'extra' => 'string',
        'service_fees' => 'string',
        'total' => 'string',
        'status' => 'string',
        'is_paid' => 'boolean',
        'currency_code' => 'string',
        'currency_name' => 'string',
        'service_slug' => 'string',
        'is_paid_to_business' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Generate ID
     * @return int|string
     */
    public static function generateID($service, $orderId, $customerId){
        //get last record
        $record = Invoice::count() + 1;

        return $service.'-'.$record.'-'.$orderId.'-'.$customerId.'-'.time();
    }

    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }

}
