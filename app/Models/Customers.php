<?php

namespace App\Models;

use Eloquent as Model;
// use App\Models\Geographical;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


/**
 * Class Customers
 * @package App\Models
 * @version January 8, 2021, 3:07 pm UTC
 *
 * @property string $name
 * @property string $lastname
 * @property integer $dial_code
 * @property integer $phone_number
 * @property integer $phone
 * @property string $email
 * @property string $photo_profile
 * @property integer $lng
 * @property integer $lat
 * @property string $langage
 * @property integer $country_code
 * @property integer $is_active
 * @property string $activation_date
 */

class Customers extends Authenticatable implements MustVerifyEmail
{
    use SoftDeletes, Notifiable, Geographical;

    public $table = 'customers';


    protected $dates = ['deleted_at'];

    // protected $appends = [
    //     'distance'
    // ];

    protected static $kilometers = true;

    const LATITUDE  = 'lat';
    const LONGITUDE = 'lng';

    public $fillable = [
        'name',
        'lastname',
        'dial_code',
        'country_code',
        'phone_number',
        'phone',
        'email',
        'password',
        'photo_profile',
        'region_name',
        'city',
        'lng',
        'lat',
        'language',
        'is_active',
        'email_verified_at',
        'activation_date',
        'token',
        'facebook_id',
        'google_id',
        'verified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'lastname' => 'string',
        'dial_code' => 'string',
        'phone_number' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'photo_profile' => 'string',
        'region_name'   => 'string',
        'city'  =>  'string',
        'lng' => 'string',
        'lat' => 'string',
        'langage' => 'string',
        'country_code' => 'string',
        'is_active' => 'boolean',
        'verified'  =>  'boolean',
        'activation_date'   =>  'string',
        'created_at' =>  'string',
        'updated_at' =>  'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:255',
        'lastname' => 'required|max:255',
        'phone_number' => 'required',
        'email' => 'required',
        'password', 'required|confirmed'
    ];

    public function verifyCustomer()
    {
        return $this->hasOne('App\\VerifyCustomer', 'customer_id');
    }


}
