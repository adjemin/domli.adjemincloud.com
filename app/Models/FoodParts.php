<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FoodParts
 * @package App\Models
 * @version January 9, 2021, 1:48 pm UTC
 *
 * @property integer $food_id
 * @property string $title
 * @property integer $price
 */
class FoodParts extends Model
{
    use SoftDeletes;

    public $table = 'food_parts';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'parent_id',
        'food_id',
        'title',
        'subtitle',
        'is_required',
        'has_children',
        'price',
        'inputType',

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'food_id' => 'integer',
        'title' => 'string',
        // 'price' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getItemsAttribute(){
        return  json_encode(FoodPartItem::where(['food_part_id' => $this->id])->get());
    }

    public function getFoodAttribute(){
        return Food::find($this->food_id);
    }

    public function parent(){
        return $this->belongsTo(FoodPart::class, 'parent_id');
    }
}
