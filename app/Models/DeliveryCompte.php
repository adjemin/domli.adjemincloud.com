<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DeliveryCompte
 * @package App\Models
 * @version March 29, 2021, 9:32 am UTC
 *
 * @property integer $delivery_id
 * @property string $compte
 */
class DeliveryCompte extends Model
{
    use SoftDeletes;

    public $table = 'delivery_comptes';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'delivery_id',
        'compte'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'delivery_id' => 'integer',
        'compte' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
