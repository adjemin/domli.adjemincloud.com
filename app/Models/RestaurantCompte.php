<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RestaurantCompte
 * @package App\Models
 * @version March 29, 2021, 9:28 am UTC
 *
 * @property integer $restaurant_id
 * @property string $compte
 */
class RestaurantCompte extends Model
{
    use SoftDeletes;

    public $table = 'restaurant_comptes';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'restaurant_id',
        'compte'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'restaurant_id' => 'integer',
        'compte' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
