<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderAssignement
 * @package App\Models
 * @version February 25, 2021, 12:52 pm UTC
 *
 * @property integer $order_id
 * @property integer $deliverer_id
 * @property string $acceptation_time
 * @property string $rejection_time
 * @property boolean $is_waiting_acceptation
 */
class OrderAssignement extends Model
{
    use SoftDeletes;

    public $table = 'order_assignements';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'order_id',
        'deliverer_id',
        'acceptation_time',
        'rejection_time',
        'is_waiting_acceptation'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'deliverer_id' => 'integer',
        'acceptation_time' => 'string',
        'rejection_time' => 'string',
        'is_waiting_acceptation' => 'boolean',
        'created_at'    =>  'string',
        'updated_at'    =>  'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function order(){
        return $this->belongsTo(Orders::class, 'order_id', 'id');
    }

    public function delivery(){
        return $this->belongsTo(Delivery::class, 'deliverer_id', 'id');
    }
}
