<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DeliveryPayment
 * @package App\Models
 * @version February 28, 2021, 7:49 pm UTC
 *
 * @property integer $deliverer_id
 * @property number $amount
 * @property boolean $is_paid
 * @property string $paid_date
 */
class DeliveryPayment extends Model
{
    use SoftDeletes;

    public $table = 'delivery_payments';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'deliverer_id',
        'amount',
        'is_paid',
        'paid_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'deliverer_id' => 'integer',
        'amount' => 'string',
        'is_paid' => 'boolean',
        'paid_date' => 'string',
        'created_at' => 'string',
        'updated_at' => 'string',
        'deleted_at' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
