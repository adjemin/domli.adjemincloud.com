<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RestaurantAvailability
 * @package App\Models
 * @version February 9, 2021, 10:25 am UTC
 *
 * @property integer $restaurant_id
 * @property string $start_date
 * @property string $end_date
 * @property string $start_time
 * @property string $end_time
 */
class RestaurantAvailability extends Model
{
    use SoftDeletes;

    public $table = 'restaurant_availabilities';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'restaurant_id',
        'start_date',
        'end_date',
        'start_time',
        'end_time'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'restaurant_id' => 'integer',
        'start_date' => 'date',
        'end_date' => 'date',
        'start_time' => 'date',
        'end_time' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function restaurant(){
        return $this->belongsTo(Restaurants::class, 'restaurant_id', 'id');
    }
    
}
