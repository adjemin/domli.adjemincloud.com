<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PasswordDelivery
 * @package App\Models
 * @version April 7, 2021, 11:35 am UTC
 *
 * @property string $password_reset_code
 * @property string $password_reset_deadline
 * @property int $delevery_id
 */
class PasswordDelivery extends Model
{
    use SoftDeletes;

    public $table = 'password_deliveries';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'password_reset_code',
        'password_reset_deadline',
        'delevery_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'password_reset_code' => 'string',
        'password_reset_deadline' => 'string',
        'delivery_id'   =>  'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
