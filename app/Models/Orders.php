<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Orders
 * @package App\Models
 * @version January 8, 2021, 5:09 pm UTC
 *
 * @property string $order_number
 * @property boolean $is_waiting
 * @property string $current_status
 * @property integer $payment_methode_id
 * @property integer $amount
 * @property string $currency_name
 * @property boolean $is_delivery
 * @property boolean $is_wayting_payment
 * @property integer $customer_id
 * @property integer $restaurant_id
 * @property integer $delivery_id
 * @property integer $customer_id
 * @property integer $restaurant_id
 * @property integer $delivery_id
 * @property integer $delivery_fees
 * @property string $currency_code
 */
class Orders extends Model
{
    use SoftDeletes;

    const WAITING = "waiting";
    const CONFIRMED = "confirmed";
    const EDITED = "edited";
    const REFUSED = "refused";
    const DELIVERED = "delivered";
    const FAILED = "failed";
    const CANCELED = "canceled";
    const NOTED = "noted";
    const REPORTED = "reported";
    const WAITING_CUSTOMER_PICKUP = "waiting_customer_pickup";
    const WAITING_RESTAURANT_PICKUP = "waiting_restaurant_pickup";
    const CUSTOMER_PICKED = "customer_picked";
    const WAITING_CUSTOMER_DELIVERY = "waiting_customer_delivery";
    const CUSTOMER_DELIVERED = "customer_delivered";
    const CUSTOMER_PAID = "customer_paid";
    const HAS_SELLER_PAID = "has_seller_paid";

    public $table = 'orders';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'order_number',
        'is_waiting',
        'current_status',
        'delivery_date',
        'completed_delivery_date',
        // 'payment_methode_id',
        'payment_method_slug',
        'amount',
        'currency_name',
        'is_delivery',
        'location_name',
        'location_lat',
        'location_lng',
        'is_wayting_payment',
        'is_cooking_completed',
        'is_waiting_delivery',
        'note',
        'restaurant_id',
        'delivery_id',
        'customer_id',
        // 'restaurant_id',
        // 'delivery_id',
        'delivery_fees',
        'currency_code',
        'kilometers',
        'is_order_delivered_completed',
        'order_delivered_status',
        'order_delivered_historique'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_number' => 'string',
        'is_waiting' => 'boolean',
        'current_status' => 'string',
        'delivery_date' => 'string',
        'location_name' => 'string',
        'location_lat' => 'string',
        'location_lng' => 'string',
        'is_cooking_completed' => 'boolean',
        'is_waiting_delivery'   => 'boolean',
        'note' => 'string',
        'completed_delivery_date' => 'string',
        'payment_method_slug' => 'string',
        'payment_methode_id' => 'string',
        'amount' => 'string',
        'taxe' => 'string',
        'currency_name' => 'string',
        'is_delivery' => 'boolean',
        'is_wayting_payment' => 'boolean',
        'customer_id' => 'integer',
        'restaurant_id' => 'integer',
        'delivery_id' => 'integer',
        'delivery_fees' => 'string',
        'kilometers' => 'string',
        'currency_code' => 'string',
        'is_order_delivered_completed' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function items(){
        return $this->hasMany(OrderItems::class, 'order_id', 'id');
    }

    public function customer(){
        return $this->belongsTo(Customers::class, 'customer_id', 'id');
    }

    public function delivery(){
        return $this->belongsTo(Delivery::class, 'delivery_id', 'id');
    }

    public function restaurant(){
        return $this->belongsTo(Restaurants::class, 'restaurant_id', 'id');
    }
}
