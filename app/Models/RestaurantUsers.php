<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RestaurantUsers
 * @package App\Models
 * @version January 9, 2021, 8:16 am UTC
 *
 * @property string $name
 * @property string $email
 * @property string $password
 * @property integer $dial_code
 * @property integer $phone_number
 * @property integer $phone
 * @property boolean $is_active
 * @property integer $restaurant_id
 */
class RestaurantUsers extends Model
{
    use SoftDeletes;

    public $table = 'restaurant_users';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'email',
        'password',
        'dial_code',
        'phone_number',
        'phone',
        'is_active',
        'restaurant_id',
        'user_level'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'dial_code' => 'integer',
        'phone_number' => 'integer',
        'phone' => 'integer',
        'is_active' => 'boolean',
        'restaurant_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
