<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CustomerNotification
 * @package App\Models
 * @version July 27, 2020, 5:15 pm UTC
 *
 * @property string $title
 * @property string $subtitle
 * @property string $action
 * @property string $action_by
 * @property string $meta_data
 * @property string $meta_data_id
 * @property string $type_notification
 * @property boolean $is_read
 * @property boolean $is_received
 * @property string $data
 * @property integer $customer_id
 * @property integer $data_id
 */
class DeliveryNotification extends Model
{
    use SoftDeletes;

    public $table = 'delivery_notifications';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'subtitle',
        'action',
        'action_by',
        'meta_data',
        'meta_data_id',
        'type_notification',
        'is_read',
        'is_received',
        'data',
        'delivery_id',
        'data_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'subtitle' => 'string',
        'action' => 'string',
        'action_by' => 'string',
        'meta_data' => 'string',
        'meta_data_id' => 'string',
        'type_notification' => 'string',
        'is_read' => 'boolean',
        'is_received' => 'boolean',
        'data' => 'string',
        'delivery_id' => 'integer',
        'data_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
