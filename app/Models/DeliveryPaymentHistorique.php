<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DeliveryPaymentHistorique
 * @package App\Models
 * @version March 29, 2021, 9:35 am UTC
 *
 * @property string $amount
 * @property integer $delivery_id
 * @property string $reference_paiement
 */
class DeliveryPaymentHistorique extends Model
{
    use SoftDeletes;

    public $table = 'delivery_payment_historiques';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'amount',
        'delivery_id',
        'reference_paiement',
        'request_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'amount' => 'string',
        'delivery_id' => 'integer',
        'reference_paiement' => 'string',
        'request_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function deliverer(){
        return $this->belongsTo(\App\Models\Delivery::class, 'delivery_id', 'id');
    }

    public function request_payment(){
        return $this->belongsTo(\App\Models\RequestDeliveryPayment::class, 'request_id', 'id');
    }
}
