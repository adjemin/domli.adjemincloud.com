<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CouponCustomer extends Model
{
    protected $table = 'customer_coupons';

    protected $fillable = [
        'customer_id',
        'order_id',
        'coupon_id',
        'is_used'
    ];

    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function coupon(){
        return $this->belongsTo(Coupon::class, 'coupon_id', 'id');
    }

    public function order(){
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }
    
}
