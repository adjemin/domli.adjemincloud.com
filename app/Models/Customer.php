<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @SWG\Definition(
 *      definition="Customer",
 *      required={"last_name", "first_name", "email"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="last_name",
 *          description="last_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="first_name",
 *          description="first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="dial_code",
 *          description="dial_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone_number",
 *          description="phone_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="country_code",
 *          description="country_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="company_name",
 *          description="company_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="login",
 *          description="login",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="password",
 *          description="password",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_active",
 *          description="is_active",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_blocked",
 *          description="is_blocked",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_phone_verified",
 *          description="is_phone_verified",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="phone_verified_at",
 *          description="phone_verified_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="default_language",
 *          description="default_language",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_merchant",
 *          description="is_merchant",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_particular",
 *          description="is_particular",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="remember_token",
 *          description="remember_token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Customer extends Authenticatable
{
    use SoftDeletes, Notifiable;

    public $table = 'customers';


    protected $dates = ['deleted_at'];

    protected $guard = 'customer';

    public $fillable = [
        'name',
        'lastname',
        'dial_code',
        'phone_number',
        'country_code',
        'email',
        'photo_profile',
        'lng',
        'lat',
        'langage',
        'is_active',
        'activation_date',
        // 'token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'lastname' => 'string',
        'dial_code' => 'string',
        'phone_number' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'photo_profile' => 'string',
        'region_name'   => 'string',
        'city'  =>  'string',
        'lng' => 'string',
        'lat' => 'string',
        'langage' => 'string',
        'country_code' => 'string',
        'is_active' => 'boolean',
        'verified'  =>  'boolean',
        'activation_date'   =>  'string',
        'created_at' =>  'string',
        'updated_at' =>  'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        /* 'last_name' => 'required',
        'first_name' => 'required',
        'email' => 'required|max:255|unique:customers' */
    ];

    protected $hidden = [
        'password'
    ];

    public function getPhoneNumberAccountAttribute() {
        $phoneNumberAccounts = PhoneNumberAccount::where(['customer_id' => $this->id])->get();
    }

    /**---- RELATIONSHIPS */
    public function balance(){
        return $this->hasOne(balance::class, 'source_id')->where('source', "customer");
    }

    public function merchant(){
        return $this->hasOne(Merchant::class);
    }

    public function balanceMerchant(){
        return $this->hasOne(Balance::class, 'source_id')->where('source', "merchant");
    }

}




























// <?php

// namespace App;

// use App\Notifications\CustomerResetPassword;
// use Illuminate\Notifications\Notifiable;
// use Illuminate\Foundation\Auth\User as Authenticatable;

// class Customer extends Authenticatable
// {
//     use Notifiable;

//     /**
//      * The attributes that are mass assignable.
//      *
//      * @var array
//      */
//     protected $fillable = [
//         'name', 'email', 'password',
//     ];

//     /**
//      * The attributes that should be hidden for arrays.
//      *
//      * @var array
//      */
//     protected $hidden = [
//         'password', 'remember_token',
//     ];

//     /**
//      * Send the password reset notification.
//      *
//      * @param  string  $token
//      * @return void
//      */
//     public function sendPasswordResetNotification($token)
//     {
//         $this->notify(new CustomerResetPassword($token));
//     }
// }
