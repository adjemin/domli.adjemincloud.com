<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Menus
 * @package App\Models
 * @version January 9, 2021, 1:35 pm UTC
 *
 * @property string $title
 * @property string $slug
 * @property integer $restaurant_id
 */
class Menus extends Model
{
    use SoftDeletes;

    public $table = 'menuses';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'restaurant_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'restaurant_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
