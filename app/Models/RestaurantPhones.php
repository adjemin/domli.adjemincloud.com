<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RestaurantPhones
 * @package App\Models
 * @version January 8, 2021, 6:26 pm UTC
 *
 * @property integer $restaurant_id
 * @property integer $dial_code
 * @property integer $phone_number
 * @property integer $phone
 */
class RestaurantPhones extends Model
{
    use SoftDeletes;

    public $table = 'restaurant_phones';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'restaurant_id',
        'dial_code',
        'phone_number',
        'phone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'restaurant_id' => 'integer',
        'dial_code' => 'integer',
        'phone_number' => 'integer',
        'phone' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
