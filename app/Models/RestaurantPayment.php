<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RestaurantPayment
 * @package App\Models
 * @version February 28, 2021, 7:46 pm UTC
 *
 * @property integer $restaurant_id
 * @property number $amount
 * @property boolean $is_paid
 * @property string $paid_date
 */
class RestaurantPayment extends Model
{
    use SoftDeletes;

    public $table = 'restaurant_payments';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'restaurant_id',
        'amount',
        'is_paid',
        'paid_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'restaurant_id' => 'integer',
        'amount' => 'double',
        'is_paid' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
