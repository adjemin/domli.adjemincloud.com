<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RestaurantMenus
 * @package App\Models
 * @version January 9, 2021, 7:50 am UTC
 *
 * @property string $title
 * @property string $slug
 */
class RestaurantMenus extends Model
{
    use SoftDeletes;

    public $table = 'restaurant_menuses';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'restaurant_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'restaurant_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function restaurant(){
        return $this->belongsTo(Restaurants::class, 'restaurant_id');
    }    
}
