<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class VehicleTypes
 * @package App\Models
 * @version January 8, 2021, 3:59 pm UTC
 *
 * @property string $title
 * @property string $description
 */
class VehicleTypes extends Model
{
    use SoftDeletes;

    public $table = 'vehicle_types';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'image',
        'title',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'image' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
