<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyRestaurantMail extends Mailable
{
    use Queueable, SerializesModels;

    public $restaurant;

    /**
     * Create a new message instance.
    *
    * @return void
    */
    public function __construct($Restaurant)
    {
        $this->restaurant = $Restaurant;
    }
  /**
  * Build the message.
  *
  * @return $this
  */
  public function build()
  {
    // $restaurant = $this->Restaurant;
    return $this->view('emails.verifyRestaurant');
  }
}
