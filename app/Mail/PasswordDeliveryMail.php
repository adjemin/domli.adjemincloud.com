<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordDeliveryMail extends Mailable
{
    use Queueable, SerializesModels;

    public $delivery;
    public $passwordDeliverer;

    /**
     * Create a new message instance.
    *
    * @return void
    */
    public function __construct($delivery, $passwordDeliverer)
    {
      $this->delivery = $delivery;
      $this->passwordDeliverer = $passwordDeliverer;
    }
  /**
  * Build the message.
  *
  * @return $this
  */
  public function build()
  {
    // $restaurant = $this->Restaurant;
    return $this->view('emails.passwordDelivery');
  }
}
