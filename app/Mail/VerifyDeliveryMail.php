<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyDeliveryMail extends Mailable
{
    use Queueable, SerializesModels;

    public $delivery;
    public $verifyDeliverer;

    /**
     * Create a new message instance.
    *
    * @return void
    */
    public function __construct($delivery, $verifyDeliverer)
    {
      $this->delivery = $delivery;
      $this->verifyDeliverer = $verifyDeliverer;
    }
  /**
  * Build the message.
  *
  * @return $this
  */
  public function build()
  {
    // $restaurant = $this->Restaurant;
    return $this->view('emails.verifyDelivery');
  }
}
