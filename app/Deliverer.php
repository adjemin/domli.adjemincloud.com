<?php

namespace App;

use App\Models\Geographical;
use App\Notifications\DelivererResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Deliverer extends Authenticatable
{
    use Notifiable, Geographical;

    protected $appends = [
        'distance'
    ];

    protected static $kilometers = true;

    const LATITUDE  = 'lat';
    const LONGITUDE = 'lng';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'password', 'country_code', 'dial_code', 'phone_number', 'phone', 'region_name', 'city', 'lat', 'lng', 'language', 'is_active', 'activation_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new DelivererResetPassword($token));
    }
}
