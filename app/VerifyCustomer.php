<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyCustomer extends Model
{
    protected $guarded = [
        //
    ];

    public function customers()
    {
        return $this->belongsTo('App\\Models\\Customers', 'customer_id');
    }
}
