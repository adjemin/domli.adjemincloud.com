<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Auth;

class UserIpAdress
{
    public function handle(Request $request, Closure $next){

        // Gettingip address of remote user
        // $user_ip_address = $request->ip();
        // $request->session()->put('user_ip_address', $request->ip());
        if(!$request->session()->exists('user_ip_address') || Auth::guard('deliverer')->check()){
            // $request->session()->put('user_ip_address',json_decode(file_get_contents('http://api.ipstack.com/'.$request->ip().'?access_key=7e79beb0acc11d71293ba82eace28363'), true));
            $request->session()->put('user_ip_address',json_decode(file_get_contents('http://api.ipstack.com/'.$request->ip().'?access_key=7e79beb0acc11d71293ba82eace28363'), true));

            if(Auth::guard('deliverer')->check()){
                $userCoordinates = $request->session()->get('user_ip_address');
                $user = Auth::guard('deliverer')->user();
                $user->region_name = $userCoordinates['region_name'];
                $user->city = $userCoordinates['city'];
                $user->lat = $userCoordinates['latitude'];
                $user->lng = $userCoordinates['longitude'];
                $user->language = $userCoordinates['location']['languages'][0]['name'];
                $user->save();
            }

        }

        return $next($request);
    }
}
