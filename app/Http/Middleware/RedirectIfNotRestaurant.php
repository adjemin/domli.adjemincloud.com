<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotRestaurant
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'restaurant')
	{
		// dd("ade8bd29-042a-4a48-9bba-1462b16eb859" == "ade8bd29-042a-4a48-9bba-1462b16eb859");
	    if(Auth::check()){
			return $next($request);
		}

		if (!Auth::guard($guard)->check()) {
	        return redirect('login/restaurant');
	    }else{
			$path = explode('/', url()->current());
			
			if(in_array($path[4], ['restaurants','restaurant'])){
				if($path[5] != Auth::guard($guard)->user()->uuid){
					return redirect('/backOffice/restaurants/'.Auth::guard($guard)->user()->uuid);
				}
			}else if(in_array($path[3], ['restaurants','restaurant'])){
				if($path[4] != Auth::guard($guard)->user()->uuid){
					return redirect('/backOffice/restaurants/'.Auth::guard($guard)->user()->uuid);
				}
			}
		}

	    return $next($request);
	}
}