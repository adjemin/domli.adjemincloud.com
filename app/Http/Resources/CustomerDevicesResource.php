<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerDevicesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'firebase_id' => $this->firebase_id,
            'device_model' => $this->device_model,
            'device_os' => $this->device_os,
            'device_os' => $this->device_os,
            'device_model_type' => $this->device_model_type,
            'device_meta_data' => $this->device_meta_data,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
