<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderAssignementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_id' => $this->order_id,
            'deliverer_id' => $this->deliverer_id,
            'acceptation_time' => $this->acceptation_time,
            'rejection_time' => $this->rejection_time,
            'is_waiting_acceptation' => $this->is_waiting_acceptation,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
