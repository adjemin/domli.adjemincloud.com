<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PasswordDeliveryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'password_reset_code' => $this->password_reset_code,
            'password_reset_deadline' => $this->password_reset_deadline,
            'delevery_id' => $this->delevery_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
