<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FoodResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'restaurant_id' => $this->restaurant_id,
            'menu_id' => $this->menu_id,
            'food_category_id' => $this->food_category_id,
            'title' => $this->title,
            'description' => $this->description,
            'image' => $this->image,
            'price' => $this->price,
            'currency_code' => $this->currency_code,
            'currency_name' => $this->currency_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
