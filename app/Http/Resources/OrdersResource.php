<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrdersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_number' => $this->order_number,
            'is_waiting' => $this->is_waiting,
            'current_status' => $this->current_status,
            'payment_methode_id' => $this->payment_methode_id,
            'amount' => $this->amount,
            'currency_name' => $this->currency_name,
            'is_delivery' => $this->is_delivery,
            'is_wayting_payment' => $this->is_wayting_payment,
            'customer_id' => $this->customer_id,
            'restaurant_id' => $this->restaurant_id,
            'delivery_id' => $this->delivery_id,
            'customer_id' => $this->customer_id,
            'restaurant_id' => $this->restaurant_id,
            'delivery_id' => $this->delivery_id,
            'delivery_fees' => $this->delivery_fees,
            'currency_code' => $this->currency_code,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
