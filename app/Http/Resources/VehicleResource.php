<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VehicleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'vehicleType_id' => $this->vehicleType_id,
            'deliverer_id' => $this->deliverer_id,
            'is_active' => $this->is_active,
            'numero_police' => $this->numero_police,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'vehicule_name' => $this->vehicule_name,
            'numero_serie' => $this->numero_serie,
            'immatriculation' => $this->immatriculation,
            'is_verified' => $this->is_verified,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
