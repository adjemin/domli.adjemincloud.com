<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_id' => $this->order_id,
            'quantity' => $this->quantity,
            'unit_price' => $this->unit_price,
            'total_amount' => $this->total_amount,
            'currency_code' => $this->currency_code,
            'currency_name' => $this->currency_name,
            'meta_data_id'  =>  $this->meta_data_id,
            'meta_data' =>  $this->meta_data,
            'data'  =>  $this->data,
            'data_id'   =>  $this->data_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
