<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OtpsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'dial_code' => $this->dial_code,
            'phone_number' => $this->phone_number,
            'deadline' => $this->deadline,
            'is_used' => $this->is_used,
            'is_testing' => $this->is_testing,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
