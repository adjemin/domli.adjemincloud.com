<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryCompteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'delivery_id' => $this->delivery_id,
            'compte' => $this->compte,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
