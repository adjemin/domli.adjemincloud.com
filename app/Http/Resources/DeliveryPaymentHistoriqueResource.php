<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryPaymentHistoriqueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'amount' => $this->amount,
            'delivery_id' => $this->delivery_id,
            'request_id' => $this->request_id,
            'reference_paiement' => $this->reference_paiement,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
