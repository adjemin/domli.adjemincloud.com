<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class JobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'location_name' => $this->location_name,
            'location_description' => $this->location_description,
            'deliverer_id' => $this->deliverer_id,
            'location_lat' => $this->location_lat,
            'location_lng' => $this->location_lng,
            'is_pickup' => $this->is_pickup,
            'status' => $this->status,
            'signature_url' => $this->signature_url,
            'images' => $this->images,
            'notes' => $this->notes,
            'note_audio' => $this->note_audio,
            'before_date' => $this->before_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
