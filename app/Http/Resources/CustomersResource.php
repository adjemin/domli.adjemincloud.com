<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'dial_code' => $this->dial_code,
            'phone_number' => $this->phone_number,
            'phone' => $this->phone,
            'email' => $this->email,
            'photo_profile' => $this->photo_profile,
            'lng' => $this->lng,
            'lat' => $this->lat,
            'langage' => $this->langage,
            'country_code' => $this->country_code,
            'is_active' => $this->is_active,
            'activation_date' => $this->activation_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
