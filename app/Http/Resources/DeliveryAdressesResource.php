<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryAdressesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'adress_name' => $this->adress_name,
            'adress_slug' => $this->adress_slug,
            'lng' => $this->lng,
            'town' => $this->town,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
