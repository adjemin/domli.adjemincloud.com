<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'email' => $this->email,
            'password' => $this->password,
            'dial_code' => $this->dial_code,
            'phone_number' => $this->phone_number,
            'phone' => $this->phone,
            'delivery_type_id' => $this->delivery_type_id,
            'vehicle_type_id' => $this->vehicle_type_id,
            'lng' => $this->lng,
            'lat' => $this->lat,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
