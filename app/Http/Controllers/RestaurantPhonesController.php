<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRestaurantPhonesRequest;
use App\Http\Requests\UpdateRestaurantPhonesRequest;
use App\Repositories\RestaurantPhonesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class RestaurantPhonesController extends AppBaseController
{
    /** @var  RestaurantPhonesRepository */
    private $restaurantPhonesRepository;

    public function __construct(RestaurantPhonesRepository $restaurantPhonesRepo)
    {
        $this->restaurantPhonesRepository = $restaurantPhonesRepo;
    }

    /**
     * Display a listing of the RestaurantPhones.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantPhones = $this->restaurantPhonesRepository->all();

        return view('restaurant_phones.index')
            ->with('restaurantPhones', $restaurantPhones);
    }

    /**
     * Show the form for creating a new RestaurantPhones.
     *
     * @return Response
     */
    public function create()
    {
        return view('restaurant_phones.create');
    }

    /**
     * Store a newly created RestaurantPhones in storage.
     *
     * @param CreateRestaurantPhonesRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantPhonesRequest $request)
    {
        $input = $request->all();

        $restaurantPhones = $this->restaurantPhonesRepository->create($input);

        Flash::success('Restaurant Phones saved successfully.');

        return redirect(route('restaurantPhones.index'));
    }

    /**
     * Display the specified RestaurantPhones.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $restaurantPhones = $this->restaurantPhonesRepository->find($id);

        if (empty($restaurantPhones)) {
            Flash::error('Restaurant Phones not found');

            return redirect(route('restaurantPhones.index'));
        }

        return view('restaurant_phones.show')->with('restaurantPhones', $restaurantPhones);
    }

    /**
     * Show the form for editing the specified RestaurantPhones.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $restaurantPhones = $this->restaurantPhonesRepository->find($id);

        if (empty($restaurantPhones)) {
            Flash::error('Restaurant Phones not found');

            return redirect(route('restaurantPhones.index'));
        }

        return view('restaurant_phones.edit')->with('restaurantPhones', $restaurantPhones);
    }

    /**
     * Update the specified RestaurantPhones in storage.
     *
     * @param int $id
     * @param UpdateRestaurantPhonesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantPhonesRequest $request)
    {
        $restaurantPhones = $this->restaurantPhonesRepository->find($id);

        if (empty($restaurantPhones)) {
            Flash::error('Restaurant Phones not found');

            return redirect(route('restaurantPhones.index'));
        }

        $restaurantPhones = $this->restaurantPhonesRepository->update($request->all(), $id);

        Flash::success('Restaurant Phones updated successfully.');

        return redirect(route('restaurantPhones.index'));
    }

    /**
     * Remove the specified RestaurantPhones from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $restaurantPhones = $this->restaurantPhonesRepository->find($id);

        if (empty($restaurantPhones)) {
            Flash::error('Restaurant Phones not found');

            return redirect(route('restaurantPhones.index'));
        }

        $this->restaurantPhonesRepository->delete($id);

        Flash::success('Restaurant Phones deleted successfully.');

        return redirect(route('restaurantPhones.index'));
    }
}
