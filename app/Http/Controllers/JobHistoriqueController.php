<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJobHistoriqueRequest;
use App\Http\Requests\UpdateJobHistoriqueRequest;
use App\Repositories\JobHistoriqueRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class JobHistoriqueController extends AppBaseController
{
    /** @var  JobHistoriqueRepository */
    private $jobHistoriqueRepository;

    public function __construct(JobHistoriqueRepository $jobHistoriqueRepo)
    {
        $this->jobHistoriqueRepository = $jobHistoriqueRepo;
    }

    /**
     * Display a listing of the JobHistorique.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $jobHistoriques = $this->jobHistoriqueRepository->all();

        return view('job_historiques.index')
            ->with('jobHistoriques', $jobHistoriques);
    }

    /**
     * Show the form for creating a new JobHistorique.
     *
     * @return Response
     */
    public function create()
    {
        return view('job_historiques.create');
    }

    /**
     * Store a newly created JobHistorique in storage.
     *
     * @param CreateJobHistoriqueRequest $request
     *
     * @return Response
     */
    public function store(CreateJobHistoriqueRequest $request)
    {
        $input = $request->all();

        $jobHistorique = $this->jobHistoriqueRepository->create($input);

        Flash::success('Job Historique saved successfully.');

        return redirect(route('jobHistoriques.index'));
    }

    /**
     * Display the specified JobHistorique.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jobHistorique = $this->jobHistoriqueRepository->find($id);

        if (empty($jobHistorique)) {
            Flash::error('Job Historique not found');

            return redirect(route('jobHistoriques.index'));
        }

        return view('job_historiques.show')->with('jobHistorique', $jobHistorique);
    }

    /**
     * Show the form for editing the specified JobHistorique.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jobHistorique = $this->jobHistoriqueRepository->find($id);

        if (empty($jobHistorique)) {
            Flash::error('Job Historique not found');

            return redirect(route('jobHistoriques.index'));
        }

        return view('job_historiques.edit')->with('jobHistorique', $jobHistorique);
    }

    /**
     * Update the specified JobHistorique in storage.
     *
     * @param int $id
     * @param UpdateJobHistoriqueRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobHistoriqueRequest $request)
    {
        $jobHistorique = $this->jobHistoriqueRepository->find($id);

        if (empty($jobHistorique)) {
            Flash::error('Job Historique not found');

            return redirect(route('jobHistoriques.index'));
        }

        $jobHistorique = $this->jobHistoriqueRepository->update($request->all(), $id);

        Flash::success('Job Historique updated successfully.');

        return redirect(route('jobHistoriques.index'));
    }

    /**
     * Remove the specified JobHistorique from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jobHistorique = $this->jobHistoriqueRepository->find($id);

        if (empty($jobHistorique)) {
            Flash::error('Job Historique not found');

            return redirect(route('jobHistoriques.index'));
        }

        $this->jobHistoriqueRepository->delete($id);

        Flash::success('Job Historique deleted successfully.');

        return redirect(route('jobHistoriques.index'));
    }
}
