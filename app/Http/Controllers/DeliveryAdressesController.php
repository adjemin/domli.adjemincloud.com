<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDeliveryAdressesRequest;
use App\Http\Requests\UpdateDeliveryAdressesRequest;
use App\Repositories\DeliveryAdressesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DeliveryAdressesController extends AppBaseController
{
    /** @var  DeliveryAdressesRepository */
    private $deliveryAdressesRepository;

    public function __construct(DeliveryAdressesRepository $deliveryAdressesRepo)
    {
        $this->deliveryAdressesRepository = $deliveryAdressesRepo;
    }

    /**
     * Display a listing of the DeliveryAdresses.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryAdresses = $this->deliveryAdressesRepository->all();

        return view('delivery_adresses.index')
            ->with('deliveryAdresses', $deliveryAdresses);
    }

    /**
     * Show the form for creating a new DeliveryAdresses.
     *
     * @return Response
     */
    public function create()
    {
        return view('delivery_adresses.create');
    }

    /**
     * Store a newly created DeliveryAdresses in storage.
     *
     * @param CreateDeliveryAdressesRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryAdressesRequest $request)
    {
        $input = $request->all();

        $deliveryAdresses = $this->deliveryAdressesRepository->create($input);

        Flash::success('Delivery Adresses saved successfully.');

        return redirect(route('deliveryAdresses.index'));
    }

    /**
     * Display the specified DeliveryAdresses.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deliveryAdresses = $this->deliveryAdressesRepository->find($id);

        if (empty($deliveryAdresses)) {
            Flash::error('Delivery Adresses not found');

            return redirect(route('deliveryAdresses.index'));
        }

        return view('delivery_adresses.show')->with('deliveryAdresses', $deliveryAdresses);
    }

    /**
     * Show the form for editing the specified DeliveryAdresses.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $deliveryAdresses = $this->deliveryAdressesRepository->find($id);

        if (empty($deliveryAdresses)) {
            Flash::error('Delivery Adresses not found');

            return redirect(route('deliveryAdresses.index'));
        }

        return view('delivery_adresses.edit')->with('deliveryAdresses', $deliveryAdresses);
    }

    /**
     * Update the specified DeliveryAdresses in storage.
     *
     * @param int $id
     * @param UpdateDeliveryAdressesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryAdressesRequest $request)
    {
        $deliveryAdresses = $this->deliveryAdressesRepository->find($id);

        if (empty($deliveryAdresses)) {
            Flash::error('Delivery Adresses not found');

            return redirect(route('deliveryAdresses.index'));
        }

        $deliveryAdresses = $this->deliveryAdressesRepository->update($request->all(), $id);

        Flash::success('Delivery Adresses updated successfully.');

        return redirect(route('deliveryAdresses.index'));
    }

    /**
     * Remove the specified DeliveryAdresses from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryAdresses = $this->deliveryAdressesRepository->find($id);

        if (empty($deliveryAdresses)) {
            Flash::error('Delivery Adresses not found');

            return redirect(route('deliveryAdresses.index'));
        }

        $this->deliveryAdressesRepository->delete($id);

        Flash::success('Delivery Adresses deleted successfully.');

        return redirect(route('deliveryAdresses.index'));
    }
}
