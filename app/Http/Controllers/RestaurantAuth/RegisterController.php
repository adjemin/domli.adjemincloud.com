<?php

namespace App\Http\Controllers\RestaurantAuth;

use Imgur;
// use App\Mail\AdminNotifyEmail;
// use App\Mail\CustomerWelcomeMail;
use OTPHP\TOTP;
use App\Utils\UUID;
use App\User;
use App\Models\Customers;
use App\VerifyRestaurant;
use Laracasts\Flash\Flash;
// use Illuminate\Support\Facades\Mail;
use App\Models\Restaurants;
use Illuminate\Http\Request;
// use App\Models\Otps;
use App\Mail\NotifyAdmin;
use App\Mail\VerifyRestaurantMail;
use App\Http\Controllers\Controller;
use App\Models\RestaurantCategories;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyDeliveryMail;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Validator;
// use Gloudemans\Shoppingcart\Facades\Cart;
use App\Repositories\RestaurantsRepository;
use Illuminate\Foundation\Auth\RegistersUsers;

// use Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/login/restaurant';
    // protected $redirectToMerchant = '/merchant/home';

    /** @var  CustomersRepository */
    private $customersRepository;

    public function __construct(RestaurantsRepository $restaurantsRepo)
    {
        $this->restaurantsRepository = $restaurantsRepo;
    }
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('customer.guest');
    // }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'restaurant_adress_name' => 'required|max:255',
            'responsable_name' => 'required|max:255',
            'phone_number' => 'required',
            'email' => 'required|unique:restaurants',
        ]);
    }

    protected function validatorRestaurant(array $data)
    {
        return Validator::make($data, [
            'responsable_photo' => 'required',
            'responsable_paper_id_number' => 'required|max:255',
            'restaurant_registration_number' => 'required|max:255',
        ]);
    }

    public function storeRestaurantAccount(Request $request)
    {
        $this->validator($request->all())->validate();

        $field = $request->all();
        $userCoordinates = 'http://api.ipstack.com/'.$request->ip().'?access_key=7e79beb0acc11d71293ba82eace28363';
        $userCoordinates = json_decode(file_get_contents($userCoordinates), true);

        $field['responsable_name'] = $request->responsable_name;
        $field['name'] = $request->name;
        $field["dial_code"] = $request->dial_code;
        $field["phone_number"] = $request->phone_number;
        $field["country_code"] = $request->country_code;
        $field["email"] = $request->email;
        $field['password'] = bcrypt($request->password);
        $field['cover'] =  asset('img/default-restaurant.png');
        $field['restaurant_region_name'] = $userCoordinates['region_name'];
        $field['restaurant_adress_name'] = $request->restaurant_adress_name;
        $field["restaurant_city"] = $userCoordinates['city'];
        $field["lat"] = $userCoordinates['latitude'];
        $field["lng"] = $userCoordinates['longitude'];
        $field['is_active'] = false;
        $field['uuid'] = UUID::v4();
        // $field['activation_date'] = Now();

        $restaurant = $this->restaurantsRepository->create($field);

        $verifyRestaurant  =  VerifyRestaurant::create([
            'restaurant_id'  =>   $restaurant->id,
            'token'  =>   sha1(time())
        ]);

        Mail::to($restaurant)->send(new VerifyRestaurantMail($restaurant));

        $users = User::all();

        foreach ($users as $user) {
            Mail::to($user)->send(new NotifyAdmin($restaurant));
        }
        
        if($restaurant->verified){
            Auth::guard('restaurant')->loginUsingId($restaurant->id);
            Auth::guard('restaurant')->check();

            return redirect(route('restaurantHome'));
        }

        // dd($verifyRestaurant);
        return view('frontOffice.restaurant.pages.waitpage', compact('restaurant'));
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('customer.auth.login');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('customer');
    }

    // public function confirmEmail(Request $request)
    // {

    //     $customer_email = $request->get('customer_email');
    //     // dd($customer_email);
    //     $customer = Customers::where(['email' => $customer_email])->first();

    //     if (empty($customer)) {
    //         return view('errors.404');
    //     }

    //     $customer->is_active = true;
    //     $customer->email_verified_at = now();
    //     $customer->activation_date = now();

    //     $customer->save();

    //     return redirect()->route('customerLogin');

    // }

    public function showHomePage(){
        $restaurants = Restaurants::all()->take(6);
        $restaurant_categories = RestaurantCategories::all()->take(6);

        return view('customer.homePage.index', compact('restaurants', 'restaurant_categories'));
    }

    //Mail verification

    public function verifyRestaurant($token) {

        $verifyrestaurant = VerifyRestaurant::where('token', $token)->first();
        // dd($verifyrestaurant);
        $restaurant = Restaurants::where('id', $verifyrestaurant->restaurant_id)->first();
        // dd($restaurant);
        // $restaurant = $verifyrestaurant->restaurant;


        if(isset($verifyrestaurant) ){
            $restaurant = $verifyrestaurant->restaurants;
            if(!$restaurant->verified) {
                $restaurant->verified = 1;
                $restaurant->save();
            }
        } else {
            return redirect(route('restaurantHome'))->with('warning', "Sorry your email cannot be identified.");
        }
        return redirect(route('restaurantLastRegister', ['uuid' => $restaurant->uuid]));
    }

     public function authenticated(Request $request, $restaurant){
        if (!$restaurant->verified) {
            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        return redirect()->intended($this->redirectPath());
    }

    protected function registered(Request $request, $restaurant){
        $this->guard()->logout();
        return redirect(route('restaurantHome'))->with('status', 'We sent you an activation code. Check your email and click on the link to verify.');
    }

    public function restaurantLastRegister($uuid){

        $restaurant = Restaurants::where('uuid', $uuid)->first();
        return view('frontOffice.restaurant.pages.lastRegisterStep', compact('restaurant'));

    }

    public function restaurantConfirmView(){

        return view('frontOffice.restaurant.pages.congratulate');

    }

    public function afterRegistrationView(){

        return view('frontOffice.restaurant.pages.restaurantWaitView');

    }

    public function getOTPByPhone(Request $request)
    {
        $dial_code = $request->dial_code;
        $phone_number = $request->phone_number;

        $lang = 'FR';

        if (!$request->has('dial_code')) {
            return response('Dial Code not found');
        }

        if (!$request->has('phone_number')) {
            return response('Phone number not found');
        }

        // if (!$request->has('lang')) {
        //     return response('lang not found');
        // }

        $totp = TOTP::create(
            null, // Let the secret be defined by the class
            10, // The period (10 seconds)
            'sha1', // The digest algorithm
            6// The output will generate 6 digits
        );

        $otp_code = $totp->now();

        //SEND SMS
        $client_phone = $dial_code . "" . $phone_number;
        // if ($lang == 'FR') {
        $message = "Vous devez entrer ce code dans Jobbing: " . $otp_code;
        // } else {
        //     $message  = "You must enter this code in E-Mairie: " . $otp_code;
        // }
        $otpModel = Otp::where(['code' => $otp_code])->first();
        if (empty($otpModel)) {
            $otpModel = new Otp();
            $otpModel->code = $otp_code;
            $otpModel->dial_code = $dial_code;
            $otpModel->phone_number = $phone_number;
            $deadline = Carbon::now()->addMinutes(60)->toDateTimeString();
            $otpModel->deadline = $deadline;
            $otpModel->is_used = false;
            $otpModel->save();
        }

        $otpModel->dial_code = $dial_code;
        $otpModel->phone_number = $phone_number;
        $deadline = Carbon::now()->addMinutes(60)->toDateTimeString();
        $otpModel->deadline = $deadline;
        $otpModel->is_used = false;
        $otpModel->save();

        // dd($otpModel);
        $this->sendSMS($client_phone, $message);

        // if ($result) {
        //     return response($otpModel->toArray(), 'OTP sent');
        // } else {
        //     return response('Error found');
        // }
    }

    public function updateRestaurant(Request $request){

        $this->validatorRestaurant($request->all())->validate();

        $restaurant = Restaurants::whereIn('uuid', [$request->uuid])->first();

        if(!$restaurant){
            Flash::error("Restaurant not found");
            return redirect()->back();
        }

        if ($request->file('responsable_photo')) {
            $image = $request->file('responsable_photo');
            $pictures = [];
            if ($image !== null) {
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
            }
        } else {
            $productImageLink = "https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png";
        }

        $restaurant->update([
            'responsable_photo' => $productImageLink,
            'restaurant_registration_number' => $request->restaurant_registration_number,
            'restaurant_paper_id_type' => $request->restaurant_paper_id_type,
            'responsable_paper_id_number' => $request->responsable_paper_id_number
        ]);

        Flash::success("Responsable data save successfully");

        return redirect(route('afterRegistrationView'));
    }
}
