<?php
namespace App\Http\Controllers\RestaurantAuth;

use App\Models\restaurant;
use App\Models\restaurants;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
// use Hesto\MultiAuth\Traits\LogsoutGuard;
use App\Providers\RouteServiceProvider;
use Hesto\MultiAuth\Traits\LogsoutGuard;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers, LogsoutGuard {
    //     LogsoutGuard::logout insteadof AuthenticatesUsers;
    // }
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect()->to('/login/restaurant');
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/home/restaurant';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    function __construct()
    {
        $this->middleware('restaurant.guest', ['except' => 'logout']);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('frontOffice.restaurant.homePage.index');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('restaurant');
    }

    protected function login(Request $request){

        $credentials = $request->only('email', 'password');

        if (Auth::guard('restaurant')->attempt($credentials)) {
            // $message = 'success';
            // Authentication passed...
            $user = Auth::guard('restaurant')->user();
            // dd($user);
            // $userCoordinates = $request->session()->get('user_ip_address');
            // // dd($userCoordinates);
            // if(is_null($user->restaurant_region_name)){
            //     $user->restaurant_region_name = $userCoordinates['region_name'] ?? '';
            // }

            // if(is_null($user->city)){
            //     $user->city = $userCoordinates['city'] ?? '';
            // }

            // if(is_null($user->lat)){
            //     $user->lat = $userCoordinates['latitude'] ?? 0;
            // }


            // if(is_null($user->lng)){
            //     $user->lng = $userCoordinates['longitude'] ?? 0;
            // }

            // if (isset($userCoordinates['location']['languages'][0]['name']) && !empty($userCoordinates['location']['languages'][0]['name'])) {
            //     $user->language = $userCoordinates['location']['languages'][0]['name'];
            // }

            // $account = \App\Models\RestaurantCompte::where('restaurant_id', $user->id)->first();

            // if(empty($account) || is_null($account)){
            //     $paymentDeliveries = \App\Models\RestaurantPayment::where(['restaurant_id' => $user->id])->get();

            //     $sumDelivery = collect($paymentDeliveries)->reduce(function($carry, $item){
            //         return $item->amount + $carry;
            //     }, 0);
                
            //     $account = \App\Models\RestaurantCompte::create([
            //         'restaurant_id' => $user->id,
            //         'compte' => $sumDelivery
            //     ]);
            // }


            // $user->save();
            $data = [
                'url' => url('/backOffice/restaurants/'.$user->uuid),
                'status' => 'success'
            ];
            return response()->json($data);
        }else {
            return response()->json([
                'status' => 'error'
            ]);
            // return json_encode($data);
        }
        // return $message;
        return response()->json([
            'status' => 'error'
        ]);
    }

    function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    function handleProviderCallback($provider)
    {
        try {
            if ($provider == "google") {
                $user = Socialite::driver($provider)->user();
            } else {
                $user = Socialite::driver($provider)->fields([
                    'first_name', 'last_name', 'email', 'picture', 'short_name', 'name',
                ])->user();
            }
        } catch (\Exception $e) {
            return redirect('auth/' . $provider);
        }

        // dd($user);
        if ($user->email) {
            if ($provider == "facebook") {
                $authUser = Restaurants::where('facebook_id', $user->id)->orWhere('email', $user->email)->first();
                if (!is_null($authUser)) {
                    if (is_null($authUser->email_verifed_at) || $authUser->is_active == 0) {
                        $authUser->email_verifed_at = now();
                        $authUser->activation_date = now();
                        $authUser->is_active = 1;
                        $authUser->save();
                    }
                    //return $authUser;
                    // $aut = Auth::login($authUser, true);
                    Auth::guard('restaurant')->login($authUser);
                    return redirect($this->redirectTo);
                } else {
                    $new_user = new restaurant();
                    $new_user->name = $user->name;
                    $new_user->first_name = $user->user['first_name'];
                    $new_user->last_name = $user->user['last_name'];
                    $new_user->email = $user->email;
                    $new_user->facebook_id = $user->id;
                    $new_user->is_active = false;
                    $new_user->photo = $user->avatar ?? 'https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png';
                    $new_user->save();
                    return redirect($this->redirectTo);
                }
            } else if ($provider == "google") {
                //dd($user);
                $authUser = Restaurants::where('google_id', $user->id)->orWhere('email', $user->email)->first();

                if (!is_null($authUser)) {
                    if (is_null($authUser->email_verifed_at) || $authUser->is_active == 0) {
                        $authUser->email_verifed_at = now();
                        $authUser->activation_date = now();
                        $authUser->is_active = 1;
                        $authUser->save();
                    }
                    Auth::guard('restaurant')->login($authUser);
                    // return redirect($this->redirectTo);
                    return redirect(route('restaurant.home'));
                } else {
                    $new_user = new Restaurants();
                    $new_user->name = $user->name;
                    $new_user->first_name = $user->user['given_name'];
                    $new_user->last_name = $user->user['family_name'];
                    $new_user->email = $user->email;
                    $new_user->google_id = $user->id;
                    $new_user->is_active = false;
                    $new_user->photo = $user->avatar;
                    $new_user->save();
                    // return redirect($this->redirectTo);
                    return redirect(route('restaurant.home'));
                }
            }

        } else {
            return Redirect::route('restaurantLogin');
        }
    }


}
