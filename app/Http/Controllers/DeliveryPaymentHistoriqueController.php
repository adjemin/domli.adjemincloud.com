<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDeliveryPaymentHistoriqueRequest;
use App\Http\Requests\UpdateDeliveryPaymentHistoriqueRequest;
use App\Repositories\DeliveryPaymentHistoriqueRepository;
use App\Models\DeliveryPaymentHistorique;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DeliveryPaymentHistoriqueController extends AppBaseController
{
    /** @var  DeliveryPaymentHistoriqueRepository */
    private $deliveryPaymentHistoriqueRepository;

    public function __construct(DeliveryPaymentHistoriqueRepository $deliveryPaymentHistoriqueRepo)
    {
        $this->deliveryPaymentHistoriqueRepository = $deliveryPaymentHistoriqueRepo;
    }

    /**
     * Display a listing of the DeliveryPaymentHistorique.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryPaymentHistoriques = $this->deliveryPaymentHistoriqueRepository->all();

        return view('delivery_payment_historiques.index')
            ->with('deliveryPaymentHistoriques', $deliveryPaymentHistoriques);
    }

    /**
     * Show the form for creating a new DeliveryPaymentHistorique.
     *
     * @return Response
     */
    public function create()
    {
        return view('delivery_payment_historiques.create');
    }

    /**
     * Store a newly created DeliveryPaymentHistorique in storage.
     *
     * @param CreateDeliveryPaymentHistoriqueRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryPaymentHistoriqueRequest $request)
    {
        $input = $request->all();

        $delivery = \App\Models\DeliveryCompte::where('delivery_id', $request->delivery_id)->first();

        if(is_null($delivery) || empty($delivery)){
            Flash::error("Deliverer not found");
            return redirect()->back();
        }

        if($request->amount > $delivery->compte){
            Flash::error("Insuffisant balance");
            return redirect()->back();
        }

        $requestPayment = \App\Models\RequestDeliveryPayment::where(['amount' => $request->amount, 'is_paid' => false])->orWhere(['id' => $request->request_id])->first();

        if(empty($requestPayment) || is_null($requestPayment)){
            Flash::error("Request not found");
            return redirect()->back();
        }
        
        $requestPayment->is_paid = true;
        $requestPayment->save();

        $delivery->compte = floatval($delivery->compte) - floatval($request->amount); 
        $delivery->save(); 

        $deliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepository->create($input);

        Flash::success('Delivery Payment Historique saved successfully.');

        return redirect(route('deliveryPaymentHistoriques.index'));
    }

    /**
     * Display the specified DeliveryPaymentHistorique.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepository->find($id);

        if (empty($deliveryPaymentHistorique)) {
            Flash::error('Delivery Payment Historique not found');

            return redirect(route('deliveryPaymentHistoriques.index'));
        }

        return view('delivery_payment_historiques.show')->with('deliveryPaymentHistorique', $deliveryPaymentHistorique);
    }

    /**
     * Show the form for editing the specified DeliveryPaymentHistorique.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $deliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepository->find($id);

        if (empty($deliveryPaymentHistorique)) {
            Flash::error('Delivery Payment Historique not found');

            return redirect(route('deliveryPaymentHistoriques.index'));
        }

        return view('delivery_payment_historiques.edit')->with('deliveryPaymentHistorique', $deliveryPaymentHistorique);
    }

    /**
     * Update the specified DeliveryPaymentHistorique in storage.
     *
     * @param int $id
     * @param UpdateDeliveryPaymentHistoriqueRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryPaymentHistoriqueRequest $request)
    {
        $deliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepository->find($id);

        if (empty($deliveryPaymentHistorique)) {
            Flash::error('Delivery Payment Historique not found');

            return redirect(route('deliveryPaymentHistoriques.index'));
        }

        $deliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepository->update($request->all(), $id);

        Flash::success('Delivery Payment Historique updated successfully.');

        return redirect(route('deliveryPaymentHistoriques.index'));
    }

    /**
     * Remove the specified DeliveryPaymentHistorique from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepository->find($id);

        if (empty($deliveryPaymentHistorique)) {
            Flash::error('Delivery Payment Historique not found');

            return redirect(route('deliveryPaymentHistoriques.index'));
        }

        $this->deliveryPaymentHistoriqueRepository->delete($id);

        Flash::success('Delivery Payment Historique deleted successfully.');

        return redirect(route('deliveryPaymentHistoriques.index'));
    }

    public function delivery(Request $request, $delivery_id){
        $deliveryPaymentHistoriques  = DeliveryPaymentHistorique::where('delivery_id', $delivery_id)->orderByDesc('id')->paginate(10);

        // if(empty($deliveryPaymentHistoriques) || is_null($deliveryPaymentHistoriques)){
        //     Flash::error('Delivery not found');
        //     return redirect()->back();
        // }

        return view('delivery_payment_historiques.index')
        ->with('deliveryPaymentHistoriques', $deliveryPaymentHistoriques);
    }
}
