<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRequestDeliveryPaymentRequest;
use App\Http\Requests\UpdateRequestDeliveryPaymentRequest;
use App\Repositories\RequestDeliveryPaymentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\RequestDeliveryPayment;
use Flash;
use Response;

class RequestDeliveryPaymentController extends AppBaseController
{
    /** @var  RequestDeliveryPaymentRepository */
    private $requestDeliveryPaymentRepository;

    public function __construct(RequestDeliveryPaymentRepository $requestDeliveryPaymentRepo)
    {
        $this->requestDeliveryPaymentRepository = $requestDeliveryPaymentRepo;
    }

    /**
     * Display a listing of the RequestDeliveryPayment.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $requestDeliveryPayments = $this->requestDeliveryPaymentRepository->all();

        return view('request_delivery_payments.index')
            ->with('requestDeliveryPayments', $requestDeliveryPayments);
    }

    /**
     * Show the form for creating a new RequestDeliveryPayment.
     *
     * @return Response
     */
    public function create()
    {
        return view('request_delivery_payments.create');
    }

    /**
     * Store a newly created RequestDeliveryPayment in storage.
     *
     * @param CreateRequestDeliveryPaymentRequest $request
     *
     * @return Response
     */
    public function store(CreateRequestDeliveryPaymentRequest $request)
    {
        $input = $request->all();

        $requestDeliveryPayment = $this->requestDeliveryPaymentRepository->create($input);

        Flash::success('Request Delivery Payment saved successfully.');

        return redirect(route('requestDeliveryPayments.index'));
    }

    /**
     * Display the specified RequestDeliveryPayment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $requestDeliveryPayment = $this->requestDeliveryPaymentRepository->find($id);

        if (empty($requestDeliveryPayment)) {
            Flash::error('Request Delivery Payment not found');

            return redirect(route('requestDeliveryPayments.index'));
        }

        return view('request_delivery_payments.show')->with('requestDeliveryPayment', $requestDeliveryPayment);
    }

    /**
     * Show the form for editing the specified RequestDeliveryPayment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $requestDeliveryPayment = $this->requestDeliveryPaymentRepository->find($id);

        if (empty($requestDeliveryPayment)) {
            Flash::error('Request Delivery Payment not found');

            return redirect(route('requestDeliveryPayments.index'));
        }

        return view('request_delivery_payments.edit')->with('requestDeliveryPayment', $requestDeliveryPayment);
    }

    /**
     * Update the specified RequestDeliveryPayment in storage.
     *
     * @param int $id
     * @param UpdateRequestDeliveryPaymentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRequestDeliveryPaymentRequest $request)
    {
        $requestDeliveryPayment = $this->requestDeliveryPaymentRepository->find($id);

        if (empty($requestDeliveryPayment)) {
            Flash::error('Request Delivery Payment not found');

            return redirect(route('requestDeliveryPayments.index'));
        }

        $requestDeliveryPayment = $this->requestDeliveryPaymentRepository->update($request->all(), $id);

        Flash::success('Request Delivery Payment updated successfully.');

        return redirect(route('requestDeliveryPayments.index'));
    }

    /**
     * Remove the specified RequestDeliveryPayment from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $requestDeliveryPayment = $this->requestDeliveryPaymentRepository->find($id);

        if (empty($requestDeliveryPayment)) {
            Flash::error('Request Delivery Payment not found');

            return redirect(route('requestDeliveryPayments.index'));
        }

        $this->requestDeliveryPaymentRepository->delete($id);

        Flash::success('Request Delivery Payment deleted successfully.');

        return redirect(route('requestDeliveryPayments.index'));
    }


    public function delivery(Request $request, $delivery_id){
        $requestDeliveryPayments  = RequestDeliveryPayment::where('delivery_id', $delivery_id)->orderByDesc('id')->paginate(10);

        return view('request_delivery_payments.index')
        ->with('requestDeliveryPayments', $requestDeliveryPayments);
    }
}
