<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderAssignementRequest;
use App\Http\Requests\UpdateOrderAssignementRequest;
use App\Repositories\OrderAssignementRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OrderAssignementController extends AppBaseController
{
    /** @var  OrderAssignementRepository */
    private $orderAssignementRepository;

    public function __construct(OrderAssignementRepository $orderAssignementRepo)
    {
        $this->orderAssignementRepository = $orderAssignementRepo;
    }

    /**
     * Display a listing of the OrderAssignement.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orderAssignements = $this->orderAssignementRepository->all();

        return view('order_assignements.index')
            ->with('orderAssignements', $orderAssignements);
    }

    /**
     * Show the form for creating a new OrderAssignement.
     *
     * @return Response
     */
    public function create()
    {
        return view('order_assignements.create');
    }

    /**
     * Store a newly created OrderAssignement in storage.
     *
     * @param CreateOrderAssignementRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderAssignementRequest $request)
    {
        $input = $request->all();

        $orderAssignement = $this->orderAssignementRepository->create($input);

        Flash::success('Order Assignement saved successfully.');

        return redirect(route('orderAssignements.index'));
    }

    /**
     * Display the specified OrderAssignement.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orderAssignement = $this->orderAssignementRepository->find($id);

        if (empty($orderAssignement)) {
            Flash::error('Order Assignement not found');

            return redirect(route('orderAssignements.index'));
        }

        return view('order_assignements.show')->with('orderAssignement', $orderAssignement);
    }

    /**
     * Show the form for editing the specified OrderAssignement.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orderAssignement = $this->orderAssignementRepository->find($id);

        if (empty($orderAssignement)) {
            Flash::error('Order Assignement not found');

            return redirect(route('orderAssignements.index'));
        }

        return view('order_assignements.edit')->with('orderAssignement', $orderAssignement);
    }

    /**
     * Update the specified OrderAssignement in storage.
     *
     * @param int $id
     * @param UpdateOrderAssignementRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderAssignementRequest $request)
    {
        $orderAssignement = $this->orderAssignementRepository->find($id);

        if (empty($orderAssignement)) {
            Flash::error('Order Assignement not found');

            return redirect(route('orderAssignements.index'));
        }

        $orderAssignement = $this->orderAssignementRepository->update($request->all(), $id);

        Flash::success('Order Assignement updated successfully.');

        return redirect(route('orderAssignements.index'));
    }

    /**
     * Remove the specified OrderAssignement from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orderAssignement = $this->orderAssignementRepository->find($id);

        if (empty($orderAssignement)) {
            Flash::error('Order Assignement not found');

            return redirect(route('orderAssignements.index'));
        }

        $this->orderAssignementRepository->delete($id);

        Flash::success('Order Assignement deleted successfully.');

        return redirect(route('orderAssignements.index'));
    }

    public function delivery(Request $request, $delivery_id){
        $orderAssignements = \App\Models\OrderAssignement::where(['deliverer_id' =>  $delivery_id])
            ->orWhereNotNull('acceptation_time')
            ->orWhere('acceptation_time', '!=', '')
            ->orWhereNotNull('rejection_time')
            ->orWhere('rejection_time', '!=', '')
            ->orderByDesc('id')
            ->paginate(10);

        return view('order_assignements.index')
            ->with('orderAssignements', $orderAssignements);
    }
}
