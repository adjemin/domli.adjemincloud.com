<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRestaurantUsersRequest;
use App\Http\Requests\UpdateRestaurantUsersRequest;
use App\Repositories\RestaurantUsersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class RestaurantUsersController extends AppBaseController
{
    /** @var  RestaurantUsersRepository */
    private $restaurantUsersRepository;

    public function __construct(RestaurantUsersRepository $restaurantUsersRepo)
    {
        $this->restaurantUsersRepository = $restaurantUsersRepo;
    }

    /**
     * Display a listing of the RestaurantUsers.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantUsers = $this->restaurantUsersRepository->all();

        return view('restaurant_users.index')
            ->with('restaurantUsers', $restaurantUsers);
    }

    /**
     * Show the form for creating a new RestaurantUsers.
     *
     * @return Response
     */
    public function create()
    {
        return view('restaurant_users.create');
    }

    /**
     * Store a newly created RestaurantUsers in storage.
     *
     * @param CreateRestaurantUsersRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantUsersRequest $request)
    {
        $input = $request->all();

        $restaurantUsers = $this->restaurantUsersRepository->create($input);

        Flash::success('Restaurant Users saved successfully.');

        return redirect(route('restaurantUsers.index'));
    }

    /**
     * Display the specified RestaurantUsers.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $restaurantUsers = $this->restaurantUsersRepository->find($id);

        if (empty($restaurantUsers)) {
            Flash::error('Restaurant Users not found');

            return redirect(route('restaurantUsers.index'));
        }

        return view('restaurant_users.show')->with('restaurantUsers', $restaurantUsers);
    }

    /**
     * Show the form for editing the specified RestaurantUsers.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $restaurantUsers = $this->restaurantUsersRepository->find($id);

        if (empty($restaurantUsers)) {
            Flash::error('Restaurant Users not found');

            return redirect(route('restaurantUsers.index'));
        }

        return view('restaurant_users.edit')->with('restaurantUsers', $restaurantUsers);
    }

    /**
     * Update the specified RestaurantUsers in storage.
     *
     * @param int $id
     * @param UpdateRestaurantUsersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantUsersRequest $request)
    {
        $restaurantUsers = $this->restaurantUsersRepository->find($id);

        if (empty($restaurantUsers)) {
            Flash::error('Restaurant Users not found');

            return redirect(route('restaurantUsers.index'));
        }

        $restaurantUsers = $this->restaurantUsersRepository->update($request->all(), $id);

        Flash::success('Restaurant Users updated successfully.');

        return redirect(route('restaurantUsers.index'));
    }

    /**
     * Remove the specified RestaurantUsers from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $restaurantUsers = $this->restaurantUsersRepository->find($id);

        if (empty($restaurantUsers)) {
            Flash::error('Restaurant Users not found');

            return redirect(route('restaurantUsers.index'));
        }

        $this->restaurantUsersRepository->delete($id);

        Flash::success('Restaurant Users deleted successfully.');

        return redirect(route('restaurantUsers.index'));
    }
}
