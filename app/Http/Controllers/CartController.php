<?php

namespace App\Http\Controllers;

use App\Models\Food;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('customer.pages.cart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $data = $request->all();

        $part_items_keys = [];
        $know_keys = ['_token' ,'food_id', 'total_price', 'order_qty', 'allergy_text', 'allergie_check'];
        
        foreach($request->all() as $key => $value){
            if(in_array($key, $know_keys)){
                continue;
            }else{
                array_push($part_items_keys, $key);
            }
        }



        // dd($data);
        /***
         * check if food come from different restaurant
         */
        $food = Food::where('uuid', $data['food_id'])->first();

        if(Cart::content()->count() > 1) {
            $prevFood = Food::where(['uuid' =>  Cart::content()->first()->id])->first();
            if(!is_null($prevFood) || !empty($prevFood)){
                if($prevFood->restaurant->uuid != $food->restaurant->id){
                    return redirect()->back()->with('error', "You can't buy foods from different restaurant");
                }
            }
        } 
        
        $part_items = collect($part_items_keys)->map(function($part_item) use($data){
            return $data[$part_item];
        })->map(function($item) use($food, $data){
            return [
                "$item[0]" => 1
            ];
        })->toArray();
        // dd($test);

        $restaurant = collect(Cart::content())->map(function($item){
            $food = Food::find($item->id);
            return $food->restaurant ?? null;
        })->reject(function($restaurant){
            return is_null($restaurant);
        })->unique();

        if($restaurant->count() == 1 || is_null($restaurant->first()) || $food->restaurant->id == $restaurant->first()->id){
            $duplicata = Cart::search(function ($cartItem, $rowId) use ($data) {
                return $cartItem->id == $data['food_id'];
            });

            if ($duplicata->isNotEmpty()) {
                // $message = 'error';
                // return $response = ['cartCount' => Cart::count(), 'message' => $message];
                return redirect()->back()->with('error', "You can't buy foods from different restaurant");
            }

            if($food->uuid == 0){
                $food->uuid = \App\Utils\UUID::v4();
                $food->save();
            }

            $item = Cart::add(
                $food->uuid,
                $food->title,
                intval($data['order_qty']),
                $data['order_qty'] > 1 ? floatval($data['total_price']) / intval($data['order_qty']) : $data['total_price'],
                [
                    'allergy'   =>  array_key_exists('allergie_check', $data) ? $data['allergy_text'] : '',
                    'parts' =>  $part_items_keys ?? [],
                    'part_items'    =>  $part_items ?? []
                ]
            );
        }else{
            // dd($restaurant);
            // $message ="You can't buy foods from different restaurant";
            // return $response = ['cartCount' => Cart::count(), 'message' => $message];
            return redirect()->back()->with('error', "You can't buy foods from different restaurant");
        }

        // $item = Cart::add($food->uuid, $food->title, 1, $food->price)->associate('App\Models\Food');
        // Cart::update($item->rowId, $data['OrderQty']);

        // $data  = $request->json()->all();

        // $duplicata = Cart::search(function ($cartItem, $rowId) use ($data) {
        //     return $cartItem->id == $data['food_id'];
        // });

        // if ($duplicata->isNotEmpty()) {
        //     $message = 'error';
        //     return $response = ['cartCount' => Cart::count(), 'message' => $message];
        // }

        // $food = Food::find($data['food_id']);

        // $item = Cart::add($food->id, $food->title, 1, $food->price)->associate('App\Models\Food');
        // Cart::update($item->rowId, $data['OrderQty']);

        // $message = 'success';
        // $response = ['cartCount' => Cart::count(), 'message' => $message];
        // return $response;
        if(\Auth::guard('customer')->check()){
            return redirect()->back()->with('success', 'The dish has been added to the basket');
        }else{
            return redirect('/customer/cart')->with('success', 'The dish has been added to the basket');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        Cart::destroy();
        return redirect(route('customerCart'));
    }

    public function distroyCart($uuid){
        $cartDetail = Cart::search(function ($cartItem, $rowId) use ($uuid) {
            return $cartItem->id == $uuid;
        })->first();

        if(is_null($cartDetail)){
            return redirect()->back()->withError("Item detail not found");
        }

        Cart::remove($cartDetail->rowId);

        return redirect()->back()->with('success', 'Element has removing Succesfully');
    }

    public function foodOrderDetail($uuid){
        $cartDetail = Cart::search(function ($cartItem, $rowId) use ($uuid) {
            return $cartItem->id == $uuid;
        })->first();

        if(is_null($cartDetail)){
            return redirect()->back()->withError("Item detail not found");
        }

        return view('deliverer.pages.offerDetail', compact('cartDetail'));
    }

    public function addDiscount(Request $request){
        // dd($request->all());
        //before add coupon generation

        // $condition = new \Gloudemans\Shoppingcart\CartCondition(array(
        //     'name' => 'VAT 12.5%',
        //     'type' => 'coupon',
        //     'target' => 'subtotal',
        //     'value' => '12.5%',
        //     'attributes' => array(
        //         'description' => 'Value added tax',
        //         'more_data' => 'more data here'
        //     )
        // ));

        $uuid = $request->uuid;

        if($uuid == ''){
            return redirect()->back()->with('error', 'Your cart is empty');
        }

        $coupons = \App\Models\Coupon::where('code', $request->coupon)->orWhere('titre', $request->coupon)->get();

        if(is_null($coupons) || empty($coupons)){
            return redirect()->back()->with('error', 'Coupon not found');
        }

        $coupon = collect($coupons)->filter(function($coupon) use($uuid){
            return \App\Models\Food::where('uuid', $uuid)->first()->restaurant->id == $coupon->restaurant_id;
        })->first();

        if(is_null($coupon)){
            return redirect()->back()->with('error', 'This coupon is available for another restaurant');
        }

        if($coupon->is_expired()){
            return redirect()->back()->with('error', 'Expired');
        }

        if(isset($coupon->nbr_person) || !is_null($coupon->nbr_person) || $coupon->nbr_person > 0){
            $coupon->nbr_person = $coupon->nbr_person - 1;
            $coupon->save();
        }

        $item_condition = Cart::getCondition($coupon->titre);

        if(!is_null($item_condition)){
            return redirect()->back()->with('error', 'This coupon is already used');
        }

        $condition = new \Gloudemans\Shoppingcart\CartCondition(array(
            'name' => $coupon->titre,
            'type' => 'coupon',
            'target' => 'subtotal',
            'value' => $coupon->pourcentage."%"
        ));

        Cart::condition($condition);

        return redirect()->back()->with('success', 'Coupon add successfully');
    }
}
