<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRestaurantPaymentRequest;
use App\Http\Requests\UpdateRestaurantPaymentRequest;
use App\Repositories\RestaurantPaymentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class RestaurantPaymentController extends AppBaseController
{
    /** @var  RestaurantPaymentRepository */
    private $restaurantPaymentRepository;

    public function __construct(RestaurantPaymentRepository $restaurantPaymentRepo)
    {
        $this->restaurantPaymentRepository = $restaurantPaymentRepo;
    }

    /**
     * Display a listing of the RestaurantPayment.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantPayments = $this->restaurantPaymentRepository->all();

        return view('restaurant_payments.index')
            ->with('restaurantPayments', $restaurantPayments);
    }

    /**
     * Show the form for creating a new RestaurantPayment.
     *
     * @return Response
     */
    public function create()
    {
        return view('restaurant_payments.create');
    }

    /**
     * Store a newly created RestaurantPayment in storage.
     *
     * @param CreateRestaurantPaymentRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantPaymentRequest $request)
    {
        $input = $request->all();

        $restaurantPayment = $this->restaurantPaymentRepository->create($input);

        Flash::success('Restaurant Payment saved successfully.');

        return redirect(route('restaurantPayments.index'));
    }

    /**
     * Display the specified RestaurantPayment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $restaurantPayment = $this->restaurantPaymentRepository->find($id);

        if (empty($restaurantPayment)) {
            Flash::error('Restaurant Payment not found');

            return redirect(route('restaurantPayments.index'));
        }

        return view('restaurant_payments.show')->with('restaurantPayment', $restaurantPayment);
    }

    /**
     * Show the form for editing the specified RestaurantPayment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $restaurantPayment = $this->restaurantPaymentRepository->find($id);

        if (empty($restaurantPayment)) {
            Flash::error('Restaurant Payment not found');

            return redirect(route('restaurantPayments.index'));
        }

        return view('restaurant_payments.edit')->with('restaurantPayment', $restaurantPayment);
    }

    /**
     * Update the specified RestaurantPayment in storage.
     *
     * @param int $id
     * @param UpdateRestaurantPaymentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantPaymentRequest $request)
    {
        $restaurantPayment = $this->restaurantPaymentRepository->find($id);

        if (empty($restaurantPayment)) {
            Flash::error('Restaurant Payment not found');

            return redirect(route('restaurantPayments.index'));
        }

        $restaurantPayment = $this->restaurantPaymentRepository->update($request->all(), $id);

        Flash::success('Restaurant Payment updated successfully.');

        return redirect(route('restaurantPayments.index'));
    }

    /**
     * Remove the specified RestaurantPayment from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $restaurantPayment = $this->restaurantPaymentRepository->find($id);

        if (empty($restaurantPayment)) {
            Flash::error('Restaurant Payment not found');

            return redirect(route('restaurantPayments.index'));
        }

        $this->restaurantPaymentRepository->delete($id);

        Flash::success('Restaurant Payment deleted successfully.');

        return redirect(route('restaurantPayments.index'));
    }
}
