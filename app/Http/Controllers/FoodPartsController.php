<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFoodPartsRequest;
use App\Http\Requests\UpdateFoodPartsRequest;
use App\Repositories\FoodPartsRepository;
use App\Models\FoodParts;
use App\Models\Food;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Auth;
use Response;

class FoodPartsController extends AppBaseController
{
    /** @var  FoodPartsRepository */
    private $foodPartsRepository;

    public function __construct(FoodPartsRepository $foodPartsRepo)
    {
        $this->foodPartsRepository = $foodPartsRepo;
    }

    /**
     * Display a listing of the FoodParts.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request, $restaurant_id, $id)
    {
        if(Auth::guard('restaurant')->check()){
            return redirect()->back();
        }

        $foodParts = $this->foodPartsRepository->all();

        return view('food_parts.index')
            ->with('foodParts', $foodParts);
    }


    public function food($restaurant_id, $food_id)
    {
        if($this->policy($restaurant_id)){
            return redirect()->back();
        }

        $food = Food::find($food_id);

        return view('food_parts.index')
            ->with('food', $food);
    }

    /**
     * Show the form for creating a new FoodParts.
     *
     * @return Response
     */
    public function create($restaurant_id, $food_id)
    {
        if($this->policy($restaurant_id)){
            return redirect()->back();
        }

        $food = Food::find($food_id);
        return view('food_parts.create', compact('food'));
        // return view('food_parts.create');
    }

    /**
     * Store a newly created FoodParts in storage.
     *
     * @param CreateFoodPartsRequest $request
     *
     * @return Response
     */
    public function store(CreateFoodPartsRequest $request)
    {
        $input = $request->all();

        $food = Food::find($input['food_id']);

        $foodParts = $this->foodPartsRepository->create($input);

        Flash::success('Food Parts saved successfully.');

        return redirect("/restaurant"."/".$food->restaurant->uuid."/".$foodParts->food_id."/food_part");
        // return redirect(route('foodParts.index'));
    }

    /**
     * Display the specified FoodParts.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($restaurant_id, $id)
    {
        $foodParts = $this->foodPartsRepository->find($id);

        if($this->policy($restaurant_id)){
            return redirect()->back();
        }

        if (empty($foodParts)) {
            Flash::error('Food Parts not found');

            return redirect()->back();
            // return redirect("food_part/$foodParts->food_id");
            // return redirect(route('foodParts.index'));
        }

        return view('food_parts.show')->with('foodParts', $foodParts);
    }

    /**
     * Show the form for editing the specified FoodParts.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($restaurant_id ,$id)
    {
        $foodParts = $this->foodPartsRepository->find($id);

        if($this->policy($restaurant_id)){
            return redirect()->back();
        }

        if (empty($foodParts)) {
            Flash::error('Food Parts not found');

            return redirect()->back();
            // return redirect("food_part/$foodParts->food_id");
            // return redirect(route('foodParts.index'));
        }

        $food = Food::find($foodParts->food_id);

        return view('food_parts.edit')
                ->with('food', $food)
                ->with('foodParts', $foodParts);
    }

    /**
     * Update the specified FoodParts in storage.
     *
     * @param int $id
     * @param UpdateFoodPartsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFoodPartsRequest $request)
    {
        $foodParts = $this->foodPartsRepository->find($id);

        if (empty($foodParts)) {
            Flash::error('Food Parts not found');

            return redirect()->back();
            // return redirect("food_part/$foodParts->food_id");
            // return redirect(route('foodParts.index'));
        }

        $food = Food::find($request->food_id);

        $foodParts = $this->foodPartsRepository->update($request->all(), $id);

        Flash::success('Food Parts updated successfully.');

        return redirect("/restaurant"."/".$food->restaurant->uuid."/".$foodParts->food_id."/food_part");
        // return redirect("/restaurant"."/".$food->restaurant->uuid."/".$foodParts->food_id."/food_part");
        // return redirect(route('foodParts.index'));
    }

    /**
     * Remove the specified FoodParts from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($restaurant_id, $id)
    {
        $foodParts = $this->foodPartsRepository->find($id);

        $food = Food::find($foodParts->food_id);

        if($this->policy($restaurant_id)){
            return redirect()->back();
        }

        if (empty($foodParts)) {
            Flash::error('Food Parts not found');

            return redirect()->back();
            // return redirect("food_part/$foodParts->food_id");
            // return redirect(route('foodParts.index'));
        }

        $this->foodPartsRepository->delete($id);

        Flash::success('Food Parts deleted successfully.');

        return redirect("/restaurant"."/".$food->restaurant->uuid."/".$foodParts->food_id."/food_part");
        // return redirect(route('foodParts.index'));
        // return redirect("food_part/$foodParts->food_id");
    }

    public function policy($restaurant_id){
        if(Auth::guard('restaurant')->check()){
            if($restaurant_id != Auth::guard('restaurant')->user()->uuid){
                Flash::error("Restaurant not found");
                return true;
            }
        }
        return false;
    }
}
