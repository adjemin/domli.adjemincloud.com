<?php

namespace App\Http\Controllers\CustomerAuth;

use OTPHP\TOTP;
// use App\Mail\AdminNotifyEmail;
// use App\Mail\CustomerWelcomeMail;
use App\VerifyCustomer;
// use App\Models\User;
use App\Mail\VerifyMail;
use App\Models\Customers;
use App\Models\Orders;
use App\Models\Restaurants;
// use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\Models\Otps;
use App\Models\RestaurantCategories;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Repositories\CustomersRepository;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
// use Gloudemans\Shoppingcart\Facades\Cart;

// use Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/customer/home';
    // protected $redirectToMerchant = '/merchant/home';

    /** @var  CustomersRepository */
    private $customersRepository;

    public function __construct(CustomersRepository $customersRepo)
    {
        // $this->middleware(['auth','verified']);
        $this->customersRepository = $customersRepo;
    }
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('customer.guest');
    // }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [

            /* 'name' => 'required|max:255', */
            'email' => 'required|email|max:255|unique:customers',
            'password' => 'required|min:8|confirmed',
            'lastname' => 'required|max:255',
            'name' => 'required|max:255',
            // 'phone_number' => 'required|phone:AUTO,CI|max:255'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Customers
     */
    // protected function create(array $data)
    // {
    //     dd($data);
    //     $customer = new Customers();

    //     /* $phoneNumber = $data['phone_number'];
    //     $countryCode = $data['country_codePhoneNumber'];
    //     $phoneNumberUtil = PhoneNumberUtil::getInstance();
    //     $geoCoder = PhoneNumberOfflineGeocoder::getInstance();
    //     $carrierMapper = PhoneNumberToCarrierMapper::getInstance();
    //     try {
    //     $phoneNumberObject = $phoneNumberUtil->parse($phoneNumber, $countryCode);
    //     $region = $phoneNumberUtil->getRegionCodeForNumber($phoneNumberObject);
    //     $description = $geoCoder->getDescriptionForNumber($phoneNumberObject, 'fr');
    //     $operator = $carrierMapper->getNameForNumber($phoneNumberObject, 'fr');
    //     } catch (\Exception $e) {
    //     $region = '';
    //     $description = '';
    //     $operator = '';
    //     } */

    //     $name = ucfirst(strtolower($data['name']));
    //     $email = ucfirst(strtolower($data['email']));
    //     // $dial_code = $phoneNumberObject->getCountryCode();
    //     // $dial_code = $data['dial_codePhoneNumber'];
    //     // $phone_number = $data['phone_number'];
    //     $customer->name = $name;
    //     $customer->email = $email;
    //     // $customer->name = $last_name . " " . $first_name;
    //     // $customer->phone_number = $phone_number;
    //     // $customer->dial_code = $dial_code;
    //     // $customer->country_code = $region;
    //     // $customer->phone = $dial_code . $phone_number;
    //     // $customer->email = $data['email'];
    //     $customer->password = bcrypt($data['password']);

    //     $customer->save();

    //     /* return Customer::create([
    //     'last_name' => $data['last_name'],
    //     'first_name' => $data['first_name'],
    //     'name' => $data['last_name'] . " " . $data['first_name'],
    //     'email' => $data['email'],
    //     'password' => bcrypt($data['password']),
    //     'phone_number' => $data['phone_number'],
    //     ]); */

    //     /* if ($data['phone_number'] != null) {
    //     PhoneNumberAccount::create(
    //     [
    //     'dial_code' => $dial_code,
    //     'phone_number' => $customer->phone_number,
    //     'phone' => $customer->phone,
    //     'balance' => 0,
    //     'country_code' => $region,
    //     'location_phone' => $description,
    //     'operator_phone' => $operator,
    //     'customer_id' => $customer->id,
    //     ]
    //     );
    //     } */

    //     // Notify Customer
    //     $token = csrf_token();
    //     // $actionUrl = env('APP_URL') . "/customer/confirm/email?customer_email=" . $customer->email . "&token=" . $token . "&csrf-token=" . $token;
    //     //Mail::to($customer->email)->send(new CustomerWelcomeMail($customer, $actionUrl));

    //     // Notfy Admin
    //     // $accountUrl = env('APP_URL') . "/customers/$customer->id";
    //     // foreach (User::all() as $user) {
    //     //     Mail::to($user->email)->send(new AdminNotifyEmail($accountUrl));
    //     // }
    //     dd($customer);
    //     return $customer;
    //     // return redirect('customer/login');
    //     // return redirect(url('customer/login'));
    // }


    public function storeCustomerAccount(Request $request)
    {
        $this->validator($request->all())->validate();
        // dd($request->all());

        $field = $request->all();
        $userIp = $request->ip();
        $basicIp = '134.201.250.155';
        // $userCoordinates = file_get_contents('http://api.ipstack.com/'.$request->ip().'?access_key=7e79beb0acc11d71293ba82eace28363');
        $userCoordinates = file_get_contents('http://api.ipstack.com/'.$userIp.'?access_key=7e79beb0acc11d71293ba82eace28363');
        $userCoordinates = json_decode($userCoordinates, true);

        // dd($userCoordinates['location']['languages'][0]['name']);
        // dd($userCoordinates['lat']);

        $field['name'] = $request->name;
        $field['lastname'] = $request->lastname;
        $field["dial_code"] = $request->dial_code;
        $field["phone_number"] = $request->phone_number;
        $field["country_code"] = $request->country_code;
        $field["email"] = $request->email;
        $field['password'] = bcrypt($request->password);
        $field['photo_profile'] = 'https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png';
        $field['region_name'] = $userCoordinates['region_name'];
        $field["city"] = $userCoordinates['city'];
        $field["lat"] = $userCoordinates['latitude'];
        $field["lng"] = $userCoordinates['longitude'];
        $field["language"] = $userCoordinates['location']['languages'][0]['name'] ?? '';
        $field['is_active'] = true;
        // $field['activation_date'] = Now();

        $customer = $this->customersRepository->create($field);

        $verifyCustomer = VerifyCustomer::create([
            'customer_id' => $customer->id,
            'token' => sha1(time())
        ]);
        Mail::to($customer)->send(new VerifyMail($customer));
        // dd($verifyCustomer->token);
        // dd('ok');
        // $this->getOTPByPhone($request);
        // return view('customer.pages.otp', compact('customer'));
        // return redirect($this->redirectTo);
        // Auth::guard('customer')->check();
        // dd($verifyCustomer);
        if($customer->verified){
            Auth::guard('customer')->loginUsingId($customer->id);
            return redirect('customer/login');
        }

        return view('customer.pages.waitpage', compact('customer'));
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('customer.auth.login');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('customer');
    }

    // public function confirmEmail(Request $request)
    // {

    //     $customer_email = $request->get('customer_email');
    //     // dd($customer_email);
    //     $customer = Customers::where(['email' => $customer_email])->first();

    //     if (empty($customer)) {
    //         return view('errors.404');
    //     }

    //     $customer->is_active = true;
    //     $customer->email_verified_at = now();
    //     $customer->activation_date = now();

    //     $customer->save();

    //     return redirect()->route('customerLogin');

    // }

    public function showHomePage(){
        $restaurants = Restaurants::all()->take(6);
        $restaurant_categories = RestaurantCategories::all()->take(6);

        if(auth()->guard('customer')->check()){
            $order = Orders::latest()->where([
                'customer_id'   =>  auth()->guard('customer')->user()->id,
            ])->whereNotIn('current_status', [Orders::CUSTOMER_DELIVERED, Orders::REFUSED, Orders::CANCELED, Orders::FAILED])
            ->first();

            return view('customer.homePage.index', compact('restaurants', 'restaurant_categories', 'order'));
        }
        // dd($order);

        return view('customer.homePage.index', compact('restaurants', 'restaurant_categories'));
    }

    public function verifycustomer($token) {

        $verifycustomer = VerifyCustomer::where('token', $token)->first();

        if(isset($verifycustomer) ){
            $customer = $verifycustomer->customers;
            // dd($customer);
            if(!$customer->verified) {
                $customer->verified = 1;
                $customer->activation_date = now();
                $customer->save();
                // $status = "Your e-mail is verified. You can now login.";
            }
        } else {
            return redirect('/customer/login')->with('warning', "Sorry your email cannot be identified.");
        }
        return redirect(route('congratulate'));
    }

    public function authenticated(Request $request, $customer){
        if (!$customer->verified) {
            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        return redirect()->intended($this->redirectPath());
    }

    protected function registered(Request $request, $customer){
        $this->guard()->logout();
        return redirect('/customer/login')->with('status', 'We sent you an activation code. Check your email and click on the link to verify.');
    }

    public function getOTPByPhone(Request $request)
    {
        $dial_code = $request->dial_code;
        $phone_number = $request->phone_number;

        $lang = 'FR';

        if (!$request->has('dial_code')) {
            return response('Dial Code not found');
        }

        if (!$request->has('phone_number')) {
            return response('Phone number not found');
        }

        // if (!$request->has('lang')) {
        //     return response('lang not found');
        // }

        $totp = TOTP::create(
            null, // Let the secret be defined by the class
            10, // The period (10 seconds)
            'sha1', // The digest algorithm
            6// The output will generate 6 digits
        );

        $otp_code = $totp->now();

        //SEND SMS
        $client_phone = $dial_code . "" . $phone_number;
        // if ($lang == 'FR') {
        $message = "Vous devez entrer ce code dans Jobbing: " . $otp_code;
        // } else {
        //     $message  = "You must enter this code in E-Mairie: " . $otp_code;
        // }
        $otpModel = Otp::where(['code' => $otp_code])->first();
        if (empty($otpModel)) {
            $otpModel = new Otp();
            $otpModel->code = $otp_code;
            $otpModel->dial_code = $dial_code;
            $otpModel->phone_number = $phone_number;
            $deadline = Carbon::now()->addMinutes(60)->toDateTimeString();
            $otpModel->deadline = $deadline;
            $otpModel->is_used = false;
            $otpModel->save();
        }

        $otpModel->dial_code = $dial_code;
        $otpModel->phone_number = $phone_number;
        $deadline = Carbon::now()->addMinutes(60)->toDateTimeString();
        $otpModel->deadline = $deadline;
        $otpModel->is_used = false;
        $otpModel->save();

        // dd($otpModel);
        $this->sendSMS($client_phone, $message);

        // if ($result) {
        //     return response($otpModel->toArray(), 'OTP sent');
        // } else {
        //     return response('Error found');
        // }
    }
}
