<?php
namespace App\Http\Controllers\CustomerAuth;

use App\VerifyCustomer;
use App\Mail\VerifyMail;
use App\Models\Customer;
use App\Models\Customers;
use Illuminate\Http\Request;
// use Hesto\MultiAuth\Traits\LogsoutGuard;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Providers\RouteServiceProvider;
use Hesto\MultiAuth\Traits\LogsoutGuard;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers, LogsoutGuard {
    //     LogsoutGuard::logout insteadof AuthenticatesUsers;
    // }
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect()->to('/customer/login');
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/customer/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    function __construct()
    {
        $this->middleware('customer.guest', ['except' => 'logout']);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('customer.auth.login');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('customer');
    }

    protected function login(Request $request){
        $message = '';
        $credentials = $request->only('email', 'password');


        if (Auth::guard('customer')->attempt($credentials)) {
            // Authentication passed...
            $message = 'success';
            $user = Auth::guard('customer')->user();

            // Verified user email
            if(!$user->verified){
                $verifyCustomer = VerifyCustomer::create([
                    'customer_id' => $user->id,
                    'token' => sha1(time())
                ]);
                Mail::to($user)->send(new VerifyMail($user));
                $message = 'verified';

                auth()->guard('customer')->logout();
                $request->session()->flush();
                $request->session()->regenerate();

                return $message;
            }
            
            $userCoordinates = $request->session()->get('user_ip_address');

            $user->region_name = $userCoordinates['region_name'] ?? '';
            $user->city = $userCoordinates['city'] ?? '';
            $user->lat = $userCoordinates['latitude'];
            $user->lng = $userCoordinates['longitude'];
            if (isset($userCoordinates['location']['languages'][0]['name']) && !empty($userCoordinates['location']['languages'][0]['name'])) {
                $user->language = $userCoordinates['location']['languages'][0]['name'];
            }
            $user->save();

            return $message;
            // return redirect()->intended('/customer/home');
        }
        else {
            $message = 'error';
            // return redirect()->back()->with('error', "An error occurred, please check your information and try again.");
        }
        return $message;
    }

    function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    function handleProviderCallback($provider)
    {
        try {
            if ($provider == "google") {
                $user = Socialite::driver($provider)->user();
            } else {
                $user = Socialite::driver($provider)->fields([
                    'first_name', 'last_name', 'email', 'picture', 'short_name', 'name',
                ])->user();
            }
        } catch (\Exception $e) {
            return redirect('auth/' . $provider);
        }

        // dd($user);
        if ($user->email) {
            if ($provider == "facebook") {
                $authUser = Customer::where('facebook_id', $user->id)->orWhere('email', $user->email)->first();
                if (!is_null($authUser)) {
                    if (is_null($authUser->email_verifed_at) || $authUser->is_active == 0 || $authUser->verified == 0) {
                        $authUser->email_verifed_at = now();
                        $authUser->activation_date = now();
                        $authUser->is_active = 1;
                        $authUser->verified = 1;
                        $authUser->save();
                    }
                    //return $authUser;
                    // $aut = Auth::login($authUser, true);
                    Auth::guard('customer')->login($authUser);
                    return redirect($this->redirectTo);
                } else {
                    $new_user = new Customers();
                    // $new_user->name = $user->name;
                    $new_user->name = $user->user['first_name'];
                    $new_user->lastname = $user->user['last_name'];
                    $new_user->email = $user->email;
                    $new_user->facebook_id = $user->id;
                    $new_user->email_verifed_at = now();
                    $new_user->activation_date = now();
                    $new_user->is_active = 1;
                    $new_user->verified = 1;
                    $new_user->photo_profile = $user->avatar ?? $user->photo ?? $user->photo_profile ?? 'https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png';
                    $new_user->save();
                    return redirect($this->redirectTo);
                }
            } else if ($provider == "google") {
                //dd($user);
                $authUser = Customers::where('google_id', $user->id)->orWhere('email', $user->email)->first();

                if (!is_null($authUser)) {
                    if (is_null($authUser->email_verifed_at) || $authUser->is_active == 0 || $authUser->verified == 0) {
                        $authUser->email_verified_at = now();
                        $authUser->activation_date = now();
                        $authUser->is_active = 1;
                        $authUser->verified = 1;
                        $authUser->save();
                    }
                    Auth::guard('customer')->login($authUser);
                    // return redirect($this->redirectTo);
                    return redirect('/');
                } else {
                    $new_user = new Customers();
                    // $new_user->name = $user->name;
                    $new_user->name = $user->user['given_name'];
                    $new_user->lastname = $user->user['family_name'];
                    $new_user->email = $user->email;
                    $new_user->google_id = $user->id;
                    $authUser->email_verified_at = now();
                    $authUser->activation_date = now();
                    $authUser->is_active = 1;
                    $authUser->verified = 1;
                    $new_user->photo = $user->avatar ?? 'https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png';
                    $new_user->save();
                    // return redirect($this->redirectTo);
                    // return redirect(route('customer.home'));
                    return redirect('/');
                }
            }

        } else {
            return Redirect::route('customer.login');
        }
    }


}
