<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRestaurantPaymentHistoriqueRequest;
use App\Http\Requests\UpdateRestaurantPaymentHistoriqueRequest;
use App\Repositories\RestaurantPaymentHistoriqueRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Auth;
use Response;

class RestaurantPaymentHistoriqueController extends AppBaseController
{
    /** @var  RestaurantPaymentHistoriqueRepository */
    private $restaurantPaymentHistoriqueRepository;

    public function __construct(RestaurantPaymentHistoriqueRepository $restaurantPaymentHistoriqueRepo)
    {
        $this->restaurantPaymentHistoriqueRepository = $restaurantPaymentHistoriqueRepo;
    }

    /**
     * Display a listing of the RestaurantPaymentHistorique.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if(Auth::guard('restaurant')->check()){
            $restaurantPaymentHistoriques = \App\Models\RestaurantPaymentHistorique::where('restaurant_id', Auth::guard('restaurant')->user()->id)->orderByDesc('id')->get();
        }else if(Auth::check()){
            $restaurantPaymentHistoriques = $this->restaurantPaymentHistoriqueRepository->all();
        }else{
            Flash::error('Not allowed');
            return redirect()->back();
        }
        

        return view('restaurant_payment_historiques.index')
            ->with('restaurantPaymentHistoriques', $restaurantPaymentHistoriques);
    }

    /**
     * Show the form for creating a new RestaurantPaymentHistorique.
     *
     * @return Response
     */
    public function create()
    {
        return view('restaurant_payment_historiques.create');
    }

    /**
     * Store a newly created RestaurantPaymentHistorique in storage.
     *
     * @param CreateRestaurantPaymentHistoriqueRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantPaymentHistoriqueRequest $request)
    {
        $input = $request->all();

        $restaurant = \App\Models\RestaurantCompte::where('restaurant_id', $request->restaurant_id)->first();

        if(is_null($restaurant) || empty($restaurant)){
            Flash::error("Restaurant not found");
            return redirect()->back();
        }

        if($request->amount > $restaurant->compte){
            Flash::error("Insuffisant balance");
            return redirect()->back();
        }

        $requestPayment = \App\Models\RequestRestaurantPayment::where(['amount' => $request->amount, 'is_paid' => false])->orWhere(['id' => $request->request_id])->first();

        if(empty($requestPayment) || is_null($requestPayment)){
            Flash::error("Request not found");
            return redirect()->back();
        }

        $requestPayment->is_paid = true;
        $requestPayment->save();

        $restaurant->compte = floatval($restaurant->compte) - floatval($request->amount); 
        $restaurant->save(); 

        $restaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepository->create($input);

        Flash::success('Restaurant Payment Historique saved successfully.');

        return redirect(route('restaurantPaymentHistoriques.index'));
    }

    /**
     * Display the specified RestaurantPaymentHistorique.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $restaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepository->find($id);

        if (empty($restaurantPaymentHistorique)) {
            Flash::error('Restaurant Payment Historique not found');

            return redirect()->back();
        }

        return view('restaurant_payment_historiques.show')->with('restaurantPaymentHistorique', $restaurantPaymentHistorique);
    }

    public function restaurant($restaurant_id)
    {
        // $restaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepository->find($id);
        $restaurantPaymentHistoriques = \App\Models\RestaurantPaymentHistorique::where('restaurant_id', $restaurant_id)->get();

        if (empty($restaurantPaymentHistoriques)) {
            Flash::error('Restaurant Payment Historique not found');

            return redirect()->back();
        }

        return view('restaurant_payment_historiques.index')->with('restaurantPaymentHistoriques', $restaurantPaymentHistoriques);
    }

    /**
     * Show the form for editing the specified RestaurantPaymentHistorique.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $restaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepository->find($id);

        if (empty($restaurantPaymentHistorique)) {
            Flash::error('Restaurant Payment Historique not found');

            return redirect(route('restaurantPaymentHistoriques.index'));
        }

        return view('restaurant_payment_historiques.edit')->with('restaurantPaymentHistorique', $restaurantPaymentHistorique);
    }

    /**
     * Update the specified RestaurantPaymentHistorique in storage.
     *
     * @param int $id
     * @param UpdateRestaurantPaymentHistoriqueRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantPaymentHistoriqueRequest $request)
    {
        $restaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepository->find($id);

        if (empty($restaurantPaymentHistorique)) {
            Flash::error('Restaurant Payment Historique not found');

            return redirect(route('restaurantPaymentHistoriques.index'));
        }

        $restaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepository->update($request->all(), $id);

        Flash::success('Restaurant Payment Historique updated successfully.');

        return redirect(route('restaurantPaymentHistoriques.index'));
    }

    /**
     * Remove the specified RestaurantPaymentHistorique from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $restaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepository->find($id);

        if (empty($restaurantPaymentHistorique)) {
            Flash::error('Restaurant Payment Historique not found');

            return redirect(route('restaurantPaymentHistoriques.index'));
        }

        $this->restaurantPaymentHistoriqueRepository->delete($id);

        Flash::success('Restaurant Payment Historique deleted successfully.');

        return redirect(route('restaurantPaymentHistoriques.index'));
    }
}
