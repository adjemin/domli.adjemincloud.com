<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAgentConnexionRequest;
use App\Http\Requests\UpdateAgentConnexionRequest;
use App\Repositories\AgentConnexionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class AgentConnexionController extends AppBaseController
{
    /** @var  AgentConnexionRepository */
    private $agentConnexionRepository;

    public function __construct(AgentConnexionRepository $agentConnexionRepo)
    {
        $this->agentConnexionRepository = $agentConnexionRepo;
    }

    /**
     * Display a listing of the AgentConnexion.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $agentConnexions = $this->agentConnexionRepository->all();

        return view('agent_connexions.index')
            ->with('agentConnexions', $agentConnexions);
    }

    /**
     * Show the form for creating a new AgentConnexion.
     *
     * @return Response
     */
    public function create()
    {
        return view('agent_connexions.create');
    }

    /**
     * Store a newly created AgentConnexion in storage.
     *
     * @param CreateAgentConnexionRequest $request
     *
     * @return Response
     */
    public function store(CreateAgentConnexionRequest $request)
    {
        $input = $request->all();

        $agentConnexion = $this->agentConnexionRepository->create($input);

        Flash::success('Agent Connexion saved successfully.');

        return redirect(route('agentConnexions.index'));
    }

    /**
     * Display the specified AgentConnexion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $agentConnexion = $this->agentConnexionRepository->find($id);

        if (empty($agentConnexion)) {
            Flash::error('Agent Connexion not found');

            return redirect(route('agentConnexions.index'));
        }

        return view('agent_connexions.show')->with('agentConnexion', $agentConnexion);
    }

    /**
     * Show the form for editing the specified AgentConnexion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $agentConnexion = $this->agentConnexionRepository->find($id);

        if (empty($agentConnexion)) {
            Flash::error('Agent Connexion not found');

            return redirect(route('agentConnexions.index'));
        }

        return view('agent_connexions.edit')->with('agentConnexion', $agentConnexion);
    }

    /**
     * Update the specified AgentConnexion in storage.
     *
     * @param int $id
     * @param UpdateAgentConnexionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAgentConnexionRequest $request)
    {
        $agentConnexion = $this->agentConnexionRepository->find($id);

        if (empty($agentConnexion)) {
            Flash::error('Agent Connexion not found');

            return redirect(route('agentConnexions.index'));
        }

        $agentConnexion = $this->agentConnexionRepository->update($request->all(), $id);

        Flash::success('Agent Connexion updated successfully.');

        return redirect(route('agentConnexions.index'));
    }

    /**
     * Remove the specified AgentConnexion from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $agentConnexion = $this->agentConnexionRepository->find($id);

        if (empty($agentConnexion)) {
            Flash::error('Agent Connexion not found');

            return redirect()->back();
            // return redirect(route('agentConnexions.index'));
        }

        $this->agentConnexionRepository->delete($id);

        Flash::success('Agent Connexion deleted successfully.');


        return redirect()->back();
        // return redirect(route('agentConnexions.index'));
    }
}
