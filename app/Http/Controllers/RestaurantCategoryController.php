<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRestaurantCategoryRequest;
use App\Http\Requests\UpdateRestaurantCategoryRequest;
use App\Repositories\RestaurantCategoryRepository;
use App\Models\Restaurants;
use App\Models\Category;
use App\Models\RestaurantCategories;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Auth;
use Response;

class RestaurantCategoryController extends AppBaseController
{
    /** @var  RestaurantCategoryRepository */
    private $restaurantCategoryRepository;

    public function __construct(RestaurantCategoryRepository $restaurantCategoryRepo)
    {
        $this->restaurantCategoryRepository = $restaurantCategoryRepo;
    }

    /**
     * Display a listing of the RestaurantCategory.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantCategories = $this->restaurantCategoryRepository->all();

        if(Auth::guard('restaurant')->check()){
            return redirect('/backOffice/restaurants/'.Auth::guard('restaurant')->user()->uuid);
        }

        return view('restaurant_categories.index')
            ->with('restaurantCategories', $restaurantCategories);
    }

    public function categorie(Request $request, $restaurant_id)
    {
        $restaurant = Restaurants::where(['id' => $restaurant_id])->orWhere(['uuid' => $restaurant_id])->first();

        if(Auth::guard('restaurant')->check()){
            if($restaurant->id != Auth::guard('restaurant')->user()->id){
                Flash::error("Restaurant not found");
                return redirect()->back();
            }
        }

        if(!$restaurant){
            Flash::error('Restaurant not found');
            return redirect()->back();
        }

        $categories = RestaurantCategories::where(['restaurant_id' => $restaurant_id])->get();
        
        return view('restaurants.categorie_create',compact('restaurant', 'categories'));
    }

    /**
     * Show the form for creating a new RestaurantCategory.
     *
     * @return Response
     */
    public function create($restaurant_id)
    {   
        $restaurant = Restaurants::where(['id' => $restaurant_id])->orWhere(['uuid' => $restaurant_id])->first();
        
        if(is_null($restaurant)){
            Flash::error('Restaurant not found');
            return redirect()->back();
        }

        $categories = Category::all();
        
        return view('restaurants.resto_category_create', compact('restaurant', 'categories'));
    }

    /**
     * Store a newly created RestaurantCategory in storage.
     *
     * @param CreateRestaurantCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantCategoryRequest $request)
    {
        $input = [
            'category_id' => $request->category_id,
            'restaurant_id' => $request->restaurant_id,
            'category_slug' => Category::find($request->category_id)->slug
        ];
        // dd($input);

        $restaurantCategory = $this->restaurantCategoryRepository->create($input);

        Flash::success('Restaurant Category saved successfully.');

        return redirect("/category/restaurant/$request->restaurant_id");
    }

    /**
     * Display the specified RestaurantCategory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $restaurantCategory = $this->restaurantCategoryRepository->find($id);

        // if(Auth::guard('restaurant')->check()){
        //     if($restaurantCategory->restaurant_id != Auth::guard('restaurant')->user()->id){
        //         Flash::error("Restaurant not found");
        //         return redirect()->back();
        //     }
        // }

        if (empty($restaurantCategory)) {
            Flash::error('Restaurant Category not found');

            return redirect()->back();
        }

        return view('restaurant_categories.show')->with('restaurantCategory', $restaurantCategory);
    }

    /**
     * Show the form for editing the specified RestaurantCategory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $restaurantCategory = $this->restaurantCategoryRepository->find($id);

        // if(Auth::guard('restaurant')->check()){
        //     if($restaurantCategory->restaurant_id != Auth::guard('restaurant')->user()->id){
        //         Flash::error("Restaurant not found");
        //         return redirect()->back();
        //     }
        // }

        if (empty($restaurantCategory)) {
            Flash::error('Restaurant Category not found');

            return redirect()->back();
        }

        $categories = Category::all();

        return view('restaurant_categories.edit')
            ->with('restaurantCategory', $restaurantCategory)
            ->with('categories', $categories)
            ->with('restaurant', $restaurantCategory->restaurant);
    }

    /**
     * Update the specified RestaurantCategory in storage.
     *
     * @param int $id
     * @param UpdateRestaurantCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantCategoryRequest $request)
    {
        $restaurantCategory = $this->restaurantCategoryRepository->find($id);

        // if(Auth::guard('restaurant')->check()){
        //     if($restaurantCategory->restaurant_id != Auth::guard('restaurant')->user()->id){
        //         Flash::error("Restaurant not found");
        //         return redirect()->back();
        //     }
        // }

        if (empty($restaurantCategory)) {
            Flash::error('Restaurant Category not found');

            return redirect()->back();
        }

        $input = [
            'category_id' => $request->category_id,
            'restaurant_id' => $request->restaurant_id,
            'category_slug' => Category::find($request->category_id)->slug
        ];

        $restaurantCategory = $this->restaurantCategoryRepository->update($input, $id);

        Flash::success('Restaurant Category updated successfully.');

        return redirect("/category/restaurant/$request->restaurant_id");
    }

    /**
     * Remove the specified RestaurantCategory from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $restaurantCategory = $this->restaurantCategoryRepository->find($id);

        // if(Auth::guard('restaurant')->check()){
        //     if($restaurantCategory->restaurant_id != Auth::guard('restaurant')->user()->id){
        //         Flash::error("Restaurant not found");
        //         return redirect()->back();
        //     }
        // }

        if (empty($restaurantCategory)) {
            Flash::error('Restaurant Category not found');

            return redirect(route('restaurantCategories.index'));
        }

        $this->restaurantCategoryRepository->delete($id);

        Flash::success('Restaurant Category deleted successfully.');

        return redirect()->back();
    }
}
