<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FoodEspaceController extends Controller
{
    public function index()
    {
        return view('pages.food_espace');
    }
}
