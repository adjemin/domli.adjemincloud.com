<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRestaurantMenusRequest;
use App\Http\Requests\UpdateRestaurantMenusRequest;
use App\Repositories\RestaurantMenusRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Restaurants;
use Illuminate\Support\Str;
use Flash;
use Auth;
use Response;

class RestaurantMenusController extends AppBaseController
{
    /** @var  RestaurantMenusRepository */
    private $restaurantMenusRepository;

    public function __construct(RestaurantMenusRepository $restaurantMenusRepo)
    {
        // return $this->middleware(['auth', 'restaurant']);
        $this->restaurantMenusRepository = $restaurantMenusRepo;
    }

    /**
     * Display a listing of the RestaurantMenus.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if(Auth::guard('restaurant')->check()){
            return redirect('/backOffice/restaurants/'.Auth::guard('restaurant')->user()->uuid);
        }

        $restaurantMenuses = $this->restaurantMenusRepository->all();

        return view('restaurant_menuses.index')
            ->with('restaurantMenuses', $restaurantMenuses);
    }

    public function menusByRestaurant(Request $request, $restaurant_id)
    {
        $restaurant = Restaurants::where(['id' => $restaurant_id])->orWhere(['uuid' => $restaurant_id])->first();

        if(is_null($restaurant) || empty($restaurant)){
            Flash::error("Restaurant not found");
            return redirect()->back();
        }

        return view('restaurants.menu')
            ->with('restaurant', $restaurant);
    }

    /**
     * Show the form for creating a new RestaurantMenus.
     *
     * @return Response
     */
    public function create($restaurant_id)
    {
        $restaurant = Restaurants::where(['id' => $restaurant_id])->orWhere(['uuid' => $restaurant_id])->first();

        if (empty($restaurant)) {
            Flash::error('Restaurant not found');

            return redirect()->back();
            // return redirect(route('restaurantMenuses.index'));
        }

        return view('restaurants.menu_create')->with('restaurant', $restaurant);
        // return view('restaurant_menuses.create');
    }

    /**
     * Store a newly created RestaurantMenus in storage.
     *
     * @param CreateRestaurantMenusRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantMenusRequest $request)
    {
        // $input = $request->all();

        // $restaurantMenus = $this->restaurantMenusRepository->create($input);

        $input = [
            'title' => $request->title,
            'restaurant_id' => $request->restaurant_id,
            'slug'  =>  Str::of($request->title)->slug('-')
        ];


        $restaurantMenu = $this->restaurantMenusRepository->create($input);


        Flash::success('Restaurant Menus saved successfully.');

        $route = \Auth::check() ? "admin/restaurant/".$restaurantMenu->restaurant->uuid."/menu" : "restaurant/".$restaurantMenu->restaurant->uuid."/menu";
        return redirect($route);
        // return redirect("/restaurant/$request->restaurant_id");
        // return redirect(route('restaurantMenuses.index'));
    }

    /**
     * Display the specified RestaurantMenus.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $restaurantMenus = $this->restaurantMenusRepository->find($id);

        // if(Auth::guard('restaurant')->check()){
        //     if($restaurantMenus->restaurant_id != Auth::guard('restaurant')->user()->uuid){
        //         Flash::error("Restaurant not found");
        //         return redirect()->back();
        //     }
        // }

        if (empty($restaurantMenus)) {
            Flash::error('Restaurant Menus not found');

            return redirect()->back();
            // return redirect(route('restaurantMenuses.index'));
        }

        return view('restaurant_menuses.show')->with('restaurantMenus', $restaurantMenus);
    }

    /**
     * Show the form for editing the specified RestaurantMenus.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $restaurantMenus = $this->restaurantMenusRepository->find($id);

        // if(Auth::guard('restaurant')->check()){
        //     if($restaurantMenus->restaurant_id != Auth::guard('restaurant')->user()->id){
        //         Flash::error("Restaurant not found");
        //         return redirect()->back();
        //     }
        // }

        if (empty($restaurantMenus)) {
            Flash::error('Restaurant Menus not found');

            return redirect()->back();
            // return redirect(route('restaurantMenuses.index'));
        }

        return view('restaurant_menuses.edit')->with('restaurantMenus', $restaurantMenus);
    }

    /**
     * Update the specified RestaurantMenus in storage.
     *
     * @param int $id
     * @param UpdateRestaurantMenusRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantMenusRequest $request)
    {
        $restaurantMenus = $this->restaurantMenusRepository->find($id);

        // if(Auth::guard('restaurant')->check()){
        //     if($restaurantMenus->restaurant_id != Auth::guard('restaurant')->user()->id){
        //         Flash::error("Restaurant not found");
        //         return redirect()->back();
        //     }
        // }

        if (empty($restaurantMenus)) {
            Flash::error('Restaurant Menus not found');

            return redirect()->back();
            // return redirect(route('restaurantMenuses.index'));
        }

        $input = [
            'title' => $request->title,
            'restaurant_id' => $request->restaurant_id,
            'slug'  =>  Str::of($request->title)->slug('-')
        ];

        $restaurantMenu = $this->restaurantMenusRepository->update($input, $id);
        // $restaurantMenus = $this->restaurantMenusRepository->update($request->all(), $id);

        Flash::success('Restaurant Menus updated successfully.');

        $route = \Auth::check() ? "admin/restaurant/".$restaurantMenu->restaurant->uuid."/menu" : "restaurant/".$restaurantMenu->restaurant->uuid."/menu";
        return redirect($route);
        // return redirect(route('restaurantMenuses.index'));
    }

    /**
     * Remove the specified RestaurantMenus from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $restaurantMenus = $this->restaurantMenusRepository->find($id);

        // if(Auth::guard('restaurant')->check()){
        //     if($restaurantMenus->restaurant_id != Auth::guard('restaurant')->user()->id){
        //         Flash::error("Restaurant not found");
        //         return redirect()->back();
        //     }
        // }

        if (empty($restaurantMenus)) {
            Flash::error('Restaurant Menu not found');

            return redirect()->back();
            // return redirect(route('restaurantMenuses.index'));
        }

        $this->restaurantMenusRepository->delete($id);

        Flash::success('Restaurant Menu deleted successfully.');

        return redirect()->back();
        // return redirect(route('restaurantMenuses.index'));
    }
}
