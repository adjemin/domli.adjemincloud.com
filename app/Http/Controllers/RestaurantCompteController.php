<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRestaurantCompteRequest;
use App\Http\Requests\UpdateRestaurantCompteRequest;
use App\Repositories\RestaurantCompteRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class RestaurantCompteController extends AppBaseController
{
    /** @var  RestaurantCompteRepository */
    private $restaurantCompteRepository;

    public function __construct(RestaurantCompteRepository $restaurantCompteRepo)
    {
        $this->restaurantCompteRepository = $restaurantCompteRepo;
    }

    /**
     * Display a listing of the RestaurantCompte.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantComptes = $this->restaurantCompteRepository->all();

        return view('restaurant_comptes.index')
            ->with('restaurantComptes', $restaurantComptes);
    }

    /**
     * Show the form for creating a new RestaurantCompte.
     *
     * @return Response
     */
    public function create()
    {
        return view('restaurant_comptes.create');
    }

    /**
     * Store a newly created RestaurantCompte in storage.
     *
     * @param CreateRestaurantCompteRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantCompteRequest $request)
    {
        $input = $request->all();

        $restaurantCompte = $this->restaurantCompteRepository->create($input);

        Flash::success('Restaurant Compte saved successfully.');

        return redirect(route('restaurantComptes.index'));
    }

    /**
     * Display the specified RestaurantCompte.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $restaurantCompte = $this->restaurantCompteRepository->find($id);

        if (empty($restaurantCompte)) {
            Flash::error('Restaurant Compte not found');

            return redirect(route('restaurantComptes.index'));
        }

        return view('restaurant_comptes.show')->with('restaurantCompte', $restaurantCompte);
    }

    /**
     * Show the form for editing the specified RestaurantCompte.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $restaurantCompte = $this->restaurantCompteRepository->find($id);

        if (empty($restaurantCompte)) {
            Flash::error('Restaurant Compte not found');

            return redirect(route('restaurantComptes.index'));
        }

        return view('restaurant_comptes.edit')->with('restaurantCompte', $restaurantCompte);
    }

    /**
     * Update the specified RestaurantCompte in storage.
     *
     * @param int $id
     * @param UpdateRestaurantCompteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantCompteRequest $request)
    {
        $restaurantCompte = $this->restaurantCompteRepository->find($id);

        if (empty($restaurantCompte)) {
            Flash::error('Restaurant Compte not found');

            return redirect(route('restaurantComptes.index'));
        }

        $restaurantCompte = $this->restaurantCompteRepository->update($request->all(), $id);

        Flash::success('Restaurant Compte updated successfully.');

        return redirect(route('restaurantComptes.index'));
    }

    /**
     * Remove the specified RestaurantCompte from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $restaurantCompte = $this->restaurantCompteRepository->find($id);

        if (empty($restaurantCompte)) {
            Flash::error('Restaurant Compte not found');

            return redirect(route('restaurantComptes.index'));
        }

        $this->restaurantCompteRepository->delete($id);

        Flash::success('Restaurant Compte deleted successfully.');

        return redirect(route('restaurantComptes.index'));
    }
}
