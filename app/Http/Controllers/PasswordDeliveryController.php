<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePasswordDeliveryRequest;
use App\Http\Requests\UpdatePasswordDeliveryRequest;
use App\Repositories\PasswordDeliveryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PasswordDeliveryController extends AppBaseController
{
    /** @var  PasswordDeliveryRepository */
    private $passwordDeliveryRepository;

    public function __construct(PasswordDeliveryRepository $passwordDeliveryRepo)
    {
        $this->passwordDeliveryRepository = $passwordDeliveryRepo;
    }

    /**
     * Display a listing of the PasswordDelivery.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $passwordDeliveries = $this->passwordDeliveryRepository->all();

        return view('password_deliveries.index')
            ->with('passwordDeliveries', $passwordDeliveries);
    }

    /**
     * Show the form for creating a new PasswordDelivery.
     *
     * @return Response
     */
    public function create()
    {
        return view('password_deliveries.create');
    }

    /**
     * Store a newly created PasswordDelivery in storage.
     *
     * @param CreatePasswordDeliveryRequest $request
     *
     * @return Response
     */
    public function store(CreatePasswordDeliveryRequest $request)
    {
        $input = $request->all();

        $passwordDelivery = $this->passwordDeliveryRepository->create($input);

        Flash::success('Password Delivery saved successfully.');

        return redirect(route('passwordDeliveries.index'));
    }

    /**
     * Display the specified PasswordDelivery.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $passwordDelivery = $this->passwordDeliveryRepository->find($id);

        if (empty($passwordDelivery)) {
            Flash::error('Password Delivery not found');

            return redirect(route('passwordDeliveries.index'));
        }

        return view('password_deliveries.show')->with('passwordDelivery', $passwordDelivery);
    }

    /**
     * Show the form for editing the specified PasswordDelivery.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $passwordDelivery = $this->passwordDeliveryRepository->find($id);

        if (empty($passwordDelivery)) {
            Flash::error('Password Delivery not found');

            return redirect(route('passwordDeliveries.index'));
        }

        return view('password_deliveries.edit')->with('passwordDelivery', $passwordDelivery);
    }

    /**
     * Update the specified PasswordDelivery in storage.
     *
     * @param int $id
     * @param UpdatePasswordDeliveryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePasswordDeliveryRequest $request)
    {
        $passwordDelivery = $this->passwordDeliveryRepository->find($id);

        if (empty($passwordDelivery)) {
            Flash::error('Password Delivery not found');

            return redirect(route('passwordDeliveries.index'));
        }

        $passwordDelivery = $this->passwordDeliveryRepository->update($request->all(), $id);

        Flash::success('Password Delivery updated successfully.');

        return redirect(route('passwordDeliveries.index'));
    }

    /**
     * Remove the specified PasswordDelivery from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $passwordDelivery = $this->passwordDeliveryRepository->find($id);

        if (empty($passwordDelivery)) {
            Flash::error('Password Delivery not found');

            return redirect(route('passwordDeliveries.index'));
        }

        $this->passwordDeliveryRepository->delete($id);

        Flash::success('Password Delivery deleted successfully.');

        return redirect(route('passwordDeliveries.index'));
    }
}
