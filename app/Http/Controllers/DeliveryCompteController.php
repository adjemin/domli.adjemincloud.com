<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDeliveryCompteRequest;
use App\Http\Requests\UpdateDeliveryCompteRequest;
use App\Repositories\DeliveryCompteRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DeliveryCompteController extends AppBaseController
{
    /** @var  DeliveryCompteRepository */
    private $deliveryCompteRepository;

    public function __construct(DeliveryCompteRepository $deliveryCompteRepo)
    {
        $this->deliveryCompteRepository = $deliveryCompteRepo;
    }

    /**
     * Display a listing of the DeliveryCompte.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryComptes = $this->deliveryCompteRepository->all();

        return view('delivery_comptes.index')
            ->with('deliveryComptes', $deliveryComptes);
    }

    /**
     * Show the form for creating a new DeliveryCompte.
     *
     * @return Response
     */
    public function create()
    {
        return view('delivery_comptes.create');
    }

    /**
     * Store a newly created DeliveryCompte in storage.
     *
     * @param CreateDeliveryCompteRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryCompteRequest $request)
    {
        $input = $request->all();

        $deliveryCompte = $this->deliveryCompteRepository->create($input);

        Flash::success('Delivery Compte saved successfully.');

        return redirect(route('deliveryComptes.index'));
    }

    /**
     * Display the specified DeliveryCompte.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deliveryCompte = $this->deliveryCompteRepository->find($id);

        if (empty($deliveryCompte)) {
            Flash::error('Delivery Compte not found');

            return redirect(route('deliveryComptes.index'));
        }

        return view('delivery_comptes.show')->with('deliveryCompte', $deliveryCompte);
    }

    /**
     * Show the form for editing the specified DeliveryCompte.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $deliveryCompte = $this->deliveryCompteRepository->find($id);

        if (empty($deliveryCompte)) {
            Flash::error('Delivery Compte not found');

            return redirect(route('deliveryComptes.index'));
        }

        return view('delivery_comptes.edit')->with('deliveryCompte', $deliveryCompte);
    }

    /**
     * Update the specified DeliveryCompte in storage.
     *
     * @param int $id
     * @param UpdateDeliveryCompteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryCompteRequest $request)
    {
        $deliveryCompte = $this->deliveryCompteRepository->find($id);

        if (empty($deliveryCompte)) {
            Flash::error('Delivery Compte not found');

            return redirect(route('deliveryComptes.index'));
        }

        $deliveryCompte = $this->deliveryCompteRepository->update($request->all(), $id);

        Flash::success('Delivery Compte updated successfully.');

        return redirect(route('deliveryComptes.index'));
    }

    /**
     * Remove the specified DeliveryCompte from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryCompte = $this->deliveryCompteRepository->find($id);

        if (empty($deliveryCompte)) {
            Flash::error('Delivery Compte not found');

            return redirect(route('deliveryComptes.index'));
        }

        $this->deliveryCompteRepository->delete($id);

        Flash::success('Delivery Compte deleted successfully.');

        return redirect(route('deliveryComptes.index'));
    }
}
