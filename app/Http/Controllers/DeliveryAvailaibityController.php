<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDeliveryAvailaibityRequest;
use App\Http\Requests\UpdateDeliveryAvailaibityRequest;
use App\Repositories\DeliveryAvailaibityRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DeliveryAvailaibityController extends AppBaseController
{
    /** @var  DeliveryAvailaibityRepository */
    private $deliveryAvailaibityRepository;

    public function __construct(DeliveryAvailaibityRepository $deliveryAvailaibityRepo)
    {
        $this->deliveryAvailaibityRepository = $deliveryAvailaibityRepo;
    }

    /**
     * Display a listing of the DeliveryAvailaibity.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryAvailaibities = $this->deliveryAvailaibityRepository->all();

        return view('delivery_availaibities.index')
            ->with('deliveryAvailaibities', $deliveryAvailaibities);
    }

    /**
     * Show the form for creating a new DeliveryAvailaibity.
     *
     * @return Response
     */
    public function create()
    {
        return view('delivery_availaibities.create');
    }

    /**
     * Store a newly created DeliveryAvailaibity in storage.
     *
     * @param CreateDeliveryAvailaibityRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryAvailaibityRequest $request)
    {
        $input = $request->all();

        $deliveryAvailaibity = $this->deliveryAvailaibityRepository->create($input);

        Flash::success('Delivery Availaibity saved successfully.');

        return redirect(route('deliveryAvailaibities.index'));
    }

    /**
     * Display the specified DeliveryAvailaibity.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deliveryAvailaibity = $this->deliveryAvailaibityRepository->find($id);

        if (empty($deliveryAvailaibity)) {
            Flash::error('Delivery Availaibity not found');

            return redirect(route('deliveryAvailaibities.index'));
        }

        return view('delivery_availaibities.show')->with('deliveryAvailaibity', $deliveryAvailaibity);
    }

    /**
     * Show the form for editing the specified DeliveryAvailaibity.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $deliveryAvailaibity = $this->deliveryAvailaibityRepository->find($id);

        if (empty($deliveryAvailaibity)) {
            Flash::error('Delivery Availaibity not found');

            return redirect(route('deliveryAvailaibities.index'));
        }

        return view('delivery_availaibities.edit')->with('deliveryAvailaibity', $deliveryAvailaibity);
    }

    /**
     * Update the specified DeliveryAvailaibity in storage.
     *
     * @param int $id
     * @param UpdateDeliveryAvailaibityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryAvailaibityRequest $request)
    {
        $deliveryAvailaibity = $this->deliveryAvailaibityRepository->find($id);

        if (empty($deliveryAvailaibity)) {
            Flash::error('Delivery Availaibity not found');

            return redirect(route('deliveryAvailaibities.index'));
        }

        $deliveryAvailaibity = $this->deliveryAvailaibityRepository->update($request->all(), $id);

        Flash::success('Delivery Availaibity updated successfully.');

        return redirect(route('deliveryAvailaibities.index'));
    }

    /**
     * Remove the specified DeliveryAvailaibity from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryAvailaibity = $this->deliveryAvailaibityRepository->find($id);

        if (empty($deliveryAvailaibity)) {
            Flash::error('Delivery Availaibity not found');

            return redirect(route('deliveryAvailaibities.index'));
        }

        $this->deliveryAvailaibityRepository->delete($id);

        Flash::success('Delivery Availaibity deleted successfully.');

        return redirect(route('deliveryAvailaibities.index'));
    }
}
