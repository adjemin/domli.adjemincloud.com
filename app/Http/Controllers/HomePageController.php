<?php

namespace App\Http\Controllers;

use App\Models\Restaurants;
use App\Models\Orders;
use App\Models\RestaurantCategories;
use App\Http\Controllers\Controller;
use App\Models\FoodCategories;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($request->session()->get('user_ip_address'));
        $restaurants = Restaurants::where([
            'verified'  => 1,
            'is_active' => 1
        ])->take(6)->get();
        // dd($restaurants->get());
        $restaurant_categories = RestaurantCategories::all()->take(6);
        $food_categorires = FoodCategories::all()->take(6);
        
        if(auth()->guard('customer')->check()){
            $order = Orders::latest()->where([
                'customer_id'   =>  auth()->guard('customer')->user()->id,
            ])->whereNotIn('current_status', [Orders::CUSTOMER_DELIVERED, Orders::REFUSED, Orders::CANCELED, Orders::FAILED])
            ->first();
            // dd($order);
            return view('customer.homePage.index', compact('order','restaurants', 'food_categorires'));
        }

        return view('customer.homePage.index', compact('restaurants', 'food_categorires'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showRestaurant()
    {
        //
    }
}
