<?php

namespace App\Http\Controllers\DelivererAuth;

use App\Deliverer;
use App\VerifyDeliverer;
use Validator;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use App\Mail\DelivererRegisterMail;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    // use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/deliverer/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('deliverer.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // dd($data);
        return Validator::make($data, [
            'name' => 'required|max:255',
            'lastname' => 'required|max:255|unique:deliverers',
            'email' => 'required|email|max:255|unique:deliverers',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Deliverer
     */
    protected function create(array $data, array $userCoordinates)
    {
        return Deliverer::create([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'country_code' => $userCoordinates['country_code'],
            'dial_code' => $userCoordinates['location']['calling_code'] ?? '',
            'phone_number' => $data['phone_number'],
            'phone' => ($userCoordinates['location']['calling_code'] ?? '').''.$data['phone_number'],
            'password' => bcrypt($data['password']),
            'region_name' => $userCoordinates['region_name'],
            "city" => $userCoordinates['city'],
            "lat" => $userCoordinates['latitude'],
            "lng" => $userCoordinates['longitude'],
            "language" => $userCoordinates['location']['languages'][0]['name'] ?? '',
            'is_active' => true,
            'activation_date' => now()
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $userCoordinates = $request->session()->get('user_ip_address');

        $user = $this->create($request->all(), $userCoordinates);

        // $this->guard()->login($user);
        Auth::guard('deliverer')->loginUsingId($user->id);

        return redirect(route('profileAuth'));
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('deliverer.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('deliverer');
    }

    public function verifyDeliverer($token) {

        $verifyDeliverer = VerifyDeliverer::where('token', $token)->first();

        if(is_null($verifyDeliverer) || empty($verifyDeliverer)){
            $verifyDeliverer = \App\Models\Delivery::where('email', $token)->first();
            $deliverer = \App\Models\Delivery::where('email', $token)->first();
        }else{
            $deliverer = $verifyDeliverer->deliverer;
        }
        
        if(isset($verifyDeliverer) ){
            // dd($customer);
            if(!$deliverer->email_verified) {
                $deliverer->email_verified = 1;
                $deliverer->email_verified_at = now();
                $deliverer->save();
                // $status = "Your e-mail is verified. You can now login.";
            }

            $users = User::all();
            foreach ($users as $user) {
                Mail::to($user)->send(new DelivererRegisterMail($deliverer));
            }

            return redirect(route('congratulateDeliverer'));
        }
    }
}
