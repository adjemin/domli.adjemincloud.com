<?php

namespace App\Http\Controllers;

use App\Models\Food;
use Laracasts\Flash\Flash;
use App\Models\Restaurants;
use Illuminate\Http\Request;
use App\Models\FoodCategories;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\FoodParts;
use App\Models\RestaurantCategories;
use Illuminate\Support\Facades\Auth;

class RestaurantFrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = $request->query("categories") ?? 'all';
        // $restaurant_categories = RestaurantCategories::all();
        $food_categories = FoodCategories::all();

        $food_categories = collect($food_categories)->filter(function($food_category){
            return $food_category->foods->count() > 0;
        })->sortByDesc(function($food_category, $key){
            return $food_category->foods->count();
        });

        $restaurants = Restaurants::where([
            'verified'  => 1,
            'is_active' => 1
        ])->whereIn('categorie_id', collect($food_categories)->map(function($food_category){
            return $food_category->id;
        })->toArray())->get();

        $food_categories = $food_categories->whereIn('id', collect($restaurants)->map(function($restaurant){
            return $restaurant->categorie_id;
        }));

        if($categories != "all"){
            $categorie = FoodCategories::where('slug', $categories)->first();

            $restaurants = Restaurants::where('categorie_id', $categorie->id)->inRandomOrder()->get();
            $others = Restaurants::whereNotIn('categorie_id', [$categorie->id])->inRandomOrder()->take(10)->get();

            // $restaurants = Restaurants::Leftjoin('restaurant_categories', 'restaurants.id', '=', 'restaurant_categories.restaurant_id')->select('restaurants.*')->where('restaurant_categories.slug', $categories)->inRandomOrder()->take(10)->get();
            // $others = Restaurants::Leftjoin('restaurant_categories', 'restaurants.id', '=', 'restaurant_categories.restaurant_id')->select('restaurants.*')->where('restaurant_categories.slug', '<>',$categories)->inRandomOrder()->take(10)->get();
        }else{
            if($request->session()->exists('restaurants')){
                $categories = 'Search';
                $restaurants = $request->session()->get('restaurants');
                $request->session()->forget('restaurants');
            }else{
                $restaurants = Restaurants::inRandomOrder()->take(10)->get();
            }

            $restaurant_ids = collect($restaurants)->map(function($restaurant){
                return $restaurant->id;
            });

            $others = Restaurants::whereNotIn('id', $restaurant_ids)->inRandomOrder()->take(10)->get();
        }


        $pageInfo = ["title" => "All restaurants"];
        $userCoordinates = $request->session()->get('user_ip_address');

        if($request->session()->exists('search')){
            $search = $request->session()->get('search');
            $request->session()->forget('search');
            return view('frontOffice.pages.restaurants', compact('restaurants', 'others','pageInfo', 'food_categories', 'categories', 'userCoordinates', 'search'));
        }
        return view('frontOffice.pages.restaurants', compact('restaurants', 'others','pageInfo', 'food_categories', 'categories', 'userCoordinates'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Restaurants  $restaurants
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $uuid)
    {
        $restaurant = Restaurants::where('uuid', $uuid)->orWhere('id', $uuid)->first();

        $foods = $request->session()->exists('foods') ? $request->session()->get('foods') : Food::where('restaurant_id', $restaurant->id)->get();

        $foods = collect($foods)->groupBy(function($item, $key){
            return $item->menu_id;
        })->toArray();

        $search = $request->session()->exists('search') ? $request->session()->get('search') : '';
        $sum = 0;

        if($request->session()->exists('foods')){
            $request->session()->forget('foods');
            $request->session()->forget('search');
        }

        // $foodPart = FoodParts::where('')
        // $data = [
        //     'food' => [
        //         'uuid' => $food->uuid,
        //         'title' => $food->title,
        //         'description' => $food->description,
        //         'image'  => $food->image,
        //         'price'  => $food->price ?? $food->base_price ?? 0,
        //     ],
        //     'foodParts' => $food->parts,
        //     'foodPartItems' => collect($food->parts)->map(function($foodPart){
        //         return $foodPart->items;
        //     })->toArray()
        // ];
        return view('frontOffice.pages.restaurant', compact('foods', 'restaurant', 'search', 'sum'));
        // dd($foods);

        // $foods = FoodCategories::find(6);
        // $food = $foods->foods();


        // dd($restaurant_foods);
        // $categories = $request->query("categories") ?? 'all';

        // $restaurant = Restaurants::where('uuid', $id)->orWhere('id', $id)->first();

        // if(empty($restaurant) || is_null($restaurant)){
        //     return redirect()->back();
        // }

        // if($categories != "all"){
        //     $foods = Food::LeftJoin('food_categories', 'food_categories.id', '=', 'food.food_category_id')
        //         ->select('food.*')->where('food_categories.title', $categories)->where('restaurant_id', $restaurant->id)->get();

        //     $food_alls = Food::where('restaurant_id', $restaurant->id)->get();

        //     $food_categories_ids = collect($food_alls)->map(function($food){
        //         return $food->food_category_id;
        //     })->unique()->toArray();

        // }else{
        //     if($request->session()->exists('foods')){
        //         $foods = $request->session()->get('foods');
        //         $request->session()->forget('foods');

        //         $categories ='Search';

        //         $food_alls = Food::where('restaurant_id', $restaurant->id)->get();

        //         $food_categories_ids = collect($food_alls)->map(function($food){
        //             return $food->food_category_id;
        //         })->unique()->toArray();
        //     }else{
        //         $foods = Food::where('restaurant_id', $restaurant->id)->get();
        //         $food_categories_ids = collect($foods)->map(function($food){
        //             return $food->food_category_id;
        //         })->unique()->toArray();
        //     }

        // }


        // $food_categories = FoodCategories::whereIn('id', $food_categories_ids)->get();

        // if (empty($restaurant)) {
        //     Flash::error('Restaurant not found');

        //     return redirect()->back();
        // }

        // $userCoordinates = $request->session()->get('user_ip_address');
        // if($request->session()->exists('search')){
        //     $search = $request->session()->get('search');
        //     $request->session()->forget('search');
        //     return view('frontOffice.pages.restaurant', compact('restaurant', 'foods', 'food_categories', 'categories', 'userCoordinates', 'search'));
        // }


        // session([
        //     "restaurant" => $restaurant
        //     ,"foods" => $foods
        //     ,"food_categories" => $food_categories
        //     ,"categories" => $categories
        //     ,"userCoordinates" => $userCoordinates
        // ]);


        // $foods = Food::LeftJoin('food_categories', 'food_categories.id', '=', 'food.food_category_id')
        //         ->select('food.*')->where('food_categories.title', $categories)->where('restaurant_id', $restaurant->id)->get();
        // dd($foods);
        // return view('frontOffice.pages.restaurant', compact('restaurant', 'foods', 'food_categories', 'categories', 'userCoordinates'));
    }

    public function searchRestaurant(Request $request)
    {
        $restaurants =  Restaurants::LeftJoin('food_categories', 'restaurants.categorie_id', '=','food_categories.id')
                            ->select('restaurants.*')
                            ->where('restaurants.is_active', '=', 1)
                            ->where('restaurants.verified', '=', 1)
                            ->where('restaurants.name', 'like','%'.$request->search.'%')
                            ->orWhere('restaurants.restaurant_adress_name', 'like','%'.$request->search.'%')
                            ->orWhere('restaurants.restaurant_region_name', 'like','%'.$request->search.'%')
                            ->orWhere('restaurants.restaurant_city', 'like','%'.$request->search.'%')
                            ->orWhere('food_categories.title', 'like','%'.$request->search.'%')
                            ->orWhere('food_categories.slug', 'like','%'.$request->search.'%')
                            ->orWhere(function($query) use ($request){
                                $query->geofence($request->session()->get('user_ip_address')['latitude'] ?? \Auth::guard('customer')->user()->lat ?? 5.39150, $request->session()->get('user_ip_address')['latitude'] ?? \Auth::guard('customer')->user()->lng ?? -3.97765, 0, 15);
                            })->get();

        $request->session()->put('restaurants', $restaurants);
        $request->session()->put('search', $request->search);
        return redirect("/restaurants");
        // dd($restaurants);
    }

    public function searchFood(Request $request)
    {

        // dd($request->all());
        $foods = Food::LeftJoin('food_categories','food.food_category_id', '=', 'food_categories.id')
            ->LeftJoin('restaurant_menuses', 'food.menu_id', '=', 'restaurant_menuses.id')
            ->LeftJoin('restaurants', 'restaurants.id', '=', 'restaurant_menuses.restaurant_id')
            ->select('food.*')
            ->where('restaurants.uuid', $request->uuid)
            ->where('restaurants.is_active', 1)
            ->where('restaurants.verified', 1)
            ->orWhere('food_categories.title', 'like', '%'.$request->search.'%')
            ->orWhere('food_categories.slug', 'like', '%'.$request->search.'%')
            ->orWhere('food_categories.description', 'like', '%'.$request->search.'%')
            ->orWhere('restaurant_menuses.title', 'like', '%'.$request->search.'%')
            ->orWhere('restaurant_menuses.slug', 'like', '%'.$request->search.'%')
            ->orWhere('food.uuid', 'like', '%'.$request->search.'%')
            ->orWhere('food.title', 'like', '%'.$request->search.'%')
            ->orWhere('food.description', 'like', '%'.$request->search.'%')
            ->get();

        // dd($foods);
        // $foods = Food::
            
        //     ->where('food.restaurant_id', '=', intval($request->resto))
        //     ->orWhere('food_categories.title', 'like', '%'.$request->title.'%')
            
        //     ->get();

        // dd($foods);

        //filter one more time for get restaurant's food
        // $foods = collect($foods)->filter(function($food, $key) use ($request) {
        //     return  $food->price == intval($request->title) || $food->restaurant_id ==  $request->resto || (!is_null($food->menu) && (strpos($food->menu->title, $request->title) !== false || strpos($food->menu->slug, $request->title) !== false) ) || (!is_null($food->food_category) && (strpos($food->food_category->title, $request->title) !== false || strpos($food->food_category->slug, $request->title) !== false || strpos($food->food_category->slug, $request->title) !== false ) );
        // });

        $request->session()->put('foods', $foods);
        $request->session()->put('search', $request->search);
        return redirect("/page/restaurant/$request->uuid");
        // dd($restaurants);
    }

    public function showCategorie(){
        $food_categorires = FoodCategories::all()->take(6);
        $restaurant = RestaurantCategories::where('restaurant_id', 1)->get();
        dd($restaurant);
        return view();
    }

    public function showCategorieRestaurant(){
        // dd(session('RestaurantWithCategorie'));
        return view('frontOffice.pages.restaurantCategories');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Restaurants  $restaurants
     * @return \Illuminate\Http\Response
     */
    public function edit(Restaurants $restaurants)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Restaurants  $restaurants
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Restaurants $restaurants)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Restaurants  $restaurants
     * @return \Illuminate\Http\Response
     */
    public function destroy(Restaurants $restaurants)
    {
        //
    }
}
