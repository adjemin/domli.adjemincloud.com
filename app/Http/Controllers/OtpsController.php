<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOtpsRequest;
use App\Http\Requests\UpdateOtpsRequest;
use App\Repositories\OtpsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OtpsController extends AppBaseController
{
    /** @var  OtpsRepository */
    private $otpsRepository;

    public function __construct(OtpsRepository $otpsRepo)
    {
        $this->otpsRepository = $otpsRepo;
    }

    /**
     * Display a listing of the Otps.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $otps = $this->otpsRepository->all();

        return view('otps.index')
            ->with('otps', $otps);
    }

    /**
     * Show the form for creating a new Otps.
     *
     * @return Response
     */
    public function create()
    {
        return view('otps.create');
    }

    /**
     * Store a newly created Otps in storage.
     *
     * @param CreateOtpsRequest $request
     *
     * @return Response
     */
    public function store(CreateOtpsRequest $request)
    {
        $input = $request->all();

        $otps = $this->otpsRepository->create($input);

        Flash::success('Otps saved successfully.');

        return redirect(route('otps.index'));
    }

    /**
     * Display the specified Otps.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $otps = $this->otpsRepository->find($id);

        if (empty($otps)) {
            Flash::error('Otps not found');

            return redirect(route('otps.index'));
        }

        return view('otps.show')->with('otps', $otps);
    }

    /**
     * Show the form for editing the specified Otps.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $otps = $this->otpsRepository->find($id);

        if (empty($otps)) {
            Flash::error('Otps not found');

            return redirect(route('otps.index'));
        }

        return view('otps.edit')->with('otps', $otps);
    }

    /**
     * Update the specified Otps in storage.
     *
     * @param int $id
     * @param UpdateOtpsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOtpsRequest $request)
    {
        $otps = $this->otpsRepository->find($id);

        if (empty($otps)) {
            Flash::error('Otps not found');

            return redirect(route('otps.index'));
        }

        $otps = $this->otpsRepository->update($request->all(), $id);

        Flash::success('Otps updated successfully.');

        return redirect(route('otps.index'));
    }

    /**
     * Remove the specified Otps from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $otps = $this->otpsRepository->find($id);

        if (empty($otps)) {
            Flash::error('Otps not found');

            return redirect(route('otps.index'));
        }

        $this->otpsRepository->delete($id);

        Flash::success('Otps deleted successfully.');

        return redirect(route('otps.index'));
    }
}
