<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRestaurantAvailabilityRequest;
use App\Http\Requests\UpdateRestaurantAvailabilityRequest;
use App\Repositories\RestaurantAvailabilityRepository;
use App\Models\RestaurantAvailability;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Auth;
use Response;

class RestaurantAvailabilityController extends AppBaseController
{
    /** @var  RestaurantAvailabilityRepository */
    private $restaurantAvailabilityRepository;

    public function __construct(RestaurantAvailabilityRepository $restaurantAvailabilityRepo)
    {
        $this->restaurantAvailabilityRepository = $restaurantAvailabilityRepo;
    }

    /**
     * Display a listing of the RestaurantAvailability.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if(Auth::user()){
            $restaurantAvailabilities = RestaurantAvailability::orderByDesc('created_at')->paginate(10);
        }else if(Auth::guard('restaurant')->check()){
            $restaurantAvailabilities = RestaurantAvailability::where('restaurant_id', Auth::guard('restaurant')->user()->id)->orderByDesc('created_at')->paginate(10);
        }else{
            return redirect()->back();
        }

        return view('restaurant_availabilities.index')
            ->with('restaurantAvailabilities', $restaurantAvailabilities);
    }

    /**
     * Show the form for creating a new RestaurantAvailability.
     *
     * @return Response
     */
    public function create()
    {
        return view('restaurant_availabilities.create');
    }

    /**
     * Store a newly created RestaurantAvailability in storage.
     *
     * @param CreateRestaurantAvailabilityRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantAvailabilityRequest $request)
    {
        $input = $request->all();

        $input['restaurant_id'] = Auth::guard('restaurant')->user()->id;

        $restaurantAvailability = $this->restaurantAvailabilityRepository->create($input);

        Flash::success('Restaurant Availability saved successfully.');

        return redirect(route('restaurantAvailabilities.index'));
    }

    /**
     * Display the specified RestaurantAvailability.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $restaurantAvailability = $this->restaurantAvailabilityRepository->find($id);

        if(Auth::guard('restaurant')->check()){
            if($restaurantAvailability->restaurant_id != Auth::guard('restaurant')->user()->id){
                Flash::error("Restaurant not found");
                return redirect()->back();
            }
        }

        if (empty($restaurantAvailability)) {
            Flash::error('Restaurant Availability not found');

            return redirect(route('restaurantAvailabilities.index'));
        }

        return view('restaurant_availabilities.show')->with('restaurantAvailability', $restaurantAvailability);
    }

    /**
     * Show the form for editing the specified RestaurantAvailability.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $restaurantAvailability = $this->restaurantAvailabilityRepository->find($id);

        if(Auth::guard('restaurant')->check()){
            if($restaurantAvailability->restaurant_id != Auth::guard('restaurant')->user()->id){
                Flash::error("Restaurant not found");
                return redirect()->back();
            }
        }

        if (empty($restaurantAvailability)) {
            Flash::error('Restaurant Availability not found');

            return redirect(route('restaurantAvailabilities.index'));
        }

        return view('restaurant_availabilities.edit')->with('restaurantAvailability', $restaurantAvailability);
    }

    /**
     * Update the specified RestaurantAvailability in storage.
     *
     * @param int $id
     * @param UpdateRestaurantAvailabilityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantAvailabilityRequest $request)
    {
        $restaurantAvailability = $this->restaurantAvailabilityRepository->find($id);

        if(Auth::guard('restaurant')->check()){
            if($restaurantAvailability->restaurant_id != Auth::guard('restaurant')->user()->id){
                Flash::error("Restaurant not found");
                return redirect()->back();
            }
        }

        if (empty($restaurantAvailability)) {
            Flash::error('Restaurant Availability not found');

            return redirect(route('restaurantAvailabilities.index'));
        }

        $restaurantAvailability = $this->restaurantAvailabilityRepository->update($request->all(), $id);

        Flash::success('Restaurant Availability updated successfully.');

        return redirect(route('restaurantAvailabilities.index'));
    }

    /**
     * Remove the specified RestaurantAvailability from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $restaurantAvailability = $this->restaurantAvailabilityRepository->find($id);

        if(Auth::guard('restaurant')->check()){
            if($restaurantAvailability->restaurant_id != Auth::guard('restaurant')->user()->id){
                Flash::error("Restaurant not found");
                return redirect()->back();
            }
        }

        if (empty($restaurantAvailability)) {
            Flash::error('Restaurant Availability not found');

            return redirect(route('restaurantAvailabilities.index'));
        }

        $this->restaurantAvailabilityRepository->delete($id);

        Flash::success('Restaurant Availability deleted successfully.');

        return redirect(route('restaurantAvailabilities.index'));
    }
}
