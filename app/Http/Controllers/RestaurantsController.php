<?php

namespace App\Http\Controllers;

use Flash;
use Imgur;
use Auth;
use Response;
use Yish\Imgur\Upload;
use Illuminate\Http\File;
use App\Models\Restaurants;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Repositories\RestaurantsRepository;
use App\Http\Requests\CreateRestaurantsRequest;
use App\Http\Requests\UpdateRestaurantsRequest;


class RestaurantsController extends AppBaseController
{
    /** @var  RestaurantsRepository */
    private $restaurantsRepository;

    public function __construct(RestaurantsRepository $restaurantsRepo)
    {
        $this->restaurantsRepository = $restaurantsRepo;
    }

    /**
     * Display a listing of the Restaurants.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $restaurants = $this->restaurantsRepository->all();
        if(Auth::guard('restaurant')->check()){
            return redirect()->back();
        }
        
        $restaurants = Restaurants::orderByDesc('id')->paginate(10);

        return view('restaurants.index')
            ->with('restaurants', $restaurants);
    }

    /**
     * Show the form for creating a new Restaurants.
     *
     * @return Response
     */
    public function create()
    {
        return view('restaurants.create');
    }

    /**
     * Store a newly created Restaurants in storage.
     *
     * @param CreateRestaurantsRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        // $input = $request->all();
        $this->validate($request, [
            'cover_file' => 'required|image',
            'logo_file' => 'required|image',
            'lat' => 'required',
            'title' => 'required',
            'lng' => 'required',
        ]);

        $cover_file = $request->file('cover_file');
        $logo_file = $request->file('logo_file');

        $coverImage = Imgur::upload($cover_file);
        $coverImageLink = $coverImage->link();
        $logoImage = Imgur::upload($logo_file);
        $logoImageLink = $logoImage->link();
        //dd($coverImage->link());
        $input = [
            'title' => $request->title,
            'cover' => $coverImageLink ,
            'logo' =>  $logoImageLink,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'location_name' => $request->address,
            'description'=> $request->description
        ];

        $restaurants = $this->restaurantsRepository->create($input);

        Flash::success('Restaurants saved successfully.');

        return redirect()->back();
        // return redirect(route('restaurants.index'));
    }

    /**
     * Display the specified Restaurants.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $restaurants = Restaurants::where('uuid', $id)->first() ?? $this->restaurantsRepository->find($id);

        if (empty($restaurants)) {
            Flash::error('Restaurants not found');

            return redirect(route('restaurants.index'));
        }

        $account = \App\Models\RestaurantCompte::where('restaurant_id', $restaurants->id)->first();

        if(empty($account) || is_null($account)){
            $paymentDeliveries = \App\Models\RestaurantPayment::where(['restaurant_id' => $restaurants->id])->get();

            $sumDelivery = collect($paymentDeliveries)->reduce(function($carry, $item){
                return $item->amount + $carry;
            }, 0);
            
            $account = \App\Models\RestaurantCompte::create([
                'restaurant_id' => $restaurants->id,
                'compte' => $sumDelivery
            ]);
        }

        return view('restaurants.show')->with('restaurants', $restaurants);
    }

    /**
     * Show the form for editing the specified Restaurants.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $restaurants = Restaurants::where('uuid', $id)->first() ?? $this->restaurantsRepository->find($id);

        if (empty($restaurants)) {
            Flash::error('Restaurants not found');

            return redirect(route('restaurants.index'));
        }

        $userCoordinates = $request->session()->get('user_ip_address');

        return view('restaurants.edit')->with('restaurants', $restaurants)->with('userCoordinates', $userCoordinates);
    }

    /**
     * Update the specified Restaurants in storage.
     *
     * @param int $id
     * @param UpdateRestaurantsRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'cover_file' => 'nullable|image',
            'logo_file' => 'nullable|image',
            'lat' => 'required',
            'title' => 'required',
            'lng' => 'required',
        ]);

        $restaurant = Restaurants::where('uuid', $id)->first() ?? $this->restaurantsRepository->find($id);

        // dd($restaurants);
        if (empty($restaurant) || is_null($restaurant)) {
            Flash::error('Restaurants not found');

            return redirect()->back();
            // return redirect(route('restaurants.index'));
        }

        if($request->file('cover_file')){
            $coverImage = Imgur::upload($request->cover_file);
            $coverImageLink = $coverImage->link();
        }

        if($request->file('logo_file')){
            $logoImage = Imgur::upload($request->logo_file);
            $logoImageLink = $logoImage->link();
        }

        $input = [
            'title' => $request->title ?? $restaurant->title,
            'cover' => $coverImageLink  ?? $restaurant->cover,
            'logo' =>  $logoImageLink ?? $restaurant->logo,
            'lat' => $request->lat ?? $restaurant->lat,
            'lng' => $request->lng ?? $restaurant->lng,
            'location_name' => $request->address,
            'description'=> $request->description ?? $restaurant->description
        ];

        // dd($input);

        $restaurants = $this->restaurantsRepository->update($input, $id);
        // $restaurants = $this->restaurantsRepository->update($request->all(), $id);

        Flash::success('Restaurants updated successfully.');

        return redirect(url('/backOffice/restaurants/'.$restaurants->uuid ?? $restaurants->id));
        // return redirect(route('restaurants.index'));
    }

    /**
     * Remove the specified Restaurants from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $restaurants = Restaurants::where('uuid', $id)->first() ?? $this->restaurantsRepository->find($id);

        if (empty($restaurants)) {
            Flash::error('Restaurants not found');

            return redirect()->back();
            // return redirect(route('customers.index'));
        }

        $this->restaurantsRepository->delete($restaurants->id);

        Flash::success('Restaurants deleted successfully.');

        return redirect(route('restaurants.index'));
    }

    public function change_state(Request $request, $id){
        $restaurant = $this->restaurantsRepository->find($id);

        if (empty($restaurant)) {
            Flash::error('Restaurant not found');

            return redirect()->back();
            // return redirect(route('customers.index'));
        }

        $restaurant->is_active = !$restaurant->is_active;
        if($restaurant->is_active){
            $restaurant->activation_date = now();
        }else{
            $restaurant->activation_date = null;
        }

        $restaurant->save();

        Flash::success('Restaurant update successfully');
        return redirect()->back();
    }
}
