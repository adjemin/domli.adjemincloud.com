<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Deliverer;
use App\Models\Delivery;
use App\Models\Orders;
use App\Models\Customers;
use App\Models\Restaurants;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customers::all();
        $restaurants = Restaurants::all();
        $deliverers = Delivery::all();
        $orders = Orders::all()->groupBy(function($item, $key){
            return date_format($item->created_at, "Y/m");
        })->toArray();

        $orders_all = Orders::paginate(10);

        return view('home', compact('customers', 'restaurants', 'deliverers','orders', 'orders_all'));
    }
}
