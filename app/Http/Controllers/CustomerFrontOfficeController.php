<?php

namespace App\Http\Controllers;

// use App\Models\Customer;
use App\Models\Customers;
use App\Models\Orders;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Repositories\CustomersRepository;
use Imgur;
use App\Http\Controllers\AppBaseController;


class CustomerFrontOfficeController extends AppBaseController
{

    /** @var  CustomersRepository */
    private $customersRepository;

    public function __construct(CustomersRepository $customersRepo)
    {
        $this->customersRepository = $customersRepo;
    }

    public function showProfile()
    {
        if(Auth::guard('customer')->check()){
            $customer = Customers::find(Auth::guard('customer')->user()->id);
            $orders = Orders::where(['customer_id' => $customer->id])->get();
            // $customerRequests = Customer::where(['id' => $customer->id])->get();
            // dd($customer);
            return view('customer.pages.profile', compact('customer', 'orders'));
        }else{
            return redirect()->back();
        }
    }

    public function showOrder()
    {
        // $customer = Customers::find(Auth::guard('customer')->user()->id);
        // $customerRequests = Customer::where(['id' => $customer->id])->get();
        // dd($customer);
        return view('customer.pages.orderDetail');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->all());
        $data = $request->all();
        // dd($request->json()->all());
        // dd($request->json());
        $message = '';
        $imgLink = null;
        // dd($request->json()->get('photo_profile'));
        // $immmage = $request->json()->get('photo_profile');
        // $image = $request->file($immmage);
        // dd($image);
        if($request->hasFile('photo_profile')){
            $productImage = Imgur::upload($request->file('photo_profile'));
            $imgLink = $productImage->link();
            // dd($imgLink);
        }
        // dd($imgLink);

        // if($request->file('photo_profile')){
        //     dd($request->file('photo_profile'));
        //     $image = $request->file('photo_profile');
        //     $pictures = [];
        //     if ($image != null) {
        //         $productImage = Imgur::upload($image);
        //         $imgLink = $productImage->link();
        //     }
        // }
        // else{
        //     $imgLink = 'https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png';
        // }

        if(isset($data["photo_profile"])){
            unset($data["photo_profile"]);
        }

        if(filter_var($data['email'], FILTER_VALIDATE_EMAIL) && !empty($data['last_name']) && !empty($data['name'])){

            $customer = Customers::whereIn('id', [$data['customer_id']])->first();
            
            $customer->update([
                'email' => $data['email'],
                'last_name' => $data['last_name'],
                'name' => $data['name'],
                'photo_profile' => !is_null($imgLink) ? $imgLink : $customer->photo_profile
            ]);

            $message = 'success';
            return $this->sendSuccess($message);
        }
        else {
            $message = 'error';
            return $this->sendError($message);
        }

        return $this->sendSuccess($message);
        // return $message;
    }

    public function showOrderFromOrderNumber(Request $request, $order_number){
        $order = Orders::where(['order_number' => $order_number])->first();

        if(is_null($order) || empty($order)){
            return redirect()->back();
        }
        
        return view('customer.pages.orderDetailSingle', compact('order'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
