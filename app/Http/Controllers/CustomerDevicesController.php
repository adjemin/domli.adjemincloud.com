<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerDevicesRequest;
use App\Http\Requests\UpdateCustomerDevicesRequest;
use App\Repositories\CustomerDevicesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CustomerDevicesController extends AppBaseController
{
    /** @var  CustomerDevicesRepository */
    private $customerDevicesRepository;

    public function __construct(CustomerDevicesRepository $customerDevicesRepo)
    {
        $this->customerDevicesRepository = $customerDevicesRepo;
    }

    /**
     * Display a listing of the CustomerDevices.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $customerDevices = $this->customerDevicesRepository->all();

        return view('customer_devices.index')
            ->with('customerDevices', $customerDevices);
    }

    /**
     * Show the form for creating a new CustomerDevices.
     *
     * @return Response
     */
    public function create()
    {
        return view('customer_devices.create');
    }

    /**
     * Store a newly created CustomerDevices in storage.
     *
     * @param CreateCustomerDevicesRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerDevicesRequest $request)
    {
        $input = $request->all();

        $customerDevices = $this->customerDevicesRepository->create($input);

        Flash::success('Customer Devices saved successfully.');

        return redirect(route('customerDevices.index'));
    }

    /**
     * Display the specified CustomerDevices.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customerDevices = $this->customerDevicesRepository->find($id);

        if (empty($customerDevices)) {
            Flash::error('Customer Devices not found');

            return redirect(route('customerDevices.index'));
        }

        return view('customer_devices.show')->with('customerDevices', $customerDevices);
    }

    /**
     * Show the form for editing the specified CustomerDevices.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customerDevices = $this->customerDevicesRepository->find($id);

        if (empty($customerDevices)) {
            Flash::error('Customer Devices not found');

            return redirect(route('customerDevices.index'));
        }

        return view('customer_devices.edit')->with('customerDevices', $customerDevices);
    }

    /**
     * Update the specified CustomerDevices in storage.
     *
     * @param int $id
     * @param UpdateCustomerDevicesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerDevicesRequest $request)
    {
        $customerDevices = $this->customerDevicesRepository->find($id);

        if (empty($customerDevices)) {
            Flash::error('Customer Devices not found');

            return redirect(route('customerDevices.index'));
        }

        $customerDevices = $this->customerDevicesRepository->update($request->all(), $id);

        Flash::success('Customer Devices updated successfully.');

        return redirect(route('customerDevices.index'));
    }

    /**
     * Remove the specified CustomerDevices from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customerDevices = $this->customerDevicesRepository->find($id);

        if (empty($customerDevices)) {
            Flash::error('Customer Devices not found');

            // return redirect(route('customerDevices.index'));
            return redirect()->back();
        }

        $this->customerDevicesRepository->delete($id);

        Flash::success('Customer Devices deleted successfully.');
        return redirect()->back();
        // return redirect(route('customerDevices.index'));
    }
}
