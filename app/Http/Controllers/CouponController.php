<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCouponRequest;
use App\Http\Requests\UpdateCouponRequest;
use App\Repositories\CouponRepository;
use App\Models\Coupon;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Auth;
use Response;

class CouponController extends AppBaseController
{
    /** @var  CouponRepository */
    private $couponRepository;

    public function __construct(CouponRepository $couponRepo)
    {
        $this->couponRepository = $couponRepo;
    }

    /**
     * Display a listing of the Coupon.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $coupons = $this->couponRepository->all();
        if(Auth::check()){
            $coupons = Coupon::orderBy('created_at','desc')->paginate(10);
        }else if(Auth::guard('restaurant')->check()){
            $coupons = Coupon::where('restaurant_id', Auth::guard('restaurant')->user()->id)->orderBy('created_at','desc')->paginate(10);
        }

        return view('coupons.index')
            ->with('coupons', $coupons);
    }

    /**
     * Show the form for creating a new Coupon.
     *
     * @return Response
     */
    public function create()
    {
        return view('coupons.create');
    }

    /**
     * Store a newly created Coupon in storage.
     *
     * @param CreateCouponRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['restaurant_id'] = Auth::guard('restaurant')->user()->id;
        // dd($input);

        $coupon = $this->couponRepository->create($input);

        Flash::success('Coupon saved successfully.');

        $route = Auth::check() ? route('admin-restaurants-index') : url('/backOffice/restaurants/'.Auth::guard('restaurant')->user()->uuid) ;
        return redirect(route($route));
        // return redirect(route('coupons.index'));
    }

    /**
     * Display the specified Coupon.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $coupon = $this->couponRepository->find($id);

        if (empty($coupon) || $coupon->restaurant_id != Auth::guard('restaurant')->user()->id) {
            Flash::error('Coupon not found');

            return redirect()->back();
            // return redirect(route('coupons.index'));
        }

        return view('coupons.show')->with('coupon', $coupon);
    }

    /**
     * Show the form for editing the specified Coupon.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $coupon = $this->couponRepository->find($id);

        if (empty($coupon) || $coupon->restaurant_id != Auth::guard('restaurant')->user()->id) {
            Flash::error('Coupon not found');

            return redirect()->back();
            // return redirect(route('coupons.index'));
        }

        return view('coupons.edit')->with('coupon', $coupon);
    }

    /**
     * Update the specified Coupon in storage.
     *
     * @param int $id
     * @param UpdateCouponRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $coupon = $this->couponRepository->find($id);
        $input['restaurant_id'] = Auth::guard('restaurant')->user()->id;

        if (empty($coupon) || $coupon->restaurant_id != Auth::guard('restaurant')->user()->id) {
            Flash::error('Coupon not found');

            return redirect()->back();
            // return redirect(route('coupons.index'));
        }

        $coupon = $this->couponRepository->update($request->all(), $id);

        Flash::success('Coupon updated successfully.');

        // return redirect(route('coupons.index'));
        $route = Auth::check() ? route('admin-restaurants-index') : url('/backOffice/restaurants/'.Auth::guard('restaurant')->user()->uuid) ;
        return redirect(route($route));
    }

    /**
     * Remove the specified Coupon from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $coupon = $this->couponRepository->find($id);

        if (empty($coupon) || $coupon->restaurant_id != Auth::guard('restaurant')->user()->id) {
            Flash::error('Coupon not found');

            return redirect()->back();
            // return redirect(route('coupons.index'));
        }

        $this->couponRepository->delete($id);

        Flash::success('Coupon deleted successfully.');

        return redirect()->back();
        // return redirect(route('coupons.index'));
    }
}
