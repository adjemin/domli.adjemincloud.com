<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOtpsAPIRequest;
use App\Http\Requests\API\UpdateOtpsAPIRequest;
use App\Models\Otps;
use App\Repositories\OtpsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\OtpsResource;
use Response;

/**
 * Class OtpsController
 * @package App\Http\Controllers\API
 */

class OtpsAPIController extends AppBaseController
{
    /** @var  OtpsRepository */
    private $otpsRepository;

    public function __construct(OtpsRepository $otpsRepo)
    {
        $this->otpsRepository = $otpsRepo;
    }

    /**
     * Display a listing of the Otps.
     * GET|HEAD /otps
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $otps = $this->otpsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(OtpsResource::collection($otps), 'Otps retrieved successfully');
    }

    /**
     * Store a newly created Otps in storage.
     * POST /otps
     *
     * @param CreateOtpsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOtpsAPIRequest $request)
    {
        $input = $request->all();

        $otps = $this->otpsRepository->create($input);

        return $this->sendResponse(new OtpsResource($otps), 'Otps saved successfully');
    }

    /**
     * Display the specified Otps.
     * GET|HEAD /otps/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Otps $otps */
        $otps = $this->otpsRepository->find($id);

        if (empty($otps)) {
            return $this->sendError('Otps not found');
        }

        return $this->sendResponse(new OtpsResource($otps), 'Otps retrieved successfully');
    }

    /**
     * Update the specified Otps in storage.
     * PUT/PATCH /otps/{id}
     *
     * @param int $id
     * @param UpdateOtpsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOtpsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Otps $otps */
        $otps = $this->otpsRepository->find($id);

        if (empty($otps)) {
            return $this->sendError('Otps not found');
        }

        $otps = $this->otpsRepository->update($input, $id);

        return $this->sendResponse(new OtpsResource($otps), 'Otps updated successfully');
    }

    /**
     * Remove the specified Otps from storage.
     * DELETE /otps/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Otps $otps */
        $otps = $this->otpsRepository->find($id);

        if (empty($otps)) {
            return $this->sendError('Otps not found');
        }

        $otps->delete();

        return $this->sendSuccess('Otps deleted successfully');
    }
}
