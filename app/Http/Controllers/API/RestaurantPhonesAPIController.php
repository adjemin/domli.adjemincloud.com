<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRestaurantPhonesAPIRequest;
use App\Http\Requests\API\UpdateRestaurantPhonesAPIRequest;
use App\Models\RestaurantPhones;
use App\Repositories\RestaurantPhonesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\RestaurantPhonesResource;
use Response;

/**
 * Class RestaurantPhonesController
 * @package App\Http\Controllers\API
 */

class RestaurantPhonesAPIController extends AppBaseController
{
    /** @var  RestaurantPhonesRepository */
    private $restaurantPhonesRepository;

    public function __construct(RestaurantPhonesRepository $restaurantPhonesRepo)
    {
        $this->restaurantPhonesRepository = $restaurantPhonesRepo;
    }

    /**
     * Display a listing of the RestaurantPhones.
     * GET|HEAD /restaurantPhones
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantPhones = $this->restaurantPhonesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(RestaurantPhonesResource::collection($restaurantPhones), 'Restaurant Phones retrieved successfully');
    }

    /**
     * Store a newly created RestaurantPhones in storage.
     * POST /restaurantPhones
     *
     * @param CreateRestaurantPhonesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantPhonesAPIRequest $request)
    {
        $input = $request->all();

        $restaurantPhones = $this->restaurantPhonesRepository->create($input);

        return $this->sendResponse(new RestaurantPhonesResource($restaurantPhones), 'Restaurant Phones saved successfully');
    }

    /**
     * Display the specified RestaurantPhones.
     * GET|HEAD /restaurantPhones/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RestaurantPhones $restaurantPhones */
        $restaurantPhones = $this->restaurantPhonesRepository->find($id);

        if (empty($restaurantPhones)) {
            return $this->sendError('Restaurant Phones not found');
        }

        return $this->sendResponse(new RestaurantPhonesResource($restaurantPhones), 'Restaurant Phones retrieved successfully');
    }

    /**
     * Update the specified RestaurantPhones in storage.
     * PUT/PATCH /restaurantPhones/{id}
     *
     * @param int $id
     * @param UpdateRestaurantPhonesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantPhonesAPIRequest $request)
    {
        $input = $request->all();

        /** @var RestaurantPhones $restaurantPhones */
        $restaurantPhones = $this->restaurantPhonesRepository->find($id);

        if (empty($restaurantPhones)) {
            return $this->sendError('Restaurant Phones not found');
        }

        $restaurantPhones = $this->restaurantPhonesRepository->update($input, $id);

        return $this->sendResponse(new RestaurantPhonesResource($restaurantPhones), 'RestaurantPhones updated successfully');
    }

    /**
     * Remove the specified RestaurantPhones from storage.
     * DELETE /restaurantPhones/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RestaurantPhones $restaurantPhones */
        $restaurantPhones = $this->restaurantPhonesRepository->find($id);

        if (empty($restaurantPhones)) {
            return $this->sendError('Restaurant Phones not found');
        }

        $restaurantPhones->delete();

        return $this->sendSuccess('Restaurant Phones deleted successfully');
    }
}
