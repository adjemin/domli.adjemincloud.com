<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePaymentMethodesAPIRequest;
use App\Http\Requests\API\UpdatePaymentMethodesAPIRequest;
use App\Models\PaymentMethodes;
use App\Repositories\PaymentMethodesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\PaymentMethodesResource;
use Response;

/**
 * Class PaymentMethodesController
 * @package App\Http\Controllers\API
 */

class PaymentMethodesAPIController extends AppBaseController
{
    /** @var  PaymentMethodesRepository */
    private $paymentMethodesRepository;

    public function __construct(PaymentMethodesRepository $paymentMethodesRepo)
    {
        $this->paymentMethodesRepository = $paymentMethodesRepo;
    }

    /**
     * Display a listing of the PaymentMethodes.
     * GET|HEAD /paymentMethodes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $paymentMethodes = $this->paymentMethodesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(PaymentMethodesResource::collection($paymentMethodes), 'Payment Methodes retrieved successfully');
    }

    /**
     * Store a newly created PaymentMethodes in storage.
     * POST /paymentMethodes
     *
     * @param CreatePaymentMethodesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentMethodesAPIRequest $request)
    {
        $input = $request->all();

        $paymentMethodes = $this->paymentMethodesRepository->create($input);

        return $this->sendResponse(new PaymentMethodesResource($paymentMethodes), 'Payment Methodes saved successfully');
    }

    /**
     * Display the specified PaymentMethodes.
     * GET|HEAD /paymentMethodes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PaymentMethodes $paymentMethodes */
        $paymentMethodes = $this->paymentMethodesRepository->find($id);

        if (empty($paymentMethodes)) {
            return $this->sendError('Payment Methodes not found');
        }

        return $this->sendResponse(new PaymentMethodesResource($paymentMethodes), 'Payment Methodes retrieved successfully');
    }

    /**
     * Update the specified PaymentMethodes in storage.
     * PUT/PATCH /paymentMethodes/{id}
     *
     * @param int $id
     * @param UpdatePaymentMethodesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentMethodesAPIRequest $request)
    {
        $input = $request->all();

        /** @var PaymentMethodes $paymentMethodes */
        $paymentMethodes = $this->paymentMethodesRepository->find($id);

        if (empty($paymentMethodes)) {
            return $this->sendError('Payment Methodes not found');
        }

        $paymentMethodes = $this->paymentMethodesRepository->update($input, $id);

        return $this->sendResponse(new PaymentMethodesResource($paymentMethodes), 'PaymentMethodes updated successfully');
    }

    /**
     * Remove the specified PaymentMethodes from storage.
     * DELETE /paymentMethodes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PaymentMethodes $paymentMethodes */
        $paymentMethodes = $this->paymentMethodesRepository->find($id);

        if (empty($paymentMethodes)) {
            return $this->sendError('Payment Methodes not found');
        }

        $paymentMethodes->delete();

        return $this->sendSuccess('Payment Methodes deleted successfully');
    }
}
