<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRestaurantMenusAPIRequest;
use App\Http\Requests\API\UpdateRestaurantMenusAPIRequest;
use App\Models\RestaurantMenus;
use App\Repositories\RestaurantMenusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\RestaurantMenusResource;
use Response;

/**
 * Class RestaurantMenusController
 * @package App\Http\Controllers\API
 */

class RestaurantMenusAPIController extends AppBaseController
{
    /** @var  RestaurantMenusRepository */
    private $restaurantMenusRepository;

    public function __construct(RestaurantMenusRepository $restaurantMenusRepo)
    {
        $this->restaurantMenusRepository = $restaurantMenusRepo;
    }

    /**
     * Display a listing of the RestaurantMenus.
     * GET|HEAD /restaurantMenuses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantMenuses = $this->restaurantMenusRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(RestaurantMenusResource::collection($restaurantMenuses), 'Restaurant Menuses retrieved successfully');
    }

    /**
     * Store a newly created RestaurantMenus in storage.
     * POST /restaurantMenuses
     *
     * @param CreateRestaurantMenusAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantMenusAPIRequest $request)
    {
        $input = $request->all();

        $restaurantMenus = $this->restaurantMenusRepository->create($input);

        return $this->sendResponse(new RestaurantMenusResource($restaurantMenus), 'Restaurant Menus saved successfully');
    }

    /**
     * Display the specified RestaurantMenus.
     * GET|HEAD /restaurantMenuses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RestaurantMenus $restaurantMenus */
        $restaurantMenus = $this->restaurantMenusRepository->find($id);

        if (empty($restaurantMenus)) {
            return $this->sendError('Restaurant Menus not found');
        }

        return $this->sendResponse(new RestaurantMenusResource($restaurantMenus), 'Restaurant Menus retrieved successfully');
    }

    /**
     * Update the specified RestaurantMenus in storage.
     * PUT/PATCH /restaurantMenuses/{id}
     *
     * @param int $id
     * @param UpdateRestaurantMenusAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantMenusAPIRequest $request)
    {
        $input = $request->all();

        /** @var RestaurantMenus $restaurantMenus */
        $restaurantMenus = $this->restaurantMenusRepository->find($id);

        if (empty($restaurantMenus)) {
            return $this->sendError('Restaurant Menus not found');
        }

        $restaurantMenus = $this->restaurantMenusRepository->update($input, $id);

        return $this->sendResponse(new RestaurantMenusResource($restaurantMenus), 'RestaurantMenus updated successfully');
    }

    /**
     * Remove the specified RestaurantMenus from storage.
     * DELETE /restaurantMenuses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RestaurantMenus $restaurantMenus */
        $restaurantMenus = $this->restaurantMenusRepository->find($id);

        if (empty($restaurantMenus)) {
            return $this->sendError('Restaurant Menus not found');
        }

        $restaurantMenus->delete();

        return $this->sendSuccess('Restaurant Menus deleted successfully');
    }
}
