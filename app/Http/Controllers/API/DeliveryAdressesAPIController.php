<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDeliveryAdressesAPIRequest;
use App\Http\Requests\API\UpdateDeliveryAdressesAPIRequest;
use App\Models\DeliveryAdresses;
use App\Repositories\DeliveryAdressesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\DeliveryAdressesResource;
use Response;

/**
 * Class DeliveryAdressesController
 * @package App\Http\Controllers\API
 */

class DeliveryAdressesAPIController extends AppBaseController
{
    /** @var  DeliveryAdressesRepository */
    private $deliveryAdressesRepository;

    public function __construct(DeliveryAdressesRepository $deliveryAdressesRepo)
    {
        $this->deliveryAdressesRepository = $deliveryAdressesRepo;
    }

    /**
     * Display a listing of the DeliveryAdresses.
     * GET|HEAD /deliveryAdresses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryAdresses = $this->deliveryAdressesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(DeliveryAdressesResource::collection($deliveryAdresses), 'Delivery Adresses retrieved successfully');
    }

    /**
     * Store a newly created DeliveryAdresses in storage.
     * POST /deliveryAdresses
     *
     * @param CreateDeliveryAdressesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryAdressesAPIRequest $request)
    {
        $input = $request->all();

        $deliveryAdresses = $this->deliveryAdressesRepository->create($input);

        return $this->sendResponse(new DeliveryAdressesResource($deliveryAdresses), 'Delivery Adresses saved successfully');
    }

    /**
     * Display the specified DeliveryAdresses.
     * GET|HEAD /deliveryAdresses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DeliveryAdresses $deliveryAdresses */
        $deliveryAdresses = $this->deliveryAdressesRepository->find($id);

        if (empty($deliveryAdresses)) {
            return $this->sendError('Delivery Adresses not found');
        }

        return $this->sendResponse(new DeliveryAdressesResource($deliveryAdresses), 'Delivery Adresses retrieved successfully');
    }

    /**
     * Update the specified DeliveryAdresses in storage.
     * PUT/PATCH /deliveryAdresses/{id}
     *
     * @param int $id
     * @param UpdateDeliveryAdressesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryAdressesAPIRequest $request)
    {
        $input = $request->all();

        /** @var DeliveryAdresses $deliveryAdresses */
        $deliveryAdresses = $this->deliveryAdressesRepository->find($id);

        if (empty($deliveryAdresses)) {
            return $this->sendError('Delivery Adresses not found');
        }

        $deliveryAdresses = $this->deliveryAdressesRepository->update($input, $id);

        return $this->sendResponse(new DeliveryAdressesResource($deliveryAdresses), 'DeliveryAdresses updated successfully');
    }

    /**
     * Remove the specified DeliveryAdresses from storage.
     * DELETE /deliveryAdresses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DeliveryAdresses $deliveryAdresses */
        $deliveryAdresses = $this->deliveryAdressesRepository->find($id);

        if (empty($deliveryAdresses)) {
            return $this->sendError('Delivery Adresses not found');
        }

        $deliveryAdresses->delete();

        return $this->sendSuccess('Delivery Adresses deleted successfully');
    }
}
