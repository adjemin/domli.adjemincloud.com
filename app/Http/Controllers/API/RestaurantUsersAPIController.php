<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRestaurantUsersAPIRequest;
use App\Http\Requests\API\UpdateRestaurantUsersAPIRequest;
use App\Models\RestaurantUsers;
use App\Repositories\RestaurantUsersRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\RestaurantUsersResource;
use Response;

/**
 * Class RestaurantUsersController
 * @package App\Http\Controllers\API
 */

class RestaurantUsersAPIController extends AppBaseController
{
    /** @var  RestaurantUsersRepository */
    private $restaurantUsersRepository;

    public function __construct(RestaurantUsersRepository $restaurantUsersRepo)
    {
        $this->restaurantUsersRepository = $restaurantUsersRepo;
    }

    /**
     * Display a listing of the RestaurantUsers.
     * GET|HEAD /restaurantUsers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantUsers = $this->restaurantUsersRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(RestaurantUsersResource::collection($restaurantUsers), 'Restaurant Users retrieved successfully');
    }

    /**
     * Store a newly created RestaurantUsers in storage.
     * POST /restaurantUsers
     *
     * @param CreateRestaurantUsersAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantUsersAPIRequest $request)
    {
        $input = $request->all();

        $restaurantUsers = $this->restaurantUsersRepository->create($input);

        return $this->sendResponse(new RestaurantUsersResource($restaurantUsers), 'Restaurant Users saved successfully');
    }

    /**
     * Display the specified RestaurantUsers.
     * GET|HEAD /restaurantUsers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RestaurantUsers $restaurantUsers */
        $restaurantUsers = $this->restaurantUsersRepository->find($id);

        if (empty($restaurantUsers)) {
            return $this->sendError('Restaurant Users not found');
        }

        return $this->sendResponse(new RestaurantUsersResource($restaurantUsers), 'Restaurant Users retrieved successfully');
    }

    /**
     * Update the specified RestaurantUsers in storage.
     * PUT/PATCH /restaurantUsers/{id}
     *
     * @param int $id
     * @param UpdateRestaurantUsersAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantUsersAPIRequest $request)
    {
        $input = $request->all();

        /** @var RestaurantUsers $restaurantUsers */
        $restaurantUsers = $this->restaurantUsersRepository->find($id);

        if (empty($restaurantUsers)) {
            return $this->sendError('Restaurant Users not found');
        }

        $restaurantUsers = $this->restaurantUsersRepository->update($input, $id);

        return $this->sendResponse(new RestaurantUsersResource($restaurantUsers), 'RestaurantUsers updated successfully');
    }

    /**
     * Remove the specified RestaurantUsers from storage.
     * DELETE /restaurantUsers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RestaurantUsers $restaurantUsers */
        $restaurantUsers = $this->restaurantUsersRepository->find($id);

        if (empty($restaurantUsers)) {
            return $this->sendError('Restaurant Users not found');
        }

        $restaurantUsers->delete();

        return $this->sendSuccess('Restaurant Users deleted successfully');
    }
}
