<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateJobHistoriqueAPIRequest;
use App\Http\Requests\API\UpdateJobHistoriqueAPIRequest;
use App\Models\JobHistorique;
use App\Repositories\JobHistoriqueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\JobHistoriqueResource;
use Response;

/**
 * Class JobHistoriqueController
 * @package App\Http\Controllers\API
 */

class JobHistoriqueAPIController extends AppBaseController
{
    /** @var  JobHistoriqueRepository */
    private $jobHistoriqueRepository;

    public function __construct(JobHistoriqueRepository $jobHistoriqueRepo)
    {
        $this->jobHistoriqueRepository = $jobHistoriqueRepo;
    }

    /**
     * Display a listing of the JobHistorique.
     * GET|HEAD /jobHistoriques
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $jobHistoriques = $this->jobHistoriqueRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(JobHistoriqueResource::collection($jobHistoriques), 'Job Historiques retrieved successfully');
    }

    /**
     * Store a newly created JobHistorique in storage.
     * POST /jobHistoriques
     *
     * @param CreateJobHistoriqueAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateJobHistoriqueAPIRequest $request)
    {
        $input = $request->all();

        $jobHistorique = $this->jobHistoriqueRepository->create($input);

        return $this->sendResponse(new JobHistoriqueResource($jobHistorique), 'Job Historique saved successfully');
    }

    /**
     * Display the specified JobHistorique.
     * GET|HEAD /jobHistoriques/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var JobHistorique $jobHistorique */
        $jobHistorique = $this->jobHistoriqueRepository->find($id);

        if (empty($jobHistorique)) {
            return $this->sendError('Job Historique not found');
        }

        return $this->sendResponse(new JobHistoriqueResource($jobHistorique), 'Job Historique retrieved successfully');
    }

    /**
     * Update the specified JobHistorique in storage.
     * PUT/PATCH /jobHistoriques/{id}
     *
     * @param int $id
     * @param UpdateJobHistoriqueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobHistoriqueAPIRequest $request)
    {
        $input = $request->all();

        /** @var JobHistorique $jobHistorique */
        $jobHistorique = $this->jobHistoriqueRepository->find($id);

        if (empty($jobHistorique)) {
            return $this->sendError('Job Historique not found');
        }

        $jobHistorique = $this->jobHistoriqueRepository->update($input, $id);

        return $this->sendResponse(new JobHistoriqueResource($jobHistorique), 'JobHistorique updated successfully');
    }

    /**
     * Remove the specified JobHistorique from storage.
     * DELETE /jobHistoriques/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var JobHistorique $jobHistorique */
        $jobHistorique = $this->jobHistoriqueRepository->find($id);

        if (empty($jobHistorique)) {
            return $this->sendError('Job Historique not found');
        }

        $jobHistorique->delete();

        return $this->sendSuccess('Job Historique deleted successfully');
    }
}
