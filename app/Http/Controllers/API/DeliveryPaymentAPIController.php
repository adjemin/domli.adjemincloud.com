<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDeliveryPaymentAPIRequest;
use App\Http\Requests\API\UpdateDeliveryPaymentAPIRequest;
use App\Models\DeliveryPayment;
use App\Repositories\DeliveryPaymentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\DeliveryPaymentResource;
use Response;

/**
 * Class DeliveryPaymentController
 * @package App\Http\Controllers\API
 */

class DeliveryPaymentAPIController extends AppBaseController
{
    /** @var  DeliveryPaymentRepository */
    private $deliveryPaymentRepository;

    public function __construct(DeliveryPaymentRepository $deliveryPaymentRepo)
    {
        $this->deliveryPaymentRepository = $deliveryPaymentRepo;
    }

    /**
     * Display a listing of the DeliveryPayment.
     * GET|HEAD /deliveryPayments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryPayments = $this->deliveryPaymentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(DeliveryPaymentResource::collection($deliveryPayments), 'Delivery Payments retrieved successfully');
    }

    /**
     * Store a newly created DeliveryPayment in storage.
     * POST /deliveryPayments
     *
     * @param CreateDeliveryPaymentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryPaymentAPIRequest $request)
    {
        $input = $request->all();

        $deliveryPayment = $this->deliveryPaymentRepository->create($input);

        return $this->sendResponse(new DeliveryPaymentResource($deliveryPayment), 'Delivery Payment saved successfully');
    }

    /**
     * Display the specified DeliveryPayment.
     * GET|HEAD /deliveryPayments/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DeliveryPayment $deliveryPayment */
        $deliveryPayment = $this->deliveryPaymentRepository->find($id);

        if (empty($deliveryPayment)) {
            return $this->sendError('Delivery Payment not found');
        }

        return $this->sendResponse(new DeliveryPaymentResource($deliveryPayment), 'Delivery Payment retrieved successfully');
    }

    /**
     * Update the specified DeliveryPayment in storage.
     * PUT/PATCH /deliveryPayments/{id}
     *
     * @param int $id
     * @param UpdateDeliveryPaymentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryPaymentAPIRequest $request)
    {
        $input = $request->all();

        /** @var DeliveryPayment $deliveryPayment */
        $deliveryPayment = $this->deliveryPaymentRepository->find($id);

        if (empty($deliveryPayment)) {
            return $this->sendError('Delivery Payment not found');
        }

        $deliveryPayment = $this->deliveryPaymentRepository->update($input, $id);

        return $this->sendResponse(new DeliveryPaymentResource($deliveryPayment), 'DeliveryPayment updated successfully');
    }

    /**
     * Remove the specified DeliveryPayment from storage.
     * DELETE /deliveryPayments/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DeliveryPayment $deliveryPayment */
        $deliveryPayment = $this->deliveryPaymentRepository->find($id);

        if (empty($deliveryPayment)) {
            return $this->sendError('Delivery Payment not found');
        }

        $deliveryPayment->delete();

        return $this->sendSuccess('Delivery Payment deleted successfully');
    }
}
