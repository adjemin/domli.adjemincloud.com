<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrderItemsAPIRequest;
use App\Http\Requests\API\UpdateOrderItemsAPIRequest;
use App\Models\OrderItems;
use App\Repositories\OrderItemsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\OrderItemsResource;
use Response;

/**
 * Class OrderItemsController
 * @package App\Http\Controllers\API
 */

class OrderItemsAPIController extends AppBaseController
{
    /** @var  OrderItemsRepository */
    private $orderItemsRepository;

    public function __construct(OrderItemsRepository $orderItemsRepo)
    {
        $this->orderItemsRepository = $orderItemsRepo;
    }

    /**
     * Display a listing of the OrderItems.
     * GET|HEAD /orderItems
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $orderItems = $this->orderItemsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(OrderItemsResource::collection($orderItems), 'Order Items retrieved successfully');
    }

    /**
     * Store a newly created OrderItems in storage.
     * POST /orderItems
     *
     * @param CreateOrderItemsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderItemsAPIRequest $request)
    {
        $input = $request->all();

        $orderItems = $this->orderItemsRepository->create($input);

        return $this->sendResponse(new OrderItemsResource($orderItems), 'Order Items saved successfully');
    }

    /**
     * Display the specified OrderItems.
     * GET|HEAD /orderItems/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var OrderItems $orderItems */
        $orderItems = $this->orderItemsRepository->find($id);

        if (empty($orderItems)) {
            return $this->sendError('Order Items not found');
        }

        return $this->sendResponse(new OrderItemsResource($orderItems), 'Order Items retrieved successfully');
    }

    /**
     * Update the specified OrderItems in storage.
     * PUT/PATCH /orderItems/{id}
     *
     * @param int $id
     * @param UpdateOrderItemsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderItemsAPIRequest $request)
    {
        $input = $request->all();

        /** @var OrderItems $orderItems */
        $orderItems = $this->orderItemsRepository->find($id);

        if (empty($orderItems)) {
            return $this->sendError('Order Items not found');
        }

        $orderItems = $this->orderItemsRepository->update($input, $id);

        return $this->sendResponse(new OrderItemsResource($orderItems), 'OrderItems updated successfully');
    }

    /**
     * Remove the specified OrderItems from storage.
     * DELETE /orderItems/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var OrderItems $orderItems */
        $orderItems = $this->orderItemsRepository->find($id);

        if (empty($orderItems)) {
            return $this->sendError('Order Items not found');
        }

        $orderItems->delete();

        return $this->sendSuccess('Order Items deleted successfully');
    }
}
