<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerDevicesAPIRequest;
use App\Http\Requests\API\UpdateCustomerDevicesAPIRequest;
use App\Models\CustomerDevices;
use App\Repositories\CustomerDevicesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\CustomerDevicesResource;
use Response;

/**
 * Class CustomerDevicesController
 * @package App\Http\Controllers\API
 */

class CustomerDevicesAPIController extends AppBaseController
{
    /** @var  CustomerDevicesRepository */
    private $customerDevicesRepository;

    public function __construct(CustomerDevicesRepository $customerDevicesRepo)
    {
        $this->customerDevicesRepository = $customerDevicesRepo;
    }

    /**
     * Display a listing of the CustomerDevices.
     * GET|HEAD /customerDevices
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $customerDevices = $this->customerDevicesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(CustomerDevicesResource::collection($customerDevices), 'Customer Devices retrieved successfully');
    }

    /**
     * Store a newly created CustomerDevices in storage.
     * POST /customerDevices
     *
     * @param CreateCustomerDevicesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerDevicesAPIRequest $request)
    {
        $input = $request->all();

        $customerDevices = CustomerDevices::where(['firebase_id' => $request->firebase_id])->get();

        if(collect($customerDevices)->count() == 0){
            $customerDevice = $this->customerDevicesRepository->create($input);
        }else{
            $customerDevice = $customerDevices[0];

            $customerDevice = $this->customerDevicesRepository->update($input, $customerDevice->id);
        }

        $customerDevices = CustomerDevices::where(['customer_id' => auth('deliverer_api')->user()->id ])->get();

        return $this->sendResponse($customerDevices->toArray(), 'Customer Devices saved successfully');
    }

    /**
     * Display the specified CustomerDevices.
     * GET|HEAD /customerDevices/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CustomerDevices $customerDevices */
        $customerDevices = $this->customerDevicesRepository->find($id);

        if (empty($customerDevices)) {
            return $this->sendError('Customer Devices not found');
        }

        return $this->sendResponse(new CustomerDevicesResource($customerDevices), 'Customer Devices retrieved successfully');
    }

    /**
     * Update the specified CustomerDevices in storage.
     * PUT/PATCH /customerDevices/{id}
     *
     * @param int $id
     * @param UpdateCustomerDevicesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerDevicesAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomerDevices $customerDevices */
        $customerDevices = $this->customerDevicesRepository->find($id);

        if (empty($customerDevices)) {
            return $this->sendError('Customer Devices not found');
        }

        $customerDevices = $this->customerDevicesRepository->update($input, $id);

        return $this->sendResponse(new CustomerDevicesResource($customerDevices), 'CustomerDevices updated successfully');
    }

    /**
     * Remove the specified CustomerDevices from storage.
     * DELETE /customerDevices/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CustomerDevices $customerDevices */
        $customerDevices = $this->customerDevicesRepository->find($id);

        if (empty($customerDevices)) {
            return $this->sendError('Customer Devices not found');
        }

        $customerDevices->delete();

        return $this->sendSuccess('Customer Devices deleted successfully');
    }
}
