<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVehicleTypesAPIRequest;
use App\Http\Requests\API\UpdateVehicleTypesAPIRequest;
use App\Models\VehicleTypes;
use App\Repositories\VehicleTypesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\VehicleTypesResource;
use Response;

/**
 * Class VehicleTypesController
 * @package App\Http\Controllers\API
 */

class VehicleTypesAPIController extends AppBaseController
{
    /** @var  VehicleTypesRepository */
    private $vehicleTypesRepository;

    public function __construct(VehicleTypesRepository $vehicleTypesRepo)
    {
        $this->vehicleTypesRepository = $vehicleTypesRepo;
    }

    /**
     * Display a listing of the VehicleTypes.
     * GET|HEAD /vehicleTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $vehicleTypes = $this->vehicleTypesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(VehicleTypesResource::collection($vehicleTypes), 'Vehicle Types retrieved successfully');
    }

    /**
     * Store a newly created VehicleTypes in storage.
     * POST /vehicleTypes
     *
     * @param CreateVehicleTypesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVehicleTypesAPIRequest $request)
    {
        $input = $request->all();

        $vehicleTypes = $this->vehicleTypesRepository->create($input);

        return $this->sendResponse(new VehicleTypesResource($vehicleTypes), 'Vehicle Types saved successfully');
    }

    /**
     * Display the specified VehicleTypes.
     * GET|HEAD /vehicleTypes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var VehicleTypes $vehicleTypes */
        $vehicleTypes = $this->vehicleTypesRepository->find($id);

        if (empty($vehicleTypes)) {
            return $this->sendError('Vehicle Types not found');
        }

        return $this->sendResponse(new VehicleTypesResource($vehicleTypes), 'Vehicle Types retrieved successfully');
    }

    /**
     * Update the specified VehicleTypes in storage.
     * PUT/PATCH /vehicleTypes/{id}
     *
     * @param int $id
     * @param UpdateVehicleTypesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVehicleTypesAPIRequest $request)
    {
        $input = $request->all();

        /** @var VehicleTypes $vehicleTypes */
        $vehicleTypes = $this->vehicleTypesRepository->find($id);

        if (empty($vehicleTypes)) {
            return $this->sendError('Vehicle Types not found');
        }

        $vehicleTypes = $this->vehicleTypesRepository->update($input, $id);

        return $this->sendResponse(new VehicleTypesResource($vehicleTypes), 'VehicleTypes updated successfully');
    }

    /**
     * Remove the specified VehicleTypes from storage.
     * DELETE /vehicleTypes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var VehicleTypes $vehicleTypes */
        $vehicleTypes = $this->vehicleTypesRepository->find($id);

        if (empty($vehicleTypes)) {
            return $this->sendError('Vehicle Types not found');
        }

        $vehicleTypes->delete();

        return $this->sendSuccess('Vehicle Types deleted successfully');
    }
}
