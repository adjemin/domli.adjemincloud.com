<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRestaurantCompteAPIRequest;
use App\Http\Requests\API\UpdateRestaurantCompteAPIRequest;
use App\Models\RestaurantCompte;
use App\Repositories\RestaurantCompteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\RestaurantCompteResource;
use Response;

/**
 * Class RestaurantCompteController
 * @package App\Http\Controllers\API
 */

class RestaurantCompteAPIController extends AppBaseController
{
    /** @var  RestaurantCompteRepository */
    private $restaurantCompteRepository;

    public function __construct(RestaurantCompteRepository $restaurantCompteRepo)
    {
        $this->restaurantCompteRepository = $restaurantCompteRepo;
    }

    /**
     * Display a listing of the RestaurantCompte.
     * GET|HEAD /restaurantComptes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantComptes = $this->restaurantCompteRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(RestaurantCompteResource::collection($restaurantComptes), 'Restaurant Comptes retrieved successfully');
    }

    /**
     * Store a newly created RestaurantCompte in storage.
     * POST /restaurantComptes
     *
     * @param CreateRestaurantCompteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantCompteAPIRequest $request)
    {
        $input = $request->all();

        $restaurantCompte = $this->restaurantCompteRepository->create($input);

        return $this->sendResponse(new RestaurantCompteResource($restaurantCompte), 'Restaurant Compte saved successfully');
    }

    /**
     * Display the specified RestaurantCompte.
     * GET|HEAD /restaurantComptes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RestaurantCompte $restaurantCompte */
        $restaurantCompte = $this->restaurantCompteRepository->find($id);

        if (empty($restaurantCompte)) {
            return $this->sendError('Restaurant Compte not found');
        }

        return $this->sendResponse(new RestaurantCompteResource($restaurantCompte), 'Restaurant Compte retrieved successfully');
    }

    /**
     * Update the specified RestaurantCompte in storage.
     * PUT/PATCH /restaurantComptes/{id}
     *
     * @param int $id
     * @param UpdateRestaurantCompteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantCompteAPIRequest $request)
    {
        $input = $request->all();

        /** @var RestaurantCompte $restaurantCompte */
        $restaurantCompte = $this->restaurantCompteRepository->find($id);

        if (empty($restaurantCompte)) {
            return $this->sendError('Restaurant Compte not found');
        }

        $restaurantCompte = $this->restaurantCompteRepository->update($input, $id);

        return $this->sendResponse(new RestaurantCompteResource($restaurantCompte), 'RestaurantCompte updated successfully');
    }

    /**
     * Remove the specified RestaurantCompte from storage.
     * DELETE /restaurantComptes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RestaurantCompte $restaurantCompte */
        $restaurantCompte = $this->restaurantCompteRepository->find($id);

        if (empty($restaurantCompte)) {
            return $this->sendError('Restaurant Compte not found');
        }

        $restaurantCompte->delete();

        return $this->sendSuccess('Restaurant Compte deleted successfully');
    }
}
