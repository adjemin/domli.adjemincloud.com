<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRestaurantPaymentAPIRequest;
use App\Http\Requests\API\UpdateRestaurantPaymentAPIRequest;
use App\Models\RestaurantPayment;
use App\Repositories\RestaurantPaymentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\RestaurantPaymentResource;
use Response;

/**
 * Class RestaurantPaymentController
 * @package App\Http\Controllers\API
 */

class RestaurantPaymentAPIController extends AppBaseController
{
    /** @var  RestaurantPaymentRepository */
    private $restaurantPaymentRepository;

    public function __construct(RestaurantPaymentRepository $restaurantPaymentRepo)
    {
        $this->restaurantPaymentRepository = $restaurantPaymentRepo;
    }

    /**
     * Display a listing of the RestaurantPayment.
     * GET|HEAD /restaurantPayments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantPayments = $this->restaurantPaymentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(RestaurantPaymentResource::collection($restaurantPayments), 'Restaurant Payments retrieved successfully');
    }

    /**
     * Store a newly created RestaurantPayment in storage.
     * POST /restaurantPayments
     *
     * @param CreateRestaurantPaymentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantPaymentAPIRequest $request)
    {
        $input = $request->all();

        $restaurantPayment = $this->restaurantPaymentRepository->create($input);

        return $this->sendResponse(new RestaurantPaymentResource($restaurantPayment), 'Restaurant Payment saved successfully');
    }

    /**
     * Display the specified RestaurantPayment.
     * GET|HEAD /restaurantPayments/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RestaurantPayment $restaurantPayment */
        $restaurantPayment = $this->restaurantPaymentRepository->find($id);

        if (empty($restaurantPayment)) {
            return $this->sendError('Restaurant Payment not found');
        }

        return $this->sendResponse(new RestaurantPaymentResource($restaurantPayment), 'Restaurant Payment retrieved successfully');
    }

    /**
     * Update the specified RestaurantPayment in storage.
     * PUT/PATCH /restaurantPayments/{id}
     *
     * @param int $id
     * @param UpdateRestaurantPaymentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantPaymentAPIRequest $request)
    {
        $input = $request->all();

        /** @var RestaurantPayment $restaurantPayment */
        $restaurantPayment = $this->restaurantPaymentRepository->find($id);

        if (empty($restaurantPayment)) {
            return $this->sendError('Restaurant Payment not found');
        }

        $restaurantPayment = $this->restaurantPaymentRepository->update($input, $id);

        return $this->sendResponse(new RestaurantPaymentResource($restaurantPayment), 'RestaurantPayment updated successfully');
    }

    /**
     * Remove the specified RestaurantPayment from storage.
     * DELETE /restaurantPayments/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RestaurantPayment $restaurantPayment */
        $restaurantPayment = $this->restaurantPaymentRepository->find($id);

        if (empty($restaurantPayment)) {
            return $this->sendError('Restaurant Payment not found');
        }

        $restaurantPayment->delete();

        return $this->sendSuccess('Restaurant Payment deleted successfully');
    }
}
