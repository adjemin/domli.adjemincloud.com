<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDeliveryCompteAPIRequest;
use App\Http\Requests\API\UpdateDeliveryCompteAPIRequest;
use App\Models\DeliveryCompte;
use App\Repositories\DeliveryCompteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\DeliveryCompteResource;
use Response;

/**
 * Class DeliveryCompteController
 * @package App\Http\Controllers\API
 */

class DeliveryCompteAPIController extends AppBaseController
{
    /** @var  DeliveryCompteRepository */
    private $deliveryCompteRepository;

    public function __construct(DeliveryCompteRepository $deliveryCompteRepo)
    {
        $this->deliveryCompteRepository = $deliveryCompteRepo;
    }

    /**
     * Display a listing of the DeliveryCompte.
     * GET|HEAD /deliveryComptes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryComptes = $this->deliveryCompteRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(DeliveryCompteResource::collection($deliveryComptes), 'Delivery Comptes retrieved successfully');
    }

    /**
     * Store a newly created DeliveryCompte in storage.
     * POST /deliveryComptes
     *
     * @param CreateDeliveryCompteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryCompteAPIRequest $request)
    {
        $input = $request->all();

        $deliveryCompte = $this->deliveryCompteRepository->create($input);

        return $this->sendResponse(new DeliveryCompteResource($deliveryCompte), 'Delivery Compte saved successfully');
    }

    /**
     * Display the specified DeliveryCompte.
     * GET|HEAD /deliveryComptes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DeliveryCompte $deliveryCompte */
        $deliveryCompte = $this->deliveryCompteRepository->find($id);

        if (empty($deliveryCompte)) {
            return $this->sendError('Delivery Compte not found');
        }

        return $this->sendResponse(new DeliveryCompteResource($deliveryCompte), 'Delivery Compte retrieved successfully');
    }

    /**
     * Update the specified DeliveryCompte in storage.
     * PUT/PATCH /deliveryComptes/{id}
     *
     * @param int $id
     * @param UpdateDeliveryCompteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryCompteAPIRequest $request)
    {
        $input = $request->all();

        /** @var DeliveryCompte $deliveryCompte */
        $deliveryCompte = $this->deliveryCompteRepository->find($id);

        if (empty($deliveryCompte)) {
            return $this->sendError('Delivery Compte not found');
        }

        $deliveryCompte = $this->deliveryCompteRepository->update($input, $id);

        return $this->sendResponse(new DeliveryCompteResource($deliveryCompte), 'DeliveryCompte updated successfully');
    }

    /**
     * Remove the specified DeliveryCompte from storage.
     * DELETE /deliveryComptes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DeliveryCompte $deliveryCompte */
        $deliveryCompte = $this->deliveryCompteRepository->find($id);

        if (empty($deliveryCompte)) {
            return $this->sendError('Delivery Compte not found');
        }

        $deliveryCompte->delete();

        return $this->sendSuccess('Delivery Compte deleted successfully');
    }
}
