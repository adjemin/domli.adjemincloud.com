<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDeliveryAvailaibityAPIRequest;
use App\Http\Requests\API\UpdateDeliveryAvailaibityAPIRequest;
use App\Models\DeliveryAvailaibity;
use App\Repositories\DeliveryAvailaibityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\DeliveryAvailaibityResource;
use Response;

/**
 * Class DeliveryAvailaibityController
 * @package App\Http\Controllers\API
 */

class DeliveryAvailaibityAPIController extends AppBaseController
{
    /** @var  DeliveryAvailaibityRepository */
    private $deliveryAvailaibityRepository;

    public function __construct(DeliveryAvailaibityRepository $deliveryAvailaibityRepo)
    {
        $this->deliveryAvailaibityRepository = $deliveryAvailaibityRepo;
    }

    /**
     * Display a listing of the DeliveryAvailaibity.
     * GET|HEAD /deliveryAvailaibities
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryAvailaibities = $this->deliveryAvailaibityRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(DeliveryAvailaibityResource::collection($deliveryAvailaibities), 'Delivery Availaibities retrieved successfully');
    }

    /**
     * Store a newly created DeliveryAvailaibity in storage.
     * POST /deliveryAvailaibities
     *
     * @param CreateDeliveryAvailaibityAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryAvailaibityAPIRequest $request)
    {
        $input = $request->all();

        $deliveryAvailaibity = $this->deliveryAvailaibityRepository->create($input);

        return $this->sendResponse(new DeliveryAvailaibityResource($deliveryAvailaibity), 'Delivery Availaibity saved successfully');
    }

    /**
     * Display the specified DeliveryAvailaibity.
     * GET|HEAD /deliveryAvailaibities/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DeliveryAvailaibity $deliveryAvailaibity */
        $deliveryAvailaibity = $this->deliveryAvailaibityRepository->find($id);

        if (empty($deliveryAvailaibity)) {
            return $this->sendError('Delivery Availaibity not found');
        }

        return $this->sendResponse(new DeliveryAvailaibityResource($deliveryAvailaibity), 'Delivery Availaibity retrieved successfully');
    }

    /**
     * Update the specified DeliveryAvailaibity in storage.
     * PUT/PATCH /deliveryAvailaibities/{id}
     *
     * @param int $id
     * @param UpdateDeliveryAvailaibityAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryAvailaibityAPIRequest $request)
    {
        $input = $request->all();

        /** @var DeliveryAvailaibity $deliveryAvailaibity */
        $deliveryAvailaibity = $this->deliveryAvailaibityRepository->find($id);

        if (empty($deliveryAvailaibity)) {
            return $this->sendError('Delivery Availaibity not found');
        }

        $deliveryAvailaibity = $this->deliveryAvailaibityRepository->update($input, $id);

        return $this->sendResponse(new DeliveryAvailaibityResource($deliveryAvailaibity), 'DeliveryAvailaibity updated successfully');
    }

    /**
     * Remove the specified DeliveryAvailaibity from storage.
     * DELETE /deliveryAvailaibities/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DeliveryAvailaibity $deliveryAvailaibity */
        $deliveryAvailaibity = $this->deliveryAvailaibityRepository->find($id);

        if (empty($deliveryAvailaibity)) {
            return $this->sendError('Delivery Availaibity not found');
        }

        $deliveryAvailaibity->delete();

        return $this->sendSuccess('Delivery Availaibity deleted successfully');
    }
}
