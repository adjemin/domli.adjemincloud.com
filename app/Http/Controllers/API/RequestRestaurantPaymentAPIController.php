<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRequestRestaurantPaymentAPIRequest;
use App\Http\Requests\API\UpdateRequestRestaurantPaymentAPIRequest;
use App\Models\RequestRestaurantPayment;
use App\Repositories\RequestRestaurantPaymentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\RequestRestaurantPaymentResource;
use Response;

/**
 * Class RequestRestaurantPaymentController
 * @package App\Http\Controllers\API
 */

class RequestRestaurantPaymentAPIController extends AppBaseController
{
    /** @var  RequestRestaurantPaymentRepository */
    private $requestRestaurantPaymentRepository;

    public function __construct(RequestRestaurantPaymentRepository $requestRestaurantPaymentRepo)
    {
        $this->requestRestaurantPaymentRepository = $requestRestaurantPaymentRepo;
    }

    /**
     * Display a listing of the RequestRestaurantPayment.
     * GET|HEAD /requestRestaurantPayments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $requestRestaurantPayments = $this->requestRestaurantPaymentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(RequestRestaurantPaymentResource::collection($requestRestaurantPayments), 'Request Restaurant Payments retrieved successfully');
    }

    /**
     * Store a newly created RequestRestaurantPayment in storage.
     * POST /requestRestaurantPayments
     *
     * @param CreateRequestRestaurantPaymentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRequestRestaurantPaymentAPIRequest $request)
    {
        $input = $request->all();

        $requestRestaurantPayment = $this->requestRestaurantPaymentRepository->create($input);

        return $this->sendResponse(new RequestRestaurantPaymentResource($requestRestaurantPayment), 'Request Restaurant Payment saved successfully');
    }

    /**
     * Display the specified RequestRestaurantPayment.
     * GET|HEAD /requestRestaurantPayments/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RequestRestaurantPayment $requestRestaurantPayment */
        $requestRestaurantPayment = $this->requestRestaurantPaymentRepository->find($id);

        if (empty($requestRestaurantPayment)) {
            return $this->sendError('Request Restaurant Payment not found');
        }

        return $this->sendResponse(new RequestRestaurantPaymentResource($requestRestaurantPayment), 'Request Restaurant Payment retrieved successfully');
    }

    /**
     * Update the specified RequestRestaurantPayment in storage.
     * PUT/PATCH /requestRestaurantPayments/{id}
     *
     * @param int $id
     * @param UpdateRequestRestaurantPaymentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRequestRestaurantPaymentAPIRequest $request)
    {
        $input = $request->all();

        /** @var RequestRestaurantPayment $requestRestaurantPayment */
        $requestRestaurantPayment = $this->requestRestaurantPaymentRepository->find($id);

        if (empty($requestRestaurantPayment)) {
            return $this->sendError('Request Restaurant Payment not found');
        }

        $requestRestaurantPayment = $this->requestRestaurantPaymentRepository->update($input, $id);

        return $this->sendResponse(new RequestRestaurantPaymentResource($requestRestaurantPayment), 'RequestRestaurantPayment updated successfully');
    }

    /**
     * Remove the specified RequestRestaurantPayment from storage.
     * DELETE /requestRestaurantPayments/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RequestRestaurantPayment $requestRestaurantPayment */
        $requestRestaurantPayment = $this->requestRestaurantPaymentRepository->find($id);

        if (empty($requestRestaurantPayment)) {
            return $this->sendError('Request Restaurant Payment not found');
        }

        $requestRestaurantPayment->delete();

        return $this->sendSuccess('Request Restaurant Payment deleted successfully');
    }
}
