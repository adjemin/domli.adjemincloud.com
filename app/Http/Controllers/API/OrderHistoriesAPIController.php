<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrderHistoriesAPIRequest;
use App\Http\Requests\API\UpdateOrderHistoriesAPIRequest;
use App\Models\OrderHistories;
use App\Repositories\OrderHistoriesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\OrderHistoriesResource;
use Response;

/**
 * Class OrderHistoriesController
 * @package App\Http\Controllers\API
 */

class OrderHistoriesAPIController extends AppBaseController
{
    /** @var  OrderHistoriesRepository */
    private $orderHistoriesRepository;

    public function __construct(OrderHistoriesRepository $orderHistoriesRepo)
    {
        $this->orderHistoriesRepository = $orderHistoriesRepo;
    }

    /**
     * Display a listing of the OrderHistories.
     * GET|HEAD /orderHistories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $orderHistories = $this->orderHistoriesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(OrderHistoriesResource::collection($orderHistories), 'Order Histories retrieved successfully');
    }

    /**
     * Store a newly created OrderHistories in storage.
     * POST /orderHistories
     *
     * @param CreateOrderHistoriesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderHistoriesAPIRequest $request)
    {
        $input = $request->all();

        $orderHistories = $this->orderHistoriesRepository->create($input);

        return $this->sendResponse(new OrderHistoriesResource($orderHistories), 'Order Histories saved successfully');
    }

    /**
     * Display the specified OrderHistories.
     * GET|HEAD /orderHistories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var OrderHistories $orderHistories */
        $orderHistories = $this->orderHistoriesRepository->find($id);

        if (empty($orderHistories)) {
            return $this->sendError('Order Histories not found');
        }

        return $this->sendResponse(new OrderHistoriesResource($orderHistories), 'Order Histories retrieved successfully');
    }

    /**
     * Update the specified OrderHistories in storage.
     * PUT/PATCH /orderHistories/{id}
     *
     * @param int $id
     * @param UpdateOrderHistoriesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderHistoriesAPIRequest $request)
    {
        $input = $request->all();

        /** @var OrderHistories $orderHistories */
        $orderHistories = $this->orderHistoriesRepository->find($id);

        if (empty($orderHistories)) {
            return $this->sendError('Order Histories not found');
        }

        $orderHistories = $this->orderHistoriesRepository->update($input, $id);

        return $this->sendResponse(new OrderHistoriesResource($orderHistories), 'OrderHistories updated successfully');
    }

    /**
     * Remove the specified OrderHistories from storage.
     * DELETE /orderHistories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var OrderHistories $orderHistories */
        $orderHistories = $this->orderHistoriesRepository->find($id);

        if (empty($orderHistories)) {
            return $this->sendError('Order Histories not found');
        }

        $orderHistories->delete();

        return $this->sendSuccess('Order Histories deleted successfully');
    }
}
