<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateInvoicePaymentAPIRequest;
use App\Http\Requests\API\UpdateInvoicePaymentAPIRequest;
use App\Models\InvoicePayment;
use App\Repositories\InvoicePaymentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class InvoicePaymentController
 * @package App\Http\Controllers\API
 */

class InvoicePaymentAPIController extends AppBaseController
{
    /** @var  InvoicePaymentRepository */
    private $invoicePaymentRepository;

    public function __construct(InvoicePaymentRepository $invoicePaymentRepo)
    {
        $this->invoicePaymentRepository = $invoicePaymentRepo;
    }

    /**
     * Display a listing of the InvoicePayment.
     * GET|HEAD /invoicePayments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $invoicePayments = $this->invoicePaymentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($invoicePayments->toArray(), 'Invoice Payments retrieved successfully');
    }

    /**
     * Store a newly created InvoicePayment in storage.
     * POST /invoicePayments
     *
     * @param CreateInvoicePaymentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInvoicePaymentAPIRequest $request)
    {
        $input = $request->all();

        $invoicePayment = $this->invoicePaymentRepository->create($input);

        return $this->sendResponse($invoicePayment->toArray(), 'Invoice Payment saved successfully');
    }

    /**
     * Display the specified InvoicePayment.
     * GET|HEAD /invoicePayments/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var InvoicePayment $invoicePayment */
        $invoicePayment = $this->invoicePaymentRepository->find($id);

        if (empty($invoicePayment)) {
            return $this->sendError('Invoice Payment not found');
        }

        return $this->sendResponse($invoicePayment->toArray(), 'Invoice Payment retrieved successfully');
    }

    /**
     * Update the specified InvoicePayment in storage.
     * PUT/PATCH /invoicePayments/{id}
     *
     * @param int $id
     * @param UpdateInvoicePaymentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInvoicePaymentAPIRequest $request)
    {
        $input = $request->all();

        /** @var InvoicePayment $invoicePayment */
        $invoicePayment = $this->invoicePaymentRepository->find($id);

        if (empty($invoicePayment)) {
            return $this->sendError('Invoice Payment not found');
        }

        $invoicePayment = $this->invoicePaymentRepository->update($input, $id);

        return $this->sendResponse($invoicePayment->toArray(), 'InvoicePayment updated successfully');
    }

    /**
     * Remove the specified InvoicePayment from storage.
     * DELETE /invoicePayments/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var InvoicePayment $invoicePayment */
        $invoicePayment = $this->invoicePaymentRepository->find($id);

        if (empty($invoicePayment)) {
            return $this->sendError('Invoice Payment not found');
        }

        $invoicePayment->delete();

        return $this->sendSuccess('Invoice Payment deleted successfully');
    }
}
