<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrderDeliveriesAPIRequest;
use App\Http\Requests\API\UpdateOrderDeliveriesAPIRequest;
use App\Models\OrderDeliveries;
use App\Repositories\OrderDeliveriesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\OrderDeliveriesResource;
use Response;

/**
 * Class OrderDeliveriesController
 * @package App\Http\Controllers\API
 */

class OrderDeliveriesAPIController extends AppBaseController
{
    /** @var  OrderDeliveriesRepository */
    private $orderDeliveriesRepository;

    public function __construct(OrderDeliveriesRepository $orderDeliveriesRepo)
    {
        $this->orderDeliveriesRepository = $orderDeliveriesRepo;
    }

    /**
     * Display a listing of the OrderDeliveries.
     * GET|HEAD /orderDeliveries
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $orderDeliveries = $this->orderDeliveriesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(OrderDeliveriesResource::collection($orderDeliveries), 'Order Deliveries retrieved successfully');
    }

    /**
     * Store a newly created OrderDeliveries in storage.
     * POST /orderDeliveries
     *
     * @param CreateOrderDeliveriesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderDeliveriesAPIRequest $request)
    {
        $input = $request->all();

        $orderDeliveries = $this->orderDeliveriesRepository->create($input);

        return $this->sendResponse(new OrderDeliveriesResource($orderDeliveries), 'Order Deliveries saved successfully');
    }

    /**
     * Display the specified OrderDeliveries.
     * GET|HEAD /orderDeliveries/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var OrderDeliveries $orderDeliveries */
        $orderDeliveries = $this->orderDeliveriesRepository->find($id);

        if (empty($orderDeliveries)) {
            return $this->sendError('Order Deliveries not found');
        }

        return $this->sendResponse(new OrderDeliveriesResource($orderDeliveries), 'Order Deliveries retrieved successfully');
    }

    /**
     * Update the specified OrderDeliveries in storage.
     * PUT/PATCH /orderDeliveries/{id}
     *
     * @param int $id
     * @param UpdateOrderDeliveriesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderDeliveriesAPIRequest $request)
    {
        $input = $request->all();

        /** @var OrderDeliveries $orderDeliveries */
        $orderDeliveries = $this->orderDeliveriesRepository->find($id);

        if (empty($orderDeliveries)) {
            return $this->sendError('Order Deliveries not found');
        }

        $orderDeliveries = $this->orderDeliveriesRepository->update($input, $id);

        return $this->sendResponse(new OrderDeliveriesResource($orderDeliveries), 'OrderDeliveries updated successfully');
    }

    /**
     * Remove the specified OrderDeliveries from storage.
     * DELETE /orderDeliveries/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var OrderDeliveries $orderDeliveries */
        $orderDeliveries = $this->orderDeliveriesRepository->find($id);

        if (empty($orderDeliveries)) {
            return $this->sendError('Order Deliveries not found');
        }

        $orderDeliveries->delete();

        return $this->sendSuccess('Order Deliveries deleted successfully');
    }
}
