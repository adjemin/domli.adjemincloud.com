<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Mail;
use App\Http\Requests\API\CreateDeliveryAPIRequest;
use App\Http\Requests\API\UpdateDeliveryAPIRequest;
use App\Models\Delivery;
use App\Models\Vehicle;
use App\Models\AgentConnexion;
use App\Models\OrderAssignement;
use App\Models\DeliveryPayment;
use App\Models\RestaurantPayment;
use App\Models\RequestDeliveryPayment;
use App\User;
use Carbon\Carbon;
use Tymon\JWTAuth\Providers\Auth\Illuminate;
use App\Models\VehicleTypes;
use App\Mail\VerifyDeliveryMail;
use App\Mail\PasswordDeliveryMail;
use App\Mail\DelivererUpdateMail;
use App\VerifyDeliverer;
use App\Models\Orders;
use App\Models\Job;
use App\Models\JobHistorique;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Repositories\DeliveryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\DeliveryResource;
use Response;
use Hash;
use Imgur;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Mail\DelivererRegisterMail;

/**
 * Class DeliveryController
 * @package App\Http\Controllers\API
 */

class DeliveryAPIController extends AppBaseController
{
    /** @var  DeliveryRepository */
    private $deliveryRepository;

    public function __construct(DeliveryRepository $deliveryRepo)
    {
        $this->deliveryRepository = $deliveryRepo;
    }

    /**
     * Display a listing of the Delivery.
     * GET|HEAD /deliveries
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveries = $this->deliveryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        // $this->middleware([])

        return $this->sendResponse(DeliveryResource::collection($deliveries), 'Deliveries retrieved successfully');
    }

    /**
     * Store a newly created Delivery in storage.
     * POST /deliveries
     *
     * @param CreateDeliveryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryAPIRequest $request)
    {
        // $deliverer = Delivery::latest()->first();

        // Mail::to($deliverer)->send(new VerifyDeliveryMail($deliverer));
        
        // dd("successfully");

        /**
         * for get data from "form" or "json"
         */
        $input = $request->all() ?? $request->json()->all();
       
        /**
         * required fields
         */
        $fields = ['first_name', 'last_name', 'email','password', 'dial_code', 'phone_number', 'vehicle_type_id', 'numero_police', 'date_start', 'date_end', 'vehicule_name', 'numero_serie', 'immatriculation'];

        /**
         * check the list of fields
         */
        $results = $this->checkExists($input, $fields);

        if(!$results[0]){
            return $this->sendError($results[1] .' is required');
        }

        /**
         * check unique using of email
         */
        if($this->dynamicCall('Delivery', ['email' => $input['email']])->count() >= 1 ){
            return $this->sendError('Email is already used');
        }

        /**
         * handle error 
         */
        // if(!$results[0]){
        //     return $this->sendError($results[1] .' is required');
        // }

        /**
         * check if vehicleType exist in database
         */

        $vehicleType = VehicleTypes::find(intval($input['vehicle_type_id']));

        /**
         * send error when we catch it
         */
        if(empty($vehicleType) || is_null($vehicleType)){
            return $this->sendError('Vehicle type not found');
        }

        if($request->hasFile('photo')){
            $url = Imgur::upload($request->file('photo'));
        }else{
            $url = "https://www.searchpng.com/wp-content/uploads/2019/02/Deafult-Profile-Pitcher.png";
        }

        /**
         * create deliverer after validation
         */
        $deliverer = Delivery::create([
            'is_connected'  =>  1,
            'first_name'  => $input['first_name'],
            'last_name'  => $input['last_name'],
            'name'  =>  $input['first_name'].' '.$input['last_name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            'dial_code' => $input['dial_code'],
            'phone_number' => $input['phone_number'],
            'phone' => $input['dial_code'].''.$input['phone_number'],
            'email_verified'    => 0,
            'photo'    => $url,
            'email_verified_at' => null,
            'is_active' => 0
        ]);

        /**
         * create token for verification
         */
        if(is_null($deliverer->verifyDeliverer) || empty($deliverer->verifyDeliverer)){
            $verify = VerifyDeliverer::create([
                'deliverer_id' => $deliverer->id,
                'token' => substr(uniqid(),0,6)
            ]);

            /**
             * sending mail
             */
            Mail::to($deliverer)->send(new VerifyDeliveryMail($deliverer, $verify));
            // return $this->sendResponse($this->prepareResponseToSend($deliverer, "login"), 'Delivery saved successfully');
        }
        

        /**
         * create vehicle
         */
        $vehicle = Vehicle::create([
            'is_active' => 0,
            'is_verified' => 0,
            'deliverer_id' => $deliverer->id,
            'vehicleType_id' => $vehicleType->id,
            'numero_police' => $input['numero_police'],
            'date_start' => $input['date_start'],
            'date_end' => $input['date_end'],
            'vehicule_name' =>  $input['vehicule_name'],
            'numero_serie'  =>  $input['numero_serie'],
            'immatriculation'   =>  $input['immatriculation']
        ]);

        // dd($vehicle);

        // $delivery = $this->deliveryRepository->create($input);
        /**
         * send data
         */
        // return $this->sendResponse($deliverer->toArray(), 'Delivery saved successfully');
        return $this->sendResponse($this->prepareResponseToSend($deliverer, "login"), 'Delivery saved successfully');
    }

    public function checkToken(Request $request){
        $field = $request->json()->all() ?? $request->all();

        $verifyDeliverer = \App\VerifyDeliverer::where([
            'deliverer_id' => $field['deliverer_id'],
            'token' => $field['token']
        ])->first();
        
        $deliverer = $verifyDeliverer->deliverer;
        if(isset($deliverer)) {
            $deliverer->email_verified = 1;
            $deliverer->email_verified_at = now();
            $deliverer->save();
            // $status = "Your e-mail is verified. You can now login.";
        }   

        $users = \App\User::all();
        foreach ($users as $user) {
            Mail::to($user)->send(new DelivererRegisterMail($deliverer));
        }

        return $this->sendResponse($this->prepareResponseToSend($deliverer, "login"), 'Delivery saved successfully');
    }


    public function checkUser(Request $request){
        /**
         * for get data from "form" or "json"
         */
        $input = $request->all() ?? $request->json()->all();
       
        /**
         * required fields
         */
        $fields = ['email', 'dial_code','phone_number'];

        /**
         * check the list of fields
         */
        $results = $this->checkExists($input, $fields);

        /**
         * handle error 
         */
        if(!$results[0]){
            return $this->sendError($results[1] .' is required');
        }

        $userCount = \App\Models\Delivery::where([
            'email' => $input['email']
        ])->orWhere([
            'phone' => $input['dial_code'].''.$input['phone_number']
        ])->count();

        return $this->sendResponse($userCount > 0, 'User check successfully');
    }

    public function login(Request $request){
        /**
         * for get data from "form" or "json"
         */
        $input = $request->all() ?? $request->json()->all();
       
        /**
         * required fields
         */
        $fields = ['email', 'password'];

        /**
         * check the list of fields
         */
        $results = $this->checkExists($input, $fields);

        /**
         * handle error 
         */
        if(!$results[0]){
            return $this->sendError($results[1] .' is required');
        }

        /**
         * get the current delivery
         */
        $deliverer = $this->dynamicCall('Delivery', ['email' => $input['email']])->first();

        /**
         * handle error
         */
        if(is_null($deliverer) || empty($deliverer)){
            return $this->sendError("Deliverer not found");
        }

        // if(!$deliverer->email_verified){
        //     return $this->sendError('You should verified your email before');
        // }

        // if(!$deliverer->is_active){
        //     return $this->sendError('Wait until an administrator confirm your account');
        // }

        if (!Hash::check($input['password'], $deliverer->password)) {
            return $this->sendError('The provided password does not match our records.');
        }

        $deliverer->is_connected = 1;
        $deliverer->save();

        /**
         * prepare data to return to mobile app
         */
        $vehicle = $deliverer->vehicle();
        
        if($vehicle->is_expired()){
            $vehicle->is_verified = 0;
            $vehicle->save();
        }

        /**
         * send data
         */
        return $this->sendResponse($this->prepareResponseToSend($deliverer, "login"), 'Delivery saved successfully');

    }

    public function logout(Request $request){
        try {
            JWTAuth::invalidate($request->token);
            auth('deliverer_api')->logout();
            return $this->sendSuccess("User logged out successfully");
        } catch (JWTException $exception) {
            return $this->sendError("Sorry, the user cannot be logged out");
        }
    }
    /**
     * Display the specified Delivery.
     * GET|HEAD /deliveries/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Delivery $delivery */
        $delivery = $this->deliveryRepository->find($id);

        if (empty($delivery)) {
            return $this->sendError('Delivery not found');
        }

        return $this->sendResponse(new DeliveryResource($delivery), 'Delivery retrieved successfully');
    }

    public function getNotifications(Request $request, $userId){
        $notifications = \App\Models\DeliveryNotification::where('delivery_id', $userId)->get();

        return $this->sendResponse($notifications->toArray(), 'Notifications retrieved successfully');
    }

    public function putNotifications(Request $request, $userId){
        $notification = \App\Models\DeliveryNotification::where('delivery_id', $userId)->first();

        $notification->update($request->all());

        return $this->sendResponse($notification->toArray(), 'Notification retrieved successfully');
    }

    /**
     * Update the specified Delivery in storage.
     * PUT/PATCH /deliveries/{id}
     *
     * @param int $id
     * @param UpdateDeliveryAPIRequest $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $input = $request->all() ?? $request->json()->all();

        /** @var Delivery $delivery */
        // $delivery = $this->deliveryRepository->find($id);
        $delivery = auth('deliverer_api')->user();

        if (empty($delivery)) {
            return $this->sendError('Delivery not found');
        }
       
        /**
         * required fields
         */
        // dd($user = JWTAuth::parseToken()->user());
        // dd($user = auth('deliverer_api')->authenticate());
        $fields = ['first_name', 'last_name', 'email',
        //  'photo',
        //  'password', 
         'dial_code', 'phone_number', 
        //  'vehicle_type_id', 
         'numero_police',
        //  'date_start', 'date_end', 
         'vehicule_name', 'numero_serie', 'immatriculation'];

        /**
         * check the list of fields
         */
        $results = $this->checkExists($input, $fields);

        /**
         * handle error 
         */
        if(!$results[0]){
            return $this->sendError($results[1] .' is required');
        }

        /**
         * check if vehicleType exist in database
         */
        // $vehicleType = VehicleTypes::find(intval($input['vehicle_type_id']));

        // /**
        //  * send error when we catch it
        //  */
        // if(empty($vehicleType) || is_null($vehicleType)){
        //     return $this->sendError('Vehicle type not found');
        // }

        $delivery->first_name = $input['first_name'] ?? $delivery->first_name;
        $delivery->last_name = $input['last_name'] ?? $delivery->last_name;
        $delivery->name = $input['first_name'].' '.$input['last_name'] ?? $delivery->name;
        $delivery->email = $input['email'] ?? $delivery->email;
        // $delivery->password = Hash::make($input['password']) ?? $delivery->password;
        $delivery->dial_code = $input['dial_code'] ?? $delivery->dial_code;
        $delivery->phone_number = $input['phone_number'] ?? $delivery->phone_number;
        $delivery->phone = $input['phone'] ?? $delivery->phone;

        // if($request->file('photo')){
        //     $url = Imgur::upload($request->file('photo'));
        //     $delivery->photo =  $url->link();
        // }else{
        //     $delivery->photo =  $request->photo ?? $delivery->photo;
        // }

        $delivery->save();

        $change = 0;

        // if($delivery->vehicle->vehicleType_id != $input['vehicle_type_id']){
        //     $delivery->vehicle->vehicleType_id = $input['vehicle_type_id'];
        //     $change++;
        // }

        if($delivery->vehicle->numero_police != $input['numero_police']){
            $delivery->vehicle->numero_police = $input['numero_police'];
            $change++;
        }

        // if($delivery->vehicle->date_start != $input['date_start']){
        //     $delivery->vehicle->date_start = $input['date_start'];
        //     $change++;
        // }

        // if($delivery->vehicle->date_end != $input['date_end']){
        //     $delivery->vehicle->date_end = $input['date_end'];
        //     $change++;
        // }

        if($delivery->vehicle->vehicule_name != $input['vehicule_name']){
            $delivery->vehicle->vehicule_name = $input['vehicule_name'];
            $change++;
        }

        if($delivery->vehicle->numero_serie != $input['numero_serie']){
            $delivery->vehicle->numero_serie = $input['numero_serie'];
            $change++;
        }

        if($delivery->vehicle->immatriculation != $input['immatriculation']){
            $delivery->vehicle->immatriculation = $input['immatriculation'];
            $change++;
        }
        
        if($change >= 1){
            $delivery->vehicle->is_verified = 0;
            $delivery->vehicle->save();

            $users = User::all();
            foreach ($users as $user) {
                Mail::to($user)->send(new DelivererUpdateMail($deliverer));
            }
        }
        
        return $this->sendResponse($this->prepareResponseToSend($delivery), 'Delivery updated successfully');
    }

    public function changeOnlineStatus(Request $request) {
        // $deliverer = $this->deliveryRepository->find($id);
        $deliverer = auth('deliverer_api')->authenticate();

        // if($deliverer->is_connected != $request->is_connected){
            if($request->is_connected){
                $deliverer->lat = $request->lat ?? $deliverer->lat;
                $deliverer->lng = $request->lon ?? $deliverer->lng;
            }

            $deliverer->is_connected = $request->is_connected;
            $deliverer->save();
        // }


        $agentConnexion = AgentConnexion::where(['deliverer_id' => $deliverer->id])->first();
        if(is_null($agentConnexion) || empty($agentConnexion)){
            AgentConnexion::create([
                'deliverer_id' => $deliverer->id,
                'location_lat' => $request->lat ?? $deliverer->lat,
                'location_lng' => $request->lon ?? $deliverer->lng,
                'location_name' => '',
                'is_active' => $request->is_connected
            ]);
        }else{
            $agentConnexion->location_lat = $request->lat ?? $deliverer->lat;
            $agentConnexion->location_lng = $request->lng ?? $deliverer->lng;
            $agentConnexion->location_lat = $request->lat ?? $deliverer->lat;
            $agentConnexion->is_active = $request->is_connected;
            $agentConnexion->save();
        }

        return $this->sendResponse($this->prepareResponseToSend($deliverer), "User status has changed");
    }


    public function deliveryAssignement(Request $request){
        // $delivery = $this->deliveryRepository->find($id);
        $delivery = auth('deliverer_api')->user();

        $orderAssignements = $this->dynamicCall('OrderAssignement', ['deliverer_id' => $delivery->id])->get();

        return $this->sendResponse($orderAssignements->toArray(), 'Order assignements retrieved successfully');
    }

    public function deliveryAssignementAccepted(Request $request){
        // $delivery = $this->deliveryRepository->find($id);
        $delivery = auth('deliverer_api')->user();

        $orderAssignements = $this->dynamicCall('OrderAssignement', ['deliverer_id' => $delivery->id, 'is_waiting_acceptation' => 1])->get();

        return $this->sendResponse($orderAssignements->toArray(), 'Order assignements retrieved successfully');
    }

    public function acceptOrRejectAssignement(Request $request, $id){
        $delivery = auth('deliverer_api')->user();
        $orderAssignement = $this->dynamicCall('OrderAssignement', ['id' => $id])->first();
        
        if(empty($orderAssignement) || is_null($orderAssignement)){
            return $this->sendError('Assignement not found', 404);
        }

        $orderAssignmentAcceptedList = \App\Models\OrderAssignement::where(['order_id' => $orderAssignement->order_id])->whereNotNull('acceptation_time')->get(); 
        
        $notAcceptedYet =  \App\Models\OrderAssignement::where(['order_id' => $orderAssignement->order_id, 'is_waiting_acceptation', true])->get();
        if(!empty($orderAssignmentAcceptedList)){
            foreach ($notAcceptedYet as $value) {
                $value->is_waiting_acceptation = false;
                $value->save();
            }
            
            return $this->sendError('This order is already accept by another delivery', 400);
        }

        $job = null;
        $JobHistorique = null;
        
        if(!is_null($orderAssignement->rejection_time) || !is_null($orderAssignement->acceptation_time)){
            return $this->sendError('Assignment already processed !', 400);
        }

        if(!empty($request->deliver_response) && $request->deliver_response == 'accepted'){
            $orderAssignement->order->is_waiting_delivery = false;
            $orderAssignement->order->is_waiting = false;
            $orderAssignement->order->is_order_delivered_completed = false;
            $orderAssignement->order->order_delivered_status = Orders::WAITING_RESTAURANT_PICKUP;
            $orderAssignement->order->order_delivered_historique = Job::PENDING;
            $orderAssignement->order->current_status = Orders::WAITING_RESTAURANT_PICKUP;
            $orderAssignement->order->delivery_id = $delivery->id;
            $orderAssignement->order->save();
            
            $orderAssignement->acceptation_time = now();
                    

            $oldJob = Job::where(['order_id' => $orderAssignement->order->id])->latest()->first();

            $job = Job::create([
                'order_id'  => $orderAssignement->order->id,
                'job_code'  => 1,
                'location_name' => !is_null($oldJob) ? ($oldJob->is_pickup ? $orderAssignement->order->restaurant->restaurant_adress_name : '') : $orderAssignement->order->restaurant->restaurant_adress_name,
                'location_description' => '',
                'deliverer_id' => $delivery->id,
                'location_lat' => !is_null($oldJob) ? ($oldJob->is_pickup ? $orderAssignement->order->restaurant->lat : $orderAssignement->delivery->lat) : $orderAssignement->order->restaurant->lat,
                'location_lng' => !is_null($oldJob) ? ($oldJob->is_pickup ? $orderAssignement->order->restaurant->lng : $orderAssignement->delivery->lng) : $orderAssignement->order->restaurant->lng,
                'is_pickup' => is_null($oldJob) ? true : $oldJob->is_pickup,
                'status' => Job::PENDING,
                'signature_url' => is_null($oldJob) ? '' : $oldJob->signature_url,
                'images' => is_null($oldJob) ? '' : $oldJob->images,
                'notes' => is_null($oldJob) ? '' : $oldJob->notes,
                'note_audio' => is_null($oldJob) ? '' : $oldJob->note_audio,
                'before_date' => $orderAssignement->order->delivery_date
            ]);

            $JobHistorique = JobHistorique::create([
                'job_id' => $job->id,
                'status' => $job->status
            ])->toArray();

            $orderAssignements = $this->dynamicCall('OrderAssignement', ['order_id' => $orderAssignement->order->id, 'is_waiting_acceptation' => true])->get();
            
            foreach($orderAssignements as $assignement){
                // $assignement->rejection_time = now();
                $assignement->is_waiting_acceptation = false;
                $assignement->save();
            }
            
            $job = $job->toArray();
        }else{
            $orderAssignement->rejection_time = now();
        }

        $orderAssignement->is_waiting_acceptation = false;
        $orderAssignement->save();

        $order_assignments = OrderAssignement::where(['deliverer_id' =>  $orderAssignement->deliverer_id, 'is_waiting_acceptation' => true])->get();

        $data = [
            'job'   => $job,
            'jobHistorique' => $JobHistorique,
            'orderAssignement'  => $orderAssignement->toArray(),
            'order_assignments' => $order_assignments
        ];

        return $this->sendResponse($data, 'Order assignement update successfully');
    }

    public function getOrderDetails(Request $request, $order_id){
        $order = \App\Models\Orders::find($order_id);

        if( is_null($order) || empty($order)){
            return $this->sendError("Order not found");
        }

        $response = [
            'restaurant' => $order->restaurant->toArray(),
            'customer' => $order->customer->toArray(),
            'items' => $order->items->toArray(),
            'jobs' => \App\Models\Job::where('order_id', $order->id)->get()->toArray()
        ];

        return $this->sendResponse($response, 'Order detail retrieved successfully');
    }

    public function changeJobStatus(Request $request, $id){
        $delivery = auth('deliverer_api')->user();
        $job = $this->dynamicCall('Job', ['id' => $id, 'deliverer_id' => $delivery->id])->first();

        if(empty($job) || is_null($job)){
            return $this->sendError('Job not found');
        }

        $job->status = $request->status;
        $job->job_code = $request->job_code;

        if($request->file('images')){
            $images_url = [];
            $images = $request->file('images');

            foreach($images as $image){
                $url = Imgur::upload($image);
                array_push($images_url, $url->link());
            }

            $job->images = json_encode($images_url) ?? '';
        }

        if($request->file('note_audio')){
            $notes_url = [];
            $audios = $request->file('note_audio');

            foreach($audios as $audio){
                $audio_url = \Storage::disk('public')->put('note_audio/'.$job->deliverer_id,$audio);
                array_push($audios, $audio_url);
            }

            $job->note_audio = json_encode($audios) ?? '';
        }

        if($request->file('signature_url')){
            $signature_url = Imgur::upload($request->file('signature_url'));
            $job->signature_url = $signature_url->link() ?? '';
        }

        $job->notes = json_encode($request->notes) ?? '';
        $job->save();

        $JobHistorique = JobHistorique::create([
            'job_id' => $job->id,
            'status' => $job->status
        ]);

        if($job->status == Job::FAILED){
            
        }

        if($job->status == Job::CANCELED){
            $deliveries = Delivery::where('id','!=', $delivery->id)->where('is_connected', 1)->geofence($job->is_pickup ? $job->order->restaurant->lat : $delivery->lat, $job->is_pickup ? $job->order->restaurant->lng : $delivery->lng, 0, 15)->get();

            if(collect($deliveries)->count() == 0){
                $deliveries = Delivery::where('id','!=', $delivery->id)->where('is_connected', 1)->distance($job->is_pickup ? $job->order->restaurant->lat : $delivery->lat, $job->is_pickup ? $job->order->restaurant->lng : $delivery->lng)->orderByDesc('distance')->take(3)->get();    
            }

            foreach($deliveries as $delivery){
                $orderAssignement = OrderAssignement::create([
                    'order_id' => $job->order_id,
                    'deliverer_id' => $delivery->id,
                    'is_waiting_acceptation' => true
                ]);

                $notification = \App\Models\DeliveryNotification::create([
                    'title' => 'New assignement',
                    'subtitle' => 'You have been assigned to do a new task',
                    'action' => '#',
                    'action_by' => '#',
                    'meta_data_id' => $orderAssignement->id,
                    'meta_data' => json_encode($orderAssignement),
                    'type_notification' => '',
                    'is_read' => 0,
                    'is_received' => 0,
                    'data' => 'string',
                    'delivery_id' => $delivery->id,
                    'data_id' => $orderAssignement->id,
                    'data'  =>  json_encode($orderAssignement)
                ]);

                \App\Utils\DeliveryMessagingUtils::notify($notification);
            }

            $order = Orders::find($job->order_id);
            $order->is_waiting = true;
            $order->save();
        }

        $livraison = null;

        if($job->status == Job::SUCCESSFULLY){
            if($job->is_pickup){
                $order = Orders::find($job->order_id);
                $order->current_status =  Orders::CUSTOMER_PICKED;
                $order->save();
                
                $livraison = Job::create([
                    'order_id'  => $job->order_id,
                    'job_code'  => 1,
                    'location_name' => $job->order->location_name,
                    'location_description' => '',
                    'deliverer_id' => $job->deliverer_id,
                    'location_lat' => $job->order->location_lat,
                    'location_lng' => $job->order->location_lng,
                    'is_pickup' => 0,
                    'status' => Job::PENDING,
                    'signature_url' => '',
                    'images' => $job->images ?? '',
                    'notes' => $job->notes ?? '',
                    'note_audio' => $job->audio ?? '',
                    'before_date' => $job->order->delivery_date
                ]);

                $JobHistorique = JobHistorique::create([
                    'job_id' => $job->id,
                    'status' => $job->status
                ]);
            }else{
                $order = Orders::find($job->order_id);
                $order->current_status =  Orders::CUSTOMER_DELIVERED;
                $order->save();

                RestaurantPayment::create([
                    'restaurant_id' => $order->restaurant_id,
                    'amount' => $order->amount,
                    'is_paid' => 0,
                    'paid_date' => null
                ]);

                $currentRestaurant = \App\Models\RestaurantCompte::where('restaurant_id', $order->restaurant_id)->first();

                if(is_null($currentRestaurant) || empty($currentRestaurant)){
                    $paymentRestaurants = RestaurantPayment::where(['restaurant_id' => $order->restaurant_id])->get();

                    $sum = collect($paymentRestaurants)->reduce(function($carry, $item){
                        return $item->amount + $carry;
                    }, 0);
                    
                    \App\Models\RestaurantCompte::create([
                        'restaurant_id' => $order->restaurant_id,
                        'compte' => $sum
                    ]);

                }else{
                    $currentRestaurant->update([
                        'amount' => floatval($currentRestaurant->amount) +  floatval($order->amount)
                    ]);
                }

                DeliveryPayment::create([
                    'deliverer_id' => $order->delivery_id,
                    'amount' => $order->delivery_fees,
                    'is_paid' => 0,
                    'paid_date' => null
                ]);

                $currentDelivery = \App\Models\DeliveryCompte::where('delivery_id', $order->delivery_id)->first();

                if(is_null($currentDelivery) || empty($currentDelivery)){
                    $paymentDeliveries = DeliveryPayment::where(['deliverer_id' => $order->delivery_id])->get();

                    $sumDelivery = collect($paymentDeliveries)->reduce(function($carry, $item){
                        return $item->amount + $carry;
                    }, 0);
                    
                    \App\Models\DeliveryCompte::create([
                        'delivery_id' => $order->delivery_id,
                        'compte' => $sumDelivery
                    ]);

                }else{
                    $currentDelivery->update([
                        'amount' => floatval($currentDelivery->amount) +  floatval($order->amount)
                    ]);
                }
            }

            $this->lauchNotification($order);
        }

        return $this->sendResponse(is_null($livraison) ? $job->toArray() : $livraison->toArray(), 'Job changed successfully');
    }

    public function deliveryPayment(Request $request){
        $delivery = auth('deliverer_api')->user();
        
        $account = \App\Models\DeliveryCompte::where(['delivery_id' => $delivery->id])->first();
        
        if(is_null($account) || empty($account)){
            $paymentDeliveries = DeliveryPayment::where(['deliverer_id' => $delivery->id])->get();

            $sumDelivery = collect($paymentDeliveries)->reduce(function($carry, $item){
                return $item->amount + $carry;
            }, 0);
            
            $account = \App\Models\DeliveryCompte::create([
                'delivery_id' => $delivery->id,
                'compte' => $sumDelivery
            ]);
        }

        $data = [
            'payments' => \App\Models\DeliveryPayment::where(['deliverer_id' => $delivery->id])->get()->toArray(),
            'account' =>   $account->toArray(),
            'historiques'   => \App\Models\DeliveryPaymentHistorique::where(['delivery_id' => $delivery->id])->get()->toArray(),
            'requests'  => \App\Models\RequestDeliveryPayment::where(['delivery_id' => $delivery->id])->get()->toArray()
        ];
        
        return $this->sendResponse($data, 'Delivery Payment retrieved successfully');
    }

    public function deliveryPaymentPaid(Request $request){
        $delivery = auth('deliverer_api')->user();
        
        $deliveryPayments = DeliveryPayment::where(['deliverer_id' => $delivery->id, 'is_paid' => 1])->get();

        return $this->sendResponse($deliveryPayments->toArray(), 'Delivery Payment retrieved successfully');
    }

    /**
     * Remove the specified Delivery from storage.
     * DELETE /deliveries/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Delivery $delivery */
        $delivery = $this->deliveryRepository->find($id);

        if (empty($delivery)) {
            return $this->sendError('Delivery not found');
        }

        $delivery->delete();

        return $this->sendSuccess('Delivery deleted successfully');
    }

    public function sendRequest(Request $request){
        $delivery = auth('deliverer_api')->user();
        
        if($request->delivery_id != $delivery->id){
            return $this->sendError("User not allowed");
        }

        $balance = \App\Models\DeliveryCompte::where(['delivery_id' => $delivery->id])->first();

        if(floatval($request->amount) > floatval($balance->compte)){
            return $this->sendError("Insuffisant balance");
        }

        $requestPayment = RequestDeliveryPayment::create([
            'amount' => $request->amount,
            'paiement_method' => $request->paiement_method,
            'date' => $request->date,
            'account_reference' => $request->account_reference,
            'delivery_id'   => $delivery->id,
            'is_paid'   => false
        ]);

        return $this->sendResponse($requestPayment->toArray(), 'Request save successfully');
    }

    public function getHistorique($id){
        $historique = \App\Models\DeliveryPaymentHistorique::where(['request_id' => $id])->first();
        
        return $this->sendResponse(is_null($historique) ? [] : $historique->toArray(), "Delivery historique retrieved successfully");
    }

    public function lauchNotification($order){
        // \App\Models\RestaurantNotification::create([
        //     'title' => $order->status,
        //     'subtitle' => 'Status changed to '.$order->status,
        //     'action' => '#',
        //     'action_by' => '#',
        //     'meta_data_id' => $order->id,
        //     'meta_data' => $order,
        //     'type_notification' => $order->status,
        //     'is_read' => 0,
        //     'is_received' => 0,
        //     'data' => json_encode($order),
        //     'restaurant_id' => $order->restaurant_id,
        //     'data_id' => $order->id,
        //     'data'  =>  json_encode($order)
        // ]);

        $orderHistory = new \App\Models\OrderHistory();
        $orderHistory->order_id = $order->id;
        $orderHistory->status = $order->status;
        $orderHistory->creator = 'customers';
        $orderHistory->creator_id = $order->customer_id;
        $orderHistory->creator_name = \App\Models\Customer::find($order->customer_id)->name;
        $orderHistory->save();
    }

    public function checkExists(array $input, array $value){
        foreach($value as $item){
            if(!array_key_exists($item, $input)){
                return [false, $item];
            }
        }

        return [true, ''];
    }

    public function dynamicCall(String $table, array $param){
        $tableName = ucfirst($table);

        return call_user_func_array(["\App\Models\\".$tableName, 'where'], [$param]);
    }

    public function prepareResponseToSend($delivery, $default = "refresh"){
        // $token = \Auth::guard('deliverer_api')->fromUser($delivery);
        // $token = JWTAuth::attempt(['email' => $delivery->email, 'password' => $delivery]);
        // $token = JWTAuth::fromUser($delivery);
        $token = $default == 'refresh' ? \Auth::guard('deliverer_api')->refresh() : \Auth::guard('deliverer_api')->login($delivery);
        $orders = $this->dynamicCall('Orders', ['delivery_id' => $delivery->id])->get();

        $order_ids = collect($orders)->map(function($order){
            return $order->id;
        })->toArray();

        $data = [
            'token' => $token,
            'deliverer' => $delivery->toArray(),
            'vehicle' => $delivery->vehicle()->toArray(),
            // 'availabilities' => $this->dynamicCall('DeliveryAvailaibity', ['delivery_id' => $delivery->id, 'is_active' => 1])->first(),
            'orders'    =>  $orders,
            'orderAssignements' => $this->dynamicCall('OrderAssignement', ['deliverer_id' => $delivery->id])->get(),
            'jobs' => \App\Models\Job::whereIn('order_id', $order_ids)->get(),
            'notifications' => $this->dynamicCall('DeliveryNotification', ['delivery_id' => $delivery->id])->get()
        ];

        return $data;
    }

    public function deliveryPasswordForgotten(Request $request){
        // dd($request->all());

        if(empty($request->email) || is_null($request->email)){
            return $this->sendError("Email required", 400);
        }    

        /** @var Delivery $userDeliveryService */
        $user_delivery_services = Delivery::where(['email' => $request->email])->first();
        if(empty($user_delivery_services) || is_null($user_delivery_services)){
            return $this->sendError("Delivery not found", 400);
        }

        //GENERATE CODE AND SEND
        $code =  mt_rand(100000,999999);

        $passwordReset = \App\Models\PasswordDelivery::create([
            'password_reset_code' => $code,
            'password_reset_deadline' => Carbon::now()->copy()->addMinutes(30),
            'delevery_id' => $user_delivery_services->id
        ]);

        // $user_delivery_services->password_reset_code = $code;
        // $user_delivery_services->password_reset_code_deadline = Carbon::now()->copy()->addMinutes(30);
        // $user_delivery_services->save();


        //SEND EMAIL
        // $to_name = $user_delivery_services->name;
        // $to_email = $user_delivery_services->email;
        // $data = array('code'=> $code);

        
        // Mail::send('api_auth.email', $data, function($message) use ($to_name, $to_email) {
        //     $message->to($to_email, $to_name)->subject('Reset Password');
        //     $message->from('noreply@smartlivraison.com','Team Smart Livraison');
        // });

        Mail::to($user_delivery_services)->send(new PasswordDeliveryMail($user_delivery_services, $passwordReset));

        return $this->sendResponse($user_delivery_services, "Email sent successfully");

    }

    public function delivery_edit_picture(Request $request){
        $delivery = auth('deliverer_api')->user();

        $delivery->photo = $request->image_url;
        $delivery->save();

        return $this->sendResponse($this->prepareResponseToSend($delivery), 'Delivery updated successfully');
    }


    /**
     * manager_verify_code_password_forgotten
     * parameters: email, code
     */
    public function checkCodeForPasswordReset(Request $request){
        $code = $request->code;

        if(empty($request->code) || is_null($request->code)){
            return $this->sendError("Code is required", 400);
        }

        if(empty($request->email) || is_null($request->email)){
            return $this->sendError("Email required", 400);
        }

        /** @var Delivery $userDeliveryService */
        $user_delivery_services = Delivery::where(['email' => $request->email])->first();
        if(empty($user_delivery_services) || is_null($user_delivery_services)){
            return $this->sendError("Delivery not found", 400);
        }

        $password = \App\Models\PasswordDelivery::where(['delevery_id' => $user_delivery_services->id])->orderByDesc('id')->first();

        if(is_null($password) || empty($password)){
            return $this->sendError("Delivery not found", 400);
        }

        if($password->password_reset_code == $code){
            $codeDate = Carbon::parse($password->password_reset_deadline);
            if(Carbon::now()->greaterThan($codeDate)){
                return $this->sendError("Code expired", 401);
            }else{
                return $this->sendResponse($user_delivery_services, "OK");
            }
        }else{
            return $this->sendError("Bad code", 401);
        }
    }


    /**
     * manager_rest_password_forgotten
     * parameters: email, password, confirm_password
     */
    public function restPassword(Request $request){
        if(empty($request->password) || is_null($request->password)){
            return $this->sendError("Password required", 400);
        }

        if(empty($request->confirm_password) || is_null($request->confirm_password)){
            return $this->sendError("Password confirmation is required", 400);
        }

        /** @var Delivery $userDeliveryService */
        $user_delivery_services = Delivery::where(['email' => $request->email])->first();
        if(empty($user_delivery_services) || is_null($user_delivery_services)){
            return $this->sendError("Delivery not found", 400);
        }

        $password = $request->password;
        $confirmPassword = $request->confirm_password;

        if($password == $confirmPassword){
            // $user_delivery_services->password_reset_code = null;
            // $user_delivery_services->password_reset_code_deadline = null;
            $user_delivery_services->password = Hash::make($password);;
            $user_delivery_services->save();

            return $this->sendResponse($user_delivery_services, "Password updated successfully");
        }else{
            return $this->sendError("Password not conform", 400);
        }
    }


    /**
     * manager_edit_password
     * parameters: email, password, old_password
     */
    public function editPassword(Request $request){
        if(empty($request->password) || is_null($request->password)){
            return $this->sendError("Password required", 400);
        }
        
        if(empty($request->old_password) || is_null($request->old_password)){
            return $this->sendError('Old password required', 400);
        }
        
        /** @var Delivery $userDeliveryService */
        $user_delivery_services = Delivery::where(['email' => $request->email])->first();
        if(empty($user_delivery_services) || is_null($user_delivery_services)){
            return $this->sendError("Delivery not found", 400);
        }

        $password = $request->password;
        $oldPassword = $request->old_password;

        if(Hash::check($oldPassword, $userDeliveryService->password)){
            $user_delivery_services->password = Hash::make($password);
            $user_delivery_services->save();
            return $this->sendResponse($user_delivery_services, "Password updated successfully");
        }else{
            return $this->sendError("Bad password", 401);
        }
    }
}
