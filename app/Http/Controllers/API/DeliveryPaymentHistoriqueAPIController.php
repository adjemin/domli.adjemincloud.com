<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDeliveryPaymentHistoriqueAPIRequest;
use App\Http\Requests\API\UpdateDeliveryPaymentHistoriqueAPIRequest;
use App\Models\DeliveryPaymentHistorique;
use App\Repositories\DeliveryPaymentHistoriqueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\DeliveryPaymentHistoriqueResource;
use Response;

/**
 * Class DeliveryPaymentHistoriqueController
 * @package App\Http\Controllers\API
 */

class DeliveryPaymentHistoriqueAPIController extends AppBaseController
{
    /** @var  DeliveryPaymentHistoriqueRepository */
    private $deliveryPaymentHistoriqueRepository;

    public function __construct(DeliveryPaymentHistoriqueRepository $deliveryPaymentHistoriqueRepo)
    {
        $this->deliveryPaymentHistoriqueRepository = $deliveryPaymentHistoriqueRepo;
    }

    /**
     * Display a listing of the DeliveryPaymentHistorique.
     * GET|HEAD /deliveryPaymentHistoriques
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryPaymentHistoriques = $this->deliveryPaymentHistoriqueRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(DeliveryPaymentHistoriqueResource::collection($deliveryPaymentHistoriques), 'Delivery Payment Historiques retrieved successfully');
    }

    /**
     * Store a newly created DeliveryPaymentHistorique in storage.
     * POST /deliveryPaymentHistoriques
     *
     * @param CreateDeliveryPaymentHistoriqueAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryPaymentHistoriqueAPIRequest $request)
    {
        $input = $request->all();

        $deliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepository->create($input);

        return $this->sendResponse(new DeliveryPaymentHistoriqueResource($deliveryPaymentHistorique), 'Delivery Payment Historique saved successfully');
    }

    /**
     * Display the specified DeliveryPaymentHistorique.
     * GET|HEAD /deliveryPaymentHistoriques/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DeliveryPaymentHistorique $deliveryPaymentHistorique */
        $deliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepository->find($id);

        if (empty($deliveryPaymentHistorique)) {
            return $this->sendError('Delivery Payment Historique not found');
        }

        return $this->sendResponse(new DeliveryPaymentHistoriqueResource($deliveryPaymentHistorique), 'Delivery Payment Historique retrieved successfully');
    }

    /**
     * Update the specified DeliveryPaymentHistorique in storage.
     * PUT/PATCH /deliveryPaymentHistoriques/{id}
     *
     * @param int $id
     * @param UpdateDeliveryPaymentHistoriqueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryPaymentHistoriqueAPIRequest $request)
    {
        $input = $request->all();

        /** @var DeliveryPaymentHistorique $deliveryPaymentHistorique */
        $deliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepository->find($id);

        if (empty($deliveryPaymentHistorique)) {
            return $this->sendError('Delivery Payment Historique not found');
        }

        $deliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepository->update($input, $id);

        return $this->sendResponse(new DeliveryPaymentHistoriqueResource($deliveryPaymentHistorique), 'DeliveryPaymentHistorique updated successfully');
    }

    /**
     * Remove the specified DeliveryPaymentHistorique from storage.
     * DELETE /deliveryPaymentHistoriques/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DeliveryPaymentHistorique $deliveryPaymentHistorique */
        $deliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepository->find($id);

        if (empty($deliveryPaymentHistorique)) {
            return $this->sendError('Delivery Payment Historique not found');
        }

        $deliveryPaymentHistorique->delete();

        return $this->sendSuccess('Delivery Payment Historique deleted successfully');
    }
}
