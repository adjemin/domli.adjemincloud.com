<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRestaurantsAPIRequest;
use App\Http\Requests\API\UpdateRestaurantsAPIRequest;
use App\Models\Restaurants;
use App\Repositories\RestaurantsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\RestaurantsResource;
use Response;

/**
 * Class RestaurantsController
 * @package App\Http\Controllers\API
 */

class RestaurantsAPIController extends AppBaseController
{
    /** @var  RestaurantsRepository */
    private $restaurantsRepository;

    public function __construct(RestaurantsRepository $restaurantsRepo)
    {
        $this->restaurantsRepository = $restaurantsRepo;
    }

    /**
     * Display a listing of the Restaurants.
     * GET|HEAD /restaurants
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurants = $this->restaurantsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($restaurants->toArray(), 'Restaurants retrieved successfully');
    }

    /**
     * Store a newly created Restaurants in storage.
     * POST /restaurants
     *
     * @param CreateRestaurantsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantsAPIRequest $request)
    {
        $input = $request->all();

        $restaurants = $this->restaurantsRepository->create($input);

        return $this->sendResponse($restaurants->toArray(), 'Restaurants saved successfully');
    }

    /**
     * Display the specified Restaurants.
     * GET|HEAD /restaurants/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Restaurants $restaurants */
        $restaurants = $this->restaurantsRepository->find($id);

        if (empty($restaurants)) {
            return $this->sendError('Restaurants not found');
        }

        return $this->sendResponse($restaurants->toArray(), 'Restaurants retrieved successfully');
    }

    /**
     * Update the specified Restaurants in storage.
     * PUT/PATCH /restaurants/{id}
     *
     * @param int $id
     * @param UpdateRestaurantsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Restaurants $restaurants */
        $restaurants = $this->restaurantsRepository->find($id);

        if (empty($restaurants)) {
            return $this->sendError('Restaurants not found');
        }

        $restaurants = $this->restaurantsRepository->update($input, $id);

        return $this->sendResponse($restaurants->toArray(), 'Restaurants updated successfully');
    }

    /**
     * Remove the specified Restaurants from storage.
     * DELETE /restaurants/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Restaurants $restaurants */
        $restaurants = $this->restaurantsRepository->find($id);

        if (empty($restaurants)) {
            return $this->sendError('Restaurants not found');
        }

        $restaurants->delete();

        return $this->sendSuccess('Restaurants deleted successfully');
    }
}
