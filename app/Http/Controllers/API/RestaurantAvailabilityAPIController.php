<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRestaurantAvailabilityAPIRequest;
use App\Http\Requests\API\UpdateRestaurantAvailabilityAPIRequest;
use App\Models\RestaurantAvailability;
use App\Repositories\RestaurantAvailabilityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\RestaurantAvailabilityResource;
use Response;

/**
 * Class RestaurantAvailabilityController
 * @package App\Http\Controllers\API
 */

class RestaurantAvailabilityAPIController extends AppBaseController
{
    /** @var  RestaurantAvailabilityRepository */
    private $restaurantAvailabilityRepository;

    public function __construct(RestaurantAvailabilityRepository $restaurantAvailabilityRepo)
    {
        $this->restaurantAvailabilityRepository = $restaurantAvailabilityRepo;
    }

    /**
     * Display a listing of the RestaurantAvailability.
     * GET|HEAD /restaurantAvailabilities
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantAvailabilities = $this->restaurantAvailabilityRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(RestaurantAvailabilityResource::collection($restaurantAvailabilities), 'Restaurant Availabilities retrieved successfully');
    }

    /**
     * Store a newly created RestaurantAvailability in storage.
     * POST /restaurantAvailabilities
     *
     * @param CreateRestaurantAvailabilityAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantAvailabilityAPIRequest $request)
    {
        $input = $request->all();

        $restaurantAvailability = $this->restaurantAvailabilityRepository->create($input);

        return $this->sendResponse(new RestaurantAvailabilityResource($restaurantAvailability), 'Restaurant Availability saved successfully');
    }

    /**
     * Display the specified RestaurantAvailability.
     * GET|HEAD /restaurantAvailabilities/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RestaurantAvailability $restaurantAvailability */
        $restaurantAvailability = $this->restaurantAvailabilityRepository->find($id);

        if (empty($restaurantAvailability)) {
            return $this->sendError('Restaurant Availability not found');
        }

        return $this->sendResponse(new RestaurantAvailabilityResource($restaurantAvailability), 'Restaurant Availability retrieved successfully');
    }

    /**
     * Update the specified RestaurantAvailability in storage.
     * PUT/PATCH /restaurantAvailabilities/{id}
     *
     * @param int $id
     * @param UpdateRestaurantAvailabilityAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantAvailabilityAPIRequest $request)
    {
        $input = $request->all();

        /** @var RestaurantAvailability $restaurantAvailability */
        $restaurantAvailability = $this->restaurantAvailabilityRepository->find($id);

        if (empty($restaurantAvailability)) {
            return $this->sendError('Restaurant Availability not found');
        }

        $restaurantAvailability = $this->restaurantAvailabilityRepository->update($input, $id);

        return $this->sendResponse(new RestaurantAvailabilityResource($restaurantAvailability), 'RestaurantAvailability updated successfully');
    }

    /**
     * Remove the specified RestaurantAvailability from storage.
     * DELETE /restaurantAvailabilities/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RestaurantAvailability $restaurantAvailability */
        $restaurantAvailability = $this->restaurantAvailabilityRepository->find($id);

        if (empty($restaurantAvailability)) {
            return $this->sendError('Restaurant Availability not found');
        }

        $restaurantAvailability->delete();

        return $this->sendSuccess('Restaurant Availability deleted successfully');
    }
}
