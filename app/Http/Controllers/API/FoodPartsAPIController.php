<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFoodPartsAPIRequest;
use App\Http\Requests\API\UpdateFoodPartsAPIRequest;
use App\Models\FoodParts;
use App\Repositories\FoodPartsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\FoodPartsResource;
use Response;

/**
 * Class FoodPartsController
 * @package App\Http\Controllers\API
 */

class FoodPartsAPIController extends AppBaseController
{
    /** @var  FoodPartsRepository */
    private $foodPartsRepository;

    public function __construct(FoodPartsRepository $foodPartsRepo)
    {
        $this->foodPartsRepository = $foodPartsRepo;
    }

    /**
     * Display a listing of the FoodParts.
     * GET|HEAD /foodParts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $foodParts = $this->foodPartsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(FoodPartsResource::collection($foodParts), 'Food Parts retrieved successfully');
    }

    /**
     * Store a newly created FoodParts in storage.
     * POST /foodParts
     *
     * @param CreateFoodPartsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFoodPartsAPIRequest $request)
    {
        $input = $request->all();

        $foodParts = $this->foodPartsRepository->create($input);

        return $this->sendResponse(new FoodPartsResource($foodParts), 'Food Parts saved successfully');
    }

    /**
     * Display the specified FoodParts.
     * GET|HEAD /foodParts/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var FoodParts $foodParts */
        $foodParts = $this->foodPartsRepository->find($id);

        if (empty($foodParts)) {
            return $this->sendError('Food Parts not found');
        }

        return $this->sendResponse(new FoodPartsResource($foodParts), 'Food Parts retrieved successfully');
    }

    /**
     * Update the specified FoodParts in storage.
     * PUT/PATCH /foodParts/{id}
     *
     * @param int $id
     * @param UpdateFoodPartsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFoodPartsAPIRequest $request)
    {
        $input = $request->all();

        /** @var FoodParts $foodParts */
        $foodParts = $this->foodPartsRepository->find($id);

        if (empty($foodParts)) {
            return $this->sendError('Food Parts not found');
        }

        $foodParts = $this->foodPartsRepository->update($input, $id);

        return $this->sendResponse(new FoodPartsResource($foodParts), 'FoodParts updated successfully');
    }

    /**
     * Remove the specified FoodParts from storage.
     * DELETE /foodParts/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var FoodParts $foodParts */
        $foodParts = $this->foodPartsRepository->find($id);

        if (empty($foodParts)) {
            return $this->sendError('Food Parts not found');
        }

        $foodParts->delete();

        return $this->sendSuccess('Food Parts deleted successfully');
    }
}
