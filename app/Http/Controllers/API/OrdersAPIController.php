<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrdersAPIRequest;
use App\Http\Requests\API\UpdateOrdersAPIRequest;
use App\Models\Orders;
use App\Repositories\OrdersRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\OrdersResource;
use Response;

/**
 * Class OrdersController
 * @package App\Http\Controllers\API
 */

class OrdersAPIController extends AppBaseController
{
    /** @var  OrdersRepository */
    private $ordersRepository;

    public function __construct(OrdersRepository $ordersRepo)
    {
        $this->ordersRepository = $ordersRepo;
    }

    /**
     * Display a listing of the Orders.
     * GET|HEAD /orders
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $orders = $this->ordersRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(OrdersResource::collection($orders), 'Orders retrieved successfully');
    }

    /**
     * Store a newly created Orders in storage.
     * POST /orders
     *
     * @param CreateOrdersAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdersAPIRequest $request)
    {
        $input = $request->all();

        $orders = $this->ordersRepository->create($input);

        return $this->sendResponse(new OrdersResource($orders), 'Orders saved successfully');
    }

    /**
     * Display the specified Orders.
     * GET|HEAD /orders/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Orders $orders */
        $orders = $this->ordersRepository->find($id);

        if (empty($orders)) {
            return $this->sendError('Orders not found');
        }

        return $this->sendResponse(new OrdersResource($orders), 'Orders retrieved successfully');
    }

    /**
     * Update the specified Orders in storage.
     * PUT/PATCH /orders/{id}
     *
     * @param int $id
     * @param UpdateOrdersAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdersAPIRequest $request)
    {
        $input = $request->all();

        /** @var Orders $orders */
        $orders = $this->ordersRepository->find($id);

        if (empty($orders)) {
            return $this->sendError('Orders not found');
        }

        $orders = $this->ordersRepository->update($input, $id);

        return $this->sendResponse(new OrdersResource($orders), 'Orders updated successfully');
    }

    /**
     * Remove the specified Orders from storage.
     * DELETE /orders/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Orders $orders */
        $orders = $this->ordersRepository->find($id);

        if (empty($orders)) {
            return $this->sendError('Orders not found');
        }

        $orders->delete();

        return $this->sendSuccess('Orders deleted successfully');
    }
}
