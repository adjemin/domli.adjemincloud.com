<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRestaurantPaymentHistoriqueAPIRequest;
use App\Http\Requests\API\UpdateRestaurantPaymentHistoriqueAPIRequest;
use App\Models\RestaurantPaymentHistorique;
use App\Repositories\RestaurantPaymentHistoriqueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\RestaurantPaymentHistoriqueResource;
use Response;

/**
 * Class RestaurantPaymentHistoriqueController
 * @package App\Http\Controllers\API
 */

class RestaurantPaymentHistoriqueAPIController extends AppBaseController
{
    /** @var  RestaurantPaymentHistoriqueRepository */
    private $restaurantPaymentHistoriqueRepository;

    public function __construct(RestaurantPaymentHistoriqueRepository $restaurantPaymentHistoriqueRepo)
    {
        $this->restaurantPaymentHistoriqueRepository = $restaurantPaymentHistoriqueRepo;
    }

    /**
     * Display a listing of the RestaurantPaymentHistorique.
     * GET|HEAD /restaurantPaymentHistoriques
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantPaymentHistoriques = $this->restaurantPaymentHistoriqueRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(RestaurantPaymentHistoriqueResource::collection($restaurantPaymentHistoriques), 'Restaurant Payment Historiques retrieved successfully');
    }

    /**
     * Store a newly created RestaurantPaymentHistorique in storage.
     * POST /restaurantPaymentHistoriques
     *
     * @param CreateRestaurantPaymentHistoriqueAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantPaymentHistoriqueAPIRequest $request)
    {
        $input = $request->all();

        $restaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepository->create($input);

        return $this->sendResponse(new RestaurantPaymentHistoriqueResource($restaurantPaymentHistorique), 'Restaurant Payment Historique saved successfully');
    }

    /**
     * Display the specified RestaurantPaymentHistorique.
     * GET|HEAD /restaurantPaymentHistoriques/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RestaurantPaymentHistorique $restaurantPaymentHistorique */
        $restaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepository->find($id);

        if (empty($restaurantPaymentHistorique)) {
            return $this->sendError('Restaurant Payment Historique not found');
        }

        return $this->sendResponse(new RestaurantPaymentHistoriqueResource($restaurantPaymentHistorique), 'Restaurant Payment Historique retrieved successfully');
    }

    /**
     * Update the specified RestaurantPaymentHistorique in storage.
     * PUT/PATCH /restaurantPaymentHistoriques/{id}
     *
     * @param int $id
     * @param UpdateRestaurantPaymentHistoriqueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantPaymentHistoriqueAPIRequest $request)
    {
        $input = $request->all();

        /** @var RestaurantPaymentHistorique $restaurantPaymentHistorique */
        $restaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepository->find($id);

        if (empty($restaurantPaymentHistorique)) {
            return $this->sendError('Restaurant Payment Historique not found');
        }

        $restaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepository->update($input, $id);

        return $this->sendResponse(new RestaurantPaymentHistoriqueResource($restaurantPaymentHistorique), 'RestaurantPaymentHistorique updated successfully');
    }

    /**
     * Remove the specified RestaurantPaymentHistorique from storage.
     * DELETE /restaurantPaymentHistoriques/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RestaurantPaymentHistorique $restaurantPaymentHistorique */
        $restaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepository->find($id);

        if (empty($restaurantPaymentHistorique)) {
            return $this->sendError('Restaurant Payment Historique not found');
        }

        $restaurantPaymentHistorique->delete();

        return $this->sendSuccess('Restaurant Payment Historique deleted successfully');
    }
}
