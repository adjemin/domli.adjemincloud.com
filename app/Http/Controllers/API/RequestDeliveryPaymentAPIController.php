<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRequestDeliveryPaymentAPIRequest;
use App\Http\Requests\API\UpdateRequestDeliveryPaymentAPIRequest;
use App\Models\RequestDeliveryPayment;
use App\Repositories\RequestDeliveryPaymentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\RequestDeliveryPaymentResource;
use Response;

/**
 * Class RequestDeliveryPaymentController
 * @package App\Http\Controllers\API
 */

class RequestDeliveryPaymentAPIController extends AppBaseController
{
    /** @var  RequestDeliveryPaymentRepository */
    private $requestDeliveryPaymentRepository;

    public function __construct(RequestDeliveryPaymentRepository $requestDeliveryPaymentRepo)
    {
        $this->requestDeliveryPaymentRepository = $requestDeliveryPaymentRepo;
    }

    /**
     * Display a listing of the RequestDeliveryPayment.
     * GET|HEAD /requestDeliveryPayments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $requestDeliveryPayments = $this->requestDeliveryPaymentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(RequestDeliveryPaymentResource::collection($requestDeliveryPayments), 'Request Delivery Payments retrieved successfully');
    }

    /**
     * Store a newly created RequestDeliveryPayment in storage.
     * POST /requestDeliveryPayments
     *
     * @param CreateRequestDeliveryPaymentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRequestDeliveryPaymentAPIRequest $request)
    {
        $input = $request->all();

        $requestDeliveryPayment = $this->requestDeliveryPaymentRepository->create($input);

        return $this->sendResponse(new RequestDeliveryPaymentResource($requestDeliveryPayment), 'Request Delivery Payment saved successfully');
    }

    /**
     * Display the specified RequestDeliveryPayment.
     * GET|HEAD /requestDeliveryPayments/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RequestDeliveryPayment $requestDeliveryPayment */
        $requestDeliveryPayment = $this->requestDeliveryPaymentRepository->find($id);

        if (empty($requestDeliveryPayment)) {
            return $this->sendError('Request Delivery Payment not found');
        }

        return $this->sendResponse(new RequestDeliveryPaymentResource($requestDeliveryPayment), 'Request Delivery Payment retrieved successfully');
    }

    /**
     * Update the specified RequestDeliveryPayment in storage.
     * PUT/PATCH /requestDeliveryPayments/{id}
     *
     * @param int $id
     * @param UpdateRequestDeliveryPaymentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRequestDeliveryPaymentAPIRequest $request)
    {
        $input = $request->all();

        /** @var RequestDeliveryPayment $requestDeliveryPayment */
        $requestDeliveryPayment = $this->requestDeliveryPaymentRepository->find($id);

        if (empty($requestDeliveryPayment)) {
            return $this->sendError('Request Delivery Payment not found');
        }

        $requestDeliveryPayment = $this->requestDeliveryPaymentRepository->update($input, $id);

        return $this->sendResponse(new RequestDeliveryPaymentResource($requestDeliveryPayment), 'RequestDeliveryPayment updated successfully');
    }

    /**
     * Remove the specified RequestDeliveryPayment from storage.
     * DELETE /requestDeliveryPayments/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RequestDeliveryPayment $requestDeliveryPayment */
        $requestDeliveryPayment = $this->requestDeliveryPaymentRepository->find($id);

        if (empty($requestDeliveryPayment)) {
            return $this->sendError('Request Delivery Payment not found');
        }

        $requestDeliveryPayment->delete();

        return $this->sendSuccess('Request Delivery Payment deleted successfully');
    }
}
