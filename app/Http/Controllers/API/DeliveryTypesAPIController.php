<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDeliveryTypesAPIRequest;
use App\Http\Requests\API\UpdateDeliveryTypesAPIRequest;
use App\Models\DeliveryTypes;
use App\Repositories\DeliveryTypesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\DeliveryTypesResource;
use Response;

/**
 * Class DeliveryTypesController
 * @package App\Http\Controllers\API
 */

class DeliveryTypesAPIController extends AppBaseController
{
    /** @var  DeliveryTypesRepository */
    private $deliveryTypesRepository;

    public function __construct(DeliveryTypesRepository $deliveryTypesRepo)
    {
        $this->deliveryTypesRepository = $deliveryTypesRepo;
    }

    /**
     * Display a listing of the DeliveryTypes.
     * GET|HEAD /deliveryTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryTypes = $this->deliveryTypesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(DeliveryTypesResource::collection($deliveryTypes), 'Delivery Types retrieved successfully');
    }

    /**
     * Store a newly created DeliveryTypes in storage.
     * POST /deliveryTypes
     *
     * @param CreateDeliveryTypesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryTypesAPIRequest $request)
    {
        $input = $request->all();

        $deliveryTypes = $this->deliveryTypesRepository->create($input);

        return $this->sendResponse(new DeliveryTypesResource($deliveryTypes), 'Delivery Types saved successfully');
    }

    /**
     * Display the specified DeliveryTypes.
     * GET|HEAD /deliveryTypes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DeliveryTypes $deliveryTypes */
        $deliveryTypes = $this->deliveryTypesRepository->find($id);

        if (empty($deliveryTypes)) {
            return $this->sendError('Delivery Types not found');
        }

        return $this->sendResponse(new DeliveryTypesResource($deliveryTypes), 'Delivery Types retrieved successfully');
    }

    /**
     * Update the specified DeliveryTypes in storage.
     * PUT/PATCH /deliveryTypes/{id}
     *
     * @param int $id
     * @param UpdateDeliveryTypesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryTypesAPIRequest $request)
    {
        $input = $request->all();

        /** @var DeliveryTypes $deliveryTypes */
        $deliveryTypes = $this->deliveryTypesRepository->find($id);

        if (empty($deliveryTypes)) {
            return $this->sendError('Delivery Types not found');
        }

        $deliveryTypes = $this->deliveryTypesRepository->update($input, $id);

        return $this->sendResponse(new DeliveryTypesResource($deliveryTypes), 'DeliveryTypes updated successfully');
    }

    /**
     * Remove the specified DeliveryTypes from storage.
     * DELETE /deliveryTypes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DeliveryTypes $deliveryTypes */
        $deliveryTypes = $this->deliveryTypesRepository->find($id);

        if (empty($deliveryTypes)) {
            return $this->sendError('Delivery Types not found');
        }

        $deliveryTypes->delete();

        return $this->sendSuccess('Delivery Types deleted successfully');
    }
}
