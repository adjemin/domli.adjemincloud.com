<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrderAssignementAPIRequest;
use App\Http\Requests\API\UpdateOrderAssignementAPIRequest;
use App\Models\OrderAssignement;
use App\Repositories\OrderAssignementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\OrderAssignementResource;
use Response;

/**
 * Class OrderAssignementController
 * @package App\Http\Controllers\API
 */

class OrderAssignementAPIController extends AppBaseController
{
    /** @var  OrderAssignementRepository */
    private $orderAssignementRepository;

    public function __construct(OrderAssignementRepository $orderAssignementRepo)
    {
        $this->orderAssignementRepository = $orderAssignementRepo;
    }

    /**
     * Display a listing of the OrderAssignement.
     * GET|HEAD /orderAssignements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $orderAssignements = $this->orderAssignementRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(OrderAssignementResource::collection($orderAssignements), 'Order Assignements retrieved successfully');
    }

    /**
     * Store a newly created OrderAssignement in storage.
     * POST /orderAssignements
     *
     * @param CreateOrderAssignementAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderAssignementAPIRequest $request)
    {
        $input = $request->all();

        $orderAssignement = $this->orderAssignementRepository->create($input);

        return $this->sendResponse(new OrderAssignementResource($orderAssignement), 'Order Assignement saved successfully');
    }

    /**
     * Display the specified OrderAssignement.
     * GET|HEAD /orderAssignements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var OrderAssignement $orderAssignement */
        $orderAssignement = $this->orderAssignementRepository->find($id);

        if (empty($orderAssignement)) {
            return $this->sendError('Order Assignement not found');
        }

        return $this->sendResponse(new OrderAssignementResource($orderAssignement), 'Order Assignement retrieved successfully');
    }

    /**
     * Update the specified OrderAssignement in storage.
     * PUT/PATCH /orderAssignements/{id}
     *
     * @param int $id
     * @param UpdateOrderAssignementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderAssignementAPIRequest $request)
    {
        $input = $request->all();

        /** @var OrderAssignement $orderAssignement */
        $orderAssignement = $this->orderAssignementRepository->find($id);

        if (empty($orderAssignement)) {
            return $this->sendError('Order Assignement not found');
        }

        $orderAssignement = $this->orderAssignementRepository->update($input, $id);

        return $this->sendResponse(new OrderAssignementResource($orderAssignement), 'OrderAssignement updated successfully');
    }

    /**
     * Remove the specified OrderAssignement from storage.
     * DELETE /orderAssignements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var OrderAssignement $orderAssignement */
        $orderAssignement = $this->orderAssignementRepository->find($id);

        if (empty($orderAssignement)) {
            return $this->sendError('Order Assignement not found');
        }

        $orderAssignement->delete();

        return $this->sendSuccess('Order Assignement deleted successfully');
    }
}
