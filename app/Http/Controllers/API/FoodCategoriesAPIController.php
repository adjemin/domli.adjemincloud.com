<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFoodCategoriesAPIRequest;
use App\Http\Requests\API\UpdateFoodCategoriesAPIRequest;
use App\Models\FoodCategories;
use App\Repositories\FoodCategoriesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\FoodCategoriesResource;
use Response;

/**
 * Class FoodCategoriesController
 * @package App\Http\Controllers\API
 */

class FoodCategoriesAPIController extends AppBaseController
{
    /** @var  FoodCategoriesRepository */
    private $foodCategoriesRepository;

    public function __construct(FoodCategoriesRepository $foodCategoriesRepo)
    {
        $this->foodCategoriesRepository = $foodCategoriesRepo;
    }

    /**
     * Display a listing of the FoodCategories.
     * GET|HEAD /foodCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $foodCategories = $this->foodCategoriesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(FoodCategoriesResource::collection($foodCategories), 'Food Categories retrieved successfully');
    }

    /**
     * Store a newly created FoodCategories in storage.
     * POST /foodCategories
     *
     * @param CreateFoodCategoriesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFoodCategoriesAPIRequest $request)
    {
        $input = $request->all();

        $foodCategories = $this->foodCategoriesRepository->create($input);

        return $this->sendResponse(new FoodCategoriesResource($foodCategories), 'Food Categories saved successfully');
    }

    /**
     * Display the specified FoodCategories.
     * GET|HEAD /foodCategories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var FoodCategories $foodCategories */
        $foodCategories = $this->foodCategoriesRepository->find($id);

        if (empty($foodCategories)) {
            return $this->sendError('Food Categories not found');
        }

        return $this->sendResponse(new FoodCategoriesResource($foodCategories), 'Food Categories retrieved successfully');
    }

    /**
     * Update the specified FoodCategories in storage.
     * PUT/PATCH /foodCategories/{id}
     *
     * @param int $id
     * @param UpdateFoodCategoriesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFoodCategoriesAPIRequest $request)
    {
        $input = $request->all();

        /** @var FoodCategories $foodCategories */
        $foodCategories = $this->foodCategoriesRepository->find($id);

        if (empty($foodCategories)) {
            return $this->sendError('Food Categories not found');
        }

        $foodCategories = $this->foodCategoriesRepository->update($input, $id);

        return $this->sendResponse(new FoodCategoriesResource($foodCategories), 'FoodCategories updated successfully');
    }

    /**
     * Remove the specified FoodCategories from storage.
     * DELETE /foodCategories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var FoodCategories $foodCategories */
        $foodCategories = $this->foodCategoriesRepository->find($id);

        if (empty($foodCategories)) {
            return $this->sendError('Food Categories not found');
        }

        $foodCategories->delete();

        return $this->sendSuccess('Food Categories deleted successfully');
    }
}
