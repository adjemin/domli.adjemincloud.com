<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePasswordDeliveryAPIRequest;
use App\Http\Requests\API\UpdatePasswordDeliveryAPIRequest;
use App\Models\PasswordDelivery;
use App\Repositories\PasswordDeliveryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\PasswordDeliveryResource;
use Response;

/**
 * Class PasswordDeliveryController
 * @package App\Http\Controllers\API
 */

class PasswordDeliveryAPIController extends AppBaseController
{
    /** @var  PasswordDeliveryRepository */
    private $passwordDeliveryRepository;

    public function __construct(PasswordDeliveryRepository $passwordDeliveryRepo)
    {
        $this->passwordDeliveryRepository = $passwordDeliveryRepo;
    }

    /**
     * Display a listing of the PasswordDelivery.
     * GET|HEAD /passwordDeliveries
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $passwordDeliveries = $this->passwordDeliveryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(PasswordDeliveryResource::collection($passwordDeliveries), 'Password Deliveries retrieved successfully');
    }

    /**
     * Store a newly created PasswordDelivery in storage.
     * POST /passwordDeliveries
     *
     * @param CreatePasswordDeliveryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePasswordDeliveryAPIRequest $request)
    {
        $input = $request->all();

        $passwordDelivery = $this->passwordDeliveryRepository->create($input);

        return $this->sendResponse(new PasswordDeliveryResource($passwordDelivery), 'Password Delivery saved successfully');
    }

    /**
     * Display the specified PasswordDelivery.
     * GET|HEAD /passwordDeliveries/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PasswordDelivery $passwordDelivery */
        $passwordDelivery = $this->passwordDeliveryRepository->find($id);

        if (empty($passwordDelivery)) {
            return $this->sendError('Password Delivery not found');
        }

        return $this->sendResponse(new PasswordDeliveryResource($passwordDelivery), 'Password Delivery retrieved successfully');
    }

    /**
     * Update the specified PasswordDelivery in storage.
     * PUT/PATCH /passwordDeliveries/{id}
     *
     * @param int $id
     * @param UpdatePasswordDeliveryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePasswordDeliveryAPIRequest $request)
    {
        $input = $request->all();

        /** @var PasswordDelivery $passwordDelivery */
        $passwordDelivery = $this->passwordDeliveryRepository->find($id);

        if (empty($passwordDelivery)) {
            return $this->sendError('Password Delivery not found');
        }

        $passwordDelivery = $this->passwordDeliveryRepository->update($input, $id);

        return $this->sendResponse(new PasswordDeliveryResource($passwordDelivery), 'PasswordDelivery updated successfully');
    }

    /**
     * Remove the specified PasswordDelivery from storage.
     * DELETE /passwordDeliveries/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PasswordDelivery $passwordDelivery */
        $passwordDelivery = $this->passwordDeliveryRepository->find($id);

        if (empty($passwordDelivery)) {
            return $this->sendError('Password Delivery not found');
        }

        $passwordDelivery->delete();

        return $this->sendSuccess('Password Delivery deleted successfully');
    }
}
