<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAgentConnexionAPIRequest;
use App\Http\Requests\API\UpdateAgentConnexionAPIRequest;
use App\Models\AgentConnexion;
use App\Repositories\AgentConnexionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\AgentConnexionResource;
use Response;

/**
 * Class AgentConnexionController
 * @package App\Http\Controllers\API
 */

class AgentConnexionAPIController extends AppBaseController
{
    /** @var  AgentConnexionRepository */
    private $agentConnexionRepository;

    public function __construct(AgentConnexionRepository $agentConnexionRepo)
    {
        $this->agentConnexionRepository = $agentConnexionRepo;
    }

    /**
     * Display a listing of the AgentConnexion.
     * GET|HEAD /agentConnexions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $agentConnexions = $this->agentConnexionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(AgentConnexionResource::collection($agentConnexions), 'Agent Connexions retrieved successfully');
    }

    /**
     * Store a newly created AgentConnexion in storage.
     * POST /agentConnexions
     *
     * @param CreateAgentConnexionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAgentConnexionAPIRequest $request)
    {
        $input = $request->all();

        $agentConnexion = $this->agentConnexionRepository->create($input);

        return $this->sendResponse(new AgentConnexionResource($agentConnexion), 'Agent Connexion saved successfully');
    }

    /**
     * Display the specified AgentConnexion.
     * GET|HEAD /agentConnexions/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var AgentConnexion $agentConnexion */
        $agentConnexion = $this->agentConnexionRepository->find($id);

        if (empty($agentConnexion)) {
            return $this->sendError('Agent Connexion not found');
        }

        return $this->sendResponse(new AgentConnexionResource($agentConnexion), 'Agent Connexion retrieved successfully');
    }

    /**
     * Update the specified AgentConnexion in storage.
     * PUT/PATCH /agentConnexions/{id}
     *
     * @param int $id
     * @param UpdateAgentConnexionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAgentConnexionAPIRequest $request)
    {
        $input = $request->all();

        /** @var AgentConnexion $agentConnexion */
        $agentConnexion = $this->agentConnexionRepository->find($id);

        if (empty($agentConnexion)) {
            return $this->sendError('Agent Connexion not found');
        }

        $agentConnexion = $this->agentConnexionRepository->update($input, $id);

        return $this->sendResponse(new AgentConnexionResource($agentConnexion), 'AgentConnexion updated successfully');
    }

    /**
     * Remove the specified AgentConnexion from storage.
     * DELETE /agentConnexions/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var AgentConnexion $agentConnexion */
        $agentConnexion = $this->agentConnexionRepository->find($id);

        if (empty($agentConnexion)) {
            return $this->sendError('Agent Connexion not found');
        }

        $agentConnexion->delete();

        return $this->sendSuccess('Agent Connexion deleted successfully');
    }
}
