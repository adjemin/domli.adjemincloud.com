<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRestaurantCategoriesAPIRequest;
use App\Http\Requests\API\UpdateRestaurantCategoriesAPIRequest;
use App\Models\RestaurantCategories;
use App\Repositories\RestaurantCategoriesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\RestaurantCategoriesResource;
use Response;

/**
 * Class RestaurantCategoriesController
 * @package App\Http\Controllers\API
 */

class RestaurantCategoriesAPIController extends AppBaseController
{
    /** @var  RestaurantCategoriesRepository */
    private $restaurantCategoriesRepository;

    public function __construct(RestaurantCategoriesRepository $restaurantCategoriesRepo)
    {
        $this->restaurantCategoriesRepository = $restaurantCategoriesRepo;
    }

    /**
     * Display a listing of the RestaurantCategories.
     * GET|HEAD /restaurantCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $restaurantCategories = $this->restaurantCategoriesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(RestaurantCategoriesResource::collection($restaurantCategories), 'Restaurant Categories retrieved successfully');
    }

    /**
     * Store a newly created RestaurantCategories in storage.
     * POST /restaurantCategories
     *
     * @param CreateRestaurantCategoriesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantCategoriesAPIRequest $request)
    {
        $input = $request->all();

        $restaurantCategories = $this->restaurantCategoriesRepository->create($input);

        return $this->sendResponse(new RestaurantCategoriesResource($restaurantCategories), 'Restaurant Categories saved successfully');
    }

    /**
     * Display the specified RestaurantCategories.
     * GET|HEAD /restaurantCategories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RestaurantCategories $restaurantCategories */
        $restaurantCategories = $this->restaurantCategoriesRepository->find($id);

        if (empty($restaurantCategories)) {
            return $this->sendError('Restaurant Categories not found');
        }

        return $this->sendResponse(new RestaurantCategoriesResource($restaurantCategories), 'Restaurant Categories retrieved successfully');
    }

    /**
     * Update the specified RestaurantCategories in storage.
     * PUT/PATCH /restaurantCategories/{id}
     *
     * @param int $id
     * @param UpdateRestaurantCategoriesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantCategoriesAPIRequest $request)
    {
        $input = $request->all();

        /** @var RestaurantCategories $restaurantCategories */
        $restaurantCategories = $this->restaurantCategoriesRepository->find($id);

        if (empty($restaurantCategories)) {
            return $this->sendError('Restaurant Categories not found');
        }

        $restaurantCategories = $this->restaurantCategoriesRepository->update($input, $id);

        return $this->sendResponse(new RestaurantCategoriesResource($restaurantCategories), 'RestaurantCategories updated successfully');
    }

    /**
     * Remove the specified RestaurantCategories from storage.
     * DELETE /restaurantCategories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RestaurantCategories $restaurantCategories */
        $restaurantCategories = $this->restaurantCategoriesRepository->find($id);

        if (empty($restaurantCategories)) {
            return $this->sendError('Restaurant Categories not found');
        }

        $restaurantCategories->delete();

        return $this->sendSuccess('Restaurant Categories deleted successfully');
    }
}
