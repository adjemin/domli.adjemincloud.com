<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderDeliveriesRequest;
use App\Http\Requests\UpdateOrderDeliveriesRequest;
use App\Repositories\OrderDeliveriesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OrderDeliveriesController extends AppBaseController
{
    /** @var  OrderDeliveriesRepository */
    private $orderDeliveriesRepository;

    public function __construct(OrderDeliveriesRepository $orderDeliveriesRepo)
    {
        $this->orderDeliveriesRepository = $orderDeliveriesRepo;
    }

    /**
     * Display a listing of the OrderDeliveries.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orderDeliveries = $this->orderDeliveriesRepository->all();

        return view('order_deliveries.index')
            ->with('orderDeliveries', $orderDeliveries);
    }

    /**
     * Show the form for creating a new OrderDeliveries.
     *
     * @return Response
     */
    public function create()
    {
        return view('order_deliveries.create');
    }

    /**
     * Store a newly created OrderDeliveries in storage.
     *
     * @param CreateOrderDeliveriesRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderDeliveriesRequest $request)
    {
        $input = $request->all();

        $orderDeliveries = $this->orderDeliveriesRepository->create($input);

        Flash::success('Order Deliveries saved successfully.');

        return redirect(route('orderDeliveries.index'));
    }

    /**
     * Display the specified OrderDeliveries.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orderDeliveries = $this->orderDeliveriesRepository->find($id);

        if (empty($orderDeliveries)) {
            Flash::error('Order Deliveries not found');

            return redirect(route('orderDeliveries.index'));
        }

        return view('order_deliveries.show')->with('orderDeliveries', $orderDeliveries);
    }

    /**
     * Show the form for editing the specified OrderDeliveries.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orderDeliveries = $this->orderDeliveriesRepository->find($id);

        if (empty($orderDeliveries)) {
            Flash::error('Order Deliveries not found');

            return redirect(route('orderDeliveries.index'));
        }

        return view('order_deliveries.edit')->with('orderDeliveries', $orderDeliveries);
    }

    /**
     * Update the specified OrderDeliveries in storage.
     *
     * @param int $id
     * @param UpdateOrderDeliveriesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderDeliveriesRequest $request)
    {
        $orderDeliveries = $this->orderDeliveriesRepository->find($id);

        if (empty($orderDeliveries)) {
            Flash::error('Order Deliveries not found');

            return redirect(route('orderDeliveries.index'));
        }

        $orderDeliveries = $this->orderDeliveriesRepository->update($request->all(), $id);

        Flash::success('Order Deliveries updated successfully.');

        return redirect(route('orderDeliveries.index'));
    }

    /**
     * Remove the specified OrderDeliveries from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orderDeliveries = $this->orderDeliveriesRepository->find($id);

        if (empty($orderDeliveries)) {
            Flash::error('Order Deliveries not found');

            return redirect(route('orderDeliveries.index'));
        }

        $this->orderDeliveriesRepository->delete($id);

        Flash::success('Order Deliveries deleted successfully.');

        return redirect(route('orderDeliveries.index'));
    }
}
