<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRequestRestaurantPaymentRequest;
use App\Http\Requests\UpdateRequestRestaurantPaymentRequest;
use App\Repositories\RequestRestaurantPaymentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Auth;
use Response;

class RequestRestaurantPaymentController extends AppBaseController
{
    /** @var  RequestRestaurantPaymentRepository */
    private $requestRestaurantPaymentRepository;

    public function __construct(RequestRestaurantPaymentRepository $requestRestaurantPaymentRepo)
    {
        $this->requestRestaurantPaymentRepository = $requestRestaurantPaymentRepo;
    }

    /**
     * Display a listing of the RequestRestaurantPayment.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if(Auth::guard('restaurant')->check()){
            $requestRestaurantPayments = \App\Models\RequestRestaurantPayment::where('restaurant_id', Auth::guard('restaurant')->user()->id)->orderByDesc('id')->get();
        }else if(Auth::check()){
            $requestRestaurantPayments = $this->requestRestaurantPaymentRepository->all();
        }else{
            Flash::error('Not allowed');
            return redirect()->back();
        }

        return view('request_restaurant_payments.index')
            ->with('requestRestaurantPayments', $requestRestaurantPayments);
    }

    /**
     * Show the form for creating a new RequestRestaurantPayment.
     *
     * @return Response
     */
    public function create()
    {
        return view('request_restaurant_payments.create');
    }

    /**
     * Store a newly created RequestRestaurantPayment in storage.
     *
     * @param CreateRequestRestaurantPaymentRequest $request
     *
     * @return Response
     */
    public function store(CreateRequestRestaurantPaymentRequest $request)
    {
        $input = $request->all();

        $requestRestaurantPayment = $this->requestRestaurantPaymentRepository->create($input);

        Flash::success('Request Restaurant Payment saved successfully.');

        return redirect(route('requestRestaurantPayments.index'));
    }

    /**
     * Display the specified RequestRestaurantPayment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $requestRestaurantPayment = $this->requestRestaurantPaymentRepository->find($id);

        if (empty($requestRestaurantPayment)) {
            Flash::error('Request Restaurant Payment not found');

            // return redirect(route('requestRestaurantPayments.index'));
            return redirect()->back();
        }

        return view('request_restaurant_payments.show')->with('requestRestaurantPayment', $requestRestaurantPayment);
    }

    public function restaurant($restaurant_id)
    {
        $requestRestaurantPayments = \App\Models\RequestRestaurantPayment::where('restaurant_id', $restaurant_id)->get();

        if (empty($requestRestaurantPayments)) {
            Flash::error('Request Restaurant Payment not found');

            // return redirect(route('requestRestaurantPayments.index'));
            
            return redirect()->back();
        }

        return view('request_restaurant_payments.index')->with('requestRestaurantPayments', $requestRestaurantPayments);
    }

    /**
     * Show the form for editing the specified RequestRestaurantPayment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $requestRestaurantPayment = $this->requestRestaurantPaymentRepository->find($id);

        if (empty($requestRestaurantPayment)) {
            Flash::error('Request Restaurant Payment not found');

            return redirect(route('requestRestaurantPayments.index'));
        }

        return view('request_restaurant_payments.edit')->with('requestRestaurantPayment', $requestRestaurantPayment);
    }

    /**
     * Update the specified RequestRestaurantPayment in storage.
     *
     * @param int $id
     * @param UpdateRequestRestaurantPaymentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRequestRestaurantPaymentRequest $request)
    {
        $requestRestaurantPayment = $this->requestRestaurantPaymentRepository->find($id);

        if (empty($requestRestaurantPayment)) {
            Flash::error('Request Restaurant Payment not found');

            return redirect(route('requestRestaurantPayments.index'));
        }

        $requestRestaurantPayment = $this->requestRestaurantPaymentRepository->update($request->all(), $id);

        Flash::success('Request Restaurant Payment updated successfully.');

        return redirect(route('requestRestaurantPayments.index'));
    }

    /**
     * Remove the specified RequestRestaurantPayment from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $requestRestaurantPayment = $this->requestRestaurantPaymentRepository->find($id);

        if (empty($requestRestaurantPayment)) {
            Flash::error('Request Restaurant Payment not found');

            return redirect(route('requestRestaurantPayments.index'));
        }

        $this->requestRestaurantPaymentRepository->delete($id);

        Flash::success('Request Restaurant Payment deleted successfully.');

        return redirect(route('requestRestaurantPayments.index'));
    }
}
