<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TranslationController extends Controller
{
    public function changeLocale(Request $request)
    {
        // dd($request->all());
        // $this->validate($request, ['locale' => 'required|in:fr,en,ch']);

        \Session::put('locale', $request->locale);
        App::setLocale($request->locale);

        return redirect()->back();
    }
}
