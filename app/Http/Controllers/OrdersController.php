<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrdersRequest;
use App\Http\Requests\UpdateOrdersRequest;
use App\Models\Orders;
use App\Models\Delivery;
use App\Models\Restaurants;
use App\Models\RestaurantNotification;
use App\Repositories\OrdersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Auth;
use Response;

class OrdersController extends AppBaseController
{
    /** @var  OrdersRepository */
    private $ordersRepository;

    public function __construct(OrdersRepository $ordersRepo)
    {
        $this->ordersRepository = $ordersRepo;
    }

    /**
     * Display a listing of the Orders.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $orders = $this->ordersRepository->all();
        $orders = Orders::orderByDesc('id')->paginate(10);

        return view('orders.index')
            ->with('orders', $orders);
    }

    public function restaurant(Request $request, $restaurant_id){
        $restaurant = Restaurants::where('uuid', $restaurant_id)->orWhere('id', $restaurant_id)->first();

        if(Auth::guard('restaurant')->check()){
            RestaurantNotification::where([
                'restaurant_id' =>  Auth::guard('restaurant')->user()->id,
                'is_read' => 0  
            ])->update([
                'is_read' => 1
            ]);
        }
        
        $orders = Orders::where('restaurant_id', $restaurant->id)->orderByDesc('id')->paginate(10);
        
        return view('orders.index')
            ->with('orders', $orders);
    }

    /**
     * Show the form for creating a new Orders.
     *
     * @return Response
     */
    public function create()
    {
        return view('orders.create');
    }

    /**
     * Store a newly created Orders in storage.
     *
     * @param CreateOrdersRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdersRequest $request)
    {
        $input = $request->all();

        $orders = $this->ordersRepository->create($input);

        Flash::success('Orders saved successfully.');

        return redirect(route('orders.index'));
    }

    /**
     * Display the specified Orders.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $order = \App\Models\Orders::where(['order_number' => $id])->orWhere(['id' => $id])->first();

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect()->back();
        }

        return view('customer.pages.orderDetailSingle', compact('order'));
        // return view('orders.show')->with('orders', $orders);
    }

    /**
     * Display the specified Orders.
     *
     * @param int $id
     *
     * @return Response
     */
    public function delivery(Request $request, $delivery_id)
    {
        $orders = \App\Models\Orders::where('delivery_id',$delivery_id)->orderByDesc('id')->paginate(10);

        if (empty($orders)) {
            Flash::error('Order not found');
            return  redirect()->back();
        }

        return view('orders.index')
            ->with('orders', $orders);
    }

    public function showRestaurant(Request $request, $restaurant_id, $id)
    {
        $order = \App\Models\Orders::where('order_number',$id)->orWhere('id', $id)->first();

        // dd($orders);
        if (empty($order)) {
            Flash::error('Order not found');

            return  redirect()->back();
        }
        

        return view('customer.pages.orderDetailSingle', compact('order'));
    }

    /**
     * Show the form for editing the specified Orders.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orders = $this->ordersRepository->find($id);

        if (empty($orders)) {
            Flash::error('Orders not found');

            return redirect(route('orders.index'));
        }

        return view('orders.edit')->with('orders', $orders);
    }

    /**
     * Update the specified Orders in storage.
     *
     * @param int $id
     * @param UpdateOrdersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdersRequest $request)
    {
        $orders = $this->ordersRepository->find($id);

        if (empty($orders)) {
            Flash::error('Orders not found');

            return redirect(route('orders.index'));
        }

        $orders = $this->ordersRepository->update($request->all(), $id);

        Flash::success('Orders updated successfully.');

        return redirect(route('orders.index'));
    }

    /**
     * Remove the specified Orders from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orders = $this->ordersRepository->find($id);

        if (empty($orders)) {
            Flash::error('Orders not found');

            return redirect(route('orders.index'));
        }

        $this->ordersRepository->delete($id);

        Flash::success('Orders deleted successfully.');

        return redirect(route('orders.index'));
    }

    public function change_status(Request $request, $order_number){
        
        $order = Orders::where(['order_number' => $order_number])->first();

        if(is_null($order) || empty($order)){
            return redirect()->back();
        }

        if($request->status == 'completed_cooking') {
            $order->is_cooking_completed = 1;
            $order->save();
        }else{
            $order->current_status = $request->status;
            $order->save();

            if($request->status == 'confirmed'){
                $deliveries = Delivery::where('is_connected', 1)->geofence($order->restaurant->lat, $order->restaurant->lng, 0, 15)->get();

                if($deliveries->count() == 0){
                    $deliveries = Delivery::distance($order->restaurant->lat,$order->restaurant->lng)->orderByDesc('distance')->get();    
                }
                // dd($deliveries);

                foreach($deliveries as $delivery){
                    // dd($delivery);
                    $orderAssignement = \App\Models\OrderAssignement::create([
                        'order_id' => $order->id,
                        'deliverer_id' => $delivery->id,
                        'is_waiting_acceptation' => true
                    ]);

                    $notification = \App\Models\DeliveryNotification::create([
                        'title' => 'New assignement',
                        'subtitle' => 'You have been assigned to do a new task',
                        'action' => '#',
                        'action_by' => '#',
                        'meta_data_id' => $orderAssignement->id,
                        'meta_data' => json_encode($orderAssignement),
                        'type_notification' => '',
                        'is_read' => 0,
                        'is_received' => 0,
                        'data' => 'string',
                        'delivery_id' => $delivery->id,
                        'data_id' => $orderAssignement->id,
                        'data'  =>  json_encode($orderAssignement)
                    ]);

                    \App\Utils\DeliveryMessagingUtils::notify($notification);
                }

                $order = Orders::find($order->id);
                $order->is_waiting = true;
                $order->save();
            }

            if($request->status == 'noted'){
                $order->is_delivery = isset($request->cke) ? $request->cke : 0;
                $order->save();
            }
        }

        return redirect()->back();
    }
}
