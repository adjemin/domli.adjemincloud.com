<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFoodCategoriesRequest;
use App\Http\Requests\UpdateFoodCategoriesRequest;
use App\Repositories\FoodCategoriesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Imgur;
use Flash;
use Response;

class FoodCategoriesController extends AppBaseController
{
    /** @var  FoodCategoriesRepository */
    private $foodCategoriesRepository;

    public function __construct(FoodCategoriesRepository $foodCategoriesRepo)
    {
        $this->foodCategoriesRepository = $foodCategoriesRepo;
    }

    /**
     * Display a listing of the FoodCategories.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $foodCategories = $this->foodCategoriesRepository->all();

        return view('food_categories.index')
            ->with('foodCategories', $foodCategories);
    }

    /**
     * Show the form for creating a new FoodCategories.
     *
     * @return Response
     */
    public function create()
    {
        return view('food_categories.create');
    }

    /**
     * Store a newly created FoodCategories in storage.
     *
     * @param CreateFoodCategoriesRequest $request
     *
     * @return Response
     */
    public function store(CreateFoodCategoriesRequest $request)
    {
        $input = $request->all();

        $productImageLink = null;

        if ($request->file('photo')) {

            $image = $request->file('photo');
            $pictures = [];
            if ($image != null) {
                $productImage = Imgur::upload($image);
                $productImageLink = $productImage->link();
            }
        } else {
            $productImageLink = "https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png";
        }

        if(isset($input["photo"])){
            unset($input["photo"]);
        }
        $input["photo"] = $productImageLink;

        $foodCategories = $this->foodCategoriesRepository->create($input);

        Flash::success('Food Categories saved successfully.');

        return redirect(route('foodCategories.index'));
    }

    /**
     * Display the specified FoodCategories.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $foodCategories = $this->foodCategoriesRepository->find($id);

        if (empty($foodCategories)) {
            Flash::error('Food Categories not found');

            return redirect(route('foodCategories.index'));
        }

        return view('food_categories.show')->with('foodCategories', $foodCategories);
    }

    /**
     * Show the form for editing the specified FoodCategories.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $foodCategories = $this->foodCategoriesRepository->find($id);

        if (empty($foodCategories)) {
            Flash::error('Food Categories not found');

            return redirect(route('foodCategories.index'));
        }

        return view('food_categories.edit')->with('foodCategories', $foodCategories);
    }

    /**
     * Update the specified FoodCategories in storage.
     *
     * @param int $id
     * @param UpdateFoodCategoriesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFoodCategoriesRequest $request)
    {
        $foodCategories = $this->foodCategoriesRepository->find($id);

        if (empty($foodCategories)) {
            Flash::error('Food Categories not found');

            return redirect(route('foodCategories.index'));
        }

        $foodCategories = $this->foodCategoriesRepository->update($request->all(), $id);

        Flash::success('Food Categories updated successfully.');

        return redirect(route('foodCategories.index'));
    }

    /**
     * Remove the specified FoodCategories from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $foodCategories = $this->foodCategoriesRepository->find($id);

        if (empty($foodCategories)) {
            Flash::error('Food Categories not found');

            return redirect(route('foodCategories.index'));
        }

        $this->foodCategoriesRepository->delete($id);

        Flash::success('Food Categories deleted successfully.');

        return redirect(route('foodCategories.index'));
    }
}
