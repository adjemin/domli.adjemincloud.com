<?php

namespace App\Http\Controllers;

use Auth;
use Stripe\Stripe;
use App\Models\Food;
use Stripe\PaymentIntent;
use Illuminate\Support\Arr;
use App\Models\OrderHistory;
use App\Models\OrderItems;
use App\Models\Invoice;
use App\Models\Orders;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Repositories\OrderRepository;

class OrderController extends Controller
{

    /** @var  OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepo)
    {
        $this->orderRepository = $orderRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::guard('customer')->check()){
            return redirect('customer/login');
        }

        $userCoordinates = $request->session()->get('user_ip_address');

        // dd(Cart::content());
        $restaurants = collect(Cart::content())->map(function($item){
            $food = Food::find($item->id);
            return $food->restaurant ?? null;
        })->reject(function($restaurant){
            return is_null($restaurant);
        })->unique()->first();
        // $restaurants = App\Models\Restaurants::where()->in

        if(Cart::count() <= 0) {
            return redirect()->back();
        }
        // dd($request->foodId);
        return view('frontOffice.pages.orderDetail', compact('userCoordinates', 'restaurants'));
    }

    public function notAuthIndex(Request $request)
    {

        $foodqty = $request->OrderQty;
        $foodId = $request->foodId;
        $food = Food::find($foodId);
        $restaurant = $food->restaurant;
        // dd(['food' => $food, 'foodId' => $foodId, 'foodqty' => $foodqty]);
        // $userCoordinates = $request->session()->get('user_ip_address');
        // dd(Cart::content());
        // $restaurants = collect(Cart::content())->map(function($item){
            // return $food->restaurant;
        // })->unique()->first();
        // $restaurants = App\Models\Restaurants::where()->in

        // if (Cart::count() <= 0) {
            //     return redirect()->back();
            // }
            // , compact('userCoordinates', 'restaurants')
        // dd($request->foodId);
        return view('frontOffice.pages.orderDetail', compact('food', 'foodqty', 'restaurant'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'delivery-adress' => 'required|string',
        //     'lat'   => 'required_if:delivery-adress,false',
        //     'lon'   => 'required_if:delivery-adress,false',
        //     'delivery_fees_input'   => 'required_if:delivery-adress,false'
        // ]);

        if ($request->checkout === "add-credit-card") {
            Stripe::setApiKey('sk_test_51IAwvKDVFLc3spxtE2L6CTqldp0yBPz53yczNkI8ZBHXyjZ2vURCrt69basxwfCA4xVWSGjBxZKRnoDeU9VDS9Nl00TZMoXXLg');

            $intent = PaymentIntent::create([
                'amount' => round(Cart::total()),
                'currency' => 'usd'
            ]);

            $clientSecret = Arr::get($intent, 'client_secret');
            return view('frontOffice.pages.checkout')->with(compact('clientSecret'));
        }else{
            $jsonFields = $request->all();

            $input = [];

            $restaurant = collect(Cart::content())->map(function($item){
                $food = Food::where('uuid',$item->id)->first();
                return $food->restaurant;
            })->unique()->first();

            $input['customer_id'] = \Auth::guard('customer')->user()->id;
            $input['order_number'] = \App\Utils\UUID::v4();
            $input['is_waiting'] = true;
            $input['current_status'] = Orders::WAITING;
            $input['payment_method_slug'] = "cash";
            $input['delivery_fees'] =  isset($jsonFields['delivery_fees']) ? (double) $jsonFields['delivery_fees'] : 0;
            $input['is_delivery'] = false;
            $input['currency_name'] = 'Dollard';
            $input['currency_code'] = 'USD';
            $input['taxe'] = Cart::tax();
            // $input['rating'] = '';
            $input['is_wayting_payment'] = true;
            $input['restaurant_id'] =  $restaurant->id;
            // $input['restaurant_id'] =  $restaurant->id;
            $input['kilometers'] =  (double) $jsonFields['kilometers'];
            $input['location_lat'] =  $jsonFields['lat_input'] ?? '';
            $input['location_lng'] =  $jsonFields['lon_input'] ?? '';
            $input['location_name'] = $jsonFields['delivery-adress'] ?? '';
            // $input['food_id'] =  (int) $jsonFields->get('food_id') ?? null ;

            // $input['note'] = '';

            // dd($input);
            $customer = \App\Models\Customers::where(['id' => $input['customer_id']])->first();

            $items = Cart::content();
            
            $order = $this->orderRepository->create($input);

            \App\Models\RestaurantNotification::create([
                'title' => 'New reservation',
                'subtitle' => 'a customer has just placed an order',
                'action' => '#',
                'action_by' => '#',
                'meta_data_id' => $order->id,
                'meta_data' => json_encode($order),
                'type_notification' => '',
                'is_read' => 0,
                'is_received' => 0,
                'data' => 'string',
                'restaurant_id' => $restaurant->id,
                'data_id' => $order->id,
                'data'  =>  json_encode(Cart::content())
            ]);

            $orderHistory = new OrderHistory();
            $orderHistory->order_id = $order->id;
            $orderHistory->status = Orders::WAITING;
            $orderHistory->creator = 'customers';
            $orderHistory->creator_id = $customer->id;
            $orderHistory->creator_name = $customer->name;
            $orderHistory->save();

            $subtotal = (double) $input['delivery_fees'] + Cart::subtotal();

            foreach ($items as $item){
                $el =  Food::where('uuid', $item->id)->first();

                $orderItem = OrderItems::create([
                    'order_id'=> $order->id,
                    'meta_data_id' => $el->id,
                    'meta_data' => json_encode($el),
                    'data' => json_encode($item),
                    // 'data_id' => '',
                    'quantity' => $item->qty,
                    'unit_price' => $item->price / $item->qty,
                    'total_amount' => $item->price
                ]);
            }
   
            $order->amount = $subtotal;
            $order->update();

            $discount = collect(Cart::getConditions())->map(function($condition){
                return floatval($condition->parsedRawValue) ?? 0;
            })->sum();

            $invoice = Invoice::create([
                'order_id' => $order->id,
                'service_slug' => 'restaurant',
                'customer_id' => $input['customer_id'],
                'reference' => Invoice::generateID('restaurant', $order->id, $input['customer_id']),
                'link' => "#",
                'subtotal' => $subtotal,
                'tax' => Cart::tax(),
                'fees_delivery' => (double) $input['delivery_fees'],
                'total' => Cart::total(),
                'status' => 'unpaid',
                'is_paid' => 0,
                'discount' => $discount ?? 0.0,
                'service_slug' => 'restaurant',
                'currency_code' => 'USD',
                'currency_name' => 'Dollar',
                'is_paid_to_business' => false
            ]);

            Cart::destroy();

            return redirect('/');
        }
    }

    // public function getPAymentForm(Request $request){
    //     //
    // }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
