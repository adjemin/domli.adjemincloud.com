<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDeliveryPaymentRequest;
use App\Http\Requests\UpdateDeliveryPaymentRequest;
use App\Repositories\DeliveryPaymentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DeliveryPaymentController extends AppBaseController
{
    /** @var  DeliveryPaymentRepository */
    private $deliveryPaymentRepository;

    public function __construct(DeliveryPaymentRepository $deliveryPaymentRepo)
    {
        $this->deliveryPaymentRepository = $deliveryPaymentRepo;
    }

    /**
     * Display a listing of the DeliveryPayment.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryPayments = $this->deliveryPaymentRepository->all();

        return view('delivery_payments.index')
            ->with('deliveryPayments', $deliveryPayments);
    }

    /**
     * Show the form for creating a new DeliveryPayment.
     *
     * @return Response
     */
    public function create()
    {
        return view('delivery_payments.create');
    }

    /**
     * Store a newly created DeliveryPayment in storage.
     *
     * @param CreateDeliveryPaymentRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryPaymentRequest $request)
    {
        $input = $request->all();

        $deliveryPayment = $this->deliveryPaymentRepository->create($input);

        Flash::success('Delivery Payment saved successfully.');

        return redirect(route('deliveryPayments.index'));
    }

    /**
     * Display the specified DeliveryPayment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deliveryPayment = $this->deliveryPaymentRepository->find($id);

        if (empty($deliveryPayment)) {
            Flash::error('Delivery Payment not found');

            return redirect(route('deliveryPayments.index'));
        }

        return view('delivery_payments.show')->with('deliveryPayment', $deliveryPayment);
    }

    /**
     * Show the form for editing the specified DeliveryPayment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $deliveryPayment = $this->deliveryPaymentRepository->find($id);

        if (empty($deliveryPayment)) {
            Flash::error('Delivery Payment not found');

            return redirect(route('deliveryPayments.index'));
        }

        return view('delivery_payments.edit')->with('deliveryPayment', $deliveryPayment);
    }

    /**
     * Update the specified DeliveryPayment in storage.
     *
     * @param int $id
     * @param UpdateDeliveryPaymentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryPaymentRequest $request)
    {
        $deliveryPayment = $this->deliveryPaymentRepository->find($id);

        if (empty($deliveryPayment)) {
            Flash::error('Delivery Payment not found');

            return redirect(route('deliveryPayments.index'));
        }

        $deliveryPayment = $this->deliveryPaymentRepository->update($request->all(), $id);

        Flash::success('Delivery Payment updated successfully.');

        return redirect(route('deliveryPayments.index'));
    }

    /**
     * Remove the specified DeliveryPayment from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryPayment = $this->deliveryPaymentRepository->find($id);

        if (empty($deliveryPayment)) {
            Flash::error('Delivery Payment not found');

            return redirect(route('deliveryPayments.index'));
        }

        $this->deliveryPaymentRepository->delete($id);

        Flash::success('Delivery Payment deleted successfully.');

        return redirect(route('deliveryPayments.index'));
    }
}
