<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFoodPartItemRequest;
use App\Http\Requests\UpdateFoodPartItemRequest;
use App\Repositories\FoodPartItemRepository;
use App\Models\FoodParts;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Auth;
use Response;

class FoodPartItemController extends AppBaseController
{
    /** @var  FoodPartItemRepository */
    private $foodPartItemRepository;

    public function __construct(FoodPartItemRepository $foodPartItemRepo)
    {
        $this->foodPartItemRepository = $foodPartItemRepo;
    }

    /**
     * Display a listing of the FoodPartItem.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $foodPartItems = $this->foodPartItemRepository->all();

        return view('food_part_items.index')
            ->with('foodPartItems', $foodPartItems);
    }



    public function food_part($restaurant_id, $food_part_id)
    {
        if($this->policy($restaurant_id)){
            return redirect()->back();
        }

        $foodPart = FoodParts::find($food_part_id);

        return view('food_part_items.index')
            ->with('foodPart', $foodPart);
    }

    /**
     * Show the form for creating a new FoodPartItem.
     *
     * @return Response
     */
    public function create($restaurant_id, $food_part_id)
    {
        if($this->policy($restaurant_id)){
            return redirect()->back();
        }

        $foodPart = FoodParts::find($food_part_id);
        return view('food_part_items.create', compact('foodPart'));
    }

    /**
     * Store a newly created FoodPartItem in storage.
     *
     * @param CreateFoodPartItemRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['price'] = $input['price'] ?? 0;
        $input['has_display_price'] = $input['price'] > 0 ? 1 : 0;

        $foodPartItem = $this->foodPartItemRepository->create($input);

        Flash::success('Food Part Item saved successfully.');

        return redirect("/restaurant"."/".$foodPartItem->food->restaurant->uuid."/".$foodPartItem->foodPart->id."/food_part_item");
    }

    /**
     * Display the specified FoodPartItem.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $foodPartItem = $this->foodPartItemRepository->find($id);

        if (empty($foodPartItem)) {
            Flash::error('Food Part Item not found');

            return redirect()->back();
        }

        return view('food_part_items.show')->with('foodPartItem', $foodPartItem);
    }

    /**
     * Show the form for editing the specified FoodPartItem.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $foodPartItem = $this->foodPartItemRepository->find($id);

        if (empty($foodPartItem)) {
            Flash::error('Food Part Item not found');
            return redirect()->back();
            // return redirect("/restaurant"."/".$food->restaurant->uuid."/".$foodParts->food_id."/food_part_item");
        }

        $foodPart = FoodParts::find($foodPartItem->food_part_id);

        return view('food_part_items.edit')
            ->with('foodPartItem', $foodPartItem)
            ->with('foodPart', $foodPart);
    }

    /**
     * Update the specified FoodPartItem in storage.
     *
     * @param int $id
     * @param UpdateFoodPartItemRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $foodPartItem = $this->foodPartItemRepository->find($id);

        if (empty($foodPartItem)) {
            Flash::error('Food Part Item not found');

            return redirect()->back();
            // return redirect("/restaurant"."/".$food->restaurant->uuid."/".$foodParts->food_id."/food_part_item");
        }

        $input = $request->all();
        $input['price'] = $input['price'] ?? 0;
        $input['has_display_price'] = $input['price'] > 0 ? 1 : 0;

        $foodPartItem = $this->foodPartItemRepository->update($input, $id);

        Flash::success('Food Part Item updated successfully.');

        return redirect("/restaurant"."/".$foodPartItem->food->restaurant->uuid."/".$foodPartItem->foodPart->id."/food_part_item");
    }

    /**
     * Remove the specified FoodPartItem from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $foodPartItem = $this->foodPartItemRepository->find($id);

        $foodPart = $foodPartItem->foodPart;

        if (empty($foodPartItem)) {
            Flash::error('Food Part Item not found');

            return redirect()->back();
            // return redirect("/restaurant"."/".$food->restaurant->uuid."/".$foodParts->food_id."/food_part_item");
        }

        $this->foodPartItemRepository->delete($id);

        Flash::success('Food Part Item deleted successfully.');

        return redirect("/restaurant"."/".$foodPart->restaurant->uuid."/".$foodPart->id."/food_part_item");
    }
    
    public function policy($restaurant_id){
        if(Auth::guard('restaurant')->check()){
            if($restaurant_id != Auth::guard('restaurant')->user()->uuid){
                Flash::error("Restaurant not found");
                return true;
            }
        }
        return false;
    }
}
