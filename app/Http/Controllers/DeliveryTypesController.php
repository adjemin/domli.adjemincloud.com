<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDeliveryTypesRequest;
use App\Http\Requests\UpdateDeliveryTypesRequest;
use App\Repositories\DeliveryTypesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DeliveryTypesController extends AppBaseController
{
    /** @var  DeliveryTypesRepository */
    private $deliveryTypesRepository;

    public function __construct(DeliveryTypesRepository $deliveryTypesRepo)
    {
        $this->deliveryTypesRepository = $deliveryTypesRepo;
    }

    /**
     * Display a listing of the DeliveryTypes.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryTypes = $this->deliveryTypesRepository->all();

        return view('delivery_types.index')
            ->with('deliveryTypes', $deliveryTypes);
    }

    /**
     * Show the form for creating a new DeliveryTypes.
     *
     * @return Response
     */
    public function create()
    {
        return view('delivery_types.create');
    }

    /**
     * Store a newly created DeliveryTypes in storage.
     *
     * @param CreateDeliveryTypesRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryTypesRequest $request)
    {
        $input = $request->all();

        $deliveryTypes = $this->deliveryTypesRepository->create($input);

        Flash::success('Delivery Types saved successfully.');

        return redirect(route('deliveryTypes.index'));
    }

    /**
     * Display the specified DeliveryTypes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deliveryTypes = $this->deliveryTypesRepository->find($id);

        if (empty($deliveryTypes)) {
            Flash::error('Delivery Types not found');

            return redirect(route('deliveryTypes.index'));
        }

        return view('delivery_types.show')->with('deliveryTypes', $deliveryTypes);
    }

    /**
     * Show the form for editing the specified DeliveryTypes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $deliveryTypes = $this->deliveryTypesRepository->find($id);

        if (empty($deliveryTypes)) {
            Flash::error('Delivery Types not found');

            return redirect(route('deliveryTypes.index'));
        }

        return view('delivery_types.edit')->with('deliveryTypes', $deliveryTypes);
    }

    /**
     * Update the specified DeliveryTypes in storage.
     *
     * @param int $id
     * @param UpdateDeliveryTypesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryTypesRequest $request)
    {
        $deliveryTypes = $this->deliveryTypesRepository->find($id);

        if (empty($deliveryTypes)) {
            Flash::error('Delivery Types not found');

            return redirect(route('deliveryTypes.index'));
        }

        $deliveryTypes = $this->deliveryTypesRepository->update($request->all(), $id);

        Flash::success('Delivery Types updated successfully.');

        return redirect(route('deliveryTypes.index'));
    }

    /**
     * Remove the specified DeliveryTypes from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryTypes = $this->deliveryTypesRepository->find($id);

        if (empty($deliveryTypes)) {
            Flash::error('Delivery Types not found');

            return redirect(route('deliveryTypes.index'));
        }

        $this->deliveryTypesRepository->delete($id);

        Flash::success('Delivery Types deleted successfully.');

        return redirect(route('deliveryTypes.index'));
    }
}
