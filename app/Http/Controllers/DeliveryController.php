<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDeliveryRequest;
use App\Http\Requests\UpdateDeliveryRequest;
use App\Repositories\DeliveryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DeliveryController extends AppBaseController
{
    /** @var  DeliveryRepository */
    private $deliveryRepository;

    public function __construct(DeliveryRepository $deliveryRepo)
    {
        $this->deliveryRepository = $deliveryRepo;
    }

    /**
     * Display a listing of the Delivery.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveries = $this->deliveryRepository->all();

        return view('deliveries.index')
            ->with('deliveries', $deliveries);
    }

    /**
     * Show the form for creating a new Delivery.
     *
     * @return Response
     */
    public function create()
    {
        return view('deliveries.create');
    }

    /**
     * Store a newly created Delivery in storage.
     *
     * @param CreateDeliveryRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryRequest $request)
    {
        $input = $request->all();

        $delivery = $this->deliveryRepository->create($input);

        Flash::success('Delivery saved successfully.');

        return redirect(route('deliveries.index'));
    }

    /**
     * Display the specified Delivery.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $delivery = $this->deliveryRepository->find($id);

        $account = \App\Models\DeliveryCompte::where('delivery_id', $delivery->id)->first();
        
        if (empty($delivery)) {
            Flash::error('Delivery not found');
            
            return redirect(route('deliveries.index'));
        }
        
        $account = \App\Models\DeliveryCompte::where('delivery_id', $delivery->id)->first();

        if(empty($account) || is_null($account)){
            $paymentDeliveries = \App\Models\DeliveryPayment::where(['deliverer_id' => $delivery->id])->get();

            $sumDelivery = collect($paymentDeliveries)->reduce(function($carry, $item){
                return $item->amount + $carry;
            }, 0);
            
            $account = \App\Models\DeliveryCompte::create([
                'delivery_id' => $delivery->id,
                'compte' => $sumDelivery
            ]);
        }

        return view('deliveries.show')->with('delivery', $delivery);
    }

    /**
     * Show the form for editing the specified Delivery.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $delivery = $this->deliveryRepository->find($id);

        if (empty($delivery)) {
            Flash::error('Delivery not found');

            return redirect(route('deliveries.index'));
        }

        return view('deliveries.edit')->with('delivery', $delivery);
    }

    /**
     * Update the specified Delivery in storage.
     *
     * @param int $id
     * @param UpdateDeliveryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryRequest $request)
    {
        $delivery = $this->deliveryRepository->find($id);

        if (empty($delivery)) {
            Flash::error('Delivery not found');

            return redirect(route('deliveries.index'));
        }

        $delivery = $this->deliveryRepository->update($request->all(), $id);

        Flash::success('Delivery updated successfully.');

        return redirect(route('deliveries.index'));
    }

    /**
     * Remove the specified Delivery from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $delivery = $this->deliveryRepository->find($id);

        if (empty($delivery)) {
            Flash::error('Delivery not found');

            return redirect(route('deliveries.index'));
        }

        $this->deliveryRepository->delete($id);

        Flash::success('Delivery deleted successfully.');

        return redirect(route('deliveries.index'));
    }


    public function change_state(Request $request, $id){
        $delivery = $this->deliveryRepository->find($id);
        $delivery->is_active = !$delivery->is_active;
        $delivery->save();
        Flash::success('Delivery update successfully');
        return redirect()->back();
    }
}
