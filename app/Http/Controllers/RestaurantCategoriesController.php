<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRestaurantCategoriesRequest;
use App\Http\Requests\UpdateRestaurantCategoriesRequest;
use App\Repositories\RestaurantCategoriesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Auth;
use App\Models\Restaurants;
// use App\Models\Category;
use App\Models\RestaurantCategories;
use Flash;
use Response;

class RestaurantCategoriesController extends AppBaseController
{
    /** @var  RestaurantCategoriesRepository */
    private $restaurantCategoriesRepository;

    public function __construct(RestaurantCategoriesRepository $restaurantCategoriesRepo)
    {
        $this->restaurantCategoriesRepository = $restaurantCategoriesRepo;
    }

    /**
     * Display a listing of the RestaurantCategories.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if(Auth::guard('restaurant')->check()){
            return redirect('/backOffice/restaurants/'.Auth::guard('restaurant')->user()->uuid);
        }

        $restaurantCategories = $this->restaurantCategoriesRepository->all();

        return view('restaurant_categories.index')
            ->with('restaurantCategories', $restaurantCategories);
    }

    public function categorie(Request $request, $restaurant_id)
    {
        $restaurant = Restaurant::find($restaurant_id);
        // $categories = RestaurantCategories::where(['restaurant_id' => $restaurant_id])->get();
        
        return view('restaurants.categorie_create',compact('restaurant'));
    }

    /**
     * Show the form for creating a new RestaurantCategories.
     *
     * @return Response
     */
    public function create()
    {
        $restaurant = Restaurant::find($restaurant_id);
        
        if(is_null($restaurant)){
            Flash::error('Restaurant not found');
            return redirect()->back();
        }
        $categories = RestaurantCategories::all();
        
        return view('restaurants.resto_category_create', compact('restaurant', 'categories'));
        // return view('restaurant_categories.create');
    }

    /**
     * Store a newly created RestaurantCategories in storage.
     *
     * @param CreateRestaurantCategoriesRequest $request
     *
     * @return Response
     */
    public function store(CreateRestaurantCategoriesRequest $request)
    {
        $input = $request->all();
        // dd($input);

        $restaurantCategories = $this->restaurantCategoriesRepository->create($input);
        // dd($restaurantCategories->restaurant->uuid);

        Flash::success('Restaurant Categories saved successfully.');
        $route = Auth::check() ? url("admin/restaurant/".$restaurantCategories->restaurant->uuid."/category") : url("restaurant/".$restaurantCategories->restaurant->uuid."/category");
        return redirect($route);
    }

    /**
     * Display the specified RestaurantCategories.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $restaurantCategories = $this->restaurantCategoriesRepository->find($id);

        // if(Auth::guard('restaurant')->check()){
        //     if($restaurantCategories->restaurant_id != Auth::guard('restaurant')->user()->id){
        //         Flash::error("Restaurant not found");
        //         return redirect()->back();
        //     }
        // }

        if (empty($restaurantCategories)) {
            Flash::error('Restaurant Categories not found');

            return redirect()->back();
            // return redirect(route('restaurantCategories.index'));
        }

        return view('restaurant_categories.show')->with('restaurantCategories', $restaurantCategories);
    }

    /**
     * Show the form for editing the specified RestaurantCategories.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $restaurantCategories = $this->restaurantCategoriesRepository->find($id);

        // if(Auth::guard('restaurant')->check()){
        //     if($restaurantCategories->restaurant_id != Auth::guard('restaurant')->user()->id){
        //         Flash::error("Restaurant not found");
        //         return redirect()->back();
        //     }
        // }

        if (empty($restaurantCategories)) {
            Flash::error('Restaurant Categories not found');

            return redirect()->back();
            // return redirect(route('restaurantCategories.index'));
        }

        return view('restaurant_categories.edit')->with('restaurantCategories', $restaurantCategories);
    }

    /**
     * Update the specified RestaurantCategories in storage.
     *
     * @param int $id
     * @param UpdateRestaurantCategoriesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRestaurantCategoriesRequest $request)
    {
        $restaurantCategories = $this->restaurantCategoriesRepository->find($id);

        // if(Auth::guard('restaurant')->check()){
        //     if($restaurantCategories->restaurant_id != Auth::guard('restaurant')->user()->id){
        //         Flash::error("Restaurant not found");
        //         return redirect()->back();
        //     }
        // }

        if (empty($restaurantCategories)) {
            Flash::error('Restaurant Categories not found');

            return redirect()->back();
            // return redirect(route('restaurantCategories.index'));
        }

        $restaurantCategories = $this->restaurantCategoriesRepository->update($request->all(), $id);

        Flash::success('Restaurant Categories updated successfully.');

        $route = Auth::check() ? url("admin/restaurant/".$restaurantCategories->restaurant->uuid."/category") : url("restaurant/".$restaurantCategories->restaurant->uuid."/category");

        return redirect($route);
        // return redirect(route('restaurantCategories.index'));
    }

    /**
     * Remove the specified RestaurantCategories from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $restaurantCategories = $this->restaurantCategoriesRepository->find($id);

        // if(Auth::guard('restaurant')->check()){
        //     if($restaurantCategories->restaurant_id != Auth::guard('restaurant')->user()->id){
        //         Flash::error("Restaurant not found");
        //         return redirect()->back();
        //     }
        // }

        if (empty($restaurantCategories)) {
            Flash::error('Restaurant Categories not found');

            return redirect()->back();
            // return redirect(route('restaurantCategories.index'));
        }

        $this->restaurantCategoriesRepository->delete($id);

        Flash::success('Restaurant Categories deleted successfully.');

        $route = Auth::check() ? url("admin/restaurant/".$restaurantCategories->restaurant->uuid."/category") : url("restaurant/".$restaurantCategories->restaurant->uuid."/category");

        return redirect($route);
        // return redirect(route('restaurantCategories.index'));
    }
}
