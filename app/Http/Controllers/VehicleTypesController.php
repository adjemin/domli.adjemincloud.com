<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVehicleTypesRequest;
use App\Http\Requests\UpdateVehicleTypesRequest;
use App\Repositories\VehicleTypesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class VehicleTypesController extends AppBaseController
{
    /** @var  VehicleTypesRepository */
    private $vehicleTypesRepository;

    public function __construct(VehicleTypesRepository $vehicleTypesRepo)
    {
        $this->vehicleTypesRepository = $vehicleTypesRepo;
    }

    /**
     * Display a listing of the VehicleTypes.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $vehicleTypes = $this->vehicleTypesRepository->all();

        return view('vehicle_types.index')
            ->with('vehicleTypes', $vehicleTypes);
    }

    /**
     * Show the form for creating a new VehicleTypes.
     *
     * @return Response
     */
    public function create()
    {
        return view('vehicle_types.create');
    }

    /**
     * Store a newly created VehicleTypes in storage.
     *
     * @param CreateVehicleTypesRequest $request
     *
     * @return Response
     */
    public function store(CreateVehicleTypesRequest $request)
    {
        $input = $request->all();

        $vehicleTypes = $this->vehicleTypesRepository->create($input);

        Flash::success('Vehicle Types saved successfully.');

        return redirect(route('vehicleTypes.index'));
    }

    /**
     * Display the specified VehicleTypes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $vehicleTypes = $this->vehicleTypesRepository->find($id);

        if (empty($vehicleTypes)) {
            Flash::error('Vehicle Types not found');

            return redirect(route('vehicleTypes.index'));
        }

        return view('vehicle_types.show')->with('vehicleTypes', $vehicleTypes);
    }

    /**
     * Show the form for editing the specified VehicleTypes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $vehicleTypes = $this->vehicleTypesRepository->find($id);

        if (empty($vehicleTypes)) {
            Flash::error('Vehicle Types not found');

            return redirect(route('vehicleTypes.index'));
        }

        return view('vehicle_types.edit')->with('vehicleTypes', $vehicleTypes);
    }

    /**
     * Update the specified VehicleTypes in storage.
     *
     * @param int $id
     * @param UpdateVehicleTypesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVehicleTypesRequest $request)
    {
        $vehicleTypes = $this->vehicleTypesRepository->find($id);

        if (empty($vehicleTypes)) {
            Flash::error('Vehicle Types not found');

            return redirect(route('vehicleTypes.index'));
        }

        $vehicleTypes = $this->vehicleTypesRepository->update($request->all(), $id);

        Flash::success('Vehicle Types updated successfully.');

        return redirect(route('vehicleTypes.index'));
    }

    /**
     * Remove the specified VehicleTypes from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $vehicleTypes = $this->vehicleTypesRepository->find($id);

        if (empty($vehicleTypes)) {
            Flash::error('Vehicle Types not found');

            return redirect(route('vehicleTypes.index'));
        }

        $this->vehicleTypesRepository->delete($id);

        Flash::success('Vehicle Types deleted successfully.');

        return redirect(route('vehicleTypes.index'));
    }
}
