<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePaymentMethodesRequest;
use App\Http\Requests\UpdatePaymentMethodesRequest;
use App\Repositories\PaymentMethodesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PaymentMethodesController extends AppBaseController
{
    /** @var  PaymentMethodesRepository */
    private $paymentMethodesRepository;

    public function __construct(PaymentMethodesRepository $paymentMethodesRepo)
    {
        $this->paymentMethodesRepository = $paymentMethodesRepo;
    }

    /**
     * Display a listing of the PaymentMethodes.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $paymentMethodes = $this->paymentMethodesRepository->all();

        return view('payment_methodes.index')
            ->with('paymentMethodes', $paymentMethodes);
    }

    /**
     * Show the form for creating a new PaymentMethodes.
     *
     * @return Response
     */
    public function create()
    {
        return view('payment_methodes.create');
    }

    /**
     * Store a newly created PaymentMethodes in storage.
     *
     * @param CreatePaymentMethodesRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentMethodesRequest $request)
    {
        $input = $request->all();

        $paymentMethodes = $this->paymentMethodesRepository->create($input);

        Flash::success('Payment Methodes saved successfully.');

        return redirect(route('paymentMethodes.index'));
    }

    /**
     * Display the specified PaymentMethodes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $paymentMethodes = $this->paymentMethodesRepository->find($id);

        if (empty($paymentMethodes)) {
            Flash::error('Payment Methodes not found');

            return redirect(route('paymentMethodes.index'));
        }

        return view('payment_methodes.show')->with('paymentMethodes', $paymentMethodes);
    }

    /**
     * Show the form for editing the specified PaymentMethodes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $paymentMethodes = $this->paymentMethodesRepository->find($id);

        if (empty($paymentMethodes)) {
            Flash::error('Payment Methodes not found');

            return redirect(route('paymentMethodes.index'));
        }

        return view('payment_methodes.edit')->with('paymentMethodes', $paymentMethodes);
    }

    /**
     * Update the specified PaymentMethodes in storage.
     *
     * @param int $id
     * @param UpdatePaymentMethodesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentMethodesRequest $request)
    {
        $paymentMethodes = $this->paymentMethodesRepository->find($id);

        if (empty($paymentMethodes)) {
            Flash::error('Payment Methodes not found');

            return redirect(route('paymentMethodes.index'));
        }

        $paymentMethodes = $this->paymentMethodesRepository->update($request->all(), $id);

        Flash::success('Payment Methodes updated successfully.');

        return redirect(route('paymentMethodes.index'));
    }

    /**
     * Remove the specified PaymentMethodes from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $paymentMethodes = $this->paymentMethodesRepository->find($id);

        if (empty($paymentMethodes)) {
            Flash::error('Payment Methodes not found');

            return redirect(route('paymentMethodes.index'));
        }

        $this->paymentMethodesRepository->delete($id);

        Flash::success('Payment Methodes deleted successfully.');

        return redirect(route('paymentMethodes.index'));
    }
}
