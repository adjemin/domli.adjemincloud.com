<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFoodRequest;
use App\Http\Requests\UpdateFoodRequest;
use App\Repositories\FoodRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Food;
use App\Models\Restaurants;
use App\Models\RestaurantMenus;
use App\Models\FoodCategories;
use App\Models\FoodParts;
use Flash;
use Imgur;
use Auth;
use Response;

class FoodController extends AppBaseController
{
    /** @var  FoodRepository */
    private $foodRepository;

    public function __construct(FoodRepository $foodRepo)
    {
        $this->foodRepository = $foodRepo;
    }

    /**
     * Display a listing of the Food.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $food = $this->foodRepository->all();

        return view('food.index')
            ->with('food', $food);
    }

    public function foodsByRestaurant($restaurant_id, Request $request){

        $restaurant = Restaurants::where(['id' => $restaurant_id])->orWhere(['uuid' => $restaurant_id])->first();
        
        if(Auth::guard('restaurant')->check()){
            if($restaurant->id != Auth::guard('restaurant')->user()->id){
                Flash::error("Restaurant not found");
                return redirect()->back();
            }
        }

        if(is_null($restaurant) || empty($restaurant)){
            Flash::error("Restaurant not found");
            return redirect()->back();
        }

        $food = Food::where(['restaurant_id' => $restaurant->id])->get();

        return view('food.index')
            ->with('food', $food)
            ->with('restaurant', $restaurant);
    }

    /**
     * Show the form for creating a new Food.
     *
     * @return Response
     */
    public function create($restaurant_id)
    {
        $restaurant = Restaurants::where(['id' => $restaurant_id])->orWhere(['uuid' => $restaurant_id])->first();

        if(!$restaurant){
            Flash::error("Restaurant not found");
            return redirect()->back();
        }

        $menus = RestaurantMenus::where('restaurant_id', $restaurant->id)->get();
        $categories = FoodCategories::all();


        return view('food.create', compact('restaurant', 'menus', 'categories'));
    }

    /**
     * Store a newly created Food in storage.
     *
     * @param CreateFoodRequest $request
     *
     * @return Response
     */
    public function store(CreateFoodRequest $request)
    {
        // $input = $request->all();
        // dd($request->all());
        
        $this->validate($request, [
            // 'image_file.*' => 'required|image',
            'image_file' => 'required|image',
            'title' => 'required'
        ]);

        $image = $request->file('image_file');
        $logoImage = Imgur::upload($image);
        $logoImageLink = $logoImage->link();

        $input = [
            "quantity" => $request->quantity ?? 0,
            "restaurant_id" => $request->restaurant_id,
            "menu_id" => $request->menu_id,
            "food_category_id" => $request->category_id ?? $request->categorie_id,
            "title" => $request->title,
            "price" => floatval($request->price) ?? floatval($request->base_price) ?? 0,
            "currency_code" => $request->currency_code ?? 'USD',
            "currency_name" => $request->currency_name ?? 'Dollar',
            "image" => $logoImageLink ?? '',
            "has_part" => $request->has_part ?? 0,
            "description" => $request->description,
            'uuid' => \App\Utils\UUID::v4()
        ];

        $food = $this->foodRepository->create($input);

        if($food->price == 0){
            $food->price = floatval($request->price);
            $food->save();
        }

        Flash::success('Food saved successfully.');

        // return redirect("foods_restaurant/$food->restaurant_id");
        
        $route = Auth::check() ? route('admin-restaurant-food-index', ['restaurant_id' => $food->restaurant->uuid]) : route('backoffice-restaurant-food-index', ['restaurant_id' => $food->restaurant->uuid]) ;
        return redirect($route);
        // return redirect(route('food.index'));
    }

    /**
     * Display the specified Food.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $food = $this->foodRepository->find($id);

        if(Auth::guard('restaurant')->check()){
            if($food->restaurant_id != Auth::guard('restaurant')->user()->id){
                Flash::error("Restaurant not found");
                return redirect()->back();
            }
        }

        if (empty($food)) {
            Flash::error('Food not found');

            // return redirect("foods_restaurant/$food->restaurant_id");
            return redirect()->back();
            // return redirect(route('food.index'));
        }

        return view('food.show')->with('food', $food);
    }

    /**
     * Show the form for editing the specified Food.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // $food = $this->foodRepository->find($id);
        $food = Food::where(['id' => $id])->orWhere(['uuid' => $id])->first();
        
        if(Auth::guard('restaurant')->check()){
            if($food->restaurant_id != Auth::guard('restaurant')->user()->id){
                Flash::error("Restaurant not found");
                return redirect()->back();
            }
        }

        if (empty($food)) {
            Flash::error('Food not found');

            // return redirect("foods_restaurant/$food->restaurant_id");
            return redirect()->back();
            // return redirect(route('food.index'));
        }
        $restaurant = Restaurants::find($food->restaurant_id);
        return view('food.edit',compact('food', 'restaurant'));
        // return view('food.edit')->with('food', $food);
    }

    /**
     * Update the specified Food in storage.
     *
     * @param int $id
     * @param UpdateFoodRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFoodRequest $request)
    {
        // $food = $this->foodRepository->find($id);
        $food = Food::where(['id' => $id])->orWhere(['uuid' => $id])->first();

        if(Auth::guard('restaurant')->check()){
            if($food->restaurant_id != Auth::guard('restaurant')->user()->id){
                Flash::error("Restaurant not found");
                return redirect()->back();
            }
        }

        if (empty($food)) {
            Flash::error('Food not found');

            // return redirect("foods_restaurant/$request->restaurant_id");
            return redirect()->back();
            // return redirect(route('food.index'));
        }

        if($request->file('image')){
            $logoImage = Imgur::upload($request->file('image'));
            $logoImageLink = $logoImage->link();
        }

        $input = [
            "quantity" => $request->quantity,
            "restaurant_id" => $request->restaurant_id ?? $food->restaurant_id,
            "menu_id" => $request->menu_id ?? $food->menu_id,
            "food_category_id" => $request->category_id ?? $food->categorie_id,
            "title" => $request->title ?? $food->title,
            "price" => floatval($request->base_price) ?? floatval($request->price) ?? 0,
            "currency_code" => $request->currency_code ?? $food->currency_code,
            "currency_name" => $request->currency_name,
            "image" => $logoImageLink ?? $food->image ?? '',
            "has_part" => $request->has_part ??  $food->has_part ?? 0,
            "description" => $request->description ?? $food->description
        ];

        $food = $this->foodRepository->update($request->all(), $id);

        Flash::success('Food updated successfully.');

        
        $route = Auth::check() ? route('admin-restaurant-food-index', ['restaurant_id' => $food->restaurant->uuid]) : route('backoffice-restaurant-food-index', ['restaurant_id' => $food->restaurant->uuid]) ;
        return redirect($route);
        // return redirect("foods_restaurant/$food->restaurant_id");
    }

    /**
     * Remove the specified Food from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        // $food = $this->foodRepository->find($id);
        $food = Food::where(['id' => $id])->orWhere(['uuid' => $id])->first();

        if(Auth::guard('restaurant')->check()){
            if($food->restaurant_id != Auth::guard('restaurant')->user()->id){
                Flash::error("Restaurant not found");
                return redirect()->back();
            }
        }

        if (empty($food)) {
            Flash::error('Food not found');

            return redirect()->back();
            // return redirect("foods_restaurant/$food->restaurant_id");
            // return redirect(route('food.index'));
        }

        $this->foodRepository->delete($id);

        Flash::success('Food deleted successfully.');

        $route = Auth::check() ? route('admin-restaurant-food-index', ['restaurant_id' => $food->restaurant->uuid]) : route('backoffice-restaurant-food-index', ['restaurant_id' => $food->restaurant->uuid]) ;
        return redirect($route);
        // return redirect(route('food.index'));
    }

    public function getFoodData(Request $request){
        $input = $request->all() ?? $request->json()->all();
        // dd($input);

        $food = Food::where('uuid',$input['id'])->first();

        $foodPart = FoodParts::where('food_id', $food);
        $data = [
            'food' => [
                'uuid' => $food->uuid,
                'title' => $food->title,
                'description' => $food->description,
                'image'  => $food->image,
                'price'  => $food->price ?? $food->base_price ?? 0,
            ],
            'foodParts' => $food->parts,
            'foodPartItems' => collect($food->parts)->map(function($foodPart){
                return $foodPart->items;
            })->toArray()
        ];
        return json_encode($data);
        // return $this->sendResponse(view('frontOffice.pages.dynamic_modal_food_detail', ['food' => $data['food'], 'foodParts' => $data['foodParts'], 'foodPartItems' => $data['foodPartItems']])->render(), 'food data retrieved success');
    }

    public function sendResponse($data, $message)
    {
        return Response::json([
            'success' => true,
            'data'    => $data,
            'message' => $message,
        ]);
    }

    public function stock(Request $request, $food_id){
        $food = Food::where(['id' => $food_id])->orWhere(["uuid" => $food_id])->first();

        if(Auth::guard('restaurant')->check()){
            if($food->restaurant_id != Auth::guard('restaurant')->user()->id){
                Flash::error("Restaurant not found");
                return redirect()->back();
            }
        }

        if (empty($food)) {
            Flash::error('Food not found');

            return redirect()->back();
            // return redirect(route('customers.index'));
        }

        $input = $request->all();
        
        $food->quantity += intval($input['quantity']);
        $food->save();

        Flash::success('Food update successfully');
        return redirect()->back();
    }
}
