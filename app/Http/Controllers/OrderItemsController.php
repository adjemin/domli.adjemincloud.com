<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderItemsRequest;
use App\Http\Requests\UpdateOrderItemsRequest;
use App\Repositories\OrderItemsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OrderItemsController extends AppBaseController
{
    /** @var  OrderItemsRepository */
    private $orderItemsRepository;

    public function __construct(OrderItemsRepository $orderItemsRepo)
    {
        $this->orderItemsRepository = $orderItemsRepo;
    }

    /**
     * Display a listing of the OrderItems.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orderItems = $this->orderItemsRepository->all();

        return view('order_items.index')
            ->with('orderItems', $orderItems);
    }

    /**
     * Show the form for creating a new OrderItems.
     *
     * @return Response
     */
    public function create()
    {
        return view('order_items.create');
    }

    /**
     * Store a newly created OrderItems in storage.
     *
     * @param CreateOrderItemsRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderItemsRequest $request)
    {
        $input = $request->all();

        $orderItems = $this->orderItemsRepository->create($input);

        Flash::success('Order Items saved successfully.');

        return redirect(route('orderItems.index'));
    }

    /**
     * Display the specified OrderItems.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orderItems = $this->orderItemsRepository->find($id);

        if (empty($orderItems)) {
            Flash::error('Order Items not found');

            return redirect(route('orderItems.index'));
        }

        return view('order_items.show')->with('orderItems', $orderItems);
    }

    /**
     * Show the form for editing the specified OrderItems.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orderItems = $this->orderItemsRepository->find($id);

        if (empty($orderItems)) {
            Flash::error('Order Items not found');

            return redirect(route('orderItems.index'));
        }

        return view('order_items.edit')->with('orderItems', $orderItems);
    }

    /**
     * Update the specified OrderItems in storage.
     *
     * @param int $id
     * @param UpdateOrderItemsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderItemsRequest $request)
    {
        $orderItems = $this->orderItemsRepository->find($id);

        if (empty($orderItems)) {
            Flash::error('Order Items not found');

            return redirect(route('orderItems.index'));
        }

        $orderItems = $this->orderItemsRepository->update($request->all(), $id);

        Flash::success('Order Items updated successfully.');

        return redirect(route('orderItems.index'));
    }

    /**
     * Remove the specified OrderItems from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orderItems = $this->orderItemsRepository->find($id);

        if (empty($orderItems)) {
            Flash::error('Order Items not found');

            return redirect(route('orderItems.index'));
        }

        $this->orderItemsRepository->delete($id);

        Flash::success('Order Items deleted successfully.');

        return redirect(route('orderItems.index'));
    }
}
