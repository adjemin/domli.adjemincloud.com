<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderHistoriesRequest;
use App\Http\Requests\UpdateOrderHistoriesRequest;
use App\Repositories\OrderHistoriesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OrderHistoriesController extends AppBaseController
{
    /** @var  OrderHistoriesRepository */
    private $orderHistoriesRepository;

    public function __construct(OrderHistoriesRepository $orderHistoriesRepo)
    {
        $this->orderHistoriesRepository = $orderHistoriesRepo;
    }

    /**
     * Display a listing of the OrderHistories.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orderHistories = $this->orderHistoriesRepository->all();

        return view('order_histories.index')
            ->with('orderHistories', $orderHistories);
    }

    /**
     * Show the form for creating a new OrderHistories.
     *
     * @return Response
     */
    public function create()
    {
        return view('order_histories.create');
    }

    /**
     * Store a newly created OrderHistories in storage.
     *
     * @param CreateOrderHistoriesRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderHistoriesRequest $request)
    {
        $input = $request->all();

        $orderHistories = $this->orderHistoriesRepository->create($input);

        Flash::success('Order Histories saved successfully.');

        return redirect(route('orderHistories.index'));
    }

    /**
     * Display the specified OrderHistories.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orderHistories = $this->orderHistoriesRepository->find($id);

        if (empty($orderHistories)) {
            Flash::error('Order Histories not found');

            return redirect(route('orderHistories.index'));
        }

        return view('order_histories.show')->with('orderHistories', $orderHistories);
    }

    /**
     * Show the form for editing the specified OrderHistories.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orderHistories = $this->orderHistoriesRepository->find($id);

        if (empty($orderHistories)) {
            Flash::error('Order Histories not found');

            return redirect(route('orderHistories.index'));
        }

        return view('order_histories.edit')->with('orderHistories', $orderHistories);
    }

    /**
     * Update the specified OrderHistories in storage.
     *
     * @param int $id
     * @param UpdateOrderHistoriesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderHistoriesRequest $request)
    {
        $orderHistories = $this->orderHistoriesRepository->find($id);

        if (empty($orderHistories)) {
            Flash::error('Order Histories not found');

            return redirect(route('orderHistories.index'));
        }

        $orderHistories = $this->orderHistoriesRepository->update($request->all(), $id);

        Flash::success('Order Histories updated successfully.');

        return redirect(route('orderHistories.index'));
    }

    /**
     * Remove the specified OrderHistories from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orderHistories = $this->orderHistoriesRepository->find($id);

        if (empty($orderHistories)) {
            Flash::error('Order Histories not found');

            return redirect(route('orderHistories.index'));
        }

        $this->orderHistoriesRepository->delete($id);

        Flash::success('Order Histories deleted successfully.');

        return redirect(route('orderHistories.index'));
    }
}
