<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\FoodCategories;
use App\Models\RestaurantCategories;
use App\Models\Restaurants;
use Illuminate\Support\Facades\DB;

class RestaurantCategoriesComp extends Component
{
    public function render()
    {
        // $restaurant_categories = session('RestaurantWithCategorie');
        // $restaurants = DB::table('restaurants')->get();
        // $restaurant_categories = collect($restaurants)->groupBy('categorie_id');
        // $restaurant_categories->all();
        
        $food_categorires = FoodCategories::all();
        $food_categorires = collect($food_categorires)->filter(function($food_category){
            return $food_category->foods->count() > 0;
        })->sortByDesc(function($food_category, $key){
            return $food_category->foods->count();
        });
        // dd($RestaurantCategories);

        $restaurant_categories = RestaurantCategories::whereIn('category_id', collect($food_categorires)->map(function($food_category){
            return $food_category->id;
        })->toArray())->get()->groupBy(function($restaurantCategorie){
            return $restaurantCategorie->category_id;
        })->sortBy(function($value, $key){
            return $key;
        });

        // dd($restaurant_categories);

        $food_categorires = $food_categorires->whereIn('id', collect($restaurant_categories)->map(function($restaurant, $key){
            return intval($key);
        }))->sortby(function($food_categorire){
            return $food_categorire->id;
        });

        // dd($food_categorires);

        // $restaurant_categories = collect($restaurants)->groupBy('categorie_id');

        return view('livewire.restaurant-categories-comp', compact('restaurant_categories', 'food_categorires'));
    }
}
