<?php

namespace App\Http\Livewire;

use Livewire\Component;

class TestComp extends Component
{
    public $test;

    public function render()
    {
        return view('livewire.test-comp');
    }
}
