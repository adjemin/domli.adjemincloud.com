<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\FoodCategories;
use App\Models\Restaurants;
// use Illuminate\Support\Facades\DB;

class SuggestionComp extends Component
{
    public function render()
    {
        $food_categories = FoodCategories::all();
        $food_categories = collect($food_categories)->filter(function($food_category){
            return $food_category->foods->count() > 0;
        })->sortByDesc(function($food_category, $key){
            return $food_category->foods->count();
        })->take(6);

        // dd($food_categories);

        // $restaurants = Restaurants::whereIn('categorie_id', collect($food_categories)->map(function($food_category){
        //     return $food_category->id;
        // })->toArray())->get();

        // $food_categories = $food_categories->whereIn('id', collect($restaurants)->map(function($restaurant){
        //     return $restaurant->categorie_id;
        // }));
        // dd($food_categories);
        return view('livewire.suggestion-comp', compact('food_categories'));
    }
}
