<?php

namespace App\Http\Livewire;

use App\Models\Food;
use Livewire\Component;
use Laracasts\Flash\Flash;
use App\Models\Restaurants;
use App\Models\FoodCategories;
use App\Models\RestaurantCategories;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class RestaurantFront extends Component
{
    public $restaurant;
    public $search;
    public $foods;
    public $categories;
    public $restaurant_categories;
    public $food;
    public $food_categories;
    public $food_alls;
    public $plus;
    public $sidik = "mon message";

    public function searchFood(Request $request){

        // $foods = Food::where('restaurant_id', intval($request->resto))->get();

        $foods = Food::LeftJoin('food_categories','food.food_category_id', '=', 'food_categories.id')
            ->LeftJoin('menuses', 'food.menu_id', '=', 'menuses.id')
            ->select('food.*')
            ->where('food.restaurant_id', '=', intval($request->resto))
            ->orWhere('food_categories.title', 'like', '%'.$request->title.'%')
            ->orWhere('food_categories.description', 'like', '%'.$request->title.'%')
            ->orWhere('menuses.title', 'like', '%'.$request->title.'%')
            ->orWhere('menuses.slug', 'like', '%'.$request->title.'%')
            ->get();

        // dd($foods);

        //filter one more time for get restaurant's food
        $foods = collect($foods)->filter(function($food, $key) use ($request) {
            return  $food->price == intval($request->title) || $food->restaurant_id ==  $request->resto || (!is_null($food->menu) && (strpos($food->menu->title, $request->title) !== false || strpos($food->menu->slug, $request->title) !== false) ) || (!is_null($food->food_category) && (strpos($food->food_category->title, $request->title) !== false || strpos($food->food_category->slug, $request->title) !== false || strpos($food->food_category->slug, $request->title) !== false ) );
        });

        $request->session()->put('foods', $foods);
        $request->session()->put('search', $request->title);
        return redirect("/restaurant/$request->resto");
        // dd($restaurants);
    }

    public function searchRestaurant(Request $request){
        $restaurants =  Restaurants::LeftJoin('restaurant_categories', 'restaurants.categorie_id', '=','restaurant_categories.id')
                            ->select('restaurants.*')
                            ->where('restaurants.name', 'like','%'.$request->search.'%')
                            ->orWhere('restaurants.surname', 'like','%'.$request->search.'%')
                            ->orWhere('restaurant_categories.title', 'like','%'.$request->search.'%')
                            ->orWhere('restaurant_categories.slug', 'like','%'.$request->search.'%')
                            ->orWhere('restaurants.surname', 'like','%'.$request->search.'%')
                            ->orWhere(function($query) use ($request){
                                $query->geofence($request->session()->get('user_ip_address')['latitude'] ?? \Auth::guard('customer')->user()->lat ?? 5.39150, $request->session()->get('user_ip_address')['latitude'] ?? \Auth::guard('customer')->user()->lng ?? -3.97765, 0, 15);
                            })->get();

        $request->session()->put('restaurants', $restaurants);
        $request->session()->put('search', $request->search);
        return redirect("/restaurants");
        // dd($restaurants);
    }

    public function addPlus(){
        $this->plus += 10;
    }
    public function showCategorie(){
        // dd("ok");
        $this->restaurants = Restaurants::where('slug', $slug);
    }


    // public function show(Request $request, $id)
    // {
    //     $categories = $request->query("categories") ?? 'all';

    //     $restaurant = Restaurants::where('id', $id)->first();

    //     if(empty($restaurant) || is_null($restaurant)){
    //         return redirect()->back();
    //     }

    //     if($categories != "all"){
    //         $foods = Food::LeftJoin('food_categories', 'food_categories.id', '=', 'food.food_category_id')
    //             ->select('food.*')->where('food_categories.title', $categories)->where('restaurant_id', $restaurant->id)->get();

    //         $food_alls = Food::where('restaurant_id', $restaurant->id)->get();

    //         $food_categories_ids = collect($food_alls)->map(function($food){
    //             return $food->food_category_id;
    //         })->unique()->toArray();
    //     }else{
    //         if($request->session()->exists('foods')){
    //             $foods = $request->session()->get('foods');
    //             $request->session()->forget('foods');

    //             $categories ='Search';

    //             $food_alls = Food::where('restaurant_id', $restaurant->id)->get();

    //             $food_categories_ids = collect($food_alls)->map(function($food){
    //                 return $food->food_category_id;
    //             })->unique()->toArray();
    //         }else{
    //             $foods = Food::where('restaurant_id', $restaurant->id)->get();
    //             $food_categories_ids = collect($foods)->map(function($food){
    //                 return $food->food_category_id;
    //             })->unique()->toArray();
    //         }

    //     }


    //     $food_categories = FoodCategories::whereIn('id', $food_categories_ids)->get();

    //     if (empty($restaurant)) {
    //         Flash::error('Restaurant not found');

    //         return redirect()->back();
    //     }

    //     $userCoordinates = $request->session()->get('user_ip_address');
    //     if($request->session()->exists('search')){
    //         $search = $request->session()->get('search');
    //         $request->session()->forget('search');
    //         return view('frontOffice.pages.restaurant', compact('restaurant', 'foods', 'food_categories', 'categories', 'userCoordinates', 'search'));
    //     }
    //     return view('frontOffice.pages.restaurant', compact('restaurant', 'foods', 'food_categories', 'categories', 'userCoordinates'));
    // }

    public function index(Request $request)
    {
        $categories = $request->query("categories") ?? 'all';
        $restaurant_categories = RestaurantCategories::all();
        // dd($categories);
        if($categories != "all"){
            $restaurant_categorie = RestaurantCategories::where('slug', $categories)->first();

            $restaurants = Restaurants::where('categorie_id', $restaurant_categorie->id)->inRandomOrder()->get();
            $others = Restaurants::whereNotIn('categorie_id', [$restaurant_categorie->id])->inRandomOrder()->take(10)->get();

            // $restaurants = Restaurants::Leftjoin('restaurant_categories', 'restaurants.id', '=', 'restaurant_categories.restaurant_id')->select('restaurants.*')->where('restaurant_categories.slug', $categories)->inRandomOrder()->take(10)->get();
            // $others = Restaurants::Leftjoin('restaurant_categories', 'restaurants.id', '=', 'restaurant_categories.restaurant_id')->select('restaurants.*')->where('restaurant_categories.slug', '<>',$categories)->inRandomOrder()->take(10)->get();
        }else{
            if($request->session()->exists('restaurants')){
                $categories = 'Search';
                $restaurants = $request->session()->get('restaurants');
                $request->session()->forget('restaurants');
            }else{
                $restaurants = Restaurants::inRandomOrder()->take(10)->get();
            }

            $restaurant_ids = collect($restaurants)->map(function($restaurant){
                return $restaurant->id;
            });

            $others = Restaurants::whereNotIn('id', $restaurant_ids)->inRandomOrder()->take(10)->get();
        }


        $pageInfo = ["title" => "All restaurants"];
        $userCoordinates = $request->session()->get('user_ip_address');

        if($request->session()->exists('search')){
            $search = $request->session()->get('search');
            $request->session()->forget('search');
            return view('livewire.restaurant-food', compact('restaurants', 'others','pageInfo', 'restaurant_categories', 'categories', 'userCoordinates', 'search'));
        }
        return view('livewire.restaurant-food', compact('restaurants', 'others','pageInfo', 'restaurant_categories', 'categories', 'userCoordinates'));

    }

    public function mount(Request $request, $id){

        $this->restaurant_categories = $request->query("categories") ?? 'all';

        $this->restaurant = Restaurants::where('id', $id)->first();

        if (empty($this->restaurant) || is_null($this->restaurant)){
            return redirect()->back();
        }

        if ($this->restaurant_categories != "all"){

            $this->foods = Food::LeftJoin('food_categories', 'food_categories.id', '=', 'food.food_category_id')
                ->select('food.*')->where('food_categories.title', $this->restaurant_categories)->where('restaurant_id', $this->restaurant->id)->get();
            $this->food_alls = Food::where('restaurant_id', $this->restaurant->id)->get();

            $food_categories_ids = collect($this->food_alls)->map(function($food){
                return $food->food_category_id;
            })->unique()->toArray();
        }else{
            if($request->session()->exists('foods')){
                $this->foods = $request->session()->get('foods');
                $request->session()->forget('foods');

                $this->restaurant_categories ='Search';

                $this->food_alls = Food::where('restaurant_id', $this->restaurant->id)->get();

                $food_categories_ids = collect($this->food_alls)->map(function($food){
                    return $food->food_category_id;
                })->unique()->toArray();
                // dd($food_categories_ids);
            }else{

                $this->foods = Food::where('restaurant_id', $this->restaurant->id)->get();
                $food_categories_ids = collect($this->foods)->map(function($food){
                    return $food->food_category_id;
                })->unique()->toArray();
                // dd($food_categories_ids);
            }

        }


        $this->food_categories = FoodCategories::whereIn('id', $food_categories_ids)->get();

        if (empty($restaurant)) {
            Flash::error('Restaurant not found');

            return redirect()->back();
        }

        // $userCoordinates = $request->session()->get('user_ip_address');
        // if($request->session()->exists('search')){
        //     $search = $request->session()->get('search');
        //     $request->session()->forget('search');
        //     // return view('frontOffice.pages.restaurant', compact('restaurant', 'foods', 'food_categories', 'categories', 'userCoordinates', 'search'));
        // }
    }

    public function render()
    {
        return view('livewire.restaurant');
    }
}
