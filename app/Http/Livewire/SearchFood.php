<?php

namespace App\Http\Livewire;

use App\Models\Food;
use App\Models\FoodCategories;
use App\Models\RestaurantCategories;
use App\Models\RestaurantMenus;
use App\Models\Restaurants;
use Livewire\Component;

class SearchFood extends Component
{
    public $categories;
    public $restaurant;
    public $foods;
    public $food_categories;

    public $resultByMenu;
    public $query;

    public function mount()
    {
        $arr = explode('/',url()->current());
        $id = end($arr);
        $this->foods = session('food_categories');
        $this->categories = session('categories');
        // $this->restaurant = session('restaurant')->where('id', $id)->first();
        $this->restaurant = Restaurants::find($id);
        $this->food_categories = session('food_categories');
    }

    public function searchByCategory($id){
            // dd($this->restaurant->id);
        $searching_foods = Food::where('menu_id', $id)->orWhere('restaurant_id',$this->restaurant->id)->get();

        $this->resultByMenu = $searching_foods;
    }


    public function render()
    {
        // dd( $this->categories);

        // $foods =  $restaurant_cat->getFoodAttribute();

        // $foods->where('title', $this->query)->get();

        // $food_categories =
        //     Food::LeftJoin('food_categories', 'food_categories.id', '=', 'food.food_category_id')
        //     ->select('food.*')
        //     ->where('food_categories.title',  )
        //     ->where('restaurant_id', $restaurant->id)->get();

        // dd($food_categories);



        // $restaurant = Restaurants::where('id', $id)->first();

        // if(empty($restaurant) || is_null($restaurant)){
        //     return redirect()->back();
        // }

        // if($this->categories != "all"){
        //     $foods = Food::LeftJoin('food_categories', 'food_categories.id', '=', 'food.food_category_id')
        //     ->select('food.*')->where('food_categories.title', $this->categories)->where('restaurant_id', $restaurant->id)->get();

        //     $food_alls = Food::where('restaurant_id', $restaurant->id)->get();

        //     $food_categories_ids = collect($food_alls)->map(function($food){
        //         return $food->food_category_id;
        //     })->unique()->toArray();

        // }else{
        //     if($request->session()->exists('foods')){
        //         $foods = $request->session()->get('foods');
        //         $request->session()->forget('foods');

        //         $categories ='Search';

        //         $food_alls = Food::where('restaurant_id', $restaurant->id)->get();

        //         $food_categories_ids = collect($food_alls)->map(function($food){
        //             return $food->food_category_id;
        //         })->unique()->toArray();
        //     }else{
        //         $foods = Food::where('restaurant_id', $restaurant->id)->get();
        //         $food_categories_ids = collect($foods)->map(function($food){
        //             return $food->food_category_id;
        //         })->unique()->toArray();
        //     }

        // }


        // $food_categories = FoodCategories::whereIn('id', $food_categories_ids)->get();

        // if (empty($restaurant)) {
        //     Flash::error('Restaurant not found');

        //     return redirect()->back();
        // }

        // $userCoordinates = $request->session()->get('user_ip_address');
        // if($request->session()->exists('search')){
        //     $search = $request->session()->get('search');
        //     $request->session()->forget('search');
        //     return view('frontOffice.pages.restaurant', compact('restaurant', 'foods', 'food_categories', 'categories', 'userCoordinates', 'search'));
        // }

        // $foods = session('foods');
        // $food_categories = session('food_categories');
        // $categories = session('categories');
        // $userCoordinates = session('userCoordinates');


        // dd($restaurant);



        $searching_foods = Food::where('restaurant_id', $this->restaurant->id)->get();
        // ->where('title', 'like', '%'. $this->query . '%')

        if($this->query != null){
            // dd($restaurant->id);
            $searching_foods = Food::where('restaurant_id', $this->restaurant->id)
                    ->where('title', 'like', '%'. $this->query . '%')
                    ->get();
            // dd($foods);
        }

        if($this->resultByMenu != null){
            $searching_foods = $this->resultByMenu;
        }



        return view('livewire.search-food');
    }
}
