<?php

namespace App\Http\Livewire;

use App\Mode;
use App\Models\Restaurants;
use Livewire\Component;

class PopularComp extends Component
{



    public function render()
    {
        // dd($this->restaurant);

        $restaurants = Restaurants::where([
            'verified'  =>  1,
            'is_active'  =>  1,
        ])->get();

        $restaurants = collect($restaurants)->sortByDesc(function($restaurant){
            $restaurant->orders->count();
        });

        return view('livewire.popular-comp', compact('restaurants'));
    }
}
