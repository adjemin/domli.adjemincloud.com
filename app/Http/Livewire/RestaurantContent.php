<?php

namespace App\Http\Livewire;

use Livewire\Component;

class RestaurantContent extends Component
{
    public $sidik;

    public function render()
    {
        return view('livewire.restaurant-content');
    }
}
