<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyDeliverer extends Model
{

    protected $table = 'verify_deliverers';

    public $fillable = [
        'deliverer_id',
        'token',
    ];

     /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'deliverer_id' => 'integer',
        'token' => 'string'
    ];


    public function deliverer(){
        return $this->belongsTo(\App\Models\Delivery::class, 'deliverer_id', 'id');
    }

}
