<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = '';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = 'admin/home';
    // public const HOME = '/home';
    // public const CUSTOMER_HOME = '/customer/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapDelivererRoutes();

        $this->mapCustomerRoutes();

        // $this->mapAdminRoutes();
    }

    /**
     * Define the "deliverer" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapDelivererRoutes()
    {
        Route::group([
            'middleware' => ['web', 'deliverer', 'auth:deliverer'],
            'prefix' => 'deliverer',
            'as' => 'deliverer.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/deliverer.php');
        });
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->as('api.')
            ->namespace($this->namespace.'\\App\\Http\\Controllers\\API')
            ->group(base_path('routes/api.php'));
    }

    // protected function mapAdminRoutes()
    // {
    //     Route::prefix('admin')
    //          ->middleware('admin')
    //          ->namespace($this->namespace)
    //          ->group(base_path('routes/admin.php'));
    // }
    protected function mapCustomerRoutes(){
        Route::prefix('customer')
             ->middleware('customer')
             ->namespace($this->namespace)
             ->group(base_path('routes/customer.php'));
    }

    // protected function mapDelivererRoutes(){
    //     Route::prefix('deliverer')
    //          ->middleware('deliverer')
    //          ->namespace($this->namespace)
    //          ->group(base_path('routes/deliverer.php'));
    // }
}
