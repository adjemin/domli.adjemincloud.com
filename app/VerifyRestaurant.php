<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyRestaurant extends Model
{


    public $fillable = [
        'restaurant_id',
        'token',
    ];

     /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'restaurant_id' => 'integer',
        'token' => 'string'
    ];


    public function restaurants()
    {
        return $this->belongsTo('App\\Models\\Restaurants', 'restaurant_id');
    }
}
