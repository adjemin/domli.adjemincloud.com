<?php

namespace App\Repositories;

use App\Models\Otps;
use App\Repositories\BaseRepository;

/**
 * Class OtpsRepository
 * @package App\Repositories
 * @version January 8, 2021, 3:30 pm UTC
*/

class OtpsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'dial_code',
        'phone_number',
        'deadline',
        'is_used',
        'is_testing'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Otps::class;
    }
}
