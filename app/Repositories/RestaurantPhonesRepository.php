<?php

namespace App\Repositories;

use App\Models\RestaurantPhones;
use App\Repositories\BaseRepository;

/**
 * Class RestaurantPhonesRepository
 * @package App\Repositories
 * @version January 8, 2021, 6:26 pm UTC
*/

class RestaurantPhonesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'restaurant_id',
        'dial_code',
        'phone_number',
        'phone'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RestaurantPhones::class;
    }
}
