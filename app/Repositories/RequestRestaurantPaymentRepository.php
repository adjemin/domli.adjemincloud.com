<?php

namespace App\Repositories;

use App\Models\RequestRestaurantPayment;
use App\Repositories\BaseRepository;

/**
 * Class RequestRestaurantPaymentRepository
 * @package App\Repositories
 * @version March 29, 2021, 9:43 am UTC
*/

class RequestRestaurantPaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'amount',
        'date',
        'paiement_method',
        'account_reference'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RequestRestaurantPayment::class;
    }
}
