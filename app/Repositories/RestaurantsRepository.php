<?php

namespace App\Repositories;

use App\Models\Restaurants;
use App\Repositories\BaseRepository;

/**
 * Class RestaurantsRepository
 * @package App\Repositories
 * @version January 8, 2021, 3:53 pm UTC
*/

class RestaurantsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'uuid',
        'name',
        'responsable_name',
        'password',
        'cover',
        'logo',
        'lng',
        'lat',
        'email'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Restaurants::class;
    }
}
