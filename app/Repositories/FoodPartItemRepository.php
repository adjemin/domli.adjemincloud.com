<?php

namespace App\Repositories;

use App\Models\FoodPartItem;
use App\Repositories\BaseRepository;

/**
 * Class FoodPartItemRepository
 * @package App\Repositories
 * @version July 27, 2020, 3:52 pm UTC
*/

class FoodPartItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'food_part_id',
        'food_id',
        'title',
        'price',
        'inputType',
        'is_add_on',
        'has_display_price',
        'has_price',
        'currency_code',
        'currency_name',
        'is_essential'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FoodPartItem::class;
    }
}
