<?php

namespace App\Repositories;

use App\Models\VehicleTypes;
use App\Repositories\BaseRepository;

/**
 * Class VehicleTypesRepository
 * @package App\Repositories
 * @version January 8, 2021, 3:59 pm UTC
*/

class VehicleTypesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VehicleTypes::class;
    }
}
