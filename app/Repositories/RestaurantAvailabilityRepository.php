<?php

namespace App\Repositories;

use App\Models\RestaurantAvailability;
use App\Repositories\BaseRepository;

/**
 * Class RestaurantAvailabilityRepository
 * @package App\Repositories
 * @version February 9, 2021, 10:25 am UTC
*/

class RestaurantAvailabilityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'restaurant_id',
        'start_date',
        'end_date',
        'start_time',
        'end_time'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RestaurantAvailability::class;
    }
}
