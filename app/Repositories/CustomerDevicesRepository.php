<?php

namespace App\Repositories;

use App\Models\CustomerDevices;
use App\Repositories\BaseRepository;

/**
 * Class CustomerDevicesRepository
 * @package App\Repositories
 * @version January 8, 2021, 4:16 pm UTC
*/

class CustomerDevicesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'firebase_id',
        'device_model',
        'device_os',
        'device_os',
        'device_model_type',
        'device_meta_data'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerDevices::class;
    }
}
