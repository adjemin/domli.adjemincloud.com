<?php

namespace App\Repositories;

use App\Models\DeliveryAdresses;
use App\Repositories\BaseRepository;

/**
 * Class DeliveryAdressesRepository
 * @package App\Repositories
 * @version January 8, 2021, 5:44 pm UTC
*/

class DeliveryAdressesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'adress_name',
        'adress_slug',
        'lng',
        'town'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DeliveryAdresses::class;
    }
}
