<?php

namespace App\Repositories;

use App\Models\RestaurantPayment;
use App\Repositories\BaseRepository;

/**
 * Class RestaurantPaymentRepository
 * @package App\Repositories
 * @version February 28, 2021, 7:46 pm UTC
*/

class RestaurantPaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'restaurant_id',
        'amount',
        'is_paid',
        'paid_date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RestaurantPayment::class;
    }
}
