<?php

namespace App\Repositories;

use App\Models\DeliveryPayment;
use App\Repositories\BaseRepository;

/**
 * Class DeliveryPaymentRepository
 * @package App\Repositories
 * @version February 28, 2021, 7:49 pm UTC
*/

class DeliveryPaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'deliverer_id',
        'amount',
        'is_paid',
        'paid_date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DeliveryPayment::class;
    }
}
