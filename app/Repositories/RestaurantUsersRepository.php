<?php

namespace App\Repositories;

use App\Models\RestaurantUsers;
use App\Repositories\BaseRepository;

/**
 * Class RestaurantUsersRepository
 * @package App\Repositories
 * @version January 9, 2021, 8:16 am UTC
*/

class RestaurantUsersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'password',
        'dial_code',
        'phone_number',
        'phone',
        'is_active',
        'restaurant_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RestaurantUsers::class;
    }
}
