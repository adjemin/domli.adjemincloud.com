<?php

namespace App\Repositories;

use App\Models\DeliveryPaymentHistorique;
use App\Repositories\BaseRepository;

/**
 * Class DeliveryPaymentHistoriqueRepository
 * @package App\Repositories
 * @version March 29, 2021, 9:35 am UTC
*/

class DeliveryPaymentHistoriqueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'amount',
        'delivery_id',
        'reference_paiement'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DeliveryPaymentHistorique::class;
    }
}
