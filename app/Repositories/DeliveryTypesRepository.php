<?php

namespace App\Repositories;

use App\Models\DeliveryTypes;
use App\Repositories\BaseRepository;

/**
 * Class DeliveryTypesRepository
 * @package App\Repositories
 * @version January 8, 2021, 3:57 pm UTC
*/

class DeliveryTypesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DeliveryTypes::class;
    }
}
