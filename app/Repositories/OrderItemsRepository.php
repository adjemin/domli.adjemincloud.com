<?php

namespace App\Repositories;

use App\Models\OrderItems;
use App\Repositories\BaseRepository;

/**
 * Class OrderItemsRepository
 * @package App\Repositories
 * @version January 8, 2021, 6:24 pm UTC
*/

class OrderItemsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'quantity',
        'unit_price',
        'total_amount',
        'currency_code',
        'currency_name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderItems::class;
    }
}
