<?php

namespace App\Repositories;

use App\Models\Orders;
use App\Repositories\BaseRepository;

/**
 * Class OrdersRepository
 * @package App\Repositories
 * @version January 8, 2021, 5:09 pm UTC
*/

class OrdersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_number',
        'is_waiting',
        'current_status',
        'payment_methode_id',
        'amount',
        'currency_name',
        'is_delivery',
        'is_wayting_payment',
        'customer_id',
        'restaurant_id',
        'delivery_id',
        'customer_id',
        'restaurant_id',
        'delivery_id',
        'delivery_fees',
        'currency_code'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Orders::class;
    }
}
