<?php

namespace App\Repositories;

use App\Models\RestaurantCategories;
use App\Repositories\BaseRepository;

/**
 * Class RestaurantCategoriesRepository
 * @package App\Repositories
 * @version January 9, 2021, 8:08 am UTC
*/

class RestaurantCategoriesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        // 'title',
        // 'slug'
        'restaurant_id',
        'category_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RestaurantCategories::class;
    }
}
