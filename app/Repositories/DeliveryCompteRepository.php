<?php

namespace App\Repositories;

use App\Models\DeliveryCompte;
use App\Repositories\BaseRepository;

/**
 * Class DeliveryCompteRepository
 * @package App\Repositories
 * @version March 29, 2021, 9:32 am UTC
*/

class DeliveryCompteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'delivery_id',
        'compte'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DeliveryCompte::class;
    }
}
