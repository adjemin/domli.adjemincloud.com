<?php

namespace App\Repositories;

use App\Models\Menus;
use App\Repositories\BaseRepository;

/**
 * Class MenusRepository
 * @package App\Repositories
 * @version January 9, 2021, 1:35 pm UTC
*/

class MenusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'slug',
        'restaurant_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Menus::class;
    }
}
