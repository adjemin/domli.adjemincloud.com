<?php

namespace App\Repositories;

use App\Models\FoodParts;
use App\Repositories\BaseRepository;

/**
 * Class FoodPartsRepository
 * @package App\Repositories
 * @version January 9, 2021, 1:48 pm UTC
*/

class FoodPartsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'food_id',
        'title',
        'price'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FoodParts::class;
    }
}
