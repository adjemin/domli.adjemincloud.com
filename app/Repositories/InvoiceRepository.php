<?php

namespace App\Repositories;

use App\Models\Invoice;
use App\Repositories\BaseRepository;

/**
 * Class InvoiceRepository
 * @package App\Repositories
 * @version July 27, 2020, 4:40 pm UTC
*/

class InvoiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'customer_id',
        'reference',
        'link',
        'subtotal',
        'tax',
        'fees_delivery',
        'discount',
        'extra',
        'service_fees',
        'total',
        'status',
        'is_paid',
        'currency_code',
        'currency_name',
        'service_slug',
        'is_paid_to_business'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Invoice::class;
    }
}
