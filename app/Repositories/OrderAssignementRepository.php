<?php

namespace App\Repositories;

use App\Models\OrderAssignement;
use App\Repositories\BaseRepository;

/**
 * Class OrderAssignementRepository
 * @package App\Repositories
 * @version February 25, 2021, 12:52 pm UTC
*/

class OrderAssignementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'deliverer_id',
        'acceptation_time',
        'rejection_time',
        'is_waiting_acceptation'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderAssignement::class;
    }
}
