<?php

namespace App\Repositories;

use App\Models\Orders;
use App\Repositories\BaseRepository;

/**
 * Class OrderRepository
 * @package App\Repositories
 * @version January 9, 2021, 1:35 pm UTC
*/

class OrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_number',
        'is_waiting',
        'current_status',
        // 'payment_methode_id',
        'payment_method_slug',
        'amount',
        'currency_name',
        'is_delivery',
        'is_wayting_payment',
        'restaurant_id',
        'delivery_id',
        'customer_id',
        // 'restaurant_id',
        // 'delivery_id',
        'delivery_fees',
        'currency_code',
        'is_waiting_delivery'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Orders::class;
    }
}
