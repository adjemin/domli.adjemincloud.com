<?php

namespace App\Repositories;

use App\Models\Delivery;
use App\Repositories\BaseRepository;

/**
 * Class DeliveryRepository
 * @package App\Repositories
 * @version January 8, 2021, 5:30 pm UTC
*/

class DeliveryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'surname',
        'email',
        'password',
        'dial_code',
        'phone_number',
        'phone',
        'delivery_type_id',
        'vehicle_type_id',
        'lng',
        'lat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Delivery::class;
    }
}
