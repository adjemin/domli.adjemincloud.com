<?php

namespace App\Repositories;

use App\Models\DeliveryAvailaibity;
use App\Repositories\BaseRepository;

/**
 * Class DeliveryAvailaibityRepository
 * @package App\Repositories
 * @version February 23, 2021, 10:15 am UTC
*/

class DeliveryAvailaibityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'delivery_id',
        'start_date',
        'end_date',
        'start_time',
        'end_time'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DeliveryAvailaibity::class;
    }
}
