<?php

namespace App\Repositories;

use App\Models\RestaurantMenus;
use App\Repositories\BaseRepository;

/**
 * Class RestaurantMenusRepository
 * @package App\Repositories
 * @version January 9, 2021, 7:50 am UTC
*/

class RestaurantMenusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RestaurantMenus::class;
    }
}
