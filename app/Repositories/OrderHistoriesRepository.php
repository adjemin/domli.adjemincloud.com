<?php

namespace App\Repositories;

use App\Models\OrderHistories;
use App\Repositories\BaseRepository;

/**
 * Class OrderHistoriesRepository
 * @package App\Repositories
 * @version January 8, 2021, 6:17 pm UTC
*/

class OrderHistoriesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'status',
        'customer_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderHistories::class;
    }
}
