<?php

namespace App\Repositories;

use App\Models\AgentConnexion;
use App\Repositories\BaseRepository;

/**
 * Class AgentConnexionRepository
 * @package App\Repositories
 * @version February 25, 2021, 12:49 pm UTC
*/

class AgentConnexionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'deliverer_id',
        'location_lat',
        'location_lng',
        'location_name',
        'is_active'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AgentConnexion::class;
    }
}
