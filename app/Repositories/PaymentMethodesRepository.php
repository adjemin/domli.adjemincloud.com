<?php

namespace App\Repositories;

use App\Models\PaymentMethodes;
use App\Repositories\BaseRepository;

/**
 * Class PaymentMethodesRepository
 * @package App\Repositories
 * @version January 8, 2021, 4:26 pm UTC
*/

class PaymentMethodesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'slug',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PaymentMethodes::class;
    }
}
