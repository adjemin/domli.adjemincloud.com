<?php

namespace App\Repositories;

use App\Models\RestaurantPaymentHistorique;
use App\Repositories\BaseRepository;

/**
 * Class RestaurantPaymentHistoriqueRepository
 * @package App\Repositories
 * @version March 29, 2021, 9:38 am UTC
*/

class RestaurantPaymentHistoriqueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'amount',
        'restaurant_id',
        'reference_paiement'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RestaurantPaymentHistorique::class;
    }
}
