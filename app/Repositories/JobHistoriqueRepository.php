<?php

namespace App\Repositories;

use App\Models\JobHistorique;
use App\Repositories\BaseRepository;

/**
 * Class JobHistoriqueRepository
 * @package App\Repositories
 * @version February 25, 2021, 12:46 pm UTC
*/

class JobHistoriqueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'job_id',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return JobHistorique::class;
    }
}
