<?php

namespace App\Repositories;

use App\Models\OrderDeliveries;
use App\Repositories\BaseRepository;

/**
 * Class OrderDeliveriesRepository
 * @package App\Repositories
 * @version January 8, 2021, 5:22 pm UTC
*/

class OrderDeliveriesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_code',
        'delivery_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderDeliveries::class;
    }
}
