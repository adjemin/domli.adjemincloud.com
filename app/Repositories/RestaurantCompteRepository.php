<?php

namespace App\Repositories;

use App\Models\RestaurantCompte;
use App\Repositories\BaseRepository;

/**
 * Class RestaurantCompteRepository
 * @package App\Repositories
 * @version March 29, 2021, 9:28 am UTC
*/

class RestaurantCompteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'restaurant_id',
        'compte'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RestaurantCompte::class;
    }
}
