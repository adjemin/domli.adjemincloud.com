<?php

namespace App\Repositories;

use App\Models\RequestDeliveryPayment;
use App\Repositories\BaseRepository;

/**
 * Class RequestDeliveryPaymentRepository
 * @package App\Repositories
 * @version March 29, 2021, 9:45 am UTC
*/

class RequestDeliveryPaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'amount',
        'paiement_method',
        'date',
        'account_reference'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RequestDeliveryPayment::class;
    }
}
