<?php

namespace App\Repositories;

use App\Models\FoodCategories;
use App\Repositories\BaseRepository;

/**
 * Class FoodCategoriesRepository
 * @package App\Repositories
 * @version January 9, 2021, 8:02 am UTC
*/

class FoodCategoriesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description',
        'photo',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FoodCategories::class;
    }
}
