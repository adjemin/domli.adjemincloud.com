<?php

namespace App\Repositories;

use App\Models\PasswordDelivery;
use App\Repositories\BaseRepository;

/**
 * Class PasswordDeliveryRepository
 * @package App\Repositories
 * @version April 7, 2021, 11:35 am UTC
*/

class PasswordDeliveryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'password_reset_code',
        'password_reset_deadline',
        'delevery_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PasswordDelivery::class;
    }
}
