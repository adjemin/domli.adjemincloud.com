<?php

namespace App\Repositories;

use App\Models\RestaurantCategories;
use App\Repositories\BaseRepository;

/**
 * Class RestaurantCategoryRepository
 * @package App\Repositories
 * @version July 27, 2020, 3:40 pm UTC
*/

class RestaurantCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'restaurant_id',
        'category_id',
        'category_slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RestaurantCategories::class;
    }
}
