<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RestaurantPaymentHistorique;
use Faker\Generator as Faker;

$factory->define(RestaurantPaymentHistorique::class, function (Faker $faker) {

    return [
        'amount' => $faker->word,
        'restaurant_id' => $faker->randomDigitNotNull,
        'reference_paiement' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
