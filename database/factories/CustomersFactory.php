<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Customers;
use Faker\Generator as Faker;

$factory->define(Customers::class, function (Faker $faker) {

    return [
        'name' => $faker->text,
        'surname' => $faker->word,
        'dial_code' => $faker->randomDigitNotNull,
        'phone_number' => $faker->randomDigitNotNull,
        'phone' => $faker->randomDigitNotNull,
        'email' => $faker->word,
        'photo_profile' => $faker->word,
        'lng' => $faker->randomDigitNotNull,
        'lat' => $faker->randomDigitNotNull,
        'langage' => $faker->text,
        'country_code' => $faker->randomDigitNotNull,
        'is_active' => $faker->randomDigitNotNull,
        'activation_date' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
