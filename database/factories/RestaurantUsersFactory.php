<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RestaurantUsers;
use Faker\Generator as Faker;

$factory->define(RestaurantUsers::class, function (Faker $faker) {

    return [
        'name' => $faker->text,
        'email' => $faker->text,
        'password' => bcrypt("password"),
        'dial_code' => $faker->randomDigitNotNull,
        'phone_number' => $faker->randomDigitNotNull,
        'phone' => $faker->randomDigitNotNull,
        'is_active' => 1,
        'restaurant_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
