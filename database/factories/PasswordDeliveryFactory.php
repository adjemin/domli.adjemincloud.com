<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PasswordDelivery;
use Faker\Generator as Faker;

$factory->define(PasswordDelivery::class, function (Faker $faker) {

    return [
        'password_reset_code' => $faker->word,
        'password_reset_deadline' => $faker->word,
        'delevery_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
