<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrderItems;
use Faker\Generator as Faker;

$factory->define(OrderItems::class, function (Faker $faker) {

    return [
        'order_id' => $faker->randomDigitNotNull,
        'quantity' => $faker->randomDigitNotNull,
        'unit_price' => $faker->randomDigitNotNull,
        'total_amount' => $faker->randomDigitNotNull,
        'currency_code' => $faker->randomDigitNotNull,
        'currency_name' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
