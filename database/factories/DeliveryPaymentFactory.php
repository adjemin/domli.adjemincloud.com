<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DeliveryPayment;
use Faker\Generator as Faker;

$factory->define(DeliveryPayment::class, function (Faker $faker) {

    return [
        'deliverer_id' => $faker->randomDigitNotNull,
        'amount' => $faker->word,
        'is_paid' => $faker->word,
        'paid_date' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
