<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Delivery;
use Faker\Generator as Faker;

$factory->define(Delivery::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'surname' => $faker->text,
        'email' => $faker->text,
        'password' => $faker->text,
        'dial_code' => $faker->randomDigitNotNull,
        'phone_number' => $faker->randomDigitNotNull,
        'phone' => $faker->randomDigitNotNull,
        'delivery_type_id' => $faker->randomDigitNotNull,
        'vehicle_type_id' => $faker->randomDigitNotNull,
        'lng' => $faker->randomDigitNotNull,
        'lat' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
