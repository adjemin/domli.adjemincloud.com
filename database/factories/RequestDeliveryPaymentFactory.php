<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RequestDeliveryPayment;
use Faker\Generator as Faker;

$factory->define(RequestDeliveryPayment::class, function (Faker $faker) {

    return [
        'amount' => $faker->word,
        'paiement_method' => $faker->word,
        'date' => $faker->word,
        'account_reference' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
