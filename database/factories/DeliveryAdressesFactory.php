<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DeliveryAdresses;
use Faker\Generator as Faker;

$factory->define(DeliveryAdresses::class, function (Faker $faker) {

    return [
        'adress_name' => $faker->text,
        'adress_slug' => $faker->text,
        'lng' => $faker->randomDigitNotNull,
        'town' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
