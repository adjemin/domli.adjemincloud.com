<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RequestRestaurantPayment;
use Faker\Generator as Faker;

$factory->define(RequestRestaurantPayment::class, function (Faker $faker) {

    return [
        'amount' => $faker->word,
        'date' => $faker->word,
        'paiement_method' => $faker->word,
        'account_reference' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
