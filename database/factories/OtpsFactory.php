<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Otps;
use Faker\Generator as Faker;

$factory->define(Otps::class, function (Faker $faker) {

    return [
        'code' => $faker->randomDigitNotNull,
        'dial_code' => $faker->randomDigitNotNull,
        'phone_number' => $faker->randomDigitNotNull,
        'deadline' => $faker->date('Y-m-d H:i:s'),
        'is_used' => $faker->randomDigitNotNull,
        'is_testing' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
