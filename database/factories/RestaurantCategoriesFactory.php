<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RestaurantCategories;
use Faker\Generator as Faker;

$factory->define(RestaurantCategories::class, function (Faker $faker) {

    return [
        // 'title' => $faker->word,
        // 'slug' => $faker->word,
        'restaurant_id' => $faker->numberBetween(1, 100),
        'category_id' => $faker->numberBetween(1, 6),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
