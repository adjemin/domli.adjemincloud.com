<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CustomerDevices;
use Faker\Generator as Faker;

$factory->define(CustomerDevices::class, function (Faker $faker) {

    return [
        'customer_id' => $faker->randomDigitNotNull,
        'firebase_id' => $faker->randomDigitNotNull,
        'device_model' => $faker->word,
        'device_os' => $faker->word,
        'device_os' => $faker->text,
        'device_model_type' => $faker->word,
        'device_meta_data' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
