<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RestaurantAvailability;
use Faker\Generator as Faker;

$factory->define(RestaurantAvailability::class, function (Faker $faker) {

    return [
        'restaurant_id' => $faker->randomDigitNotNull,
        'start_date' => $faker->word,
        'end_date' => $faker->word,
        'start_time' => $faker->word,
        'end_time' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
