<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrderDeliveries;
use Faker\Generator as Faker;

$factory->define(OrderDeliveries::class, function (Faker $faker) {

    return [
        'order_code' => $faker->randomDigitNotNull,
        'delivery_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
