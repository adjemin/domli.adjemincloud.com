<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DeliveryCompte;
use Faker\Generator as Faker;

$factory->define(DeliveryCompte::class, function (Faker $faker) {

    return [
        'delivery_id' => $faker->randomDigitNotNull,
        'compte' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
