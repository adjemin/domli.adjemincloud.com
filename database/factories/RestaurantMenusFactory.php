<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RestaurantMenus;
use App\Models\Restaurants;
use Faker\Generator as Faker;

$factory->define(RestaurantMenus::class, function (Faker $faker) {

    return [
        'title' => $faker->word,
        'slug' => $faker->word,
        'restaurant_id' => $faker->numberBetween(1, 50),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
