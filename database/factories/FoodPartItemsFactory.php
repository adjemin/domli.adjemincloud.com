<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\FoodPartItem;
use App\Models\FoodParts;
use Faker\Generator as Faker;

$factory->define(FoodPartItem::class, function (Faker $faker) {

    return [
        'food_id' => $faker->numberBetween(1, 50),
        'food_part_id' => $faker->numberBetween(1, 50),
        'title' => $faker->word,
        'price' => $faker->numberBetween(2, 100) * 100,
        'is_add_on' =>  $faker->numberBetween(0, 1),
        'has_display_price' =>  $faker->numberBetween(0, 1),
        'has_price' =>  $faker->numberBetween(0, 1),
        'currency_code' =>  'US',
        // 'is_essential' => $faker->numberBetween(0, 1),
        'currency_name' => 'DOLLAR',
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
