<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RestaurantPhones;
use Faker\Generator as Faker;

$factory->define(RestaurantPhones::class, function (Faker $faker) {

    return [
        'restaurant_id' => $faker->randomDigitNotNull,
        'dial_code' => $faker->randomDigitNotNull,
        'phone_number' => $faker->randomDigitNotNull,
        'phone' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
