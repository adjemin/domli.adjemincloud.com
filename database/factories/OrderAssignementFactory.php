<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrderAssignement;
use Faker\Generator as Faker;

$factory->define(OrderAssignement::class, function (Faker $faker) {

    return [
        'order_id' => $faker->randomDigitNotNull,
        'deliverer_id' => $faker->randomDigitNotNull,
        'acceptation_time' => $faker->word,
        'rejection_time' => $faker->word,
        'is_waiting_acceptation' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
