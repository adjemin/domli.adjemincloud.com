<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Job;
use Faker\Generator as Faker;

$factory->define(Job::class, function (Faker $faker) {

    return [
        'location_name' => $faker->word,
        'location_description' => $faker->text,
        'deliverer_id' => $faker->randomDigitNotNull,
        'location_lat' => $faker->word,
        'location_lng' => $faker->word,
        'is_pickup' => $faker->word,
        'status' => $faker->word,
        'signature_url' => $faker->word,
        'images' => $faker->word,
        'notes' => $faker->text,
        'note_audio' => $faker->word,
        'before_date' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
