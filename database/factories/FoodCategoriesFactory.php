<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\FoodCategories;
use Faker\Generator as Faker;

$factory->define(FoodCategories::class, function (Faker $faker) {

    return [
        'title' => $faker->word,
        'description' => $faker->text,
        'photo' => 'https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/cef389b486cb4827e6ba007f26ebddab.svg',
        'slug' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
