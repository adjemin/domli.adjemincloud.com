<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Orders;
use Faker\Generator as Faker;

$factory->define(Orders::class, function (Faker $faker) {

    return [
        'order_number' => $faker->word,
        'is_waiting' => 1,
        'current_status' => $faker->word,
        'payment_methode_id' => $faker->randomDigitNotNull,
        'amount' => $faker->randomDigitNotNull,
        'currency_name' => $faker->word,
        'is_delivery' => $faker->word,
        'is_wayting_payment' => $faker->word,
        'customer_id' => $faker->randomDigitNotNull,
        'restaurant_id' => $faker->randomDigitNotNull,
        'delivery_id' => $faker->randomDigitNotNull,
        'customer_id' => $faker->randomDigitNotNull,
        'restaurant_id' => $faker->randomDigitNotNull,
        'delivery_id' => $faker->randomDigitNotNull,
        'delivery_fees' => $faker->randomDigitNotNull,
        'currency_code' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
