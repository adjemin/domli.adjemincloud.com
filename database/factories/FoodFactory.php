<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Food;
use Faker\Generator as Faker;

$factory->define(Food::class, function (Faker $faker) {

    return [
        'uuid' => \App\Utils\UUID::v4(),
        'menu_id' => $faker->numberBetween(1,300),
        'food_category_id' => $faker->numberBetween(1, 6),
        'restaurant_id' => $faker->numberBetween(1, 50),
        'title' => $faker->sentence(5),
        'description' => $faker->sentence(10),
        'image' => 'https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/cef389b486cb4827e6ba007f26ebddab.svg',
        'price' => $faker->numberBetween(15, 300) * 100,
        'currency_code' => 'USD',
        'currency_name' => 'US Dollar',
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
