<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AgentConnexion;
use Faker\Generator as Faker;

$factory->define(AgentConnexion::class, function (Faker $faker) {

    return [
        'deliverer_id' => $faker->randomDigitNotNull,
        'location_lat' => $faker->word,
        'location_lng' => $faker->word,
        'location_name' => $faker->word,
        'is_active' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
