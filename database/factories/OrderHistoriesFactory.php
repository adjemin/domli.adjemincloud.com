<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrderHistories;
use Faker\Generator as Faker;

$factory->define(OrderHistories::class, function (Faker $faker) {

    return [
        'order_id' => $faker->randomDigitNotNull,
        'status' => $faker->word,
        'customer_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
