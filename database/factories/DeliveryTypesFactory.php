<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DeliveryTypes;
use Faker\Generator as Faker;

$factory->define(DeliveryTypes::class, function (Faker $faker) {

    return [
        'title' => $faker->text,
        'description' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
