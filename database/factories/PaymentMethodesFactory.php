<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PaymentMethodes;
use Faker\Generator as Faker;

$factory->define(PaymentMethodes::class, function (Faker $faker) {

    return [
        'title' => $faker->text,
        'slug' => $faker->word,
        'description' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
