<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\FoodParts;
use Faker\Generator as Faker;

$factory->define(FoodParts::class, function (Faker $faker) {

    return [
        'food_id' => $faker->numberBetween(1, 50),
        'title' => $faker->word,
        'subtitle' => $faker->word,
        'is_required' => $faker->numberBetween(0, 1),
        // 'inputType' => $faker->numberBetween(0, 1),
        'has_chidren' => $faker->numberBetween(0, 1),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
