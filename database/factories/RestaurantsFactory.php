<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Restaurants;
use Faker\Generator as Faker;

$factory->define(Restaurants::class, function (Faker $faker) {

    return [
        'uuid' => \App\Utils\UUID::v4(),
        'name' => $faker->name,
        'cover' => $faker->imageUrl(1280, 760),
        'logo' => $faker->imageUrl(75, 75),
        'categorie_id' => $faker->numberBetween(1, 6),
        'lng' => $faker->randomDigitNotNull,
        'lat' => $faker->randomDigitNotNull,
        'email' => $faker->email,
        'responsable_name' => $faker->name,
        'restaurant_adress_name' => $faker->address,
        'restaurant_region_name' => $faker->address,
        'restaurant_city' => $faker->address,
        'restaurant_contact' => $faker->phoneNumber,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
