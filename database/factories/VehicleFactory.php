<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Vehicle;
use Faker\Generator as Faker;

$factory->define(Vehicle::class, function (Faker $faker) {

    return [
        'vehicleType_id' => $faker->randomDigitNotNull,
        'deliverer_id' => $faker->randomDigitNotNull,
        'is_active' => $faker->word,
        'numero_police' => $faker->word,
        'date_start' => $faker->word,
        'date_end' => $faker->word,
        'vehicule_name' => $faker->word,
        'numero_serie' => $faker->word,
        'immatriculation' => $faker->word,
        'is_verified' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
