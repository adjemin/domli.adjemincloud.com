<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\JobHistorique;
use Faker\Generator as Faker;

$factory->define(JobHistorique::class, function (Faker $faker) {

    return [
        'job_id' => $faker->randomDigitNotNull,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
