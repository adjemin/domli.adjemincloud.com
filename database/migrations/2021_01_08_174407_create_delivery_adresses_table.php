<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryAdressesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_adresses', function (Blueprint $table) {
            $table->increments('id');
            $table->text('adress_name')->nullable();
            $table->text('adress_slug')->nullable();
            $table->integer('lng')->unsigned()->nullable();
            $table->integer('lat')->unsigned()->nullable();
            $table->text('town')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('delivery_adresses');
    }
}
