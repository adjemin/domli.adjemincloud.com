<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtpsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code')->nullable();
            $table->integer('dial_code')->unsigned()->nullable();
            $table->integer('phone_number')->unsigned()->nullable();
            $table->timestamp('deadline')->nullable();
            $table->integer('is_used')->unsigned()->nullable();
            $table->integer('is_testing')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('otps');
    }
}
