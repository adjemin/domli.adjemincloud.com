<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('name')->nullable();
            // $table->text('surname')->nullable();
            $table->string('photo')->unique()->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->integer('dial_code')->unsigned()->nullable();
            $table->integer('phone_number')->unsigned()->nullable();
            $table->integer('phone')->unsigned()->nullable();
            // $table->integer('delivery_type_id')->unsigned();
            // $table->integer('vehicle_type_id')->unsigned();
            $table->float('lng')->nullable();
            $table->float('lat')->nullable();
            $table->boolean('is_active')->default(0);
            $table->boolean('is_connected')->default(0);
            $table->timestamps();
            $table->softDeletes();
            // $table->foreign('delivery_type_id')->references('id')->on('delivery_type');
            // $table->foreign('vehicle_type_id')->references('id')->on('vehicle_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deliveries');
    }
}
