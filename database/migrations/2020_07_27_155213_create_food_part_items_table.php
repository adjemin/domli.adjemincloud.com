<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodPartItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_part_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('food_part_id')->nullable();
            $table->bigInteger('food_id')->nullable();
            $table->string('title')->nullable();
            $table->string('price')->default('0')->nullable();
            $table->string('inputType')->default('select_unique')->nullable();//select_unique, multi_select, quantity, text
            $table->boolean('is_add_on')->default(false)->nullable();
            $table->boolean('has_display_price')->default(false)->nullable();
            $table->boolean('has_price')->default(false)->nullable();
            $table->string('currency_code')->default('XOF')->nullable();
            $table->string('currency_name')->default('CFA')->nullable();
            $table->boolean('is_essential')->default(false)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('food_part_items');
    }
}
