<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid')->nullable();
            $table->string('name')->nullable();
            // $table->string('lastname')->nullable();
            $table->string('password')->nullable();
            $table->string('cover')->nullable();
            $table->string('logo')->nullable();
            $table->integer('categorie_id')->unsigned()->nullable('restaurant_categories', 'id');
            // $table->foreign('categorie_id')->references('id')->on('restaurant_categories');
            // $table->integer('tel2')->unsigned()->nullable();
            // $table->integer('tel3')->unsigned()->nullable();
            $table->float('lng')->nullable();
            $table->float('lat')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('responsable_name')->nullable();
            $table->string('responsable_photo')->nullable();
            $table->string('responsable_paper_id_number')->nullable();
            $table->string('restaurant_paper_id_type')->nullable();
            $table->string('restaurant_adress_name')->nullable();
            $table->string('restaurant_region_name')->nullable();
            $table->string('restaurant_registration_number')->nullable();
            $table->string('restaurant_city')->nullable();
            $table->string('restaurant_contact')->nullable();
            $table->boolean('verified')->default(false);
            $table->boolean('is_active')->default(false);
            $table->date('activation_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('restaurants');
    }
}
