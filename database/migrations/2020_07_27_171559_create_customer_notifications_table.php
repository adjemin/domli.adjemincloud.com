<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerNotificationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('subtitle')->nullable();
            $table->string('action')->nullable();
            $table->string('action_by')->nullable();
            $table->text('meta_data')->nullable();
            $table->string('meta_data_id')->nullable();
            $table->string('type_notification')->nullable();
            $table->boolean('is_read')->nullable();
            $table->boolean('is_received')->nullable();
            $table->text('data')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->bigInteger('data_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_notifications');
    }
}
