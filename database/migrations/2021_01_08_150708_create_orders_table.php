<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number')->nullable();
            $table->boolean('is_waiting');
            $table->string('current_status')->nullable();
            $table->string('order_delivered_status')->nullable();
            $table->string('order_delivered_historique')->nullable();
            // $table->integer('payment_methode_id')->unsigned();
            $table->string('payment_method_slug')->nullable();
            $table->float('amount')->unsigned()->nullable();
            $table->float('kilometers')->unsigned()->nullable();
            $table->float('taxe')->unsigned()->nullable();
            $table->string('currency_name')->nullable();
            $table->boolean('is_delivery')->default(0);
            $table->boolean('is_wayting_payment')->default(0);
            $table->boolean('is_order_delivered_completed')->default(0);
            $table->timestamp('completed_delivery_date')->default(0);
            $table->timestamp('delivery_date')->nullable();
            $table->string('location_name')->nullable();
            $table->string('location_lat')->nullable();
            $table->string('location_lng')->nullable();
            $table->boolean('is_cooking_completed')->default(0);
            $table->text('note')->nullable();
            $table->integer('delivery_id')->unsigned()->nullable();
            // $table->integer('customer_id')->unsigned();
            // $table->integer('restaurant_id')->unsigned();
            // $table->integer('delivery_id')->unsigned();
            $table->integer('delivery_fees')->unsigned()->nullable();
            $table->string('currency_code')->nullable();
            $table->timestamps();
            $table->softDeletes();
            // $table->foreign('payment_methode_id')->references('id')->on('payment_methodes');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('restaurant_id')->references('id')->on('restaurant');
            $table->foreign('delivery_id')->references('id')->on('delivery');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
