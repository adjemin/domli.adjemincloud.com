<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicePaymentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('invoice_id')->nullable();
            $table->string('payment_method')->default("cash")->nullable(); //cash or online
            $table->string('payment_reference')->unique();
            $table->string('amount')->default("0")->nullable();
            $table->string('currency_code')->default("XOF")->nullable();
            $table->string('currency_name')->default("CFA")->nullable();
            $table->string('coupon')->nullable();
            $table->string('creator_id')->nullable();
            $table->string('creator_name')->nullable();
            $table->string('creator')->default("customers")->nullable();
            $table->string('status')->default("WAITING")->nullable(); //waiting and successfull
            $table->boolean('is_waiting')->default(true)->nullable();
            $table->boolean('is_completed')->default(false)->nullable();
            $table->string('payment_gateway_trans_id')->nullable();
            $table->string('payment_gateway_custom')->nullable();
            $table->string('payment_gateway_currency')->nullable();
            $table->string('payment_gateway_amount')->nullable();
            $table->string('payment_gateway_payid')->nullable();
            $table->string('payment_gateway_payment_date')->nullable();
            $table->string('payment_gateway_payment_time')->nullable();
            $table->string('payment_gateway_error_message')->nullable();
            $table->string('payment_gateway_payment_method')->nullable();
            $table->string('payment_gateway_phone_prefixe')->nullable();
            $table->string('payment_gateway_cel_phone_num')->nullable();
            $table->string('payment_gateway_ipn_ack')->nullable();
            $table->string('payment_gateway_created_at')->nullable();
            $table->string('payment_gateway_updated_at')->nullable();
            $table->string('payment_gateway_cpm_result')->nullable();;
            $table->string('payment_gateway_trans_status')->nullable();
            $table->string('payment_gateway_designation')->nullable();
            $table->string('payment_gateway_buyer_name')->nullable();
            $table->string('payment_gateway_signature')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoice_payments');
    }
}
