<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->string('reference')->unique();
            $table->string('link')->nullable();
            $table->string('subtotal')->default("0")->nullable();
            $table->string('tax')->default("0")->nullable();
            $table->string('fees_delivery')->default("0")->nullable();
            $table->string('discount')->default("0")->nullable();
            $table->string('extra')->default("0")->nullable();
            $table->string('service_fees')->default("0")->nullable();
            $table->string('total')->default("0")->nullable();
            $table->string('status')->default("unpaid")->nullable();
            $table->boolean('is_paid')->default(false)->nullable();
            $table->string('currency_code')->default("XOF")->nullable();
            $table->string('currency_name')->default("CFA")->nullable();
            $table->string('service_slug')->nullable();
            $table->boolean('is_paid_to_business')->default(false)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
