<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('location_name')->nullable();
            $table->text('location_description')->nullable();
            $table->integer('order_id')->nullable();
            $table->integer('job_code')->nullable();
            $table->integer('deliverer_id')->nullable();
            $table->double('location_lat')->nullable();
            $table->double('location_lng')->nullable();
            $table->boolean('is_pickup')->nullable();
            $table->string('status')->nullable();
            $table->string('signature_url')->nullable();
            $table->json('images')->nullable();
            $table->text('notes')->nullable();
            $table->json('note_audio')->nullable();
            $table->dateTime('before_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jobs');
    }
}
