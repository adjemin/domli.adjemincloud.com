<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->integer('dial_code')->unsigned()->nullable();
            $table->integer('phone_number')->unsigned()->nullable();
            $table->string('country_code')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('photo_profile')->nullable();
            $table->string('region_name')->nullable();
            $table->string('city')->nullable();
            $table->float('lat')->nullable();
            $table->float('lng')->nullable();
            $table->string('language')->nullable();
            $table->boolean('is_active')->nullable();
            // $table->timestamp('email_verified_at')->nullable();
            $table->boolean('verified')->default(false);
            $table->string('activation_date')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}
