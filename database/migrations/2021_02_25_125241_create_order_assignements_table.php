<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderAssignementsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_assignements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('deliverer_id');
            $table->dateTime('acceptation_time');
            $table->dateTime('rejection_time');
            $table->boolean('is_waiting_acceptation');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_assignements');
    }
}
