<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliverersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliverers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            // $table->string('surname')->unique();
            $table->string('email')->unique();
            $table->string('country_code')->nullable();
            $table->string('dial_code')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('phone')->nullable();
            $table->string('photo_profile')->default('https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png');
            $table->string('city')->nullable();
            $table->double('lng')->nullable();
            $table->double('lat')->nullable();
            $table->string('region_name')->nullable();
            $table->string('language')->nullable();
            $table->boolean('is_active')->nullable();
            $table->string('activation_date')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deliverers');
    }
}
