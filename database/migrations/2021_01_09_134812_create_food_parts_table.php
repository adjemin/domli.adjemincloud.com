<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodPartsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_parts', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('parent_id')->nullable();
            $table->bigInteger('food_id')->nullable();
            $table->string('subtitle')->nullable();
            $table->boolean('is_required')->default(true)->nullable();;
            // $table->string('inputType')->default("select_unique")->nullable();//select_unique, multi_select, quantity, text
            $table->boolean('has_chidren')->default(false)->nullable();
            $table->text('title')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('food_id')->references('id')->on('food');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('food_parts');
    }
}
