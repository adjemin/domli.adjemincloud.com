<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => "Le champs :attribute doit être accepté.",
    'active_url' => "Le champs :attribute n'est pas une URL valide.",
    'after' => 'Le champs :attribute doit être une date après :date.',
    'after_or_égal à' => 'Le champs :attribute doit être une date postérieure ou égale à :date.',
    'alpha' => 'Le champs :attribute ne peut contenir que des lettres.',
    'alpha_dash' => 'Le champs :attribute ne peut contenir que des lettres, des chiffres, des tirets et des traits de soulignement.',
    'alpha_num' => 'Le champs :attribute ne peut contenir que des lettres et des chiffres.',
    'array' => 'Le champs :attribute doit être un tableau.',
    'before' => 'Le champs :attribute doit être une date antérieure :date.',
    'before_or_égal à' => 'Le champs :attribute doit être une date antérieure ou égale à :date.',
    'between' => [
        'numeric' => 'Le champs :attribute Doit être entre :min et :max.',
        'file' => 'Le champs :attribute Doit être entre :min et :max kilo-octets.',
        'string' => 'Le champs :attribute Doit être entre :min et :max personnages.',
        'array' => 'Le champs :attribute doit avoir entre:min et :max éléments.',
    ],
    'boolean' => 'Le champs :attribute le champ doit être vrai ou faux.',
    'confirmed' => 'Le champs :attribute la confirmation ne correspond pas.',
    'date' => "Le champs :attribute la date n'est pas valide.",
    'date_égal às' => 'Le champs :attribute doit être une date égale à :date.',
    'date_format' => 'Le champs :attribute ne correspond pas au format :format.',
    'different' => 'Le champs :attribute et :other doit être différent.',
    'digits' => 'Le champs :attribute Doit être :digits chiffres.',
    'digits_between' => 'Le champs :attribute Doit être entre :min et :max digits.',
    'dimensions' => "Le champs :attribute a des dimensions d'image non valides.",
    'distinct' => 'Le champs :attribute le champ a une valeur en double.',
    'email' => 'Le champs :attribute Doit être une adresse e-mail valide.',
    'ends_with' => "Le champs :attribute doit se terminer par l'un des suivants: :values.",
    'exists' => "Le champs select :attribute n'est pas valide.",
    'file' => 'Le champs :attribute Doit être un fichier.',
    'filled' => 'Le champs :attribute le champ doit avoir une valeur.',
    'gt' => [
        'numeric' => 'Le champs :attribute Doit être plus grand que:value.',
        'file' => 'Le champs :attribute Doit être plus grand que:value kilo-octets.',
        'string' => 'Le champs :attribute Doit être plus grand que:value personnages.',
        'array' => "Le champs :attribute doit avoir plus que :value éléments",
    ],
    'gte' => [
        'numeric' => 'Le champs :attribute Doit être plus grand que ou égal à :value.',
        'file' => 'Le champs :attribute Doit être plus grand que ou égal à :value kilo-octets.',
        'string' => 'Le champs :attribute Doit être plus grand que ou égal à :value personnages.',
        'array' => 'Le champs :attribute doit avoir :value éléments ou plus.',
    ],
    'image' => 'Le champs :attribute Doit être une image.',
    'in' => 'Le champs select :attribute est invalide.',
    'in_array' => "Le champs :attribute  n'existe pas dans :other",
    'integer' => 'Le champs :attribute Doit être un entier.',
    'ip' => 'Le champs :attribute Doit être une adresse IP valide.',
    'ipv4' => 'Le champs :attribute Doit être une adresse IPV4 valide.',
    'ipv6' => 'Le champs :attribute Doit être une adresse IPV6 valide.',
    'json' => 'Le champs :attribute Doit être une chaîne JSON valide.',
    'lt' => [
        'numeric' => 'Le champs :attribute Doit être moins que :value.',
        'file' => 'Le champs :attribute Doit être moins que :value kilo-octets.',
        'string' => 'Le champs :attribute Doit être moins que :value personnages.',
        'array' => 'Le champs :attribute must have moins que :value éléments.',
    ],
    'lte' => [
        'numeric' => 'Le champs :attribute Doit être moins que ou égal à :value.',
        'file' => 'Le champs :attribute Doit être moins que ou égal à :value kilo-octets.',
        'string' => 'Le champs :attribute Doit être moins que ou égal à :value personnages.',
        'array' => 'Le champs :attribute ne doit pas avoir plus de:value éléments.',
    ],
    'max' => [
        'numeric' => "Le champs :attribute n'est peut être pas plus grand que:max.",
        'file' => "Le champs :attribute n'est peut être pas plus grand que:max kilo-octets.",
        'string' => "Le champs :attribute n'est peut être pas plus grand que:max personnages.",
        'array' => "Le champs :attribute ne peut pas avoir plus de :max éléments.",
    ],
    'mimes' => 'Le champs :attribute Doit être un fichier de type: :values.',
    'mimetypes' => 'Le champs :attribute Doit être un fichier de type: :values.',
    'min' => [
        'numeric' => 'Le champs :attribute Doit être au moins :min.',
        'file' => 'Le champs :attribute Doit être au moins :min kilo-octets.',
        'string' => 'Le champs :attribute Doit être au moins :min personnages.',
        'array' => 'Le champs :attribute must have au moins :min éléments.',
    ],
    'not_in' => 'Le champs select :attribute est invalide.',
    'not_regex' => 'Le champs :attribute format est invalide.',
    'numeric' => 'Le champs :attribute Doit être un nombre.',
    'password' => 'Le mot de passe est Incorrect.',
    'present' => 'Le champs :attribute Doit être present.',
    'regex' => 'Le champs :attribute format est invalide.',
    'required' => 'Le champs :attribute est requis.',
    'required_if' => 'Le champs :attribute est requis quand :other est :value.',
    'required_unless' => 'Le champs :attribute est requis à moins que :other soit dans :values.',
    'required_with' => 'Le champs :attribute est requis quand :values est present.',
    'required_with_all' => 'Le champs :attribute est requis quand :values sont presentes.',
    'required_without' => "Le champs :attribute est requis quand :values n'est pas present.'",
    'required_without_all' => 'Le champs :attribute est requis quand aucun de :values sont presentes.',
    'same' => 'Le champs :attribute et :other doivent correspondre.',
    'size' => [
        'numeric' => 'Le champs :attribute Doit être :size.',
        'file' => 'Le champs :attribute Doit être :size kilo-octets.',
        'string' => 'Le champs :attribute Doit être :size personnages.',
        'array' => 'Le champs :attribute doit contenir :size éléments.',
    ],
    'starts_with' => "Le champs :attribute doit commencer par l'un des éléments suivants: :values.",
    'string' => 'Le champs :attribute Doit être une chaîne de caractères.',
    'timezone' => 'Le champs :attribute Doit être une zone valide.',
    'unique' => 'Le champs :attribute a déjà été pris.',
    'uploaded' => 'Le champs :attribute échec du téléchargement.',
    'url' => 'Le champs :attribute format est invalide.',
    'uuid' => 'Le champs :attribute Doit être une UUID valide.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'message personnalisé',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
