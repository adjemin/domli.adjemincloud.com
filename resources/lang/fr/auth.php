<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => "Ces informations d'identification ne correspondent pas à nos enregistrements.",
    'throttle' => "Trop de tentatives de connexion. Veuillez réessayer dans: secondes secondes.",
    'sign_in'   => "Connexion",
    'login' => [
        'title' =>  'Interface de connexion administrateur',
        'forgot_password'   =>  "Réinitialisation du mot de passe"
    ],
    'register' =>  "S'inscrire",
    'email' =>  'Email',
    'password'  =>  'Mot de passe',
    'registration' =>   [
        'title' =>  "S'inscrire en tant qu'administrateur",
        'have_membership'   =>  "Avoir déjà un compte",
        'lorem' =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
        incididunt ut labore et dolore magna aliqua.'
    ],
    'full_name' =>  "Nom complet",
    'confirm_password'  =>  "Confirmez le mot de passe",
    'reset_password' => [
        'title' =>  "réinitialiser le mot de passe",
        'reset_pwd_btn'  =>  "Réinitialiser"
    ],
    'emails' => [
        'password'  => [
            'reset_link'    =>  " Réinitialiser le lien",
        ]
    ],
    'app' => [
        'messages'  => 'Notifications',
        'settings'  =>  "Réglages",
        'profile'   =>  'Profil',
        'lock_account'  =>  "Verrouiller le compte"
    ],
    'sign_out'  =>  "Déconnexion",
    'verify'    =>  [
        'title' => 'Vérifiez votre adresse e-mail',
        'success' => 'Un nouveau lien de vérification a été envoyé à
        votre adresse e-mail',
        'explain' =>    'Avant de procéder, veuillez consulter votre e-mail pour obtenir un lien de vérification. Si vous
        n’a pas reçu l’e-mail,',
        'button'    =>  'click here to request another'
    ],
];
