<?php

return [
    'how_to'  =>  'Comment devenir un livreur domli ???',
    'step_one_question' => '1- Comment devenir livreur sur Domli ?',
    'step_one'  =>  [
        'title' =>  'étape 1 : Télécharger l\'application',
        'step_one'  =>  'Trouvez l’application de livraison Domli sur l’AppStore et le PlayStore',
        'step_two'  =>  'Télécharger l’application et s’inscrire',
        'step_three'    =>  'Vous recevrez un e-mail d’activation après vérification de vos informations',
    ],
    'step_two_question' => '2- Comment recevoir les courses ?',
    'step_two'  =>  [
        'title' =>  'étape 2 : Effectuer des livraisons',
        'step_one'  =>  'Acceptez de partager votre position',
        'step_two'  =>  'Voir la liste des tâches non assignées',
        'step_three'    =>  'Voyez la distance entre votre position et le restaurant et la distance entre le restaurant et le client',
        'step_four' =>  'Accepter la tâche',
        'step_five' =>  'Afficher l’état des tâches',
        'step_six' =>  'Commencez le ramassage au restaurant',
        'step_seven'    =>  'Commencer la livraison au client'
    ],
    'step_three_question' => '3- Pourquoi devenir livreur sur Domli ?',
    'step_three'  =>  [
        'title' =>  'étape 3: Faire de l’argent sur Domli',
        'step_one'  =>  'Gagnez de l’argent supplémentaire pour vos objectifs, atteindre vos objectifs à court terme ou des rêves à long terme tout en conduisant avec Domli.',
    ],
    'step_four_question' => '4- Comment recevoir ma contre-partie?',
    'step_four'  =>  [
        'title' =>  'Étape 4 : Encaissement',
        'explain'   =>  'Vous voyez votre solde en temps réel et vous pouvez demander un retrait en suivant ces conseils :',
        'step_one'  =>  'Définir le montant',
        'step_two'  =>  'Définir le compte de paiement',
        'step_three'    =>  'Définir le numéro de compte',
        'step_four' =>  'Valider la demande de paiement',
    ],
    'download_title'    =>  'Vous pouvez maintenant télécharger l’application mobile !',
    'download_sub'  =>  'Télécharger sur :'
];