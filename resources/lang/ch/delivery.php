<?php

return [
    'how_to'  =>  '如何成為多姆利???的送貨員',
    'step_one_question' => '1-如何成為多姆利的送貨員？',
    'step_one'  =>  [
        'title' =>  '步驟1：下載應用程式',
        'step_one'  =>  '在應用商店和Play商店中查找多姆利交付應用程式',
        'step_two'  =>  '下載申請並註冊',
        'step_three'    =>  '驗證您的資訊後，您將收到啟動電子郵件',
    ],
    'step_two_question' => '2-如何接收比賽？',
    'step_two'  =>  [
        'title' =>  '第 2 步 ：交付',
        'step_one'  =>  '同意共用您的位置',
        'step_two'  =>  '查看未分配工作清單',
        'step_three'    =>  '查看從您的位置到餐廳的距離以及從餐廳到客戶的距離',
        'step_four' =>  '接受任務',
        'step_five' =>  '查看任務狀態',
        'step_six' =>  '在餐廳開始取貨',
        'step_seven'    =>  '開始交付給客戶'
    ],
    'step_three_question' => '3-為什麼要成為多姆利的送貨員？',
    'step_three'  =>  [
        'title' =>  '第3步：在多姆利賺錢',
        'step_one'  =>  '賺取額外的錢為你的目標，實現你的短期目標或長期夢想，而駕駛與多姆利。',
    ],
    'step_four_question' => '4-如何接收我的反部分？',
    'step_four'  =>  [
        'title' =>  '第4步：兌現',
        'explain'   =>  '您可以即時查看餘額，您可以通過以下提示請求套現：',
        'step_one'  =>  '定義金額',
        'step_two'  =>  '定義付款帳戶',
        'step_three'    =>  '定義帳號',
        'step_four' =>  '驗證付款請求',
    ],
    'download_title'    =>  '你現在可以下載移動應用程式！',
    'download_sub'  =>  '下載內容：'
];