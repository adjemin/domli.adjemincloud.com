<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => "這些憑據與我們的記錄不匹配",
    'throttle' => '登錄嘗試次數過多。請再試一次 :seconds 秒.',
    'sign_in'   => '登錄',
    'login' => [
        'title' =>  '管理員登錄界面',
        'forgot_password'   =>  '重設密碼'
    ],
    'register' =>  '登記',
    'email' =>  '電子郵件',
    'password'  =>  '密碼',
    'registration' =>   [
        'title' =>  '登記 作為管理員',
        'have_membership'   =>  '已經有一個帳號',
        'lorem' =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
        incididunt ut labore et dolore magna aliqua.'
    ],
    'full_name' =>  ' 全名',
    'confirm_password'  =>  '確認密碼',
    'reset_password' => [
        'title' =>  '重設密碼',
        'reset_pwd_btn'  =>  '重置'
    ],
    'emails' => [
        'password'  => [
            'reset_link'    => '重置鏈接',
        ]
    ],
    'app' => [
        'messages'  => '通知事項',
        'settings'  =>  '設定值',
        'profile'   =>  '輪廓',
        'lock_account'  =>  '鎖定賬戶'
    ],
    'sign_out'  =>  '登出',
    'verify'    =>  [
        'title' => '驗證您的電子郵寄地址',
        'success' => '已發送新的驗證連結
        您的電子郵件位址',
        'explain' =>    '在繼續之前，請檢查您的電子郵件是否有驗證連結。如果你
        未收到電子郵件，',
        'button'    =>  '單擊此處請求另一個'
    ],
];
