<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'sign_in'   => 'Login',
    'login' => [
        'title' =>  'Admin login interface',
        'forgot_password'   =>  'Password reset'
    ],
    'register' =>  'Register',
    'email' =>  'Email',
    'password'  =>  'Password',
    'registration' =>   [
        'title' =>  'Register as admin',
        'have_membership'   =>  'Have already an account',
        'lorem' =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
        incididunt ut labore et dolore magna aliqua.'
    ],
    'full_name' =>  'Full name',
    'confirm_password'  =>  'Confirm password',
    'reset_password' => [
        'title' =>  'Reset Password',
        'reset_pwd_btn'  =>  'Reset'
    ],
    'emails' => [
        'password'  => [
            'reset_link'    =>  'Reset link',
        ]
    ],
    'app' => [
        'messages'  => 'Notifications',
        'settings'  =>  'Settings',
        'profile'   =>  'Profile',
        'lock_account'  =>  'Lock account'
    ],
    'sign_out'  =>  'Sign out',
    'verify'    =>  [
        'title' => 'Verify Your Email Address',
        'success' => 'A fresh verification link has been sent to
        your email address',
        'explain' =>    'Before proceeding, please check your email for a verification link.If you
        did not receive the email,',
        'button'    =>  'click here to request another'
    ],
    
];
