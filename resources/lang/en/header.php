<?php

return [
    
    'home' => [
        'catchPhrase' => "Feeling a little peckish ? You've come to the right place",
        'homeLink1'  => 'Home Page',
        'homeLink2' => "Become",
        'homeLink22' => "Restaurant",
        'homeLink3' => "Restaurants",
        'homeLink4' => "Contact Us",
        'homeLink5' => "About Us",
        'homeLink6' => "Login",
        'homeLink7' => "Register",
        'homeLink8' => "Profil",
        'homeLink9' => "Logout",
        'placeholderIn' => "Enter your address",
        'hith3' =>"How it works",
        'hit1' =>"Order",
        'hit2' =>"Payment",
        'hit3' =>"Livraison",
        'mobileCallh1'=> "",
        'mobileCallp'=> "",
        'aboutSectiontitle1' =>"Customer",
        'aboutSectiontitle2' =>"Delivery man",
        'aboutSectiontitle3' =>"Restaurants",
        'aboutSectiontext1' =>"Welcome to all flavors
        Find the best restaurants you dream of nearby, choose your favorite dishes and thousands of new local and national dishes, in Canada and the Caribbean.
        Because every desire deserves to be fulfilled.",

        'aboutSectiontext2' =>"Attract more new customers, access a new source of income and maximize the capacity of your kitchen thanks to the main food delivery network available on Domli delivery.",
        
        'aboutSectiontext3' =>"Find many nearby meal deliveries.
        As a delivery driver, you'll make reliable 
        money—working anytime, anywhere.",

        'aboutSectiontextbe' =>"Become",
        'aboutSectiontextan' =>"now",

        'footertitle1' =>"Join us",
        'footertitle2' =>"Help",
        'footertitle3' =>"Legal",
        'footertitle4' =>"Follow us",

        'footerlink11' =>"Employement",
        'footerlink12' =>"Partner businesses",
        'footerlink13' =>"Courier",
        'footerlink14' =>"Domli Business",

        'footerlink21' =>"FAQ",
        'footerlink22' =>"Contact us",


        'footerlink31' =>"Terms & Conditions",
        'footerlink32' =>"Privacy Policy",
        'footerlink33' =>"Cookies Policy",


        'footerlink41' =>"FaceBook",
        'footerlink42' =>"Instagram",
        'footerlink43' =>"Twitter",


        'restaurant-banner-text'=>"Grow with Domli! Boost your sales and access new opportunities thanks to our platform! Join Domli now! Register your restaurant on Domli Increase your sales Customers are ordering more than ever online. Go meet them",

        'tab1'=>"Already a member",
        'tab2'=>"I am new here",
        'placeholderEa'=>"Email adress",
        'remember'=>"Remember me",
        'mdpo' => "Forgot your password?",
        'btnIdentification'=>"Login",
        'placeholderPsw'=>"Password",
        'placeholderFullname' => "Enter responsible full name",
        'placeholderResname' => "Enter your restaurant name",
        'placeholderResadd' => "Enter your restaurant address",
        'placeholderPswC' => "Confirm password",
        'privacyP1' => "I agree to the ",
        'privacyP2' => "Privacy policy",
        'privacyP3' => "and",
        'privacyP4' => "terms of service",
        'btnIncription' => "Sign up",

        'hitr1' => "Track your orders from preparation to delivery and increase your customer address book",
         'hitr2' => "Improve your sales and increase your profits",
         'hitr3' => "Deliver orders quickly and easily every day, 24 hours a day.",

        'delivery-newPaperh1' => "Partner restaurants",
        'delivery-newPaperp' => "  Become a Domli partner and attract even more customers. Do not worry about the delivery, we put you in touch with a network of delivery people nearby, you only manage the preparation! ",
        'delivery-newPaperbtn' => "See more",

    ],
    'no_order_progress'  =>  'No order in progress',
    'cooking'   =>  'your order is being cooked',
    'waiting_acceptation_order' =>  'your order waiting for confirmation',
    'waiting_customer_delivery' => 'Waiting customer delivery',
    'delivered_order'   =>  'your order is being delivered',
    'delivered_question'  =>   'Did you receive your order?',
    'save'  =>  'Save',
    'note_title'    =>  'Note the order here',
    'desc_note' =>  'leave a note',
    'penalty_canceled'  =>  '8% of the amount paid will be returned to the customer and delivery person, do you want to cancel the order?',
    'canceled_order'    =>  'do you want to cancel the order?',
    'see_more'  =>  'See more',
    'popular_near'  =>  'Popular near you',
    'our_suggestion' =>   'Our suggestions',
    'congratulations'   =>  'Congratulations',
    'confirm_text'  =>  'Your Domli account has been created, confirm your email by consulting the email that was sent to you at the address',
    'update_profile_title'  =>  'Update your profile',
    'full_name' =>  'Full name',
    'e_mail'    =>  'E-mail',
    'update_photo'  =>  'Update your photo',
    'name_user' =>   ' Name',
    'surname'   =>  'Surname',
    'update_first_name_tip' =>  'update your first name',
    'last_name_user'  => 'Last Name',
    'update_last_name_tip' =>  'update your first name',
    'update_email_tip'  =>  'E-mail adress',
    'field_empty' =>    'Error . Check that no field is empty.',
    'save_change'   =>  'Save changes',
    'cancel_text'   =>  'Cancel',
    'compostion'    =>  'Composition',
    'date'  =>  'Date',
    'payment'  =>    'Means of payment',
    'status'    =>  'Status',
    'detail'    =>  'Detail',
    'menu'  =>  'Menu',
    'profile'   =>  'Profile',
    'orders'    =>  'Orders',
    'about_us'  =>  'About us',
    'about_title'   =>  'Domli is a digital company that offers online order taking and delivery services through its Domli and Domli Delivery Platform.',
    'about_desc'    =>  'These services connect Customers, Restaurants and nearby delivery people.',
    'get_started'   =>  'Get started',
    'my_cart'   =>  'My shopping cart',
    'buy'   =>  'Buy now',
    'clear_cart'    =>  'Clear cart',
    'promo_code'    =>  'Promo code',
    'promo_question'    =>  'Are you a promo code ?',
    'check' =>  'Check',
    'price' =>  'Price',
    'remove'    =>  'Remove',
    'quantity'  =>  'Quantity',
    'empty_cart'    =>  'Your cart is empty',
    'cart_detail'   =>  'Cart detail',
    'items' =>  'Items',
    'subtotal'  =>  'Subtotal',
    'tax'   =>  'Tax',
    'total_discount'    =>  'Total discount',
    'total_pay' =>  'Total pay',
    'welcome_domli' =>  'Welcome to domli',
    'signin_welcome_link'   =>  'Click here to sign in now',
    'lorem' =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore eum repellat voluptate omnis odit non neque velit molestiae nobis. Asperiores voluptas facilis nobis quos sed? Minus consequuntur autem dolorum error sapiente adipisci, quaerat ab accusantium magni soluta quis nihil. Necessitatibus iusto vero temporibus praesentium, voluptates dolorum ipsum ',
    'e_mail_require'    =>  'E-mail (require)',
    'object_require'    =>  'Object (require)',
    'enter_message' =>  'Enter your message here',
    'send_message'  =>  'Send message',
    'current_status'    =>  'Current status',
    'payment_method'    =>  'Payment method',
    'amount'    =>  'Amount',
    'delivery_date'  =>  'Delivery date',
    'delivery_location' =>  'Delivery location name',
    'fees'  =>  'Delivery fees',
    'kilometers'    =>  'Kilometers',
    'actions'   =>   'Actions',
    'products_detail'   =>  'Product(s) Detail',
    'name'  =>  'Name',
    'restaurant_address'   =>  'restaurant address',
    'delivery_detail'   =>  'Delivery detail',
    'phone' =>  'Phone',
    'no_delivery'   =>  'No delivery',
    'our_restaurant'    =>  'Our different restaurants',
    'domli_about'   =>  'Domli is a digital company that offers online order taking and delivery services through its Domli and Domli Delivery Platform.These services connect Customers, Restaurants and nearby delivery people.',
    'customer'  =>  'Customer',
    'customer_about'    =>  'We bring restaurant customers together via the Web and Mobile.We hope that from Mobile, no one will have difficulty finding meals to satisfy their cravings.We make it easy for customers to find the best restaurants nearby, access a plethora of menus and have a quick delivery anywhere.',
    'restaurant'    =>  'Restaurant',
    'restaurant_about'  =>  'We allow restaurants to gain visibility, develop their brand and increase their turnover by finding new customers nearby.',
    'delivery_man'  =>  'Delivery man',
    'delivery_man_about'    =>  'You decide what you earn.You benefit from flexibility and financial stability. Find orders 24/7.You are your own boss, you choose when you log in and what orders you accept.',
    'search'    =>  'Search',
    'search_dish'   =>  'Search dish now',
    'no_name'   =>  'Name not found',
    'custom_order'  =>   'Customize your order',
    'required'   =>  'required',
    'checked'   =>  'checked',
    'notice'    =>  'notice',
    'desc'  =>  'description',
    'no_result' =>  'No results',
    'contact_title' =>  'For all your concerns or for more information, please leave us your need.Our team is responsible for assisting you by providing you with more information.',
    'other' =>    'Other',
    'order_detail'  =>  'Order detail',
    'enter_address' =>  'Enter delivery adress ',
    'payment_info'  =>  'Payment information',
    'credit_card'   =>  'Add credit card',
    'delivery_pay'  =>  'Pay on delivery',
    'confirm_pay'   =>  'Confirm order',
    'as_soon'   =>  'as soon as possible',
    'items_total'   =>    'Items total',
    'distance'  =>  'Distance',
    'delivery'  =>  'delivery',
    'total_discount'    =>  'Total discount',
    'and'   =>  'and',
    'login_customer' => [
        'title' =>  'Login using media social to get quick access',
        'facebook_title'    => 'Sign in with facebook',
        'gmail_title'   =>  'Sign in with gmail',
        'already'   =>  'Already a member',
        'new'   =>  'I am new here',
        'email' =>  'E-mail adress',
        'password'  =>  'Password',
        'confirm_password'  =>  'Confirm password',
        'remember'  =>  'Remember me',
        'forget'    =>  'Forget password ?',
        'sign_up'   =>  'Sign up',
        'agree' =>  'I agree to the ',
        'policy'    =>  'Prevacy policy',
        'terms'  =>  'terms of service',
    ],
    'no_restaurant_found'   =>  'Nothing restaurant found',
    'no_name_found' =>  'Name not found',
    'login_text'    =>  'Grow with Domli!
    Boost sales and access new opportunities thanks to our platform!

    Join Domli now!

    Register your restaurant on Domli

    Increase your sales

    Customers are ordering more than ever online. Go meet them',
];