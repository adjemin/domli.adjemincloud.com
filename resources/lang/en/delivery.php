<?php

return [
    'how_to'  =>  'How to become a delivery man on domli ???',
    'step_one_question' => '1- How to become a delivery person on Domli?',
    'step_one'  =>  [
        'title' =>  'step 1 :Download the Application',
        'step_one'  =>  'Find the Domli delivery application on the AppStore and PlayStore',
        'step_two'  =>  'Download the Application and Register',
        'step_three'    =>  'You will receive an activation email after verifying your information',
    ],
    'step_two_question' => '2- How to receive the races ?',
    'step_two'  =>  [
        'title' =>  'step 2 :Make deliveries',
        'step_one'  =>  'Agree to share your location',
        'step_two'  =>  'See the list of unassigned tasks',
        'step_three'    =>  'See the distance from your position to the Restaurant and the distance from the Restaurant to the Client',
        'step_four' =>  'Accept the task',
        'step_five' =>  'View task status',
        'step_six' =>  'Start pickup at the Restaurant',
        'step_seven'    =>  'Start delivery to the Customer'
    ],
    'step_three_question' => '3- Why become a delivery man on Domli?',
    'step_three'  =>  [
        'title' =>  'step 3: Make money on Domli',
        'step_one'  =>  'Earn extra money for your goals,Achieve your short term goals or long term dreams while driving with Domli.',
    ],
    'step_four_question' => '4- How to receive my counterpart?',
    'step_four'  =>  [
        'title' =>  'Step 4: Cashing out',
        'explain'   =>  'You see your balance in real time and you can request a cashout by following these tips:',
        'step_one'  =>  'Define the amount',
        'step_two'  =>  'Define the payment account',
        'step_three'    =>  'Define the account number',
        'step_four' =>  'Validate the payment request',
    ],
    'download_title'    =>  'You can now download the mobile app !',
    'download_sub'  =>  'Download on :'
];