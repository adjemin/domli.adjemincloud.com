<!-- Adress Name Field -->
<div class="form-group">
    {!! Form::label('adress_name', 'Adress Name:') !!}
    <p>{{ $deliveryAdresses->adress_name }}</p>
</div>

<!-- Adress Slug Field -->
<div class="form-group">
    {!! Form::label('adress_slug', 'Adress Slug:') !!}
    <p>{{ $deliveryAdresses->adress_slug }}</p>
</div>

<!-- Lng Field -->
<div class="form-group">
    {!! Form::label('lng', 'Lng:') !!}
    <p>{{ $deliveryAdresses->lng }}</p>
</div>

<!-- Town Field -->
<div class="form-group">
    {!! Form::label('town', 'Town:') !!}
    <p>{{ $deliveryAdresses->town }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $deliveryAdresses->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $deliveryAdresses->updated_at }}</p>
</div>

