<!-- Adress Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('adress_name', 'Adress Name:') !!}
    {!! Form::text('adress_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Adress Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('adress_slug', 'Adress Slug:') !!}
    {!! Form::text('adress_slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Lng Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lng', 'Lng:') !!}
    {!! Form::number('lng', null, ['class' => 'form-control']) !!}
</div>

<!-- Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lat', 'Lat:') !!}
    {!! Form::number('lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Town Field -->
<div class="form-group col-sm-6">
    {!! Form::label('town', 'Town:') !!}
    {!! Form::text('town', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('deliveryAdresses.index') }}" class="btn btn-secondary">Cancel</a>
</div>
