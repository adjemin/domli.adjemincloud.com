@php
    $account = \App\Models\RestaurantCompte::where('restaurant_id', Auth::guard('restaurant')->user()->id)->first();
@endphp

<input type="hidden" name="restaurant_id" value="{{Auth::guard('restaurant')->user()->id}}">
<div class="row">
    <div class="col-6">
        <!-- Amount Field -->
        <div class="form-group">
            {!! Form::label('amount', 'Amount:') !!}
            {!! Form::number('amount', null, ['required' => 'required', 'min' => 0, 'max' => intval($account->compte) ?? 0]) !!}
        </div>
    </div>
    <div class="col-6">
        <!-- Date Field -->
        <div class="form-group">
            {!! Form::label('date', 'Date:') !!}
            {!! Form::date('date', null, ['required' => 'required']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <!-- Paiement Method Field -->
        <div class="form-group">
            {!! Form::label('paiement_method', 'Paiement Method:') !!}
            {!! Form::text('paiement_method', null, ['required' => 'required', 'minLength' => 2]) !!}
        </div>
    </div>
    <div class="col-6">
        <!-- Account Reference Field -->
        <div class="form-group">
            {!! Form::label('account_reference', 'Account Reference:') !!}
            {!! Form::text('account_reference', null, ['required' => 'required', 'minLength' => 6]) !!}
        </div>
    </div>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('requestRestaurantPayments.index') }}" class="btn btn-secondary">Cancel</a>
</div>
