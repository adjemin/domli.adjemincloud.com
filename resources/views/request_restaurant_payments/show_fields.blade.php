<div class="row">
    <div class="col-6">
        <!-- Amount Field -->
        <div class="form-group">
            {!! Form::label('amount', 'Amount:') !!}
            <p>{{ $requestRestaurantPayment->amount }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Date Field -->
        <div class="form-group">
            {!! Form::label('date', 'Date:') !!}
            <p>{{ $requestRestaurantPayment->date }}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <!-- Paiement Method Field -->
        <div class="form-group">
            {!! Form::label('paiement_method', 'Paiement Method:') !!}
            <p>{{ $requestRestaurantPayment->paiement_method }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Account Reference Field -->
        <div class="form-group">
            {!! Form::label('account_reference', 'Account Reference:') !!}
            <p>{{ $requestRestaurantPayment->account_reference }}</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Created At:') !!}
            <p>{{ \Carbon\Carbon::parse($requestRestaurantPayment->created_at)->diffForHumans() }}</p>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            {!! Form::label('is_paid', 'Is Paid:') !!}
            <p>{!! $requestRestaurantPayment->is_paid ? '<span style="color:green">Yes</span>' :  '<span style="color:red">No</span>' !!}</p>
        </div>
    </div>
</div>

@if(!$requestRestaurantPayment->is_paid && Auth::check())
    {!! Form::open(['route' => 'restaurantPaymentHistoriques.store']) !!}
        <input type="hidden" name="amount" value="{{$requestRestaurantPayment->amount}}">
        <input type="hidden" name="restaurant_id" value="{{$requestRestaurantPayment->restaurant_id}}">
        <input type="hidden" name="request_id" value="{{$requestRestaurantPayment->id}}">
        <div class="row">
            <div class="col-12">
                <!-- Date Field -->
                <div class="form-group">
                    {!! Form::label('reference_paiement', 'Payment reference :') !!}
                    {!! Form::text('reference_paiement', null, ['class' => 'form-control', 'required' => 'required', 'minLength' => 6]) !!}
                </div>
            </div>
            <input type="submit" value="Approuved" class="btn btn-success float-right py-2 px-3">
        </div>
    {!! Form::close() !!}
@endif
{{-- <!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $requestRestaurantPayment->updated_at }}</p>
</div> --}}