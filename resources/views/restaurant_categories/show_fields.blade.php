{{--
<!-- Restaurant Id Field -->
<div class="form-group">
    {!! Form::label('restaurant_id', 'Restaurant Id:') !!}
    <p>{{ $restaurantCategories->restaurant_id }}</p>
</div>

<!-- Food Categorie Id Field -->
<div class="form-group">
    {!! Form::label('food_categorie_id', 'Food Categorie Id:') !!}
    <p>{{ $restaurantCategories->food_categorie_id }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $restaurantCategories->slug }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $restaurantCategories->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $restaurantCategories->updated_at }}</p>
</div>

--}}
<!-- Restaurant Id Field -->
<div class="form-group">
    {!! Form::label('restaurant_id', 'Restaurant:') !!}
    <p>{{ $restaurantCategories->restaurant->name ?? '' }}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category:') !!}
    <p>{{ $restaurantCategories->category->title ?? '' }}</p>
</div>

<!-- Category Slug Field -->
<div class="form-group">
    {!! Form::label('category_slug', 'Category Slug:') !!}
    <p>{{ $restaurantCategories->category->slug }}</p>
</div>

{{--
<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $restaurantCategory->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $restaurantCategory->updated_at }}</p>
</div>
--}}

