{{--
@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('restaurantCategories.index') }}">Restaurant Categories</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('restaurantCategories.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('restaurant_categories.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
--}}
@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">Restaurant Category</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ !\Auth::check() ? route('backoffice-restaurant-categorie-index', ['restaurant_id' => $restaurantCategories->restaurant->id]) : route('admin-restaurant-categorie-index', ['restaurant_id' => $restaurantCategories->restaurant->id]) }}" class="pull-right btn btn-primary">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('restaurant_categories.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
