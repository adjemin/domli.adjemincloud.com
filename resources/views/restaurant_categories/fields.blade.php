{{--<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('restaurantCategories.index') }}" class="btn btn-secondary">Cancel</a>
</div>
--}}
<!-- Restaurant Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('restaurant_id', 'Restaurant Id:') !!}
    {!! Form::text('restaurant_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Category Id:') !!}
    {!! Form::text('category_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_slug', 'Category Slug:') !!}
    {!! Form::text('category_slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! \Auth::check() ? route('admin-restaurant-categorie-index', ['restaurant_id' => $restaurant->uuid]) : route('backoffice-restaurant-categorie-index', ['restaurant_id' => $restaurant->uuid]) !!}" class="btn btn-secondary">Cancel</a>
</div>
