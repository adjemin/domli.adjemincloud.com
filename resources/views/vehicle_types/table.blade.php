<div class="table-responsive-sm">
    <table class="table table-striped" id="vehicleTypes-table">
        <thead>
            <tr>
                <th>Title</th>
        <th>Description</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($vehicleTypes as $vehicleTypes)
            <tr>
                <td>{{ $vehicleTypes->title }}</td>
            <td>{{ $vehicleTypes->description }}</td>
                <td>
                    {!! Form::open(['route' => ['vehicleTypes.destroy', $vehicleTypes->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('vehicleTypes.show', [$vehicleTypes->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('vehicleTypes.edit', [$vehicleTypes->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>