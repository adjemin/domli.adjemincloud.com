<!-- Location Name Field -->
<div class="form-group">
    {!! Form::label('location_name', 'Location Name:') !!}
    <p>{{ $job->location_name }}</p>
</div>

<!-- Location Description Field -->
<div class="form-group">
    {!! Form::label('location_description', 'Location Description:') !!}
    <p>{{ $job->location_description }}</p>
</div>

<!-- Deliverer Id Field -->
<div class="form-group">
    {!! Form::label('deliverer_id', 'Deliverer Id:') !!}
    <p>{{ $job->deliverer_id }}</p>
</div>

<!-- Location Lat Field -->
<div class="form-group">
    {!! Form::label('location_lat', 'Location Lat:') !!}
    <p>{{ $job->location_lat }}</p>
</div>

<!-- Location Lng Field -->
<div class="form-group">
    {!! Form::label('location_lng', 'Location Lng:') !!}
    <p>{{ $job->location_lng }}</p>
</div>

<!-- Is Pickup Field -->
<div class="form-group">
    {!! Form::label('is_pickup', 'Is Pickup:') !!}
    <p>{{ $job->is_pickup }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $job->status }}</p>
</div>

<!-- Signature Url Field -->
<div class="form-group">
    {!! Form::label('signature_url', 'Signature Url:') !!}
    <p>{{ $job->signature_url }}</p>
</div>

<!-- Images Field -->
<div class="form-group">
    {!! Form::label('images', 'Images:') !!}
    <p>{{ $job->images }}</p>
</div>

<!-- Notes Field -->
<div class="form-group">
    {!! Form::label('notes', 'Notes:') !!}
    <p>{{ $job->notes }}</p>
</div>

<!-- Note Audio Field -->
<div class="form-group">
    {!! Form::label('note_audio', 'Note Audio:') !!}
    <p>{{ $job->note_audio }}</p>
</div>

<!-- Before Date Field -->
<div class="form-group">
    {!! Form::label('before_date', 'Before Date:') !!}
    <p>{{ $job->before_date }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $job->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $job->updated_at }}</p>
</div>

