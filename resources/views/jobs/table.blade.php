<div class="table-responsive-sm">
    <table class="table table-striped" id="jobs-table">
        <thead>
            <tr>
                <th>Location Name</th>
        <th>Location Description</th>
        <th>Deliverer Id</th>
        <th>Location Lat</th>
        <th>Location Lng</th>
        <th>Is Pickup</th>
        <th>Status</th>
        <th>Signature Url</th>
        <th>Images</th>
        <th>Notes</th>
        <th>Note Audio</th>
        <th>Before Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($jobs as $job)
            <tr>
                <td>{{ $job->location_name }}</td>
            <td>{{ $job->location_description }}</td>
            <td>{{ $job->deliverer_id }}</td>
            <td>{{ $job->location_lat }}</td>
            <td>{{ $job->location_lng }}</td>
            <td>{{ $job->is_pickup }}</td>
            <td>{{ $job->status }}</td>
            <td>{{ $job->signature_url }}</td>
            <td>{{ $job->images }}</td>
            <td>{{ $job->notes }}</td>
            <td>{{ $job->note_audio }}</td>
            <td>{{ $job->before_date }}</td>
                <td>
                    {!! Form::open(['route' => ['jobs.destroy', $job->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('jobs.show', [$job->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('jobs.edit', [$job->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>