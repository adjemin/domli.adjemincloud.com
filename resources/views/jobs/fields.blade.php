<!-- Location Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_name', 'Location Name:') !!}
    {!! Form::text('location_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('location_description', 'Location Description:') !!}
    {!! Form::textarea('location_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Deliverer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deliverer_id', 'Deliverer Id:') !!}
    {!! Form::select('deliverer_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Location Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_lat', 'Location Lat:') !!}
    {!! Form::text('location_lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Lng Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_lng', 'Location Lng:') !!}
    {!! Form::text('location_lng', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Pickup Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_pickup', 'Is Pickup:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_pickup', 0) !!}
        {!! Form::checkbox('is_pickup', '1', null) !!}
    </label>
</div>


<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Signature Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('signature_url', 'Signature Url:') !!}
    {!! Form::text('signature_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Notes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('notes', 'Notes:') !!}
    {!! Form::text('notes', null, ['class' => 'form-control']) !!}
</div>

<!-- Before Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('before_date', 'Before Date:') !!}
    {!! Form::text('before_date', null, ['class' => 'form-control','id'=>'before_date']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#before_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('jobs.index') }}" class="btn btn-secondary">Cancel</a>
</div>
