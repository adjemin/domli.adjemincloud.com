<div class="row">
    <div class="col-6">
        <!-- Amount Field -->
        <div class="form-group">
            {!! Form::label('amount', 'Amount:') !!}
            <p>{{ $requestDeliveryPayment->amount.' USD' }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Paiement Method Field -->
        <div class="form-group">
            {!! Form::label('paiement_method', 'Paiement Method:') !!}
            <p>{{ $requestDeliveryPayment->paiement_method }}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <!-- Date Field -->
        <div class="form-group">
            {!! Form::label('date', 'Date:') !!}
            <p>{{ $requestDeliveryPayment->date }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Account Reference Field -->
        <div class="form-group">
            {!! Form::label('account_reference', 'Account Reference:') !!}
            <p>{{ $requestDeliveryPayment->account_reference }}</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group">
            {!! Form::label('created_at', 'Created At:') !!}
            <p>{{ \Carbon\Carbon::parse($requestDeliveryPayment->created_at)->diffForHumans() }}</p>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            {!! Form::label('is_paid', 'Is Paid:') !!}
            <p>{!! $requestDeliveryPayment->is_paid ? '<span style="color:green">Yes</span>' :  '<span style="color:red">No</span>' !!}</p>
        </div>
    </div>
</div>

@if(!$requestDeliveryPayment->is_paid)

    {!! Form::open(['route' => 'deliveryPaymentHistoriques.store']) !!}
        <input type="hidden" name="amount" value="{{$requestDeliveryPayment->amount}}">
        <input type="hidden" name="delivery_id" value="{{$requestDeliveryPayment->delivery_id}}">
        <input type="hidden" name="request_id" value="{{$requestDeliveryPayment->id}}">
        <div class="row">
            <div class="col-12">
                <!-- Date Field -->
                <div class="form-group">
                    {!! Form::label('reference_paiement', 'Payment reference :') !!}
                    {!! Form::text('reference_paiement', null, ['class' => 'form-control', 'required' => 'required', 'minLength' => 6]) !!}
                </div>
            </div>
            <input type="submit" value="Approuved" class="btn btn-success float-right py-2 px-3">
        </div>
    {!! Form::close() !!}
@endif
{{-- <!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $requestDeliveryPayment->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $requestDeliveryPayment->updated_at }}</p>
</div> --}}

