<div class="row">
    <div class="col-6">
        <!-- Email Field -->
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            <p>{{ $contact->email }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Object Field -->
        <div class="form-group">
            {!! Form::label('object', 'Object:') !!}
            <p>{{ $contact->object }}</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <!-- Message Field -->
        <div class="form-group">
            {!! Form::label('message', 'Message:') !!}
            <p>{{ $contact->message }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Created At:') !!}
            <p>{{ \Carbon\Carbon::parse($contact->created_at)->diffForHumans() }}</p>
        </div>
    </div>
</div>

{{-- <!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $contact->updated_at }}</p>
</div>
 --}}