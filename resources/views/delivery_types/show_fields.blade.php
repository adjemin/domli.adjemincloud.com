<div class="form-group">
    {!! Form::label('image', 'Title:') !!}
    <p><img src="{{ $deliveryTypes->image }}" heigth="150" alt="{{ $deliveryTypes->title }}"> </p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $deliveryTypes->title }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $deliveryTypes->description }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $deliveryTypes->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $deliveryTypes->updated_at }}</p>
</div>

