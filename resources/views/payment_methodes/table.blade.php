<div class="table-responsive-sm">
    <table class="table table-striped" id="paymentMethodes-table">
        <thead>
            <tr>
                <th>Title</th>
        <th>Slug</th>
        <th>Description</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($paymentMethodes as $paymentMethodes)
            <tr>
                <td>{{ $paymentMethodes->title }}</td>
            <td>{{ $paymentMethodes->slug }}</td>
            <td>{{ $paymentMethodes->description }}</td>
                <td>
                    {!! Form::open(['route' => ['paymentMethodes.destroy', $paymentMethodes->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('paymentMethodes.show', [$paymentMethodes->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('paymentMethodes.edit', [$paymentMethodes->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>