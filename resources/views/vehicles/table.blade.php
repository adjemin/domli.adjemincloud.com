<div class="table-responsive-sm">
    <table class="table table-striped" id="vehicles-table">
        <thead>
            <tr>
                <th>Vehicletype Id</th>
        <th>Deliverer Id</th>
        <th>Is Active</th>
        <th>Numero Police</th>
        <th>Date Start</th>
        <th>Date End</th>
        <th>Vehicule Name</th>
        <th>Numero Serie</th>
        <th>Immatriculation</th>
        <th>Is Verified</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($vehicles as $vehicle)
            <tr>
                <td>{{ $vehicle->vehicleType_id }}</td>
            <td>{{ $vehicle->deliverer_id }}</td>
            <td>{{ $vehicle->is_active }}</td>
            <td>{{ $vehicle->numero_police }}</td>
            <td>{{ $vehicle->date_start }}</td>
            <td>{{ $vehicle->date_end }}</td>
            <td>{{ $vehicle->vehicule_name }}</td>
            <td>{{ $vehicle->numero_serie }}</td>
            <td>{{ $vehicle->immatriculation }}</td>
            <td>{{ $vehicle->is_verified }}</td>
                <td>
                    {!! Form::open(['route' => ['vehicles.destroy', $vehicle->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('vehicles.show', [$vehicle->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('vehicles.edit', [$vehicle->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>