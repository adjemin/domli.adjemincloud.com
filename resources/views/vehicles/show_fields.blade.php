<!-- Vehicletype Id Field -->
<div class="form-group">
    {!! Form::label('vehicleType_id', 'Vehicletype Id:') !!}
    <p>{{ $vehicle->vehicleType_id }}</p>
</div>

<!-- Deliverer Id Field -->
<div class="form-group">
    {!! Form::label('deliverer_id', 'Deliverer Id:') !!}
    <p>{{ $vehicle->deliverer_id }}</p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    <p>{{ $vehicle->is_active }}</p>
</div>

<!-- Numero Police Field -->
<div class="form-group">
    {!! Form::label('numero_police', 'Numero Police:') !!}
    <p>{{ $vehicle->numero_police }}</p>
</div>

<!-- Date Start Field -->
<div class="form-group">
    {!! Form::label('date_start', 'Date Start:') !!}
    <p>{{ $vehicle->date_start }}</p>
</div>

<!-- Date End Field -->
<div class="form-group">
    {!! Form::label('date_end', 'Date End:') !!}
    <p>{{ $vehicle->date_end }}</p>
</div>

<!-- Vehicule Name Field -->
<div class="form-group">
    {!! Form::label('vehicule_name', 'Vehicule Name:') !!}
    <p>{{ $vehicle->vehicule_name }}</p>
</div>

<!-- Numero Serie Field -->
<div class="form-group">
    {!! Form::label('numero_serie', 'Numero Serie:') !!}
    <p>{{ $vehicle->numero_serie }}</p>
</div>

<!-- Immatriculation Field -->
<div class="form-group">
    {!! Form::label('immatriculation', 'Immatriculation:') !!}
    <p>{{ $vehicle->immatriculation }}</p>
</div>

<!-- Is Verified Field -->
<div class="form-group">
    {!! Form::label('is_verified', 'Is Verified:') !!}
    <p>{{ $vehicle->is_verified }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $vehicle->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $vehicle->updated_at }}</p>
</div>

