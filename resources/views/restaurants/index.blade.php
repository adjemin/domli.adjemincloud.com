@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Restaurants</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             {{-- <i class="fa fa-align-justify"></i> --}}
                             Restaurants
                            {{-- <a class="pull-right" href="{{ route('restaurants.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>--}}
                         </div>
                         <div class="card-body">
                            @include('restaurants.table')
                            <div class="d-flex justify-content-center mr-3">
                                {!! $restaurants->links() !!}
                            </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

