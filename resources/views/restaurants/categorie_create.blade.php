@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! \Auth::check() ? route('admin-restaurant-categorie-index', ['restaurant_id' => $restaurant->uuid]) : route('backoffice-restaurant-categorie-index', ['restaurant_id' => $restaurant->uuid]) !!}">Food Categories</a>
      </li>
      <!-- <li class="breadcrumb-item active">Add</li> -->
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header" style="display: flex; justify-content: space-between">
                                <!-- <i class="fa fa-plus-square-o fa-lg"></i> -->
                                <strong>food Categories</strong>
                                <a class="pull-right" href="{{ Auth::check() ? '/admin/restaurant/'.$restaurant->uuid.'/category/create' : '/restaurant/'.$restaurant->uuid.'/category/create'}}">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve" width="28" height="28">
                                        <circle style="fill:#E8C52A;" cx="25" cy="25" r="25"/>
                                        <line style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="25" y1="13" x2="25" y2="38"/>
                                        <line style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="37.5" y1="25" x2="12.5" y2="25"/>
                                    </svg>
                                </a>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'restaurantCategories.store']) !!}

                                   @include('restaurants.category_table')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
