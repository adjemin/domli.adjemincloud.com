<!-- Name Field -->
{{--<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Surname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('surname', 'Surname:') !!}
    {!! Form::text('surname', null, ['class' => 'form-control']) !!}
</div>

<!-- Cover Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover', 'Cover:') !!}
    {!! Form::file('cover') !!}
</div>
<div class="clearfix"></div>

<!-- Logo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logo', 'Logo:') !!}
    {!! Form::file('logo') !!}
</div>
<div class="clearfix"></div>

<!-- Lng Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lng', 'Lng:') !!}
    {!! Form::number('lng', null, ['class' => 'form-control']) !!}
</div>

<!-- Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lat', 'Lat:') !!}
    {!! Form::number('lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('restaurants.index') }}" class="btn btn-secondary">Cancel</a>
</div>--}}

<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('logo', 'Logo:') !!}    
        <input type="file" name="logo_file" class="form-control" onchange="loadFile1(event)" value="{{old('logo_file')}}"/>
    </div>

    <!-- Cover Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('cover', 'Cover:') !!}
        <input type="file" name="cover_file" class="form-control" onchange="loadFile(event)" value="{{old('cover_file')}}"/>
    </div>
</div>
<div id="ouput">
    <div class="row">
        <div class="col-lg-4 col-sm-4 col-xs-12">
            @if(isset($restaurants))
                <img src="{{$restaurants->logo}}" width="200" id="output_logo" alt="{{$restaurants->title}}">
            @else
                <img src="" width="200" id="output_logo" alt="">
            @endif
        </div>
        <div class="col-lg-8 col-sm-8 col-xs-12">
            @if(isset($restaurants))
                <img src="{{$restaurants->cover}}" width="450" id="output_cover" alt="{{$restaurants->title}}">
            @else
                <img src="" width="450" id="output_cover" alt="">
            @endif
        </div>
    </div>
</div>
<div class="row mt-4">
    <!-- Title Field -->
    <!-- Location Field -->
    <div class="form-group col-6">
        <div class="row">
            <div class="col-8">
                <label>Adresse</label>
                <input type="text" id="addr" name="address" class="form-control"  style="border:2px solid black !important;" id="address"  />
            </div>
            <div class="col-4">
                <a onclick="addr_search(event)" class="mt-4 btn btn-primary">Search</a>
            </div>
        </div>
    </div>
    <div class="form-group col-6">
        {!! Form::label('title', 'Title:') !!}
        @if(isset($restaurants))
            {!! Form::text('title', old('title') ?? $restaurants->title, ['class' => 'form-control', 'style' =>"border:2px solid black !important;"]) !!}
        @else
            {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder'=>'restaurant name' , 'style' =>"border:2px solid black !important;"]) !!}
        @endif
    </div>
</div>
<div id="results" class="py-2">

</div>

<div id="map">
</div>

<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('Latitude', 'Latitude:') !!}
        @if(isset($restaurants))
            {!! Form::text('lat', old('lat') ?? $restaurants->lat, ['id' => 'lat','class' => 'form-control', 'readonly' => 'readonly']) !!}
        @else
            {!! Form::text('lat', old('lat'), ['id' => 'lat','class' => 'form-control', 'readonly' => 'readonly']) !!}
        @endif
    </div>

    <!-- Location Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('Longitude', 'Longitude:') !!}
        @if(isset($restaurants))
            {!! Form::text('lng', old('lng') ?? $restaurants->lng, ['id' => 'lng','class' => 'form-control', 'readonly' => 'readonly']) !!}
        @else
            {!! Form::text('lng', old('lng'), ['id' => 'lng','class' => 'form-control', 'readonly' => 'readonly']) !!}
        @endif
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('description', 'Description:') !!}
        @if(isset($restaurants))
            {!! Form::textarea('description', old('description') ?? $restaurants->description, ['id' => 'description','class' => 'form-control']) !!}
        @else
            {!! Form::textarea('description', old('description'), ['id' => 'description','class' => 'form-control']) !!}
        @endif
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-12">
        <a href="{{ url()->previous() }}" class="btn btn-secondary float-right mx-3">Cancel</a>
        {!! Form::submit('Save', ['class' => 'btn card-right-btn float-right  mx-3']) !!}
    </div>
</div>