<input type="hidden" name="restaurant_id" value="{{$restaurant->id}}">
<div class="row">
    <div class="col-6">
        <!-- Title Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    </div>
    <div class="col-6">
        <!-- Submit Field -->
        <div class="form-group col-sm-12" style="display: flex; justify-content: flex-end; align-items: center; height: 100%;">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{!! \Auth::check() ? route('admin-restaurant-menu-index', ['restaurant_id' => $restaurant->uuid]) : route('backoffice-restaurant-menu-index', ['restaurant_id' => $restaurant->uuid]) !!}" class="btn btn-danger " style="margin-left: 10px">Cancel</a>
        </div>
    </div>
</div>
