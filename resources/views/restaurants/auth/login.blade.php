
@extends('layouts.index')


@section('content')
    @include('customer.pageParts.pagesHeader')
    <section class="login-logup-page container">
        <div class="row content">
            <div class="text-card col-md-5">
                <div class="text-card-content">
                    <div class="logo-container">
                        <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="90" alt="...">
                    </div>
                    <h6>Login using media social to get quick access</h6>
                    <div class="media-social-btn">
                        <a href="{!! url('/auth/facebook') !!}" class="fb-btn btn">
                            <svg id="Bold" enable-background="new 0 0 24 24" height="25" viewBox="0 0 24 24" width="25" xmlns="http://www.w3.org/2000/svg">
                                <path d="m15.997 3.985h2.191v-3.816c-.378-.052-1.678-.169-3.192-.169-3.159 0-5.323 1.987-5.323 5.639v3.361h-3.486v4.266h3.486v10.734h4.274v-10.733h3.345l.531-4.266h-3.877v-2.939c.001-1.233.333-2.077 2.051-2.077z"/>
                            </svg>
                            &nbsp; Sign in with facebook</a>
                        <a href="{!! url('/auth/google') !!}" class="gmail-btn btn">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25" height="25" viewBox="0 0 604.35 604.35" style="enable-background:new 0 0 604.35 604.35;"xml:space="preserve">
                                <g id="google-plus">
                                    <path d="M516.375,255v-76.5h-51V255h-76.5v51h76.5v76.5h51V306h76.5v-51H516.375z M320.025,341.7l-28.051-20.4
                                        c-10.2-7.649-20.399-17.85-20.399-35.7s12.75-33.15,25.5-40.8c33.15-25.5,66.3-53.55,66.3-109.65c0-53.55-33.15-84.15-51-99.45
                                        h43.35l30.6-35.7h-158.1c-112.2,0-168.3,71.4-168.3,147.9c0,58.65,45.9,122.4,127.5,122.4h20.4c-2.55,7.65-10.2,20.4-10.2,33.15
                                        c0,25.5,10.2,35.7,22.95,51c-35.7,2.55-102,10.2-150.45,40.8c-45.9,28.05-58.65,66.3-58.65,94.35
                                        c0,58.65,53.55,114.75,168.3,114.75c137.7,0,204.001-76.5,204.001-150.449C383.775,400.35,355.725,372.3,320.025,341.7z
                                        M126.225,109.65c0-56.1,33.15-81.6,68.85-81.6c66.3,0,102,89.25,102,140.25c0,66.3-53.55,79.05-73.95,79.05
                                        C159.375,247.35,126.225,168.3,126.225,109.65z M218.024,568.65c-84.15,0-137.7-38.25-137.7-94.351c0-56.1,51-73.95,66.3-81.6
                                        c33.15-10.2,76.5-12.75,84.15-12.75s12.75,0,17.85,0c61.2,43.35,86.7,61.2,86.7,102C335.324,530.4,286.875,568.65,218.024,568.65z
                                        "/>
                                </g>
                            </svg>

                            &nbsp;Sign in with gmail
                        </a>
                    </div>
                </div>
            </div>
            <div class="form-tab-container col-md-7">
                <nav class="nav nav-tabs row">
                    <a href="#login" class="nav-item nav-link col-6 active" data-toggle="tab" id="login-link">Already a member</a>
                    <a href="#logup" class="nav-item nav-link col-6" data-toggle="tab" id="logup-link">I am new here</a>
                </nav>
                <div class="tab-content">
                    <div class="tab-pane active" id="login">
                        <form id="customer-login-form" role="form" method="POST" action="{{ route('customer.login') }}" class="customer-form">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-12">
                                    <div class="row">
                                        <input type="mail" id="login_email" placeholder="E-mail adress" class="input col-12"
                                        name="email" value="{{ old('email') }}" autofocus>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block" style="color:red;">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} col-12">
                                    <div class="row">
                                        <input type="password" name="password" id="password" placeholder="Password" class="input col-12">
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            {{ $errors->first('password') }}
                                        </span>
                                    @endif
                                </div>

                                <div class="remember col-sm-6 col-12 pl-0">
                                    <input type="checkbox" name="remember" id="remember">
                                    <label class="remember-label" for="remember">Remember me</label>
                                </div>
                                <div class="col-sm-6 col-12 forgoten-password pr-0">
                                    <a href="{{ url('/customer/password/reset') }}" >Forget password ?</a>
                                </div>
                                <span class="help-block" style="color:red;">

                                </span>
                                <button type="submit" class="submit-btn col-12 hvr-wobble-vertical" id="login-btn">
                                    Sign in
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="logup">
                        <form id="customer-form" action="{{ url('customer/register') }}" role="form" method="POST" class="customer-form">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-12 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <div class="row" >                       <input type="text" name="name" id="name" placeholder="Name" class="input  col-12" value="{{ old('name') }}">
                                    </div>
                                    @if ($errors->has('name'))
                                        <span class="help-block" style="color: red;">
                                            {{ $errors->first('name') }}
                                        </span>
                                    @endif
                                </div>

                                <div class="col-12 form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                    <div class="row">
                                        <input type="text" name="lastname" id="lastname" placeholder="Last name" class="input col-12" value="{{ old('lastname') }}">
                                    </div>
                                    @if ($errors->has('lastname'))
                                        <span class="help-block" style="color: red;">
                                            {{ $errors->first('lastname') }}
                                        </span>
                                    @endif
                                </div>

                                <div class="col-12 form-group">
                                    <div class="row">
                                        <input type="tel" name="phone_number" id="phone_number" class="input col-12"  required style="padding-bottom: 5px;" value="{{ old('phone_number') }}">

                                        <input type="hidden" name="dial_code" id="dial_code">

                                        <input type="hidden" name="phone_number_customer" id="phone_number_customer">

                                        <input type="hidden" name="country_code" id="country_code" value="" />

                                        <span id="valid-msgPhoneNumber" class="hide"></span>
                                        <span id="error-msgPhoneNumber" class="hide" style="color:red;"></span>

                                    </div>
                                </div>
                                <div class="col-12 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="row">
                                        <input type="mail" name="email" id="email" placeholder="E-mail" class="input col-12" value="{{ old('email') }}">
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block" style="color: red;">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="col-12 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="row">
                                        <input type="password" name="password" id="password" placeholder="Password" class=" col-12">
                                    </div>
                                </div>
                                <div class="col-12 form-group{{ $errors->has('password2') ? ' has-error' : '' }}">
                                    <div class="row">
                                        <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirm password" class="col-12">
                                        <span class="help-block-password col-12" style="color:red;"></span>
                                    </div>
                                    {{-- @if ($errors->has('password_confirmation'))
                                        <span class="help-block" style="color: red;">
                                            {{ $errors->first('password_confirmation') }}
                                        </span>
                                    @endif --}}
                                </div>

                                <input type="hidden" name="customer_region" id="customerRegion" value="" />

                                <input type="hidden" name="customer_city" id="customerCity" value="" />

                                <input type="hidden" name="lat" id="customerLat" value="" />

                                <input type="hidden" name="lng" id="customerLng" value="" />

                                <input type="hidden" name="language" id="customerLanguage" value="" />

                                <p>I agree to the <a href="#">Prevacy policy</a> and <a href="#">terms of service</a></p>

                                {{-- <a href="#" class="submit-btn col-12" id="customerRegisterSubmitLink">Sign up</a> --}}
                                <button type="submit" class="submit-btn col-12 hvr-wobble-vertical" id="customerRegisterSubmitBtn">
                                    Sign up
                                </button>
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                @if (session('warning'))
                                    <div class="alert alert-warning">
                                        {{ session('warning') }}
                                    </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('customer-login-script')
<script>
    const loginForm = document.getElementById('customer-login-form');
    const loginMailInput = document.querySelector('#customer-login-form #login_email');
    const loginPasswordInput = document.querySelector('#customer-login-form #password');
    const loginBtn = document.getElementById('login-btn');

    const registerName = document.querySelector('#logup #name');
    const registerBtn = document.querySelector('#customerRegisterSubmitBtn');
    const registerForm = document.querySelector('#customer-form');
    const logupPassword = document.querySelector('#logup #password');
    const logupPassword2 = document.querySelector('#logup #password_confirmation');

    // console.log(registerName.value);


    //Window load with Active
    window.addEventListener('load', function(){
        if(registerName.value !== ''){
            // console.log('ok');
            document.querySelector('#login-link').classList.remove('active');
            document.querySelector('#login').classList.remove('active');

            document.querySelector('#logup').classList.add('active');
            document.querySelector('#logup-link').classList.add('active');
        }
    });

    if (logupPassword.value == '' || logupPassword.value == null || logupPassword2.value == null || logupPassword2.value == '') {
        registerBtn.setAttribute('disabled', true);
        registerBtn.style.opacity = "0.5";
    }

    let password1 = '';
    let passwordConfirm = '';
    let passwordMsg = ''
    logupPassword.addEventListener('input', function(e){
        password1 = logupPassword.value
    });

    logupPassword2.addEventListener('input', function(e){
        passwordConfirm = logupPassword2.value
        // console.log(passwordConfirm.length)
        if (logupPassword.value == logupPassword2.value) {
            if (passwordConfirm.length < 8) {
                document.querySelector('#logup .help-block-password').innerText = 'Your password must be at least 8 characters long.';
            }
            else{
                document.querySelector('#logup .help-block-password').innerText = '✓ valid';
                document.querySelector('#logup .help-block-password').style.color = "#15b025";
                registerBtn.removeAttribute('disabled');
                registerBtn.style.opacity = "1";
            }
        }
        else {
            document.querySelector('#logup .help-block-password').innerText = 'Enter identical password';
        }
    });





    // Register button add Event listener
    registerBtn.addEventListener('click', function(e) {
        e.preventDefault();
        let action = 'register';
        this.setAttribute('disabled', true);
        this.style.opacity = '0.5';
        registerForm.submit();

    });

    //Listen to the login button
    loginBtn.addEventListener('click', function(e){
        e.preventDefault();
        this.setAttribute('disabled', true);
        this.style.opacity = '0.5';
        // console.log('ok');        //Get login form value
        let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        let url = "{{ route('customer.login') }}";
        let _data = {
            "email": loginMailInput.value,
            "password": loginPasswordInput.value
        };

        // Ajax with Fetch
        fetch(
            url,
            {
                headers: {
                    "Content-type": "application/json;charset=UTF-8",
                    "Accept": "application/json, text-plain, */*",
                    "X-Requested-with": "XMLHttpRequest",
                    "X-CSRF-TOKEN": token
                },
                method: "post",
                body:  JSON.stringify(_data)
            })
            .then(res => res.text())
            .then(data => {
                if (data == 'error') {
                    document.querySelector('#customer-login-form .help-block').innerText = 'An error occurred, please check your information and try again.';
                    this.removeAttribute('disabled');
                    this.style.opacity = '1';
                }
                else{
                    this.style.opacity = '1';
                    window.location.href = "{{ route('customerHome') }}";
                }
            })
            .catch(err => console.log('Request Failed', err));
    });

    //Listen to the register button
    // registerBtn.addEventListener('click', function(e){
    //     e.preventDefault();

    // });
</script>
@endpush

@push('intel-input-script')
    <!-- Pour le numéro de téléphone -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script> -->
    <script src="{{ asset('js/intlTelInput.js') }}"></script>
    <script src="{{ asset('js/utils.js') }}"></script>
    <script>

        var input = document.querySelector("#phone_number"),
        errorMsg = document.querySelector("#error-msg"),
        validMsg = document.querySelector("#valid-msg"),
        dialCode = document.querySelector('#dial_code'),
        countryCode = document.querySelector('#country_code');

        // Error messages based on the code returned from getValidationError
        var errorMap = [ "Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

        // Initialise plugin
        var intl = window.intlTelInput(input, {
            initialCountry: "ca",
            utilsScript: "js/utils.js",
            geoIpLookup: function(success, failure) {
                $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                    countryCode.value = (resp && resp.country) ? resp.country : "";
                    success(countryCode);
                });
            },

        });

        var reset = function() {
            input.classList.remove("error");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };

        // Validate on blur event
        input.addEventListener('blur', function() {
            reset();
            if(input.value.trim()){
                if(intl.isValidNumber()){
                    validMsg.classList.remove("hide");
                    validMsg.innerHTML = "✓ Valid";
                }else{
                    input.classList.add("error");
                    var errorCode = intl.getValidationError();
                    errorMsg.innerHTML = errorMap[errorCode];
                    errorMsg.classList.remove("hide");
                }
            }
        });

        // Reset on keyup/change event
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);
    </script>


    {{-- <script>
        // var CSRFToken = "{{ csrf_token() }}";
        // var inputPhoneShop = document.querySelector("#phone_number");
        // var inputPhoneOwner = document.querySelector("#phone_owners");
        // var errorMsg = document.querySelector("#error-msgPhoneNumber"),
        //     validMsg = document.querySelector("#valid-msgPhoneNumber");
        // // here, the index maps to the error code returned from getValidationError - see readme
        // var errorMap = ["Numéro Invalide !", "Code de pays invalide !", "Numéro de téléphone Trop Court",
        //     "Numéro de téléphone Trop Long", "Numéro Invalide"
        // ];
        // var iti = window.intlTelInput(inputPhoneShop, {
        //     // allowDropdown: false,
        //     // autoHideDialCode: false,
        //     // autoPlaceholder: "off",
        //     dropdownContainer: document.body,
        //     // excludeCountries: ["us"],
        //     // formatOnDisplay: false,
        //     initialCountry: "auto",
        //     separateDialCode: true,
        //     geoIpLookup: function(success, failure) {
        //         $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //             var countryCode = (resp && resp.country) ? resp.country : "ci";
        //             success(countryCode);
        //         });
        //     },
        //     // hiddenInput: "full_number",
        //     // localizedCountries: { 'de': 'Deutschland' },
        //     //nationalMode: true,
        //     // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        //     placeholderNumberType: "MOBILE",
        //     // preferredCountries: ['cn', 'jp'],
        //     utilsScript: "{{ asset('js/utils.js?1562189064761') }}",
        // });
        // var reset = function() {
        //     inputPhoneShop.classList.remove("error");
        //     errorMsg.innerHTML = "";
        //     errorMsg.classList.add("hide");
        //     validMsg.classList.add("hide");
        // };

        // // on blur: validate
        // inputPhoneShop.addEventListener('blur', function() {
        //     reset();
        //     if (inputPhoneShop.value.trim()) {
        //         if (iti.isValidNumber()) {
        //             $("#dial_code").val(iti.getSelectedCountryData().dialCode);
        //             // $("#phone_number").val(iti.);
        //             $("#country_code").val(iti.getSelectedCountryData().iso2.toUpperCase());
        //             //console.log(iti.s.phone);
        //             validMsg.classList.remove("hide");
        //         } else {
        //             inputPhoneShop.classList.add("error");
        //             var errorCode = iti.getValidationError();
        //             errorMsg.innerHTML = errorMap[errorCode];
        //             errorMsg.classList.remove("hide");
        //         }
        //     }
        // });

        // // on keyup / change flag: reset
        // inputPhoneShop.addEventListener('change', reset);
        // inputPhoneShop.addEventListener('keyup', reset);
        // var errorMsgPhoneOwner = document.querySelector("#error-msgPhoneOwner"),
        //     validMsgPhoneOwner = document.querySelector("#valid-msgPhoneOwner");
        // // here, the index maps to the error code returned from getValidationError - see readme
        // var errorMapPhoneOwner = ["Numéro Invalide !", "Code de pays invalide !", "Numéro de téléphone Trop Court",
        //     "Numéro de téléphone Trop Long", "Numéro Invalide"
        // ];
        // var itiPhoneOwner = window.intlTelInput(inputPhoneOwner, {
        //     // allowDropdown: false,
        //     // autoHideDialCode: false,
        //     // autoPlaceholder: "off",
        //     dropdownContainer: document.body,
        //     // excludeCountries: ["us"],
        //     // formatOnDisplay: false,
        //     initialCountry: "auto",
        //     separateDialCode: true,
        //     geoIpLookup: function(success, failure) {
        //         $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //             var countryCodePhoneOwner = (resp && resp.country) ? resp.country : "us";
        //             success(countryCodePhoneOwner);
        //         });
        //     },
        //     // hiddenInput: "full_number",
        //     // localizedCountries: { 'de': 'Deutschland' },
        //     //nationalMode: true,
        //     // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        //     placeholderNumberType: "MOBILE",
        //     // preferredCountries: ['cn', 'jp'],
        //     utilsScript: "{{ asset('js/utils.js?1562189064761') }}",
        // });
        // var resetPhoneOwner = function() {
        //     inputPhoneOwner.classList.remove("error");
        //     errorMsgPhoneOwner.innerHTML = "";
        //     errorMsgPhoneOwner.classList.add("hide");
        //     validMsgPhoneOwner.classList.add("hide");
        // };
        // // on blur: validate
        // inputPhoneOwner.addEventListener('blur', function() {
        //     resetPhoneOwner();
        //     if (inputPhoneOwner.value.trim()) {
        //         if (itiPhoneOwner.isValidNumber()) {
        //             $("#dial_codePhoneOwner").val(itiPhoneOwner.getSelectedCountryData().dialCode);
        //             // $("#phone_number").val(iti.);
        //             $("#country_codePhoneOwner").val(itiPhoneOwner.getSelectedCountryData().iso2.toUpperCase());
        //             //console.log(iti.s.phone);
        //             validMsgPhoneOwner.classList.remove("hide");
        //         } else {
        //             inputPhoneOwner.classList.add("error");
        //             var errorCodePhoneOwner = itiPhoneOwner.getValidationError();
        //             errorMsgPhoneOwner.innerHTML = errorMapPhoneOwner[errorCodePhoneOwner];
        //             errorMsgPhoneOwner.classList.remove("hide");
        //         }
        //     }
        // });
        // // on keyup / change flag: reset
        // inputPhoneOwner.addEventListener('change', reset);
        // inputPhoneOwner.addEventListener('keyup', reset);

    </script> --}}
@endpush
