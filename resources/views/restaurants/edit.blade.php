@extends('layouts.app')

@section('css')
    <!--link href="{{asset('css/jquery.filer.css')}}" type="text/css" rel="stylesheet" /-->
    <!--link href="{{asset('css/themes/jquery.filer-dragdropbox-theme.css')}}" type="text/css" rel="stylesheet" /-->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"/>
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
    <style>
        #map {
            width: auto;
            height: 400px;
        }
    </style>
@endsection

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! Auth::check() ? route('restaurants.index') : url('/backOffice/restaurants/'.Auth::guard('restaurant')->user()->uuid) !!}">Restaurants</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.873 477.873" style="enable-background:new 0 0 477.873 477.873; width: 20px;" xml:space="preserve">
                                    <path d="M392.533,238.937c-9.426,0-17.067,7.641-17.067,17.067V426.67c0,9.426-7.641,17.067-17.067,17.067H51.2 c-9.426,0-17.067-7.641-17.067-17.067V85.337c0-9.426,7.641-17.067,17.067-17.067H256c9.426,0,17.067-7.641,17.067-17.067 S265.426,34.137,256,34.137H51.2C22.923,34.137,0,57.06,0,85.337V426.67c0,28.277,22.923,51.2,51.2,51.2h307.2 c28.277,0,51.2-22.923,51.2-51.2V256.003C409.6,246.578,401.959,238.937,392.533,238.937z"/>
                                    <path d="M458.742,19.142c-12.254-12.256-28.875-19.14-46.206-19.138c-17.341-0.05-33.979,6.846-46.199,19.149L141.534,243.937 c-1.865,1.879-3.272,4.163-4.113,6.673l-34.133,102.4c-2.979,8.943,1.856,18.607,10.799,21.585 c1.735,0.578,3.552,0.873,5.38,0.875c1.832-0.003,3.653-0.297,5.393-0.87l102.4-34.133c2.515-0.84,4.8-2.254,6.673-4.13 l224.802-224.802C484.25,86.023,484.253,44.657,458.742,19.142z M434.603,87.419L212.736,309.286l-66.287,22.135l22.067-66.202 L390.468,43.353c12.202-12.178,31.967-12.158,44.145,0.044c5.817,5.829,9.095,13.72,9.12,21.955 C443.754,73.631,440.467,81.575,434.603,87.419z"/>
                                </svg>
                              <strong>Edit Restaurants</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($restaurants, ['route' => ['restaurants.update', $restaurants->id], 'method' => 'patch', 'files' => true]) !!}

                              @include('restaurants.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection

@push('scripts')
    <script>
        var loadFile = function (event) {
            var reader = new FileReader();
            reader.onload = function () {
                var output = document.getElementById('output_cover');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        }

        var loadFile1 = function (event) {
            var reader = new FileReader();
            reader.onload = function () {
                var output = document.getElementById('output_logo');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        }

        var startlat = {{ $restaurants->lat ?? $userCoordinates["latitude"] ?? 5.225936 }};
        var startlon = {{ $restaurants->lon ?? $userCoordinates["longitude"] ?? -3.753666 }};
        
        document.getElementById('lat').value = startlat;
        document.getElementById('lng').value = startlon;

        var options = {
            center: [startlat, startlon],
            zoom: 12
        }
        
        var map = L.map('map', options);
        var nzoom = 12;
            
        var marker = L.marker(
            [startlat, startlon], {
            draggable:'true',
            title: "Position du restaurant",
            alt: "Position du restaurant"
        }).addTo(map).on('dragend', function () {
            var lat = marker.getLatLng().lat.toFixed(8);
            var lon = marker.getLatLng().lng.toFixed(8);
            var czoom = map.getZoom();

            if (czoom < 18) {
                nzoom = czoom + 2;
            }
            if (nzoom > 18) {
                nzoom = 18;
            }
            if (czoom != 18) {
                map.setView([lat, lon], nzoom);
            } else {
                map.setView([lat, lon]);
            }
            document.getElementById('lat').value = lat;
            document.getElementById('lng').value = lon;

            marker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();
        });

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OSM'}).addTo(map);

        function chooseAddr(lat1, lng1) {
            marker.closePopup();
            map.setView([lat1, lng1], 18);
            marker.setLatLng([lat1, lng1]);
            lat = lat1.toFixed(8);
            lon = lng1.toFixed(8);
            document.getElementById('lat').value = lat;
            document.getElementById('lng').value = lon;
            document.getElementById('lng').value = lon;
            // document.getElementById('results').css.display = 'none';
            
            marker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();

            $.get('https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=' + lat + '&lon=' + lon, function (data) {
                document.getElementById('addr').value = data.display_name;
            });
        }

        function addr_search(e) {
            e.preventDefault()
            var country_code = '{{ $userCoordinates["country_code"] ?? "CI" }}';
            var inp = document.getElementById("addr");
            var xmlhttp = new XMLHttpRequest();
            var url = "https://nominatim.openstreetmap.org/search?format=json&countrycodes="+country_code+"&limit=3&q=" + inp.value;
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    var myArr = JSON.parse(this.responseText);
                    myFunction(myArr);
                }
            };
            xmlhttp.open("GET", url, true);
            xmlhttp.send();
        }

        function myFunction(arr) {
            var out = "<br />";
            var i;

            if (arr.length > 0) {
                for (i = 0; i < arr.length; i++) {
                    out += "<div class='address' title='Show Location and Coordinates' style='cursor:pointer' value='" + arr[i].display_name + "' onclick='chooseAddr(" + arr[i].lat + ", " + arr[i].lon + ");return false;'>" + arr[i].display_name + "</div><br/>";
                }
                    document.getElementById('results').innerHTML = out;
            } else {
                document.getElementById('results').innerHTML = "Sorry, no results...";
            }
        }
    </script>
@endpush
