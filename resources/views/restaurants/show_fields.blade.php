{{--<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $restaurants->name }}</p>
</div>

<!-- Surname Field -->
<div class="form-group">
    {!! Form::label('surname', 'Surname:') !!}
    <p>{{ $restaurants->surname }}</p>
</div>

<!-- Cover Field -->
<div class="form-group">
    {!! Form::label('cover', 'Cover:') !!}
    <p>{{ $restaurants->cover }}</p>
</div>

<!-- Logo Field -->
<div class="form-group">
    {!! Form::label('logo', 'Logo:') !!}
    <p>{{ $restaurants->logo }}</p>
</div>

<!-- Tel1 Field -->
<div class="form-group">
    {!! Form::label('tel1', 'Tel1:') !!}
    <p>{{ $restaurants->tel1 }}</p>
</div>

<!-- Tel2 Field -->
<div class="form-group">
    {!! Form::label('tel2', 'Tel2:') !!}
    <p>{{ $restaurants->tel2 }}</p>
</div>

<!-- Tel3 Field -->
<div class="form-group">
    {!! Form::label('tel3', 'Tel3:') !!}
    <p>{{ $restaurants->tel3 }}</p>
</div>

<!-- Lng Field -->
<div class="form-group">
    {!! Form::label('lng', 'Lng:') !!}
    <p>{{ $restaurants->lng }}</p>
</div>

<!-- Lat Field -->
<div class="form-group">
    {!! Form::label('lat', 'Lat:') !!}
    <p>{{ $restaurants->lat }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $restaurants->email }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $restaurants->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $restaurants->updated_at }}</p>
</div>--}}

<div class="bg-img">
    <!-- <img  class="img" src="{{ asset('img/default-restaurant.png')}}" alt="{{ $restaurants->title }}" width="110"> -->
    <img  class="img" src="{{ $restaurants->logo ?? asset('img/default-restaurant.png')}}" alt="{{ $restaurants->title }}" width="110">
    <h1>{{ $restaurants->name }}</h1>
</div>
<div class="mt-3 py-2">
    <div class="info-container">
        <div class="info-content row">
            <div class="col-12 title">
                <h2>Location</h2>
            </div>
            <div class="col-12 mt-3">
                <div class="row">
                    <div class="col-12 info-card">
                        <div class="form-group">
                            {!! Form::label('country', 'Country :', ['class' => 'title']) !!}
                            <p class="default">{{ $restaurants->restaurant_region_name }}</p>
                        </div>
                    </div>
                    <div class="col-12 info-card">
                        <div class="form-group">
                            {!! Form::label('City', 'City :', ['class' => 'title']) !!}
                            <p class="default">{{ $restaurants->restaurant_city }}</p>
                        </div>
                    </div>
                    <div class="col-12 info-card">
                        <div class="form-group">
                            {!! Form::label('restaurant_adress_name', 'Address:', ['class' => 'title']) !!}
                            <p class="default">{{ $restaurants->restaurant_adress_name }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 info-content">
            <div class="col-lg-6 lng-card">
                <div class="form-group">
                    {!! Form::label('Latitude', 'Latitude', ['class' => 'title']) !!}
                    <p class="default">{{ $restaurants->lat }}</p>
                </div>
            </div>
            <div class="col-lg-6 lat-card">
                <div class="form-group">
                    {!! Form::label('Longitude', 'Longitude:', ['class' => 'title']) !!}
                    <p class="default">{{ $restaurants->lng }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="px-4 info-container">
        <div class="info-content row">
            <div class="col-12 title">
                <h2>Manager and Restaurant info</h2>
            </div>
            <div class="col-12 mt-3">
                <div class="row">
                    <div class="col-12 info-card">
                        <div class="form-group">
                            {!! Form::label('responsable_name', 'Manager name', ['class' => 'title']) !!}
                            <p class="default">{{ $restaurants->responsable_name }}</p>
                        </div>
                    </div>
                    {{-- <div class="col-12 info-card">
                        <div class="form-group">
                            {!! Form::label('responsable_name', 'Manager name', ['class' => 'title']) !!}
                            <p class="default">{{ $restaurants->responsable_name }}</p>
                        </div>
                    </div> --}}
                    <div class="col-12 info-card">
                        <div class="form-group">
                            {!! Form::label('email', 'Email:', ['class' => 'title']) !!}
                            <p class="default">{{ $restaurants->email }}</p>
                        </div>
                    </div>
                    <div class="col-12 info-card">
                        <div class="form-group">
                            {!! Form::label('restaurant_contact', 'Restaurant contact:', ['class' => 'title']) !!}
                            <p class="default">{{ $restaurants->restaurant_contact }}</p>
                        </div>
                    </div>
                    <div class="col-12 info-card">
                        <div class="form-group">
                            {!! Form::label('restaurant_registration_number', 'Restaurant registration number:', ['class' => 'title']) !!}
                            <p class="default">{{ $restaurants->restaurant_registration_number }}</p>
                        </div>
                    </div>
                    <div class="col-12 info-card">
                        <div class="form-group">
                            {!! Form::label('restaurant_paper_id_type', 'Restaurant paper id type:', ['class' => 'title']) !!}
                            <p class="default">{{$restaurants->restaurant_paper_id_type}}</p>
                        </div>
                    </div>
                    <div class="col-12 info-card">
                        <div class="form-group">
                            {!! Form::label('restaurant_paper_id_number', 'Restaurant paper id number:', ['class' => 'title']) !!}
                            <p class="default">{{$restaurants->restaurant_paper_id_number}}</p>
                        </div>
                    </div>
                    <div class="col-12 info-card">
                        <div class="form-group">
                            {!! Form::label('restaurant_contact', 'Restaurant status:', ['class' => 'title']) !!}

                            <p class="default">{!! in_array($restaurants->is_active, [1, true]) ? '<span class="badge badge-success px-3 py-2">Active</span>' : '<span class="badge badge-danger px-3 py-2">Inactive</span>' !!}</p>
                        </div>
                    </div>
                    @if($restaurants->is_active)
                        <div class="col-12 info-card">
                            <div class="form-group">
                                {!! Form::label('activation_date', 'Activation date :', ['class' => 'title']) !!}
                                <p class="default">{{ $restaurants->activation_date }}</p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
