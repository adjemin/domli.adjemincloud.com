<!-- Restaurant Id Field -->
<div class="form-group col-sm-6">
    <input type="hidden" name="restaurant_id" value='{{$restaurant->id ?? $restaurantCategories->restaurant->id}}'>
</div>
<div class="row">
    <div class="form-group col-sm-12">
        <select name="category_id" id="category_id" class="form-control">
            @php
                $categories = $categories ?? \App\Models\Category::all();
            @endphp
            @foreach($categories as $categorie)
                <option value="{{$categorie->id}}" {{ old("category_id") == $categorie->id ? "selected" : "" }}><img height="40" scr="{{$categorie->photo ?? 'https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/cef389b486cb4827e6ba007f26ebddab.svg' }}" class="img-fluid rounded-circle" alt="{{$categorie->title}}"> {{$categorie->title}}</option>
            @endforeach
        </select>
    </div>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    <div class="row">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{!! \Auth::check() ? route('admin-restaurant-categorie-index', ['restaurant_id' =>         $restaurant->uuid ?? $restaurantCategories->restaurant->uuid]) : route('backoffice-restaurant-categorie-index', ['restaurant_id' => $restaurant->uuid ?? $restaurantCategories->restaurant->uuid]) !!}" class="btn btn-danger" style="margin-left: 10px">Cancel</a>
    </div>
</div>
