@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{!! !\Auth::check() ? url('/backOffice/restaurants/'.$restaurant->uuid) : url('/admin/restaurants/'.$restaurant->uuid) !!}">Restaurant</a></li>
        <li class="breadcrumb-item">{{$restaurant->name}} Menu's</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header" style="display: flex; justify-content: space-between">
                             {{-- <i class="fa fa-align-justify"></i> --}}
                             The list of {{$restaurant->name}} menu's
                             <a class="pull-right" href="{!! \Auth::check() ? route('admin-restaurant-menu-create', ['restaurant_id' => $restaurant->uuid]) : route('backoffice-restaurant-menu-create', ['restaurant_id' => $restaurant->uuid]) !!}">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve" height="28" height="28">
                                    <circle style="fill:#E8C52A;" cx="25" cy="25" r="25"/>
                                    <line style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="25" y1="13" x2="25" y2="38"/>
                                    <line style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="37.5" y1="25" x2="12.5" y2="25"/>
                                </svg>
                            </a>
                         </div>
                         <div class="card-body">
                             @include('restaurants.menu_table')
                              <div class="pull-right mr-3">

                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

