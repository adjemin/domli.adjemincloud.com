@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{{ \Auth::check() ? '/admin/restaurant/'.$restaurant->uuid.'/menu' : '/restaurant/'.$restaurant->uuid.'/menu' }}">Restaurant Menu</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                {{-- <i class="fa fa-plus-square-o fa-lg"></i> --}}
                                <strong>Create Restaurant Menu</strong>
                            </div>
                            <div class="card-body">
                                @php
                                    $prefix = \Auth::check() ? 'admin': '';
                                    $route = "$prefix/restaurant/$restaurant->uuid/menu/create";
                                @endphp
                                {!! Form::open(['url' => "$route"]) !!}

                                   @include('restaurants.menu_fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
