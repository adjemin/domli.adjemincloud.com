<!DOCTYPE html>
<html>
	<head>
		<title>Domli</title>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel Multi Auth Guard') }}</title>

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([ 'csrfToken' => csrf_token(), ]); ?>
        </script>

		<!--Font-->
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">

        {{-- Bootstrap and local css file --}}
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

        <link rel="stylesheet" href="{{ asset('css/Hover/css/hover-min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/intlTelInput.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">

        <!--Pop up package-->
        <link type="text/css" rel="stylesheet" href="{{ asset('jQuery-popModal/popModal.css') }}">

		<!--FontAwesome Scrypt-->
		<script src="https://kit.fontawesome.com/f0abbeb7fb.js" crossorigin="anonymous"></script>

	</head>
	<body>
		<div class="container-fluid">
