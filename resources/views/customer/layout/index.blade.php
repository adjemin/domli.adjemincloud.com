@include('layouts.header')

{{-- Page body content --}}
@yield('content')

@include('layouts.footer')
