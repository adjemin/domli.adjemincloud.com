<footer class="col-12">
    <div class="row">
        <div class="bottom-card col-12">
            <div class="container">
                <div class="row">
					{{-- <div class="col-lg-3 col-md-6 col-6 description">
                        <h1>DOMLI</h1>{{-- <img src="img/LOGO-DOMLI.png" class="my-2" height="90" alt="logo">
                        <ul>
                            <li><a href="#">About</a></li>
                        </ul>
                    </div> --}}
                    <div class="col-lg-3 col-md-6 col-6 description">
                        <h1>{{__('header.home.footertitle1')}}</h1>
                        <ul>
                            <li><a href="#">Employement</a></li>
                            <li><a href="#">Partner businesses</a></li>
                            <li><a href="#">Courier</a></li>
                            <li><a href="#">Domli Business</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6 description">
                        <h1>{{__('header.home.footertitle2')}}</h1>
                        <ul>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Contact us</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6 description">
                        <h1>{{__('header.home.footertitle3')}}</h1>
                        <ul>
                            <li><a href="#">Terms & Conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Cookies Policy</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6 description">
                        <h1>{{__('header.home.footertitle4')}}</h1>
                        <ul>
                            <li><a href="#">FaceBook</a></li>
                            <li><a href="#">Instagram</a></li>
                            <li><a href="#">Twitter</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 signature-container">
        <div class="container">
            <div class="row signature-content">
                <div class="signature col-6">
                    © Copyright Domli | <a href="https://www.adjemincloud.com" target="_blank" >AdjeminCloud</a>
                </div>
                <div class="col-6">
                    <select name="categorie" id="categorie" class="">
                        <option value="" data-content="">English</option>
                        <option value="1" data-content="">French</option>
                        <option value="2" data-content="">Mandarin</option>
                        <option value="3" data-content="">English</option>
                    </select>
                </div>
            </div>

        </div>
    </div>
    </div>
</footer>
<!-- Scripts -->
<script src="{{ asset('/js/app.js') }}"></script>


<!--jQuery pop up modal-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="{{ asset('jQuery-popModal/popModal.js') }}"></script>

<!--Bootstrap tab-->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.0/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<!-- Pour le numéro de téléphone -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
<script src="{{ asset('js/intlTelInput.js') }}"></script>
<script src="{{ asset('js/owl.carousel.js') }}"></script>
<script>

    var CSRFToken = "{{ csrf_token() }}";
    var inputPhoneShop = document.querySelector("#phone_number");
    var inputPhoneOwner = document.querySelector("#phone_owners");
    var errorMsg = document.querySelector("#error-msgPhoneNumber"),
        validMsg = document.querySelector("#valid-msgPhoneNumber");
    // here, the index maps to the error code returned from getValidationError - see readme
    var errorMap = ["Numéro Invalide !", "Code de pays invalide !", "Numéro de téléphone Trop Court",
        "Numéro de téléphone Trop Long", "Numéro Invalide"
    ];
    var iti = window.intlTelInput(inputPhoneShop, {
        // allowDropdown: false,
        // autoHideDialCode: false,
        // autoPlaceholder: "off",
        dropdownContainer: document.body,
        // excludeCountries: ["us"],
        // formatOnDisplay: false,
        initialCountry: "auto",
        separateDialCode: true,
        geoIpLookup: function(success, failure) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "us";
                success(countryCode);
            });
        },
        // hiddenInput: "full_number",
        // localizedCountries: { 'de': 'Deutschland' },
        //nationalMode: true,
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        placeholderNumberType: "MOBILE",
        // preferredCountries: ['cn', 'jp'],
        utilsScript: "{{ asset('js/utils.js?1562189064761') }}",
    });
    var reset = function() {
        inputPhoneShop.classList.remove("error");
        errorMsg.innerHTML = "";
        errorMsg.classList.add("hide");
        validMsg.classList.add("hide");
    };
    // on blur: validate
    inputPhoneShop.addEventListener('blur', function() {
        reset();
        if (inputPhoneShop.value.trim()) {
            if (iti.isValidNumber()) {
                $("#dial_code").val(iti.getSelectedCountryData().dialCode);
                // $("#phone_number").val(iti.);
                $("#country_code").val(iti.getSelectedCountryData().iso2.toUpperCase());
                //console.log(iti.s.phone);
                validMsg.classList.remove("hide");
            } else {
                inputPhoneShop.classList.add("error");
                var errorCode = iti.getValidationError();
                errorMsg.innerHTML = errorMap[errorCode];
                errorMsg.classList.remove("hide");
            }
        }
    });
    // on keyup / change flag: reset
    inputPhoneShop.addEventListener('change', reset);
    inputPhoneShop.addEventListener('keyup', reset);
    var errorMsgPhoneOwner = document.querySelector("#error-msgPhoneOwner"),
        validMsgPhoneOwner = document.querySelector("#valid-msgPhoneOwner");
    // here, the index maps to the error code returned from getValidationError - see readme
    var errorMapPhoneOwner = ["Numéro Invalide !", "Code de pays invalide !", "Numéro de téléphone Trop Court",
        "Numéro de téléphone Trop Long", "Numéro Invalide"
    ];
    var itiPhoneOwner = window.intlTelInput(inputPhoneOwner, {
        // allowDropdown: false,
        // autoHideDialCode: false,
        // autoPlaceholder: "off",
        dropdownContainer: document.body,
        // excludeCountries: ["us"],
        // formatOnDisplay: false,
        initialCountry: "auto",
        separateDialCode: true,
        geoIpLookup: function(success, failure) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCodePhoneOwner = (resp && resp.country) ? resp.country : "us";
                success(countryCodePhoneOwner);
            });
        },
        // hiddenInput: "full_number",
        // localizedCountries: { 'de': 'Deutschland' },
        //nationalMode: true,
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        placeholderNumberType: "MOBILE",
        // preferredCountries: ['cn', 'jp'],
        utilsScript: "{{ asset('js/utils.js?1562189064761') }}",
    });
    var resetPhoneOwner = function() {
        inputPhoneOwner.classList.remove("error");
        errorMsgPhoneOwner.innerHTML = "";
        errorMsgPhoneOwner.classList.add("hide");
        validMsgPhoneOwner.classList.add("hide");
    };
    // on blur: validate
    inputPhoneOwner.addEventListener('blur', function() {
        resetPhoneOwner();
        if (inputPhoneOwner.value.trim()) {
            if (itiPhoneOwner.isValidNumber()) {
                $("#dial_codePhoneOwner").val(itiPhoneOwner.getSelectedCountryData().dialCode);
                // $("#phone_number").val(iti.);
                $("#country_codePhoneOwner").val(itiPhoneOwner.getSelectedCountryData().iso2.toUpperCase());
                //console.log(iti.s.phone);
                validMsgPhoneOwner.classList.remove("hide");
            } else {
                inputPhoneOwner.classList.add("error");
                var errorCodePhoneOwner = itiPhoneOwner.getValidationError();
                errorMsgPhoneOwner.innerHTML = errorMapPhoneOwner[errorCodePhoneOwner];
                errorMsgPhoneOwner.classList.remove("hide");
            }
        }
    });
    // on keyup / change flag: reset
    inputPhoneOwner.addEventListener('change', reset);
    inputPhoneOwner.addEventListener('keyup', reset);

</script>

<!--Add my script -->
<script src="{{ asset('js/script.js') }}"></script>
</body>
</html>
