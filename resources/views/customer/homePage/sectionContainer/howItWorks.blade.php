<section class="howItWorks-section">
	<div class="container">
		<div class="row">
			<div class="section-title col-sm-12">
				<h3>{{__('header.home.hith3')}}</h3>
			</div>
			<div class="col-sm-4 howItWorks-img hvr-float">
				<div class="howItWorks-img-content rounded-circle">
					<img src="{{ asset('img/hamburger.svg') }}" alt="...">
				</div>
				<p>{{__('header.home.hit1')}}</p>
			</div>
			<div class="col-sm-4 howItWorks-img hvr-float">
				<div class="howItWorks-img-content rounded-circle">
					<img src="{{ asset('img/payment-method.svg') }}" alt="...">
				</div>
				<p>{{__('header.home.hit2')}}</p>
			</div>
			<div class="col-sm-4 howItWorks-img hvr-float">
				<div class="howItWorks-img-content rounded-circle">
					<img src="{{ asset('img/food-delivery.svg') }}" alt="...">
				</div>
				<p>{{__('header.home.hit3')}}</p>
			</div>
		</div>
	</div>
</section>
