<section class="suggestions-section row">
	<div class="container">
		<div class="row">
			<div class="section-title col-12">
                <h3>{{('header.our_suggestion')}}</h3>
                {{-- /restaurants?categories=all --}}
                <a href="{{ route('searchRestaurantsCategorie') }}">{{ __('header.see_more') }}</a>
			</div>
            {{--@for ($i = 7; $i <= 12; $i++)--}}
            @foreach($food_categorires as $food)
                <div class="card item col-lg-4 col-md-6">
                    <div class="card-container">
                        <div class="img-card">
                            <a href="{{ url('/restaurants?categories='.$food->slug) }}">
                                <img src="{{ asset('img/'.$food->id.'.jpg') }}" class="attachment-post-thumb size-post-thumb wp-post-image" alt="...">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="card-text">
                                <h5>{{$food->title}}</h5>
                                <a href="{{ url('/restaurants?categories='.$food->slug) }}">
                                    <p>
                                        {{$food->description}}
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
		</div>
	</div>
</section>
