@if(!(Auth::check() || Auth::guard('deliverer')->check() || Auth::guard('customer')->check()))
    <section class="about-section">
        <div class="container">
            <div class="row">

                    @php
                        $titleDefault = [
                            'customer', 
                            'delivery', 
                            'restaurant'
                        ];


                        $titles = [
                            'header.home.aboutSectiontitle1', 
                            'header.home.aboutSectiontitle2', 
                            'header.home.aboutSectiontitle3'
                        ];

                        $descriptions = [
                            'header.home.aboutSectiontext1',
                            'header.home.aboutSectiontext2', 
                            'header.home.aboutSectiontext3'
                        ];

                        $i = 0;
                    @endphp
                    @foreach ($titles as $title)
                        <div class="card item col-lg-4 col-md-6">
                            <div class="card-container" style="background-image: url('{{ asset('img/'.strtolower($titleDefault[$i]).'.jpg') }}');background-size:cover;background-position: center;">
                                <div class="card-body">
                                    <div class="card-text">
                                        <h5> {{ __($title) }} </h5>
                                        <p>
                                            {{__($descriptions[$i])}}
                                        </p>
                                        {{-- $title=='Restaurant'?'/home/restaurant':'#' --}}
                                        <a href="
                                        <?php if($titleDefault[$i] == 'restaurant') { echo'/login/restaurant';} elseif($titleDefault[$i] == 'delivery') { echo '/delivery/home'; }else { echo'/customer/login'; } ?>
                                        " class="action-btn">{{__('header.home.aboutSectiontextbe')}} {{  strtolower(__($title))  }} {{__('header.home.aboutSectiontextan')}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @php
                            $i++;
                        @endphp
                    @endforeach
            </div>
        </div>
    </section>
@endif

