@extends('layouts.index')
{{-- @extends('frontOffice.pages.restaurantsLayouts.index') --}}

@section('content')
    @include('customer.pageParts.pagesHeader')

    <div class="delivery-art">
        <section>
            <div class="container">
                <h2 class="mb-5">
                   {{ __('delivery.how_to') }}
                </h2>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                        <div class="step-delivery">
                            <img src="{{ asset('img/DELIVERY-DOMLI.png') }}"  alt="..." >
                            <h3>{{ __('delivery.step_one_question') }}</h3>
                            <p><strong>{{__('delivery.step_one.title')}}</strong></p>

                            <ul style="padding:0 20px ">
                                <li>{{ __('delivery.step_one.step_one') }}</li>
                                <li>{{ __('delivery.step_one.step_two') }}</li>
                                <li>{{ __('delivery.step_one.step_three') }}</li>
                            </ul>

                            {{-- <button class="js-toggle border-none bg-white">Click it!!</button> --}}
                                {{-- <div class="js-target">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quasi officiis eaque veritatis maxime?</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quasi officiis eaque veritatis maxime?</p>
                                </div> --}}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                        <div class="step-delivery">
                            <img src="{{ asset('img/DELIVERY-DOMLI.png') }}"  alt="..." >
                            <h3>{{__('delivery.step_two_question')}}</h3>
                            <p><strong>{{ __('delivery.step_two.title')}}</strong></p>
                            
                            <ul style="padding:0 20px ">
                                <li>{{ __('delivery.step_two.step_one')}}</li>
                                <li>{{ __('delivery.step_two.step_two')}}</li>
                                <li>{{ __('delivery.step_two.step_three')}}</li>
                                <li>{{ __('delivery.step_two.step_four') }}</li>
                                <li>{{ __('delivery.step_two.step_five') }}</li>
                                <li>{{ __('delivery.step_two.step_six') }}</li>
                                <li>{{ __('delivery.step_two.step_seven')}}</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                        <div class="step-delivery">
                            <img src="{{ asset('img/DELIVERY-DOMLI.png') }}"  alt="..." >
                             <h3>{{__('delivery.step_three_question')}}</h3>
                            <p><strong>{{__('delivery.step_three.title')}}</strong></p>
                            
                            <ul style="padding:0 20px ">
                                <li>{{ __('delivery.step_three.step_one') }}</li>   
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                        <div class="step-delivery">
                            <img src="{{ asset('img/DELIVERY-DOMLI.png') }}"  alt="..." >
                            <h3>{{__('delivery.step_four_question')}}</h3>
                            <p><strong>{{ __('delivery.step_four.title')}}</strong>
                                {{__('delivery.step_four.explain')}}
                            </p>
                            
                            <ul style="padding:0 20px ">
                                <li>{{ __('delivery.step_four.step_one') }}</li>
                                <li>{{ __('delivery.step_four.step_two') }}</li>
                                <li>{{ __('delivery.step_four.step_three') }}</li>
                                <li>{{ __('delivery.step_four.step_four') }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- <h2>Comment ça marche?</h2> -->
                {{-- <div class="row align-items-center">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 py-5">
                        <h2 class="mb-5">
                            Comment devenir un livreur sur domli
                        </h2>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                <div class="step-delivery">
                                    <img src="{{ asset('img/food-delivery.svg') }}"  alt="..." >
                                    <h3>To become a delivery man ?</h3>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt a officia quas eaque commodi nemo expedita nobis voluptates numquam dicta sed .</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 ">
                        <div class="card-bglivr">
                            <img src="{{ asset('img/chemin-concept-illustration_114360-1191.jpg') }}"  alt="..." style="width:100%; height:400px ; object-fit:contain;">
                        </div>
                    </div>
                </div> --}}
            </div>
            {{-- ********** --}}
            <div class="img-deliverer">
                <img src="{{ asset('img/food-delivery.svg') }}"  alt="...">
            </div>
        </section>

        <section class="delivery-explain">
            <div class='container'>
                <div class="row d-flex align-items-center">
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-6">
                         <img src="{{ asset('img/PHONE-DOM.png') }}"  alt="..." style="width:100%; height:550px ; object-fit:contain;">
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-6 mt-5">
                        <h2 class="text-left"> {{ __('delivery.download_title')}} </h2>
                        <p> {{ __('delivery.download_sub')}} </p>

                        <div class="store">
                            <a href="">
                                <img src="{{ asset('img/store_1.jpg') }}"  alt="..." style="width:100% ; object-fit:contain;" >
                            </a>

                            <a href="">
                                <img src="{{ asset('img/store_2.jpg') }}"  alt="..." style="width:100% ; object-fit:contain;"  >
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @push('scripts')
            <script>
                var $toggle = $('.js-toggle'); 
                var $target = $('.js-target');  
                $toggle.on('click', function () {
                    // console.log(this.innerText.substring(0,100));
                    console.log($target.innerText.substring(0,100))
                }).trigger('click');
            </script>
            {{-- <script>
                var $toggle = $('.js-toggle'); 
                var $target = $('.js-target');  
                $toggle.on('click', function () {
                $target.slideToggle();
                }).trigger('click');
            </script> --}}
        @endpush
   </div>
@endsection
{{-- @endif --}}



