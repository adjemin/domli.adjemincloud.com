@extends('layouts.index')

{{-- @include('customer.homePage.dishProgressPopUp') --}}
@section('content')
    @include('customer.homePage.sectionContainer.banner')
    @include('customer.homePage.sectionContainer.howItWorks')
    @include('customer.homePage.sectionContainer.mobileCall')
    {{-- @livewire('popular-comp') --}}
    {{-- @livewire('suggestion-comp') --}}
    {{-- @include('customer.homePage.sectionContainer.suggestions') --}}
    @include('customer.homePage.sectionContainer.about')
@endsection
