<div class="blur-bg disabled" id="blur-bg">
</div>
<div class="select-quantity disabled" id="select-quantity-pop-up">
   <div class="row">
        <div class="card-img col-12" style="background: url('{{ asset('img/2.jpg') }}'); background-size: cover; background-position: center;height: 300px; width: 100%;">

        </div>
        <div class="col-12">
            <div class="cancel-btn col-12"><i class="far fa-window-close" id="fa-window-close"></i></div>
            <h2 class="col-12">Order for a dish of Rice and Peas</h2>
            <div class="form-card col-md-12 col-sm-12">
                <form class="form" id="contact-form" method="post">
                    <div class="controls">
                        <div class="row">
                            <div class="more-or-less-btn col-md-6">
                                <button class="less-btn">
                                    <svg aria-hidden="true" focusable="false" viewBox="0 0 24 24" class="bi bh bn bo"><path d="M19.333 11H4.667v2h14.666z"></path></svg>
                                </button>
                                <div class="show-quantity">1</div>
                                <button class="more-btn">
                                    <svg aria-hidden="true" focusable="false" viewBox="0 0 24 24" class="bi bh bn bo"><path d="M19.333 11H13V4.665h-2v6.333H4.667v2H11v6.334h2v-6.334h6.333z"></path></svg>
                                </button>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-error has-danger">
                                    <input id="form_name" type="text" name="name" placeholder="Add 1 to order">
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <button type="submit" class="butn butn-bg disabled"><span>Continue</span></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
