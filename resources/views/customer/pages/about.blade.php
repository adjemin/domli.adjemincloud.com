@extends('layouts.index')

@include('customer.pageParts.pagesHeader')

@section('content')
    <section class="aboutSlide col-12">
        <div class="slide-title row">
            <div class="container">
                <a href="#" >
                    <h2 class="col-12">{{__('header.about_us')}} </h2>
                </a>
            </div>
        </div>
    </section>
    <section class="about-page-section container">
        <div class="row">
            <div class="col-lg-6 about-text">
                <div class="about-text-content">
                    <p>
                       {{ __('header.about_title') }}
                    </p>
                    <p>
                        {{ __('header.about_desc') }}
                    </p>
                </div>
                <a href="#" class="call-action-btn hvr-buzz-out"> {{ __('header.get_started')}} </a>
            </div>
            <div class="col-lg-6 about-img">
                <img src="{{ asset('img/about-img.jpg') }}" alt="..." class="hvr-bounce-out">
            </div>
        </div>
    </section>
@endsection
