<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Font-->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">
    <title>Domli</title>
    <style>
        *{
            font-family: 'Poppins', sans-serif;
        }
        body{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .logo{
            max-width: 250px;
            height: auto;
        }
        h1{
            font-size: 35px;
            font-weight: 700;
            color: #E8C52A
        }
        p{
            font-size: 18px;
            width: 50%;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }
        .waitImg{
            max-width: 400px;
            height: auto;
            margin-top: -30px;
        }
    </style>
</head>
<body>
    <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="50" alt="..." class="logo">
    <img src="{{ asset('img/waitimg.jpg') }}" alt="..." class="waitImg">
    <h1>
        {{ __('header.congratulations')}}
    </h1>
    <p>
        {{ __('header.confirm_text')}} <strong>{{ $customer->email }}</strong>
    </p>
</body>
</html>
