@extends('layouts.index')


@section('content')
    @include('customer.pageParts.pagesHeader')
    <section class="aboutSlide col-12">
        <div class="slide-title row">
            <div class="container">
                <a href="#" >
                    <h2 class="col-12"> {{__('header.my_cart')}} </h2>
                </a>
            </div>
        </div>
    </section>
    <section class="page-section container">
        <div class="row">
            <div class="col-lg-8 profileAuth-left" style="margin-left: auto; margin-right: auto;">
                <div class="profileAuth-left-content">
                    <div class="tab-pane cart-container" id="order">
                        <nav class="col-12" style="padding: 20px 0; margin-bottom: 15px;">
                            <a href="{{ route('createOrder') }}" class="btn btn-success"> {{__('header.buy')}} </a>
                            <a href="{{ route('emptyCart') }}" class="btn btn-danger"> {{__('header.clear_cart')}} </a>
                        </nav>
                        @if (session('success'))
                            <div class="alert alert-success col-12">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if(session('error'))
                            <div class="alert alert-danger col-12">
                                {{ session('error') }}
                            </div>
                        @endif

                        @php
                            $first = collect(Cart::content())->first();
                        @endphp
                        <form action="{{ route('addDiscount') }}" method="post">
                            @csrf
                            <h4> {{__('header.promo_code')}} </h4>
                            <div class="order-input-card row">
                                <div class="input-group">
                                    <div class="input-group-prepend input-group-append row">
                                        <div class="col-2">
                                            <span>
                                                <img src="{{ asset('img/promo.svg') }}" alt="..." width="30" height="30">
                                            </span>
                                        </div>
                                        <input type="hidden" name="uuid" value="{{$first->id ?? ''}}">
                                        <input type="text" name="coupon" id="delivery-adress" class="form-control col-6" placeholder="{{__('header.promo_question')}} ">
                                        <div class="col-sm-4 col-12" style="padding-top: 15px;">
                                            <input type="submit" class="btn btn-primary input-group-text" value="{{__('header.check')}}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        @if (Cart::count() > 0)
                            @foreach (Cart::content() as $food)
                                @php
                                    $item = \App\Models\Food::where('uuid', $food->id)->first();
                                    if (is_null($item) || empty($item)) {
                                        $item = \App\Models\Refreshment::where('uuid', $food->id)->first();
                                    }
                                @endphp
                                <div class="card item-container col-12" style="margin-bottom: 20px">
                                    <div class="card-container row ">
                                        <div class="card-img col-md-4"
                                        style="background: url('{{ $item->image }}');
                                            background-size: cover;
                                            background-position: center;">
                                        </div>
                                        <div class="card-body col-md-6" style="padding-left: 15px;">
                                            <a href="{{url('/food/detail/'.$food->id)}}">
                                                <p class="location">{{$item->restaurant->region_name}}</p>
                                                <h5>{{ $item->title }}</h5>
                                                <p class="description">
                                                    {{ $item->description }}
                                                </p>
                                                <p>
                                                    {{ __('header.price')}} : {{ '$'.number_format($food->price, 2) }}
                                                </p>
                                            </a>
                                        </div>
                                        <div class="card-btn col-md-2 d-flex flex-column justify-content-between" style="padding-top: 15px;">
                                            <form action="{{ route('distroyCart', $food->id) }}" method="POST">
                                                @csrf

                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" style="float: right;"> {{__('header.remove')}} </button>
                                            </form>
                                            <p>
                                                {{__('header.quantity')}} : {{ $food->qty }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <p style="font-size: 25px;">{{ __('header.empty_cart') }}</p>
                        @endif
                                {{-- <table class="info-order col-12" celspacing="0" cellpadding="15" border="0">
                                    <tr class="description">
                                        <td>

                                        </td>
                                        <td class="btn">
                                            <form action="{{ route('distroyCart', $food->rowId) }}" method="POST">
                                                @csrf

                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Remove</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr class="date">
                                        <td>
                                            Quantity :
                                        </td>
                                        <td>
                                            {{ $food->qty }}
                                        </td>
                                    </tr>
                                    <tr class="payment">
                                        <td>
                                            Price :
                                        </td>
                                        <td>
                                            {{ $food->model->getPrice() }}
                                        </td>
                                    </tr>
                                </table> --}}
                    </div>
                </div>
            </div>
            <div class="col-lg-4 order-detail-right">
                <div class="order-detail-right-content row">
                    <h3> {{__('header.cart_detail')}} </h3>
                    <table class="info-table col-12" celspacing="0" cellpadding="10" border="0">
                        <tr class="Items">
                            <td>
                                {{__('header.items')}} :
                            </td>
                            <td>
                                {{ Cart::count() }}
                            </td>
                        </tr>
                        <tr class="Items">
                            <td>
                                {{__('header.subtotal')}} :
                            </td>
                            <td>
                                {{ Cart::subtotal() }}
                            </td>
                        </tr>
                        <tr class="store">
                            <td>
                                {{__('header.tax')}} :
                            </td>
                            <td>
                                {{ Cart::tax() }}
                            </td>
                        </tr>
                        {{-- <tr class="delivery">
                            <td>
                                Delivery :
                            </td>
                            <td>
                                $30
                            </td>
                        </tr> --}}
                        {{-- <tr class="total">
                            <td>
                                discount :
                            </td>
                            <td>
                                $0
                            </td>
                        </tr> --}}
                        <tr class="total">
                            <td>
                                {{__('header.total_discount')}} :
                            </td>
                            <td>
                                {{-- @php
                                    $discount = collect(Cart::getConditions())->map(function($condition){
                                        return floatval($condition->parsedRawValue) ?? 0;
                                    })->sum();
                                @endphp
                                {{ $discount }} --}}
                            </td>
                        </tr>
                        <tr class="total-pay">
                            <td>
                                {{__('header.total_pay')}} :
                            </td>
                            <td class="price">
                                {{ Cart::total() }}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
