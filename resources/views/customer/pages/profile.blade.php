@extends('layouts.index')


@section('content')
    @include('customer.pageParts.pagesHeader')
    <section class="aboutSlide col-12">
        <div class="slide-title row">
            <div class="container">
                <a href="#">
                    <h2 class="col-12">{{ $customer->name . ' ' . $customer->lastname }}</h2>
                </a>
            </div>
        </div>
    </section>
    <section class="page-section container">
        <div class="row">
            <div class="col-lg-7 col-md-7 profileAuth-left profile">
                <div class="tab-content profileAuth-left-content">
                    <div class="tab-pane active" id="profile">
                        <table class="info-delivery col-12" celspacing="0" cellpadding="15" border="0" id="profile-info">
                            <tr class="photo-profile">
                                <td>
                                    <div class="photo-container"
                                        style="background-image: url('{{ Auth::guard('customer')->user()->photo_profile ?? asset('img/user.png') }}'); background-size: cover; background-position: center;">
                                    </div>
                                </td>
                                <td>
                                    <div class="hvr-buzz-out table-btn" id="update-btn">
                                        {{__('header.update_profile_title')}}
                                    </div>
                                </td>
                            </tr>
                            <tr class="name">
                                <td>
                                    {{__('header.full_name')}} :
                                </td>
                                <td>
                                    {{ Auth::guard('customer')->user()->name . ' ' . Auth::guard('customer')->user()->lastname }}
                                </td>
                            </tr>
                            <tr class="mail">
                                <td>
                                    {{__('header.e_mail')}} :
                                </td>
                                <td>
                                    {{ Auth::guard('customer')->user()->email }}
                                </td>
                            </tr>
                        </table>
                        <form method="POST" action="#" style="display: none;" id="update-form" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="customer_id" id="customer_id" value="{{ Auth::guard('customer')->user()->id }}">

                            <table class="info-delivery col-12" celspacing="0" cellpadding="15" border="0">
                                <tr class="photo-profile">
                                    <td>
                                        <div class="photo-container"
                                            style="background-image: url('{{ Auth::guard('customer')->user()->photo_profile ?? asset('img/user.png') }}'); background-size: cover; background-position: center;"
                                            id="userImg">
                                        </div>
                                    </td>
                                    <td>
                                        <input type="file" name="photo_profile" id="photo_profile" accept="images/*" hidden>
                                        <label for="photo_profile" class="hvr-buzz-out photo_profile_btn">
                                            {{__('header.update_photo')}}
                                        </label>
                                    </td>
                                </tr>
                                <tr class="name">
                                    <td>
                                        {{__('header.name_user')}} :
                                    </td>
                                    <td>
                                        <div class="row">
                                            <input type="text" id="name" placeholder="{{ __('header.update_first_name_tip') }}"
                                                class="input col-12" name="name"
                                                value="{{ Auth::guard('customer')->user()->name }}">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="name">
                                    <td>
                                        {{__('header.last_name_user')}} :
                                    </td>
                                    <td>
                                        <div class="row">
                                            <input type="text" id="last_name" placeholder="{{ __('header.update_last_name_tip') }}"
                                                class="input col-12" name="last_name"
                                                value="{{ Auth::guard('customer')->user()->lastname }}">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="mail">
                                    <td>
                                        {{ __('header.e_mail') }} :
                                    </td>
                                    <td>
                                        <div class="row">
                                            <input type="mail" id="email" placeholder="{{__('header.update_email_tip')}}" class="input col-12"
                                                name="email" value="{{ Auth::guard('customer')->user()->email }}">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="submit-btn">
                                    <td colspan="2">
                                        <span id="update-error" style="color: #d11411; display: none;">{{ __('header.field_empty')}}</span>
                                    </td>
                                    {{-- <td>

                                    </td> --}}
                                </tr>
                                <tr class="submit-btn">
                                    <td>
                                        <button id="update-user-submit" type="submit" class="hvr-buzz-out">
                                            {{ __('header.save_change')}}
                                        </button>
                                    </td>
                                    <td>
                                        <a href="#" class="hvr-buzz-out cancel-btn" id="cancel-update-btn">{{ __('header.cancel_text')}}</a>
                                    </td>
                                </tr>

                            </table>
                        </form>
                    </div>
                    <div class="tab-pane" id="order">
                        @php
                            $items = collect($orders)->map(function($order){
                                return $order->items;
                            })->reject(function($item, $key){
                                return is_null($item) || empty($item);
                            })->toArray();
                            //dd($items);
                            $names = [];
                            foreach($items as $order){
                                $item = collect($order)->map(function($el){
                                    return json_decode($el['data'])->name;
                                });
                                array_push($names, $item); 
                            }   

                        @endphp
                        @foreach($orders as $key => $order)
                        <table class="info-order col-12 my-2" celspacing="0" cellpadding="15" border="0">
                            {{--@if(!is_null($order->items->first()))
                                @if(!is_null(json_decode($order->items->first()->meta_data)->title))
                                    <tr>
                                        <td>Foods</td>
                                        <td>
                                            {{json_decode($order->items->first()->meta_data)->title}}
                                        </td>
                                    </tr>
                                @endif
                            @endif--}}
                            @if(count($names[$key]) > 0)
                            <tr>
                                <td>
                                    {{ __('header.composition') }} :
                                </td>
                                <td>
                                    {{ implode(', ', $names[$key]->toArray())}}   
                                </td>
                            </tr>
                            @endif
                            <tr class="date">
                                <td>
                                    {{ __('header.date') }} :
                                </td>
                                <td>
                                    {{ $order->delivery_date ?? $order->created_at }}
                                </td>
                            </tr>
                            <tr class="payment">
                                <td>
                                    {{ __('header.payment') }} :
                                </td>
                                <td>
                                    {{ $order->payment_method_slug }}
                                </td>
                            </tr>
                            <tr class="time">
                                <td>
                                    {{ __('header.status')}} :
                                </td>
                                <td>
                                    {{ $order->current_status }}
                                </td>
                            </tr>
                            <tr class="description">
                                <td></td>
                                <td class="btn"><a href="{{ url('/customer/orders/'.$order->order_number) }}" class="accept-btn"> {{__('header.detail')}} </a></td>
                            </tr>
                        </table>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 profileAuth-right profile">

                <div class="nav nav-tabs profileAuth-right-content row">
                    <h3>{{__('header.menu')}} </h3>
                    <a href="#profile" class="nav-item nav-link active col-12 hvr-underline-from-left"
                        data-toggle="tab"> {{__('header.profile')}} </a>
                    <a href="#order" class="nav-item nav-link col-12 hvr-underline-from-left" data-toggle="tab"> {{__('header.orders')}} </a>
                    {{-- <a href="#info" class="nav-item nav-link col-12 hvr-underline-from-left" data-toggle="tab">Info</a> --}}
                </div>

            </div>
        </div>
    </section>
@endsection

@push('auth-profile-script')
    <script>
        const updateBtn = document.getElementById('update-btn');
        const updateForm = document.getElementById('update-form');
        const infoView = document.getElementById('profile-info');
        const userPhotoInput = document.getElementById('photo_profile');
        const updateUserSubmit = document.getElementById('update-user-submit');
        const cancelBtn = document.getElementById('cancel-update-btn');
        const allUpdateInput = document.querySelectorAll('#update-form input');

        // console.log(allUpdateInput);
        //Listener image input
        userPhotoInput.addEventListener('change', function(event) {
            event.preventDefault();
            // var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function() {
                userPhotoInput.src = reader.result;
                document.getElementById('userImg').style.backgroundImage = `url('${userPhotoInput.src}')`;
            };
            reader.readAsDataURL(event.target.files[0]);
            // };
        });

        // Listener submit update
        updateUserSubmit.addEventListener('click', function(event) {
            event.preventDefault();

            const userImgCard = document.getElementsByClassName('photo-container');


            var loadFile = function(event) {
                var reader = new FileReader();
                reader.onload = function() {
                    userPhotoInput.src = reader.result;
                    console.log(userPhotoInput.src);
                };
                reader.readAsDataURL(event.target.files[0]);
            };

            // loadFile();
            // let file = userPhotoInput.files[0];

            // let reader = new FileReader();

            // // console.log(reader);
            // let imgPath = reader.result(file);


            // reader.onload = function() {
            //     console.log(imgPath);
            //     // userImgCard.style.background = "url('')"
            // };

            // reader.onerror = function() {
            //     console.log(reader.error);
            // };
        });


        // Update button
        updateBtn.addEventListener('click', function(e) {
            e.preventDefault();
            updateForm.removeAttribute('style');
            infoView.style.display = "none";
        });

        // Cancel button
        cancelBtn.addEventListener('click', function(e) {
            e.preventDefault();
            updateForm.style.display = "none";
            infoView.removeAttribute('style');
        });


        /*
         * Update user profile action
         */

        updateUserSubmit.addEventListener('click', function(e) {
            e.preventDefault();
            let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            let url = "{{ route('profileUpdate') }}";
            let form = document.getElementById('update-form')
            let _data = new FormData(form)
            $.ajax({
                url:url,
                method:"POST",
                data: _data,
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function(data){
                    location.reload();
                },
                error:function(err)
                {
                    document.getElementById('update-error').style.display = "block"
                }
            })
            // let _data = Array.from(allUpdateInput).reduce((acc, input) => ({
            //     ...acc,
            //     [input.id]: input.value
            // }), {})
            // console.log("#######");
            // console.log(_data);
            // console.log(_data);

            // Ajax with Fetch
            
        //     fetch(
        //         url,
        //         {
        //             headers: {
        //                 "Content-type": "multipart/form-data;charset=UTF-8",
        //                 "Accept": "application/json, text-plain, */*",
        //                 "X-Requested-with": "XMLHttpRequest",
        //                 "X-CSRF-TOKEN": token
        //             },
        //             method: "post",
        //             body: JSON.stringify(_data)
        //         })
        //         .then(res => res.text())
        //         .then(data => {
        //             console.log(data);
        //             if (data == 'success') {
        //                 location.reload();
        //             } else {
        //                 document.getElementById('update-error').style.display = "block";
        //             }
        //         })
        //         .catch(err => console.log('Request Failed', err));
        });

    </script>
@endpush
