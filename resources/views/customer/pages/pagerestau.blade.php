@extends('layouts.index')

@section('content')
    <section class="restau--bg">
        <div class="container">
            <h1 class="restau--title">{{__('header.our_restaurant')}}</h1>

            <div class="row">
                @php
                    $restaurants = \App\Models\Restaurants::whereIn('verified', [1, true])->whereNotNull('logo')->inRandomOrder()->get();
                @endphp
                @foreach($restaurants as $restaurant)    
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <a href="{{ url('/page/restaurant/'.$restaurant->uuid) }}">
                            <div class="card--restau hvr-bounce-out">
                                <div class="card--restau__illustration">
                                    <img src="{{ $restaurant->logo }}" alt="{{ $restaurant->name }}" >
                                </div>

                                <div class="card--restau__description text-center">
                                    <p>
                                        {{ $restaurant->name }}
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
{{--                 
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <a href="#">
                        <div class="card--restau hvr-bounce-out">
                            <div class="card--restau__illustration">
                                <img src="{{ asset('img/about-img.jpg') }}" alt="..." >
                            </div>

                            <div class="card--restau__description text-center">
                                <p>
                                    Restaurant chez tantie alice
                                </p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <a href="#">
                        <div class="card--restau hvr-bounce-out">
                            <div class="card--restau__illustration">
                                <img src="{{ asset('img/about-img.jpg') }}" alt="..." >
                            </div>

                            <div class="card--restau__description text-center">
                                <p>
                                    Restaurant chez tantie alice
                                </p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <a href="#">
                        <div class="card--restau hvr-bounce-out">
                            <div class="card--restau__illustration">
                                <img src="{{ asset('img/about-img.jpg') }}" alt="..." >
                            </div>

                            <div class="card--restau__description text-center">
                                <p>
                                    Restaurant chez tantie alice
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <a href="#">
                        <div class="card--restau hvr-bounce-out">
                            <div class="card--restau__illustration">
                                <img src="{{ asset('img/about-img.jpg') }}" alt="..." >
                            </div>

                            <div class="card--restau__description text-center">
                                <p>
                                    Restaurant chez tantie alice
                                </p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <a href="#">
                        <div class="card--restau hvr-bounce-out">
                            <div class="card--restau__illustration">
                                <img src="{{ asset('img/about-img.jpg') }}" alt="..." >
                            </div>

                            <div class="card--restau__description text-center">
                                <p>
                                    Restaurant chez tantie alice
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
            </div> --}}
        </div>
    </section>
@endsection