@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            @if(Auth::guard('restaurant')->check())
                <li class="breadcrumb-item">
                    <a href="{{ url('/backOffice/restaurants/'.Auth::guard('restaurant')->user()->uuid) }}">Orders</a>
                </li>
            @endif
            <li class="breadcrumb-item">
                <a href="{{ Auth::check() ? route('admin-orders-index') : route('backoffice-restaurant-order-index', ['restaurant_id' => Auth::guard('restaurant')->user()->uuid]) }}">Orders</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ Auth::check() ? route('admin-orders-index') : route('backoffice-restaurant-order-index', ['restaurant_id' => Auth::guard('restaurant')->user()->uuid]) }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('orders.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
