<!-- Order Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_number', 'Order Number:') !!}
    {!! Form::text('order_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Waiting Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_waiting', 'Is Waiting:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_waiting', 0) !!}
        {!! Form::checkbox('is_waiting', '1', null) !!}
    </label>
</div>


<!-- Current Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('current_status', 'Current Status:') !!}
    {!! Form::text('current_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::number('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_name', 'Currency Name:') !!}
    {!! Form::text('currency_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Delivery Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_delivery', 'Is Delivery:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_delivery', 0) !!}
        {!! Form::checkbox('is_delivery', '1', null) !!}
    </label>
</div>


<!-- Is Wayting Payment Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_wayting_payment', 'Is Wayting Payment:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_wayting_payment', 0) !!}
        {!! Form::checkbox('is_wayting_payment', '1', null) !!}
    </label>
</div>


<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Restaurant Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('restaurant_id', 'Restaurant Id:') !!}
    {!! Form::number('restaurant_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_id', 'Delivery Id:') !!}
    {!! Form::number('delivery_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_fees', 'Delivery Fees:') !!}
    {!! Form::number('delivery_fees', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    {!! Form::text('currency_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('orders.index') }}" class="btn btn-secondary">Cancel</a>
</div>
