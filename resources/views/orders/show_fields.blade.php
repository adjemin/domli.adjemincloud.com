<!-- Order Number Field -->
<div class="form-group">
    {!! Form::label('order_number', 'Order Number:') !!}
    <p>{{ $orders->order_number }}</p>
</div>

<!-- Is Waiting Field -->
<div class="form-group">
    {!! Form::label('is_waiting', 'Is Waiting:') !!}
    <p>{{ $orders->is_waiting }}</p>
</div>

<!-- Current Status Field -->
<div class="form-group">
    {!! Form::label('current_status', 'Current Status:') !!}
    <p>{{ $orders->current_status }}</p>
</div>

<!-- Payment Methode Id Field -->
<div class="form-group">
    {!! Form::label('payment_methode_id', 'Payment Methode Id:') !!}
    <p>{{ $orders->payment_methode_id }}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{{ $orders->amount }}</p>
</div>

<!-- Currency Name Field -->
<div class="form-group">
    {!! Form::label('currency_name', 'Currency Name:') !!}
    <p>{{ $orders->currency_name }}</p>
</div>

<!-- Is Delivery Field -->
<div class="form-group">
    {!! Form::label('is_delivery', 'Is Delivery:') !!}
    <p>{{ $orders->is_delivery }}</p>
</div>

<!-- Is Wayting Payment Field -->
<div class="form-group">
    {!! Form::label('is_wayting_payment', 'Is Wayting Payment:') !!}
    <p>{{ $orders->is_wayting_payment }}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{{ $orders->customer_id }}</p>
</div>

<!-- Restaurant Id Field -->
<div class="form-group">
    {!! Form::label('restaurant_id', 'Restaurant Id:') !!}
    <p>{{ $orders->restaurant_id }}</p>
</div>

<!-- Delivery Id Field -->
<div class="form-group">
    {!! Form::label('delivery_id', 'Delivery Id:') !!}
    <p>{{ $orders->delivery_id }}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{{ $orders->customer_id }}</p>
</div>

<!-- Restaurant Id Field -->
<div class="form-group">
    {!! Form::label('restaurant_id', 'Restaurant Id:') !!}
    <p>{{ $orders->restaurant_id }}</p>
</div>

<!-- Delivery Id Field -->
<div class="form-group">
    {!! Form::label('delivery_id', 'Delivery Id:') !!}
    <p>{{ $orders->delivery_id }}</p>
</div>

<!-- Delivery Fees Field -->
<div class="form-group">
    {!! Form::label('delivery_fees', 'Delivery Fees:') !!}
    <p>{{ $orders->delivery_fees }}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{{ $orders->currency_code }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $orders->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $orders->updated_at }}</p>
</div>

