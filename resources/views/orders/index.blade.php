@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        @if(Auth::guard('restaurant')->check())<li class="breadcrumb-item"><a href="{{ url('/backOffice/restaurants/'.Auth::guard('restaurant')->user()->uuid) }}"> Restaurant</a></li> @endif
        <li class="breadcrumb-item">Orders</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             {{-- <i class="fa fa-align-justify"></i> --}}
                             Orders
                             {{--<a class="pull-right" href="{{ route('orders.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>--}}
                         </div>
                         <div class="card-body">
                             @include('orders.table')
                              <div class="pull-right mr-3">

                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

