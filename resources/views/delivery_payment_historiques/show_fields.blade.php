<div class="row">
   <div class="col-6">
       <!-- Amount Field -->
       <div class="form-group">
           {!! Form::label('amount', 'Amount:') !!}
           <p>{{ $deliveryPaymentHistorique->amount }}</p>
       </div>
   </div>
   <div class="col-6">
       <!-- Delivery Id Field -->
       <div class="form-group">
           {!! Form::label('delivery_id', 'Delivery Id:') !!}
           <p>{{ $deliveryPaymentHistorique->deliverer->name }}</p>
       </div>
    </div> 
</div>

<div class="row">
    <div class="col-6">
        <!-- Reference Paiement Field -->
        <div class="form-group">
            {!! Form::label('reference_paiement', 'Reference Paiement:') !!}
            <p>{{ $deliveryPaymentHistorique->reference_paiement }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Created At:') !!}
            <p>{{ \Carbon\Carbon::parse($deliveryPaymentHistorique->created_at)->diffForHumans() }}</p>
        </div>
    </div>
</div>
{{-- <!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $deliveryPaymentHistorique->updated_at }}</p>
</div> --}}