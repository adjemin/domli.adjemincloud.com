<!DOCTYPE html>
<html>
	<head>
		<title>Domli</title>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- @yield('meta-csrf-token') --}}

        <title>{{ config('app.name', 'Laravel Multi Auth Guard') }}</title>

        <!-- Scripts -->
        @yield('extra-script')
        {{-- <script>
            window.Laravel = <?php //echo json_encode([ 'csrfToken' => csrf_token(), ]); ?>
        </script> --}}

		<!--Font-->
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">

        {{-- Bootstrap and local css file --}}
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

        <link rel="stylesheet" href="{{ asset('css/Hoverr/css/hover-min.css') }}">

        {{-- Map css --}}
        {{--<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"/>--}}
        @if(in_array(url()->current() , [route('createOrder'), route('createOrderNotAuth'), route('restaurantHome')]))
            <link href='https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.css' rel='stylesheet' />
            <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.0.2/mapbox-gl-directions.css" type="text/css"/>
            <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.2.0/mapbox-gl-geocoder.css' type='text/css' />
        @endif

        <!-- International input css-->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/intlTelInput.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/pages.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/mediaqueries.css') }}">

        <!--Pop up package -->
        <link type="text/css" rel="stylesheet" href="{{ asset('jQuery-popModal/popModal.css') }}">
        <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">

        {{-- Style imported --}}
        @stack('orderDetailStyle')


		<!--FontAwesome Scrypt-->
		<script src="https://kit.fontawesome.com/f0abbeb7fb.js" crossorigin="anonymous"></script>
        {{--<link rel="stylesheet" href="{{ asset('css/app.css') }}">--}}
        @livewireStyles
	</head>
	<body>
		<div class="container-fluid">
            <div class="preloader">


                <div class="lds-ripple">
                    <div></div>
                    <div></div>
                </div>
                {{--<div class="preloader-ring">
                    <div></div><div></div>
                </div>--}}
            </div>
        </div>
