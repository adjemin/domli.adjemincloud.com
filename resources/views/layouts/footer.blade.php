{{-- </div> --}}
<footer class="col-12">
    <div >
        <div class="bottom-card col-12">
            <div class="container">
                <div class="row">
					{{-- <div class="col-lg-3 col-md-6 col-6 description">
                        <h1>DOMLI</h1>{{-- <img src="img/logo.png" class="my-2" height="90" alt="logo">
                        <ul>
                            <li><a href="#">About</a></li>
                        </ul>
                    </div> --}}
                    <div class="col-lg-3 col-md-6 col-6 description">
                        <h1>{{__('header.home.footertitle1')}}</h1>
                        <ul>
                            <li><a href="#">{{__('header.home.footerlink11')}}</a></li>
                            <li><a href="#">{{__('header.home.footerlink12')}}</a></li>
                            <li><a href="#">{{__('header.home.footerlink13')}}</a></li>
                            <li><a href="#">{{__('header.home.footerlink14')}}</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6 description">
                        <h1>{{__('header.home.footertitle2')}}</h1>
                        <ul>
                            <li><a href="#">{{__('header.home.footerlink21')}}</a></li>
                            <li><a href="#">{{__('header.home.footerlink22')}}</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6 description">
                        <h1>{{__('header.home.footertitle3')}}</h1>
                        <ul>
                            <li><a href="#">{{__('header.home.footerlink31')}}</a></li>
                            <li><a href="#">{{__('header.home.footerlink32')}}</a></li>
                            <li><a href="#">{{__('header.home.footerlink33')}}</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6 description">
                        <h1>{{__('header.home.footertitle4')}}</h1>
                        <ul>
                            <li><a href="#">{{__('header.home.footerlink41')}}</a></li>
                            <li><a href="#">{{__('header.home.footerlink42')}}</a></li>
                            <li><a href="#">{{__('header.home.footerlink43')}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 signature-container">
        <div class="container">
            <div class="row signature-content">
                <div class="signature col-6">
                    © Copyright Domli | <a href="https://www.adjemincloud.com" target="_blank" >AdjeminCloud</a>
                </div>
                <div class="col-6">
                    {{-- <select name="categorie" id="categorie" class="select-for-language" style="color: #000;"> --}}
                        {{-- <option value="" data-content="">English</option>
                        <option value="1" data-content="">French</option>
                        <option value="2" data-content="">Mandarin</option>
                        <option value="3" data-content="">English</option> --}}
                        {{-- <option value="1" >
                            <img src="{{ asset('img/united-states.svg') }}"  alt="..." style="width:100%; height:400px ; object-fit:contain;">
                        </option>
                    </select> --}}

                </div>
            </div>

        </div>
    </div>
    </div>
</footer>
<!-- Scripts -->
<script src="{{ asset('/js/app.js') }}"></script>


<!--jQuery pop up modal-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="{{ asset('jQuery-popModal/popModal.js') }}"></script>

<!--Bootstrap tab-->
{{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.0/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

{{-- leaflet-realtime Get postion to google map card--}}
{{--<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>--}}
@if(in_array(url()->current() , [route('createOrder'), route('createOrderNotAuth')]))
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.js'></script>
    <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.0.2/mapbox-gl-directions.js"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.2.0/mapbox-gl-geocoder.min.js'></script>
@endif
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@if(in_array(url()->current() , [route('createOrder'), route('createOrderNotAuth')]))
    <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoibmVnaWVzdW0iLCJhIjoiY2trNWhmcmxwMDVkbzJwbjRqOXRtNGFvdyJ9.bE09ljRXuH102hE_OdkDbA';
        var accessToken = 'pk.eyJ1IjoibmVnaWVzdW0iLCJhIjoiY2trNWhmcmxwMDVkbzJwbjRqOXRtNGFvdyJ9.bE09ljRXuH102hE_OdkDbA';
        var start, end;

        var latitude = "{{ $userCoordinates['latitude'] ?? 5.3891782 }}";
        var longitude = "{{ $userCoordinates['longitude'] ?? -4.0371347 }}";

        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
            center: [parseFloat(longitude), parseFloat(latitude)], // starting position
            zoom: 12 // starting zoom
        });

        var latitude_restaurant = "{{ $restaurants->lat ?? 5.3891782}}";
        var longitude_restaurant = "{{ $restaurants->lng ?? -4.0371347}}";
        country_code = '{{ $userCoordinates["country_code"] ?? "CI" }}',
        console.log(country_code)
        end = [parseFloat(longitude_restaurant), parseFloat(latitude_restaurant)]

        var marker_restaurant = new mapboxgl.Marker({
            color: "#E8C52A",
            draggable: false
        }).setLngLat([parseFloat(longitude_restaurant), parseFloat(latitude_restaurant)])
        .addTo(map)

        map.addControl(new mapboxgl.FullscreenControl())

        // if(latitude != -4.0371347 || longitude != 5.3891782){
        //     var bounds = [
        //         [-4.0291, 5.4354],
        //         [-4.0675, 5.3438]
        //     ];
        // }

        // var latitude_restaurant = {{5.3891782 ?? $restaurants->lat}},
        // longitude_restaurant = {{-4.0371347 ?? $restaurants->lng}};
        // country_code = '{{ "CI" ??  $userCoordinates["country_code"] ?? "CI" }}',
        // console.log(country_code)
        // end = [longitude_restaurant, latitude_restaurant]

        // var marker_restaurant = new mapboxgl.Marker({
        //     color: "#E8C52A",
        //     draggable: false
        // }).setLngLat([longitude_restaurant, latitude_restaurant])
        // .addTo(map)

        // map.addControl(new mapboxgl.FullscreenControl())

        // if(latitude != -4.0371347 || longitude != 5.3891782){
        //     var bounds = [
        //         [-4.0291, 5.4354],
        //         [-4.0675, 5.3438]
        //     ];
        // }

        var canvas = map.getCanvasContainer();

        getRoute(end, end, accessToken);


        // function getRoute(start, end, accessToken) {
        //     var url = 'https://api.mapbox.com/directions/v5/mapbox/driving/' + start[0] + ',' + start[1] + ';' + end[0] + ',' + end[1] +'?steps=true&geometries=geojson&access_token=' + accessToken;
        //     var req = new XMLHttpRequest();
        //     req.open('GET', url, true);
        //     req.onload = function() {
        //         var json = JSON.parse(req.response);
        //         var data = json.routes[0];

        //         document.getElementById('distance_delivery').innerText = parseFloat(data.distance / 1000).toFixed(2)  + " KM";
        //         document.getElementById('delivery_fees_table').innerText = '$ ' + parseFloat((data.distance/1000)*20 + 6).toFixed(2)
        //         $('#delivery_fees').val(parseFloat((data.distance/1000)*20 + 6).toFixed(2))
        //         document.querySelector('#total_price_pay').innerText = parseFloat(document.querySelector('#total_price_pay').innerText) + parseFloat(document.querySelector('#delivery_fees').value)
        //         var route = data.geometry.coordinates;
        //         var route = data.geometry.coordinates;
        //         var geojson = {
        //             type: 'Feature',
        //             properties: {},
        //             geometry: {
        //                 type: 'LineString',
        //                 coordinates: route
        //             }
        //             // add turn instructions here at the end
        //         };
        //         req.send();
        //     }
        //     var latitude_restaurant = {{5.3891782 ?? $restaurants->lat}},
        //     longitude_restaurant = {{-4.0371347 ?? $restaurants->lng}};
        //     country_code = '{{ $userCoordinates["country_code"] ?? "CI" }}',
        //     console.log(country_code)
        //     end = [longitude_restaurant, latitude_restaurant]

        //     var marker_restaurant = new mapboxgl.Marker({
        //         color: "#E8C52A",
        //         draggable: false
        //     }).setLngLat([longitude_restaurant, latitude_restaurant])
        //     .addTo(map)

        //     map.addControl(new mapboxgl.FullscreenControl())

        //     if(latitude != -4.0371347 || longitude != 5.3891782){
        //         var bounds = [
        //             [-4.0291, 5.4354],
        //             [-4.0675, 5.3438]
        //         ];
        //     }

        //     var canvas = map.getCanvasContainer();

        //     getRoute(end, end, accessToken);
        // }


        function getRoute(start, end, accessToken) {
            var url = 'https://api.mapbox.com/directions/v5/mapbox/driving/' + start[0] + ',' + start[1] + ';' + end[0] + ',' + end[1] +'?steps=true&geometries=geojson&access_token=' + accessToken;
            var req = new XMLHttpRequest();
            req.open('GET', url, true);
            req.onload = function() {
                var json = JSON.parse(req.response);
                var data = json.routes[0];
                console.log(url)
                console.log(data)
                console.log(data.distance)
                console.log(data.duration)
                if(data.code != "NoRoute"){
                    document.getElementById('distance_delivery').innerText = parseFloat(data.distance / 1000).toFixed(2)  + " KM";
                    document.getElementById('delivery_fees_table').innerText = '$ ' + parseFloat(Math.ceil((data.distance/1000)*20) + 6).toFixed(2)
                    $('#delivery_fees').val(parseFloat(Math.ceil((data.distance/1000)*20) + 6).toFixed(2))
                    $('#kilometers').val(parseFloat(data.distance).toFixed(2))
                    document.querySelector('#total_price_pay').innerText = parseFloat(document.querySelector('#total_price_pay').innerText) + parseFloat(document.querySelector('#delivery_fees').value)
                    var route = data.geometry.coordinates;

                    var geojson = {
                        type: 'Feature',
                        properties: {},
                        geometry: {
                            type: 'LineString',
                            coordinates: route
                        }
                    };

                    if (map.getSource('route')) {
                        map.getSource('route').setData(geojson);
                    } else {
                        // otherwise, make a new request
                        map.addLayer({
                            id: 'route',
                            type: 'line',
                            source: {
                                type: 'geojson',
                                data: {
                                    type: 'Feature',
                                    properties: {},
                                    geometry: {
                                        type: 'LineString',
                                        coordinates: geojson
                                    }
                                }
                            },
                            layout: {
                                'line-join': 'round',
                                'line-cap': 'round'
                            },
                            paint: {
                                'line-color': '#3887be',
                                'line-width': 6,
                                'line-opacity': 0.75
                            }
                        });
                    }
                }else{
                    Swal.fire({
                        title:'Oops...',
                        text:'The dish has already been added to the cart!',
                        icon:'error'
                    });
                    document.querySelector('button.submit-btn[type="submit"]').disabled = true;
                }


                // add turn instructions here at the end
            };
            req.send();
        }

    </script>
    @if(url()->current() == route('createOrder'))
    <script>
        var marker
        // window.onload = function () {
        //     let posiion = [];

        //     let startlat = 5.39150;
        //     let startlon = -3.97765;

        //     var options = {
        //         center: [startlat, startlon],
        //         zoom: 15
        //     }

        //     var map = L.map('map', options);

        //     L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OSM'}).addTo(map);

        //     // var input_label = data[index].input_label;
        //     console.log('Latitude ===>   '+ startlat);
        //     console.log('Longitude ===>   ' + startlon);

        //     var marker = L.marker([startlat, startlon], {
        //         title: "votre position",
        //         alt: "votre position",
        //     }).addTo(map)
        //     .bindPopup("votre position" + "<br/>" + "Client: " + "votre position" + "<br/>" + "Lat: " + startlat + "<br />Lon: " + startlon).openPopup();
        // }
        function getLocation(str) {
            value = str.match(/(\d+)(\.\d+)?/g); // trim
            return value;
        }
        //search = document.getElementById("input_search");
        const matchList = document.getElementById("match-list")
        $("#delivery_adress").autocomplete({
            source: function(query, callback) {
                $.getJSON(
                    `https://nominatim.openstreetmap.org/search?postalcode=${query.term}&countrycodes=CI,CA&format=json&addressdetails=1&limit=100`,
                    // `https://nominatim.openstreetmap.org/search?q=${query.term}&countrycodes=${country_code}&format=json&addressdetails=1&limit=100`,
                    function(data) {
                        var places = data;
                        places = places.map(place => {
                            console.log("==== place ===");
                            console.log(place);
                            return {
                                title: place.display_name.replace(/<br\/>/g, ", "),
                                value: place.display_name.replace(/<br\/>/g, ", "),
                                lat: place.lat,
                                lon: place.lon,
                                id: place.osm_id
                            };
                        });
                        return callback(places);
                    });
            },
            minLength: 2,
            select: function(event, ui) {
                console.log(ui);
                $('#lat_input').val(ui.item.lat);
                $('#lon_input').val(ui.item.lon);
                $("#delivery_adress").val(ui.item.title);
                // $(this).val("")
                // return false

                start = [ui.item.lon , ui.item.lat];

                map.flyTo({
                    center : start,
                    zoom: 13,
                    essential: true
                });
                console.log("Selected: " + ui.item.title);

                if(marker != null || marker != undefined){
                    marker.remove()
                }

                marker = new mapboxgl.Marker({
                    color: "#E8C52A",
                    draggable: false
                }).setLngLat([ui.item.lon, ui.item.lat])
                .addTo(map)

                map.fitBounds([
                    end,
                    start
                ], {
                    padding: {
                        top: 10,
                        bottom:25,
                        left: 15,
                        right: 5
                    }
                });

                getRoute(start, end, accessToken)
            },
            open: function() {
                $('.ui-autocomplete').css('width', '400px').css('cursor', 'pointer'); // HERE
            }
        });
    </script>
    @endif
@endif

@stack('intel-input-script')

<!--Add my script -->
{{-- <script src="{{ asset('js/script.js') }}"></script> --}}

<!--Sweet alert -->
{{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
{{-- <script src="sweetalert2/dist/sweetalert2.all.min.js"></script> --}}
<!-- Include a polyfill for ES6 Promises (optional) for IE11 -->
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>


{{-- Customized script --}}
    @yield('extra-js')
    @yield('scriptRestaurantCategorie')
    @stack('scripts')
    @stack('customer-login-script')
    @stack('auth-profile-script')
    @stack('order-script')
{{-- End Customized script --}}

@if (url()->current() == url('/'))
<script>
    function getLocation(str) {
        value = str.match(/(\d+)(\.\d+)?/g); // trim
        return value;
    }
    const matchList = document.getElementById("match-list")
    $("#delivery_adress").autocomplete({
        source: function(query, callback) {
            $.getJSON(
                `https://nominatim.openstreetmap.org/search?street=${query.term}&countrycodes=CI,CA&format=json&addressdetails=1&limit=100`,
                // `https://nominatim.openstreetmap.org/search?q=${query.term}&countrycodes=${country_code}&format=json&addressdetails=1&limit=100`,
                function(data) {
                    var places = data;
                    places = places.map(place => {
                        console.log("==== place ===");
                        console.log(place);
                        return {
                            title: place.display_name.replace(/<br\/>/g, ", "),
                            value: place.display_name.replace(/<br\/>/g, ", "),
                            lat: place.lat,
                            lon: place.lon,
                            id: place.osm_id
                        };
                    });
                    return callback(places);
                });
        },
        minLength: 2,
        select: function(event, ui) {
            console.log(ui);
            $('#lat_input').val(ui.item.lat);
            $('#lon_input').val(ui.item.lon);
            $("#delivery_adress").val(ui.item.title);
            // $(this).val("")
            // return false
        },
        open: function() {
            $('.ui-autocomplete').css('width', '600px').css('background-color', 'white').css('z-index',1000).css('cursor', 'pointer'); // HERE
        }
    });
</script>
@endif

<script>
    // Preloader animation

    const preloader = document.querySelector(".preloader");

    window.addEventListener("load", ()=>{
        preloader.classList.replace("preloader", "fondu-out");
    });
</script>
<script>   
    function myFunction(smallImg)
    {
        var fullImg = document.getElementById ("imageBox");
        fullImg.src = smallImg.src;
    }
</script>
<script src="{{ asset('/js/owl.carousel.min.js') }}"></script>
@livewireScripts
</body>
</html>
