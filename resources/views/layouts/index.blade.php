@include('layouts.header')

{{-- Page body content --}}
@yield('content')

@stack('scripts')
@include('layouts.footer')
