@if(\Auth::check())
<li class="nav-item {{ Request::is('admin/Home*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin-home-index') }}">
        {{-- <i class="flaticon-home"></i> --}}
        <span>Home</span>
    </a>
</li>
<li class="nav-item {{ Request::is('admin/customers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin-customers-index') }}">
        {{-- <i class="flaticon-customer"></i> --}}
        <span>Customers</span>
    </a>
</li>
<li class="nav-item {{ Request::is('admin/foodCategories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin-foodCategories-index') }}">
        {{-- <i class="flaticon-categories"></i> --}}
        <span>Food Categories</span>
    </a>
</li>
<li class="nav-item {{ Request::is('contacts*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('contacts.index') }}">
        {{-- <i class="nav-icon icon-cursor"></i> --}}
        <span>Contacts</span>
    </a>
</li>
<li class="nav-item {{ Request::is('admin/deliveries*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin-deliverers-index') }}">
        {{-- <i class="nav-icon icon-cursor"></i> --}}
        <span>Deliveries</span>
    </a>
</li>
@endif
@if(\Auth::guard('restaurant')->check())
<li class="nav-item {{ Request::is('admin/Home*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin-home-index') }}">
        {{-- <i class="flaticon-home"></i> --}}
        <span>Home</span>
    </a>
</li>
<li class="nav-item {{ Request::is('admin/customers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin-customers-index') }}">
        {{-- <i class="flaticon-customer"></i> --}}
        <span>Customers</span>
    </a>
</li>
<li class="nav-item {{ Request::is('admin/foodCategories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin-foodCategories-index') }}">
        {{-- <i class="flaticon-categories"></i> --}}
        <span>Food Categories</span>
    </a>
</li>
@endif
<li class="nav-item {{ Request::is('admin/restaurants*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin-restaurants-index') }}">
        {{-- <i class="flaticon-restaurant"></i> --}}
        <span>Restaurants</span>
    </a>
</li>
{{--<li class="nav-item {{ Request::is('otps*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('otps.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Otps</span>
    </a>
</li>--}}
{{-- <li class="nav-item {{ Request::is('vehicleTypes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('vehicleTypes.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Vehicle Types</span>
    </a>
</li> --}}

{{--
<li class="nav-item {{ Request::is('customerDevices*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('customerDevices.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Customer Devices</span>
    </a>
</li>
<li class="nav-item {{ Request::is('paymentMethodes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('paymentMethodes.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Payment Methodes</span>
    </a>
</li>
--}}
<li class="nav-item {{ Request::is('admin/orders*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin-orders-index') }}">
        {{-- <i class="flaticon-order-food"></i> --}}
        <span>Orders</span>
    </a>
</li>
{{--
<li class="nav-item {{ Request::is('orderDeliveries*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orderDeliveries.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Order Deliveries</span>
    </a>
</li>--}}
{{--
<li class="nav-item {{ Request::is('deliveryAdresses*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('deliveryAdresses.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Delivery Adresses</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orderHistories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orderHistories.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Order Histories</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orderItems*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orderItems.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Order Items</span>
    </a>
</li>
<li class="nav-item {{ Request::is('restaurantPhones*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('restaurantPhones.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Restaurant Phones</span>
    </a>
</li>
<li class="nav-item {{ Request::is('restaurantMenuses*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('restaurantMenuses.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Restaurant Menuses</span>
    </a>
</li>
<li class="nav-item {{ Request::is('restaurantCategories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('restaurantCategories.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Restaurant Categories</span>
    </a>
</li>
<li class="nav-item {{ Request::is('restaurantUsers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('restaurantUsers.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Restaurant Users</span>
    </a>
</li>
<li class="nav-item {{ Request::is('menuses*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('menuses.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Menuses</span>
    </a>
</li>
--}}
{{--<li class="nav-item {{ Request::is('admin/food*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin-food-index') }}">
        <i class="flaticon-dish"></i>
        <span>Food</span>
    </a>
</li>
<li class="nav-item {{ Request::is('admin/foodParts*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin-foodParts-index') }}">
        <i class="flaticon-circular-cake-graphic-with-quarter-part-cutted"></i>
        <span>Food Parts</span>
    </a>
</li>--}}
{{--<li class="nav-item {{ Request::is('restaurantAvailabilities*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('restaurantAvailabilities.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Restaurant Availabilities</span>
    </a>
</li>


<li class="nav-item {{ Request::is('deliveryTypes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('deliveryTypes.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Delivery Types</span>
    </a>
</li>
--}}

{{-- <li class="nav-item {{ Request::is('vehicles*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('vehicles.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Vehicles</span>
    </a>
</li>
<li class="nav-item {{ Request::is('deliveryAvailaibities*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('deliveryAvailaibities.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Delivery Availaibities</span>
    </a>
</li>
<li class="nav-item {{ Request::is('jobs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('jobs.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Jobs</span>
    </a>
</li>
<li class="nav-item {{ Request::is('jobHistoriques*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('jobHistoriques.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Job Historiques</span>
    </a>
</li>
<li class="nav-item {{ Request::is('agentConnexions*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('agentConnexions.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Agent Connexions</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orderAssignements*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orderAssignements.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Order Assignements</span>
    </a>
</li>
<li class="nav-item {{ Request::is('restaurantPayments*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('restaurantPayments.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Restaurant Payments</span>
    </a>
</li>
<li class="nav-item {{ Request::is('deliveryPayments*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('deliveryPayments.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Delivery Payments</span>
    </a>
</li> --}}
{{-- <li class="nav-item {{ Request::is('restaurantComptes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('restaurantComptes.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Restaurant Comptes</span>
    </a>
</li>
<li class="nav-item {{ Request::is('deliveryComptes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('deliveryComptes.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Delivery Comptes</span>
    </a>
</li>
<li class="nav-item {{ Request::is('deliveryPaymentHistoriques*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('deliveryPaymentHistoriques.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Delivery Payment Historiques</span>
    </a>
</li>
<li class="nav-item {{ Request::is('restaurantPaymentHistoriques*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('restaurantPaymentHistoriques.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Restaurant Payment Historiques</span>
    </a>
</li>
<li class="nav-item {{ Request::is('requestRestaurantPayments*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('requestRestaurantPayments.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Request Restaurant Payments</span>
    </a>
</li>
<li class="nav-item {{ Request::is('requestDeliveryPayments*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('requestDeliveryPayments.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Request Delivery Payments</span>
    </a>
</li> --}}

{{-- <li class="nav-item {{ Request::is('passwordDeliveries*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('passwordDeliveries.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Password Deliveries</span>
    </a>
</li> --}}
