<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{config('app.name')}}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 4.1.1 -->
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/css/coreui.min.css">
    <!-- Ionicons -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css"> --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">
    {{-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet"> --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css"> --}}
    @yield('css')
    <link rel="stylesheet" href="{{ asset('css/restaurantBack.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('css/flaticon.css') }}"> --}}
    <script src="https://kit.fontawesome.com/f0abbeb7fb.js" crossorigin="anonymous"></script>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show {{ Auth::guard('restaurant')->check() ? ' app-body-restaurant' : '' }}">
<header class="app-header navbar">
    {{-- <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button> --}}
    <a class="navbar-brand" href="#">
        <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="50" alt="..." class="navbar-brand-full">
        {{-- <img class="navbar-brand-full" src="http://infyom.com/images/logo/blue_logo_150x150.jpg" width="30" height="30"
             alt="InfyOm Logo">
        <img class="navbar-brand-minimized" src="http://infyom.com/images/logo/blue_logo_150x150.jpg" width="30"
             height="30" alt="InfyOm Logo"> --}}
    </a>
    {{-- <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button> --}}

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="{!! Auth::guard('restaurant')->check() ? route('backoffice-restaurant-order-index', ['restaurant_id' => Auth::guard('restaurant')->user()->uuid]) : route('admin-orders-index') !!}">
                <svg id="Layer_4" enable-background="new 0 0 24 24" height="25" viewBox="0 0 24 24" width="25" xmlns="http://www.w3.org/2000/svg">
                    <path d="m21.379 16.913c-1.512-1.278-2.379-3.146-2.379-5.125v-2.788c0-3.519-2.614-6.432-6-6.92v-1.08c0-.553-.448-1-1-1s-1 .447-1 1v1.08c-3.387.488-6 3.401-6 6.92v2.788c0 1.979-.867 3.847-2.388 5.133-.389.333-.612.817-.612 1.329 0 .965.785 1.75 1.75 1.75h16.5c.965 0 1.75-.785 1.75-1.75 0-.512-.223-.996-.621-1.337z"/><path d="m12 24c1.811 0 3.326-1.291 3.674-3h-7.348c.348 1.709 1.863 3 3.674 3z"/>
                </svg>
                <span class="badge badge-pill badge-danger">{{ Auth::guard('restaurant')->check() ? \App\Models\RestaurantNotification::where(['restaurant_id' => Auth::guard('restaurant')->user()->id ,'is_read' => 0])->count() : \App\Models\RestaurantNotification::where('is_read', 0)->count() }}</span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link" style="margin-right: 10px" data-toggle="dropdown" href="#" role="button"
               aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->name ?? Auth::guard('restaurant')->user()->name ?? '' }}
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>Account</strong>
                </div>
                <a class="dropdown-item" href="{!! Auth::guard('restaurant')->check() ? route('backoffice-restaurant-order-index', ['restaurant_id' => Auth::guard('restaurant')->user()->uuid]) : route('admin-orders-index') !!}">
                    <i class="fa fa-envelope-o"></i> @lang('auth.app.messages')
                    <span class="badge badge-success">{{ Auth::guard('restaurant')->check() ? \App\Models\RestaurantNotification::where(['restaurant_id' => Auth::guard('restaurant')->user()->id ,'is_read' => 0])->count() : \App\Models\RestaurantNotification::where('is_read', 0)->count() }}</span>
                </a>
                {{--<div class="dropdown-header text-center">
                    <strong>@lang('auth.app.settings')</strong>
                </div>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-user"></i> @lang('auth.app.profile')</a>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-wrench"></i> @lang('auth.app.settings')</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-shield"></i> @lang('auth.app.lock_account')</a>--}}
                <a class="dropdown-item" href="{{ Auth::check() ? url('/admin/logout') :  url('/restaurant/logout') }}" class="btn btn-default btn-flat"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-lock"></i>@lang('auth.sign_out')
                </a>
                <form id="logout-form" action="{{ Auth::check() ? url('/admin/logout') :  url('/restaurant/logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</header>

<div class="app-body">
    {{-- @if(Auth::check()) --}}
        @include('layouts.sidebar')
    {{-- @endif --}}
    <main class="main {{ Auth::guard('restaurant')->check() ? ' restaurant-main' : '' }}">
        @yield('content')
    </main>
</div>
<footer class="app-footer">
    <div>
        <a href="https://www.adjemincloud.com">Domli | </a>
        <span>&copy; 2020 adjemincloud.</span>
    </div>
</footer>
</body>
<!-- jQuery 3.1.1 -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/js/coreui.min.js"></script>
@stack('scripts')

</html>
