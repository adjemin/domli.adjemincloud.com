<div>
    <section class="page-section container">
        <div id="nav-cat-scroll">
            <div class="form-container col-lg-12">
                <div class="row">
                    {{--<form id="lis-input" method="post" action="/search/restaurants">
                        <input type="text" name="search" id="myInput" placeholder="Search a restaurant now" class="writting">
                        <input class="addBtn" type="submit" id="addBtn" value="Add">
                    </form>--}}
                    <form method="post" action="/search/restaurants" id="lis-input" class="col-sm-7 col-12">
                        @csrf
                        <input type="text" name="search" id="myInput" placeholder="Search a restaurant now" class="writting" value="{{$search ?? ''}}">
                        <input class="addBtn" type="submit" id="addBtn" value="<i class='fas fa-search'></i>">
                        <nav class="addBtn" id="addBtn"><i class="fas fa-search"></i></nav>
                    </form>
                    <div class="location-container col-sm-5 col-12">
                        <div class="location-content">
                            <span class="location-pin"><img src="{{ asset('img/pin.svg') }}" alt="..." width="35" height="35"></span>
                            <span>
                                <h2>{{ $userCoordinates['city'] ?? '' }}</h2>
                                <p>{{ $userCoordinates['country_code'] ?? '' }}</p>
                            </span>
                        </div>
                    </div>
                    {{-- @yield('location') --}}
                </div>
            </div>
            <div class="col-lg-12 restaurant-categorie-list" id="CategorieMenu">
                {{-- @yield('restaurant-categorie-list') --}}
                <ul>
                    {{--<li><a href="#" class="hvr-buzz-out">Good food</a></li>
                    <li><a href="#" class="hvr-buzz-out">Pizza</a></li>
                    <li><a href="#" class="hvr-buzz-out">Burger</a></li>
                    <li><a href="#" class="hvr-buzz-out">American</a></li>
                    <li><a href="#" class="hvr-buzz-out">Suchi</a></li>
                    <li><a href="#" class="hvr-buzz-out">Italian</a></li>
                    <li><a href="#" class="hvr-buzz-out">French</a></li>

                    <li><a href="/restaurants?categories={{$food->slug}}" class="hvr-buzz-out">Japanese</a></li>--}}
                    @php
                        $firstLink = 1;
                        $ids = [];
                        // dd($_SERVER['REQUEST_URI']);
                    @endphp
                    @foreach($food_categorires as $food)
                        @if ($firstLink <= 1)
                            <li ><a href="#{{$food->slug}}" class="hvr-buzz-out navigation__link active">{{$food->title}}</a></li>
                        @else
                            <li ><a href="#{{$food->slug}}" class="hvr-buzz-out navigation__link">{{$food->title}}</a></li>
                        @endif

                        @php
                            array_push($ids, $food->id);
                            $firstLink++;
                        @endphp
                    @endforeach
                    <!-- <li class="see-more-btn"><a href="#">See more</a></li> -->
                </ul>
            </div>
        </div>
        @if (!empty($restaurant_categories->all()))
            @php
                $restaurant_categories_sort = $restaurant_categories->sortKeys();
                $i = 0;
            @endphp
            {{-- @dd($restaurant_categories_sort) --}}
            {{-- @for($i = 1; $i <= length($restaurant_categories); $i++ ) --}}
            @foreach ($restaurant_categories_sort as $restaurants)
                    <div class="col-lg-12 restaurant-categorie-container" id="{{collect($food_categorires)->where('id', $ids[$i])->first()->slug}}">
                        <div class="row">
                            <div class="section-title col-md-12 col-lg-12 col-sm-12">
                                <h3>{{ collect($food_categorires)->where('id', $ids[$i])->first()->title ?? __('header.no_name_found')}}</h3>
                            </div>
                            {{-- <div class="row"> --}}
                                @php
                                    $j = 1;
                                @endphp

                                {{-- @if (isset($restaurants)) --}}
                                    @forelse($restaurants as $restaurant)
                                        {{-- @dd($restaurant) --}}
                                        <div class="card item-container col-md-6 hvr-wobble-vertical">
                                            <div class="card-container row ">
                                                <div class="card-img col-5"
                                                style="background: url('{{ $restaurant->restaurant->cover ?? $restaurant->restaurant->logo ?? 'https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/cef389b486cb4827e6ba007f26ebddab.svg' }}');background-size: cover; background-position: center;">
                                                </div>
                                                <div class="card-body col-7">
                                                    {{-- <p class="note-promo">-100%</p> --}}
                                                    {{-- <a href="{{ route('restaurant', $restaurant->id) }}">--}}
                                                    <a href="{{ url('/page/restaurant/'.$restaurant->restaurant->uuid) }}">
                                                        <p class="location">{{ $restaurant->restaurant->restaurant_city ?? '' }}</p>
                                                        <h5>{{ $restaurant->restaurant->name }}</h5>
                                                        <p class="description">
                                                            {{\Str::limit(__('header.lorem', 50))}}
                                                        </p>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- @php
                                            $j++;
                                        @endphp --}}
                                    @empty
                                        <div class="justify-content-center align-content-center ">
                                            <h3>{{('header.no_result')}}</h3>
                                        </div>
                                    @endforelse
                                {{-- @endif --}}
                            {{-- </div> --}}
                        </div>
                    </div>
                    @php
                        $i++;
                    @endphp
                @endforeach
            {{-- @endfor --}}
        @else
            <h1> {{__('header.no_restaurant_found')}} </h1>
        @endif

        {{-- <div class="col-lg-12 restaurant-categorie-container">

            <div class="row">
                <div class="section-title col-md-12 col-lg-12 col-sm-12">
                    <h3>Other</h3>
                </div>
                <div class="row">
                    @php
                        $i = 1;
                    @endphp
                    @if (isset($others))
                        @foreach($others as $restaurant)
                            <div class="card item-container col-md-6 hvr-wobble-vertical">
                                <div class="card-container row ">
                                    @if ($i > 10)
                                        <div class="card-img col-md-5"
                                        style="background: url('https://via.placeholder.com/468x60');
                                            background-size: cover;
                                            background-position: center;">
                                        </div>
                                    @else
                                        <div class="card-img col-md-5"
                                        style="background: url('{{ asset('img/'.$i.'.jpg') }}');
                                            background-size: cover;
                                            background-position: center;">
                                        </div>
                                    @endif
                                    <div class="card-body col-md-7">
                                        {{-- <p class="note-promo">-100%</p> --}
                                        <a href="{{ route('restaurant', $restaurant->id) }}">
                                            <p class="location">Californie</p>
                                            <h5>{{ $restaurant->name }}</h5>
                                            <p class="description">
                                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, quisquam
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @php
                                $i++;
                            @endphp

                        @endforeach
                    @endif
                </div>
            </div>
        </div> --}}
    </section>
</div>
