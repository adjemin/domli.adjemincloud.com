{{-- @include('frontOffice.pages.dishPopUp') --}}
{{-- @include('customer.pageParts.pagesHeader') --}}
<section class="pagesBanner col-12" style="background: url('{{ asset('img/8.jpg') }}'); background-size: cover; background-position: center;">
    <div class="slide-title row">
        <div class="container">
            {{-- <h2 class="col-12">{{ $restaurant->name }}</h2> --}}
        </div>
    </div>
</section>
<section class="page-section container">
    <div class="form-container col-lg-12">
        <div class="row">
            <form id="lis-input" class="col-7" action="/search/foods" method="post">
                @csrf
                {{-- <input type="hidden" name="resto" value="{{$restaurant->id}}" > --}}
                <input type="text" name="title" id="myInput" placeholder="Seach a dish here . . ."  value="" class="writting" wire:model="sidik">

                <input class="addBtn" type="submit" id="addBtn" value="<i class='fas fa-search'></i>">
                <nav class="addBtn" id="addBtn"><i class="fas fa-search"></i></nav>
            </form>
            <div class="location-container col-5">
                <div class="location-content">
                    <span class="location-pin"><img src="{{ asset('img/pin.svg') }}" alt="..." width="35" height="35"></span>
                    <span>
                        {{ $sidik ?? '' }}
                        {{-- @php
                            $reverse_localisation = json_decode(file_get_contents('https://nominatim.openstreetmap.org/reverse.php?lat='.$restaurant->lat.'&lon='.$restaurant->lng.'&format=jsonv2'), true);
                        @endphp
                        @if(!is_null($reverse_localisation))
                            <h2>{{$reverse_localisation["name"] ?? $reverse_localisation["display_name"] ?? '' }}</h2>
                            <p>{{ $reverse_localisation["display_name"][country] }}</p>
                        @endif --}}
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 restaurant-categorie-list">
        <ul>
            {{-- /restaurant/{{$restaurant->id}}?categories={{$categorie->title}} --}}
            {{-- @foreach($this->food_categories as $categorie) --}}
                {{-- <li><a href="#" wire:click="showCategorie" class="hvr-buzz-out">{{$categorie->title}}</a></li> --}}
            {{-- @endforeach --}}
        </ul>
    </div>
    <div class="col-lg-12 restaurant-categorie-container">
        <div class="row dish-content">
            <div class="section-title col-md-12 col-lg-12 col-sm-12">
                {{-- <h3>{{$this->restaurant_categories}}</h3> --}}
            </div>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="row" style="width: 100%;">
                {{-- @php
                    $i = 1;
                @endphp
                @foreach ($foods as $food)

                    <div class="card item-container dish-container col-md-3">
                        <div class="card-container row hvr-wobble-vertical">
                            <div class="card-img col-md-12"
                            style="background: url('{{ asset('img/'.$i.'.jpg') }}');
                                background-size: cover;
                                background-position: center;">

                                <div class="card-body">
                                    <p class="price">
                                        {{ getPrice($food->price) }}
                                    </p>
                                    <a href="#" class="submitLink">
                                        <h5>{{ $food->title }}</h5>
                                        <p class="description">
                                            @if (strlen($food->description) >= 90)
                                                {{ substr($food->description, 0, -50) }}
                                            @else
                                                {{ $food->description }}
                                            @endif

                                        </p>
                                    </a>
                                    @if(Auth::check() || Auth::guard('deliverer')->check() || Auth::guard('customer')->check())
                                        {{-- @dd(Auth::guard('customer')->user()->id)
                                        <form method="POST" action="{{ route('cartStore') }}">
                                            @csrf
                                            <input type="hidden" name="food_id" value="{{ $food->id }}">

                                            <input type="hidden" name="auth_id" value="{{ Auth::guard('customer')->user()->id }}">

                                            <button type="submit" id="{{ $food->id }}" style="border: 0; background: #fff; font-size: 25px; font-weight: 500; border-radius: 50px;width: 50px; height: 50px"
                                            wire:click="addQty">
                                                +
                                            </button>
                                        </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @php
                        $i++;
                    @endphp
                @endforeach --}}
            </div>
        </div>
    </div>
</section>
