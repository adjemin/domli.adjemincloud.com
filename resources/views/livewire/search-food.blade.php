<div>
    <section class="pagesBanner col-12" style="background: url('{{ asset('img/8.jpg') }}'); background-size: cover; background-position: center;">
        <div class="slide-title row">
            <div class="container">
                <h2 class="col-12">
                {{$restaurant->name}}
                </h2>
                {{-- @livewire('test-comp') --}}
            </div>
        </div>
    </section>

    <section class="page-section container">
        <div id="nav-cat-scroll">
            <div class="form-container col-lg-12">
                <div class="row">
                    <form id="lis-input" class="col-7" action="/search/foods" method="post">
                        @csrf
                        {{-- <input type="hidden" name="resto" value="{{$restaurant->id}}" > --}}
                        <input type="text" name="title" id="myInput" placeholder="Seach a dish here . . ." class="writting" wire:model='query'>
                        <nav class="addBtn" id="addBtn"><i class="fas fa-search"></i></nav>
                    </form>
                    <div class="location-container col-5">
                        <div class="location-content">
                            <span class="location-pin"><img src="{{ asset('img/pin.svg') }}" alt="..." width="35" height="35"></span>
                                {{-- @php
                                    $reverse_localisation = json_decode(file_get_contents('https://nominatim.openstreetmap.org/reverse.php?lat='.$restaurant->lat.'&lon='.$restaurant->lng.'&format=jsonv2'), true);
                                @endphp
                                @if(!is_null($reverse_localisation))
                                    <h2>{{$reverse_localisation["name"] ?? $reverse_localisation["display_name"] ?? '' }}</h2>
                                    <p>{{ $reverse_localisation["display_name"][country] }}</p>
                                @endif --}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="form-container col-lg-12">
                <div class="row">
                    <form id="lis-input" method="post" action="/search/restaurants">
                        <input type="text" name="search" id="myInput" placeholder="Search a restaurant now" class="writting">
                        <input class="addBtn" type="submit" id="addBtn" value="Add">
                    </form>
                    <form method="post" action="/search/restaurants" id="lis-input" class="col-7">
                        @csrf
                        <input type="text" name="search" id="myInput" placeholder="Search a restaurant now" class="writting" value="{{$search ?? ''}}">
                        <input class="addBtn" type="submit" id="addBtn" value="<i class='fas fa-search'></i>">
                        <nav class="addBtn" id="addBtn"><i class="fas fa-search"></i></nav>
                    </form>
                    <div class="location-container col-5">
                        <div class="location-content">
                            <span class="location-pin"><img src="{{ asset('img/pin.svg') }}" alt="..." width="35" height="35"></span>
                            <span>
                                <h2>{{ $userCoordinates['city'] ?? '' }}</h2>
                                <p>{{ $userCoordinates['country_code'] ?? '' }}</p>
                            </span>
                        </div>
                    </div>
                    @yield('location')
                </div>
            </div> --}}
            <div class="col-lg-12 restaurant-categorie-list" id="CategorieMenu">
                {{-- @yield('restaurant-categorie-list') --}}
                <ul>
                    @php
                        $firstLink = 1;
                        // dd($_SERVER['REQUEST_URI']);
                    @endphp
                    @foreach($food_categories as $categories)
                        @if ($firstLink <= 1)
                            <li ><a href="#{{$categories->slug}}" class="hvr-buzz-out navigation__link active">{{$categories->title}}</a></li>
                        @else
                            <li ><a href="#{{$categories->slug}}" class="hvr-buzz-out navigation__link">{{$categories->title}}</a></li>
                        @endif

                        @php
                            $firstLink++;
                        @endphp
                    @endforeach
                    <!-- <li class="see-more-btn"><a href="#">See more</a></li> -->
                </ul>
            </div>
        </div>


        <div class="col-lg-12 restaurant-categorie-container"  id="{{$foods->slug}}">
            <div class="row dish-content">
                <div class="section-title col-md-12 col-lg-12 col-sm-12">
                    <h3>{{$food->title}}</h3>
                </div>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <div class="row" style="width: 100%;">
                    @php
                        $i = 1;
                    @endphp

                    @if(isset($searching_foods) && $searching_foods !== null)

                        @foreach ($searching_foods as $food)
                            <div class="card item-container dish-container col-md-3">
                                <div class="card-container row hvr-wobble-vertical">
                                    <div class="card-img col-md-12"
                                    style="background: url('{{ asset('img/'.$i.'.jpg') }}');
                                        background-size: cover;
                                        background-position: center;">

                                        <div class="card-body">
                                            <p class="price">
                                                {{ getPrice($food->price) }}
                                            </p>
                                            <a href="#" class="submitLink">
                                                <h5>{{ $food->title }}</h5>
                                                <p class="description">
                                                    @if (strlen($food->description) >= 90)
                                                        {{ substr($food->description, 0, -50) }}
                                                    @else
                                                        {{ $food->description }}
                                                    @endif

                                                </p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="hidden" data-id="{{ $food->uuid }}" class="btn btn-primary partModalBtn" data-toggle="modal" data-target="#exampleModal" style="padding: 0px; border: 0px; background: transparent;">
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                {{--<div class="modal-dialog" role="document">
                                    <div class="modal-content" style="border-radius: 15px;">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Customize your order</h5>
                                            <button type="button" class="close" id="closeModal" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @if(Auth::check() || Auth::guard('deliverer')->check() || Auth::guard('customer')->check())
                                            <div class="modal-body">

                                                <div class="row" style="margin-left: 0; margin-right: 0;">
                                                    <div class="food-img col-12 overflow-hidden mb-20" style="height: 300px; margin-bottom: 25px;">
                                                        <img src="{{ asset('img/'.$i.'.jpg') }}" style="max-width: 100%; height:auto;">
                                                    </div>

                                                    <div class="col-12 d-flex">
                                                        <button class="rounded-circle border-light increaseQty" style="font-size: 18px; margin-right: 20px;  border: 0; width: 40px; height: 40px;">+</button>

                                                        <nav style="font-size: 20px;" id="qtyNav"></nav>

                                                        <button class="rounded-circle border-light DecreaseQty" style="font-size: 18px; margin-left: 20px; border: 0; width: 40px; height: 40px;">-</button>
                                                    </div>

                                                    <div class="col-12 food-part-conatainer">
                                                        <div class="row">
                                                            <div class="part-container col-12">
                                                                <div class="row">
                                                                    <h3 class="col-12">Part title</h3>
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <input id="" type="checkbox" checked="checked" name="cke" class="col-1">
                                                                            <span class="checkmark-box"></span>
                                                                            <label class="col-11">Did you receive your order?</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input id="note-desc" type="text" name="note-desc" placeholder="leave a note" style="margin-top: 15px">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <form method="POST" action="{{ route('cartStore') }}" id="cartStoreForm">
                                                    @csrf
                                                    <input type="hidden" name="food_id" value="{{ $food->id }}">

                                                    <input type="hidden" name="auth_id" value="{{ Auth::guard('customer')->user()->id ?? '' }}">

                                                    <input type="hidden" class="form-control" id="OrderQty" name="OrderQty">

                                                    <input type="hidden" class="form-control" id="foodId" name="foodId" value="{{ $food->id }}">

                                                    <button type="submit" id="{{ $food->id }}" style="border: 0; background: #fff; font-size: 25px; font-weight: 500; border-radius: 50px; width: 50px; height: 50px">

                                                    </button>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn" id="addModalCart" style="border-radius: 50px; background: #E8C52A; padding: 10px 20px;">Add to cart</button>
                                            </div>
                                        @else
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="food-img col-12 overflow-hidden mb-20" style="height: 300px; margin-bottom: 25px;">
                                                        <img src="{{ asset('img/'.$i.'.jpg') }}" style="max-width: 100%; height:auto;">
                                                    </div>
                                                    <div class="col-12 d-flex">
                                                        <button class="rounded-circle border-light increaseQty" style="font-size: 18px; margin-right: 20px;  border: 0; width: 40px; height: 40px;">+</button>

                                                        <nav style="font-size: 20px;" id="qtyNav"></nav>

                                                        <button class="rounded-circle border-light DecreaseQty" style="font-size: 18px; margin-left: 20px; border: 0; width: 40px; height: 40px;">-</button>
                                                    </div>
                                                </div>
                                                <form method="POST" action="{{ route('createOrderNotAuth') }}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <input type="hidden" class="form-control" id="OrderQty" name="OrderQty">
                                                        <input type="hidden" class="form-control" id="foodId" name="foodId" value="{{ $food->id }}">
                                                    </div>
                                                    <button type="submit" hidden id="ModalSubmitBtn">

                                                    </button>
                                                    <div class="form-group">
                                                        <label for="message-text" class="col-form-label">Message:</label>
                                                        <textarea class="form-control" id="message-text"></textarea>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn sendModalFormBtn" style="border-radius: 50px; background: #E8C52A; padding: 10px 20px;">Buy now</button>
                                            </div>
                                        @endif
                                    </div>
                                </div>--}}
                            </div>
                            {{-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            ...
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            @php
                                $i++;
                            @endphp
                        @endforeach
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div>
                                <button type="button" class="btn" id="addModalCart" style="border-radius: 50px; background: #E8C52A; padding: 10px 20px;">Add to cart</button>
                            </div>
                        </div>
                    @else

                        @foreach ($foods as $food)
                            <div class="card item-container dish-container col-md-3">
                                <div class="card-container row hvr-wobble-vertical">
                                    <div class="card-img col-md-12"
                                    style="background: url('{{ asset('img/'.$i.'.jpg') }}');
                                        background-size: cover;
                                        background-position: center;">

                                        <div class="card-body">
                                            <p class="price">
                                                {{ getPrice($food->price) }}
                                            </p>
                                            <a href="#" class="submitLink">
                                                <h5>{{ $food->title }}</h5>
                                                <p class="description">
                                                    @if (strlen($food->description) >= 90)
                                                        {{ substr($food->description, 0, -50) }}
                                                    @else
                                                        {{ $food->description }}
                                                    @endif

                                                </p>
                                            </a>
                                            @if(Auth::check() || Auth::guard('deliverer')->check() || Auth::guard('customer')->check())
                                                {{-- @dd(Auth::guard('customer')->user()->id)--}}
                                                <form method="POST" action="{{ route('cartStore') }}">
                                                    @csrf
                                                    <input type="hidden" name="food_id" value="{{ $food->id }}">

                                                    <input type="hidden" name="auth_id" value="{{ Auth::guard('customer')->user()->id }}">

                                                    <button type="submit" id="{{ $food->id }}" style="border: 0; background: #fff; font-size: 25px; font-weight: 500; border-radius: 50px;width: 50px; height: 50px"
                                                    wire:click="addQty">
                                                        +
                                                    </button>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Button trigger modal -->
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
</div>

