<section class="Popular row">
	<div class="container">
		<div class="row">
			<div class="section-title col-md-12 col-lg-12 col-sm-12">
                <h3> {{__('header.popular_near')}} </h3>
                <a href="/restaurants?categories=all">{{__('header.see_more')}}</a>
            </div>
            {{--@for ($i = 1; $i <= 6; $i++)--}}

            @foreach($restaurants as $restaurant)
                <div class="Popular-img col-md-4 col-sm-6 hvr-wobble-skew">
                    <a href="{{ route('restaurant', $restaurant->uuid) }}">
                        <div class="block-img" style="background-image: url('{{ asset($restaurant->cover) }}'); background-size: cover; background-position: center;">
                        </div>
                        <div class="restaurant-name">{{ $restaurant->name }}</div>
                        <div class="item-title"></div>
                    </a>
                </div>
            @endforeach
		</div>
	</div>
</section>
