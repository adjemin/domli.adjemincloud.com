<div class="table-responsive-sm">
    <table class="table table-striped" id="deliveryAvailaibities-table">
        <thead>
            <tr>
                <th>Delivery Id</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Start Time</th>
        <th>End Time</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($deliveryAvailaibities as $deliveryAvailaibity)
            <tr>
                <td>{{ $deliveryAvailaibity->delivery_id }}</td>
            <td>{{ $deliveryAvailaibity->start_date }}</td>
            <td>{{ $deliveryAvailaibity->end_date }}</td>
            <td>{{ $deliveryAvailaibity->start_time }}</td>
            <td>{{ $deliveryAvailaibity->end_time }}</td>
                <td>
                    {!! Form::open(['route' => ['deliveryAvailaibities.destroy', $deliveryAvailaibity->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('deliveryAvailaibities.show', [$deliveryAvailaibity->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('deliveryAvailaibities.edit', [$deliveryAvailaibity->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>