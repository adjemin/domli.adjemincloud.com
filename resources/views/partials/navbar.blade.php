 <nav class="nav-index">
     <div class="container-fluid">
         <div class="navigation">
             <div class="logo">
                 <a href=""> <img src="icones/LOGO-DOMLInoir.png" alt="logo"></a>
             </div>
             <!-- ***** -->
             <div class="menu">
                 <div class="menu--list">
                     <ul>
                         <li>
                             <a href="{{ route('home.index') }}">Page d'accueil</a>
                         </li>
                         <!--  -->
                         <li class="menu--hover position-relative">
                             <a href="#">Devenir &nbsp; <i class="fa fa-caret-down"></i></a>

                             <ul>
                                 <a href="{{ route('connexion.index') }}">
                                     <li>Restaurant</li>
                                 </a>
                             </ul>
                         </li>

                         <li>
                             <a href="{{ route('variete.index') }}">Restaurants</a>
                         </li>

                         <li>
                             <a href="#">Nous Contacter</a>
                         </li>

                         <li>
                             <a href="#">A propos de nous?</a>
                         </li>
                     </ul>
                 </div>
                 <!-- ******* -->
                 <div class="menu--others">
                     <ul>
                         <li class="position-relative">
                             <a href="#">
                                 <i class="fas fa-shopping-basket fa-1x"></i>
                             </a>

                             <div class="panier--count">
                                 <span>30</span>
                             </div>
                         </li>
                         <!-- ** -->
                         <li class="menu--hover position-relative">
                             <a href="#">FR <i class="fa fa-caret-down"></i></a>

                             <ul>
                                 <a href="#">
                                     <li>lorem</li>
                                 </a>
                             </ul>
                         </li>

                         <li>
                             <a href="{{ route('connexion.index') }}">
                                 <button class="btn--site">Inscription/Connexion</button>
                             </a>
                         </li>

                         <li class="btn--resp buttonMenu">
                             <a href="#">
                                 <i class="fas fa-bars "></i>
                             </a>
                         </li>
                     </ul>
                 </div>
             </div>
         </div>
     </div>
     <!-- ****************** -->
     <!-- ********* -->
     <div class="menu--responsive">
         <i class="far fa-times-circle buttonMenuSortie"></i>

         <ul>
             <li>
                 <a href="#">Page d'accueil</a>
             </li>
             <!--  -->
             <li>
                 <a data-toggle="collapse" href="#collapseExample" aria-expanded="false"
                     aria-controls="collapseExample">Devenir &nbsp; <i class="fa fa-caret-down"></i></a>


                 <div class="collapse" id="collapseExample">
                     <div class="card card-body">
                         <ul>
                             <li>
                                 <a href="{{ route('connexion.index') }}">Restaurant</a>
                             </li>
                         </ul>
                     </div>
                 </div>
             </li>

             <li>
                 <a href="{{ route('variete.index') }}">Restaurants</a>
             </li>

             <li>
                 <a href="#">Nous Contacter</a>
             </li>

             <li>
                 <a href="#">A propos de nous?</a>
             </li>
         </ul>
     </div>
 </nav>
