<footer class="footer">
    <div class="container">
        <div class="row wow fadeInRight">
            <div class="col-md-4 col-lg-3 ">
                <h3>REJOIGNEZ-NOUS</h3>
                <ul>
                    <li><a href="#">Emploi</a></li>
                    <li><a href="#">Entreprises partenaires</a></li>
                    <li><a href="#">Courrier</a></li>
                    <li><a href="#">Entreprise Domli</a></li>
                </ul>
            </div>

            <!-- ****************** -->
            <div class="col-md-4 col-lg-3 ">
                <h3>AIDE</h3>
                <ul>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Nous contacter</a></li>
                </ul>
            </div>

            <!-- ****************** -->
            <div class="col-md-4 col-lg-3 ">
                <h3>LÉGAL</h3>
                <ul>
                    <li><a href="#">Termes et conditions</a></li>
                    <li><a href="#">Politique de confidentialité </a></li>
                    <li><a href="#">Politique des cookies</a></li>
                </ul>
            </div>

            <!-- ****************** -->
            <div class="col-md-4 col-lg-3 ">
                <h3>SUIVEZ-NOUS </h3>
                <ul>
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Twitter</a></li>
                    <li><a href="#">Instagram</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
