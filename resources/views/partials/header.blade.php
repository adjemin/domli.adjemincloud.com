<header class="header">
    <div class="container">
        <div class="header--text">
            <h1>Vous avez un petit creux ? Vous êtes au bon endroit ,</h1>
            <p>Entrez votre adresse pour trouver les <br> restaurants à proximité</p>

            <form class="form">
                <div class="group--form">
                    <i class="fas fa-map-marker-alt"></i>
                    <input type="text" placeholder="Entrez votre adresse">
                </div>
                <!-- ************ -->
                <button>
                    <i class="fas fa-location-arrow"></i>
                </button>
            </form>
        </div>
    </div>
</header>
