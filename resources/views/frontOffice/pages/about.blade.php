@extends('layouts.index')


@section('content')
    {{-- @include('frontOffice.pageParts.pagesHeader') --}}
    @include('customer.pageParts.pagesHeader')

    <section class="aboutSlide col-12">
        <div class="slide-title row">
            <div class="container">
                <a href="#" >
                    <h2 class="col-12"> {{__('header.about_us')}} </h2>
                </a>
            </div>
        </div>
    </section>
    <section class="about-page-section container">
        <div class="row align-items-center">
            <div class="col-lg-6 about-text">
                <div class="about-text-content">
                    <p>
                        {{__('header.domli_about')}}
                    </p>

                    <div class="my-2">
                        <p class="mb-0"><strong>{{__('header.customer')}}</strong></p>
                        <p>{{__('header.customer_about')}}</p>
                    </div>

                    <div class="my-2">
                        <p class="mb-0"><strong>{{__('header.restaurant')}}</strong></p>
                        <p>{{__('header.restaurant_about')}}</p>
                    </div>

                    <div class="my-2">
                        <p class="mb-0"><strong>{{__('header.delivery_man')}}</strong></p>
                        <p>{{__('header.delivery_man_about')}}
                        </p>
                    </div>
                </div>
                <a href="{{ Auth::check() || Auth::guard('deliverer')->check() || Auth::guard('customer')->check() ? route('customerLogin') : route('restaurants') }}" class="call-action-btn hvr-buzz-out">{{__('header.get_started')}}</a>
            </div>
            <div class="col-lg-6 about-img">
                <img src="{{ asset('img/about-img.jpg') }}" alt="..." class="hvr-bounce-out">
            </div>
        </div>
    </section>
@endsection
