@extends('layouts.index')

{{-- @include('frontOffice.customer.pageParts.pagesHeader') --}}
@section('extra-script')
    <script src="https://js.stripe.com/v3/"></script>
@endsection

@section('content')
    <section class="otp-page container">
        <div class="row content" style="min-height: auto;">
            <div class="form-container col-12">
                <div class="form-content">
                    <div class="tab-pane ">
                        <form id="customer-form" action="#">
                            <h3>Add payment info here</h3>
                            <div class="row">
                                <div id="card-element" class="col-12"></div>
                                <div id="card-errors" role="alert" class="col-12"></div>
                                <button type="submit" class="submit-btn col-12 hvr-wobble-vertical" id="checkout-button">
                                    Proceed to payment
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('extra-js')
    <script type="text/javascript">
        let stripe = Stripe('pk_test_51IAwvKDVFLc3spxtR0pTw7uMuqOKwGjDp99vQUEmI6zBsHKBSDW1s3319JYACm1W23qVwkrORajFdE5J298KqBTN004m6UIx8m');
        let elements = stripe.elements();
        let style = {
            base: {
                color: "#32325d",
                fontSmoothing: "antialiased",
                fontSize: "20px",
                lineHeight: "40px",
                "::placeHolder": {
                    color: "#aab7c4"
                }
            },
            invalid: {
                color: "#fa755a",
                iconColor: "#fa755a"
            }
        }

        let card = elements.create("card", { style: style });
        card.mount("#card-element");

        card.addEventListener('change', ({error}) => {
            const displayError = document.getElementById('card-errors');
            if (error) {
                displayError.textContent = error.message;
            }
            else {
                displayError.textContent = '';
            }
        });
        var checkoutButton = document.getElementById('checkout-button');

        checkoutButton.addEventListener('click', function(e) {

            e.preventDefault();
            checkoutButton.disabled = true;

            stripe.confirmCardPayment("{{ $clientSecret }}", {
                payment_method: {
                    card: card
                }
            }).then(function(result) {
                if (result.error) {
                    console.log(result.error.message);
                    checkoutButton.disabled = false;
                }
                else{
                    if (result.paymentIntent.status === 'succeeded') {
                        console.log(result.paymentIntent);
                    }
                }
            });
        });
    </script>
@endsection
