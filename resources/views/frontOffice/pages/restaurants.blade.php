@extends('frontOffice.pages.restaurantsLayouts.index')

@section('location')
    <div class="location-container col-sm-5 col-12">
        <div class="location-content">
            <span class="location-pin"><img src="{{ asset('img/pin.svg') }}" alt="..." width="35" height="35"></span>
            <span>
                <h2>{{ $userCoordinates['city'] }}</h2>
                <p>{{ $userCoordinates['country_code'] ?? $userCoordinates['region_code']  }}</p>
            </span>
        </div>
    </div>
@endsection

@section('restaurant-categorie-list')
    <ul>
        {{--<li><a href="#" class="hvr-buzz-out">Good food</a></li>
        <li><a href="#" class="hvr-buzz-out">Pizza</a></li>
        <li><a href="#" class="hvr-buzz-out">Burger</a></li>
        <li><a href="#" class="hvr-buzz-out">American</a></li>
        <li><a href="#" class="hvr-buzz-out">Suchi</a></li>
        <li><a href="#" class="hvr-buzz-out">Italian</a></li>
        <li><a href="#" class="hvr-buzz-out">French</a></li>

        <li><a href="#" class="hvr-buzz-out">Japanese</a></li>--}}
        @if($food_categories->count() == 1)
            <li><a href="/restaurants?categories={{$food_categories->first()->slug}}" class="hvr-buzz-out">{{$food_categories->first()->title}}</a></li>
        @else
            @foreach($food_categories as $categorie)
                <li><a href="/restaurants?categories={{$categorie->slug}}" class="hvr-buzz-out">{{$categorie->title}}</a></li>
            @endforeach
        @endif
        <!-- <li class="see-more-btn"><a href="#">See more</a></li> -->
    </ul>
@endsection

@section('restaurant-categorie-container')
    <div class="col-lg-12 restaurant-categorie-container">
        <div class="row">
            <div class="section-title col-12">
                <h3>{{$categories}}</h3>
            </div>
            <div class="col-12">
                <div class="row">
                    @forelse($restaurants as $restaurant)
                        <div class="card item-container col-md-6 hvr-wobble-vertical">
                            <div class="card-container row ">
                                <div class="card-img col-5"
                                style="background: url('{{ $restaurant->cover }}');
                                    background-size: cover;
                                    background-position: center;">
                                </div>
                                <div class="card-body col-7">
                                    {{-- <p class="note-promo">-100%</p> --}}
                                    <a href="{{ route('restaurant', $restaurant->uuid) }}">
                                        <p class="location">{{$restaurant->restaurant_region_name ?? ''}}</p>
                                        <h5>{{ $restaurant->name }}</h5>
                                        <p class="description">
                                            {{$restaurant->description ?? \Str::limit(__("header.lorem"),100)}}
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="justify-content-center align-content-center ">
                            <h3>{{__('header.no_result')}}</h3>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
    @if($others->count())
    <div class="col-lg-12 restaurant-categorie-container">

        <div class="row">
            <div class="section-title col-md-12 col-lg-12 col-sm-12">
                <h3> {{__('header.other')}} </h3>
            </div>
            <div class="row">
                @foreach($others as $restaurant)
                    <div class="card item-container col-md-6 hvr-wobble-vertical">
                        <div class="card-container row ">
                            <div class="card-img col-5"style="background: url('{{ $restaurant->cover }}'); background-size: cover;background-position: center;">
                            </div>
                            <div class="card-body col-7">
                                <a href="{{ route('restaurant', $restaurant->id) }}">
                                    <p class="location">{{ $restaurant->restaurant_region_name }}</p>
                                    <h5>{{ $restaurant->name }}</h5>
                                    <p class="description">
                                        {{$restaurant->description ?? \Str::limit(__("header.lorem"), 100)}}
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif
@endsection
