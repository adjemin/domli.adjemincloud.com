@extends('layouts.index')


@section('content')

    @include('customer.pageParts.pagesHeader')
    @include('frontOffice.pageParts.pageSlide')
    <section class="page-section container">
        <div class="form-container col-lg-12">
            <div class="row">
                {{--<form id="lis-input" method="post" action="/search/restaurants">
                    <input type="text" name="search" id="myInput" placeholder="Search a restaurant now" class="writting">
                    <input class="addBtn" type="submit" id="addBtn" value="Add">
                </form>--}}
                <form method="post" action="/search/restaurants" id="lis-input" class="col-sm-7 col-12">
                    @csrf
                    {{-- <input type="text" name="search" id="myInput" placeholder="Search a restaurant now" class="writting" value="{{$search ?? ''}}"> --}}
                     <div class="inputfield--ind">
                        <form>
                            <div class="inputfield--ind__location">
                                <img src="{{asset('img/location.svg') }}" alt="...">
                                <input type="text" name="" id="" placeholder="{{__('header.enter_address')}}" style="background:#ccc"/>
                            </div>

                            {{-- <button class="inputfield--ind__position">
                                <img src="{{asset('img/gps.svg') }}" alt="...">
                            </button> --}}
                        </form>
                    </div>
                    {{-- <input class="addBtn" type="submit" id="addBtn" value=""> --}}
                    {{-- <nav class="addBtn" id="addBtn"><i class="fas fa-search"></i></nav> --}}
                </form>
                @yield('location')
            </div>
        </div>
        <div class="col-lg-12 restaurant-categorie-list">
            @yield('restaurant-categorie-list')
        </div>

        @yield('restaurant-categorie-container')
    </section>
@endsection
