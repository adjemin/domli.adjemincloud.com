@extends('layouts.index')


@section('content')
    @include('customer.pageParts.pagesHeader')
    {{-- @dd($restaurant_categories) --}}
    <section class="pagesBanner col-12" style="background: url('{{ asset('img/8.jpg') }}'); background-size: cover; background-position: center;">

        <div class="slide-title row">
            <div class="container">
                <h2 class="col-12">{{ "Restaurant categories" }}</h2>
            </div>
        </div>
    </section>
    {{-- @include('frontOffice.pageParts.pageSlide') --}}
    @livewire('restaurant-categories-comp')
@endsection

@section('scriptRestaurantCategorie')
    <script>
        //smooth scrolling
        const links = document.querySelectorAll(".page-section .restaurant-categorie-list ul a");
        const menu = document.getElementById("CategorieMenu");

        const navigationLinks = document.getElementsByClassName("navigation__link");

        for (const link of links) {
            link.addEventListener("click", clickHandler);
        }

        function clickHandler(e) {
            e.preventDefault();
            const href = this.getAttribute("href");
            const offsetTop = document.querySelector(href).offsetTop - 180;

            scroll({
                top: offsetTop,
                behavior: "smooth"
            });
        }

        for (var i = 0; i < navigationLinks.length; i++) {
            navigationLinks[i].addEventListener("click", function() {
                var current = document.getElementsByClassName("active");
                current[0].className = current[0].className.replace(" active", "");
                this.className += " active";
                // console.log("ok");
            });
        }

        // Nav scroll
        // Do a nav scrool effect with simply js

        const navCatScroll = document.getElementById('nav-cat-scroll');
        const formContainer = document.getElementsByClassName('form-container');
        const categorieMenu = document.getElementById('CategorieMenu');
        // console.log(formContainer);

        // const headerContainer = document.getElementById("header-container");
        window.onscroll = function(){
            if (window.innerWidth >= 540) {
                if(document.documentElement.scrollTop > 600){
                    navCatScroll.style.background = "#fff";
                    navCatScroll.style.position = "fixed";
                    navCatScroll.style.top = "5px";
                    navCatScroll.style.right = "5%";
                    navCatScroll.style.left = "5%";
                    navCatScroll.style.marginLeft = "auto";
                    navCatScroll.style.marginRight = "auto";
                    navCatScroll.style.width = "90%";
                    navCatScroll.style.margin = "5% auto";
                    navCatScroll.style.marginTop = "0px";
                    navCatScroll.style.zIndex = "10000000";
                    navCatScroll.style.webkitBoxShadow = "0px 0px 15px 0px rgba(0, 0, 0, 0.16)";
                    navCatScroll.style.boxShadow = "0px 0px 15px 0px rgba(0, 0, 0, 0.16)";
                    navCatScroll.style.borderRadius = "15px";
                    // navCatScroll.style.background = "#222";
                    navCatScroll.style.transition = "all 0.3s";
                    // formContainer.style.height = "80px";
                    for (var i = 0; i < formContainer.length; i++){
                        formContainer[i].style.height = "80px";
                        formContainer[i].style.boxShadow = "none";
                        formContainer[i].style.webkitBoxShadow = "none";
                        formContainer[i].style.borderRadius = "0";
                        formContainer[i].style.marginTop = "0";
                    }
                    categorieMenu.style.marginTop = "10px";
                    categorieMenu.style.marginBottom = "10px";
                    categorieMenu.style.paddingBottom = "0px";
                }
                else{
                    navCatScroll.removeAttribute('style');
                    categorieMenu.removeAttribute('style');
                    for (var i = 0; i < formContainer.length; i++){
                        formContainer[i].removeAttribute('style');
                    }
                }
            }
        };
    </script>
@endsection
