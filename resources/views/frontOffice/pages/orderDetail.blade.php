@extends('layouts.index')

{{-- @include('frontOffice.popUp.checkout') --}}
@push('orderDetailStyle')
    <style>
        .ui-menu{
            background: #fff;
            list-style-type: none;
            list-style: none;
        }
        .ui-menu-item{
            line-height: 50px;
        }
        /* Reset Select */
        select.select {
            -webkit-appearance: none;
            -moz-appearance: none;
            -ms-appearance: none;
            appearance: none;
            outline: 0;
            box-shadow: none;
            border: 0 ;
            border-radius: 0;
            border-bottom: 2px solid rgba(0, 0, 0, 0.2);
            /* background: #2c3e50; */
            background-image: none;
        }
        /* Remove IE arrow */
        select.select::-ms-expand {
            display: none;
        }

        /* Custom Select */
        select.select {
            position: relative;
            display: flex;
            /* width: 20em; */
            /* height: 3em; */
            /* line-height: 3; */
            /* background: #2c3e50; */
            border-bottom: 2px solid rgba(0, 0, 0, 0.2);
            overflow: hidden;
            /* border-radius: .25em; */
        }
        select.select {
            flex: 1;
            /* padding: 0 .5em; */
            /* color: #fff; */
            cursor: pointer;
        }
        /* Arrow */
        select.select::after {
            content: '\25BC';
            position: absolute;
            top: 0;
            right: 0;
            padding: 0 1em;
            background: #34495e;
            cursor: pointer;
            pointer-events: none;
            -webkit-transition: .25s all ease;
            -o-transition: .25s all ease;
            transition: .25s all ease;
        }
        /* Transition */
        select.select:hover::after {
            color: #f39c12;
        }

        /* .mapboxgl-ctrl-geocoder {
            box-sizing: border-box !important;
            max-width: 100% !important;
            width: 100% !important;
            font-size: 15px !important;
            line-height: 20px !important;
            margin: 10px 5px !important;
        } */
    </style>
@endpush
@section('content')


    @include('customer.pageParts.pagesHeader')
    <section class="pagesSlide col-12">
        <div class="slide-title row">
            <div class="container">
                <a href="{{ route('customerCart') }}" >
                    <h2 class="col-12"><i class="fas fa-arrow-left"></i>{{ __('header.order_detail') }}</h2>
                </a>
            </div>
        </div>
    </section>

    <section class="page-section container">
        <div class="row">
            <div class="col-lg-7 order-detail-left">
                <div class="order-detail-left-content">
                    <form method="POST" action="{{ route('storeOrder') }}">
                    @csrf

                        <h3>Delivery adress</h3>
                        <div class="map-google"  id="map">
                            {{-- <img src="{{ asset('img/map.jpg') }}" alt="..."> --}}
                        </div>
                        <!-- <div id="geocCoder"></div> -->
                        <div class="order-input-card row">
                            <span class="col-sm-1 col-2">
                                <img src="{{ asset('img/pin.svg') }}" alt="...">
                            </span>
                            <input type="text" name="delivery-adress" id="delivery_adress" class="{{ $errors->has('delivery_adress') || $errors->has('lat') || $errors->has('lon') ? ' has-error' : '' }} form-control col-sm-11 col-10" placeholder="{{__('enter_address')}}" value="">
                            @if ($errors->has('delivery_adress') || $errors->has('lat') || $errors->has('lon'))
                                <span class="help-block" style="color:red;">
                                    {{ $errors->has('delivery_adress') ?? $errors->has('lat') ?? $errors->has('lon') }}
                                </span>
                            @endif
                            <input type="hidden" name="delivery_fees" id="delivery_fees">
                            <input type="hidden" name="kilometers" id="kilometers">
                            <input type="hidden" name="lat" id="lat_input">
                            <input type="hidden" name="lon" id="lon_input">
                            <div id="match-list"
                                style="margin: 10px auto; width: 300px;position:absolute; top:70px;right:0;">
                            </div>
                        </div>
                        <div class="order-input-card row">
                            <span class="col-sm-1 col-2">
                                <img src="{{ asset('img/future.svg') }}" alt="...">
                            </span>
                            <select name="date" id="delivery-dates" class="select form-control col-sm-7 col-6" >
                                @php
                                    $nowDate = date("Y-m-d");
                                    $datetime = new DateTime($nowDate);
                                    $day2 = new DateTime($nowDate);
                                    $day2->modify('+1 day');
                                    $day3 = new DateTime($nowDate);
                                    $day3->modify('+2 day');
                                    $day4 = new DateTime($nowDate);
                                    $day4->modify('+3 day');
                                    $day5 = new DateTime($nowDate);
                                    $day5->modify('+4 day');
                                    // $day3 = $datetime->modify('+2 day');
                                    // day4 = $datetime->modify('+3 day');
                                    // $day5 = $datetime->modify('+4 day');
                                    // echo $datetime->format('l, d, M, Y');
                                @endphp
                                <option value="now">
                                    To day
                                </option>
                                <option value="{{ $day2->format('l, d, M, Y') }}">
                                    {{ $day2->format('l, d, M, Y') }}
                                </option>
                                <option value="{{ $day3->format('l, d, M, Y') }}">
                                    {{ $day3->format('l, d, M, Y') }}
                                </option>
                                <option value="{{ $day4->format('l, d, M, Y') }}">
                                    {{ $day4->format('l, d, M, Y') }}
                                </option>
                                <option value="{{ $day5->format('l, d, M, Y') }}">
                                    {{ $day5->format('l, d, M, Y') }}
                                </option>
                            </select>
                            <select name="hour" id="delivery-dates" class="form-control select col-sm-4 col-4" >
                                @php
                                    $nowDate = date("Y-m-d");
                                    $datetime = new DateTime($nowDate);
                                    $day2 = new DateTime($nowDate);
                                    $day2->modify('+1 day');
                                    $day3 = new DateTime($nowDate);
                                    $day3->modify('+2 day');
                                    $day4 = new DateTime($nowDate);
                                    $day4->modify('+3 day');
                                    $day5 = new DateTime($nowDate);
                                    $day5->modify('+4 day');
                                    // $day3 = $datetime->modify('+2 day');
                                    // day4 = $datetime->modify('+3 day');
                                    // $day5 = $datetime->modify('+4 day');
                                    // echo $datetime->format('l, d, M, Y');
                                @endphp
                                <option value="as-soon-as-possible">
                                    {{__('header.as_sound')}}
                                </option>
                                <option value="8h-10h">
                                    8h - 10h
                                </option>
                                <option value="10h-12h">
                                    10h - 12h
                                </option>
                                <option value="12h-14h">
                                    12h - 14h
                                </option>
                            </select>
                            {{-- <input type="text" name="hour" id="delivery-adress" class="form-control col-5" placeholder="15h - 16h" style="border-left: 2px solid rgba(0, 0, 0, 0.2);"> --}}
                        </div>
                        <h4> {{__('header.payment_info')}}  </h4>
                        <div class="order-input-card row">
                            <span class="col-sm-1 col-2">
                                <img src="{{ asset('img/card.svg') }}" alt="...">
                            </span>
                            {{-- <input type="text" name="delivery-adress" id="delivery-adress" class="form-control col-10" placeholder="Add a payment method "> --}}
                            <select name="checkout" id="delivery-dates" class="form-control select col-11" >
                                <option value="add-credit-card" selected >
                                    {{__('header.credit_card')}}
                                </option>
                                <option value="pay-on-delivery">
                                    {{__('header.delivery_pay')}}
                                </option>
                            </select>
                        </div>
                        {{--<h4>Promo code</h4>
                        <div class="order-input-card row">
                            <span class="col-1">
                                <img src="{{ asset('img/promo.svg') }}" alt="..." width="35" height="35">
                            </span>
                            <input type="text" name="coupon" id="delivery-adress" class="form-control col-11" placeholder="Are you a promo code ? ">
                        </div>--}}
                        <h4> contact</h4>
                        <div class="order-input-card row" style="padding-left: 15px;">
                            {{-- <span class="col-1">
                                <img src="{{ asset('img/phone-call.svg') }}" alt="..." width="35" height="35">
                            </span> --}}
                            <input type="tel" name="phone_number" class="form-control col-5 " id="phone_number">

                            <span id="valid-msgPhoneNumber" class="hide"></span>
                            <span id="error-msgPhoneNumber" class="hide" style="color:red;"></span>
                            {{-- <span id="valid-msgPhoneNumber" class="hide"></span> --}}
                            {{-- <span id="error-msgPhoneNumber" class="hide" style="color:red;"></span> --}}
                            {{-- <input type="text" name="delivery-adress" id="delivery-adress" class="form-control col-10" placeholder="Are you a promo code ? "> --}}
                        </div>

                        {{--<input type="hidden" id="delivery_fees_input">
                        <input type="hidden" id="delivery-adress_input" value="{{$userCoordinates['city'] ?? 'Abidjan'}}">--}}

                        <button type="submit" class="submit-butn butn-bg hvr-radial-out "><span>{{ __('header.confirm_pay') }}</span></button>
                    </form>
                </div>
            </div>
            <div class="col-lg-5 order-detail-right">
                <div class="order-detail-right-content row">
                    <h3>{{__('header.order_detail')}}</h3>
                    {{--@if(Auth::check() || Auth::guard('deliverer')->check() || Auth::guard('customer')->check())--}}
                        <table class="info-table col-12" celspacing="0" cellpadding="10" border="0">
                            <tr class="Items">
                                <td>
                                    {{__('header.items_total')}}  ({{ Cart::count() }} {{__('header.items')}}) :
                                </td>
                                <td>
                                   {{ Cart::subtotal() }}
                                </td>
                            </tr>
                            <tr class="store">
                                <td>
                                    {{__('header.distance')}} :
                                </td>
                                <td id="distance_delivery">
                                    0 KM
                                </td>
                            </tr>
                            <tr class="store">
                                <td>
                                    {{__('header.delivery')}} :
                                </td>
                                <td id="delivery_fees_table">
                                    0
                                </td>
                            </tr>
                            <tr class="delivery">
                                <td>
                                    {{__('header.tax')}}:
                                </td>
                                <td>
                                    {{ Cart::tax() }}
                                </td>
                            </tr>
                            <tr class="total">
                                <td>
                                    {{ __('header.total_discount') }} :
                                </td>
                                <td>
                                    @php
                                        $discount = collect(Cart::getConditions())->map(function($condition){
                                            return floatval($condition->parsedRawValue) ?? 0;
                                        })->sum();
                                    @endphp
                                    {{ $discount }}
                                </td>
                            </tr>
                            <tr class="total-pay">
                                <td>
                                    {{ __('header.total_pay')}} :
                                </td>
                                <td class="price" id="total_price_pay">
                                    {{ Cart::total() }}
                                </td>
                            </tr>
                        </table>
                    {{--@else
                        <table class="info-table col-12" celspacing="0" cellpadding="10" border="0">
                            <tr class="Items">
                                <td>
                                    Items total ({{ $foodqty }} items) :
                                </td>
                                <td>
                                    @php
                                        $itemsPrice = $food->price * $foodqty;
                                    @endphp
                                    {{ $itemsPrice }}
                                </td>
                            </tr>
                            <tr class="store">
                                <td>
                                    Distance :
                                </td>
                                <td id="distance_delivery">
                                    0
                                </td>
                            </tr>
                            <tr class="store">
                                <td>
                                    Delivery :
                                </td>
                                <td id="delivery_fees">
                                    0
                                </td>
                            </tr>
                            <tr class="delivery">
                                <td>
                                    Tax :
                                </td>
                                <td>
                                    @php
                                        $tax = ($itemsPrice * 41) / 100;
                                    @endphp
                                    {{ getPrice($tax) }}
                                </td>
                            </tr>
                            <tr class="total">
                                <td>
                                    Total discount :
                                </td>
                                <td>--}}
                                    {{-- @php --}}

                                    {{-- // @endphp --}}
                                    {{--    {{ getPrice(($itemsPrice)) }}
                                </td>
                            </tr>
                            <tr class="total-pay">
                                <td>
                                    Total pay :
                                </td>
                                <td class="price">
                                    {{ getPrice(($food->price * $foodqty) + ($foodqty * 6)) }}
                                </td>
                            </tr>
                        </table>
                    @endif--}}
                </div>
            </div>
        </div>
    </section>
@endsection

@push('order-script')
    <!-- Pour le numéro de téléphone -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script> -->
    <script src="{{ asset('js/intlTelInput.js') }}"></script>
    <script src="{{ asset('js/utils.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                event.preventDefault();
                return false;
                }
            });
        })
        var input = document.querySelector("#phone_number"),
        errorMsg = document.querySelector("#error-msgPhoneNumber"),
        validMsg = document.querySelector("#valid-msgPhoneNumber"),
        dialCode = document.querySelector('#dial_code'),
        countryCode = document.querySelector('#country_code');

        // Error messages based on the code returned from getValidationError
        var errorMap = [ "", "Invalid country code", "Too short", "Too long", "Invalid number"];

        // Initialise plugin
        var intl = window.intlTelInput(input, {
            initialCountry: "ca",
            utilsScript: "js/utils.js",
            geoIpLookup: function(success, failure) {
                $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                    countryCode.value = (resp && resp.country) ? resp.country : "";
                    success(countryCode);
                });
            },

        });

        var reset = function() {
            input.classList.remove("error");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };

        // Validate on blur event
        input.addEventListener('blur', function() {
            reset();
            if(input.value.trim()){
                if(intl.isValidNumber()){
                    validMsg.classList.remove("hide");
                    validMsg.innerHTML = "✓ Valid";
                }else{
                    input.classList.add("error");
                    var errorCode = intl.getValidationError();
                    errorMsg.innerHTML = errorMap[errorCode];
                    errorMsg.classList.remove("hide");
                }
            }
        });

        // Reset on keyup/change event
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);
    </script>
@endpush
