@extends('layouts.index')

{{-- Pop up for quantity --}}
{{-- @include('frontOffice.pages.dishPopUp') --}}
{{-- @section('meta-csrf-token')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection --}}

@section('content')

<style>
input[type="number"] {
  -webkit-appearance: textfield;
  -moz-appearance: textfield;
  appearance: textfield;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
}

.number-input {
  border: 0;
  display: inline-flex;
}

.number-input,
.number-input * {
  box-sizing: border-box;
}

.number-input button {
  outline:none;
  -webkit-appearance: none;
  /* background-color: transparent; */
  border: none;
  align-items: center;
  justify-content: center;
  width: 3rem;
  height: 3rem;
  border-radius: 50%;
  cursor: pointer;
  margin: 0;
  position: relative;
}

.number-input button:before,
.number-input button:after {
  display: inline-block;
  position: absolute;
  content: '';
  width: 1rem;
  height: 2px;
  background-color: #212121;
  transform: translate(-50%, -50%);
}
.number-input button.plus:after {
  transform: translate(-50%, -50%) rotate(90deg);
}

.number-input input[type=number] {
  font-family: sans-serif;
  max-width: 5rem;
  padding: .5rem;
  border: 0;
  /* border-width: 0 2px; */
  font-size: 18px;
  height: 3rem;
  font-weight: 500;
  text-align: center;
  opacity: 0.7;
}
</style>

    @include('customer.pageParts.pagesHeader')
    {{-- @livewire('restaurant-content') --}}
    @include('frontOffice.pageParts.search-food')

@endsection

@push('scripts')
    <script>
        let partModalBtn = document.getElementsByClassName('partModalBtn');
        let foodCardLink = document.getElementsByClassName('food-card-link');
        const modalCard = document.getElementsByClassName('modal');
        let qty = 1;
        // console.log(modalCard);

        // let foodSubmitLinks = document.getElementsByClassName('submitLink');
        // let saveModalFormBtn = document.getElementsByClassName('sendModalFormBtn');
        let ModalSubmitBtn = document.getElementById('ModalSubmitBtn');
        let addModalCart = document.getElementById('addModalCart');
        let foodInfoSubmitBtn = document.getElementById('ModalSubmitBtn');
        let submitBtn = document.querySelectorAll('input.sendModalFormBtn[type="submit"]');
        let  = document.querySelectorAll('input.sendModalFormBtn[type="submit"]');

        for (let i = 0; i < submitBtn.length; i++){
            console.log(submitBtn[i])
            submitBtn[i].addEventListener('click', function(e){
                e.preventDefault()
                var total_price = document.querySelector(`input[name="total_price"][data-id="${this.dataset.id}"]`)
                if(parseFloat(total_price.value) != 0){
                    document.getElementById(`form-${this.dataset.id}`).submit();
                    submitBtn[i].disabled = false;
                }else{
                    submitBtn[i].disabled = true;
                }
            });
        }

        for (let i = 0; i < partModalBtn.length; i++) {
            foodCardLink[i].addEventListener('click', function(event){
                event.preventDefault();
                partModalBtn[i].click();

                let qtyInput = document.querySelector('input[type="number"]');
                const modalCard = document.querySelector('.modal');
                let OrderPrice = document.querySelector('#modal-total-price');
                // let qtyBtns = document.querySelectorAll('.number-input .qtyBtn');







                // console.log(qtyBtn);

                // for (let j = 0; j < qtyBtns.length; j++) {
                //     qtyBtns[i].addEventListener('click', function(){
                //         console.log(OrderPrice);
                //     });
                // }
                // OrderPrice.innerText = (qtyInput.value * parseInt(OrderPrice.innerText) ) / 100;
                // console.log(OrderPrice.innerText);

                qtyInput.addEventListener('change', function(event){

                    event.preventDefault();
                    const modalCard = document.querySelector('.modal');
                    let OrderPrice = document.querySelector('#modal-total-price');

                    OrderPrice.innerText = (qtyInput.value * parseInt(OrderPrice.innerText) ) / 100;
                    console.log(OrderPrice.innerText);
                });
                // //Increase decrease action
                // // var increaseQty = .querySelectorAll('button.increaseQty');
                // // var DecreaseQty = document.querySelectorAll('button.DecreaseQty');
                // var SubPrices = document.querySelectorAll('p.price');
                // var price = document.querySelector('global-price');

                // // console.log(partModalBtn[i].dataset.id);
                // // partModalBtn[i].addEventListener('click',function(e){
                // // e.preventDefault();
                // let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                // let url = "/get/food/data";
                // let _data = {
                //     "id": partModalBtn[i].dataset.id
                // };
            });
        }


        for (let i = 0; i < modalCard.length; i++) {
            const form = modalCard[i].querySelector('.food_content_form');
            let qty = modalCard[i].querySelector('input[type="number"]');


            // increaseQty = modalCard[i].querySelector('.increaseQty');
            // decreaseQty = modalCard[i].querySelector('.decreaseQty');
            // qtyNav = modalCard[i].querySelector('#qtyNav');


            // console.log(increaseQty);

            // increaseQty.addEventListener('click', function(e){
            //     e.preventDefault();
            //     increaseQty.nextElementSibling.innertext = 3;
            // });


            // let form_data = Array.from(form).reduce((acc, input) => ({...acc, [input.name]: input.value}), {})



            // console.log(form_data);
        }
        // for (let l = 0; l < DecreaseQty.length; l++) {
        //     DecreaseQty[l].previousElementSibling.addEventListener('change', function(e){
        //         e.preventDefault();
        //         let data_name = DecreaseQty[l].dataset.name == 'decrease-global' ? 'global' : slugify(DecreaseQty[l].dataset.name);

        //         if(data_name == "decrease-global"){
        //             if(DecreaseQty[l].previousElementSibling.innerText == 1){
        //                 DecreaseQty[l].setAttribute('disabled', true);
        //             }else{
        //                 DecreaseQty[l].setAttribute('disabled', false);
        //             }
        //         }else{
        //             if(DecreaseQty[l].previousElementSibling.innerText == 0){
        //                 DecreaseQty[l].setAttribute('disabled', true);
        //             }else{
        //                 DecreaseQty[l].setAttribute('disabled', false);
        //             }
        //         }
        //     });
        // }
                // fetch(
                //     url,
                //     {
                //         headers: {
                //             "Content-type": "application/json;charset=UTF-8",
                //             "Accept": "application/json, text-plain, */*",
                //             "X-Requested-with": "XMLHttpRequest",
                //             "X-CSRF-TOKEN": token
                //         },
                //         method: "post",
                //         body:  JSON.stringify(_data)
                //     }
                // )
                // .then(res => res.json())
                // .then(data => {
                //     let modal = document.querySelector(`#a${partModalBtn[i].dataset.id}`)
                //     let foodPartCard = modal.querySelector('.part-container');
                //     console.log(data);
                //     for (let j = 0; j < data.length; j++) {
                //         console.log(data[i]);
                //     }

                // })
                //         document.getElementById('exampleModal').innerHTML = data.data;
                //         // var defaultPrice = ;
                //         var increaseQty = document.querySelectorAll('button.increaseQty');
                //         var DecreaseQty = document.querySelectorAll('button.DecreaseQty');
                //         var SubPrices = document.querySelectorAll('p.price');
                //         var price = document.querySelector('global-price');

                //         document.getElementById('closeModal').addEventListener('click', function(e){
                //             e.preventDefault();
                //             $('#exampleModal').modal('hide');
                //         });

                //         document.getElementById('allergy').addEventListener('change', (event) => {
                //             if (event.currentTarget.checked) {
                //                 document.getElementById('allergy_div').style.display = "block"
                //             } else {
                //                 document.getElementById('allergy_div').style.display = "none"
                //             }
                //         })

                //         for (let index = 0; index < DecreaseQty.length; index++) {
                //             // console.log(DecreaseQty[index]);
                //             // console.log(increaseQty[index]);
                //             // console.log('/**************** *//');
                //         }

                //         for (let l = 0; l < DecreaseQty.length; l++) {
                //             DecreaseQty[l].previousElementSibling.addEventListener('change', function(e){
                //                 e.preventDefault();
                //                 let data_name = DecreaseQty[l].dataset.name == 'decrease-global' ? 'global' : slugify(DecreaseQty[l].dataset.name);
                //                 if(data_name == "decrease-global"){
                //                     if(DecreaseQty[l].previousElementSibling.innerText == 1){
                //                         DecreaseQty[l].setAttribute('disabled', true);
                //                     }else{
                //                         DecreaseQty[l].setAttribute('disabled', false);
                //                     }
                //                 }else{
                //                     if(DecreaseQty[l].previousElementSibling.innerText == 0){
                //                         DecreaseQty[l].setAttribute('disabled', true);
                //                     }else{
                //                         DecreaseQty[l].setAttribute('disabled', false);
                //                     }
                //                 }
                //             });
                //             // DecreaseQty[l].previousElementSibling.innerText = 1;
                //             DecreaseQty[l].addEventListener('click', function(e){
                //                 e.preventDefault();
                //                 let qty = parseInt(DecreaseQty[l].previousElementSibling.innerText);
                //                 let data_name = DecreaseQty[l].dataset.name == 'decrease-global' ? 'global' : slugify(DecreaseQty[l].dataset.name);
                //                 let global_price = document.querySelector('div[data-name="global-price"]');

                //                 if(data_name == "decrease-global"){
                //                     if (qty > 1) {
                //                         qty -= 1;
                //                     }
                //                 }else{
                //                     if (qty > 0) {
                //                         qty -= 1;
                //                     }
                //                 }
                //                 DecreaseQty[l].previousElementSibling.innerText = qty;
                //                 if(data_name == 'global'){
                //                     // global_price.innerText = parseFloat(global_price.innerText) * qty
                //                     let spans = document.querySelectorAll(`span.price`)
                //                     // let navs = document.querySelectorAll(`nav`)
                //                     let sum = 0
                //                     for (let index = 0; index < spans.length; index++) {
                //                         sum += parseFloat(spans[index].innerText);
                //                     }

                //                     global_price.innerText = sum * qty;
                //                     document.getElementById('total_price').value = sum * qty;
                //                 }else{
                //                     let span_price = document.querySelector(`span.price[data-name='${data_name}']`)
                //                     // console.log(span_price)
                //                     span_price.innerText = document.querySelector(`nav[data-name="${data_name}"]`).dataset.default * qty

                //                     let spans = document.querySelectorAll(`span.price`)
                //                     // let navs = document.querySelectorAll(`nav`)
                //                     let sum = 0
                //                     for (let index = 0; index < spans.length; index++) {
                //                         sum += parseFloat(spans[index].innerText);
                //                     }

                //                     global_price.innerText = sum * document.querySelector('input[data-name="global"]').value
                //                     document.getElementById('total_price').value = sum * qty;
                //                 }

                //                 document.querySelector(`input[data-name='${data_name}']`).value = qty;

                //             });
                //         }

                //         for (let l = 0; l < increaseQty.length; l++) {
                //             increaseQty[l].nextElementSibling.addEventListener('change', function(e){
                //                 e.preventDefault();
                //                 if(increaseQty[l].nextElementSibling.innerText == 6){
                //                     increaseQty[l].setAttribute('disabled', true);
                //                 }
                //                 else{
                //                     increaseQty[l].setAttribute('disabled', false);
                //                 }
                //             });
                //             increaseQty[l].nextElementSibling.innerText = 1;
                //             increaseQty[l].addEventListener('click', function(e){
                //                 e.preventDefault();
                //                 let qty = parseInt(increaseQty[l].nextElementSibling.innerText);
                //                 if (qty < 6) {
                //                     qty += 1;
                //                 }

                //                 increaseQty[l].nextElementSibling.innerText = qty;
                //                 let data_name = increaseQty[l].dataset.name == 'increase-global' ? 'global' : slugify(increaseQty[l].dataset.name);
                //                 // console.log(data_name)
                //                 document.querySelector(`input[data-name='${data_name}']`).value = qty;
                //                 let global_price = document.querySelector('div[data-name="global-price"]');
                //                 if(data_name == 'global'){
                //                     let sum = 0
                //                     if(parseFloat(global_price.innerHTML) == 0){
                //                         // global_price.innerText = parseFloat(global_price.innerText) * qty
                //                         let spans = document.querySelectorAll(`span.price`)
                //                         // let navs = document.querySelectorAll(`nav`)
                //                         for (let index = 0; index < spans.length; index++) {
                //                             sum += parseFloat(spans[index].innerText);
                //                         }

                //                         global_price.innerText = sum * qty;
                //                     }else{
                //                         sum = global_price.innerText
                //                     }

                //                     document.getElementById('total_price').value = sum * qty;
                //                 }else{
                //                     let span_price = document.querySelector(`span.price[data-name='${data_name}']`)
                //                     // console.log(span_price)
                //                     span_price.innerText = document.querySelector(`nav[data-name="${data_name}"]`).dataset.default * qty

                //                     let spans = document.querySelectorAll(`span.price`)
                //                     // let navs = document.querySelectorAll(`nav`)
                //                     let sum = 0
                //                     for (let index = 0; index < spans.length; index++) {
                //                         sum += parseFloat(spans[index].innerText);
                //                     }
                //                     global_price.innerText = sum * document.querySelector('input[data-name="global"]').value;
                //                     document.getElementById('total_price').value = sum * qty;
                //                 }
                //                 // if (qtyValue >= 1 && qtyValue < 6) {
                //                 //     qtyValue += 1;
                //                 // }
                //                 // increaseQty[l].nextElementSibling.innerText = qtyValue;
                //                 // OrderQty.value = qtyValue;
                //             });
                //         }

                //         // document.getElementById('cartCount').innerText = data.cartCount;
                //         // document.getElementById('closeModal').click();

                //         // if (data.message === 'success') {
                //         //     this.removeAttribute('disabled');
                //         //     Swal.fire('Food has added to cart!');
                //         // }
                //         // else{
                //         //     Swal.fire('Oops...', 'The dish has already been added to the cart!', 'error');
                //         // }
                //     }).catch(err => console.log('Request Failed', err));
                // });

        // var inputqty = document.getElementById('OrderQty');
        // console.log(token);
        // let qtyValue = 1;

        // inputqty.value = qtyValue;
        // Modal form submit action
        // for (let i = 0; i < foodSubmitLinks.length; i++) {
        //     foodSubmitLinks[i].addEventListener('click',function (ev) {
        //        ev.preventDefault();

        //         for (let j = 0; j < partModalBtn.length; j++) {
        //             partModalBtn[j].click();
        //         }

        //         for (let k = 0; k < saveModalFormBtn.length; k++) {
        //             saveModalFormBtn[k].addEventListener('click', function(e){
        //                 e.preventDefault();
        //                 ModalSubmitBtn.click();
        //             })
        //         }
        //     });

        // }

        // increaseQty script


        // DecreaseQty script


        // Update qty cart

        // // for (let i = 0; i < addModalCart.length; i++) {
        // addModalCart.addEventListener('click', function(e){

        //     e.preventDefault();
        //     this.setAttribute('disabled', true);
        //     const cartStoreForm = document.getElementById('cartStoreForm');
        //     let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        //     let url = cartStoreForm.action;
        //     let _data = {
        //         food_id: document.querySelector('input[name="food_id"]').value,
        //         auth_id: document.querySelector('input[name="auth_id"]').value,
        //         OrderQty: document.querySelector('input[name="OrderQty"]').value
        //     }
        //     // console.log(data);
        //     // GET Request.
        //     // var formData = new FormData();
        //     // formData.append("title", myInput.value)
        //     // formData.append("_token", document.querySelector('input[name="_token"]').value)

        //     fetch(
        //         url,
        //         {
        //             headers: {
        //                 "Content-type": "application/json;charset=UTF-8",
        //                 "Accept": "application/json, text-plain, */*",
        //                 "X-Requested-with": "XMLHttpRequest",
        //                 "X-CSRF-TOKEN": token
        //             },
        //             method: "post",
        //             body:  JSON.stringify(_data)
        //         })
        //     .then(res => res.json())
        //     .then(data => {
        //         // data = JSON.parse(data);
        //         // console.log(data);
        //         document.getElementById('cartCount').innerText = data.cartCount;
        //         document.getElementById('closeModal').click();
        //         // console.log(document.getElementById('closeModal'));

        //         if (data.message === 'success') {
        //             this.removeAttribute('disabled');
        //             Swal.fire('Food has added to cart!');

        //         }
        //         else{
        //             Swal.fire('Oops...', 'The dish has already been added to the cart!', 'error');
        //         }
        //         // toast('Success Toast','success');
        //         // alert().success('SuccessAlert','Lorem ipsum dolor sit amet.');

        //         // location.reload;
        //     })
        //     .catch(err => console.log('Request Failed', err));
        //     //     .then(response => response.json())  // convert to json
        //     //     .then(json => console.log(json))    //print data to console
        //     //     .catch(err => console.log('Request Failed', err)); // Catch errors
        // });
        // // }














        // document.getElementById("addBtn").addEventListener('click',function(e){
        //     e.preventDefault();
        //     document.getElementById("lis-input").submit();
        // });

        // function slugify(string) {
        //     return string
        //         .toString()
        //         .trim()
        //         .toLowerCase()
        //         .replace(/\s+/g, "-")
        //         .replace(/[^\w\-]+/g, "")
        //         .replace(/\-\-+/g, "-")
        //         .replace(/^-+/, "")
        //         .replace(/-+$/, "");
        // }

    </script>


    <script>
        //smooth scrolling
        const links = document.querySelectorAll(".page-section .restaurant-categorie-list ul a");
        const menu = document.getElementById("CategorieMenu");

        const navigationLinks = document.getElementsByClassName("navigation__link");

        for (const link of links) {
            link.addEventListener("click", clickHandler);
        }

        function clickHandler(e) {
            e.preventDefault();
            const href = this.getAttribute("href");
            const offsetTop = document.querySelector(href).offsetTop - 180;

            scroll({
                top: offsetTop,
                behavior: "smooth"
            });
        }

        for (var i = 0; i < navigationLinks.length; i++) {
            navigationLinks[i].addEventListener("click", function() {
                var current = document.getElementsByClassName("active");
                current[0].className = current[0].className.replace(" active", "");
                this.className += " active";
            });
        }

        // Nav scroll
        // Do a nav scrool effect with simply js

        const navCatScroll = document.getElementById('nav-cat-scroll');
        const formContainer = document.getElementsByClassName('form-container');
        const categorieMenu = document.getElementById('CategorieMenu');

        // const headerContainer = document.getElementById("header-container");
            window.onscroll = function(){
                if (window.innerWidth >= 540) {
                    if(document.documentElement.scrollTop > 600){
                        navCatScroll.style.background = "#fff";
                        navCatScroll.style.position = "fixed";
                        navCatScroll.style.top = "5px";
                        navCatScroll.style.right = "5%";
                        navCatScroll.style.left = "5%";
                        navCatScroll.style.marginLeft = "auto";
                        navCatScroll.style.marginRight = "auto";
                        navCatScroll.style.width = "90%";
                        navCatScroll.style.margin = "5% auto";
                        navCatScroll.style.marginTop = "0px";
                        navCatScroll.style.zIndex = "100";
                        navCatScroll.style.webkitBoxShadow = "0px 0px 15px 0px rgba(0, 0, 0, 0.16)";
                        navCatScroll.style.boxShadow = "0px 0px 15px 0px rgba(0, 0, 0, 0.16)";
                        navCatScroll.style.borderRadius = "15px";
                        navCatScroll.style.transition = "all 0.3s";
                        for (var i = 0; i < formContainer.length; i++){
                            formContainer[i].style.height = "80px";
                            formContainer[i].style.boxShadow = "none";
                            formContainer[i].style.webkitBoxShadow = "none";
                            formContainer[i].style.borderRadius = "0";
                            formContainer[i].style.marginTop = "0";
                        }
                        categorieMenu.style.marginTop = "10px";
                        categorieMenu.style.marginBottom = "10px";
                        categorieMenu.style.paddingBottom = "0px";
                    }
                    else{
                        navCatScroll.removeAttribute('style');
                        categorieMenu.removeAttribute('style');
                        for (var i = 0; i < formContainer.length; i++){
                            formContainer[i].removeAttribute('style');
                        }
                    }
                }
            };

        function showDiv(e){
            console.log(e.dataset)
            var div = document.querySelector(`.allergy_div[data-id="${e.dataset.id}"]`);
            console.log(div)
            div.style.display = document.querySelector(`input[data-id="${e.dataset.id}"][name='allergie_check']`).checked ? "block" : "none";
        }

        function changeQty(e){
            console.log(e)
            if(e.value < e.min){
                e.value = e.min
            }else if(e.value > e.max){
                e.value = e.max
            }else{
                e.value = e.value
            }

            currentPrice(e)
            document.querySelector(`input[name="order_qty"][data-id="${e.dataset.id}"]`).value=e.value;
        }

        function setUp(e){
            var input = e.parentNode.querySelector('input[type=number]')
            if(input.value != input.max){
                input.value =  parseInt(input.value) + 1;
                document.querySelector(`input[name="order_qty"][data-id="${e.dataset.id}"]`).value = parseInt(input.value);
            }
            currentPrice(e)
        }

        function setDown(e){
            var input = e.parentNode.querySelector('input[type=number]')
            if(input.value != input.min){
                input.value =  parseInt(input.value) - 1;
                document.querySelector(`input[name="order_qty"][data-id="${e.dataset.id}"]`).value = parseInt(input.value);
            }
            currentPrice(e)
        }

        function currentPrice(e){
            console.log(e);
            var id = (e.tagName == 'input') ? e.dataset.id : e.getAttribute('data-id');
            var sum = 0;
            var total_price = document.querySelector(`input[name="total_price"][data-id="${id}"]`)
            var submit = document.querySelector(`input[type='submit'][data-id="${id}"]`)
            var span_price = document.querySelector(`span[data-id="${id}"]`)
            var price = document.querySelectorAll(`input[data-price][data-id="${id}"]:checked`)
            var base_price = document.querySelector(`input[type="submit"][data-id="${id}"][data-price]`)

            for(let i=0; i < price.length; i++){
                sum += parseFloat(price[i].dataset.price);
            }

            sum +=  parseFloat(base_price.dataset.price)
            span_price.innerText = parseFloat(sum * document.querySelector(`input[name="order_qty"][data-id="${id}"]`).value).toFixed(2)
            total_price.value = parseFloat(span_price.innerText).toFixed(2)

            if(parseFloat(total_price.value) > 0){
                if(submit.hasAttribute('disabled')){
                    submit.removeAttribute("disabled");
                }
            }else{
                submit.setAttribute("disabled", "")
            }
        }
        // currentPrice()
    </script>
@endpush
