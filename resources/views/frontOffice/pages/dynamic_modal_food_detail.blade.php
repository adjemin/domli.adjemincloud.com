






































<div class="modal-dialog" role="document">
    @csrf
    <input type="hidden" name="food_id" value="{{ $food['uuid'] }}">
    <div class="modal-content" style="border-radius: 15px;">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">{{ $food['title'] }}</h3>
            <button type="button" class="close" id="closeModal" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{ route('cartStore') }}" id="cartStoreForm">
                <div class="row" style="margin-left: 0; margin-right: 0;">
                    <div class="food-img col-12 overflow-hidden mb-20" style="height: 300px; margin-bottom: 25px;">
                        <img src="{{ $food['image'] }}" style="max-width: 100%; height:auto;">
                    </div>
                    <div class="col-12 food-part-conatainer">
                        <div class="row">
                            <div class="part-container col-12">
                                <div class="row">
                                    <h3 class="col-12">{{$food['title']}}</h3>
                                    <p>{{$food['description']}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(count($foodParts) > 0)
                        <div class="accordion" id="foodPartAccordion">
                            @for($i=0; $i< count($foodParts); $i++)
                                <div class=" accordion-item">
                                    <h2 class="accordion-header" id="foodPart{{$i}}">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
                                            {{$foodParts[$i]->title}} {!! in_array($foodParts[$i]->is_required, [1,true]) ? ' ( Obligatoire )' : '' !!}
                                        </button>
                                    </h2>
                                    <div id="collapse{{$i}}" class="accordion-collapse collapse show" aria-labelledby="heading{{$i}}" data-bs-parent="#foodPartAccordion">
                                        <div class="accordion-body">
                                            {{$foodParts[$i]->subtitle}}
                                            @if(count($foodPartItems) > 0)
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <input name="parts[{{\Str::slug($foodParts[$i]->title, '-')}}]{!! $foodParts[$i]->inputType == 'checkbox' ? '[]' : '' !!}" type="{{ $foodParts[$i]->inputType }}" value="{{ $foodParts[$i]->title }}_{{ $foodParts[$i]->uuid }}" {!! in_array($foodParts[$i]->is_required, [1,true]) ? 'checked required' : '' !!}>
                                                    </div>
                                                    <div class="accordion" id="foodPartItemAccordion{{$i}}">
                                                        @foreach(json_decode($foodPartItems[$i]) as $foodPartItem)
                                                            <h2 class="accordion-header" id="foodPartItems-{{$foodPartItem->title}}-{{$foodPartItem->id}}">
                                                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-{{$foodPartItem->title}}-{{$foodPartItem->id}}" aria-expanded="true" aria-controls="collapse-{{$foodPartItem->title}}-{{$foodPartItem->id}}">
                                                                    <input name="{{\Str::slug($foodPartItem->title, '-')}}{!! $foodPartItem->inputType == 'checkbox' ? '[]' : '' !!}" type="{{$foodPartItem->inputType}}" style="padding:5px;"  value="{{ $foodPartItem->title }}-{{$foodPartItem->id}}" {!! in_array($foodPartItem->is_essential, [1,true]) ? 'checked' : '' !!}>  {{$foodPartItem->title}} {!! in_array($foodPartItem->is_essential, [1,true]) ? ' ( Essential )' : '' !!}
                                                                </button>
                                                            </h2>
                                                            <div id="collapse-{{$foodPartItem->title}}-{{$foodPartItem->id}}" class="accordion-collapse collapse show" aria-labelledby="heading-{{$foodPartItem->title}}-{{$foodPartItem->id}}" data-bs-parent="#foodPartItemAccordion{{$i}}">
                                                                <div class="accordion-body">
                                                                    @if(in_array($foodPartItem->is_add_on, [1, true]))
                                                                        <div class="col-6 d-flex">
                                                                            <button class="rounded-circle border-light increaseQty" data-name="{{$foodPartItem->title}}" style="font-size: 18px; margin-right: 20px;  border: 0; width: 40px; height: 40px;">+</button>
                                                                                <nav style="font-size: 20px;" data-name="{{\Str::slug($foodPartItem->title, '-')}}" id="qtyNav" data-default="{{$foodPartItem->price}}">0</nav>
                                                                            <button class="rounded-circle border-light DecreaseQty" data-name="{{$foodPartItem->title}}" style="font-size: 18px; margin-left: 20px; border: 0; width: 40px; height: 40px;">-</button>
                                                                            <input type="hidden" data-name="{{\Str::slug($foodPartItem->title, '-')}}" name="qty_{{\Str::slug($foodPartItem->title, '-')}}" value="0">
                                                                        </div>
                                                                    @endif
                                                                    @if(in_array($foodPartItem->has_display_price, [1,true]) || in_array($foodPartItem->has_price, [1,true])  || intval($foodPartItem->price) > 0)
                                                                        <div class="col-6">
                                                                            <p>
                                                                                <span class="price" data-name="{{\Str::slug($foodPartItem->title, '-')}}">{{$foodPartItem->price}}</span>
                                                                                <span class="currency">{{$foodPartItem->currency_code}}</span>
                                                                            </p>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @else
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <input type="{{ $foodParts[$i]->inputType }}">
                                                    </div>
                                                    <div>Aucun élément definie</div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    @endif
                </div>
                <div class="row mx-1">
                    <label for="allergy" >
                        <input type="checkbox" name="allergy" id="allergy"> Allergy
                    </label>
                    <div id="allergy_div" style="display:none;width=100%;">
                        <textarea name="allergy_description" id="allergy_description" class="form-control" rows=3></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="d-flex">
                    <div>
                        <div class="d-flex">
                            <button data-name="increase-global" class="rounded-circle border-light increaseQty" style="font-size: 18px; margin-right: 20px;  border: 0; width: 40px; height: 40px;">+</button>
                                <nav data-name="qtyNav-global" style="font-size: 20px;" id="qtyNav">1</nav>
                            <button class="rounded-circle border-light DecreaseQty" data-name="decrease-global" style="font-size: 18px; margin-left: 20px; border: 0; width: 40px; height: 40px;">-</button>
                            <input type="hidden" name="qty" data-name="global" value="1">
                        </div>
                    </div>
                    <div data-name="global-price" data-default="{{$food['price'] == 0 ? '' : $food['price']}}">
                        {{$food['price'] != 0 ?  $food['price'] : ''}}
                    </div>
                    <input type="hidden" name="total_price" value="0" id="total_price">
                </form>
                    <div>
                        <button type="submit" class="btn" id="addModalCart" style="border-radius: 50px; background: #E8C52A; padding: 10px 20px;">Add to cart</button>
                    </div>
                </div>
            </div>
        </div>
</div>
