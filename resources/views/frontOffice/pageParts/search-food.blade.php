<div>
    <style>
        .btn-primary.focus, .btn-primary:focus {
            color: #fff;
            background-color: transparent;
            border-color: none;
            box-shadow: none;
        }
    </style>
    <section class="pagesBanner col-12" style="background: url('{{ asset($restaurant->cover) }}'); background-size: cover; background-position: center;">
        <div class="slide-title row">
            <div class="container">
                <h2 class="col-12">
                {{ $search != '' ? __('header.search') : $restaurant->name }}
                </h2>
                {{-- @livewire('test-comp') --}}
            </div>
        </div>
    </section>
    <section class="page-section container">

        <div id="nav-cat-scroll">
            <div class="form-container col-lg-12">
                <div class="row">
                    {{--<form id="lis-input" method="post" action="/search/restaurants">
                        <input type="text" name="search" id="myInput" placeholder="Search a restaurant now" class="writting">
                        <input class="addBtn" type="submit" id="addBtn" value="Add">
                    </form>--}}
                    <form method="post" action="/search/foods" id="lis-input" class="col-sm-7 col-12">
                        @csrf
                        <input type="text" name="search" id="myInput" placeholder="{{__('header.search_dish')}}" class="writting" value="{{$search ?? ''}}">
                        <input type="hidden" name="uuid" id="uuid" value="{{ $restaurant->uuid }}">
                        <input class="addBtn" type="submit" id="addBtn" value="<i class='fas fa-search'></i>">
                        <nav class="addBtn" id="addBtn">
                            <i class="fas fa-search"></i>
                        </nav>
                    </form>
                    <div class="location-container col-sm-5 col-12">
                        <div class="location-content">
                            <span class="location-pin"><img src="{{ asset('img/pin.svg') }}" alt="..." width="35" height="35"></span>
                            <span>
                                <h2>{{ Str::limit($restaurant->restaurant_region_name, 20) ?? '' }}</h2>
                                <p>{{  Str::limit($restaurant->restaurant_city, 20) ?? '' }}</p>
                            </span>
                        </div>
                    </div>
                    {{-- @yield('location') --}}
                </div>
            </div>
            <div class="col-lg-12 restaurant-categorie-list" id="CategorieMenu">
                <ul>
                    @php
                        $firstLink = 1;
                    @endphp
                    @foreach(array_keys($foods) as $menu_id)
                        @php
                           $menu = App\Models\RestaurantMenus::find($menu_id);
                        //    dd($menu);
                        @endphp
                        @if(isset($menu) && !empty($menu))
                            @if(!is_null($menu) && !is_null($menu->slug) && !is_null($menu->title))
                                <li ><a href="#{{$menu->slug}}" class="hvr-buzz-out navigation__link {{$firstLink == 1 ? 'active' : ''}}">{{$menu->title}}</a></li>
                            @endif
                        @endif
                        @php
                            $firstLink++;
                        @endphp
                    @endforeach
                </ul>
            </div>
        </div>


        @php
            $i = 0;
        @endphp
        @foreach ($foods as $menu_id => $food)
            @php
                $menu = App\Models\RestaurantMenus::find($menu_id)
            @endphp
            <div class="col-lg-12 restaurant-categorie-container" id="{{$menu->slug ?? ''}}">
                {{-- <div id="{{$menu->slug}}" class="smooth-id">olivier</div> --}}
                <div class="row">
                    <div class="section-title col-md-12 col-lg-12 col-sm-12">
                        <h3>{{$menu->title ?? __('header.no_name')}}</h3>
                    </div>
                    {{-- <div class="row"> --}}

                        @forelse($food as $item)
                            @php
                                $foodParts = App\Models\FoodParts::where('food_id', $item['id'])->get();
                            @endphp

                            <div class="col-lg-4  col-md-6 col-12 card item-container dish-container">
                                <div class="card-container row hvr-wobble-vertical">
                                    <div class="card-img col-md-12"
                                    style="background: url('{{ asset($item['image']) }}');
                                        background-size: cover;
                                        background-position: center;">

                                        <div class="card-body">
                                            @if(floatval($item['price']) > 0)
                                            <p class="price">
                                                $ {{ number_format($item['price'], '2' ,'.' ,',') }}
                                            </p>
                                            @endif
                                            <a href="#" class="food-card-link">
                                                <h5>{{ $item['title'] }}</h5>
                                                <p class="description">
                                                    {{ Str::limit($item['description'], 90) }}
                                                </p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- each food modal --}}
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary partModalBtn" data-id="{{ $item['uuid'] }}" data-toggle="modal" data-target="#a{{ $item['uuid'] }}" style="opacity: 0; position: relative; z-index: -1;">
                            </button>


                            <!-- Modal -->
                            <div class="modal fade food-modal" id="a{{ $item['uuid'] }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">

                                        {{-- Modal Header content --}}
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">{{__('header.custom_order')}} </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        {{-- Modal body content --}}
                                        {{--<form class="food_content_form" method="POST" action="{{ route('createOrderNotAuth') }}">--}}
                                        <form class="food_content_form" id="form-{{$item['uuid']}}" method="POST" action="{{ route('cartStore') }}">
                                            <div class="modal-body">
                                                @csrf
                                                <div class="row">
                                                    <div class="food-img col-12 overflow-hidden mb-20" style="height: 300px; margin-bottom: 25px;">
                                                        <img src="{{ asset($item['image']) }}" style="max-width: 100%; height:auto;">
                                                    </div>
                                                    <h1 class="col-12">{{ $item['title']}}</h1>
                                                    <p class="col-12">{{ $item['description']}}</p>

                                                    <div class="col-12">
                                                        <div class="row">
                                                                @foreach ($foodParts as $foodPart_id => $foodPart)
                                                                {{-- @if ($foodPart->has_children == 1 || $foodPart->has_children == true) --}}
                                                                {{-- @dd($foodPart) --}}
                                                                    @php
                                                                        $foodPartItems = collect($foodParts)->map(function($foodPart){
                                                                            return json_decode($foodPart->items);
                                                                        }
                                                                        )->toArray();
                                                                        // @dd($foodPartItems)
                                                                        $sum = 0;
                                                                    @endphp

                                                                    @if ($foodPart->has_children || count($foodPartItems) > 0)
                                                                        <div class="part-container col-12">
                                                                            <h3>
                                                                                {{ $foodPart->title }} {{$foodPart->is_required  == 1 ? __('header.required')  : ''}}
                                                                            </h3>
                                                                            <div class="food-content row">
                                                                                @foreach ($foodPartItems as $foodPartItem)
                                                                                    @foreach ($foodPartItem as $element)
                                                                                        <div class="form-group col-12">
                                                                                            <input type="{{$foodPart->inputType}}" data-id="{{$item['uuid']}}" data-price="{{ $element->is_add_on == 1 ? number_format($element->price,2) : '0' }}" name="{{ str_replace(' ', '_',$foodPart->title) }}{{ $foodPart->inputType == 'checkbox' ? '[]' : ''}}" value="{{$element->title}}" class="food-part-input" onchange="currentPrice(this)" {{ $element->is_essential ? 'checked' : '' }}>
                                                                                            <label for="{{str_replace(' ', '_',$foodPart->title)}}{{$foodPart->inputType == 'checkbox' ? '[]' : ''}}">
                                                                                                {{ $element->title}} {{ $element->is_add_on ? '( + '.$element->price.' '.$element->currency_name.' )' : '' }}
                                                                                            </label>
                                                                                        </div>
                                                                                        @php
                                                                                            $sum += $element->is_essential ? ($element->is_add_on == 1 ? $element->price : 0) : 0
                                                                                        @endphp
                                                                                    @endforeach
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                    {{-- @endif --}}
                                                                @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="hidden" data-id="{{$item['uuid']}}" class="form-control" name="order_qty" value="1">
                                                    <input type="hidden" data-id="{{$item['uuid']}}" class="form-control" name="total_price" value="{{ number_format($item['price'] + $sum, '2' ,'.' ,',') ?? 0}}">
                                                    <input type="hidden" class="form-control" name="food_id" value="{{ $item['uuid'] }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="allergy_text" class="col-form-label"><input type="checkbox" data-id="{{$item['uuid']}}" onchange="showDiv(this)" name="allergie_check" value="1" >{{__('header.notice')}}:</label>
                                                </div>
                                                <div class="form-group allergy_div" style="display:none;" data-id="{{$item['uuid']}}">
                                                    <label for="allergy_text" class="col-form-label">{{__('header.desc')}}:</label>
                                                    <textarea class="form-control" data-id="{{$item['uuid']}}" name="allergy_text"></textarea>
                                                </div>
                                            </div>

                                            <button type="submit" hidden id="ModalSubmitBtn">
                                            </button>
                                        </form>
                                        {{-- Modal footer content --}}
                                        <div class="modal-footer">

                                            <div class="d-flex">
                                                <div class="number-input">
                                                    <button data-id="{{$item['uuid']}}" onclick="setDown(this)" class="qtyBtn"></button>
                                                    <input data-id="{{$item['uuid']}}" class="quantity" min="1" name="quantity" value="1" max="{{ $item['quantity'] }}" type="number" onchange="changeQty(this)">
                                                    <button data-id="{{$item['uuid']}}" onclick="setUp(this)" class="plus"></button>
                                                </div>
                                            </div>

                                            <span class="modal-total-price" data-id="{{$item['uuid']}}">{{ number_format($item['price'] + $sum, '2' ,'.' ,',') }}</span>

                                            <input type="submit" value="{{ __('header.buy') }}" data-id="{{$item['uuid']}}" data-price="{{ number_format($item['price'] + $sum, '2' ,'.' ,',') ?? 0}}"  class="btn sendModalFormBtn" style="border-radius: 50px; background: #E8C52A; padding: 10px 20px;">
                                            <!-- <button type="button" class="btn sendModalFormBtn" style="border-radius: 50px; background: #E8C52A; padding: 10px 20px;">
                                                Buy now
                                            </button> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @empty
                            <div class="justify-content-center align-content-center ">
                                <h3>{{__('header.no_result')}}</h3>
                            </div>
                        @endforelse
                    {{-- </div> --}}
                </div>
            </div>
            @php
                $i++;
            @endphp
        @endforeach

    </section>
</div>

