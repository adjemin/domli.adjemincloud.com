<section class="pagesBanner col-12" style="background: url('{{ asset('img/8.jpg') }}'); background-size: cover; background-position: center;">

    <div class="slide-title row">
        <div class="container">
            <h2 class="col-12">{{ $pageInfo['title'] ?? '' }}</h2>
        </div>
    </div>
</section>
