
<div class="page card-top col-12">
    <div class="container">
        <div class="row card-top-content">
            <div class="logo col-lg-2 col-md-4 col-4" >
                <a href="/">
                    <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="50" alt="...">
                </a>
            </div>
            <menu class="col-lg-10 col-md-8 col-8">
                <input type="checkbox" id="check">
                <label for="check">
                    <i class="fas fa-bars" id="btn"></i>
                    <i class="fas fa-times" id="cancel"></i>
                </label>
                <ul class="menu-ul">
                    <li class="menu"><a href="/">{{ __('header.home.homeLink1')}}</a></li>
                    <li class="menu"><a href="#">{{ __('header.home.homeLink2')}} &nbsp; <i class="fas fa-chevron-down"></i></a>
                        <ul class="submenu">
                            {{-- <li><a href="{{ route('delivererLogin') }}">Delivery</a></li> --}}
                            <li><a href="{{ route('restaurantHome') }}">{{ __('header.home.homeLink22')}}</a></li>
                        </ul>
                    </li>
                    <li class="menu"><a href="{{ route('restaurants') }}">{{ __('header.home.homeLink3')}}</a></li>
                    <li class="menu"><a href="{{ route('contactUs') }}">{{ __('header.home.homeLink4')}}</a>
                    <li class="menu"><a href="{{ route('aboutUs') }}">{{ __('header.home.homeLink5')}}</a>
                        {{-- <ul class="submenu">
                            <li><a href="#">Blog 1</a></li>
                            <li><a href="#">Blog 2</a></li>
                            <li><a href="#">Blog 3</a></li>
                        </ul> --}}
                    </li>
                    <li><a href="{{ route('customerLogin') }}" class="btn appli-btn" id="appli-btn">{{ __('header.home.homeLink7')}} / {{ __('header.home.homeLink6')}}</a></li>
                    <!-- <li><a href="#" class="lang-btn"><img src="img/fr.gif" alt="..." width="30" height="30"></a></li> -->
                </ul>
            </menu>
        </div>
    </div>
</div>
