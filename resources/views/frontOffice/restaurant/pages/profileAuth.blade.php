@extends('frontOffice.layout.index')

@include('frontOffice.delivery.pageParts.pagesHeader')

@section('content')
    <section class="aboutSlide col-12">
        <div class="slide-title row">
            <div class="container">
                <a href="#" >
                    <h2 class="col-12">Profile</h2>
                </a>
            </div>
        </div>
    </section>
    <section class="page-section container">
        <div class="row">
            <div class="col-lg-8 profileAuth-left">
                <div class="tab-content profileAuth-left-content">
                    <div class="tab-pane active" id="profile">
                        <table class="info-delivery col-12" celspacing="0" cellpadding="15" border="0">
                            <tr class="photo-profile">
                                <td rows="2">
                                    <div class="photo-container" style="background-image: url('{{ asset('img/pp.jpg') }}'); background-size: cover; background-position: center">
                                    </div>
                                </td>
                            </tr>
                            <tr class="name">
                                <td>
                                    Name :
                                </td>
                                <td>
                                    Yao
                                </td>
                            </tr>
                            <tr class="surname">
                                <td>
                                    Surname :
                                </td>
                                <td>
                                    Olivier
                                </td>
                            </tr>
                            <tr class="mail">
                                <td>
                                    E-mail :
                                </td>
                                <td>
                                    contact@domli.com
                                </td>
                            </tr>
                            <tr class="order">
                                <td>
                                    Deliveries carried out :
                                </td>
                                <td >
                                    50
                                </td>
                            </tr>
                            <tr class="comments">
                                <td>
                                    comments :
                                </td>
                                <td >
                                    20
                                </td>
                            </tr>
                            <tr class="update-btn">
                                <td>
                                    <a href="#">Update profile</a>
                                </td>
                                <td >

                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane" id="order">
                        <table class="info-order col-12" celspacing="0" cellpadding="15" border="0">
                            <tr class="description">
                                <td>
                                    officiis? ipsum dolor sit amet, consectetur adipisicing elit. Earum, officiis?
                                </td>
                                <td class="btn">
                                    <a href="{{ route('deliveryOffer') }}" class="accept-btn">Accept</a>
                                </td>
                            </tr>
                            <tr class="date">
                                <td>
                                    date :
                                </td>
                                <td>
                                    To day
                                </td>
                            </tr>
                            <tr class="payment">
                                <td>
                                    Means of payment :
                                </td>
                                <td>
                                    VISA
                                </td>
                            </tr>
                            <tr class="time">
                                <td>
                                    Ready :
                                </td>
                                <td>
                                    20 minutes
                                </td>
                            </tr>
                        </table>
                        <table class="info-order col-12" celspacing="0" cellpadding="15" border="0">
                            <tr class="description">
                                <td>
                                    officiis? ipsum dolor sit amet, consectetur adipisicing elit. Earum, officiis?
                                </td>
                                <td class="btn">
                                    <a href="{{ route('deliveryOffer') }}" class="accept-btn">Accept</a>
                                </td>
                            </tr>
                            <tr class="date">
                                <td>
                                    date :
                                </td>
                                <td>
                                    To day
                                </td>
                            </tr>
                            <tr class="payment">
                                <td>
                                    Means of payment :
                                </td>
                                <td>
                                    VISA
                                </td>
                            </tr>
                            <tr class="time">
                                <td>
                                    Ready :
                                </td>
                                <td>
                                    20 minutes
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 profileAuth-right">

                <div class="nav nav-tabs profileAuth-right-content row">
                    <h3>Menu</h3>
                    <a href="#profile" class="nav-item nav-link active col-12 hvr-underline-from-left" data-toggle="tab">Profile</a>
                    <a href="#order" class="nav-item nav-link col-12 hvr-underline-from-left" data-toggle="tab">Order</a>
                    {{-- <a href="#info" class="nav-item nav-link col-12 hvr-underline-from-left" data-toggle="tab">Info</a> --}}
                </div>

            </div>
        </div>
    </section>
@endsection
