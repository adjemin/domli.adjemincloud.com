@extends('frontOffice.layout.index')

@include('frontOffice.delivery.pageParts.pagesHeader')

@section('content')
    <section class="login-logup-page ">
        <div class="row content">
            <div class="text-card col-md-5">
                <div class="text-card-content">
                    <div class="logo-container">
                        <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="50" alt="">
                    </div>
                    <h6>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore eum repellat voluptate omnis odit non neque velit molestiae nobis. Asperiores voluptas facilis nobis quos sed? Minus consequuntur autem dolorum error sapiente adipisci, quaerat ab accusantium magni soluta quis nihil. Necessitatibus iusto vero temporibus praesentium, voluptates dolorum ipsum </h6>
                    {{-- <div class="media-social-btn">
                        <a href="#" class="fb-btn btn">
                            <svg id="Bold" enable-background="new 0 0 24 24" height="25" viewBox="0 0 24 24" width="25" xmlns="http://www.w3.org/2000/svg">
                                <path d="m15.997 3.985h2.191v-3.816c-.378-.052-1.678-.169-3.192-.169-3.159 0-5.323 1.987-5.323 5.639v3.361h-3.486v4.266h3.486v10.734h4.274v-10.733h3.345l.531-4.266h-3.877v-2.939c.001-1.233.333-2.077 2.051-2.077z"/>
                            </svg>
                            &nbsp; Sign in with facebook</a>
                        <a href="#" class="gmail-btn btn">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25" height="25" viewBox="0 0 604.35 604.35" style="enable-background:new 0 0 604.35 604.35;"xml:space="preserve">
                                <g>
                                <g id="google-plus">
                                    <path d="M516.375,255v-76.5h-51V255h-76.5v51h76.5v76.5h51V306h76.5v-51H516.375z M320.025,341.7l-28.051-20.4
                                        c-10.2-7.649-20.399-17.85-20.399-35.7s12.75-33.15,25.5-40.8c33.15-25.5,66.3-53.55,66.3-109.65c0-53.55-33.15-84.15-51-99.45
                                        h43.35l30.6-35.7h-158.1c-112.2,0-168.3,71.4-168.3,147.9c0,58.65,45.9,122.4,127.5,122.4h20.4c-2.55,7.65-10.2,20.4-10.2,33.15
                                        c0,25.5,10.2,35.7,22.95,51c-35.7,2.55-102,10.2-150.45,40.8c-45.9,28.05-58.65,66.3-58.65,94.35
                                        c0,58.65,53.55,114.75,168.3,114.75c137.7,0,204.001-76.5,204.001-150.449C383.775,400.35,355.725,372.3,320.025,341.7z
                                        M126.225,109.65c0-56.1,33.15-81.6,68.85-81.6c66.3,0,102,89.25,102,140.25c0,66.3-53.55,79.05-73.95,79.05
                                        C159.375,247.35,126.225,168.3,126.225,109.65z M218.024,568.65c-84.15,0-137.7-38.25-137.7-94.351c0-56.1,51-73.95,66.3-81.6
                                        c33.15-10.2,76.5-12.75,84.15-12.75s12.75,0,17.85,0c61.2,43.35,86.7,61.2,86.7,102C335.324,530.4,286.875,568.65,218.024,568.65z
                                        "/>
                                </g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                            </svg>

                            &nbsp;Sign in with gmail
                        </a>
                    </div> --}}
                </div>
            </div>
            <div class="form-tab-container col-md-7">
                <nav class="nav nav-tabs row">
                    <a href="#login" class="nav-item nav-link active col-12" data-toggle="tab">Enter your message here</a>
                    {{-- <a href="#logup" class="nav-item nav-link col-6" data-toggle="tab">I am new here</a> --}}
                </nav>
                <div class="tab-content">
                    <div class="tab-pane active">
                        <form id="customer-form">
                            <div class="row">
                                <input type="mail" name="customerEmail" id="customerEmail" placeholder="E-mail (require)" class="input col-12">

                                <input type="text" name="customerObject" id="customerObject" placeholder="Object (require)" class="input col-12">

                                <textarea name="customerMessage" id="customerMessage" rows="6" placeholder="Enter your message here" class="input col-12"></textarea>

                                <div class="remember col-6 pl-0">
                                    <input type="checkbox" name="remember" id="remember">
                                    <label class="remember-label" for="remember">Remember me</label>
                                </div>
                                <div class="col-6 forgoten-password pr-0">
                                    <a href="#" style="float: right;">Forget password ?</a>
                                </div>
                                <button type="submit" class="submit-btn col-12 hvr-wobble-vertical">
                                    Send message
                                </button>
                            </div>

                        </form>
                    </div>
                    {{-- <div class="tab-pane" id="logup">
                        <form id="customer-form">
                            <div class="row">
                                <input type="text" name="customerName" id="customerName" placeholder="Name" class="input col-12">

                                <input type="text" name="customerSurname" id="customerSurname" placeholder="Surname" class="input col-12">

                                <input type="mail" name="customerEmail" id="customerEmail" placeholder="E-mail" class="input col-12">

                                <input type="password" name="customerPassword" id="customerPassword" placeholder="Password" class="input col-12">

                                <input type="password" name="customerPassword" id="customerPassword" placeholder="Confirm password" class="input col-12">

                                <input type="phone" name="customerNumber" id="customerNumber" placeholder="07070707" class="input col-12">

                                <p>I agree to the <a href="#">Prevacy policy</a> and <a href="#">terms of service</a></p>

                                <button type="submit" class="submit-btn col-12 hvr-wobble-vertical">
                                    Sign up
                                </button>
                            </div>
                        </form>
                    </div> --}}
                </div>
            </div>
        </div>
    </section>
@endsection
