<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Font-->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">
    <title>Domli</title>
    <style>
        *{
            font-family: 'Poppins', sans-serif;
        }
        body{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .logo{
            max-width: 250px;
            height: auto;
        }
        h1{
            font-size: 35px;
            font-weight: 700;
            color: #000;
        }
        p{
            font-size: 18px;
            width: 50%;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }
        .waitImg{
            max-width: 450px;
            height: auto;
            margin-top: -30px;
        }
        a{
            background-color:  #E8C52A;
            border-radius: 50px ;
            text-decoration: none;
            color: #fff;
            font-size: 18px;
            font-weight: 500;
            padding: 13px 40px;
            transition: all 0.3s;
        }
        a:hover{
            background-color: #ede615;
        }
    </style>
</head>
<body>
    <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="50" alt="..." class="logo">
    <img src="{{ asset('img/about-img.jpg') }}" alt="..." class="waitImg">
    <h1>
        Welcome to domli.
    </h1>
    <a href="{{ route('restaurantHome') }}" class="login">Click here to sign in now</a>
    {{-- <p>
        Your Domli account has been created, confirm your email by consulting the email that was sent to you at the address yo@gmail.com
    </p> --}}
</body>
</html>
