@extends('frontOffice.layout.index')

@include('frontOffice.customer.pageParts.pagesHeader')

@section('content')
    <section class="aboutSlide col-12">
        <div class="slide-title row">
            <div class="container">
                <a href="#" >
                    <h2 class="col-12">About us</h2>
                </a>
            </div>
        </div>
    </section>
    <section class="about-page-section container">
        <div class="row">
            <div class="col-lg-6 about-text">
                <div class="about-text-content">
                    <p>
                        Pe, tenetur delectus iure tempora corrupti? Voluptatibus nisi quo similique cum quas incidunt eveniet? Officia vero veritatis sit eaque reiciendis explicabo repellendus molestiae tempora.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero earum dicta nisi reprehenderit vel, hic tenetur a rem suscipit temporibus perspiciatis repudiandae enim! Nostrum aut inventore sae
                    </p>
                </div>
                <a href="#" class="call-action-btn hvr-buzz-out">get started</a>
            </div>
            <div class="col-lg-6 about-img">
                <img src="{{ asset('img/about-img.jpg') }}" alt="..." class="hvr-bounce-out">
            </div>
        </div>
    </section>
@endsection
