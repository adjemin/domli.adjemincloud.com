@extends('layouts.index')

{{-- @include('frontOffice.pagesHeader') --}}

@section('content')
    <section class="aboutSlide col-12">
        <div class="slide-title row">
            <div class="container">
                <a href="#" >
                    {{-- {{ dd($restaurant) }} --}}
                    <h2 class="col-12">Welcome {{ $restaurant->responsable_name }}</h2>
                </a>
            </div>
        </div>
    </section>
    <section class="last-register-step container">
        <div class="row content">
            <div class="form-container col-12">
                <div class="form-content">
                    <form id="delivery-form" method="POST" action="{{ route('updateRestaurant') }}">
                        @csrf

                        <input type="hidden" name="uuid" id="uuid" value="{{ $restaurant->uuid }}"
                        <div class="form-container row">
                            <h3 class="col-12 section-title">Require step</h3>
                            <p class="title-desc col-12">
                                Here's what you need to do to set up your account.
                            </p>
                            <div class="choice-container col-12">
                                <div class="row">
                                    <div class="item PhotoID col-12">
                                        <div class="row PhotoID-content">
                                            <div class="card-left col-md-8 {{ $errors->has('responsable_photo') ? ' has-error' : '' }}">
                                                <label class="vehicoleType-label" for="respo_photo">
                                                    <span>Photo of the person in charge </span>
                                                    <p>We need a photo where we can see your face.</p>
                                                </label>
                                                <input type="file"  name="responsable_photo" id="responsable_photo"><br>
                                                @if ($errors->has('responsable_photo'))
                                                    <span class="help-block col-12" style="color:#d50d0d; font-size: 16px; font-weight: 500;">
                                                        {{ $errors->first('responsable_photo') }}
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="card-right col-md-4">
                                                <img src="{{ asset('img/user.svg') }}" id="user_img_tag" alt="...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item registration_number col-12 {{ $errors->has('restaurant_registration_number') ? ' has-error' : '' }}">

                                        <label class="vehicoleType-label" for="scooter">
                                            <span>Registration number</span>
                                            <p>registration number assigned by the Canadian government.</p>
                                        </label>

                                        <input type="text"  name="restaurant_registration_number" id="restaurant_registration_number" value=""><br>

                                        @if ($errors->has('restaurant_registration_number'))
                                            <span class="help-block" style="color:#d50d0d; font-size: 16px; font-weight: 500;">
                                                {{ $errors->first('restaurant_registration_number') }}
                                            </span>
                                        @endif
                                    </div>

                                    <div class="item paperID col-12 hvr-ripple-out {{ $errors->has('responsable_paper_id_number') ? ' has-error' : '' }}">

                                        <label class="vehicoleType-label" for="vehicoleType1">
                                            <span>ID Piece Number</span>
                                            <p>enter the number of the ID of your choice</p>
                                        </label>
                                        <div class="row" style="padding: 30px">
                                            <select name="restaurant_paper_id_type" id="restaurant_paper_id_type" class="col-5" style="margin-right: 10px">
                                                <option value="passeport">Passeport</option>
                                                <option value="driver-license">Driver's license</option>
                                                <option value="national-dentity-card">National Identity Card</option>
                                                <option value="other">Other</option>
                                            </select>

                                            <input type="text"  name="responsable_paper_id_number" id="responsable_paper_id_number" class="col-6"><br>

                                             @if ($errors->has('responsable_paper_id_number'))
                                                <span class="help-block" style="color:#d50d0d; font-size: 16px; font-weight: 500;">
                                                    {{ $errors->first('responsable_paper_id_number') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <button type="submit" class="submit-btn col-12 hvr-wobble-vertical">
                                        Continue
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script>

        let responsablePhotoInput = document.getElementById('responsable_photo');
        responsablePhotoInput.addEventListener('change', function(event){
            event.preventDefault();
            // var loadFile = function(event) {
                var reader = new FileReader();
                reader.onload = function() {
                    responsablePhotoInput.src = reader.result;
                    document.getElementById('user_img_tag').src = responsablePhotoInput.src;
                };
                reader.readAsDataURL(event.target.files[0]);
            // };
        });
        // var loadFile = function(event) {
        //     var reader = new FileReader();
        //     reader.onload = function() {
        //         var output = document.getElementById('output');
        //         output.src = reader.result;
        //     };
        //     reader.readAsDataURL(event.target.files[0]);
        // };
    </script>

@endsection
