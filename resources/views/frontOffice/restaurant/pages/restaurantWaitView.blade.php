<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Font-->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">
    <title>Domli</title>
    <style>
        *{
            font-family: 'Poppins', sans-serif;
        }
        body{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .logo{
            max-width: 250px;
            height: auto;
        }
        h1{
            font-size: 35px;
            font-weight: 700;
            color: #E8C52A;
            margin-top: 0;
            margin-bottom: 10px
        }
        p{
            font-size: 18px;
            width: 50%;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }
        .waitImg{
            max-width: 200px;
            height: auto;
            margin-top: -30px;
        }
    </style>
</head>
<body>
    <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="50" alt="..." class="logo">
    <img src="{{ asset('img/smile.svg') }}" alt="..." class="waitImg">
    <h1>
        Congratulations
    </h1>
    <p>
        Thank you for choosing Domli .
        Your request is being processed. We will contact you soon for more details.
    </p>
</body>
</html>
