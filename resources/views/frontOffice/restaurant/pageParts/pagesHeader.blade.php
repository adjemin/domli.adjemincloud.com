
<div class="page card-top col-12">
    <div class="container">
        <div class="row card-top-content">
            <div class="logo col-lg-2 col-md-4 col-4" >
                <a href="/">
                    <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="50" alt="...">
                </a>
            </div>
            <menu class="col-lg-10 col-md-8 col-8">
                <input type="checkbox" id="check">
                <label for="check">
                    <i class="fas fa-bars" id="btn"></i>
                    <i class="fas fa-times" id="cancel"></i>
                </label>
                <ul class="menu-ul">
                    <li class="menu"><a href="/">Home page</a></li>
                    {{-- <li class="menu"><a href="#">Become &nbsp; <i class="fas fa-chevron-down"></i></a>
                        <ul class="submenu">
                            <li><a href="#">Delivery</a></li>
                            <li><a href="#">Restauant</a></li>
                        </ul>
                    </li> --}}
                    {{-- <li class="menu"><a href="{{ route('restaurants') }}">Restaurants</a></li> --}}
                    <li class="menu"><a href="{{ route('contactUs') }}">Contact us</a>
                    <li class="menu"><a href="{{ route('aboutUs') }}">About us</a>
                        {{-- <ul class="submenu">
                            <li><a href="#">Blog 1</a></li>
                            <li><a href="#">Blog 2</a></li>
                            <li><a href="#">Blog 3</a></li>
                        </ul> --}}
                    </li>
                    <li><a href="{{ route('login') }}" class="btn appli-btn" id="appli-btn">login / sign up</a></li>
                    <!-- <li><a href="#" class="lang-btn"><img src="img/fr.gif" alt="..." width="30" height="30"></a></li> -->
                </ul>
            </menu>
        </div>
    </div>
</div>
