<header class="col-12 restaurant-home">
    <div class="header-content row">
        <div class="card-top col-12">
            <div class="container">
                <div class="row card-top-content">
                    <div class="logo col-lg-2 col-md-4 col-4">
                        <a href="/">
                            <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="50" alt="...">
                        </a>
                    </div>
                    <menu class="col-lg-10 col-md-8 col-8">
                        <input type="checkbox" id="check">
                        <label for="check">
                            <i class="fas fa-bars" id="btn"></i>
                            <i class="fas fa-times" id="cancel"></i>
                        </label>
                        <ul class="menu-ul">
                            <li class="menu"><a href="/">{{__('header.home.homeLink1')}}</a></li>
                            <li class="menu"><a href="{{ route('aboutUs') }}">{{__('header.home.homeLink5')}}</a></li>
                            <li class="menu"><a href="{{ route('contactUs') }}">{{__('header.home.homeLink4')}}</a>
                            </li>
                            {{-- <li><a href="{{ route('login') }}" class="btn signIn" id="homSignIn">login / sign up</a></li> --}}
                        </ul>
                    </menu>
                </div>
            </div>
        </div>
        <section class="restaurant-login-logup container">
            <div class="row content">
                <div class="restaurant-banner-text col-md-6 ">
                    <h1 style="font-size:24px;">{{__('header.home.restaurant-banner-text')}}</h1>
                </div>
                <div class="form-tab-container restaurant-login-container col-md-6">
                    <nav class="nav nav-tabs row">
                        <a href="#login" class="nav-item nav-link active col-6" data-toggle="tab" id="login-link">{{__('header.home.tab1')}}</a>
                        <a href="#logup" class="nav-item nav-link col-6" data-toggle="tab" id="logup-link">{{__('header.home.tab2')}}</a>
                    </nav>
                    <div class="tab-content">
                        <div class="tab-pane active" id="login">
                            <form id="restaurant-login-form" action="{{ route('restaurant_login') }}" method="POST">
                                {{--<form id="restaurant-login-form" action="{{ route('restaurant_backoffice_home') }}" method="POST">--}}
                                @csrf
                                <div class="row">
                                    <div class="col-12 form-group">
                                        <div class="row">
                                            <input type="mail" name="email" id="login_email" placeholder="{{__('header.home.placeholderEa')}}" class="pt-2 input col-12">
                                            <span class="help-block-email col-12" style="color:red;"></span>
                                        </div>

                                    </div>

                                    <div class="col-12 form-group">
                                        <div class="row">
                                            <input type="password" name="password" id="login_password" placeholder="{{__('header.home.placeholderPsw')}}" class="pt-2 input col-12">
                                            <span class="help-block-password col-12" style="color:red;"></span>
                                        </div>
                                    </div>

                                    <span class="help-block col-12" style="color:red;"></span>

                                    <div class="remember col-sm-6 col-12 pl-0">
                                        <input type="checkbox" name="remember" id="remember">
                                        <label class="remember-label" for="remember">{{__('header.home.remember')}}</label>
                                    </div>
                                    <div class="col-sm-6 col-12 forgoten-password pr-0">
                                        <a href="{{ url('/restaurant/password/reset') }}">{{__('header.home.mdpo')}}</a>
                                    </div>
                                    <button type="submit" class="submit-btn col-12 hvr-wobble-vertical" id="login-btn">
                                       {{__('header.home.btnIdentification')}}
                                    </button>
                                </div>

                            </form>
                        </div>
                        <div class="tab-pane" id="logup">
                            <form id="restaurant-registration-form" action="{{ route('restaurant_register') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="row">

                                    <div class="form-group {{ $errors->has('responsable_name') ? ' has-error' : '' }} col-12">
                                        <div class="row">
                                            <input type="text" name="responsable_name" id="responsable_name" placeholder="{{__('header.home.placeholderFullname')}}" class="input col-12" value="{{ old('responsable_name') }}">

                                            @if ($errors->has('responsable_name'))
                                                <span class="help-block" style="color:#d50d0d;">
                                                    {{ $errors->first('responsable_name') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} col-12">
                                        <div class="row">
                                            <input type="text" name="name" id="name" placeholder="{{__('header.home.placeholderResname')}}" class="input col-12" value="{{ old('name') }}">
                                            @if ($errors->has('name'))
                                                <span class="help-block" style="color:#d50d0d;">
                                                    {{ $errors->first('name') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('restaurant_adress_name') ? ' has-error' : '' }} col-12">
                                        <div class="row">
                                            <input type="text" name="restaurant_adress_name" id="restaurant_adress_name" placeholder="{{__('header.home.placeholderResadd')}}" class="input col-12" value="{{ old('restaurant_adress_name') }}">
                                        </div>
                                        @if ($errors->has('restaurant_adress_name'))
                                            <span class="help-block" style="color:#d50d0d;">
                                                {{ $errors->first('restaurant_adress_name') }}
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} col-12">
                                        <div class="row">
                                            <input type="email" name="email" id="email" placeholder="{{__('header.home.placeholderEa')}}" class="input col-12" value="{{ old('email') }}">
                                            @if ($errors->has('email'))
                                            <span class="help-block" style="color:#d50d0d;">
                                                {{ $errors->first('email') }}
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-12 form-group">
                                        <div class="row">
                                            <input type="tel" name="phone_number" id="phone_number" class="input col-12"  required style="padding-bottom: 5px;" value="{{ old('phone_number') }}">

                                            <span id="valid-msg" class="hide" style="color: #15b025; margin-top:8px; padding-left: 15px;"></span>
                                            <span id="error-msg" class="hide" style="color: #d50d0d; margin-top:8px; padding-left: 15px;"></span>

                                            <input type="hidden" name="dial_code" id="dial_code" value="">

                                            {{-- <input type="hidden" name="phone_number_customer" id="phone_number_customer"> --}}

                                            <input type="hidden" name="country_code" id="country_code" value="" />

                                            <span id="valid-msgPhoneNumber" class="hide"></span>
                                            <span id="error-msgPhoneNumber" class="hide" style="color:red;"></span>

                                        </div>
                                    </div>

                                    <div class="form-group col-12">
                                        <div class="row">
                                            <input type="password" name="password" id="password" placeholder="{{__('header.home.placeholderPsw')}}" class="input col-12">
                                            {{-- <span class="help-block-password" style="color:#d50d0d;">
                                                </span> --}}
                                        </div>
                                    </div>

                                    <div class="form-group col-12">
                                        <div class="row">
                                            <input type="password" name="password2" id="password2" placeholder="{{__('header.home.placeholderPswC')}}" class="input col-12">
                                            <span class="help-block-password" style="color:#d50d0d; padding-left: 15px;padding-top: 10px;">
                                            </span>
                                        </div>
                                    </div>

                                    <p> {{__('header.home.privacyP1')}} <a href="#"> {{__('header.home.privacyP2')}} </a>  {{__('header.home.privacyP3')}}  <a href="#"> {{__('header.home.privacyP4')}} </a></p>

                                    <button type="submit" class="submit-btn col-12 hvr-wobble-vertical" id="restaurant_register_btn">
                                        {{__('header.home.btnIncription')}}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</header>

{{--@section('extra-js')
<script>
    function getLocation(str) {
        value = str.match(/(\d+)(\.\d+)?/g); // trim
        return value;
    }
    var country_code = "{{ $userCoordinates['country_code'] ?? 'CI' }}";


    //search = document.getElementById("input_search");
    const matchList = document.getElementById("match-list")
    $("#adress").autocomplete({
        source: function(query, callback) {
            $.getJSON(
                `https://nominatim.openstreetmap.org/search?q=${query.term}&countrycodes=${country_code}&format=json&addressdetails=1&limit=100`,
                function(data) {
                    var places = data;
                    places = places.map(place => {
                        console.log("==== place ===");
                        console.log(place);
                        return {
                            title: place.display_name.replace(/<br\/>/g, ", "),
                            value: place.display_name.replace(/<br\/>/g, ", "),
                            lat: place.lat,
                            lon: place.lon,
                            id: place.osm_id
                        };
                    });
                    return callback(places);
                });
        },
        minLength: 2,
        select: function(event, ui) {
            console.log(ui);
            $('#lat_input').val(ui.item.lat);
            $('#lon_input').val(ui.item.lon);

            console.log("Selected: " + ui.item.title);
        },
        open: function() {
            $('.ui-autocomplete').css('width', '400px').css('cursor', 'pointer'); // HERE
        }
    });

</script>
@endsection--}}
