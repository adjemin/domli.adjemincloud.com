<section class="delivery-howItWorks-section ">
	<div class="container">
		<div>
			<div class="section-title col-md-12 col-lg-12 col-sm-12">
				<h3>{{__('header.home.hith3')}}</h3>
            </div>
            <div class="col-12">
                <div class="delivery-howItWork-content">
                    <div class="row">
                        <div class="col-lg-4 howItWorks-text">
                            <div class="howItWorks-text-content hvr-bounce-to-left">
                                <img src="{{ asset('img/commander-de-la-nourriture.svg') }}"  class="my-3" height="70" alt="...">
                                <p>
                                   {{__('header.home.hitr1')}}
                                </p>
                            </div>
                        </div>

                        <div class="col-lg-4 howItWorks-text">
                            <div class="howItWorks-text-content hvr-bounce-to-left">
                               <img src="{{ asset('img/vendre.svg') }}"  class="my-3" height="70" alt="...">
                                <p>
                                    {{__('header.home.hitr2')}}
                                </p>
                            </div>
                        </div>

                        <div class="col-lg-4 howItWorks-text">
                            <div class="howItWorks-text-content hvr-bounce-to-left">
                               <img src="{{ asset('img/camion-de-livraison.svg') }}"  class="my-3" height="70" alt="...">
                                <p>
                                    {{__('header.home.hitr3')}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>
