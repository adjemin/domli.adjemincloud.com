@php
    $restaurants = \App\Models\Restaurants::whereIn('verified', [1, true])->whereNotNull('logo')->inRandomOrder()->take(10)->get();
@endphp
<section class="delivery-newPaper col-12">
	<div class="container">
		<div class="row">
            <div class="col-12 delivery-newPaper-container">
                <div class="row d-flex align-items-center ">
                    <div class="delivery-newPaper-img col-sm-12 col-xs-12 col-md-12 col-lg-6">
                        <div class="owl-carousel owl-theme owl-loaded">
                            <div class="owl-stage-outer">
                                <div class="owl-stage">
                                    @foreach ($restaurants as $restaurant)    
                                        <div class="owl-item">
                                            <a href="{{ url('/page/restaurant/'.$restaurant->uuid) }}" >
                                                <img src="{{ $restaurant->logo }}" alt="..." class="w-100" style="height:300px ; object-fit:contain;">
                                            </a>
                                            <br>
                                            <h3 class="text-center">{{ $restaurant->name }}</h3>
                                        </div>
                                    @endforeach
                                    {{-- <div class="owl-item">
                                        <a href="#">
                                            <img src="{{ asset('img/restaurant.jpg') }}" alt="..." class="w-100">
                                        </a>
                                    </div>
                                    <div class="owl-item">
                                        <a href="#">
                                            <img src="{{ asset('img/African_Food.jpg') }}" alt="..." class="w-100">
                                        </a>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                            
                    </div>
                    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-6 delivery-newPaper-text">
                        <h1>{{__('header.home.delivery-newPaperh1')}}</h1>
                        <p>
                            {{__('header.home.delivery-newPaperp')}}
                        </p>
                        <a href="/partenaire/restaurants" class="delivery-newPaper-btn">{{__('header.home.delivery-newPaperbtn')}}</a>
                    </div>
                </div>
            </div>
		</div>

        @push('scripts')
            <script>
                $(document).ready(function(){
                    $(".owl-carousel").owlCarousel({
                        items: 1,
                        loop: true,
                        autoplay:true
                    });
                });
            </script>
        @endpush
	</div>
</section>
