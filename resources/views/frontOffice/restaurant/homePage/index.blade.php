@extends('layouts.index')

{{-- @include('frontOffice.customer.homePage.dishProgressPopUp') --}}
@section('content')
    @include('frontOffice.restaurant.homePage.sectionContainer.banner')
    @include('frontOffice.restaurant.homePage.sectionContainer.howItWorks')
    @include('frontOffice.restaurant.homePage.sectionContainer.newPaper')
    @include('frontOffice.restaurant.homePage.sectionContainer.suggestions')

@endsection



@push('customer-login-script')
    <script>
        const loginForm = document.getElementById('login');
        const restaurantLoginForm = document.getElementById('restaurant-login-form');
        const loginMailInput = document.querySelector('#login #login_email');
        const loginPasswordInput = document.querySelector('#login #login_password');
        const loginBtn = document.getElementById('login-btn');

        let logupPassword = document.querySelector('#logup #password');
        let logupPassword2 = document.querySelector('#logup #password2');
        const registerName = document.querySelector('#logup #name');
        const registerResponsableName = document.querySelector('#logup #responsable_name');

        const registerBtn = document.querySelector('#restaurant_register_btn');

        // console.log(registerName.value);


        //Window load with Active
        window.addEventListener('load', function(){
            if(registerName.value !== '' || registerResponsableName.value !== ''){
                // console.log('ok');
                document.querySelector('#login-link').classList.remove('active');
                document.querySelector('#login').classList.remove('active');

                document.querySelector('#logup').classList.add('active');
                document.querySelector('#logup-link').classList.add('active');
            }
        });

        // // Register button add Event listener
        // registerBtn.addEventListener('click', function(e) {
        //     // e.preventDefault();

        //     let action = 'register';
        //     this.setAttribute('disabled', true);
        //     this.style.opacity = '0.5';

        // });

        if (logupPassword.value == '' || logupPassword.value == null || logupPassword2.value == null || logupPassword2.value == '') {
            registerBtn.setAttribute('disabled', true);
            registerBtn.style.opacity = "0.5";
        }

        let password1 = '';
        let passwordConfirm = '';
        let passwordMsg = ''
        logupPassword.addEventListener('input', function(e){
            password1 = logupPassword.value
        });

        logupPassword2.addEventListener('input', function(e){
            passwordConfirm = logupPassword2.value
            // console.log(passwordConfirm.length)
            if (logupPassword.value == logupPassword2.value) {
                if (passwordConfirm.length < 8) {
                    document.querySelector('#restaurant-registration-form .help-block-password').innerText = 'Your password must be at least 8 characters long.';
                }
                else{
                    document.querySelector('#restaurant-registration-form .help-block-password').innerText = '✓ valid';
                    document.querySelector('#restaurant-registration-form .help-block-password').style.color = "#15b025";
                    registerBtn.removeAttribute('disabled');
                    registerBtn.style.opacity = "1";
                }
            }
            else {
                document.querySelector('#restaurant-registration-form .help-block-password').innerText = 'Enter identical password';
            }
        });

        //Listen to the login button
        loginBtn.addEventListener('click', function(e){
            e.preventDefault();
            this.setAttribute('disabled', true);
            this.style.opacity = '0.5';

            document.querySelector('#restaurant-login-form .help-block-email').innerText = '';
            document.querySelector('#restaurant-login-form .help-block-password').innerText = '';

            if (loginMailInput.value == null || loginMailInput.value == '') {
                document.querySelector('#restaurant-login-form .help-block-email').innerText = 'please fill in the email field';
                this.removeAttribute('disabled');
                this.style.opacity = '1';
            }
            else if(loginPasswordInput.value == null || loginPasswordInput.value == '' || loginPasswordInput.value.length < 8) {
                document.querySelector('#restaurant-login-form .help-block-password').innerText = 'The password field must be at least 8 characters long.';
                this.removeAttribute('disabled');
                this.style.opacity = '1';
            }
            else{
                this.removeAttribute('disabled');
                this.style.opacity = '1';
                // this.classList.remove('prevented');
                // restaurantLoginForm.submit();
                // console.log('ok');
                //Get login form value
                let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                let url = "{{ route('restaurant_login') }}";
                let _data = {
                    "email": loginMailInput.value,
                    "password": loginPasswordInput.value
                };
                // // Ajax with Fetch
                fetch(
                    url,
                    {
                        headers: {
                            "Content-type": "application/json;charset=UTF-8",
                            "Accept": "application/json, text-plain, */*",
                            "X-Requested-with": "XMLHttpRequest",
                            "X-CSRF-TOKEN": token
                        },
                        method: "post",
                        body:  JSON.stringify(_data)
                    }
                )
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    if (data['status'] == 'error') {
                        document.querySelector('#restaurant-login-form .help-block').innerText = 'An error occurred, please check your information and try again.';
                        this.removeAttribute('disabled');
                        this.style.opacity = '1';
                    }
                    else{
                        this.removeAttribute('disabled');
                        this.style.opacity = '1';
                        window.location.replace(data['url'])
                    }
                })
                .catch(err => console.log('Request Failed', err));
            }
        });

        //Listen to the register button
        // registerBtn.addEventListener('click', function(e){
        //     e.preventDefault();

        // });
    </script>
@endpush

@push('intel-input-script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{ asset('js/intlTelInput.js') }}"></script>
    <script src="{{ asset('js/utils.js') }}"></script>
    <script>

        var input = document.querySelector("#phone_number"),
        errorMsg = document.querySelector("#error-msg"),
        validMsg = document.querySelector("#valid-msg"),
        dialCode = document.querySelector('#dial_code'),
        countryCode = document.querySelector('#country_code');

        // Error messages based on the code returned from getValidationError
        var errorMap = [ "Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

        // Initialise plugin
        var intl = window.intlTelInput(input, {
            initialCountry: "ca",
            utilsScript: "js/utils.js",
            geoIpLookup: function(success, failure) {
                $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    success(countryCode);
                });
            },

        });

        var reset = function() {
            input.classList.remove("error");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };

        // Validate on blur event
        input.addEventListener('blur', function() {
            reset();
            if(input.value.trim()){
                if(intl.isValidNumber()){
                    validMsg.classList.remove("hide");
                    validMsg.innerHTML = "✓ Valid";
                }else{
                    input.classList.add("error");
                    var errorCode = intl.getValidationError();
                    errorMsg.innerHTML = errorMap[errorCode];
                    errorMsg.classList.remove("hide");
                }
            }
        });

        // Reset on keyup/change event
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);
    </script>
@endpush





