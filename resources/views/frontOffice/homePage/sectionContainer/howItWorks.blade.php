<section class="howItWorks-section row">
	<div class="container">
		<div class="row">
			<div class="section-title col-md-12 col-lg-12 col-sm-12">
				<h3>How it works</h3>
			</div>
			<div class="col-lg-4 howItWorks-img hvr-float">
				<div class="howItWorks-img-content rounded-circle">
					<img src="{{ asset('img/hamburger.svg') }}" alt="...">
				</div>
				<p>Order</p>
			</div>
			<div class="col-lg-4 howItWorks-img hvr-float">
				<div class="howItWorks-img-content rounded-circle">
					<img src="{{ asset('img/payment-method.svg') }}" alt="...">
				</div>
				<p>Payement</p>
			</div>
			<div class="col-lg-4 howItWorks-img hvr-float">
				<div class="howItWorks-img-content rounded-circle">
					<img src="{{ asset('img/food-delivery.svg') }}" alt="...">
				</div>
				<p>Delivery</p>
			</div>
		</div>
	</div>
    @livewire('test-comp')
</section>
