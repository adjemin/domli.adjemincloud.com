<section class="suggestions-section row">
	<div class="container">
		<div class="row">
			<div class="section-title col-12">
                <h3>Our suggestions</h3>
                <a href="#">See more</a>
			</div>
            @for ($i = 7; $i <= 12; $i++)
                <div class="card item col-lg-4 col-md-6">
                    <div class="card-container">
                        <div class="img-card">
                            <a href="#">
                                <img src="{{ asset('img/'.$i.'.jpg') }}" class="attachment-post-thumb size-post-thumb wp-post-image" alt="...">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="card-text">
                                <h5>Lorem ipsum dolor sit amet consectetur adipisicing elit</h5>
                                <a href="/restaurants">
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae inventore consequuntur itaque
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endfor
		</div>
	</div>
</section>
