<section class="Popular row">
	<div class="container">
		<div class="row">
			<div class="section-title col-md-12 col-lg-12 col-sm-12">
                <h3>Popular near you</h3>
                <a href="#">See more</a>
            </div>
            @for ($i = 1; $i <= 6; $i++)
                <div class="Popular-img col-lg-4 hvr-wobble-skew">
                    <a href="#">
                        <div class="block-img" style="background-image: url('{{ asset('img/'.$i.'.jpg') }}'); background-size: cover; background-position: center;">
                        </div>
                        <div class="restaurant-name">Burger</div>
                        <div class="item-title">Burger King</div>
                    </a>
                </div>
            @endfor
		</div>
	</div>
</section>
