<header class="col-12">
    <div class="header-content row">
        <div class="container">
            <div class="row">
                <div class="card-top col-12">
                    <div class="row card-top-content">
                        <div class="logo col-lg-2 col-md-4 col-4">
                            <a href="/">
                                <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="50" alt="...">
                            </a>
                        </div>
                        <menu class="col-lg-10 col-md-8 col-8">
                            <input type="checkbox" id="check">
                            <label for="check">
                                <i class="fas fa-bars" id="btn"></i>
                                <i class="fas fa-times" id="cancel"></i>
                            </label>
                            <ul class="menu-ul">
                                <li class="menu"><a href="/">Home page</a></li>
                                <li class="menu"><a href="#">Become &nbsp; <i class="fas fa-chevron-down"></i></a>
                                    <ul class="submenu">
                                        {{-- <li><a href="#">Customer</a></li> --}}
                                        <li><a href="{{ route('delivererLogin') }}">Delivery</a></li>
                                        <li><a href="#">Restaurant</a></li>
                                    </ul>
                                </li>
                                <li class="menu"><a href="{{ route('CategorieRestaurants') }}">Restaurants</a></li>
                                <li class="menu"><a href="{{ route('contactUs') }}">Contact us</a>
                                </li>
                                <li class="menu"><a href="{{ route('aboutUs') }}">About us</a></li>

                                <li><a href="customer/login" class="btn signIn" id="homSignIn">login / sign up</a></li>
                            </ul>
                        </menu>
                    </div>
                </div>
                <div class="card-bottom col-12">
                    <div class="row">
                        <div class="caption d-block col-lg-6 ">
                            <div class="section-title col-12">
                                <h1>Feeling a little peckish ? You've come to the right place</h1>
                                {{-- @livewire('test-comp') --}}
                            </div>
                            <form id="lis-input" method="post" action="/search/restaurants">
                                @csrf
                                <input type="text" name="title" id="myInput" placeholder="Search a restaurant now" class="writting">
                                <input class="addBtn" type="submit" id="addBtn" value="Add">
                            </form>
                            {{--<form id="lis-input">
                                <input type="text" name="title" id="myInput" placeholder="Search a restaurant now" class="writting">
                                <input class="addBtn" type="submit" id="addBtn" value="Add">
                            </form>--}}
                        </div>
                        <!-- <div class="card-img col-lg-6 " data-aos="fade-right"
                                data-aos-delay="2000"
                                data-aos-duration="1000">
                            <img src="img/slide-img.png" class="d-block" alt="...">
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
