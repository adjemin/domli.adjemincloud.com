<section class="about-section">
	<div class="container">
		<div class="row">
            @php
                $titles = ['Customer', 'Delivery', 'Restaurant'];
                $link = ['']
                $i = 0;
            @endphp
            @foreach ($titles as $title)
                <div class="card item col-lg-4 col-md-6">
                    <div class="card-container" style="background-image: url('{{ asset('img/'.strtolower($title).'.jpg') }}');background-size:cover;background-position: center;">
                        <div class="card-body">
                            <div class="card-text">
                                <h5> {{ $title }} </h5>
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. In optio illum, facere at dolore porro recusandae hic necessitatibus. Inventore eaque animi dignissimos deleniti quia enim
                                </p>
                                <a href="{{if($title == 'Restaurant'){ /backOffice/restaurant/1 } elseif($title == 'Delivery') {/deliverer/login} else { / } }}" class="action-btn">Become {{ strtolower($title) }} now</a>
                            </div>
                        </div>
                    </div>
                </div>
                @php
                    $i++;
                @endphp
            @endforeach

			{{-- <div class="card item col-lg-4 col-md-6">
				<div class="card-container" style="background-image: url('../img/img4.jpg');background-size:cover;background-position: center;">
					<div class="card-body">
						<div class="card-text">
							<h5>Delivery</h5>
							<p>
								La  valeur d’un homme ne se mesure pas à son argent, son statut ou ses  possessions. La valeur d’un homme réside...
							</p>
							<a href="#" class="action-btn">Order now</a>
						</div>
					</div>
				</div>
			</div>
			<div class="card item col-lg-4 col-md-6">
				<div class="card-container" style="background-image: url('../img/img4.jpg');background-size:cover;background-position: center;">
					<div class="card-body">
						<div class="card-text">
							<h5>Delivery</h5>
							<p>
								La  valeur d’un homme ne se mesure pas à son argent, son statut ou ses  possessions. La valeur d’un homme réside...
							</p>
							<a href="#" class="action-btn">Order now</a>
						</div>
					</div>
				</div>
			</div> --}}
		</div>
	</div>
</section>
