@extends('layouts.index')

@include('frontOffice.homePage.dishProgressPopUp')
@section('content')
    @include('frontOffice.homePage.sectionContainer.banner')
    @include('frontOffice.homePage.sectionContainer.howItWorks')
    @include('frontOffice.homePage.sectionContainer.popular')
    @include('frontOffice.homePage.sectionContainer.suggestions')
    @include('frontOffice.homePage.sectionContainer.about')
@endsection
