@extends('layouts.index')

{{-- @include('frontOffice.customer.homePage.dishProgressPopUp') --}}
@section('content')
    @include('frontOffice.delivery.homePage.sectionContainer.banner')
    @include('frontOffice.delivery.homePage.sectionContainer.howItWorks')
    @include('frontOffice.delivery.homePage.sectionContainer.newPaper')
    @include('frontOffice.delivery.homePage.sectionContainer.suggestions')
@endsection
