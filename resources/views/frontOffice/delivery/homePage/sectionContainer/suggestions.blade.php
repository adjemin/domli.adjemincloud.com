<section class="suggestions-section row">
	<div class="container">
        <div class="section-title col-md-12 col-lg-12 col-sm-12">
            <h3>Make money on the go</h3>
        </div>
        <div class="card-section col-12">

            <div class="card-section-content row">

                <div class="card-container col-lg-4 col-md-6 col-12">

                    <article class="card-container-content">

                        <article class="post-thumb" style="background-image:url('{{ asset('img/delivery.jpg') }}'); background-size: cover;"></article>
                        <div class="card-body">
                            <h4>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.</h4>
                            <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit</p>
                            {{-- <a href="#" class="card-btn"><i class="fas fa-long-arrow-alt-right"></i>Voir plus</a> --}}
                        </div>

                    </article>

                </div>

                <div class="card-container col-lg-4 col-md-6 col-12">

                    <article class="card-container-content">

                        <article class="post-thumb" style="background-image:url('{{ asset('img/delivery2.jpg') }}'); background-size: cover;"></article>
                        <div class="card-body">
                            <h4>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.</h4>
                            <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit</p>
                            {{-- <a href="#" class="card-btn"><i class="fas fa-long-arrow-alt-right"></i>Voir plus</a> --}}
                        </div>

                    </article>

                </div>

                <div class="card-container col-lg-4 col-md-6 col-12">

                    <article class="card-container-content">

                        <article class="post-thumb" style="background-image:url('{{ asset('img/delivery1.jpg') }}'); background-size: cover;"></article>
                        <div class="card-body">
                            <h4>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.</h4>
                            <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit</p>
                            {{-- <a href="#" class="card-btn"><i class="fas fa-long-arrow-alt-right"></i>Voir plus</a> --}}
                        </div>

                    </article>

                </div>
            </div>
        </div>
    </div>
</section>
