@extends('frontOffice.layout.index')

@include('frontOffice.delivery.pageParts.pagesHeader')

@section('content')
    <section class="pagesSlide col-12">
        <div class="slide-title row">
            <div class="container">
                <a href="{{ route('profileAuth') }}" >
                    <h2 class="col-12"><i class="fas fa-arrow-left"></i>offer detail</h2>
                </a>
            </div>
        </div>
    </section>
    <section class="page-section container">
        <div class="row">
            <div class="col-lg-10 offerDetail-container" style="margin-left: auto; margin-right: auto">
                <div class="offerDetail-content">
                    <table class="info-delivery col-12" celspacing="0" cellpadding="15" border="0">
                        <tr class="photo-profile">
                            <td colspan="2">
                                <div class="photo-container" style="background-image: url('{{ asset('img/2.jpg') }}'); background-size: cover; background-position: center">
                                </div>
                            </td>
                        </tr>
                        <tr class="name">
                            <td>
                                Name :
                            </td>
                            <td>
                                Jamaica: Jerk Chicken
                            </td>
                        </tr>
                        <tr class="surname">
                            <td>
                                adress :
                            </td>
                            <td>
                                Along with reggae, jerk chicken is one of this country’s most famous and well-loved
                            </td>
                        </tr>
                        <tr class="date">
                            <td>
                                date :
                            </td>
                            <td>
                                to day
                            </td>
                        </tr>
                        <tr class="time">
                            <td>
                                Ready :
                            </td>
                            <td>
                                20 minutes
                            </td>
                        </tr>
                        <tr class="Subtotal">
                            <td>
                                Sub total :
                            </td>
                            <td>
                                $50
                            </td>
                        </tr>
                        <tr class="delivery">
                            <td>
                                delivery :
                            </td>
                            <td>
                                $20
                            </td>
                        </tr>
                        <tr class="total">
                            <td>
                                Total pay :
                            </td>
                            <td>
                                $70
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
