@extends('frontOffice.layout.index')

@include('frontOffice.delivery.pageParts.pagesHeader')

@section('content')
    <section class="pagesSlide col-12">
        <div class="slide-title row">
            <div class="container">
                <a href="{{ route('restaurantSingle') }}" >
                    <h2 class="col-12"><i class="fas fa-arrow-left"></i>Order detail</h2>
                </a>
            </div>
        </div>
    </section>
    <section class="page-section container">
        <div class="row">
            <div class="col-lg-7 order-detail-left">
                <div class="order-detail-left-content">
                    <h3>Delivery adress</h3>
                    <div class="map-google">
                        <img src="{{ asset('img/map.jpg') }}" alt="...">
                    </div>
                    <div class="order-input-card row">
                        <span class="col-1">
                            <img src="{{ asset('img/pin.svg') }}" alt="..." width="35" height="35">
                        </span>
                        <input type="text" name="delivery-adress" id="delivery-adress" class="form-control col-10" placeholder="Enter delivery adress ">
                    </div>
                    <div class="order-input-card row">
                        <span class="col-1">
                            <img src="{{ asset('img/future.svg') }}" alt="..." width="35" height="35">
                        </span>
                        <input type="text" name="delivery-adress" id="delivery-adress" class="form-control col-10" placeholder="To day ">
                    </div>
                    <h4>Payment information</h4>
                    <div class="order-input-card row">
                        <span class="col-1">
                            <img src="{{ asset('img/card.svg') }}" alt="..." width="35" height="35">
                        </span>
                        <input type="text" name="delivery-adress" id="delivery-adress" class="form-control col-10" placeholder="Add a payment method ">
                    </div>
                    <h4>Promo code</h4>
                    <div class="order-input-card row">
                        <span class="col-1">
                            <img src="{{ asset('img/promo.svg') }}" alt="..." width="35" height="35">
                        </span>
                        <input type="text" name="delivery-adress" id="delivery-adress" class="form-control col-10" placeholder="Are you a promo code ? ">
                    </div>
                    <h4> contact</h4>
                    <div class="order-input-card row">
                        {{-- <span class="col-1">
                            <img src="{{ asset('img/') }}" alt="..." width="35" height="35">
                        </span> --}}
                        <input type="text" name="delivery-adress" id="delivery-adress" class="form-control col-10" placeholder="Are you a promo code ? ">
                    </div>
                    <button type="submit" class="submit-butn butn-bg hvr-radial-out disabled" disabled><span>Confirm order</span></button>
                </div>
            </div>
            <div class="col-lg-5 order-detail-right">
                <div class="order-detail-right-content row">
                    <h3>Order detail</h3>
                    <table class="info-table col-12" celspacing="0" cellpadding="10" border="0">
                        <tr class="Items">
                            <td>
                                Items total (3 items) :
                            </td>
                            <td>
                                $5000
                            </td>
                        </tr>
                        <tr class="store">
                            <td>
                                Store sharges :
                            </td>
                            <td>
                                $0
                            </td>
                        </tr>
                        <tr class="delivery">
                            <td>
                                Delivery :
                            </td>
                            <td>
                                $30
                            </td>
                        </tr>
                        <tr class="total">
                            <td>
                                Total discount :
                            </td>
                            <td>
                                $5030
                            </td>
                        </tr>
                        <tr class="total-pay">
                            <td>
                                Total pay :
                            </td>
                            <td class="price">
                                $5030
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
