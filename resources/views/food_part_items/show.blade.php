@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{!! url('/backOffice/restaurants/'.$foodPartItem->foodPart->food->restaurant->uuid) !!}">Restaurant</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{!! url('/restaurant/'.$foodPartItem->foodPart->food->restaurant->uuid.'/foods') !!}">Food</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{!! url('/restaurant/'.$foodPartItem->foodPart->food->restaurant->uuid.'/'.$foodPartItem->foodPart->food->id.'/food_part') !!}">Food Part</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{!! url('/restaurant/'.$foodPartItem->foodPart->food->restaurant->uuid.'/'.$foodPartItem->foodPart->id.'/food_part_item') !!}">Food Part Item</a>
        </li>
        <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{!! url('/restaurant/'.$foodPartItem->foodPart->food->restaurant->uuid.'/'.$foodPartItem->foodPart->food->id.'/food_part') !!}" class="btn btn-primary float-right">Retour</a>
                             </div>
                             <div class="card-body">
                                 @include('food_part_items.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
