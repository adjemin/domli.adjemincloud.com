
<div class="row">
    <div class="col-6">
        <!-- Food Part Id Field -->
        <div class="form-group">
            {!! Form::label('food_part_id', 'Option:') !!}
            <p>{{ $foodPartItem->foodPart->title }}</p>
        </div>    
    </div>
    <div class="col-6">
        <!-- Food Id Field -->
        <div class="form-group">
            {!! Form::label('food_id', 'Plat:') !!}
            <p>{{ $foodPartItem->food->title }}</p>
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-6">
        <!-- Title Field -->
        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            <p>{{ $foodPartItem->title }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Price Field -->
        <div class="form-group">
            {!! Form::label('price', 'Price:') !!}
            <p>{{ $foodPartItem->price ?? 0 }}</p>
        </div>    
    </div>
</div>
<div class="row">
    {{--<div class="col-6">
        <!-- Inputtype Field -->
        <div class="form-group">
            {!! Form::label('inputType', 'Inputtype:') !!}
            <p>{{ $foodPartItem->inputType == 'radio' ? '' : '' }}</p>
        </div>    
    </div>--}}
    <div class="col-6">
        <!-- Currency Code Field -->
        <div class="form-group">
            {!! Form::label('currency_code', 'Currency Code:') !!}
            <p>{{ $foodPartItem->currency_code }}</p>
        </div>    
    </div>
    <div class="col-6">
        <!-- Currency Name Field -->
        <div class="form-group">
            {!! Form::label('currency_name', 'Currency Name:') !!}
            <p>{{ $foodPartItem->currency_name }}</p>
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-6">
        <!-- Is Essential Field -->
        <div class="form-group">
            {!! Form::label('is_essential', 'Is Essential:') !!}
            @if($foodPartItem->is_essential)
                <p><span class="badge badge-info px-2 py-2"> Yes</span></p>
            @else
                <p><span class="badge badge-danger px-2 py-2"> No</span></p>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            {!! Form::label('is_add_on', 'Is Add On:') !!}
            <p>{{ $foodPartItem->is_add_on ? '<span class="badge badge-info px-2 py-2"> Yes</span>' : '<span class="badge badge-danger px-2 py-2"> No</span>' }}</p>
        </div>
    </div>
</div>

{{--
<!-- Is Add On Field -->
<div class="form-group">
    {!! Form::label('is_add_on', 'Is Add On:') !!}
    <p>{{ $foodPartItem->is_add_on }}</p>
</div>
<!-- Has Display Price Field -->
<div class="form-group">
    {!! Form::label('has_display_price', 'Has Display Price:') !!}
    <p>{{ $foodPartItem->has_display_price }}</p>
</div>

<!-- Has Price Field -->
<div class="form-group">
    {!! Form::label('has_price', 'Has Price:') !!}
    <p>{{ $foodPartItem->has_price }}</p>
</div>
--}}