<!-- Food Part Id Field -->
<!-- <div class="form-group col-sm-6"> -->
    {{--{!! Form::label('food_part_id', 'Food Part Id:') !!}--}}
    {!! Form::hidden('food_part_id', $foodPart->id) !!}
<!-- </div> -->

<!-- Food Id Field -->
<!-- <div class="form-group col-sm-6"> -->
    {{--{!! Form::label('food_id', 'Food Id:') !!}--}}
    {!! Form::hidden('food_id', $foodPart->food->id) !!}
<!-- </div> -->
<div class="row">
    <!-- Title Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('is_essential', 'Is Essential:') !!}
        <label class="checkbox-inline">
            {!! Form::hidden('is_essential', 0) !!}
            {!! Form::checkbox('is_essential', '1', null) !!}
        </label>
    </div>
</div>
{{--
    <div class="row">
        <!-- Inputtype Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('inputType', 'Input type:') !!}
            <select name="inputType" id="" class="form-control">
                <option value="radio">select_unique</option>
                <option value="checkbox">multiple_select</option>
            </select>
        </div>
        <div class="form-group col-sm-6">
            {!! Form::label('currency_name', 'Nom de la devise') !!}
            <select name="currency_name" class="form-control">
                <option value="XOF">CFA</option>
            </select>
        </div>
    </div>
--}}
{{--
<!-- Is Add On Field -->

<!-- Has Display Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('has_display_price', 'Has Display Price:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('has_display_price', 0) !!}
        {!! Form::checkbox('has_display_price', '1', null) !!}
    </label>
</div>
--}}
<div class="row">
    
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="currency_name" value="Dollar">
    <input type="hidden" name="inputType" value="{{$foodPart->inputType}}">
    <!-- Is Essential Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('is_add_on', 'Is Add On:') !!}
        <label class="checkbox-inline">
            {!! Form::hidden('is_add_on', 0) !!}
            {!! Form::checkbox('is_add_on', '1', null, ['onchange'=> 'show(this)']) !!}
        </label>
    </div>
    <!-- Price Field -->
    <div class="form-group col-sm-6" id="price" style="display:none;">
        {!! Form::label('price', 'Price:') !!}
        {!! Form::number('price', null, ['class' => 'form-control', 'min' => 0, 'value' => 0]) !!}
    </div>
    {{-- <!-- Currency Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('currency_code', 'Currency Code:') !!}
        {!! Form::text('currency_code', null, ['class' => 'form-control']) !!}
    </div>
    <!-- Currency Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('currency_name', 'Currency Name:') !!}
        {!! Form::text('currency_name', null, ['class' => 'form-control']) !!}
    </div> --}}

</div>
<div class="row">
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('foodPartItems.index') }}" class="btn btn-secondary">Cancel</a>
    </div>
</div>
<script>
    function show(e){
        var price = document.getElementById('price');

        if(e.checked){
            price.style.display = 'block';
        }else{
            price.style.display = 'none';
        }
    }
</script>
