@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{!! url('/backOffice/restaurants/'.$foodPart->food->restaurant->uuid) !!}">Restaurant</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{!! url('/restaurant/'.$foodPart->food->restaurant->uuid.'/foods') !!}">Food</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{!! url('/restaurant/'.$foodPart->food->restaurant->uuid.'/'.$foodPart->food->id.'/food_part') !!}">Food Part</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{!! url('/restaurant/'.$foodPart->food->restaurant->uuid.'/'.$foodPart->id.'/food_part_item') !!}">Food Part Item</a>
        </li>
        <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve" height="28" height="28">
                                    <circle style="fill:#E8C52A;" cx="25" cy="25" r="25"/>
                                    <line style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="25" y1="13" x2="25" y2="38"/>
                                    <line style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="37.5" y1="25" x2="12.5" y2="25"/>
                                </svg>
                                <strong>Create Food Part Item</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'foodPartItems.store']) !!}

                                   @include('food_part_items.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
