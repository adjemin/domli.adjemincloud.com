<div class="table-responsive-sm">
    <table class="table table-striped" id="passwordDeliveries-table">
        <thead>
            <tr>
                <th>Password Reset Code</th>
        <th>Password Reset Deadline</th>
        <th>Delevery Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($passwordDeliveries as $passwordDelivery)
            <tr>
                <td>{{ $passwordDelivery->password_reset_code }}</td>
            <td>{{ $passwordDelivery->password_reset_deadline }}</td>
            <td>{{ $passwordDelivery->delevery_id }}</td>
                <td>
                    {!! Form::open(['route' => ['passwordDeliveries.destroy', $passwordDelivery->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('passwordDeliveries.show', [$passwordDelivery->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('passwordDeliveries.edit', [$passwordDelivery->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>