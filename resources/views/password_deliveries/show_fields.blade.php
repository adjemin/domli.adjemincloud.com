<!-- Password Reset Code Field -->
<div class="form-group">
    {!! Form::label('password_reset_code', 'Password Reset Code:') !!}
    <p>{{ $passwordDelivery->password_reset_code }}</p>
</div>

<!-- Password Reset Deadline Field -->
<div class="form-group">
    {!! Form::label('password_reset_deadline', 'Password Reset Deadline:') !!}
    <p>{{ $passwordDelivery->password_reset_deadline }}</p>
</div>

<!-- Delevery Id Field -->
<div class="form-group">
    {!! Form::label('delevery_id', 'Delevery Id:') !!}
    <p>{{ $passwordDelivery->delevery_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $passwordDelivery->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $passwordDelivery->updated_at }}</p>
</div>

