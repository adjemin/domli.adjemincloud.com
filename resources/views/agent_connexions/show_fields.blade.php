<!-- Deliverer Id Field -->
<div class="form-group">
    {!! Form::label('deliverer_id', 'Deliverer Id:') !!}
    <p>{{ $agentConnexion->deliverer_id }}</p>
</div>

<!-- Location Lat Field -->
<div class="form-group">
    {!! Form::label('location_lat', 'Location Lat:') !!}
    <p>{{ $agentConnexion->location_lat }}</p>
</div>

<!-- Location Lng Field -->
<div class="form-group">
    {!! Form::label('location_lng', 'Location Lng:') !!}
    <p>{{ $agentConnexion->location_lng }}</p>
</div>

<!-- Location Name Field -->
<div class="form-group">
    {!! Form::label('location_name', 'Location Name:') !!}
    <p>{{ $agentConnexion->location_name }}</p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    <p>{{ $agentConnexion->is_active }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $agentConnexion->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $agentConnexion->updated_at }}</p>
</div>

