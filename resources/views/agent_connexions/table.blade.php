<div class="table-responsive-sm">
    <table class="table table-striped" id="agentConnexions-table">
        <thead>
            <tr>
                <th>Deliverer Id</th>
        <th>Location Lat</th>
        <th>Location Lng</th>
        <th>Location Name</th>
        <th>Is Active</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($agentConnexions as $agentConnexion)
            <tr>
                <td>{{ $agentConnexion->deliverer_id }}</td>
            <td>{{ $agentConnexion->location_lat }}</td>
            <td>{{ $agentConnexion->location_lng }}</td>
            <td>{{ $agentConnexion->location_name }}</td>
            <td>{{ $agentConnexion->is_active }}</td>
                <td>
                    {!! Form::open(['route' => ['agentConnexions.destroy', $agentConnexion->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('agentConnexions.show', [$agentConnexion->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('agentConnexions.edit', [$agentConnexion->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>