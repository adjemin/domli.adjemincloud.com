<!-- Location Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_lat', 'Location Lat:') !!}
    {!! Form::text('location_lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Lng Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_lng', 'Location Lng:') !!}
    {!! Form::text('location_lng', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_name', 'Location Name:') !!}
    {!! Form::text('location_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_active', 'Is Active:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_active', 0) !!}
        {!! Form::checkbox('is_active', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('agentConnexions.index') }}" class="btn btn-secondary">Cancel</a>
</div>
