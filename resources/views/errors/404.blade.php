{{-- @extends('errors::minimal')

@section('title', __('Not Found')) --}}

    <div style="width: 100wv; height:100vh; display:flex; justify-content: center; align-items: center;">
        <img src="{{ asset('img/error-404.svg') }}" style="max-width: 500px; ">
        <h1 style="color: #000; weight: 700;"> Page no found</h1>
    </div>

{{-- @section('message', __('Not Found')) --}}
