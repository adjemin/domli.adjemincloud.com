{{--
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $restaurantMenus->title }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{{ $restaurantMenus->slug }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $restaurantMenus->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $restaurantMenus->updated_at }}</p>
</div>
--}}


<div class="row">
    {{-- @dd($restaurantMenus) --}}
    <div class="col-lg-4">
        {{-- {!! Form::label('slug', 'Slug:') !!} --}}
        <svg id="Layer_1" enable-background="new 0 0 512 512" height="300" viewBox="0 0 512 512" width="300" xmlns="http://www.w3.org/2000/svg">
            <path d="m368 416h16v64h-16z" fill="#ad7907"/></g><g><path d="m120 416h16v64h-16z" fill="#ad7907"/></g><g><path d="m96 32h320v392h-320z" fill="#f2c033"/></g><g><path d="m136 176h160v16h-160z" fill="#ad7907"/></g><g><path d="m136 208h160v16h-160z" fill="#ad7907"/></g><g><path d="m136 288h160v16h-160z" fill="#ad7907"/></g><g><path d="m136 320h160v16h-160z" fill="#ad7907"/></g><g><g><path d="m336 384h24v16h-24z" fill="#ad7907"/></g><g><path d="m368 384h24v16h-24z" fill="#ad7907"/></g></g><g fill="#441293"><path d="m202.5 56.4c-3.3-1.1-6.9 0-8.9 2.8l-17.6 23.5-17.6-23.5c-2.1-2.8-5.7-3.9-8.9-2.8-3.3 1.1-5.5 4.1-5.5 7.6v48c0 4.4 3.6 8 8 8s8-3.6 8-8v-24l9.6 12.8c1.5 2 3.9 3.2 6.4 3.2s4.9-1.2 6.4-3.2l9.6-12.8v24c0 4.4 3.6 8 8 8s8-3.6 8-8v-48c0-3.4-2.2-6.5-5.5-7.6z"/><path d="m248 72c4.4 0 8-3.6 8-8s-3.6-8-8-8h-24c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8h24c4.4 0 8-3.6 8-8s-3.6-8-8-8h-16v-8h16c4.4 0 8-3.6 8-8s-3.6-8-8-8h-16v-8z"/><path d="m304 56c-4.4 0-8 3.6-8 8v21.6l-17.3-26c-2-2.9-5.6-4.2-9-3.2s-5.7 4.1-5.7 7.7v48c0 4.4 3.6 8 8 8s8-3.6 8-8v-21.7l17.3 26c1.5 2.3 4 3.6 6.7 3.6.8 0 1.6-.1 2.3-.3 3.4-1 5.7-4.1 5.7-7.7v-48c0-4.4-3.6-8-8-8z"/><path d="m368 112v-48c0-4.4-3.6-8-8-8s-8 3.6-8 8v40h-16v-40c0-4.4-3.6-8-8-8s-8 3.6-8 8v48c0 4.4 3.6 8 8 8h32c4.4 0 8-3.6 8-8z"/><path d="m376 192h-24v-16h24c4.4 0 8-3.6 8-8s-3.6-8-8-8h-8c0-4.4-3.6-8-8-8s-8 3.6-8 8h-8c-4.4 0-8 3.6-8 8v32c0 4.4 3.6 8 8 8h24v16h-24c-4.4 0-8 3.6-8 8s3.6 8 8 8h8c0 4.4 3.6 8 8 8s8-3.6 8-8h8c4.4 0 8-3.6 8-8v-32c0-4.4-3.6-8-8-8z"/><path d="m376 288c4.4 0 8-3.6 8-8s-3.6-8-8-8h-8c0-4.4-3.6-8-8-8s-8 3.6-8 8h-8c-4.4 0-8 3.6-8 8v32c0 4.4 3.6 8 8 8h24v16h-24c-4.4 0-8 3.6-8 8s3.6 8 8 8h8c0 4.4 3.6 8 8 8s8-3.6 8-8h8c4.4 0 8-3.6 8-8v-32c0-4.4-3.6-8-8-8h-24v-16z"/></g><path d="m416 32-320 392v-392z" fill="#fff" opacity=".2"/>
        </svg>
        {{-- <p>{{ $restaurantMenus->image }}</p> --}}
    </div>
    <div class="col-lg-6">
        <div class="col-lg-6">
            <!-- Restaurant Id Field -->
            <div class="form-group">
                {!! Form::label('restaurant_id', 'Restaurant:') !!}
                <p>{{ $restaurantMenus->restaurant->name }}</p>
            </div>
        </div>
        <div class="col-lg-6">
            <!-- Title Field -->
            <div class="form-group">
                {!! Form::label('title', 'Title:') !!}
                <p>{{ $restaurantMenus->title }}</p>
            </div>
        </div>
        <div class="col-lg-6">
            <!-- Slug Field -->
            <div class="form-group">
                {!! Form::label('slug', 'Slug:') !!}
                <p>{{ $restaurantMenus->slug }}</p>
            </div>
        </div>
    </div>
</div>
{{-- <div class="row">

</div> --}}
