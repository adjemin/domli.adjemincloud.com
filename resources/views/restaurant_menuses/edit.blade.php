{{--@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{!! route('restaurantMenuses.index') !!}">Restaurant Menus</a>
        </li>
        <li class="breadcrumb-item active">Edit</li>
    </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Restaurant Menus</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($restaurantMenus, ['route' => ['restaurantMenuses.update', $restaurantMenus->id], 'method' => 'patch']) !!}

                              @include('restaurant_menuses.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection--}}

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li>
            <a href="{{\Auth::check() ? route('admin-restaurants-show', ['id' => Auth::guard('restaurant')->user()->uuid]) : url('/backOffice/restaurants/'.Auth::guard('restaurant')->user()->uuid) }}"></a>
        </li>
          <li class="breadcrumb-item">
             <a href="{{ \Auth::check() ? '/admin/restaurant/'.$restaurantMenus->restaurant->uuid.'/menu' : '/restaurant/'.$restaurantMenus->restaurant->uuid.'/menu' }}">{{$restaurantMenus->restaurant->name}} Menu's</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              {{-- <i class="fa fa-edit fa-lg"></i> --}}
                              <strong>Edit Restaurant Menu</strong>
                          </div>
                          <div class="card-body">
                            @php
                                $prefix = \Auth::check() ? 'admin': '';
                                $route = "$prefix/restaurant/$restaurantMenus->restaurant->uuid/menu/update";
                            @endphp

                            {!! Form::model($restaurantMenus, ['route' => ['restaurantMenuses.update', $restaurantMenus->id], 'method' => 'patch']) !!}

                                @include('restaurant_menuses.fields')

                            {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection
