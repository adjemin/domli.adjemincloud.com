{{--<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('restaurantMenuses.index') }}" class="btn btn-secondary">Cancel</a>
</div>
--}}

<input type="hidden" name="restaurant_id" value="{{$restaurantMenus->restaurant->uuid}}">
<div class="row">
    <div class="col-6">
        <!-- Title Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    </div>
    <div class="col-6">
        <!-- Submit Field -->
        <div class="form-group col-sm-12" style="display: flex; justify-content: flex-end; align-items: center; height: 100%;">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href='{{ \Auth::check() ? "/admin/restaurant/".$restaurantMenus->restaurant->uuid."/menu" : "/restaurant/".$restaurantMenus->restaurant->uuid."/menu" }}' class="btn btn-danger" style="margin-left: 10px">Cancel</a>
        </div>
    </div>
</div>
