<div class="table-responsive-sm">
    <table class="table table-striped" id="restaurantPhones-table">
        <thead>
            <tr>
                <th>Restaurant Id</th>
        <th>Dial Code</th>
        <th>Phone Number</th>
        <th>Phone</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($restaurantPhones as $restaurantPhones)
            <tr>
                <td>{{ $restaurantPhones->restaurant_id }}</td>
            <td>{{ $restaurantPhones->dial_code }}</td>
            <td>{{ $restaurantPhones->phone_number }}</td>
            <td>{{ $restaurantPhones->phone }}</td>
                <td>
                    {!! Form::open(['route' => ['restaurantPhones.destroy', $restaurantPhones->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('restaurantPhones.show', [$restaurantPhones->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('restaurantPhones.edit', [$restaurantPhones->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>