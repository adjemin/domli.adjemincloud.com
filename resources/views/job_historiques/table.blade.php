<div class="table-responsive-sm">
    <table class="table table-striped" id="jobHistoriques-table">
        <thead>
            <tr>
                <th>Job Id</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($jobHistoriques as $jobHistorique)
            <tr>
                <td>{{ $jobHistorique->job_id }}</td>
            <td>{{ $jobHistorique->status }}</td>
                <td>
                    {!! Form::open(['route' => ['jobHistoriques.destroy', $jobHistorique->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('jobHistoriques.show', [$jobHistorique->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('jobHistoriques.edit', [$jobHistorique->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>