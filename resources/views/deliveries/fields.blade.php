<div class="row mt-3">
    <div class="col-md-6">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <!-- Surname Field -->
        <div class="form-group">
            {!! Form::label('surname', 'Surname:') !!}
            {!! Form::text('surname', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Email Field -->
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <!-- Password Field -->
        <div class="form-group">
            {!! Form::label('password', 'Password:') !!}
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Dial Code Field -->
        <div class="form-group">
            {!! Form::label('dial_code', 'Dial Code:') !!}
            {!! Form::number('dial_code', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <!-- Phone Number Field -->
        <div class="form-group">
            {!! Form::label('phone_number', 'Phone Number:') !!}
            {!! Form::number('phone_number', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">

    </div>
    <div class="col-md-6">

    </div>
</div>

{{-- <!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::number('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Lng Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lng', 'Lng:') !!}
    {!! Form::number('lng', null, ['class' => 'form-control']) !!}
</div>

<!-- Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lat', 'Lat:') !!}
    {!! Form::number('lat', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('deliveries.index') }}" class="btn btn-secondary">Cancel</a>
</div>
