@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('deliveries.index') }}">Delivery</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @php
                    $account = \App\Models\DeliveryCompte::where('delivery_id', $delivery->id)->first();
                    $historiques = \App\Models\DeliveryPaymentHistorique::where(['delivery_id' => $delivery->id])->get();
                    $paid = collect($historiques)->reduce(function($carry, $item){
                        return $item->amount + $carry;
                    }, 0);
                 @endphp
                 <div class="row">
                     <div class="col-6 card">
                         <div class="card-header text-center">
                             Delivery Balance
                         </div>
                         <div class="card-body text-center">
                            {{ ($account->compte ?? 0) .' USD' }}
                         </div>
                     </div>
                     <div class="col-6 card">
                         <div class="card-header text-center">
                             Paid
                         </div>
                         <div class="card-body text-center">
                            {{ floatval($paid) .' USD' }}
                         </div>
                     </div>
                 </div>
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-9">
                         <div class="card">
                            <div class="card-header">
                                <strong class="py-2 my-3">Delivery Details</strong>
                                {!! Form::open(['route' => ['admin-delivery-change_state', $delivery->id], 'method' => 'post']) !!}
                                    {!! Form::button('Change status  <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.873 477.873" style="enable-background:new 0 0 477.873 477.873; width: 20px;" xml:space="preserve">
                                        <path d="M392.533,238.937c-9.426,0-17.067,7.641-17.067,17.067V426.67c0,9.426-7.641,17.067-17.067,17.067H51.2 c-9.426,0-17.067-7.641-17.067-17.067V85.337c0-9.426,7.641-17.067,17.067-17.067H256c9.426,0,17.067-7.641,17.067-17.067 S265.426,34.137,256,34.137H51.2C22.923,34.137,0,57.06,0,85.337V426.67c0,28.277,22.923,51.2,51.2,51.2h307.2 c28.277,0,51.2-22.923,51.2-51.2V256.003C409.6,246.578,401.959,238.937,392.533,238.937z"/>
                                        <path d="M458.742,19.142c-12.254-12.256-28.875-19.14-46.206-19.138c-17.341-0.05-33.979,6.846-46.199,19.149L141.534,243.937 c-1.865,1.879-3.272,4.163-4.113,6.673l-34.133,102.4c-2.979,8.943,1.856,18.607,10.799,21.585 c1.735,0.578,3.552,0.873,5.38,0.875c1.832-0.003,3.653-0.297,5.393-0.87l102.4-34.133c2.515-0.84,4.8-2.254,6.673-4.13 l224.802-224.802C484.25,86.023,484.253,44.657,458.742,19.142z M434.603,87.419L212.736,309.286l-66.287,22.135l22.067-66.202 L390.468,43.353c12.202-12.178,31.967-12.158,44.145,0.044c5.817,5.829,9.095,13.72,9.12,21.955 C443.754,73.631,440.467,81.575,434.603,87.419z"/>
                                    </svg>', ['type' => 'submit', 'class' => 'btn goback float-right', 'onclick' => "return confirm('Do you want to change the status of the current deliverer ?')"]) !!}
                                {!! Form::close() !!}
                                <a href="{{ route('deliveries.index') }}" class="btn goback float-right">Back</a>
                            </div>
                            <div class="card-body">
                                @include('deliveries.show_fields')
                            </div>
                        </div>
                        <div class="card my-3">
                            @php
                                $vehicle = \App\Models\Vehicle::where(['deliverer_id' => $delivery->id])->first();
                            @endphp
                            <div class="card-header">
                                <strong class="py-2 my-3">Vehicle Detail</strong>
                                {!! Form::open(['route' => ['admin-vehicle-change_state', $vehicle->id], 'method' => 'post']) !!}
                                    <input type="hidden" value="verified">
                                    {!! Form::button('Change status verified', ['type' => 'submit', 'class' => 'btn btn-success goback float-right', 'onclick' => "return confirm('Do you want to change the status of the current deliverer ?')"]) !!}
                                {!! Form::close() !!}
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            {!! Form::label('is_active', 'Is Active:') !!}
                                            <p>{!! $vehicle->is_active ? "<span style='color:green;''>Yes</span>"  : "<span style='color:red;''>No</span>" !!}</p>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <!-- Numero Police Field -->
                                        <div class="form-group">
                                            {!! Form::label('numero_police', 'Numero Police:') !!}
                                            <p>{{ $vehicle->numero_police }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            {!! Form::label('immatriculation', 'Immatriculation:') !!}
                                            <p>{{ $vehicle->immatriculation }}</p>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            {!! Form::label('numero_serie', 'Numero Serie:') !!}
                                            <p>{{ $vehicle->numero_serie }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <!-- Is Verified Field -->
                                        <div class="form-group">
                                            {!! Form::label('is_verified', 'Is Verified:') !!}
                                            <p>{!! $vehicle->is_verified ? "<span style='color:green;''>Yes</span>"  : "<span style='color:red;''>No</span>" !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="col-lg-3 card-right">
                        <div class="card">
                            <div class="card-header">
                                <strong> Actions </strong>
                            </div>
                            <div class="card-body">
                                <a class="btn btn-block card-right-btn" href="{{ route('backoffice-delivery-orderassignement-index', ['delivery_id' => $delivery->id]) }}">Assignements </a>
                                <a class="btn btn-block card-right-btn" href="{{ route('backoffice-delivery-order-index', ['delivery_id' => $delivery->id]) }}">Orders </a>
                                <a class="btn btn-block card-right-btn" href="{{ route('backoffice-delivery-payment-history-index', ['delivery_id' => $delivery->id]) }}">Payment history</a>
                                <a class="btn btn-block card-right-btn" href="{{ route('backoffice-delivery-payment-request-index', ['delivery_id' => $delivery->id]) }}">Payment Request</a>
                            </div>
                        </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
