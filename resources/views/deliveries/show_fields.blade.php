<div class="row">
    <div class="col-6">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('first_name', 'First name:') !!}
            <p>{{ $delivery->first_name }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('last_name', 'Last Name:') !!}
            <p>{{ $delivery->last_name }}</p>
        </div>
    </div>
</div>

{{-- <!-- Surname Field -->
<div class="form-group">
    {!! Form::label('surname', 'Surname:') !!}
    <p>{{ $delivery->surname }}</p>
</div> --}}

<div class="row">
    <div class="col-6">
        <!-- Email Field -->
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            <p>{{ $delivery->email }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Email Field -->
        <div class="form-group">
            {!! Form::label('email_verified', 'Email verified :') !!}
            <p>{!!  $delivery->email_verified ? "<span style='color:green;''>Yes</span>"  : "<span style='color:red;''>No</span>"!!}</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <!-- Dial Code Field -->
        <div class="form-group">
            {!! Form::label('dial_code', 'Dial Code:') !!}
            <p>{{ $delivery->dial_code }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Phone Number Field -->
        <div class="form-group">
            {!! Form::label('phone_number', 'Phone Number:') !!}
            <p>{{ $delivery->phone_number }}</p>
        </div>        
    </div>
</div>
<div class="row">
    <div class="col-6">
        <!-- Lng Field -->
        <div class="form-group">
            {!! Form::label('lng', 'Latest Longitude:') !!}
            <p>{{ $delivery->lng ?? 'not registered' }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Lat Field -->
        <div class="form-group">
            {!! Form::label('lat', 'Latest Latitude:') !!}
            <p>{{ $delivery->lat ?? 'not registered' }}</p>
        </div>    
    </div>
</div>

<div class="row">
    <div class="col-6">
        {!! $delivery->is_active ? '<span style="color:green;">Active</span>' : '<span style="color:red;">Inactive</span>' !!}
    </div>
</div>
<!-- Password Field -->
{{-- <div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{{ $delivery->password }}</p>
</div> --}}



<!-- Phone Field -->
{{-- <div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $delivery->phone }}</p>
</div> --}}

<!-- Delivery Type Id Field -->
{{-- <div class="form-group">
    {!! Form::label('delivery_type_id', 'Delivery Type Id:') !!}
    <p>{{ $delivery->delivery_type_id }}</p>
</div> --}}

<!-- Vehicle Type Id Field -->
{{-- <div class="form-group">
    {!! Form::label('vehicle_type_id', 'Vehicle Type Id:') !!}
    <p>{{ $delivery->vehicle_type_id }}</p>
</div> --}}


<!-- Created At Field -->
{{-- <div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $delivery->created_at }}</p>
</div> --}}

<!-- Updated At Field -->
{{-- <div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $delivery->updated_at }}</p>
</div> --}}