<div class="table-responsive-sm">
    <table class="table table-striped" id="menuses-table">
        <thead>
            <tr>
                <th>Title</th>
        <th>Slug</th>
        <th>Restaurant Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($menuses as $menus)
            <tr>
                <td>{{ $menus->title }}</td>
            <td>{{ $menus->slug }}</td>
            <td>{{ $menus->restaurant_id }}</td>
                <td>
                    {!! Form::open(['route' => ['menuses.destroy', $menus->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('menuses.show', [$menus->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('menuses.edit', [$menus->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>