<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $restaurantUsers->name }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $restaurantUsers->email }}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{{ $restaurantUsers->password }}</p>
</div>

<!-- Dial Code Field -->
<div class="form-group">
    {!! Form::label('dial_code', 'Dial Code:') !!}
    <p>{{ $restaurantUsers->dial_code }}</p>
</div>

<!-- Phone Number Field -->
<div class="form-group">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    <p>{{ $restaurantUsers->phone_number }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $restaurantUsers->phone }}</p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    <p>{{ $restaurantUsers->is_active }}</p>
</div>

<!-- Restaurant Id Field -->
<div class="form-group">
    {!! Form::label('restaurant_id', 'Restaurant Id:') !!}
    <p>{{ $restaurantUsers->restaurant_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $restaurantUsers->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $restaurantUsers->updated_at }}</p>
</div>

