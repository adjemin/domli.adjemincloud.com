@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8" style="margin-top: 2%">
                <div class="card" style="width: 40rem;">
                    <div class="card-body">
                        <h4 class="card-title">{{__('auth.verify.title')}}</h4>
                        @if (session('resent'))
                            <p class="alert alert-success" role="alert">{{ __('auth.verify.success') }}</p>
                        @endif
                        <p class="card-text"> {{ __('auth.verify.explain')}} </p>
                        <a href="{{ route('verification.resend') }}">{{ __('auth.verify.button')  }}</a>.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection