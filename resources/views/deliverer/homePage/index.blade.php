@extends('layouts.index')

{{-- @include('frontOffice.customer.homePage.dishProgressPopUp') --}}
@section('content')
    @include('deliverer.homePage.sectionContainer.banner')
    @include('deliverer.homePage.sectionContainer.howItWorks')
    @include('deliverer.homePage.sectionContainer.newPaper')
    @include('deliverer.homePage.sectionContainer.suggestions')
@endsection
