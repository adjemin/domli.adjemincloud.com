<header class="col-12 delivery-home">
    <div class="header-content row">
        <div class="card-top col-12">
            <div class="container">
                <div class="row card-top-content">
                    <div class="logo col-lg-2 col-md-4 col-4">
                        <a href="#">
                            <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="50" alt="...">
                        </a>
                    </div>
                    <menu class="col-lg-10 col-md-8 col-8">
                        <input type="checkbox" id="check">
                        <label for="check">
                            <i class="fas fa-bars" id="btn"></i>
                            <i class="fas fa-times" id="cancel"></i>
                        </label>
                        <ul class="menu-ul">
                            <li class="menu"><a href="{{ route('profileAuth') }}">Home page</a></li>
                            <li class="menu"><a href="{{ route('restaurants') }}">About us</a></li>
                            <li class="menu"><a href="{{ route('contactUs') }}">Contact us</a>
                            </li>
                            {{-- <li><a href="{{ route('login') }}" class="btn signIn" id="homSignIn">login / sign up</a></li> --}}
                        </ul>
                    </menu>
                </div>
            </div>
        </div>
        <section class="delivery-login-logup container">
            <div class="row content">
                <div class="text-card col-md-6">
                    <div class="text-card-content">
                        <div class="logo-container">
                            <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="90" alt="">
                        </div>
                        <h6>Our offering are flexible sos</h6>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. amet consectetur adipisicing elit.amet consectetur adipisicing elit.</p>
                    </div>
                </div>
                <div class="form-tab-container col-md-6">
                    <nav class="nav nav-tabs row">
                        <a href="#login" class="nav-item nav-link active col-6" data-toggle="tab">Already a member</a>
                        <a href="#logup" class="nav-item nav-link col-6" data-toggle="tab">I am new here</a>
                    </nav>
                    <div class="tab-content">
                        <div class="tab-pane active" id="login">
                            {{--<form id="customer-form" action="{{ route('profileAuth') }}" method="post">--}}
                            <form id="customer-form" action="{{ route('delivery_login_post') }}" method="post">
                                @csrf
                                <div class="row">
                                    <input type="mail" name="email" id="customerEmail" placeholder="E-mail adress" class="input col-12">

                                    <input type="password" name="password" id="customerPassword" placeholder="Password" class="input col-12">

                                    <div class="remember col-6 pl-0">
                                        <input type="checkbox" name="remember" id="remember">
                                        <label class="remember-label" for="remember">Remember me</label>
                                    </div>
                                    <div class="col-6 forgoten-password pr-0">
                                        <a href="#" style="float: right;">Forget password ?</a>
                                    </div>
                                    <button type="submit" class="submit-btn col-12 hvr-wobble-vertical">
                                        Sign up
                                    </button>
                                </div>

                            </form>
                        </div>
                        <div class="tab-pane" id="logup">
                            {{--<form id="customer-form" action="{{ route('deliveryOtpPage') }}">--}}
                            <form id="customer-form" action="{{ route('delivery_register_post') }}" method='post'>
                                @csrf
                                <div class="row">
                                    <input type="text" name="name" id="customerName" placeholder="Name" class="input col-12">

                                    <input type="text" name="lastname" id="customerlastname" placeholder="Last name" class="input col-12">

                                    <input type="mail" name="email" id="customerEmail" placeholder="E-mail" class="input col-12">

                                    <div class="col-12 form-group">
                                        <div class="row">
                                            <input type="tel" name="phone_number" class="form-control col-12" id="phone_number">

                                            <input type="hidden" name="dial_codePhoneNumber" id="dial_codePhoneNumber">

                                            <input type="hidden" name="phone_numberPhoneNumber" id="phone_numberPhoneNumber">

                                            <input type="hidden" name="country_codePhoneNumber" id="country_codePhoneNumber" value="" /> <br>

                                            <span id="valid-msgPhoneNumber" class="hide"></span>
                                            <span id="error-msgPhoneNumber" class="hide" style="color:red;"></span>
                                            {{-- <input type="tel" name="phone_number" id="phone_number"
                                            class="input col-12"  required>

                                            <input type="hidden" name="dial_code" id="dial_code">

                                            <!-- <input type="hidden" name="phone_number_customer" id="phone_number_customer"> -->

                                            <input type="hidden" name="country_code" id="country_code" value="" />

                                            <span id="valid-msgPhoneNumber" class="hide"></span>
                                            <span id="error-msgPhoneNumber" class="hide" style="color:red;"></span> --}}

                                        </div>
                                    </div>

                                    <input type="password" name="password" id="customerPassword" placeholder="Password" class="input col-12">

                                    <input type="password" name="password_confirmation" id="customerPassword" placeholder="Confirm password" class="input col-12">

                                    <!-- <input type="phone" name="customerNumber" id="customerNumber" placeholder="07070707" class="input col-12"> -->

                                    <div class="driver col-6 pl-0">
                                        <input type="checkbox" name="driverDelivery" id="driverDelivery">
                                        <label class="driverDelivery-label" for="driverDelivery">Sign up to driver</label>
                                    </div>

                                    <p>I agree to the <a href="#">Prevacy policy</a> and <a href="#">terms of service</a></p>

                                    <button type="submit" class="submit-btn col-12 hvr-wobble-vertical">
                                        Sign up
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</header>
