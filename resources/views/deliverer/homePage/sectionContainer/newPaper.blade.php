<section class="delivery-newPaper col-12">
	<div class="container">
		<div class="row">
            <div class="col-12 delivery-newPaper-container">
                <div class="row">
                    <div class="delivery-newPaper-img col-lg-6" style="background-image: url('{{ asset('img/delivery.jpg') }}')">
                        {{-- <img src="{{ asset('img/') }}" alt="..."> --}}
                    </div>
                    <div class="col-lg-6 delivery-newPaper-text">
                        <h1>Deliver with Domli</h1>
                        <p>
                            Ready to earn money by delivering food? Find out if Uber Eats is live in your city.
                        </p>
                        <a href="#" class="delivery-newPaper-btn">See where Delivery is available</a>
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>
