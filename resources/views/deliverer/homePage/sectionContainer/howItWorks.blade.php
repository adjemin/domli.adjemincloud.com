<section class="delivery-howItWorks-section row">
	<div class="container">
		<div class="row">
			<div class="section-title col-md-12 col-lg-12 col-sm-12">
				<h3>How it works</h3>
            </div>
            <div class="col-12">
                <div class="delivery-howItWork-content">
                    <div class="row">
                        @for ($i = 0; $i < 3; $i++)
                            <div class="col-lg-4 howItWorks-text">
                                <div class="howItWorks-text-content hvr-bounce-to-left">
                                    <h1>Lorem ipas</h1>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, officiis?  ipsum dolor sit amet, consectetur adipisicing elit. Earum, officiis?
                                    </p>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>
