@extends('layouts.index')

{{-- @include('frontOffice.customer.pageParts.pagesHeader') --}}

@section('content')
    <section class="otp-page container">
        <div class="row content">
            <div class="form-container col-12">
                <div class="form-content">
                    <div class="tab-pane ">
                        <form id="customer-form" action="{{ route('profileAuth') }}">
                            <h3>Enter the confirmation code that send to you</h3>
                            <div class="row">
                                <input type="number" name="customerNumber" id="customerNumber" placeholder="00000" class="input col-12">

                                <button type="submit" class="submit-btn col-12 hvr-wobble-vertical">
                                    confirm
                                </button>
                                <a href="#" class="col-12 sample-btn">Resend code ?</a>
                                <a href="#" class="col-12 sample-btn">Go back</a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
