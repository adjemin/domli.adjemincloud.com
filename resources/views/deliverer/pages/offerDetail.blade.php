@extends('layouts.index')

@include('deliverer.pageParts.pagesHeader')

@section('content')
    @php
        $food = \App\Models\Food::where('uuid', $cartDetail->id)->first();
    @endphp
    <section class="pagesSlide col-12">
        <div class="slide-title row">
            <div class="container">
                <a href="" >
                    <h2 class="col-12"><i class="fas fa-arrow-left"></i>{{$food->title}} details</h2>
                </a>
            </div>
        </div>
    </section>
    <section class="page-section container">
        <div class="row">
            <div class="col-lg-10 offerDetail-container" style="margin-left: auto; margin-right: auto">
                <div class="offerDetail-content">
                    <table class="info-delivery col-12" celspacing="0" cellpadding="15" border="0">
                        <tr class="photo-profile">
                            <td colspan="2">
                                <div class="photo-container" style="background-image: url('{{ $food->image }}'); background-size: cover; background-position: center">
                                </div>
                            </td>
                        </tr>
                        <tr class="name">
                            <td>
                                Name :
                            </td>
                            <td>
                                {{$food->title}}
                            </td>
                        </tr>
                        <tr class="surname">
                            <td>
                                restaurant address : 
                            </td>
                            <td>
                                <a href="{{route('restaurant',['uuid' => $food->restaurant->uuid ?? $food->restaurant->id])}}">
                                    {{$food->restaurant->restaurant_adress_name}}
                                </a>
                            </td>
                        </tr>
                        {{--<tr class="surname">
                            <td>
                                Quantity : 
                            </td>
                            <td>
                                {{$cartDetail->qty}}
                            </td>
                        </tr>--}}
                        @foreach($cartDetail->options as $key => $item)
                        <tr class="surname">
                            @if($key != 'part_items')
                                <td>
                                    {{$key}} : 
                                </td>
                            @else
                                @foreach($item as $v)
                                <td>
                                    {{array_keys($v)[0]}} : 
                                </td>
                                @endforeach
                            @endif
                            @if($key == 'parts')
                                <td>
                                    {{implode(', ', array_values($item))}}
                                </td>
                            @elseif($key == 'part_items')
                                @foreach($item as $v)
                                    <td>
                                        {{array_values($v)[0]}} 
                                    </td>
                                @endforeach
                            @else
                                <td>
                                    {{$item}}
                                </td>
                            @endif
                        </tr>
                        @endforeach
                        
                        <tr class="surname">
                            <td>
                                Quantity : 
                            </td>
                            <td>
                                {{$cartDetail->qty}}
                            </td>
                        </tr>

                        {{--<tr class="date">
                            <td>
                                date :
                            </td>
                            <td>
                                to day
                            </td>
                        </tr>
                        <tr class="time">
                            <td>
                                Ready :
                            </td>
                            <td>
                                20 minutes
                            </td>
                        </tr>
                        <tr class="Subtotal">
                            <td>
                                Sub total :
                            </td>
                            <td>
                                $50
                            </td>
                        </tr>
                        <tr class="delivery">
                            <td>
                                delivery :
                            </td>
                            <td>
                                $20
                            </td>
                        </tr>--}}
                        <tr class="total">
                            <td>
                                Total pay :
                            </td>
                            <td>
                                {{$cartDetail->price}}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
