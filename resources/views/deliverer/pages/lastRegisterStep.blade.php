@extends('layouts.index')

@include('frontOffice.delivery.pageParts.pagesHeader')

@section('content')
    <section class="aboutSlide col-12">
        <div class="slide-title row">
            <div class="container">
                <a href="#" >
                    <h2 class="col-12">Welcome delivery name</h2>
                </a>
            </div>
        </div>
    </section>
    <section class="last-register-step container">
        <div class="row content">
            <div class="form-container col-12">
                <div class="form-content">
                    <form id="delivery-form">
                        <div class="form-container row">
                            <h3 class="col-12 section-title">Require step</h3>
                            <p class="title-desc col-12">
                                Here's what you need to do to set up your account.
                            </p>
                            <div class="choice-container col-12">
                                <div class="row">
                                    <div class="item background-check col-12">
                                        <input type="radio" name="car" id="vehicoleType" hidden>
                                        <input type="checkbox" name="car" id="vehicoleType1" class="box-checked">
                                        <label class="vehicoleType-label" for="vehicoleType1">
                                            <span>Background check</span>
                                            <p>Recommended next step</p>
                                        </label>
                                    </div>
                                    <div class="item PhotoID col-12">
                                        <input type="radio" name="car" id="vehicoleType" hidden>
                                        <input type="checkbox" name="car" id="scooter" class="box-checked">
                                        <label class="vehicoleType-label" for="scooter">
                                            <span>Photo ID (Driver's License or State ID)</span>
                                            <p>Ready to begin</p>
                                        </label>
                                    </div>
                                    <div class="item PhotoProfile col-12 hvr-ripple-out">
                                        <input type="radio" name="car" id="vehicoleType" hidden>
                                        <input type="checkbox" name="car" id="vehicoleType1" class="box-checked">
                                        <label class="vehicoleType-label" for="vehicoleType1">
                                            <span>Profile Photo</span>
                                            <p>Ready to begin</p>
                                        </label>
                                    </div>
                                    <button type="submit" class="submit-btn col-12 hvr-wobble-vertical">
                                        Continue
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
