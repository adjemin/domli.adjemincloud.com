@extends('layouts.index')

{{-- @include('frontOffice.delivery.pageParts.pagesHeader') --}}

@section('content')
    <section class="choose-move-maens-section container">
        <div class="row content">
            <div class="form-container col-12">
                <div class="form-content">
                    <form id="delivery-form">
                        <div class="form-container row">
                            <h3 class="col-12 section-title">Choose how you’d like to earn with Domli</h3>
                            <div class="choice-container col-12">
                                <div class="row">
                                    <div class="vehicoleType col-12">
                                        <input type="radio" name="car" id="vehicoleType" hidden>
                                        <input type="checkbox" name="car" id="vehicoleType1" class="box-checked">
                                        <label class="vehicoleType-label" for="vehicoleType1">
                                            <span>Delivery by car</span>
                                            <p>You are at least 19 years old and have a vehicle from 1997 or later.</p>
                                        </label>
                                    </div>
                                    <div class="vehicoleType col-12">
                                        <input type="radio" name="car" id="vehicoleType" hidden>
                                        <input type="checkbox" name="car" id="scooter" class="box-checked">
                                        <label class="vehicoleType-label" for="scooter">
                                            <span>Scooter</span>
                                            <p>I have a valid driver's license and own a scooter or moped.</p>
                                        </label>
                                    </div>
                                    <div class="vehicoleType col-12">
                                        <input type="radio" name="car" id="vehicoleType" hidden>
                                        <input type="checkbox" name="car" id="vehicoleType1" class="box-checked">
                                        <label class="vehicoleType-label" for="vehicoleType1">
                                            <span>Delivery by bicycle</span>
                                            <p>You are at least 18 years old and you have a bicycle.</p>
                                        </label>
                                    </div>
                                    <button type="submit" class="submit-btn col-12 hvr-wobble-vertical">
                                        Continue
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
