<menu class="col-lg-10 col-md-8 col-8">
    <input type="checkbox" id="check">
    <label for="check">
        <i class="fas fa-bars" id="btn"></i>
        <i class="fas fa-times" id="cancel"></i>
    </label>
    <ul class="menu-ul">
        @if(Auth::check() || Auth::guard('deliverer')->check() || Auth::guard('customer')->check())
            <li class="menu"><a href="/customer/home">Home page</a></li>
            <li class="menu"><a href="#">Become &nbsp; <i class="fas fa-chevron-down"></i></a>
                <ul class="submenu">
                    {{-- <li><a href="cu">Customer</a></li> --}}
                    <li><a href="/delivery/home">Delivery</a></li>
                    {{-- <li><a href="#">Restaurant</a></li> --}}
                </ul>
            </li>
            <li class="menu">
                <a href="{{ route('restaurants') }}">Restaurants</a>
            </li>
            <li class="menu">
                <a href="{{ route('contactUs') }}">Contact us</a>
            </li>
            <li class="menu">
                <a href="{{ route('aboutUs') }}">About us</a>
            </li>
            <li class="profile">
                <a href="#" class="shopping-cart">
                    <svg id="Capa_1" enable-background="new 0 0 512 512" height="20" viewBox="0 0 512 512" width="20" xmlns="http://www.w3.org/2000/svg">
                        <path d="m512 154.803h-91.787l-113.607-113.606-21.212 21.213 92.394 92.394h-243.575l92.394-92.394-21.213-21.213-113.607 113.606h-91.787v100h33.522l40 216h364.955l40-216h33.523zm-151 210h-50v-40h50zm30-40h44.004l-7.407 40h-36.597zm-314.004 0h44.004v40h-36.597zm74.004 0h50v40h-50zm80 0h50v40h-50zm-30 70v46h-50v-46zm30 0h50v46h-50zm80 0h50v46h-50zm0-100v-40h50v40zm-30 0h-50v-40h50zm-80 0h-50v-40h50zm-80 0h-49.56l-7.407-40h56.967zm-31.041 100h31.041v46h-22.522zm301.041 46v-46h31.041l-8.519 46zm49.56-146h-49.56v-40h56.967zm41.44-70h-452v-40h31.787l-5.394 5.394 21.213 21.213 26.606-26.606h303.574l26.606 26.606 21.213-21.213-5.394-5.394h31.789z"/>
                    </svg>
                    <span class="number">{{ Cart::count() }}</span>
                </a>
                <span class="location-box">
                    <span class="location-pin">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" width="35" height= "35" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g>
                                <g>
                                    <path d="M256,0C161.896,0,85.333,76.563,85.333,170.667c0,28.25,7.063,56.26,20.49,81.104L246.667,506.5
                                        c1.875,3.396,5.448,5.5,9.333,5.5s7.458-2.104,9.333-5.5l140.896-254.813c13.375-24.76,20.438-52.771,20.438-81.021
                                        C426.667,76.563,350.104,0,256,0z M256,256c-47.052,0-85.333-38.281-85.333-85.333c0-47.052,38.281-85.333,85.333-85.333
                                        s85.333,38.281,85.333,85.333C341.333,217.719,303.052,256,256,256z"/>
                                </g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                        </svg>

                    </span>
                    @if(Auth::check() || Auth::guard('deliverer')->check() || Auth::guard('customer')->check())
                        <span>
                            <h2 style="color: #000;">{{ Auth::guard('customer')->user()->city ?? Auth::guard('deliverer')->user()->city }}<br> <nav>{{ Auth::guard('customer')->user()->region_name }}</nav></h2>
                        </span>
                    @endif
                </span>
            </li>
            <li class="menu profile">
                <span class="profile-ph">
                    <a href="#">
                        <img src="{{ Auth::guard('customer')->user()->photo_profile ?? Auth::guard('deliverer')->user()->photo_profile ?? Auth::user()->photo_profile ?? asset('img/user.png') }}" alt="..." width="40" height="40">
                    </a>
                </span>
                <ul class="submenu">
                    <li><a href="/customer/profile">Profile</a></li>
                    <li><a href="/customer/orders">Orders</a></li>
                    <li><a href="{{ url('/customer/logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ url('/customer/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                    </li>
                    {{-- <li><a href="#">Restaurant</a></li> --}}
                </ul>
            </li>
        @else
            <li class="menu"><a href="/">Home page</a></li>
            <li class="menu"><a href="#">Become &nbsp; <i class="fas fa-chevron-down"></i></a>
                <ul class="submenu">
                    {{-- <li><a href="cu">Customer</a></li> --}}
                    <li><a href="{{ route('delivererLogin') }}">Delivery</a></li>
                    {{-- <li><a href="#">Restaurant</a></li> --}}
                </ul>
            </li>
            <li class="menu">
                <a href="{{ route('restaurants') }}">Restaurants</a>
            </li>
            <li class="menu">
                <a href="{{ route('contactUs') }}">Contact us</a>
            </li>
            <li class="menu">
                <a href="{{ route('aboutUs') }}">About us</a>
            </li>
            <li><a href="/customer/login" class="btn signIn" id="homSignIn">login / sign up</a></li>
        @endif
    </ul>
</menu>
