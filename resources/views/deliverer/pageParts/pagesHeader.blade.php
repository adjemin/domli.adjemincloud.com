
<div class="page card-top col-12">
    <div class="container">
        <div class="row card-top-content">
            <div class="logo col-lg-2 col-md-4 col-4" >
                @if(Auth::check() || Auth::guard('deliverer')->check() || Auth::guard('customer')->check())
                    <a href="/customer/home">
                        <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="50" alt="...">
                    </a>
                @else
                    <a href="/">
                        <img src="{{ asset('img/LOGO-DOMLI.png') }}" class="my-2" height="50" alt="...">
                    </a>
                @endif
            </div>
            @include('customer.pageParts.menu')
            {{-- @include('customer.pageParts.gestMenu') --}}
        </div>
    </div>
</div>
