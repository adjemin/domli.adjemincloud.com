
<div class="row">
    <div class="col-lg-6">
        <!-- Restaurant Id Field -->
        <div class="form-group">
            {!! Form::label('restaurant_id', 'Restaurant:') !!}
            <p>{{ $restaurantMenu->restaurant->title }}</p>
        </div>
    </div>
    <div class="col-lg-6">
        <!-- Title Field -->
        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            <p>{{ $restaurantMenu->title }}</p>
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <!-- Slug Field -->
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            <p>{{ $restaurantMenu->slug }}</p>
        </div>    
    </div>
</div>