<div class="table-responsive-sm">
    <table class="table table-striped" id="restaurantMenus-table">
        <thead>
            <tr>
                <th>Restaurant Id</th>
        <th>Title</th>
        <th>Slug</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($restaurantMenus as $restaurantMenu)
            <tr>
                <td>{{ $restaurantMenu->restaurant_id }}</td>
            <td>{{ $restaurantMenu->title }}</td>
            <td>{{ $restaurantMenu->slug }}</td>
                <td>
                    {!! Form::open(['route' => ['restaurantMenus.destroy', $restaurantMenu->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('restaurantMenus.show', [$restaurantMenu->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('restaurantMenus.edit', [$restaurantMenu->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>