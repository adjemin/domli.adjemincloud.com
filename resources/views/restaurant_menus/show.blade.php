@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('restaurantMenus.index') }}">Restaurant Menu</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ \Auth::check() ? 'admin/restaurant/$restaurantMenus->restaurant->uuid/menu' : '/restaurant/$restaurantMenus->restaurant->uuid/menu' }}" class="btn btn-primary">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('restaurant_menus.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
