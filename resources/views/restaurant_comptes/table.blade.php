<div class="table-responsive-sm">
    <table class="table table-striped" id="restaurantComptes-table">
        <thead>
            <tr>
                <th>Restaurant Id</th>
        <th>Compte</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($restaurantComptes as $restaurantCompte)
            <tr>
                <td>{{ $restaurantCompte->restaurant_id }}</td>
            <td>{{ $restaurantCompte->compte }}</td>
                <td>
                    {!! Form::open(['route' => ['restaurantComptes.destroy', $restaurantCompte->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('restaurantComptes.show', [$restaurantCompte->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('restaurantComptes.edit', [$restaurantCompte->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>