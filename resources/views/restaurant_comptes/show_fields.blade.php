<!-- Restaurant Id Field -->
<div class="form-group">
    {!! Form::label('restaurant_id', 'Restaurant Id:') !!}
    <p>{{ $restaurantCompte->restaurant_id }}</p>
</div>

<!-- Compte Field -->
<div class="form-group">
    {!! Form::label('compte', 'Compte:') !!}
    <p>{{ $restaurantCompte->compte }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $restaurantCompte->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $restaurantCompte->updated_at }}</p>
</div>

