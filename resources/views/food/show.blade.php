{{--@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('food.index') }}">Food</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('food.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('food.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection--}}

@extends('layouts.app')

@section('css')
    <style>
        .bg-img{
            position: relative;
            color: white;
            width: auto;
            height: 80vh;
            background-position: center;
            background-size: contain;
            background-repeat: no-repeat;
            background-image:  linear-gradient(rgba(0, 0, 0, .5), rgba(0, 0, 0, .5)), url("{{$food->image}}");
        }

        .title{
            font-family: 'cursive';
            font-size: 18px;
        }

        .default{
            font-family: 'sans-serif';
            font-size: 16px;
        }

        h1{
            font-family: 'papyrus';
            letter-spacing: 4px;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
        }
    </style>
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/backOffice/restaurants/'.$food->restaurant->uuid) }}">Restaurant</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ url('restaurant/'.$food->restaurant->uuid.'/foods') }}">Food</a>
        </li>
        <li class="breadcrumb-item active">Detail</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            @include('coreui-templates::common.errors')
            <div class="row">
                <div class="col-lg-{!! $food->has_part == 1 ? 9 : 12!!}">
                    <div class="card">
                        <div class="card-header">
                            <strong class="mt-3">Details</strong>
                            <a href="{{ url('/restaurant/'.$food->restaurant->uuid.'/foods') }}" class="float-right btn goback">Go back</a>
                        </div>
                        <div class="card-body">
                            @include('food.show_fields')
                        </div>
                    </div>
                </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">
                                <strong> Actions </strong>
                            </div>
                            <div class="card-body">
                                @if(Auth::guard('restaurant')->check())
                                <form action="/food/update/stock/{{$food->id}}" method="post">
                                    <div class="row mb-3">
                                        @csrf
                                        <div class="form-group">
                                            {!! Form::label('quantity', 'Quantity:') !!}
                                            {!! Form::number('quantity', null, ['placeholder' => 'Quantity to add to previous', 'class' => 'form-control', 'min' => '0']) !!}
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="Submit" class="btn card-right-btn">
                                        </div>
                                    </div>
                                </form>
                                @endif
                                @if($food->has_part == 1)
                                    <a class="btn btn-block btn-outline-primary active" href="{{ url('/restaurant/'.$food->restaurant->uuid.'/'.$food->id.'/food_part') }}">Option</a>
                                @endif
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
