{{--
<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image') !!}
</div>
<div class="clearfix"></div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    {!! Form::number('currency_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_name', 'Currency Name:') !!}
    {!! Form::text('currency_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('food.index') }}" class="btn btn-secondary">Cancel</a>
</div>
--}}

<!-- Restaurant Id Field -->
<div class="form-group col-sm-6">
    <input type="hidden" name="restaurant_id" id="restaurant_id" value="{{$restaurant->id}}"/>
</div>
<!-- Menu Id Field -->

{{-- @dd($restaurant->categories) --}}
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('menu_id', 'Menu :') !!}
        <select name="menu_id" class="form-control" id="menu_id">
            @foreach($restaurant->menus as $menu)
                <option value="{{$menu->id}}" {{ old("menu_id") == $menu->id ? "selected" : "" }}>{{$menu->title}}</option>
            @endforeach
        </select>
    </div>
    @php
    //dd($restaurant->categories);
    @endphp
    <!-- Category Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('category_id', 'Categorie :') !!}
        <select name="category_id" id="category_id" class="form-control">
            @foreach($restaurant->categories as $categorie)
                @if(!is_null($categorie->category))
                    <option value="{{$categorie->category->id}}" {{ old("category_id") == $categorie->category->id ? "selected" : "" }}>{{$categorie->category->title ?? ''}}</option>
                @endif
            @endforeach
        </select>
    </div>
</div>
<div class="row">
    <!-- Title Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('title', 'Titre:') !!}
        @if(isset($food))
            {!! Form::text('title', $food->title, ['class' => 'form-control']) !!}
        @else
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        @endif
    </div>
    <!-- Base Price Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('price', 'Base Price:') !!}
        @if(isset($food))
            {!! Form::text('price', $food->price, ['class' => 'form-control']) !!}
        @else
            {!! Form::text('price', null, ['class' => 'form-control', 'value' => '0']) !!}
        @endif
    </div>
</div>
<div class="row">
    <!-- Description Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('description', 'Description:') !!}
        @if(isset($food))
            <textarea name="description" id="description" class='form-control'>{{$food->description}}</textarea>
        @else
            <textarea name="description" id="description" class='form-control'></textarea>
        @endif
    </div>
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="currency_name" value="Dollar(s)">
    <!-- Currency Code Field -->
    {{--<div class="form-group col-sm-6">
        {!! Form::label('currency_code', 'Currency codex:') !!}
        @if(isset($food))
            {!! Form::text('currency_code', $food->currency_code, ['class' => 'form-control']) !!}
        @else
            {!! Form::text('currency_code', null, ['class' => 'form-control']) !!}
        @endif
    </div>--}}

    <!-- Currency Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('currency_name', 'Currency') !!}
        <select name="currency_name" class="form-control">
            <option value="USD">Dollar</option>
        </select>
        {{-- @if(isset($food))
            {!! Form::text('currency_name', $food->currency_name, ['class' => 'form-control']) !!}
        @else
            {!! Form::text('currency_name', null, ['class' => 'form-control']) !!}
        @endif --}}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('image', 'Image:') !!}
        <input type="file" name="image_file" id="image" class="image" onchange='loadFile(event)' required>
    </div>

    <!-- Has Part Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('has_part', 'Has Part:') !!}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        @if(isset($food))
            <input type="checkbox" name="has_part" id="has_part" value="1" {{ $food->has_part == 1  ? 'checked' : '' }}>
        @else
            <input type="checkbox" name="has_part" id="has_part" value="1">
        @endif
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
        @if(isset($food))
            <img src="{{$food->image}}" id="output_cover" height="100"  alt="">
        @else
            <img src="" id="output_cover" height="100"  alt="">
        @endif
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('quantity', 'Quantity:') !!}
        {!! Form::number('quantity', null, ['class' => 'form-control', 'min' => '0', 'value' => 0]) !!}
    </div>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('food.index') }}" class="btn btn-secondary">cancel</a>
</div>
