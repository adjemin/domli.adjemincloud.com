{{--
<!-- Restaurant Id Field -->
<div class="form-group">
    {!! Form::label('restaurant_id', 'Restaurant Id:') !!}
    <p>{{ $food->restaurant_id }}</p>
</div>

<!-- Menu Id Field -->
<div class="form-group">
    {!! Form::label('menu_id', 'Menu Id:') !!}
    <p>{{ $food->menu_id }}</p>
</div>

<!-- Food Category Id Field -->
<div class="form-group">
    {!! Form::label('food_category_id', 'Food Category Id:') !!}
    <p>{{ $food->food_category_id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $food->title }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $food->description }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{{ $food->image }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $food->price ?? $food->base_price }}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{{ $food->currency_code }}</p>
</div>

<!-- Currency Name Field -->
<div class="form-group">
    {!! Form::label('currency_name', 'Currency Name:') !!}
    <p>{{ $food->currency_name }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $food->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $food->updated_at }}</p>
</div>
--}}

<div class="mt-3 py-2">
    <div class="px-4">
        <div class="bg-img">
            <h1>{{ $food->title }}</h1>
        </div>
        <div class="text-center py-2">
            <small>{{ $food->description }}</small>
        </div>
        <div class="mt-3 py-2 info-container">
            <div class="row info-content">
                <div class="col-12 title">
                    <h2>Informations complémentaires</h2>
                </div>
                <div class="col-12 my-2">
                    <div class="row">
                        <div class="col-12 info-card">
                            <!-- Restaurant Id Field -->
                            <div class="form-group">
                                {!! Form::label('quantity', 'Quantity In stock:', ['class' => 'title']) !!}
                                <p>{{ $food->quantity ?? 0 }}</p>
                            </div>
                        </div>
                        @if(!is_null($food->restaurant))
                            <div class="col-12 info-card">
                                <!-- Restaurant Id Field -->
                                <div class="form-group">
                                    {!! Form::label('restaurant_id', 'Restaurant :', ['class' => 'title']) !!}
                                    <p>{{ $food->restaurant->name ?? '' }}</p>
                                </div>
                            </div>
                        @endif
                        @if(!is_null($food->menu))
                            <div class="col-12 info-card">
                                <!-- Menu Id Field -->
                                <div class="form-group">
                                    {!! Form::label('menu_id', 'Menu :', ['class' => 'title']) !!}
                                    <p>{{ $food->menu->title ?? '' }}</p>
                                </div>
                            </div>
                        @endif
                        @if(!is_null($food->category))
                            <div class="col-12 info-card">
                                <!-- Category Id Field -->
                                <div class="form-group">
                                    {!! Form::label('category_id', 'Categorie:', ['class' => 'title']) !!}
                                    <p>{{ $food->category->title ?? '' }}</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row info-content">
                <div class="col-lg-6 col-sm-6 lat-card">
                    <!-- Currency Code Field -->
                    <div class="form-group">
                        {!! Form::label('currency_code', 'Currency Code:', ['class' => 'title']) !!}
                        <p>{{ $food->currency_code }}</p>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 lng-card">
                    <!-- Currency Name Field -->
                    <div class="form-group">
                        {!! Form::label('currency_name', 'Currency Name:', ['class' => 'title']) !!}
                        <p>{{ $food->currency_name }}</p>
                    </div>
                </div>
            </div>
            {{--<div class="info-content row">
                <div class="col-12 title">
                    <h2>Devise</h2>
                </div>
                <div class="col-12 my-2">
                    <div class="row">
                        <div class="col-12 info-card">
                            <!-- Currency Code Field -->
                            <div class="form-group">
                                {!! Form::label('currency_code', 'Currency Code:', ['class' => 'title']) !!}
                                <p>{{ $food->currency_code }}</p>
                            </div>
                        </div>
                        <div class="col-12 info-card">
                            <!-- Currency Name Field -->
                            <div class="form-group">
                                {!! Form::label('currency_name', 'Currency Name:', ['class' => 'title']) !!}
                                <p>{{ $food->currency_name }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}
            <div class="px-4 info-content row">

                @if($food->hasPart == 1)
                    <div class="col-12 title">
                        <h2>Option(s)</h2>
                    </div>
                    <div class="col-12 mt-3">
                        @foreach($food->parts as $part)
                            <div class="col-12 info-card">
                                <div class="form-group">
                                    <p class="default">{{ $part->title }}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
