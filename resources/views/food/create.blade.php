{{--@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('food.index') !!}">Food</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Food</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'food.store', 'files' => true]) !!}

                                   @include('food.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
--}}

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{!! Auth::check() ? route('admin-restaurant-food-index', ['restaurant_id' => $restaurant->uuid]) : route('backoffice-restaurant-food-index', ['restaurant_id' => $restaurant->uuid]) !!}">Food</a>
        </li>
      <li class="breadcrumb-item active">Add</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                {{-- <i class="fa fa-plus-square-o fa-lg"></i> --}}
                                <strong>{{$restaurant->title}} add food</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'food.store', 'files'=>true]) !!}

                                   @include('food.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection

@push('scripts')
    <script>
        var loadFile = function (event) {
            var reader = new FileReader();
            reader.onload = function () {
                var output = document.getElementById('output_cover');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>
@endpush
