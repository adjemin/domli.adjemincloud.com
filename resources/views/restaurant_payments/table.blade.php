<div class="table-responsive-sm">
    <table class="table table-striped" id="restaurantPayments-table">
        <thead>
            <tr>
                <th>Restaurant Id</th>
        <th>Amount</th>
        <th>Is Paid</th>
        <th>Paid Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($restaurantPayments as $restaurantPayment)
            <tr>
                <td>{{ $restaurantPayment->restaurant_id }}</td>
            <td>{{ $restaurantPayment->amount }}</td>
            <td>{{ $restaurantPayment->is_paid }}</td>
            <td>{{ $restaurantPayment->paid_date }}</td>
                <td>
                    {!! Form::open(['route' => ['restaurantPayments.destroy', $restaurantPayment->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('restaurantPayments.show', [$restaurantPayment->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('restaurantPayments.edit', [$restaurantPayment->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>