<!-- Restaurant Id Field -->
<div class="form-group">
    {!! Form::label('restaurant_id', 'Restaurant Id:') !!}
    <p>{{ $restaurantPayment->restaurant_id }}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{{ $restaurantPayment->amount }}</p>
</div>

<!-- Is Paid Field -->
<div class="form-group">
    {!! Form::label('is_paid', 'Is Paid:') !!}
    <p>{{ $restaurantPayment->is_paid }}</p>
</div>

<!-- Paid Date Field -->
<div class="form-group">
    {!! Form::label('paid_date', 'Paid Date:') !!}
    <p>{{ $restaurantPayment->paid_date }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $restaurantPayment->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $restaurantPayment->updated_at }}</p>
</div>

