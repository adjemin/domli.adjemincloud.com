@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        @if(Auth::guard('restaurant')->check())<li class="breadcrumb-item"><a href="{{ url('/backOffice/restaurants/'.Auth::guard('restaurant')->user()->uuid) }}"> Restaurant</a></li> @endif
        <li class="breadcrumb-item">Coupons</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header" style="display: flex; justify-content: space-between">
                            {{-- <i class="fa fa-align-justify"></i> --}}
                            Coupons
                            @if(\Auth::guard('restaurant')->check())
                                <a class="pull-right" href="{{ route('coupons.create') }}">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve" height="28" height="28">
                                        <circle style="fill:#E8C52A;" cx="25" cy="25" r="25"/>
                                        <line style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="25" y1="13" x2="25" y2="38"/>
                                        <line style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="37.5" y1="25" x2="12.5" y2="25"/>
                                    </svg>
                                </a>
                            @endif
                         </div>
                         <div class="card-body">
                            @include('coupons.table')
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection


{{--@extends('layouts.app')


@section('title')
    Coupons | {{ config('app.name') }}
@endsection


@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Coupons</h4>
                            <p class="card-category"> Tous les coupons</p>
                            <div class="col text-right">
                                <a class="pull-right btn btn-sm btn-light" href="{{ route('coupons.create') }}">Ajouter un
                                    coupon</a>
                            </div>
                        </div>
                        <div class="card-body">
                            @include('coupons.table')
                        </div>
                        <div class="mt-3 d-flex justify-content-center">
                            {{ $coupons->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection--}}
