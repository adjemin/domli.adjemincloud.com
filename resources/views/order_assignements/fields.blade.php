<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::select('order_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Deliverer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deliverer_id', 'Deliverer Id:') !!}
    {!! Form::select('deliverer_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('orderAssignements.index') }}" class="btn btn-secondary">Cancel</a>
</div>
