<div class="row">
    <div class="col-6">
        <!-- Order Id Field -->
        <div class="form-group">
            {!! Form::label('order_id', 'Order:') !!}
            <p>{{ $orderAssignement->order->order_number ?? '' }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Deliverer Id Field -->
        <div class="form-group">
            {!! Form::label('deliverer_id', 'Deliverer:') !!}
            <p>{{ $orderAssignement->delivery->name ?? '' }}</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <!-- Acceptation Time Field -->
        <div class="form-group">
            {!! Form::label('acceptation_time', 'Acceptation Time:') !!}
            <p>{{ $orderAssignement->acceptation_time ?? 'No acceptation' }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Rejection Time Field -->
        <div class="form-group">
            {!! Form::label('rejection_time', 'Rejection Time:') !!}
            <p>{{ $orderAssignement->rejection_time ?? 'No rejection' }}</p>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-6">
        <!-- Is Waiting Acceptation Field -->
        <div class="form-group">
            {!! Form::label('is_waiting_acceptation', 'Is Waiting Acceptation:') !!}
            <p>{!! $orderAssignement->is_waiting_acceptation ? "<span style='color:green;'>Yes</span>" : "<span style='color:red;'>No</span>" !!}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Created At:') !!}
            <p>{{ \Carbon\Carbon::parse($orderAssignement->created_at)->diffForHumans() }}</p>
        </div>
    </div>
</div>

{{-- <!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $orderAssignement->updated_at }}</p>
</div>
 --}}
