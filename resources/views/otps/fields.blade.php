<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Code:') !!}
    {!! Form::number('code', null, ['class' => 'form-control']) !!}
</div>

<!-- Dial Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dial_code', 'Dial Code:') !!}
    {!! Form::number('dial_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    {!! Form::number('phone_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Deadline Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deadline', 'Deadline:') !!}
    {!! Form::text('deadline', null, ['class' => 'form-control','id'=>'deadline']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#deadline').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Is Used Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_used', 'Is Used:') !!}
    {!! Form::number('is_used', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Testing Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_testing', 'Is Testing:') !!}
    {!! Form::number('is_testing', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('otps.index') }}" class="btn btn-secondary">Cancel</a>
</div>
