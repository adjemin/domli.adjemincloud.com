<div class="table-responsive-sm">
    <table class="table table-striped" id="otps-table">
        <thead>
            <tr>
                <th>Code</th>
        <th>Dial Code</th>
        <th>Phone Number</th>
        <th>Deadline</th>
        <th>Is Used</th>
        <th>Is Testing</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($otps as $otps)
            <tr>
                <td>{{ $otps->code }}</td>
            <td>{{ $otps->dial_code }}</td>
            <td>{{ $otps->phone_number }}</td>
            <td>{{ $otps->deadline }}</td>
            <td>{{ $otps->is_used }}</td>
            <td>{{ $otps->is_testing }}</td>
                <td>
                    {!! Form::open(['route' => ['otps.destroy', $otps->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('otps.show', [$otps->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('otps.edit', [$otps->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>