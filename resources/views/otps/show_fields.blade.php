<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{{ $otps->code }}</p>
</div>

<!-- Dial Code Field -->
<div class="form-group">
    {!! Form::label('dial_code', 'Dial Code:') !!}
    <p>{{ $otps->dial_code }}</p>
</div>

<!-- Phone Number Field -->
<div class="form-group">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    <p>{{ $otps->phone_number }}</p>
</div>

<!-- Deadline Field -->
<div class="form-group">
    {!! Form::label('deadline', 'Deadline:') !!}
    <p>{{ $otps->deadline }}</p>
</div>

<!-- Is Used Field -->
<div class="form-group">
    {!! Form::label('is_used', 'Is Used:') !!}
    <p>{{ $otps->is_used }}</p>
</div>

<!-- Is Testing Field -->
<div class="form-group">
    {!! Form::label('is_testing', 'Is Testing:') !!}
    <p>{{ $otps->is_testing }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $otps->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $otps->updated_at }}</p>
</div>

