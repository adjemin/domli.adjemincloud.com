<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{{ $customerDevices->customer_id }}</p>
</div>

<!-- Firebase Id Field -->
<div class="form-group">
    {!! Form::label('firebase_id', 'Firebase Id:') !!}
    <p>{{ $customerDevices->firebase_id }}</p>
</div>

<!-- Device Model Field -->
<div class="form-group">
    {!! Form::label('device_model', 'Device Model:') !!}
    <p>{{ $customerDevices->device_model }}</p>
</div>

<!-- Device Os Field -->
<div class="form-group">
    {!! Form::label('device_os', 'Device Os:') !!}
    <p>{{ $customerDevices->device_os }}</p>
</div>

<!-- Device Os Field -->
<div class="form-group">
    {!! Form::label('device_os', 'Device Os:') !!}
    <p>{{ $customerDevices->device_os }}</p>
</div>

<!-- Device Model Type Field -->
<div class="form-group">
    {!! Form::label('device_model_type', 'Device Model Type:') !!}
    <p>{{ $customerDevices->device_model_type }}</p>
</div>

<!-- Device Meta Data Field -->
<div class="form-group">
    {!! Form::label('device_meta_data', 'Device Meta Data:') !!}
    <p>{{ $customerDevices->device_meta_data }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $customerDevices->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $customerDevices->updated_at }}</p>
</div>

