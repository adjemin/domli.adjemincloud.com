<div class="table-responsive-sm">
    <table class="table table-striped" id="customerDevices-table">
        <thead>
            <tr>
                <th>Customer Id</th>
        <th>Firebase Id</th>
        <th>Device Model</th>
        <th>Device Os</th>
        <th>Device Os</th>
        <th>Device Model Type</th>
        <th>Device Meta Data</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($customerDevices as $customerDevices)
            <tr>
                <td>{{ $customerDevices->customer_id }}</td>
            <td>{{ $customerDevices->firebase_id }}</td>
            <td>{{ $customerDevices->device_model }}</td>
            <td>{{ $customerDevices->device_os }}</td>
            <td>{{ $customerDevices->device_os }}</td>
            <td>{{ $customerDevices->device_model_type }}</td>
            <td>{{ $customerDevices->device_meta_data }}</td>
                <td>
                    {!! Form::open(['route' => ['customerDevices.destroy', $customerDevices->id]]) !!}
                    <div class='btn-group'>
                        <a href="{{ route('customerDevices.show', [$customerDevices->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('customerDevices.edit', [$customerDevices->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>