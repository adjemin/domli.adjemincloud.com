<!-- Start Date Field -->
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('start_date', 'Start Date:') !!}
        {!! Form::date('start_date', null, ['class' => 'form-control','id'=>'start_date']) !!}
    </div>
    <!-- End Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('end_date', 'End Date:') !!}
        {!! Form::date('end_date', null, ['class' => 'form-control','id'=>'end_date']) !!}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('start_time', 'Start Time:') !!}
        {!! Form::time('start_time', null, ['class' => 'form-control','id'=>'start_time']) !!}
    </div>
    <!-- End Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('end_time', 'End Time:') !!}
        {!! Form::time('end_time', null, ['class' => 'form-control','id'=>'end_time']) !!}
    </div>
</div>

{{--@push('scripts')
   <script type="text/javascript">
           $('#start_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush--}}

{{--@push('scripts')
   <script type="text/javascript">
           $('#end_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush--}}


<!-- Submit Field -->
<div class="form-group col-sm-12">
    <div class="row">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('restaurantAvailabilities.index') }}" class="btn btn-danger" style="margin-left: 10px">Cancel</a>
    </div>
</div>
