@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{\Auth::check() ? route('admin-restaurants-show', ['id' => Auth::guard('restaurant')->user()->uuid]) : url('/backOffice/restaurants/'.Auth::guard('restaurant')->user()->uuid) }}">Restaurant</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{!! route('restaurantAvailabilities.index') !!}">Restaurant Availability</a>
        </li>
        <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                {{-- <i class="fa fa-plus-square-o fa-lg"></i> --}}
                                <strong>Create Restaurant Availability</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'restaurantAvailabilities.store']) !!}

                                   @include('restaurant_availabilities.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
