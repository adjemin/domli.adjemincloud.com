<div class="row">
    <div class="col-6">
        <!-- Restaurant Id Field -->
        <div class="form-group">
            {!! Form::label('restaurant_id', 'Restaurant:') !!}
            <p>{{ $restaurantAvailability->restaurant->name }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Start Date Field -->
        <div class="form-group">
            {!! Form::label('start_date', 'Start Date:') !!}
            <p>{{ $restaurantAvailability->start_date }}</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <!-- End Date Field -->
        <div class="form-group">
            {!! Form::label('end_date', 'End Date:') !!}
            <p>{{ $restaurantAvailability->end_date }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Start Time Field -->
        <div class="form-group">
            {!! Form::label('start_time', 'Start Time:') !!}
            <p>{{ $restaurantAvailability->start_time }}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <!-- End Time Field -->
        <div class="form-group">
            {!! Form::label('end_time', 'End Time:') !!}
            <p>{{ $restaurantAvailability->end_time }}</p>
        </div>
    </div>
</div>

{{--
<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $restaurantAvailability->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $restaurantAvailability->updated_at }}</p>
</div>

--}}