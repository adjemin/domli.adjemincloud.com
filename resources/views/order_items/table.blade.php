<div class="table-responsive-sm">
    <table class="table table-striped" id="orderItems-table">
        <thead>
            <tr>
                <th>Order Id</th>
        <th>Quantity</th>
        <th>Unit Price</th>
        <th>Total Amount</th>
        <th>Currency Code</th>
        <th>Currency Name</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($orderItems as $orderItems)
            <tr>
                <td>{{ $orderItems->order_id }}</td>
            <td>{{ $orderItems->quantity }}</td>
            <td>{{ $orderItems->unit_price }}</td>
            <td>{{ $orderItems->total_amount }}</td>
            <td>{{ $orderItems->currency_code }}</td>
            <td>{{ $orderItems->currency_name }}</td>
                <td>
                    {!! Form::open(['route' => ['orderItems.destroy', $orderItems->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('orderItems.show', [$orderItems->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('orderItems.edit', [$orderItems->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>