<!-- Delivery Id Field -->
<div class="form-group">
    {!! Form::label('delivery_id', 'Delivery Id:') !!}
    <p>{{ $deliveryCompte->delivery_id }}</p>
</div>

<!-- Compte Field -->
<div class="form-group">
    {!! Form::label('compte', 'Compte:') !!}
    <p>{{ $deliveryCompte->compte }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $deliveryCompte->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $deliveryCompte->updated_at }}</p>
</div>

