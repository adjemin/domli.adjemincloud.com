<div class="table-responsive-sm">
    <table class="table table-striped" id="deliveryComptes-table">
        <thead>
            <tr>
                <th>Delivery Id</th>
        <th>Compte</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($deliveryComptes as $deliveryCompte)
            <tr>
                <td>{{ $deliveryCompte->delivery_id }}</td>
            <td>{{ $deliveryCompte->compte }}</td>
                <td>
                    {!! Form::open(['route' => ['deliveryComptes.destroy', $deliveryCompte->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('deliveryComptes.show', [$deliveryCompte->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('deliveryComptes.edit', [$deliveryCompte->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>