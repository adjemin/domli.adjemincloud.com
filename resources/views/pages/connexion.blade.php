<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="icones/LOGO-DOMLI.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Domli</title>

   <!-- links -->
    <link rel="stylesheet" href="css_v2/main.css">
    <link rel="stylesheet" href="css_v2/animate.css">
    <link rel="stylesheet" href="css_v2/bootstrap.min.css">
    <link rel="stylesheet" href="css_v2/owl.carousel.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
</head>

<body>

    <nav>
        <div class="container-fluid">
            <div class="navigation">
                <div class="logo">
                    <a href=""> <img src="icones/LOGO-DOMLI.png" alt="logo"></a>
                </div>
                <!-- ***** -->
                <div class="menu">
                    <div class="menu--list">
                        <ul>
                            <li>
                                <a href="{{ route('home.index') }}">Page d'accueil</a>
                            </li>
                            <!--  -->
                            <li class="menu--hover position-relative">
                                <a href="#">Devenir &nbsp; <i class="fa fa-caret-down"></i></a>

                                <ul>
                                    <a href="{{ route('connexion.index') }}">
                                        <li>Restaurant</li>
                                    </a>
                                </ul>
                            </li>

                            <li>
                                <a href="{{ route('variete.index') }}">Restaurants</a>
                            </li>

                            <li>
                                <a href="#">Nous Contacter</a>
                            </li>

                            <li>
                                <a href="#">A propos de nous?</a>
                            </li>
                        </ul>
                    </div>
                    <!-- ******* -->
                    <div class="menu--others">
                        <ul>
                            <li class="position-relative">
                                <a href="#">
                                    <i class="fas fa-shopping-basket fa-1x"></i>
                                </a>

                                <div class="panier--count">
                                    <span>30</span>
                                </div>
                            </li>
                            <!-- ** -->
                            <li class="menu--hover position-relative">
                                <a href="#">FR <i class="fa fa-caret-down"></i></a>

                                <ul>
                                    <a href="#">
                                        <li>lorem</li>
                                    </a>
                                </ul>
                            </li>

                            <li>
                                <a href="{{ route('connexion.index') }}">
                                    <button class="btn--site">Inscription/Connexion</button>
                                </a>
                            </li>

                            <li class="btn--resp buttonMenu">
                                <a href="#">
                                    <i class="fas fa-bars "></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- ****************** -->
        <!-- ********* -->
        <div class="menu--responsive">
            <i class="far fa-times-circle buttonMenuSortie"></i>

            <ul>
                <li>
                    <a href="{{ route('home.index') }}">Page d'accueil</a>
                </li>
                <!--  -->
                <li>
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="false"
                        aria-controls="collapseExample">Devenir &nbsp; <i class="fa fa-caret-down"></i></a>


                    <div class="collapse" id="collapseExample">
                        <div class="card card-body">
                            <ul>
                                <li>
                                    <a href="{{ route('connexion.index') }}">Restaurant</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li>
                    <a href="{{ route('variete.index') }}">Restaurants</a>
                </li>

                <li>
                    <a href="#">Nous Contacter</a>
                </li>

                <li>
                    <a href="#">A propos de nous?</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- fin nav -->
    <!-- ***** -->

    <!-- header -->

    <!-- fin header -->

    <main>
        <section class="inscription-connexion__container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-6 wow fadeInLeft">
                        <div class="groupimg">
                            <img src="images/Groupe 30.png" alt="fille">
                            <img src="images/Groupe 67.png" alt="nour">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-6 wow fadeInRight">
                        <div class="content-menu">
                            <div class="menu-tabs">
                                <ul class="nav nav-pills mb-0" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="tabs1-tab" data-toggle="pill" href="#tabs1"
                                            role="tab" aria-controls="tabs1" aria-selected="true">Connexion</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tabs2-tab" data-toggle="pill" href="#tabs2" role="tab"
                                            aria-controls="tabs2" aria-selected="false">Inscription</a>
                                    </li>

                                </ul>
                            </div>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="tabs1" role="tabpanel"
                                    aria-labelledby="tabs1-tab">
                                    <div class="card--log">
                                        <form action="#">
                                            <div class="">
                                                <a href="">
                                                    <button class="btn-fb">
                                                        <i class="fab fa-facebook-f"></i> &nbsp; Connectez vous avec
                                                        Facebook
                                                    </button>
                                                </a>

                                                <!-- **** -->

                                                <a href="">
                                                    <button class="btn-google">
                                                        <i class="fab fa-google-plus-g"></i> &nbsp; Connectez vous avec
                                                        gmail
                                                    </button>
                                                </a>

                                            </div>
                                            <!-- *************** -->
                                            <div class="inputs--form my-5">
                                                <input type="text" placeholder="Adresse e-mail">
                                                <input type="text" placeholder="Mot de passe">
                                            </div>
                                            <!-- *************** -->
                                            <div class="my-5 d-flex align-items-center justify-content-between">
                                                <div class="check">
                                                    <div>
                                                        <input id="box3" type="checkbox" />
                                                        <label for="box3">Souviens-toi de moi.</label>
                                                    </div>
                                                </div>

                                                <p><a href="#">Mot de passe oublié ?</a></p>
                                            </div>
                                            <!-- *************** -->
                                            <button type="submit" class="btn--site1 p-4 w-100">
                                                Connexion
                                            </button>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tabs2" role="tabpanel" aria-labelledby="tabs2-tab">
                                    <div class="card--log">
                                        <form action="#">
                                            <div class="">
                                                <a href="">
                                                    <button class="btn-fb">
                                                        <i class="fab fa-facebook-f"></i> &nbsp; Inscrivez vous avec
                                                        Facebook
                                                    </button>
                                                </a>

                                                <!-- **** -->

                                                <a href="">
                                                    <button class="btn-google">
                                                        <i class="fab fa-google-plus-g"></i> &nbsp; Inscrivez vous avec
                                                        gmail
                                                    </button>
                                                </a>

                                            </div>
                                            <!-- *************** -->
                                            <div class="inputs--form my-5">
                                                <input type="text" placeholder="Nom">
                                                <input type="text" placeholder="Prenoms">
                                                <input type="tel" placeholder=" Votre numero">
                                                <input type="email" placeholder="E-mail">
                                                <input type="password" placeholder="Mot de passe">
                                                <input type="password" placeholder="Confirmez le mot de passe">
                                            </div>
                                            <!-- *************** -->
                                            <div class="my-5">
                                                <p>je suis d'accord avec la <a href="">Politique de confidentialité</a>
                                                    et <a href="">conditions d'utilisation</a></p>
                                            </div>
                                            <!-- *************** -->
                                            <button type="submit" class="btn--site1 p-4 w-100">
                                                Inscription
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <!-- footer -->
        <footer class="footer">
            <div class="container">
                <div class="row wow fadeInRight">
                    <div class="col-md-4 col-lg-3 ">
                        <h3>REJOIGNEZ-NOUS</h3>
                        <ul>
                            <li><a href="#">Emploi</a></li>
                            <li><a href="#">Entreprises partenaires</a></li>
                            <li><a href="#">Courrier</a></li>
                            <li><a href="#">Entreprise Domli</a></li>
                        </ul>
                    </div>

                    <!-- ****************** -->
                    <div class="col-md-4 col-lg-3 ">
                        <h3>AIDE</h3>
                        <ul>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Nous contacter</a></li>
                        </ul>
                    </div>

                    <!-- ****************** -->
                    <div class="col-md-4 col-lg-3 ">
                        <h3>LÉGAL</h3>
                        <ul>
                            <li><a href="#">Termes et conditions</a></li>
                            <li><a href="#">Politique de confidentialité </a></li>
                            <li><a href="#">Politique des cookies</a></li>
                        </ul>
                    </div>

                    <!-- ****************** -->
                    <div class="col-md-4 col-lg-3 ">
                        <h3>SUIVEZ-NOUS </h3>
                        <ul>
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Twitter</a></li>
                            <li><a href="#">Instagram</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- fin footer -->
    </main>

    <!-- Button return top-->
    <a href="#header">
        <div class="button-return" id="btnReturn">
            <!-- <img src="icones/up-arrow.svg" alt="buttontop"> -->
            <i class="fa fa-caret-up"></i>
        </div>
    </a>
    <!-- ************* -->
    <!--  -->
    <script src="js_v2/jquery.min.js"></script>
    <script src="js_v2/bootstrap.min.js"></script>
    <script src="js_v2/owl.carousel.min.js"></script>
    <script src="js_v2/wow.min.js"></script>
    <script src="js_v2/app.js"></script>
    <!-- ********* -->
    <script>
        new WOW().init();

    </script>
    <!-- ********** -->
    <script>
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('#btnReturn').fadeIn();
            } else {
                $('#btnReturn').fadeOut();
            }
        });

        $('#btnReturn').click(function() {
            $('html, body').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

    </script>
</body>

</html>
