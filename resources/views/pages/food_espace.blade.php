<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="icones/LOGO-DOMLI.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Domli</title>

    <!-- links -->
    <link rel="stylesheet" href="css_v2/main.css">
    <link rel="stylesheet" href="css_v2/animate.css">
    <link rel="stylesheet" href="css_v2/bootstrap.min.css">
    <link rel="stylesheet" href="css_v2/owl.carousel.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
</head>
<body>
  
    <nav>
    <div class="container-fluid">
        <div class="navigation">
            <div class="logo">
               <a href=""> <img src="icones/LOGO-DOMLI.png" alt="logo"></a>
            </div>
            <!-- ***** -->
            <div class="menu">
                <div class="menu--list">
                    <ul>
                        <li>
                            <a href="{{ route('home.index') }}">Page d'accueil</a>
                        </li>
                        <!--  -->
                        <li class="menu--hover position-relative">
                            <a href="#">Devenir &nbsp; <i class="fa fa-caret-down"></i></a>
    
                            <ul>
                                <a href="{{ route('connexion.index') }}">
                                    <li>Restaurant</li>
                                </a>
                            </ul>
                        </li>

                        <li>
                            <a href="{{ route('variete.index') }}">Restaurants</a>
                        </li>

                        <li>
                            <a href="#">Nous Contacter</a>
                        </li>

                        <li>
                            <a href="#">A propos de nous?</a>
                        </li>
                    </ul>
                </div>
                <!-- ******* -->
                <div class="menu--others">
                    <ul>
                        <li class="position-relative">
                            <a href="#">
                                <i class="fas fa-shopping-basket fa-1x"></i>
                            </a>

                            <div class="panier--count">
                                <span>30</span>
                            </div>
                        </li>
                        <!-- ** -->
                        <li class="menu--hover position-relative">
                            <a href="#">FR <i class="fa fa-caret-down"></i></a>
    
                            <ul>
                                <a href="#">
                                    <li>lorem</li>
                                </a>
                            </ul>
                        </li>

                        <li>
                            <a href="{{ route('connexion.index') }}">
                                <button class="btn--site">Inscription/Connexion</button>
                            </a>
                        </li>

                        <li class="btn--resp buttonMenu">
                            <a href="#">
                                <i class="fas fa-bars "></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- ****************** -->
    <!-- ********* -->
    <div class="menu--responsive">
        <i class="far fa-times-circle buttonMenuSortie"></i>

        <ul>
            <li>
                <a href="{{ route('home.index') }}">Page d'accueil</a>
            </li>
            <!--  -->
            <li >
                <a   data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Devenir &nbsp; <i class="fa fa-caret-down"></i></a>
              
            
                  <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                        <ul>
                            <li>
                                <a href="{{ route('connexion.index') }}">Restaurant</a>
                            </li>
                        </ul>
                    </div>
                  </div> 
            </li>

            <li>
                <a href="{{ route('variete.index') }}">Restaurants</a>
            </li>

            <li>
                <a href="#">Nous Contacter</a>
            </li>

            <li>
                <a href="#">A propos de nous?</a>
            </li>
        </ul>
    </div>
    </nav>
    <!-- fin nav -->
    <!-- ***** -->

    <!-- header -->
    <header class="header-fs">
        <div class="container  ">
           <div class="header-fs--text ">
               <h1>Food space</h1>
           </div>
        </div>
    </header>
    <!-- fin header -->
 
    <main>
       <!-- section heaeder bottom -->
       <div class="header-fsb">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="__descrip">
                        <h3>Max call</h3>
                        <p><span>4.5 Excellent (500+)</span>.Italien. Pizzas .kentucky</p>
                        <p>à 1.0 km.Livraison gratuite</p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="allergenes">
                        <div>
                            <i class="far fa-question-circle"></i>
                        </div>

                        <div class="ml-3">
                            <h3>Informations</h3>
                            <p class="btn-modal">Allergènes et plus</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- fin -->

    <!-- section menu food space -->
        <section class="menu-fs">
            <div class="container">
                <div class="row ">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                       <h2>Nos Menus</h2>

                       <div class="row mt-5 wow fadeInLeft">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="menu-fs--card btn-modal2">
                                    <div class="__descrip">
                                        <h3 class="mb-3">Formules Soft</h3>
                                        <p>1 bagel/Burger  Accompagnement : une boisson </p>
                                        <h3 class="mt-3">13 $ </h3>
                                    </div>

                                    <div class="__illus">
                                        <img src="images/2679731552454155135.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <!-- **** -->
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="menu-fs--card btn-modal2">
                                    <div class="__descrip">
                                        <h3 class="mb-3">Formules Soft</h3>
                                        <p>1 bagel/Burger  Accompagnement : une boisson </p>
                                        <h3 class="mt-3">13 $ </h3>
                                    </div>

                                    <div class="__illus">
                                        <img src="images/2679731552454155135.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <!-- **** -->
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="menu-fs--card btn-modal2">
                                    <div class="__descrip">
                                        <h3 class="mb-3">Formules Soft</h3>
                                        <p>1 bagel/Burger  Accompagnement : une boisson </p>
                                        <h3 class="mt-3">13 $ </h3>
                                    </div>

                                    <div class="__illus">
                                        <img src="images/2679731552454155135.png" alt="">
                                    </div>
                                </div>
                            </div><!-- **** -->
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="menu-fs--card btn-modal2">
                                    <div class="__descrip">
                                        <h3 class="mb-3">Formules Soft</h3>
                                        <p>1 bagel/Burger  Accompagnement : une boisson </p>
                                        <h3 class="mt-3">13 $ </h3>
                                    </div>

                                    <div class="__illus">
                                        <img src="images/2679731552454155135.png" alt="">
                                    </div>
                                </div>
                            </div><!-- **** -->
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="menu-fs--card btn-modal2">
                                    <div class="__descrip">
                                        <h3 class="mb-3">Formules Soft</h3>
                                        <p>1 bagel/Burger  Accompagnement : une boisson </p>
                                        <h3 class="mt-3">13 $ </h3>
                                    </div>

                                    <div class="__illus">
                                        <img src="images/2679731552454155135.png" alt="">
                                    </div>
                                </div>
                            </div><!-- **** -->
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="menu-fs--card btn-modal2">
                                    <div class="__descrip">
                                        <h3 class="mb-3">Formules Soft</h3>
                                        <p>1 bagel/Burger  Accompagnement : une boisson </p>
                                        <h3 class="mt-3">13 $ </h3>
                                    </div>

                                    <div class="__illus">
                                        <img src="images/2679731552454155135.png" alt="">
                                    </div>
                                </div>
                            </div>
                       </div>

                       <!-- ********************************** -->
                       <h2 class="mt-3">Nos boissons</h2>
                       <div class="row mt-5 wow fadeInLeft">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="menu-fs--card ">
                                    <div class="__descrip">
                                        <h3 class="mb-3">Schweppes 33 CL</h3>
                                        <p>Canette</p>
                                        <h3 class="mt-3">3 $ </h3>
                                    </div>

                                    <div class="__illus">
                                        <img src="images/b1.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <!-- **** -->
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="menu-fs--card ">
                                    <div class="__descrip">
                                        <h3 class="mb-3">Schweppes 33 CL</h3>
                                        <p>Canette</p>
                                        <h3 class="mt-3">3 $ </h3>
                                    </div>

                                    <div class="__illus">
                                        <img src="images/b1.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <!-- **** -->
                        </div>
                    </div>
    
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                        <div class="menu-fs--cart1">
                            <button class="btn--site3 w-100 mb-5">Valider mon panier</button>
                            <i class="fas fa-shopping-basket"></i>
                            <p>Votre panier est vide</p>
                        </div>

                        <!-- si votre est rempli -->
                        <div class="menu-fs--cart">
                            <button class="btn--site1  mb-5">Valider mon panier</button>
                           
                            <div class="achats">
                                <div class="qty">
                                    <button>+</button>
                                    <p>1</p>
                                    <button>-</button>
                                </div>

                                <p>Formules soft + Boisson </p>

                                <p>13 $</p>
                            </div>
                            <!-- ******** -->
                            <hr>
                            <!-- ******** -->
                            <div class="st">
                                <h3>Sous-total </h3>
                                <p>13 $</p>
                            </div>
                            <div class="fl">
                                <h3>Frais de livraison</h3>
                                <p><del>13 $</del></p>
                                <p><span>Offert</span></p>
                            </div>
                            <div class="fs">
                                <h3>Frais de service</h3>
                                <p>13 $</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- fin -->  
      
    <!-- footer -->
    <footer class="footer">
        <div class="container">
            <div class="row wow fadeInRight">
                <div class="col-md-4 col-lg-3 ">
                    <h3>REJOIGNEZ-NOUS</h3>
                    <ul>
                        <li><a href="#">Emploi</a></li>
                        <li><a href="#">Entreprises partenaires</a></li>
                        <li><a href="#">Courrier</a></li>
                        <li><a href="#">Entreprise Domli</a></li>
                    </ul>
                </div>

                <!-- ****************** -->
                <div class="col-md-4 col-lg-3 ">
                    <h3>AIDE</h3>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Nous contacter</a></li>
                    </ul>
                </div>

                <!-- ****************** -->
                <div class="col-md-4 col-lg-3 ">
                    <h3>LÉGAL</h3>
                    <ul>
                        <li><a href="#">Termes et conditions</a></li>
                        <li><a href="#">Politique de confidentialité </a></li>
                        <li><a href="#">Politique des cookies</a></li>
                    </ul>
                </div>

                <!-- ****************** -->
                <div class="col-md-4 col-lg-3 ">
                    <h3>SUIVEZ-NOUS </h3>
                    <ul>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Instagram</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- fin footer -->
    </main>

     <!-- modal--filters -->
     <div class="main-container">
        <div class="container-fluid">
            <div class="modal-card" id="modal-card">
                <div class="modal-main">
                    <div class="close" id="close">
                        <i class="far fa-times-circle"></i>                    
                    </div>
                    <!-- ////////////////************* -->
                    
                    <div class="modal-main__body px-5 text-center">
                        <h3>Informations</h3> <hr>
                        <h3 class="mb-4">Aide sur les allergènes</h3>
                        <p class="mb-3"><strong>Des questions ? Appelez le restaurant</strong> Demandez au restaurant quels sont ses ingrédients et méthodes de préparation. </p>
                        <!-- *** -->
                        <div class="d-flex align-items-center justify-content-center">
                            <i class="fas fa-phone-alt"></i>
                            <p class="ml-3"><strong>Appelez au Food Space </strong> <span><a href="#">+1622452594</a></span></p>

                        </div>
                        <!-- *** -->
                        <button class="btn--site1 w-100 mt-4">Fermer</button>
                    </div>
                </div>            
            </div>
        </div>
    </div>  

    <!-- modal2 -->
    <div class="main-container">
        <div class="container-fluid">
            <div class="modal-card" id="modal-card2">
                <div class="modal-main">
                    <div class="close" id="close2">
                        <i class="far fa-times-circle"></i>                    
                    </div>

                    <!-- ////////////////************* -->
                    
                    <div class="modal-main__body ">
                        <h3>Formules Soft</h3> <hr>
                        <p class="mb-4">Sauce tomate, mozzarella, emmental, chèvre, bleu et reblochon </p>
                        <!-- *** --> 

                        <form action="" class="mt-5">
                            <h4>Boissons au choix</h4>

                            <div class="check">
                                <div>
                                    <input id="box1" type="checkbox" />
                                    <label for="box1">Coca Cola 33 CL</label>
                                </div>
                                <div>
                                    <input id="box2" type="checkbox" />
                                    <label for="box2">Coca Cola 33 CL</label>
                                </div>
                            </div>


                            <div class="qty">
                                <button>+</button>
                                <p>1</p>
                                <button>-</button>
                            </div>

                            <div class="btns-fsb">
                                <button class="btn--site3" >Annuler</button>
                                <button class="btn--site1 ml-3" type="submit" >Ajouter au panier 13 $</button>
                            </div>

                        </form>
                    </div>
                </div>            
            </div>
        </div>
    </div> 
    <!-- fin -->
    <!-- ***** -->
    <!-- Button return top-->
    <a href="#header">
        <div class="button-return" id="btnReturn" >
            <!-- <img src="icones/up-arrow.svg" alt="buttontop"> -->
            <i class="fa fa-caret-up"></i>
        </div>
    </a>
    <!-- ************* -->
    <!--  -->
    <script src="js_v2/jquery.min.js"></script>
    <script src="js_v2/bootstrap.min.js"></script>
    <script src="js_v2/owl.carousel.min.js"></script>
    <script src="js_v2/wow.min.js"></script>
    <script src="js_v2/app.js"></script>
    <!-- ********* -->
    <script>
        new WOW().init();
    </script>
    <!-- ********** -->

    <!-- ********** -->
    <script>
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('#btnReturn').fadeIn();
            } else {
                $('#btnReturn').fadeOut();
            }
        });

        $('#btnReturn').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });	
    </script>
</body>
</html>