<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="icones/LOGO-DOMLI.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Domli</title>

    <!-- links -->
    <link rel="stylesheet" href="css_v2/main.css">
    <link rel="stylesheet" href="css_v2/animate.css">
    <link rel="stylesheet" href="css_v2/bootstrap.min.css">
    <link rel="stylesheet" href="css_v2/owl.carousel.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
</head>
<body>
   <!-- nav -->
   <nav>
    <div class="container-fluid">
        <div class="navigation">
            <div class="logo">
               <a href=""> <img src="icones/LOGO-DOMLI.png" alt="logo"></a>
            </div>
            <!-- ***** -->
            <div class="menu">
                <div class="menu--list">
                    <ul>
                        <li>
                            <a href="{{ route('home.index') }}">Page d'accueil</a>
                        </li>
                        <!--  -->
                        <li class="menu--hover position-relative">
                            <a href="#">Devenir &nbsp; <i class="fa fa-caret-down"></i></a>
    
                            <ul>
                                <a href="{{ route('connexion.index') }}">
                                    <li>Restaurant</li>
                                </a>
                            </ul>
                        </li>

                        <li>
                            <a href="{{ route('variete.index') }}">Restaurants</a>
                        </li>

                        <li>
                            <a href="#">Nous Contacter</a>
                        </li>

                        <li>
                            <a href="#">A propos de nous?</a>
                        </li>
                    </ul>
                </div>
                <!-- ******* -->
                <div class="menu--others">
                    <ul>
                        <li class="position-relative">
                            <a href="#">
                                <i class="fas fa-shopping-basket fa-1x"></i>
                            </a>

                            <div class="panier--count">
                                <span>30</span>
                            </div>
                        </li>
                        <!-- ** -->
                        <li class="menu--hover position-relative">
                            <a href="#">FR <i class="fa fa-caret-down"></i></a>
    
                            <ul>
                                <a href="#">
                                    <li>lorem</li>
                                </a>
                            </ul>
                        </li>

                        <li>
                            <a href="{{ route('connexion.index') }}">
                                <button class="btn--site">Inscription/Connexion</button>
                            </a>
                        </li>

                        <li class="btn--resp buttonMenu">
                            <a href="#">
                                <i class="fas fa-bars "></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- ****************** -->
    <!-- ********* -->
    <div class="menu--responsive">
        <i class="far fa-times-circle buttonMenuSortie"></i>

        <ul>
            <li>
                <a href="{{ route('home.index') }}">Page d'accueil</a>
            </li>
            <!--  -->
            <li >
                <a   data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Devenir &nbsp; <i class="fa fa-caret-down"></i></a>
              
            
                  <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                        <ul>
                            <li>
                                <a href="{{ route('connexion.index') }}">Restaurant</a>
                            </li>
                        </ul>
                    </div>
                  </div> 
            </li>

            <li>
                <a href="{{ route('variete.index') }}">Restaurants</a>
            </li>

            <li>
                <a href="#">Nous Contacter</a>
            </li>

            <li>
                <a href="#">A propos de nous?</a>
            </li>
        </ul>
    </div>
    </nav>
<!-- fin nav -->
    <!-- ***** -->

    <!-- header -->
    <header class="header-variete">
        <div class="container  ">
           <div class="header-variete__form ">
               <form action="" class="wow fadeInLeft">
                   <div class="group--form ">
                        <i class="fas fa-search"></i>
                        <input type="text" placeholder="Plat, restaurants ou types de cuisine"> 
                   </div>

                   <div class="location">
                        <i class="fas fa-map-marker-alt"></i>
                        <p>2, rue des Jardins Québec, QC G1R4S9 Canada</p>
                   </div>
               </form>
           </div>
        </div>
    </header>
    <!-- fin header -->

    <!-- section owl variete -->
    <section class="owl-variete">
        <div class="container">
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="#">
                        <div class="card--owlv">
                            <img src="icones/2.png" alt="card pub">
                            <p>Americain</p>
                        </div>
                    </a>
                </div>
                <!-- ****** -->
                <div class="item">
                    <a href="#">
                        <div class="card--owlv">
                            <img src="icones/1.png" alt="card pub">
                            <p>Chinois</p>
                        </div>
                    </a>
                </div>
                <!-- ****** -->
                <div class="item">
                    <a href="#">
                        <div class="card--owlv">
                            <img src="icones/3.png" alt="card pub">
                            <p>Vietnamien</p>
                        </div>
                    </a>
                </div>
                <!-- ****** -->
                <div class="item">
                    <a href="#">
                        <div class="card--owlv">
                            <img src="icones/4.png" alt="card pub">
                            <p>Tacos</p>
                        </div>
                    </a>
                </div>
                <!-- ****** -->
                <div class="item">
                    <a href="#">
                        <div class="card--owlv">
                            <img src="icones/5.png" alt="card pub">
                            <p>Végétarien</p>
                        </div>
                    </a>
                </div>
                <!-- ****** -->
                <div class="item">
                    <a href="#">
                        <div class="card--owlv">
                            <img src="icones/6.png" alt="card pub">
                            <p>Kebabs</p>
                        </div>
                    </a>
                </div>
                <!-- ****** -->
            </div>
        </div>
    </section>
    <!-- fin  -->

    <!-- section promo -->
    <section class="promos">
        <div class="container">
            <div class="row  wow fadeInLeft">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <a href="">
                        <img src="images/Groupe 1.png" alt="img--promo">
                    </a>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <a href="">
                        <img src="images/Groupe 2.png" alt="img--promo">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- section fin promo -->

    <!-- section popus -->
    <section class="popus">
        <div class="container">
            <h3>Les plus populaires</h3>
            
            <div class="row  wow fadeInLeft d-flex justify-content-center">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <a href="#">
                        <div class="popus--card">
                            <div class="__illus">
                                <img src="images/shawarma-vue-laterale-pommes-terre-frites-dans-batterie-cuisine-bord_176474-3215.png" alt="img-become">
                            </div>
                            <!-- ****** -->
                            <div class="__descrip">
                                <h3>Max call</h3>
                                <p><span>4.5 Excellent (500+)</span>.Italien. Pizzas</p>
                                <p>à 1.0 km.Livraison gratuite</p>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- ----- -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <a href="#">
                        <div class="popus--card">
                            <div class="__illus">
                                <img src="images/shawarma-vue-laterale-pommes-terre-frites-dans-batterie-cuisine-bord_176474-3215.png" alt="img-become">
                            </div>
                            <!-- ****** -->
                            <div class="__descrip">
                                <h3>Max call</h3>
                                <p><span>4.5 Excellent (500+)</span>.Italien. Pizzas</p>
                                <p>à 1.0 km.Livraison gratuite</p>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- ----- -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <a href="#">
                        <div class="popus--card">
                            <div class="__illus">
                                <img src="images/shawarma-vue-laterale-pommes-terre-frites-dans-batterie-cuisine-bord_176474-3215.png" alt="img-become">
                            </div>
                            <!-- ****** -->
                            <div class="__descrip">
                                <h3>Max call</h3>
                                <p><span>4.5 Excellent (500+)</span>.Italien. Pizzas</p>
                                <p>à 1.0 km.Livraison gratuite</p>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- ----- -->
            </div>
        </div>
    </section>
    <!-- fin -->
    <main>
        
      
    <!-- footer -->
    <footer class="footer">
        <div class="container">
            <div class="row wow fadeInRight">
                <div class="col-md-4 col-lg-3 ">
                    <h3>REJOIGNEZ-NOUS</h3>
                    <ul>
                        <li><a href="#">Emploi</a></li>
                        <li><a href="#">Entreprises partenaires</a></li>
                        <li><a href="#">Courrier</a></li>
                        <li><a href="#">Entreprise Domli</a></li>
                    </ul>
                </div>

                <!-- ****************** -->
                <div class="col-md-4 col-lg-3 ">
                    <h3>AIDE</h3>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Nous contacter</a></li>
                    </ul>
                </div>

                <!-- ****************** -->
                <div class="col-md-4 col-lg-3 ">
                    <h3>LÉGAL</h3>
                    <ul>
                        <li><a href="#">Termes et conditions</a></li>
                        <li><a href="#">Politique de confidentialité </a></li>
                        <li><a href="#">Politique des cookies</a></li>
                    </ul>
                </div>

                <!-- ****************** -->
                <div class="col-md-4 col-lg-3 ">
                    <h3>SUIVEZ-NOUS </h3>
                    <ul>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Instagram</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- fin footer -->
    </main>

    <!-- Button return top-->
    <a href="#header">
        <div class="button-return" id="btnReturn" >
            <!-- <img src="icones/up-arrow.svg" alt="buttontop"> -->
            <i class="fa fa-caret-up"></i>
        </div>
    </a>
    <!-- ************* -->
    <!--  -->
    <script src="js_v2/jquery.min.js"></script>
    <script src="js_v2/bootstrap.min.js"></script>
    <script src="js_v2/owl.carousel.min.js"></script>
    <script src="js_v2/wow.min.js"></script>
    <script src="js_v2/app.js"></script>
    <!-- ********* -->
    <script>
        new WOW().init();
    </script>
    <!-- ********** -->
    <script>
        $('.owl-carousel').owlCarousel({
            // items:5,
            loop:true,
            dots:false,
            autoplay:true,
            autoplayTimeout:4000,
            margin:7,
            // URLhashListener:true,
            // autoplayHoverPause:true,
            // startPosition: 'URLHash',
            responsiveClass:true,
                responsive: {
                    0: {
                        items: 2
                    },
                    600: {
                        items: 2
                    },
                    
                    1000: {
                        items: 4
                    },

                    1200:{
                        items:6
                    }
                }
            });
    </script>
    <!-- ********** -->
    <script>
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('#btnReturn').fadeIn();
            } else {
                $('#btnReturn').fadeOut();
            }
        });

        $('#btnReturn').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });	
    </script>
</body>
</html>