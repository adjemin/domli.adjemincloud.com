@extends('layouts_v2.app')

@section('content')

    @include('partials.header')

    <section class="howItWorks">
        <div class="container">
            <div class="row  wow fadeInLeft d-flex justify-content-center">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="howItWorks--card">
                        <div class="__illus">
                            <img src="icones/hamburger.png" alt="img-hiw">
                        </div>
                        <!-- ****** -->
                        <div class="__descrip">
                            <h3>Commander</h3>
                            <p>Commandez chez vos <br> restaurants préférés</p>
                        </div>
                    </div>
                </div>
                <!-- ----- -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="howItWorks--card">
                        <div class="__illus">
                            <img src="icones/credit.png" alt="img-hiw">
                        </div>
                        <!-- ****** -->
                        <div class="__descrip">
                            <h3>Paiement</h3>
                            <p>Payer en toute sécurité avec <br> nos moyens de paiments </p>
                        </div>
                    </div>
                </div>
                <!-- ----- -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="howItWorks--card">
                        <div class="__illus">
                            <img src="icones/food-delivery (1).png" alt="img-hiw">
                        </div>
                        <!-- ****** -->
                        <div class="__descrip">
                            <h3>Livraison</h3>
                            <p>Faites vous livrer dans <br> les meilleurs délais</p>
                        </div>
                    </div>
                </div>
                <!-- ----- -->
            </div>
        </div>
    </section>
    <!-- fin howItWorks -->

    <!-- section pub -->
    <section class="pubs ">
        <div class="container">
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="{{ route('food_espace.index') }}">
                        <div class="card--pub">
                            <img src="images/PIZZA-DEALS.png" alt="card pub">
                        </div>
                    </a>
                </div>
                <!-- ****** -->
                <div class="item">
                    <a href="{{ route('food_espace.index') }}">
                        <div class="card--pub">
                            <img src="images/COVER-PUB3.png" alt="card pub">
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- fin section pub -->

    <!-- section become -->
    <section class="become">
        <div class="container">
            <div class="row  wow fadeInLeft d-flex justify-content-center">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="become--card">
                        <div class="__illus">
                            <img src="images/shawarma-vue-laterale-pommes-terre-frites-dans-batterie-cuisine-bord_176474-3215.png"
                                alt="img-become">
                        </div>
                        <!-- ****** -->
                        <div class="__descrip __descrip1">
                            <h3>Clients</h3>
                            <p>Trouvez les meilleurs restaurants dont vous rêvez à proximité, choisissez vos plats
                                préférés , au Canada et dans les <br> Caraïbes.</p>
                            <a href="{{ route('connexion.index') }}">
                                <button class="btn--site w-100 mt-3">
                                    Devenir clients
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- ----- -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="become--card">
                        <div class="__illus">
                            <img src="images/2679731552454155135.png" alt="img-become">
                        </div>
                        <!-- ****** -->
                        <div class="__descrip __descrip2">
                            <h3>Restaurant</h3>
                            <p>Trouvez de nombreuses livraisons de repas à proximité. En tant que livreur, vous
                                fiabiliserez l’argent - travailler n’importe quand, n’importe où.</p>
                            <a href="{{ route('connexion.index') }}">

                                <button class="btn--site w-100 mt-3">
                                    Devenir Partenaire
                                </button>
                            </a>

                        </div>
                    </div>
                </div>
                <!-- ----- -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="become--card">
                        <div class="__illus">
                            <img src="images/cb03900-08-thumbnail-1080x1080-70.png" alt="img-become">
                        </div>
                        <!-- ****** -->
                        <div class="__descrip __descrip3">
                            <h3>Livreur</h3>
                            <p>Attirez plus de nouveaux clients, et maximisez la capacité de votre cuisine grâce au
                                principal réseau de livraison de nourriture</p>
                            <button class="btn--site w-100 mt-3">
                                Devenir Livreur
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
