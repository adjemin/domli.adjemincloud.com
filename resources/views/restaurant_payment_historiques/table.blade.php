<div class="table-responsive-sm">
    <table class="table table-striped" id="restaurantPaymentHistoriques-table">
        <thead>
            <tr>
                <th>Amount</th>
                <th>Restaurant</th>
                <th>Reference Paiement</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($restaurantPaymentHistoriques as $restaurantPaymentHistorique)
            <tr>
                <td>{{ $restaurantPaymentHistorique->amount }}</td>
                <td>{{ $restaurantPaymentHistorique->restaurant->name ?? 'No restaurant' }}</td>
                <td>{{ $restaurantPaymentHistorique->reference_paiement }}</td>
                <td>
                    {{-- {!! Form::open(['route' => ['restaurantPaymentHistoriques.destroy', $restaurantPaymentHistorique->id], 'method' => 'delete']) !!} --}}
                    <div class='btn-group'>
                        <a href="{{ route('restaurantPaymentHistoriques.show', [$restaurantPaymentHistorique->id]) }}" class='btn btn-ghost-success'>
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999; width: 25px;" xml:space="preserve">
                                <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035 c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201 C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418 c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418 C447.361,287.923,358.746,385.406,255.997,385.406z"/>
                                <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275 s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"/>
                            </svg>
                        </a>
                        {{-- <a href="{{ route('restaurantPaymentHistoriques.edit', [$restaurantPaymentHistorique->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!} --}}
                    </div>
                    {{-- {!! Form::close() !!} --}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>