<div class="row">
    <div class="col-6">
        <!-- Amount Field -->
        <div class="form-group">
            {!! Form::label('amount', 'Amount:') !!}
            <p>{{ $restaurantPaymentHistorique->amount }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Restaurant Id Field -->
        <div class="form-group">
            {!! Form::label('restaurant_id', 'Restaurant Id:') !!}
            <p>{{ $restaurantPaymentHistorique->restaurant->name  ?? 'No restaurant'}}</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <!-- Reference Paiement Field -->
        <div class="form-group">
            {!! Form::label('reference_paiement', 'Reference Paiement:') !!}
            <p>{{ $restaurantPaymentHistorique->reference_paiement }}</p>
        </div>
    </div>
    <div class="col-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Created At:') !!}
            <p>{{ \Carbon\Carbon::parse($restaurantPaymentHistorique->created_at)->diffForHumans() }}</p>
        </div>
    </div>
</div>
<!-- Updated At Field -->
{{-- <div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $restaurantPaymentHistorique->updated_at }}</p>
</div> --}}