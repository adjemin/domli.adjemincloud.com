@extends('layouts.app')

@section('content')
  <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row my-3">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">

                            <p class="card-category text-center">Customers</p>
                            <h3 class="card-title text-center">{{ $customers->count() ?? '' }}
                                {{-- <small>GB</small> --}}
                            </h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats text-center">
                                <!-- <i class="material-icons text-warning">visibility</i> -->
                                <a href="{{route('admin-customers-index')}}">See more...</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="card card-stats">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <!-- <i class="material-icons">person</i> -->
                            </div>
                            <p class="card-category text-center">Restaurants</p>
                            <h3 class="card-title text-center">{{ $restaurants->count() ?? '' }}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats text-center">
                                <!-- <i class="material-icons text-success">visibility</i> -->
                                <a href="{{route('admin-restaurants-index')}}">See more...</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="card card-stats">
                        <div class="card-header card-header-info card-header-icon">
                            <div class="card-icon">
                                <!-- <i class="material-icons">Deliverer</i> -->
                            </div>
                            <p class="card-category text-center">Deliverer</p>
                            <h3 class="card-title text-center">{{ $deliverers->count() ?? ''}}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats text-center">
                                <!-- <i class="material-icons text-info">visibility</i> -->
                                <a href="{{route('admin-deliverers-index')}}">See more...</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-3 card">
                <div class='col-12 card-body' style="max-height:550px !important;">
                    <canvas id="myChart"></canvas>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="row my-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i>Orders
                </div>
                <div class="card-body">
                    <div class="table-responsive-sm">
                        <table class="table table-striped" id="orders-table">
                            <thead>
                                <tr>
                                    <!-- <th>Order Number</th> -->
                                    {{-- <th>Is Waiting</th> --}}
                                    <th>Current Status</th>
                                    <th>Amount</th>
                                    <th>Is Delivery</th>
                                    <th>Is Wayting Payment</th>
                                    <th>Customer</th>
                                    <th>Restaurant</th>
                                    <th>Delivery Fees</th>
                                    <th colspan="3">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($orders_all as $order)
                                <tr>
                                    {{-- <td>{!! $order->is_waiting == 1 ? '<span class="badge badge-success px-3 py-2"> Yes </span>' : '<span class="badge badge-danger px-3 py-2"> No </span>' !!}</td> --}}
                                    <td>{{ $order->current_status }}</td>
                                    <td>{{ $order->amount ?? 0 }}</td>
                                    <td>{!! $order->is_delivery == 1 ? '<span class="badge badge-success px-3 py-2"> Yes </span>' : '<span class="badge badge-danger px-3 py-2"> No </span>' !!}</td>
                                    <td>{!! $order->is_wayting_payment == 1 ? '<span class="badge badge-success px-3 py-2"> Yes </span>' : '<span class="badge badge-danger px-3 py-2"> No </span>' !!}</td>
                                    <td>{{ $order->customer->name ?? '' }} {{ $order->customer->lastname  ?? ''  }}</td>
                                    <td>{{ $order->restaurant->name ?? '' }}</td>
                                    <td>{{ $order->delivery_fees }}</td>
                                    <td>
                                        {!! Form::open(['route' => ['admin-orders-delete', $order->id], 'method' => 'delete']) !!}
                                        <div class='btn-group'>
                                            <a href="{{ route('admin-orders-show', [$order->id]) }}" class='btn btn-ghost-success'>
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve" width="20">
                                                    <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035 c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201 C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418 c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418 C447.361,287.923,358.746,385.406,255.997,385.406z"/>
                                                    <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275 s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"/>
                                                </svg>
                                            </a>
                                            {{-- <a href="{{ route('orders.edit', [$order->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a> --}}
                                            {!! Form::button('<svg id="Layer_1" enable-background="new 0 0 512 512" height="20" viewBox="0 0 512 512" width="20" xmlns="http://www.w3.org/2000/svg"><g><path d="m424 64h-88v-16c0-26.467-21.533-48-48-48h-64c-26.467 0-48 21.533-48 48v16h-88c-22.056 0-40 17.944-40 40v56c0 8.836 7.164 16 16 16h8.744l13.823 290.283c1.221 25.636 22.281 45.717 47.945 45.717h242.976c25.665 0 46.725-20.081 47.945-45.717l13.823-290.283h8.744c8.836 0 16-7.164 16-16v-56c0-22.056-17.944-40-40-40zm-216-16c0-8.822 7.178-16 16-16h64c8.822 0 16 7.178 16 16v16h-96zm-128 56c0-4.411 3.589-8 8-8h336c4.411 0 8 3.589 8 8v40c-4.931 0-331.567 0-352 0zm313.469 360.761c-.407 8.545-7.427 15.239-15.981 15.239h-242.976c-8.555 0-15.575-6.694-15.981-15.239l-13.751-288.761h302.44z"/><path d="m256 448c8.836 0 16-7.164 16-16v-208c0-8.836-7.164-16-16-16s-16 7.164-16 16v208c0 8.836 7.163 16 16 16z"/><path d="m336 448c8.836 0 16-7.164 16-16v-208c0-8.836-7.164-16-16-16s-16 7.164-16 16v208c0 8.836 7.163 16 16 16z"/><path d="m176 448c8.836 0 16-7.164 16-16v-208c0-8.836-7.164-16-16-16s-16 7.164-16 16v208c0 8.836 7.163 16 16 16z"/></g></svg>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="d-flex justify-content-center mr-3">
                        {!! $orders_all->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    @php
        $colors = collect($orders)->map(function($order){
            return 'rgba('.rand(0,225) .','.rand(1,225) .','.rand(1,225) .','.rand(0,0.6).')';
        })->toArray();
        $sum = collect($orders)->map(function($order){
            return collect($order)->map(function($item){
                return $item['amount'];
            })->sum();
        })->toArray();
    @endphp
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js" integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw==" crossorigin="anonymous"></script>
    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: @json(array_keys($orders)),
                datasets: [{
                    label: '# Total price for orders',
                    data: @json(array_values($sum)),
                    backgroundColor: @json($colors),
                    borderColor: @json($colors),
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endpush
