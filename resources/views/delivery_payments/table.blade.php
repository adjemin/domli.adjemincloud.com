<div class="table-responsive-sm">
    <table class="table table-striped" id="deliveryPayments-table">
        <thead>
            <tr>
                <th>Deliverer Id</th>
        <th>Amount</th>
        <th>Is Paid</th>
        <th>Paid Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($deliveryPayments as $deliveryPayment)
            <tr>
                <td>{{ $deliveryPayment->deliverer_id }}</td>
            <td>{{ $deliveryPayment->amount }}</td>
            <td>{{ $deliveryPayment->is_paid }}</td>
            <td>{{ $deliveryPayment->paid_date }}</td>
                <td>
                    {!! Form::open(['route' => ['deliveryPayments.destroy', $deliveryPayment->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('deliveryPayments.show', [$deliveryPayment->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('deliveryPayments.edit', [$deliveryPayment->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>