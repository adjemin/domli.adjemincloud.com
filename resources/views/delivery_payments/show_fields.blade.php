<!-- Deliverer Id Field -->
<div class="form-group">
    {!! Form::label('deliverer_id', 'Deliverer Id:') !!}
    <p>{{ $deliveryPayment->deliverer_id }}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{{ $deliveryPayment->amount }}</p>
</div>

<!-- Is Paid Field -->
<div class="form-group">
    {!! Form::label('is_paid', 'Is Paid:') !!}
    <p>{{ $deliveryPayment->is_paid }}</p>
</div>

<!-- Paid Date Field -->
<div class="form-group">
    {!! Form::label('paid_date', 'Paid Date:') !!}
    <p>{{ $deliveryPayment->paid_date }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $deliveryPayment->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $deliveryPayment->updated_at }}</p>
</div>

