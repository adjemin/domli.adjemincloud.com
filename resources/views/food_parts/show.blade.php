{{--@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('foodParts.index') }}">Food Parts</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('foodParts.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('food_parts.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection--}}

@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/backOffice/restaurants/'.$foodParts->food->restaurant->uuid) }}">Restaurant</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('backoffice-restaurant-foodPart-index', ['restaurant_id' => $foodParts->food->restaurant->uuid, 'food_id' => $foodParts->food->id]) }}">Food Part</a>
        </li>
        <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-9">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('backoffice-restaurant-foodPart-index', ['restaurant_id' => $foodParts->food->restaurant->uuid, 'food_id' => $foodParts->food->id]) }}" class="btn btn-primary float-right">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('food_parts.show_fields')
                             </div>
                         </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header">
                                <strong> Actions </strong>
                            </div>
                            <div class="card-body">
                                <a class="btn btn-block btn-outline-primary active" href="/restaurant/{{$foodParts->food->restaurant->uuid}}/{{$foodParts->id}}/food_part_item">Item</a>
                            </div>
                        </div>
                    </div>
                 </div>
          </div>
    </div>
@endsection
