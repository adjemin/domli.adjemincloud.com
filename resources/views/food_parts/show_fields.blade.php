{{--
<!-- Food Id Field -->
<div class="form-group">
    {!! Form::label('food_id', 'Food Id:') !!}
    <p>{{ $foodPartss->food_id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $foodPartss->title }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $foodPartss->price }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $foodPartss->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $foodPartss->updated_at }}</p>
</div>

--}}

<!-- Parent Id Field -->
{{--<div class="form-group">
    {!! Form::label('parent_id', 'Parent Id:') !!}
    <p>{{ $foodParts->parent_id }}</p>
</div>---}}
<div class="row">
    <div class="col-6">
        <!-- Food Id Field -->
        <div class="form-group">
            {!! Form::label('food_id', 'Food:') !!}
            <p>{{ $foodParts->food->title }}</p>
        </div>    
    </div>
    <div class="col-6">
        <!-- Title Field -->
        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            <p>{{ $foodParts->title }}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <!-- Subtitle Field -->
        <div class="form-group">
            {!! Form::label('subtitle', 'Subtitle:') !!}
            <p>{{ $foodParts->subtitle }}</p>
        </div>    
    </div>
    <div class="col-6">
        <!-- Is Required Field -->
        <div class="form-group">
            {!! Form::label('is_required', 'Is Required:') !!}
            @if($foodParts->is_required)
                <p><span class="badge badge-info px-2 py-2"> Required</span></p>
            @else
                <p><span class="badge badge-danger px-2 py-2"> Not Required</span></p>
            @endif
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-6">
        <!-- Inputtype Field -->
        <div class="form-group">
            {!! Form::label('inputType', 'Input type:') !!}
            <p>{{ $foodParts->inputType == 'radio' ? 'Select unique' : 'Multiple select' }}</p>
        </div>    
    </div>
    {{--<div class="col-6">
        <!-- Has Chidren Field -->
        <div class="form-group">
            {!! Form::label('has_chidren', 'Has Chidren:') !!}
            <p>{{ $foodParts->has_chidren }}</p>
        </div>    
    </div>--}}
</div>