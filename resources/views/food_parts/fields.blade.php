{{--<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('foodParts.index') }}" class="btn btn-secondary">Cancel</a>
</div>--}}

<!-- Parent Id Field -->
{{--<div class="form-group col-sm-6">
    {!! Form::label('parent_id', 'Parent Id:') !!}
    {!! Form::text('parent_id', null, ['class' => 'form-control']) !!}
</div>
--}}
<!-- Food Id Field -->
<!-- <div class="form-group col-sm-6"> -->
    {{--{!! Form::label('food_id', 'Food Id:') !!}--}}
    {!! Form::hidden('food_id', $food->id) !!}
<!-- </div> -->
<div class="row">
    <!-- Title Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
    <!-- Subtitle Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('subtitle', 'Subtitle:') !!}
        {!! Form::text('subtitle', null, ['class' => 'form-control']) !!}
    </div>

</div>
<div class="row">
    <!-- Is Required Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('is_required', 'Is Required:') !!}
        {!! Form::checkbox('is_required', "1") !!}
    </div>

    <!-- Inputtype Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('inputType', 'Input type:') !!}
        <select name="inputType" id="" class="form-control">
            <option value="radio">select_unique</option>
            <option value="checkbox">multiple_select</option>
        </select>
    </div>
</div>

<!-- Has Chidren Field -->
{{--<div class="form-group col-sm-6">
    {!! Form::label('has_chidren', 'Has Chidren:') !!}
    {!! Form::text('has_chidren', null, ['class' => 'form-control']) !!}
</div>--}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('foodParts.index') }}" class="btn btn-secondary">Cancel</a>
</div>
