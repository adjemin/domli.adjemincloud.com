<div class="row mt-3">
    <div class="col-md-6">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            <p>{{ $customers->name }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Surname Field -->
        <div class="form-group">
            {!! Form::label('surname', 'Surname:') !!}
            <p>{{ $customers->surname }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Phone Field -->
        <div class="form-group">
            {!! Form::label('phone', 'Phone:') !!}
            <p>{{ $customers->phone }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Email Field -->
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            <p>{{ $customers->email }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Lng Field -->
        <div class="form-group">
            {!! Form::label('lng', 'Lng:') !!}
            <p>{{ $customers->lng }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Lat Field -->
        <div class="form-group">
            {!! Form::label('lat', 'Lat:') !!}
            <p>{{ $customers->lat }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Langage Field -->
        <div class="form-group">
            {!! Form::label('langage', 'Langage:') !!}
            <p>{{ $customers->langage }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Country Code Field -->
        <div class="form-group">
            {!! Form::label('country_code', 'Country Code:') !!}
            <p>{{ $customers->country_code }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Is Active Field -->
        <div class="form-group">
            {!! Form::label('is_active', 'Is Active:') !!}
            <p>{{ $customers->is_active }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Activation Date Field -->
        <div class="form-group">
            {!! Form::label('activation_date', 'Activation Date:') !!}
            <p>{{ $customers->activation_date }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Created At:') !!}
            <p>{{ $customers->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Updated At:') !!}
            <p>{{ $customers->updated_at }}</p>
        </div>
    </div>
</div>

{{-- <!-- Dial Code Field -->
<div class="form-group">
    {!! Form::label('dial_code', 'Dial Code:') !!}
    <p>{{ $customers->dial_code }}</p>
</div>

<!-- Phone Number Field -->
<div class="form-group">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    <p>{{ $customers->phone_number }}</p>
</div> --}}

{{-- <!-- Photo Profile Field -->
<div class="form-group">
    {!! Form::label('photo_profile', 'Photo Profile:') !!}
    <p>{{ $customers->photo_profile }}</p>
</div> --}}

