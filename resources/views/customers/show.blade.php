@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin-customers-index') }}">Customers</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-7">
                        <div class="card">
                            <div class="card-header">
                                <strong>Details</strong>
                                <a href="{{ route('admin-customers-index') }}" class="btn btn-light pull-right">Back</a>
                            </div>
                            <div class="card-body">
                                @include('customers.show_fields')
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="card">
                            <div class="card-header">
                                <strong> Photo de Profile </strong>
                            </div>
                            <div class="card-avatar">
                                <a href="javascript:;">
                                    <img class="img" src="{{ $customers->photo_profile }}" />
                                </a>
                            </div>
                        </div>
                     </div>
                </div>
          </div>
    </div>
@endsection
