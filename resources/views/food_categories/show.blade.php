@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin-foodCategories-create') }}">Food Categories</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
            @include('coreui-templates::common.errors')
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header">
                            <strong>Details</strong>
                            <a href="{{ route('admin-foodCategories-create') }}" class="btn btn-light pull-right">Back</a>
                        </div>
                        <div class="card-body">
                            @include('food_categories.show_fields')
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-header">Image</div>
                        <div class="card-body">
                            <img src="{{ $foodCategories->photo ?? asset('img/default-restaurant.png') }}" height="90" alt="{{ $foodCategories->title}}">
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
@endsection
