<div class="row mt-3">
    <div class="col-md-6">
        <!-- Title Field -->
        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            <p>{{ $foodCategories->title }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Slug Field -->
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            <p>{{ $foodCategories->slug }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <!-- Description Field -->
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            <p>{{ $foodCategories->description }}</p>
        </div>
    </div>
</div>

{{--<div class="row mt-3">
    <div class="col-md-12">
        <!-- Photo Field -->
        <div class="form-group">
            {!! Form::label('photo', 'Photo:') !!}
            <p><img src="{{ $foodCategories->photo }}" alt=""></p>
        </div>
    </div>
</div>--}}

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Created At:') !!}
            <p>{{ $foodCategories->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Updated At:') !!}
            <p>{{ $foodCategories->updated_at }}</p>
        </div>
    </div>
</div>


