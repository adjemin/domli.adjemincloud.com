<!-- Order Code Field -->
<div class="form-group">
    {!! Form::label('order_code', 'Order Code:') !!}
    <p>{{ $orderDeliveries->order_code }}</p>
</div>

<!-- Delivery Id Field -->
<div class="form-group">
    {!! Form::label('delivery_id', 'Delivery Id:') !!}
    <p>{{ $orderDeliveries->delivery_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $orderDeliveries->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $orderDeliveries->updated_at }}</p>
</div>

