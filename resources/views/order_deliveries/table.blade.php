<div class="table-responsive-sm">
    <table class="table table-striped" id="orderDeliveries-table">
        <thead>
            <tr>
                <th>Order Code</th>
        <th>Delivery Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($orderDeliveries as $orderDeliveries)
            <tr>
                <td>{{ $orderDeliveries->order_code }}</td>
            <td>{{ $orderDeliveries->delivery_id }}</td>
                <td>
                    {!! Form::open(['route' => ['orderDeliveries.destroy', $orderDeliveries->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('orderDeliveries.show', [$orderDeliveries->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('orderDeliveries.edit', [$orderDeliveries->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>