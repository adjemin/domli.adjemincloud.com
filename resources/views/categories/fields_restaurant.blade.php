<div class="row">
    <!-- Title Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('title', 'Titre:') !!}
        {!! Form::text('title', old('title') ?? $category->title, ['class' => 'form-control']) !!}
    </div>
    <!-- Photo Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('photo', 'Photo:') !!}
        {!! Form::text('photo', old('photo') ?? $category->photo, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('description', 'Description:') !!}
        <textarea name="description" class="form-control">{{old('description') ?? $category->description}}</textarea>
    </div>
</div>
<!-- Description Field -->
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('categories.index') }}" class="btn btn-secondary">Cancel</a>
</div>
