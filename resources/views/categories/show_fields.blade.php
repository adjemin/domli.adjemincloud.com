<!-- Title Field -->
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            <p>{{ $category->title }}</p>
        </div>    
    </div>
    <div class="col-lg-6">
        <!-- Description Field -->
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            <p>{{ $category->description }}</p>
        </div>    
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <!-- Photo Field -->
        <div class="form-group">
            {!! Form::label('photo', 'Photo:') !!}
            <p>{{ $category->photo }}</p>
        </div>
    </div>
    <div class="col-lg-6">    
    <!-- Slug Field -->
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            <p>{{ $category->slug }}</p>
        </div>
    </div>
</div>