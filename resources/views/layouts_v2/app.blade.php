<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="icones/LOGO-DOMLI.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Domli</title>

    <!-- links -->
    <link rel="stylesheet" href="css_v2/main.css">
    <link rel="stylesheet" href="css_v2/animate.css">
    <link rel="stylesheet" href="css_v2/bootstrap.min.css">
    <link rel="stylesheet" href="css_v2/owl.carousel.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
</head>

<body>

    @include('partials.navbar')

    <main>
        @yield('content')
    </main>

    @include('partials.footer')



    <!-- Button return top-->
    <a href="#header">
        <div class="button-return" id="btnReturn">
            <!-- <img src="icones/up-arrow.svg" alt="buttontop"> -->
            <i class="fa fa-caret-up"></i>
        </div>
    </a>
    <!-- ************* -->
    <!--  -->
    <script src="js_v2/jquery.min.js"></script>
    <script src="js_v2/bootstrap.min.js"></script>
    <script src="js_v2/owl.carousel.min.js"></script>
    <script src="js_v2/wow.min.js"></script>
    <script src="js_v2/app.js"></script>
    <!-- ********* -->
    <script>
        new WOW().init();

    </script>
    <!-- ********** -->
    <script>
        $('.owl-carousel').owlCarousel({
            // items:5,
            loop: true,
            dots: false,
            autoplay: true,
            autoplayTimeout: 4000,
            margin: 10,
            // URLhashListener:true,
            // autoplayHoverPause:true,
            // startPosition: 'URLHash',
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },

                1000: {
                    items: 2
                },

                1200: {
                    items: 2
                }
            }
        });

    </script>
    <!-- ********** -->
    <script>
        $(window).on("scroll", function() {
            if ($(window).scrollTop()) {
                $('.nav-index').addClass('scroll');
            } else {
                $('.nav-index').removeClass('scroll');
            }
        })

    </script>
    <!-- *********** -->
    <script>
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('#btnReturn').fadeIn();
            } else {
                $('#btnReturn').fadeOut();
            }
        });

        $('#btnReturn').click(function() {
            $('html, body').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

    </script>
</body>

</html>
