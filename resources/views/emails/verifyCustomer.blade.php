{{-- @dd($customer->verifyCustomer) --}}
<!DOCTYPE html>
<html>
  <head>
    <title>Welcome Email</title>
  </head>
  <body>
      <div class="verify-message-conatainer">
            <h2 style="text-align: center;">
                Welcome to Domli {{$customer->name}}
            </h2>
            <br/>
            <p>
                Your registered email-id is {{$customer->email}} , Please click on the below link to verify your email address account
            </p>
            <br/>
            <a href="{{url('/customer/verify/'. $customer->verifyCustomer->token)}}">Verify your Email</a>
      </div>
  </body>
</html>
