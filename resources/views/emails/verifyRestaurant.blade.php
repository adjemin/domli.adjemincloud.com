{{-- @dd($restaurant) --}}
<!DOCTYPE html>
<html>
  <head>
    <title>Welcome Email</title>
  </head>
  <body>
      <div class="verify-message-conatainer">
            <h2>
                Welcome to Domli {{$restaurant['responsable_name']}}
            </h2>
            <br/>
            <p>
                Your registered email-id is {{$restaurant['email']}} , Please click on the below link to verify your email address account
            </p>
            <br/>
            <a href="{{url('/restaurant/verify/'. $restaurant->verifyRestaurant->token)}}">Verify your Email</a>
      </div>
  </body>
</html>
