<div class="table-responsive-sm">
    <table class="table table-striped" id="orderHistories-table">
        <thead>
            <tr>
                <th>Order Id</th>
        <th>Status</th>
        <th>Customer Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($orderHistories as $orderHistories)
            <tr>
                <td>{{ $orderHistories->order_id }}</td>
            <td>{{ $orderHistories->status }}</td>
            <td>{{ $orderHistories->customer_id }}</td>
                <td>
                    {!! Form::open(['route' => ['orderHistories.destroy', $orderHistories->id]]) !!}
                    @method('DELETE')
                    <div class='btn-group'>
                        <a href="{{ route('orderHistories.show', [$orderHistories->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('orderHistories.edit', [$orderHistories->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>