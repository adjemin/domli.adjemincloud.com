<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\RestaurantPaymentHistorique;

class RestaurantPaymentHistoriqueApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_restaurant_payment_historique()
    {
        $restaurantPaymentHistorique = factory(RestaurantPaymentHistorique::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/restaurant_payment_historiques', $restaurantPaymentHistorique
        );

        $this->assertApiResponse($restaurantPaymentHistorique);
    }

    /**
     * @test
     */
    public function test_read_restaurant_payment_historique()
    {
        $restaurantPaymentHistorique = factory(RestaurantPaymentHistorique::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/restaurant_payment_historiques/'.$restaurantPaymentHistorique->id
        );

        $this->assertApiResponse($restaurantPaymentHistorique->toArray());
    }

    /**
     * @test
     */
    public function test_update_restaurant_payment_historique()
    {
        $restaurantPaymentHistorique = factory(RestaurantPaymentHistorique::class)->create();
        $editedRestaurantPaymentHistorique = factory(RestaurantPaymentHistorique::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/restaurant_payment_historiques/'.$restaurantPaymentHistorique->id,
            $editedRestaurantPaymentHistorique
        );

        $this->assertApiResponse($editedRestaurantPaymentHistorique);
    }

    /**
     * @test
     */
    public function test_delete_restaurant_payment_historique()
    {
        $restaurantPaymentHistorique = factory(RestaurantPaymentHistorique::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/restaurant_payment_historiques/'.$restaurantPaymentHistorique->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/restaurant_payment_historiques/'.$restaurantPaymentHistorique->id
        );

        $this->response->assertStatus(404);
    }
}
