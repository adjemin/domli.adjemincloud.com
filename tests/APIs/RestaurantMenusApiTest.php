<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\RestaurantMenus;

class RestaurantMenusApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_restaurant_menus()
    {
        $restaurantMenus = factory(RestaurantMenus::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/restaurant_menuses', $restaurantMenus
        );

        $this->assertApiResponse($restaurantMenus);
    }

    /**
     * @test
     */
    public function test_read_restaurant_menus()
    {
        $restaurantMenus = factory(RestaurantMenus::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/restaurant_menuses/'.$restaurantMenus->id
        );

        $this->assertApiResponse($restaurantMenus->toArray());
    }

    /**
     * @test
     */
    public function test_update_restaurant_menus()
    {
        $restaurantMenus = factory(RestaurantMenus::class)->create();
        $editedRestaurantMenus = factory(RestaurantMenus::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/restaurant_menuses/'.$restaurantMenus->id,
            $editedRestaurantMenus
        );

        $this->assertApiResponse($editedRestaurantMenus);
    }

    /**
     * @test
     */
    public function test_delete_restaurant_menus()
    {
        $restaurantMenus = factory(RestaurantMenus::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/restaurant_menuses/'.$restaurantMenus->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/restaurant_menuses/'.$restaurantMenus->id
        );

        $this->response->assertStatus(404);
    }
}
