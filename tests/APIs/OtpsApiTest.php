<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Otps;

class OtpsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_otps()
    {
        $otps = factory(Otps::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/otps', $otps
        );

        $this->assertApiResponse($otps);
    }

    /**
     * @test
     */
    public function test_read_otps()
    {
        $otps = factory(Otps::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/otps/'.$otps->id
        );

        $this->assertApiResponse($otps->toArray());
    }

    /**
     * @test
     */
    public function test_update_otps()
    {
        $otps = factory(Otps::class)->create();
        $editedOtps = factory(Otps::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/otps/'.$otps->id,
            $editedOtps
        );

        $this->assertApiResponse($editedOtps);
    }

    /**
     * @test
     */
    public function test_delete_otps()
    {
        $otps = factory(Otps::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/otps/'.$otps->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/otps/'.$otps->id
        );

        $this->response->assertStatus(404);
    }
}
