<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\RestaurantUsers;

class RestaurantUsersApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_restaurant_users()
    {
        $restaurantUsers = factory(RestaurantUsers::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/restaurant_users', $restaurantUsers
        );

        $this->assertApiResponse($restaurantUsers);
    }

    /**
     * @test
     */
    public function test_read_restaurant_users()
    {
        $restaurantUsers = factory(RestaurantUsers::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/restaurant_users/'.$restaurantUsers->id
        );

        $this->assertApiResponse($restaurantUsers->toArray());
    }

    /**
     * @test
     */
    public function test_update_restaurant_users()
    {
        $restaurantUsers = factory(RestaurantUsers::class)->create();
        $editedRestaurantUsers = factory(RestaurantUsers::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/restaurant_users/'.$restaurantUsers->id,
            $editedRestaurantUsers
        );

        $this->assertApiResponse($editedRestaurantUsers);
    }

    /**
     * @test
     */
    public function test_delete_restaurant_users()
    {
        $restaurantUsers = factory(RestaurantUsers::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/restaurant_users/'.$restaurantUsers->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/restaurant_users/'.$restaurantUsers->id
        );

        $this->response->assertStatus(404);
    }
}
