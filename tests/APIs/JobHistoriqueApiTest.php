<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\JobHistorique;

class JobHistoriqueApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_job_historique()
    {
        $jobHistorique = factory(JobHistorique::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/job_historiques', $jobHistorique
        );

        $this->assertApiResponse($jobHistorique);
    }

    /**
     * @test
     */
    public function test_read_job_historique()
    {
        $jobHistorique = factory(JobHistorique::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/job_historiques/'.$jobHistorique->id
        );

        $this->assertApiResponse($jobHistorique->toArray());
    }

    /**
     * @test
     */
    public function test_update_job_historique()
    {
        $jobHistorique = factory(JobHistorique::class)->create();
        $editedJobHistorique = factory(JobHistorique::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/job_historiques/'.$jobHistorique->id,
            $editedJobHistorique
        );

        $this->assertApiResponse($editedJobHistorique);
    }

    /**
     * @test
     */
    public function test_delete_job_historique()
    {
        $jobHistorique = factory(JobHistorique::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/job_historiques/'.$jobHistorique->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/job_historiques/'.$jobHistorique->id
        );

        $this->response->assertStatus(404);
    }
}
