<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\FoodCategories;

class FoodCategoriesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_food_categories()
    {
        $foodCategories = factory(FoodCategories::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/food_categories', $foodCategories
        );

        $this->assertApiResponse($foodCategories);
    }

    /**
     * @test
     */
    public function test_read_food_categories()
    {
        $foodCategories = factory(FoodCategories::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/food_categories/'.$foodCategories->id
        );

        $this->assertApiResponse($foodCategories->toArray());
    }

    /**
     * @test
     */
    public function test_update_food_categories()
    {
        $foodCategories = factory(FoodCategories::class)->create();
        $editedFoodCategories = factory(FoodCategories::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/food_categories/'.$foodCategories->id,
            $editedFoodCategories
        );

        $this->assertApiResponse($editedFoodCategories);
    }

    /**
     * @test
     */
    public function test_delete_food_categories()
    {
        $foodCategories = factory(FoodCategories::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/food_categories/'.$foodCategories->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/food_categories/'.$foodCategories->id
        );

        $this->response->assertStatus(404);
    }
}
