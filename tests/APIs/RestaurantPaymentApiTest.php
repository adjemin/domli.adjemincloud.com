<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\RestaurantPayment;

class RestaurantPaymentApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_restaurant_payment()
    {
        $restaurantPayment = factory(RestaurantPayment::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/restaurant_payments', $restaurantPayment
        );

        $this->assertApiResponse($restaurantPayment);
    }

    /**
     * @test
     */
    public function test_read_restaurant_payment()
    {
        $restaurantPayment = factory(RestaurantPayment::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/restaurant_payments/'.$restaurantPayment->id
        );

        $this->assertApiResponse($restaurantPayment->toArray());
    }

    /**
     * @test
     */
    public function test_update_restaurant_payment()
    {
        $restaurantPayment = factory(RestaurantPayment::class)->create();
        $editedRestaurantPayment = factory(RestaurantPayment::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/restaurant_payments/'.$restaurantPayment->id,
            $editedRestaurantPayment
        );

        $this->assertApiResponse($editedRestaurantPayment);
    }

    /**
     * @test
     */
    public function test_delete_restaurant_payment()
    {
        $restaurantPayment = factory(RestaurantPayment::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/restaurant_payments/'.$restaurantPayment->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/restaurant_payments/'.$restaurantPayment->id
        );

        $this->response->assertStatus(404);
    }
}
