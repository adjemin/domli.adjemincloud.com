<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\OrderDeliveries;

class OrderDeliveriesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_order_deliveries()
    {
        $orderDeliveries = factory(OrderDeliveries::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/order_deliveries', $orderDeliveries
        );

        $this->assertApiResponse($orderDeliveries);
    }

    /**
     * @test
     */
    public function test_read_order_deliveries()
    {
        $orderDeliveries = factory(OrderDeliveries::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/order_deliveries/'.$orderDeliveries->id
        );

        $this->assertApiResponse($orderDeliveries->toArray());
    }

    /**
     * @test
     */
    public function test_update_order_deliveries()
    {
        $orderDeliveries = factory(OrderDeliveries::class)->create();
        $editedOrderDeliveries = factory(OrderDeliveries::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/order_deliveries/'.$orderDeliveries->id,
            $editedOrderDeliveries
        );

        $this->assertApiResponse($editedOrderDeliveries);
    }

    /**
     * @test
     */
    public function test_delete_order_deliveries()
    {
        $orderDeliveries = factory(OrderDeliveries::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/order_deliveries/'.$orderDeliveries->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/order_deliveries/'.$orderDeliveries->id
        );

        $this->response->assertStatus(404);
    }
}
