<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PaymentMethodes;

class PaymentMethodesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_payment_methodes()
    {
        $paymentMethodes = factory(PaymentMethodes::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/payment_methodes', $paymentMethodes
        );

        $this->assertApiResponse($paymentMethodes);
    }

    /**
     * @test
     */
    public function test_read_payment_methodes()
    {
        $paymentMethodes = factory(PaymentMethodes::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/payment_methodes/'.$paymentMethodes->id
        );

        $this->assertApiResponse($paymentMethodes->toArray());
    }

    /**
     * @test
     */
    public function test_update_payment_methodes()
    {
        $paymentMethodes = factory(PaymentMethodes::class)->create();
        $editedPaymentMethodes = factory(PaymentMethodes::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/payment_methodes/'.$paymentMethodes->id,
            $editedPaymentMethodes
        );

        $this->assertApiResponse($editedPaymentMethodes);
    }

    /**
     * @test
     */
    public function test_delete_payment_methodes()
    {
        $paymentMethodes = factory(PaymentMethodes::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/payment_methodes/'.$paymentMethodes->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/payment_methodes/'.$paymentMethodes->id
        );

        $this->response->assertStatus(404);
    }
}
