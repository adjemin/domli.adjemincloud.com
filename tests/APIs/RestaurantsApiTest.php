<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Restaurants;

class RestaurantsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_restaurants()
    {
        $restaurants = factory(Restaurants::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/restaurants', $restaurants
        );

        $this->assertApiResponse($restaurants);
    }

    /**
     * @test
     */
    public function test_read_restaurants()
    {
        $restaurants = factory(Restaurants::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/restaurants/'.$restaurants->id
        );

        $this->assertApiResponse($restaurants->toArray());
    }

    /**
     * @test
     */
    public function test_update_restaurants()
    {
        $restaurants = factory(Restaurants::class)->create();
        $editedRestaurants = factory(Restaurants::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/restaurants/'.$restaurants->id,
            $editedRestaurants
        );

        $this->assertApiResponse($editedRestaurants);
    }

    /**
     * @test
     */
    public function test_delete_restaurants()
    {
        $restaurants = factory(Restaurants::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/restaurants/'.$restaurants->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/restaurants/'.$restaurants->id
        );

        $this->response->assertStatus(404);
    }
}
