<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\RestaurantCompte;

class RestaurantCompteApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_restaurant_compte()
    {
        $restaurantCompte = factory(RestaurantCompte::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/restaurant_comptes', $restaurantCompte
        );

        $this->assertApiResponse($restaurantCompte);
    }

    /**
     * @test
     */
    public function test_read_restaurant_compte()
    {
        $restaurantCompte = factory(RestaurantCompte::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/restaurant_comptes/'.$restaurantCompte->id
        );

        $this->assertApiResponse($restaurantCompte->toArray());
    }

    /**
     * @test
     */
    public function test_update_restaurant_compte()
    {
        $restaurantCompte = factory(RestaurantCompte::class)->create();
        $editedRestaurantCompte = factory(RestaurantCompte::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/restaurant_comptes/'.$restaurantCompte->id,
            $editedRestaurantCompte
        );

        $this->assertApiResponse($editedRestaurantCompte);
    }

    /**
     * @test
     */
    public function test_delete_restaurant_compte()
    {
        $restaurantCompte = factory(RestaurantCompte::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/restaurant_comptes/'.$restaurantCompte->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/restaurant_comptes/'.$restaurantCompte->id
        );

        $this->response->assertStatus(404);
    }
}
