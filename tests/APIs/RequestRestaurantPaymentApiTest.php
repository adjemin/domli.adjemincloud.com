<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\RequestRestaurantPayment;

class RequestRestaurantPaymentApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_request_restaurant_payment()
    {
        $requestRestaurantPayment = factory(RequestRestaurantPayment::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/request_restaurant_payments', $requestRestaurantPayment
        );

        $this->assertApiResponse($requestRestaurantPayment);
    }

    /**
     * @test
     */
    public function test_read_request_restaurant_payment()
    {
        $requestRestaurantPayment = factory(RequestRestaurantPayment::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/request_restaurant_payments/'.$requestRestaurantPayment->id
        );

        $this->assertApiResponse($requestRestaurantPayment->toArray());
    }

    /**
     * @test
     */
    public function test_update_request_restaurant_payment()
    {
        $requestRestaurantPayment = factory(RequestRestaurantPayment::class)->create();
        $editedRequestRestaurantPayment = factory(RequestRestaurantPayment::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/request_restaurant_payments/'.$requestRestaurantPayment->id,
            $editedRequestRestaurantPayment
        );

        $this->assertApiResponse($editedRequestRestaurantPayment);
    }

    /**
     * @test
     */
    public function test_delete_request_restaurant_payment()
    {
        $requestRestaurantPayment = factory(RequestRestaurantPayment::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/request_restaurant_payments/'.$requestRestaurantPayment->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/request_restaurant_payments/'.$requestRestaurantPayment->id
        );

        $this->response->assertStatus(404);
    }
}
