<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PasswordDelivery;

class PasswordDeliveryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_password_delivery()
    {
        $passwordDelivery = factory(PasswordDelivery::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/password_deliveries', $passwordDelivery
        );

        $this->assertApiResponse($passwordDelivery);
    }

    /**
     * @test
     */
    public function test_read_password_delivery()
    {
        $passwordDelivery = factory(PasswordDelivery::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/password_deliveries/'.$passwordDelivery->id
        );

        $this->assertApiResponse($passwordDelivery->toArray());
    }

    /**
     * @test
     */
    public function test_update_password_delivery()
    {
        $passwordDelivery = factory(PasswordDelivery::class)->create();
        $editedPasswordDelivery = factory(PasswordDelivery::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/password_deliveries/'.$passwordDelivery->id,
            $editedPasswordDelivery
        );

        $this->assertApiResponse($editedPasswordDelivery);
    }

    /**
     * @test
     */
    public function test_delete_password_delivery()
    {
        $passwordDelivery = factory(PasswordDelivery::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/password_deliveries/'.$passwordDelivery->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/password_deliveries/'.$passwordDelivery->id
        );

        $this->response->assertStatus(404);
    }
}
