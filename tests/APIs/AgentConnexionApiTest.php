<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\AgentConnexion;

class AgentConnexionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_agent_connexion()
    {
        $agentConnexion = factory(AgentConnexion::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/agent_connexions', $agentConnexion
        );

        $this->assertApiResponse($agentConnexion);
    }

    /**
     * @test
     */
    public function test_read_agent_connexion()
    {
        $agentConnexion = factory(AgentConnexion::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/agent_connexions/'.$agentConnexion->id
        );

        $this->assertApiResponse($agentConnexion->toArray());
    }

    /**
     * @test
     */
    public function test_update_agent_connexion()
    {
        $agentConnexion = factory(AgentConnexion::class)->create();
        $editedAgentConnexion = factory(AgentConnexion::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/agent_connexions/'.$agentConnexion->id,
            $editedAgentConnexion
        );

        $this->assertApiResponse($editedAgentConnexion);
    }

    /**
     * @test
     */
    public function test_delete_agent_connexion()
    {
        $agentConnexion = factory(AgentConnexion::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/agent_connexions/'.$agentConnexion->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/agent_connexions/'.$agentConnexion->id
        );

        $this->response->assertStatus(404);
    }
}
