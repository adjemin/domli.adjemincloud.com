<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DeliveryTypes;

class DeliveryTypesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_delivery_types()
    {
        $deliveryTypes = factory(DeliveryTypes::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/delivery_types', $deliveryTypes
        );

        $this->assertApiResponse($deliveryTypes);
    }

    /**
     * @test
     */
    public function test_read_delivery_types()
    {
        $deliveryTypes = factory(DeliveryTypes::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/delivery_types/'.$deliveryTypes->id
        );

        $this->assertApiResponse($deliveryTypes->toArray());
    }

    /**
     * @test
     */
    public function test_update_delivery_types()
    {
        $deliveryTypes = factory(DeliveryTypes::class)->create();
        $editedDeliveryTypes = factory(DeliveryTypes::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/delivery_types/'.$deliveryTypes->id,
            $editedDeliveryTypes
        );

        $this->assertApiResponse($editedDeliveryTypes);
    }

    /**
     * @test
     */
    public function test_delete_delivery_types()
    {
        $deliveryTypes = factory(DeliveryTypes::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/delivery_types/'.$deliveryTypes->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/delivery_types/'.$deliveryTypes->id
        );

        $this->response->assertStatus(404);
    }
}
