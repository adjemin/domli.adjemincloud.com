<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DeliveryPayment;

class DeliveryPaymentApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_delivery_payment()
    {
        $deliveryPayment = factory(DeliveryPayment::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/delivery_payments', $deliveryPayment
        );

        $this->assertApiResponse($deliveryPayment);
    }

    /**
     * @test
     */
    public function test_read_delivery_payment()
    {
        $deliveryPayment = factory(DeliveryPayment::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/delivery_payments/'.$deliveryPayment->id
        );

        $this->assertApiResponse($deliveryPayment->toArray());
    }

    /**
     * @test
     */
    public function test_update_delivery_payment()
    {
        $deliveryPayment = factory(DeliveryPayment::class)->create();
        $editedDeliveryPayment = factory(DeliveryPayment::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/delivery_payments/'.$deliveryPayment->id,
            $editedDeliveryPayment
        );

        $this->assertApiResponse($editedDeliveryPayment);
    }

    /**
     * @test
     */
    public function test_delete_delivery_payment()
    {
        $deliveryPayment = factory(DeliveryPayment::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/delivery_payments/'.$deliveryPayment->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/delivery_payments/'.$deliveryPayment->id
        );

        $this->response->assertStatus(404);
    }
}
