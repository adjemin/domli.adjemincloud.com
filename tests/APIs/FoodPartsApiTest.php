<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\FoodParts;

class FoodPartsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_food_parts()
    {
        $foodParts = factory(FoodParts::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/food_parts', $foodParts
        );

        $this->assertApiResponse($foodParts);
    }

    /**
     * @test
     */
    public function test_read_food_parts()
    {
        $foodParts = factory(FoodParts::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/food_parts/'.$foodParts->id
        );

        $this->assertApiResponse($foodParts->toArray());
    }

    /**
     * @test
     */
    public function test_update_food_parts()
    {
        $foodParts = factory(FoodParts::class)->create();
        $editedFoodParts = factory(FoodParts::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/food_parts/'.$foodParts->id,
            $editedFoodParts
        );

        $this->assertApiResponse($editedFoodParts);
    }

    /**
     * @test
     */
    public function test_delete_food_parts()
    {
        $foodParts = factory(FoodParts::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/food_parts/'.$foodParts->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/food_parts/'.$foodParts->id
        );

        $this->response->assertStatus(404);
    }
}
