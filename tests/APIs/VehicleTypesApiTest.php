<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\VehicleTypes;

class VehicleTypesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_vehicle_types()
    {
        $vehicleTypes = factory(VehicleTypes::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/vehicle_types', $vehicleTypes
        );

        $this->assertApiResponse($vehicleTypes);
    }

    /**
     * @test
     */
    public function test_read_vehicle_types()
    {
        $vehicleTypes = factory(VehicleTypes::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/vehicle_types/'.$vehicleTypes->id
        );

        $this->assertApiResponse($vehicleTypes->toArray());
    }

    /**
     * @test
     */
    public function test_update_vehicle_types()
    {
        $vehicleTypes = factory(VehicleTypes::class)->create();
        $editedVehicleTypes = factory(VehicleTypes::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/vehicle_types/'.$vehicleTypes->id,
            $editedVehicleTypes
        );

        $this->assertApiResponse($editedVehicleTypes);
    }

    /**
     * @test
     */
    public function test_delete_vehicle_types()
    {
        $vehicleTypes = factory(VehicleTypes::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/vehicle_types/'.$vehicleTypes->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/vehicle_types/'.$vehicleTypes->id
        );

        $this->response->assertStatus(404);
    }
}
