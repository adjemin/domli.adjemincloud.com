<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DeliveryAdresses;

class DeliveryAdressesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_delivery_adresses()
    {
        $deliveryAdresses = factory(DeliveryAdresses::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/delivery_adresses', $deliveryAdresses
        );

        $this->assertApiResponse($deliveryAdresses);
    }

    /**
     * @test
     */
    public function test_read_delivery_adresses()
    {
        $deliveryAdresses = factory(DeliveryAdresses::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/delivery_adresses/'.$deliveryAdresses->id
        );

        $this->assertApiResponse($deliveryAdresses->toArray());
    }

    /**
     * @test
     */
    public function test_update_delivery_adresses()
    {
        $deliveryAdresses = factory(DeliveryAdresses::class)->create();
        $editedDeliveryAdresses = factory(DeliveryAdresses::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/delivery_adresses/'.$deliveryAdresses->id,
            $editedDeliveryAdresses
        );

        $this->assertApiResponse($editedDeliveryAdresses);
    }

    /**
     * @test
     */
    public function test_delete_delivery_adresses()
    {
        $deliveryAdresses = factory(DeliveryAdresses::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/delivery_adresses/'.$deliveryAdresses->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/delivery_adresses/'.$deliveryAdresses->id
        );

        $this->response->assertStatus(404);
    }
}
