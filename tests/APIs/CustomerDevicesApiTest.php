<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CustomerDevices;

class CustomerDevicesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_customer_devices()
    {
        $customerDevices = factory(CustomerDevices::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/customer_devices', $customerDevices
        );

        $this->assertApiResponse($customerDevices);
    }

    /**
     * @test
     */
    public function test_read_customer_devices()
    {
        $customerDevices = factory(CustomerDevices::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/customer_devices/'.$customerDevices->id
        );

        $this->assertApiResponse($customerDevices->toArray());
    }

    /**
     * @test
     */
    public function test_update_customer_devices()
    {
        $customerDevices = factory(CustomerDevices::class)->create();
        $editedCustomerDevices = factory(CustomerDevices::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/customer_devices/'.$customerDevices->id,
            $editedCustomerDevices
        );

        $this->assertApiResponse($editedCustomerDevices);
    }

    /**
     * @test
     */
    public function test_delete_customer_devices()
    {
        $customerDevices = factory(CustomerDevices::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/customer_devices/'.$customerDevices->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/customer_devices/'.$customerDevices->id
        );

        $this->response->assertStatus(404);
    }
}
