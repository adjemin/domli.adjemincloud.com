<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\RequestDeliveryPayment;

class RequestDeliveryPaymentApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_request_delivery_payment()
    {
        $requestDeliveryPayment = factory(RequestDeliveryPayment::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/request_delivery_payments', $requestDeliveryPayment
        );

        $this->assertApiResponse($requestDeliveryPayment);
    }

    /**
     * @test
     */
    public function test_read_request_delivery_payment()
    {
        $requestDeliveryPayment = factory(RequestDeliveryPayment::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/request_delivery_payments/'.$requestDeliveryPayment->id
        );

        $this->assertApiResponse($requestDeliveryPayment->toArray());
    }

    /**
     * @test
     */
    public function test_update_request_delivery_payment()
    {
        $requestDeliveryPayment = factory(RequestDeliveryPayment::class)->create();
        $editedRequestDeliveryPayment = factory(RequestDeliveryPayment::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/request_delivery_payments/'.$requestDeliveryPayment->id,
            $editedRequestDeliveryPayment
        );

        $this->assertApiResponse($editedRequestDeliveryPayment);
    }

    /**
     * @test
     */
    public function test_delete_request_delivery_payment()
    {
        $requestDeliveryPayment = factory(RequestDeliveryPayment::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/request_delivery_payments/'.$requestDeliveryPayment->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/request_delivery_payments/'.$requestDeliveryPayment->id
        );

        $this->response->assertStatus(404);
    }
}
