<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\RestaurantCategories;

class RestaurantCategoriesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_restaurant_categories()
    {
        $restaurantCategories = factory(RestaurantCategories::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/restaurant_categories', $restaurantCategories
        );

        $this->assertApiResponse($restaurantCategories);
    }

    /**
     * @test
     */
    public function test_read_restaurant_categories()
    {
        $restaurantCategories = factory(RestaurantCategories::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/restaurant_categories/'.$restaurantCategories->id
        );

        $this->assertApiResponse($restaurantCategories->toArray());
    }

    /**
     * @test
     */
    public function test_update_restaurant_categories()
    {
        $restaurantCategories = factory(RestaurantCategories::class)->create();
        $editedRestaurantCategories = factory(RestaurantCategories::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/restaurant_categories/'.$restaurantCategories->id,
            $editedRestaurantCategories
        );

        $this->assertApiResponse($editedRestaurantCategories);
    }

    /**
     * @test
     */
    public function test_delete_restaurant_categories()
    {
        $restaurantCategories = factory(RestaurantCategories::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/restaurant_categories/'.$restaurantCategories->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/restaurant_categories/'.$restaurantCategories->id
        );

        $this->response->assertStatus(404);
    }
}
