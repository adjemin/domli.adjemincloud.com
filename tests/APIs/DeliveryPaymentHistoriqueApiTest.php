<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DeliveryPaymentHistorique;

class DeliveryPaymentHistoriqueApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_delivery_payment_historique()
    {
        $deliveryPaymentHistorique = factory(DeliveryPaymentHistorique::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/delivery_payment_historiques', $deliveryPaymentHistorique
        );

        $this->assertApiResponse($deliveryPaymentHistorique);
    }

    /**
     * @test
     */
    public function test_read_delivery_payment_historique()
    {
        $deliveryPaymentHistorique = factory(DeliveryPaymentHistorique::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/delivery_payment_historiques/'.$deliveryPaymentHistorique->id
        );

        $this->assertApiResponse($deliveryPaymentHistorique->toArray());
    }

    /**
     * @test
     */
    public function test_update_delivery_payment_historique()
    {
        $deliveryPaymentHistorique = factory(DeliveryPaymentHistorique::class)->create();
        $editedDeliveryPaymentHistorique = factory(DeliveryPaymentHistorique::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/delivery_payment_historiques/'.$deliveryPaymentHistorique->id,
            $editedDeliveryPaymentHistorique
        );

        $this->assertApiResponse($editedDeliveryPaymentHistorique);
    }

    /**
     * @test
     */
    public function test_delete_delivery_payment_historique()
    {
        $deliveryPaymentHistorique = factory(DeliveryPaymentHistorique::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/delivery_payment_historiques/'.$deliveryPaymentHistorique->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/delivery_payment_historiques/'.$deliveryPaymentHistorique->id
        );

        $this->response->assertStatus(404);
    }
}
