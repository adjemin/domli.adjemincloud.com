<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\OrderAssignement;

class OrderAssignementApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_order_assignement()
    {
        $orderAssignement = factory(OrderAssignement::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/order_assignements', $orderAssignement
        );

        $this->assertApiResponse($orderAssignement);
    }

    /**
     * @test
     */
    public function test_read_order_assignement()
    {
        $orderAssignement = factory(OrderAssignement::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/order_assignements/'.$orderAssignement->id
        );

        $this->assertApiResponse($orderAssignement->toArray());
    }

    /**
     * @test
     */
    public function test_update_order_assignement()
    {
        $orderAssignement = factory(OrderAssignement::class)->create();
        $editedOrderAssignement = factory(OrderAssignement::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/order_assignements/'.$orderAssignement->id,
            $editedOrderAssignement
        );

        $this->assertApiResponse($editedOrderAssignement);
    }

    /**
     * @test
     */
    public function test_delete_order_assignement()
    {
        $orderAssignement = factory(OrderAssignement::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/order_assignements/'.$orderAssignement->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/order_assignements/'.$orderAssignement->id
        );

        $this->response->assertStatus(404);
    }
}
