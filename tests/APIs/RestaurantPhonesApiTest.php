<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\RestaurantPhones;

class RestaurantPhonesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_restaurant_phones()
    {
        $restaurantPhones = factory(RestaurantPhones::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/restaurant_phones', $restaurantPhones
        );

        $this->assertApiResponse($restaurantPhones);
    }

    /**
     * @test
     */
    public function test_read_restaurant_phones()
    {
        $restaurantPhones = factory(RestaurantPhones::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/restaurant_phones/'.$restaurantPhones->id
        );

        $this->assertApiResponse($restaurantPhones->toArray());
    }

    /**
     * @test
     */
    public function test_update_restaurant_phones()
    {
        $restaurantPhones = factory(RestaurantPhones::class)->create();
        $editedRestaurantPhones = factory(RestaurantPhones::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/restaurant_phones/'.$restaurantPhones->id,
            $editedRestaurantPhones
        );

        $this->assertApiResponse($editedRestaurantPhones);
    }

    /**
     * @test
     */
    public function test_delete_restaurant_phones()
    {
        $restaurantPhones = factory(RestaurantPhones::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/restaurant_phones/'.$restaurantPhones->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/restaurant_phones/'.$restaurantPhones->id
        );

        $this->response->assertStatus(404);
    }
}
