<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\OrderHistories;

class OrderHistoriesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_order_histories()
    {
        $orderHistories = factory(OrderHistories::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/order_histories', $orderHistories
        );

        $this->assertApiResponse($orderHistories);
    }

    /**
     * @test
     */
    public function test_read_order_histories()
    {
        $orderHistories = factory(OrderHistories::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/order_histories/'.$orderHistories->id
        );

        $this->assertApiResponse($orderHistories->toArray());
    }

    /**
     * @test
     */
    public function test_update_order_histories()
    {
        $orderHistories = factory(OrderHistories::class)->create();
        $editedOrderHistories = factory(OrderHistories::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/order_histories/'.$orderHistories->id,
            $editedOrderHistories
        );

        $this->assertApiResponse($editedOrderHistories);
    }

    /**
     * @test
     */
    public function test_delete_order_histories()
    {
        $orderHistories = factory(OrderHistories::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/order_histories/'.$orderHistories->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/order_histories/'.$orderHistories->id
        );

        $this->response->assertStatus(404);
    }
}
