<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\OrderItems;

class OrderItemsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_order_items()
    {
        $orderItems = factory(OrderItems::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/order_items', $orderItems
        );

        $this->assertApiResponse($orderItems);
    }

    /**
     * @test
     */
    public function test_read_order_items()
    {
        $orderItems = factory(OrderItems::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/order_items/'.$orderItems->id
        );

        $this->assertApiResponse($orderItems->toArray());
    }

    /**
     * @test
     */
    public function test_update_order_items()
    {
        $orderItems = factory(OrderItems::class)->create();
        $editedOrderItems = factory(OrderItems::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/order_items/'.$orderItems->id,
            $editedOrderItems
        );

        $this->assertApiResponse($editedOrderItems);
    }

    /**
     * @test
     */
    public function test_delete_order_items()
    {
        $orderItems = factory(OrderItems::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/order_items/'.$orderItems->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/order_items/'.$orderItems->id
        );

        $this->response->assertStatus(404);
    }
}
