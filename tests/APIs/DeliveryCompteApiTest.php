<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DeliveryCompte;

class DeliveryCompteApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_delivery_compte()
    {
        $deliveryCompte = factory(DeliveryCompte::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/delivery_comptes', $deliveryCompte
        );

        $this->assertApiResponse($deliveryCompte);
    }

    /**
     * @test
     */
    public function test_read_delivery_compte()
    {
        $deliveryCompte = factory(DeliveryCompte::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/delivery_comptes/'.$deliveryCompte->id
        );

        $this->assertApiResponse($deliveryCompte->toArray());
    }

    /**
     * @test
     */
    public function test_update_delivery_compte()
    {
        $deliveryCompte = factory(DeliveryCompte::class)->create();
        $editedDeliveryCompte = factory(DeliveryCompte::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/delivery_comptes/'.$deliveryCompte->id,
            $editedDeliveryCompte
        );

        $this->assertApiResponse($editedDeliveryCompte);
    }

    /**
     * @test
     */
    public function test_delete_delivery_compte()
    {
        $deliveryCompte = factory(DeliveryCompte::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/delivery_comptes/'.$deliveryCompte->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/delivery_comptes/'.$deliveryCompte->id
        );

        $this->response->assertStatus(404);
    }
}
