<?php namespace Tests\Repositories;

use App\Models\DeliveryCompte;
use App\Repositories\DeliveryCompteRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DeliveryCompteRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DeliveryCompteRepository
     */
    protected $deliveryCompteRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->deliveryCompteRepo = \App::make(DeliveryCompteRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_delivery_compte()
    {
        $deliveryCompte = factory(DeliveryCompte::class)->make()->toArray();

        $createdDeliveryCompte = $this->deliveryCompteRepo->create($deliveryCompte);

        $createdDeliveryCompte = $createdDeliveryCompte->toArray();
        $this->assertArrayHasKey('id', $createdDeliveryCompte);
        $this->assertNotNull($createdDeliveryCompte['id'], 'Created DeliveryCompte must have id specified');
        $this->assertNotNull(DeliveryCompte::find($createdDeliveryCompte['id']), 'DeliveryCompte with given id must be in DB');
        $this->assertModelData($deliveryCompte, $createdDeliveryCompte);
    }

    /**
     * @test read
     */
    public function test_read_delivery_compte()
    {
        $deliveryCompte = factory(DeliveryCompte::class)->create();

        $dbDeliveryCompte = $this->deliveryCompteRepo->find($deliveryCompte->id);

        $dbDeliveryCompte = $dbDeliveryCompte->toArray();
        $this->assertModelData($deliveryCompte->toArray(), $dbDeliveryCompte);
    }

    /**
     * @test update
     */
    public function test_update_delivery_compte()
    {
        $deliveryCompte = factory(DeliveryCompte::class)->create();
        $fakeDeliveryCompte = factory(DeliveryCompte::class)->make()->toArray();

        $updatedDeliveryCompte = $this->deliveryCompteRepo->update($fakeDeliveryCompte, $deliveryCompte->id);

        $this->assertModelData($fakeDeliveryCompte, $updatedDeliveryCompte->toArray());
        $dbDeliveryCompte = $this->deliveryCompteRepo->find($deliveryCompte->id);
        $this->assertModelData($fakeDeliveryCompte, $dbDeliveryCompte->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_delivery_compte()
    {
        $deliveryCompte = factory(DeliveryCompte::class)->create();

        $resp = $this->deliveryCompteRepo->delete($deliveryCompte->id);

        $this->assertTrue($resp);
        $this->assertNull(DeliveryCompte::find($deliveryCompte->id), 'DeliveryCompte should not exist in DB');
    }
}
