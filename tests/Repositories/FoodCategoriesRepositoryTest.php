<?php namespace Tests\Repositories;

use App\Models\FoodCategories;
use App\Repositories\FoodCategoriesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class FoodCategoriesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var FoodCategoriesRepository
     */
    protected $foodCategoriesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->foodCategoriesRepo = \App::make(FoodCategoriesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_food_categories()
    {
        $foodCategories = factory(FoodCategories::class)->make()->toArray();

        $createdFoodCategories = $this->foodCategoriesRepo->create($foodCategories);

        $createdFoodCategories = $createdFoodCategories->toArray();
        $this->assertArrayHasKey('id', $createdFoodCategories);
        $this->assertNotNull($createdFoodCategories['id'], 'Created FoodCategories must have id specified');
        $this->assertNotNull(FoodCategories::find($createdFoodCategories['id']), 'FoodCategories with given id must be in DB');
        $this->assertModelData($foodCategories, $createdFoodCategories);
    }

    /**
     * @test read
     */
    public function test_read_food_categories()
    {
        $foodCategories = factory(FoodCategories::class)->create();

        $dbFoodCategories = $this->foodCategoriesRepo->find($foodCategories->id);

        $dbFoodCategories = $dbFoodCategories->toArray();
        $this->assertModelData($foodCategories->toArray(), $dbFoodCategories);
    }

    /**
     * @test update
     */
    public function test_update_food_categories()
    {
        $foodCategories = factory(FoodCategories::class)->create();
        $fakeFoodCategories = factory(FoodCategories::class)->make()->toArray();

        $updatedFoodCategories = $this->foodCategoriesRepo->update($fakeFoodCategories, $foodCategories->id);

        $this->assertModelData($fakeFoodCategories, $updatedFoodCategories->toArray());
        $dbFoodCategories = $this->foodCategoriesRepo->find($foodCategories->id);
        $this->assertModelData($fakeFoodCategories, $dbFoodCategories->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_food_categories()
    {
        $foodCategories = factory(FoodCategories::class)->create();

        $resp = $this->foodCategoriesRepo->delete($foodCategories->id);

        $this->assertTrue($resp);
        $this->assertNull(FoodCategories::find($foodCategories->id), 'FoodCategories should not exist in DB');
    }
}
