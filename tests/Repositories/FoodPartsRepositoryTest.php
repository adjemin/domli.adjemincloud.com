<?php namespace Tests\Repositories;

use App\Models\FoodParts;
use App\Repositories\FoodPartsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class FoodPartsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var FoodPartsRepository
     */
    protected $foodPartsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->foodPartsRepo = \App::make(FoodPartsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_food_parts()
    {
        $foodParts = factory(FoodParts::class)->make()->toArray();

        $createdFoodParts = $this->foodPartsRepo->create($foodParts);

        $createdFoodParts = $createdFoodParts->toArray();
        $this->assertArrayHasKey('id', $createdFoodParts);
        $this->assertNotNull($createdFoodParts['id'], 'Created FoodParts must have id specified');
        $this->assertNotNull(FoodParts::find($createdFoodParts['id']), 'FoodParts with given id must be in DB');
        $this->assertModelData($foodParts, $createdFoodParts);
    }

    /**
     * @test read
     */
    public function test_read_food_parts()
    {
        $foodParts = factory(FoodParts::class)->create();

        $dbFoodParts = $this->foodPartsRepo->find($foodParts->id);

        $dbFoodParts = $dbFoodParts->toArray();
        $this->assertModelData($foodParts->toArray(), $dbFoodParts);
    }

    /**
     * @test update
     */
    public function test_update_food_parts()
    {
        $foodParts = factory(FoodParts::class)->create();
        $fakeFoodParts = factory(FoodParts::class)->make()->toArray();

        $updatedFoodParts = $this->foodPartsRepo->update($fakeFoodParts, $foodParts->id);

        $this->assertModelData($fakeFoodParts, $updatedFoodParts->toArray());
        $dbFoodParts = $this->foodPartsRepo->find($foodParts->id);
        $this->assertModelData($fakeFoodParts, $dbFoodParts->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_food_parts()
    {
        $foodParts = factory(FoodParts::class)->create();

        $resp = $this->foodPartsRepo->delete($foodParts->id);

        $this->assertTrue($resp);
        $this->assertNull(FoodParts::find($foodParts->id), 'FoodParts should not exist in DB');
    }
}
