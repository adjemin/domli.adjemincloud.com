<?php namespace Tests\Repositories;

use App\Models\DeliveryAdresses;
use App\Repositories\DeliveryAdressesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DeliveryAdressesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DeliveryAdressesRepository
     */
    protected $deliveryAdressesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->deliveryAdressesRepo = \App::make(DeliveryAdressesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_delivery_adresses()
    {
        $deliveryAdresses = factory(DeliveryAdresses::class)->make()->toArray();

        $createdDeliveryAdresses = $this->deliveryAdressesRepo->create($deliveryAdresses);

        $createdDeliveryAdresses = $createdDeliveryAdresses->toArray();
        $this->assertArrayHasKey('id', $createdDeliveryAdresses);
        $this->assertNotNull($createdDeliveryAdresses['id'], 'Created DeliveryAdresses must have id specified');
        $this->assertNotNull(DeliveryAdresses::find($createdDeliveryAdresses['id']), 'DeliveryAdresses with given id must be in DB');
        $this->assertModelData($deliveryAdresses, $createdDeliveryAdresses);
    }

    /**
     * @test read
     */
    public function test_read_delivery_adresses()
    {
        $deliveryAdresses = factory(DeliveryAdresses::class)->create();

        $dbDeliveryAdresses = $this->deliveryAdressesRepo->find($deliveryAdresses->id);

        $dbDeliveryAdresses = $dbDeliveryAdresses->toArray();
        $this->assertModelData($deliveryAdresses->toArray(), $dbDeliveryAdresses);
    }

    /**
     * @test update
     */
    public function test_update_delivery_adresses()
    {
        $deliveryAdresses = factory(DeliveryAdresses::class)->create();
        $fakeDeliveryAdresses = factory(DeliveryAdresses::class)->make()->toArray();

        $updatedDeliveryAdresses = $this->deliveryAdressesRepo->update($fakeDeliveryAdresses, $deliveryAdresses->id);

        $this->assertModelData($fakeDeliveryAdresses, $updatedDeliveryAdresses->toArray());
        $dbDeliveryAdresses = $this->deliveryAdressesRepo->find($deliveryAdresses->id);
        $this->assertModelData($fakeDeliveryAdresses, $dbDeliveryAdresses->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_delivery_adresses()
    {
        $deliveryAdresses = factory(DeliveryAdresses::class)->create();

        $resp = $this->deliveryAdressesRepo->delete($deliveryAdresses->id);

        $this->assertTrue($resp);
        $this->assertNull(DeliveryAdresses::find($deliveryAdresses->id), 'DeliveryAdresses should not exist in DB');
    }
}
