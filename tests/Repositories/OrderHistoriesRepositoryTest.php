<?php namespace Tests\Repositories;

use App\Models\OrderHistories;
use App\Repositories\OrderHistoriesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OrderHistoriesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrderHistoriesRepository
     */
    protected $orderHistoriesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->orderHistoriesRepo = \App::make(OrderHistoriesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_order_histories()
    {
        $orderHistories = factory(OrderHistories::class)->make()->toArray();

        $createdOrderHistories = $this->orderHistoriesRepo->create($orderHistories);

        $createdOrderHistories = $createdOrderHistories->toArray();
        $this->assertArrayHasKey('id', $createdOrderHistories);
        $this->assertNotNull($createdOrderHistories['id'], 'Created OrderHistories must have id specified');
        $this->assertNotNull(OrderHistories::find($createdOrderHistories['id']), 'OrderHistories with given id must be in DB');
        $this->assertModelData($orderHistories, $createdOrderHistories);
    }

    /**
     * @test read
     */
    public function test_read_order_histories()
    {
        $orderHistories = factory(OrderHistories::class)->create();

        $dbOrderHistories = $this->orderHistoriesRepo->find($orderHistories->id);

        $dbOrderHistories = $dbOrderHistories->toArray();
        $this->assertModelData($orderHistories->toArray(), $dbOrderHistories);
    }

    /**
     * @test update
     */
    public function test_update_order_histories()
    {
        $orderHistories = factory(OrderHistories::class)->create();
        $fakeOrderHistories = factory(OrderHistories::class)->make()->toArray();

        $updatedOrderHistories = $this->orderHistoriesRepo->update($fakeOrderHistories, $orderHistories->id);

        $this->assertModelData($fakeOrderHistories, $updatedOrderHistories->toArray());
        $dbOrderHistories = $this->orderHistoriesRepo->find($orderHistories->id);
        $this->assertModelData($fakeOrderHistories, $dbOrderHistories->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_order_histories()
    {
        $orderHistories = factory(OrderHistories::class)->create();

        $resp = $this->orderHistoriesRepo->delete($orderHistories->id);

        $this->assertTrue($resp);
        $this->assertNull(OrderHistories::find($orderHistories->id), 'OrderHistories should not exist in DB');
    }
}
