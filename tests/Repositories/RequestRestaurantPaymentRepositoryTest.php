<?php namespace Tests\Repositories;

use App\Models\RequestRestaurantPayment;
use App\Repositories\RequestRestaurantPaymentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RequestRestaurantPaymentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RequestRestaurantPaymentRepository
     */
    protected $requestRestaurantPaymentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->requestRestaurantPaymentRepo = \App::make(RequestRestaurantPaymentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_request_restaurant_payment()
    {
        $requestRestaurantPayment = factory(RequestRestaurantPayment::class)->make()->toArray();

        $createdRequestRestaurantPayment = $this->requestRestaurantPaymentRepo->create($requestRestaurantPayment);

        $createdRequestRestaurantPayment = $createdRequestRestaurantPayment->toArray();
        $this->assertArrayHasKey('id', $createdRequestRestaurantPayment);
        $this->assertNotNull($createdRequestRestaurantPayment['id'], 'Created RequestRestaurantPayment must have id specified');
        $this->assertNotNull(RequestRestaurantPayment::find($createdRequestRestaurantPayment['id']), 'RequestRestaurantPayment with given id must be in DB');
        $this->assertModelData($requestRestaurantPayment, $createdRequestRestaurantPayment);
    }

    /**
     * @test read
     */
    public function test_read_request_restaurant_payment()
    {
        $requestRestaurantPayment = factory(RequestRestaurantPayment::class)->create();

        $dbRequestRestaurantPayment = $this->requestRestaurantPaymentRepo->find($requestRestaurantPayment->id);

        $dbRequestRestaurantPayment = $dbRequestRestaurantPayment->toArray();
        $this->assertModelData($requestRestaurantPayment->toArray(), $dbRequestRestaurantPayment);
    }

    /**
     * @test update
     */
    public function test_update_request_restaurant_payment()
    {
        $requestRestaurantPayment = factory(RequestRestaurantPayment::class)->create();
        $fakeRequestRestaurantPayment = factory(RequestRestaurantPayment::class)->make()->toArray();

        $updatedRequestRestaurantPayment = $this->requestRestaurantPaymentRepo->update($fakeRequestRestaurantPayment, $requestRestaurantPayment->id);

        $this->assertModelData($fakeRequestRestaurantPayment, $updatedRequestRestaurantPayment->toArray());
        $dbRequestRestaurantPayment = $this->requestRestaurantPaymentRepo->find($requestRestaurantPayment->id);
        $this->assertModelData($fakeRequestRestaurantPayment, $dbRequestRestaurantPayment->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_request_restaurant_payment()
    {
        $requestRestaurantPayment = factory(RequestRestaurantPayment::class)->create();

        $resp = $this->requestRestaurantPaymentRepo->delete($requestRestaurantPayment->id);

        $this->assertTrue($resp);
        $this->assertNull(RequestRestaurantPayment::find($requestRestaurantPayment->id), 'RequestRestaurantPayment should not exist in DB');
    }
}
