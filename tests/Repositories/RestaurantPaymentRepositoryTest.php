<?php namespace Tests\Repositories;

use App\Models\RestaurantPayment;
use App\Repositories\RestaurantPaymentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RestaurantPaymentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RestaurantPaymentRepository
     */
    protected $restaurantPaymentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->restaurantPaymentRepo = \App::make(RestaurantPaymentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_restaurant_payment()
    {
        $restaurantPayment = factory(RestaurantPayment::class)->make()->toArray();

        $createdRestaurantPayment = $this->restaurantPaymentRepo->create($restaurantPayment);

        $createdRestaurantPayment = $createdRestaurantPayment->toArray();
        $this->assertArrayHasKey('id', $createdRestaurantPayment);
        $this->assertNotNull($createdRestaurantPayment['id'], 'Created RestaurantPayment must have id specified');
        $this->assertNotNull(RestaurantPayment::find($createdRestaurantPayment['id']), 'RestaurantPayment with given id must be in DB');
        $this->assertModelData($restaurantPayment, $createdRestaurantPayment);
    }

    /**
     * @test read
     */
    public function test_read_restaurant_payment()
    {
        $restaurantPayment = factory(RestaurantPayment::class)->create();

        $dbRestaurantPayment = $this->restaurantPaymentRepo->find($restaurantPayment->id);

        $dbRestaurantPayment = $dbRestaurantPayment->toArray();
        $this->assertModelData($restaurantPayment->toArray(), $dbRestaurantPayment);
    }

    /**
     * @test update
     */
    public function test_update_restaurant_payment()
    {
        $restaurantPayment = factory(RestaurantPayment::class)->create();
        $fakeRestaurantPayment = factory(RestaurantPayment::class)->make()->toArray();

        $updatedRestaurantPayment = $this->restaurantPaymentRepo->update($fakeRestaurantPayment, $restaurantPayment->id);

        $this->assertModelData($fakeRestaurantPayment, $updatedRestaurantPayment->toArray());
        $dbRestaurantPayment = $this->restaurantPaymentRepo->find($restaurantPayment->id);
        $this->assertModelData($fakeRestaurantPayment, $dbRestaurantPayment->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_restaurant_payment()
    {
        $restaurantPayment = factory(RestaurantPayment::class)->create();

        $resp = $this->restaurantPaymentRepo->delete($restaurantPayment->id);

        $this->assertTrue($resp);
        $this->assertNull(RestaurantPayment::find($restaurantPayment->id), 'RestaurantPayment should not exist in DB');
    }
}
