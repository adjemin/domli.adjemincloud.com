<?php namespace Tests\Repositories;

use App\Models\JobHistorique;
use App\Repositories\JobHistoriqueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class JobHistoriqueRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var JobHistoriqueRepository
     */
    protected $jobHistoriqueRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->jobHistoriqueRepo = \App::make(JobHistoriqueRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_job_historique()
    {
        $jobHistorique = factory(JobHistorique::class)->make()->toArray();

        $createdJobHistorique = $this->jobHistoriqueRepo->create($jobHistorique);

        $createdJobHistorique = $createdJobHistorique->toArray();
        $this->assertArrayHasKey('id', $createdJobHistorique);
        $this->assertNotNull($createdJobHistorique['id'], 'Created JobHistorique must have id specified');
        $this->assertNotNull(JobHistorique::find($createdJobHistorique['id']), 'JobHistorique with given id must be in DB');
        $this->assertModelData($jobHistorique, $createdJobHistorique);
    }

    /**
     * @test read
     */
    public function test_read_job_historique()
    {
        $jobHistorique = factory(JobHistorique::class)->create();

        $dbJobHistorique = $this->jobHistoriqueRepo->find($jobHistorique->id);

        $dbJobHistorique = $dbJobHistorique->toArray();
        $this->assertModelData($jobHistorique->toArray(), $dbJobHistorique);
    }

    /**
     * @test update
     */
    public function test_update_job_historique()
    {
        $jobHistorique = factory(JobHistorique::class)->create();
        $fakeJobHistorique = factory(JobHistorique::class)->make()->toArray();

        $updatedJobHistorique = $this->jobHistoriqueRepo->update($fakeJobHistorique, $jobHistorique->id);

        $this->assertModelData($fakeJobHistorique, $updatedJobHistorique->toArray());
        $dbJobHistorique = $this->jobHistoriqueRepo->find($jobHistorique->id);
        $this->assertModelData($fakeJobHistorique, $dbJobHistorique->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_job_historique()
    {
        $jobHistorique = factory(JobHistorique::class)->create();

        $resp = $this->jobHistoriqueRepo->delete($jobHistorique->id);

        $this->assertTrue($resp);
        $this->assertNull(JobHistorique::find($jobHistorique->id), 'JobHistorique should not exist in DB');
    }
}
