<?php namespace Tests\Repositories;

use App\Models\VehicleTypes;
use App\Repositories\VehicleTypesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class VehicleTypesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var VehicleTypesRepository
     */
    protected $vehicleTypesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->vehicleTypesRepo = \App::make(VehicleTypesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_vehicle_types()
    {
        $vehicleTypes = factory(VehicleTypes::class)->make()->toArray();

        $createdVehicleTypes = $this->vehicleTypesRepo->create($vehicleTypes);

        $createdVehicleTypes = $createdVehicleTypes->toArray();
        $this->assertArrayHasKey('id', $createdVehicleTypes);
        $this->assertNotNull($createdVehicleTypes['id'], 'Created VehicleTypes must have id specified');
        $this->assertNotNull(VehicleTypes::find($createdVehicleTypes['id']), 'VehicleTypes with given id must be in DB');
        $this->assertModelData($vehicleTypes, $createdVehicleTypes);
    }

    /**
     * @test read
     */
    public function test_read_vehicle_types()
    {
        $vehicleTypes = factory(VehicleTypes::class)->create();

        $dbVehicleTypes = $this->vehicleTypesRepo->find($vehicleTypes->id);

        $dbVehicleTypes = $dbVehicleTypes->toArray();
        $this->assertModelData($vehicleTypes->toArray(), $dbVehicleTypes);
    }

    /**
     * @test update
     */
    public function test_update_vehicle_types()
    {
        $vehicleTypes = factory(VehicleTypes::class)->create();
        $fakeVehicleTypes = factory(VehicleTypes::class)->make()->toArray();

        $updatedVehicleTypes = $this->vehicleTypesRepo->update($fakeVehicleTypes, $vehicleTypes->id);

        $this->assertModelData($fakeVehicleTypes, $updatedVehicleTypes->toArray());
        $dbVehicleTypes = $this->vehicleTypesRepo->find($vehicleTypes->id);
        $this->assertModelData($fakeVehicleTypes, $dbVehicleTypes->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_vehicle_types()
    {
        $vehicleTypes = factory(VehicleTypes::class)->create();

        $resp = $this->vehicleTypesRepo->delete($vehicleTypes->id);

        $this->assertTrue($resp);
        $this->assertNull(VehicleTypes::find($vehicleTypes->id), 'VehicleTypes should not exist in DB');
    }
}
