<?php namespace Tests\Repositories;

use App\Models\DeliveryTypes;
use App\Repositories\DeliveryTypesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DeliveryTypesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DeliveryTypesRepository
     */
    protected $deliveryTypesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->deliveryTypesRepo = \App::make(DeliveryTypesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_delivery_types()
    {
        $deliveryTypes = factory(DeliveryTypes::class)->make()->toArray();

        $createdDeliveryTypes = $this->deliveryTypesRepo->create($deliveryTypes);

        $createdDeliveryTypes = $createdDeliveryTypes->toArray();
        $this->assertArrayHasKey('id', $createdDeliveryTypes);
        $this->assertNotNull($createdDeliveryTypes['id'], 'Created DeliveryTypes must have id specified');
        $this->assertNotNull(DeliveryTypes::find($createdDeliveryTypes['id']), 'DeliveryTypes with given id must be in DB');
        $this->assertModelData($deliveryTypes, $createdDeliveryTypes);
    }

    /**
     * @test read
     */
    public function test_read_delivery_types()
    {
        $deliveryTypes = factory(DeliveryTypes::class)->create();

        $dbDeliveryTypes = $this->deliveryTypesRepo->find($deliveryTypes->id);

        $dbDeliveryTypes = $dbDeliveryTypes->toArray();
        $this->assertModelData($deliveryTypes->toArray(), $dbDeliveryTypes);
    }

    /**
     * @test update
     */
    public function test_update_delivery_types()
    {
        $deliveryTypes = factory(DeliveryTypes::class)->create();
        $fakeDeliveryTypes = factory(DeliveryTypes::class)->make()->toArray();

        $updatedDeliveryTypes = $this->deliveryTypesRepo->update($fakeDeliveryTypes, $deliveryTypes->id);

        $this->assertModelData($fakeDeliveryTypes, $updatedDeliveryTypes->toArray());
        $dbDeliveryTypes = $this->deliveryTypesRepo->find($deliveryTypes->id);
        $this->assertModelData($fakeDeliveryTypes, $dbDeliveryTypes->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_delivery_types()
    {
        $deliveryTypes = factory(DeliveryTypes::class)->create();

        $resp = $this->deliveryTypesRepo->delete($deliveryTypes->id);

        $this->assertTrue($resp);
        $this->assertNull(DeliveryTypes::find($deliveryTypes->id), 'DeliveryTypes should not exist in DB');
    }
}
