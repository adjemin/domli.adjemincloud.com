<?php namespace Tests\Repositories;

use App\Models\OrderAssignement;
use App\Repositories\OrderAssignementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OrderAssignementRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrderAssignementRepository
     */
    protected $orderAssignementRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->orderAssignementRepo = \App::make(OrderAssignementRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_order_assignement()
    {
        $orderAssignement = factory(OrderAssignement::class)->make()->toArray();

        $createdOrderAssignement = $this->orderAssignementRepo->create($orderAssignement);

        $createdOrderAssignement = $createdOrderAssignement->toArray();
        $this->assertArrayHasKey('id', $createdOrderAssignement);
        $this->assertNotNull($createdOrderAssignement['id'], 'Created OrderAssignement must have id specified');
        $this->assertNotNull(OrderAssignement::find($createdOrderAssignement['id']), 'OrderAssignement with given id must be in DB');
        $this->assertModelData($orderAssignement, $createdOrderAssignement);
    }

    /**
     * @test read
     */
    public function test_read_order_assignement()
    {
        $orderAssignement = factory(OrderAssignement::class)->create();

        $dbOrderAssignement = $this->orderAssignementRepo->find($orderAssignement->id);

        $dbOrderAssignement = $dbOrderAssignement->toArray();
        $this->assertModelData($orderAssignement->toArray(), $dbOrderAssignement);
    }

    /**
     * @test update
     */
    public function test_update_order_assignement()
    {
        $orderAssignement = factory(OrderAssignement::class)->create();
        $fakeOrderAssignement = factory(OrderAssignement::class)->make()->toArray();

        $updatedOrderAssignement = $this->orderAssignementRepo->update($fakeOrderAssignement, $orderAssignement->id);

        $this->assertModelData($fakeOrderAssignement, $updatedOrderAssignement->toArray());
        $dbOrderAssignement = $this->orderAssignementRepo->find($orderAssignement->id);
        $this->assertModelData($fakeOrderAssignement, $dbOrderAssignement->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_order_assignement()
    {
        $orderAssignement = factory(OrderAssignement::class)->create();

        $resp = $this->orderAssignementRepo->delete($orderAssignement->id);

        $this->assertTrue($resp);
        $this->assertNull(OrderAssignement::find($orderAssignement->id), 'OrderAssignement should not exist in DB');
    }
}
