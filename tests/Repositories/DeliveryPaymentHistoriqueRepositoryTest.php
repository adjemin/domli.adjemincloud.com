<?php namespace Tests\Repositories;

use App\Models\DeliveryPaymentHistorique;
use App\Repositories\DeliveryPaymentHistoriqueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DeliveryPaymentHistoriqueRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DeliveryPaymentHistoriqueRepository
     */
    protected $deliveryPaymentHistoriqueRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->deliveryPaymentHistoriqueRepo = \App::make(DeliveryPaymentHistoriqueRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_delivery_payment_historique()
    {
        $deliveryPaymentHistorique = factory(DeliveryPaymentHistorique::class)->make()->toArray();

        $createdDeliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepo->create($deliveryPaymentHistorique);

        $createdDeliveryPaymentHistorique = $createdDeliveryPaymentHistorique->toArray();
        $this->assertArrayHasKey('id', $createdDeliveryPaymentHistorique);
        $this->assertNotNull($createdDeliveryPaymentHistorique['id'], 'Created DeliveryPaymentHistorique must have id specified');
        $this->assertNotNull(DeliveryPaymentHistorique::find($createdDeliveryPaymentHistorique['id']), 'DeliveryPaymentHistorique with given id must be in DB');
        $this->assertModelData($deliveryPaymentHistorique, $createdDeliveryPaymentHistorique);
    }

    /**
     * @test read
     */
    public function test_read_delivery_payment_historique()
    {
        $deliveryPaymentHistorique = factory(DeliveryPaymentHistorique::class)->create();

        $dbDeliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepo->find($deliveryPaymentHistorique->id);

        $dbDeliveryPaymentHistorique = $dbDeliveryPaymentHistorique->toArray();
        $this->assertModelData($deliveryPaymentHistorique->toArray(), $dbDeliveryPaymentHistorique);
    }

    /**
     * @test update
     */
    public function test_update_delivery_payment_historique()
    {
        $deliveryPaymentHistorique = factory(DeliveryPaymentHistorique::class)->create();
        $fakeDeliveryPaymentHistorique = factory(DeliveryPaymentHistorique::class)->make()->toArray();

        $updatedDeliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepo->update($fakeDeliveryPaymentHistorique, $deliveryPaymentHistorique->id);

        $this->assertModelData($fakeDeliveryPaymentHistorique, $updatedDeliveryPaymentHistorique->toArray());
        $dbDeliveryPaymentHistorique = $this->deliveryPaymentHistoriqueRepo->find($deliveryPaymentHistorique->id);
        $this->assertModelData($fakeDeliveryPaymentHistorique, $dbDeliveryPaymentHistorique->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_delivery_payment_historique()
    {
        $deliveryPaymentHistorique = factory(DeliveryPaymentHistorique::class)->create();

        $resp = $this->deliveryPaymentHistoriqueRepo->delete($deliveryPaymentHistorique->id);

        $this->assertTrue($resp);
        $this->assertNull(DeliveryPaymentHistorique::find($deliveryPaymentHistorique->id), 'DeliveryPaymentHistorique should not exist in DB');
    }
}
