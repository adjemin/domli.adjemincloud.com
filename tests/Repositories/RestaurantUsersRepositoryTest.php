<?php namespace Tests\Repositories;

use App\Models\RestaurantUsers;
use App\Repositories\RestaurantUsersRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RestaurantUsersRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RestaurantUsersRepository
     */
    protected $restaurantUsersRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->restaurantUsersRepo = \App::make(RestaurantUsersRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_restaurant_users()
    {
        $restaurantUsers = factory(RestaurantUsers::class)->make()->toArray();

        $createdRestaurantUsers = $this->restaurantUsersRepo->create($restaurantUsers);

        $createdRestaurantUsers = $createdRestaurantUsers->toArray();
        $this->assertArrayHasKey('id', $createdRestaurantUsers);
        $this->assertNotNull($createdRestaurantUsers['id'], 'Created RestaurantUsers must have id specified');
        $this->assertNotNull(RestaurantUsers::find($createdRestaurantUsers['id']), 'RestaurantUsers with given id must be in DB');
        $this->assertModelData($restaurantUsers, $createdRestaurantUsers);
    }

    /**
     * @test read
     */
    public function test_read_restaurant_users()
    {
        $restaurantUsers = factory(RestaurantUsers::class)->create();

        $dbRestaurantUsers = $this->restaurantUsersRepo->find($restaurantUsers->id);

        $dbRestaurantUsers = $dbRestaurantUsers->toArray();
        $this->assertModelData($restaurantUsers->toArray(), $dbRestaurantUsers);
    }

    /**
     * @test update
     */
    public function test_update_restaurant_users()
    {
        $restaurantUsers = factory(RestaurantUsers::class)->create();
        $fakeRestaurantUsers = factory(RestaurantUsers::class)->make()->toArray();

        $updatedRestaurantUsers = $this->restaurantUsersRepo->update($fakeRestaurantUsers, $restaurantUsers->id);

        $this->assertModelData($fakeRestaurantUsers, $updatedRestaurantUsers->toArray());
        $dbRestaurantUsers = $this->restaurantUsersRepo->find($restaurantUsers->id);
        $this->assertModelData($fakeRestaurantUsers, $dbRestaurantUsers->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_restaurant_users()
    {
        $restaurantUsers = factory(RestaurantUsers::class)->create();

        $resp = $this->restaurantUsersRepo->delete($restaurantUsers->id);

        $this->assertTrue($resp);
        $this->assertNull(RestaurantUsers::find($restaurantUsers->id), 'RestaurantUsers should not exist in DB');
    }
}
