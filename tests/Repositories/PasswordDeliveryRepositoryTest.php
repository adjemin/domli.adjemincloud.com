<?php namespace Tests\Repositories;

use App\Models\PasswordDelivery;
use App\Repositories\PasswordDeliveryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PasswordDeliveryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PasswordDeliveryRepository
     */
    protected $passwordDeliveryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->passwordDeliveryRepo = \App::make(PasswordDeliveryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_password_delivery()
    {
        $passwordDelivery = factory(PasswordDelivery::class)->make()->toArray();

        $createdPasswordDelivery = $this->passwordDeliveryRepo->create($passwordDelivery);

        $createdPasswordDelivery = $createdPasswordDelivery->toArray();
        $this->assertArrayHasKey('id', $createdPasswordDelivery);
        $this->assertNotNull($createdPasswordDelivery['id'], 'Created PasswordDelivery must have id specified');
        $this->assertNotNull(PasswordDelivery::find($createdPasswordDelivery['id']), 'PasswordDelivery with given id must be in DB');
        $this->assertModelData($passwordDelivery, $createdPasswordDelivery);
    }

    /**
     * @test read
     */
    public function test_read_password_delivery()
    {
        $passwordDelivery = factory(PasswordDelivery::class)->create();

        $dbPasswordDelivery = $this->passwordDeliveryRepo->find($passwordDelivery->id);

        $dbPasswordDelivery = $dbPasswordDelivery->toArray();
        $this->assertModelData($passwordDelivery->toArray(), $dbPasswordDelivery);
    }

    /**
     * @test update
     */
    public function test_update_password_delivery()
    {
        $passwordDelivery = factory(PasswordDelivery::class)->create();
        $fakePasswordDelivery = factory(PasswordDelivery::class)->make()->toArray();

        $updatedPasswordDelivery = $this->passwordDeliveryRepo->update($fakePasswordDelivery, $passwordDelivery->id);

        $this->assertModelData($fakePasswordDelivery, $updatedPasswordDelivery->toArray());
        $dbPasswordDelivery = $this->passwordDeliveryRepo->find($passwordDelivery->id);
        $this->assertModelData($fakePasswordDelivery, $dbPasswordDelivery->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_password_delivery()
    {
        $passwordDelivery = factory(PasswordDelivery::class)->create();

        $resp = $this->passwordDeliveryRepo->delete($passwordDelivery->id);

        $this->assertTrue($resp);
        $this->assertNull(PasswordDelivery::find($passwordDelivery->id), 'PasswordDelivery should not exist in DB');
    }
}
