<?php namespace Tests\Repositories;

use App\Models\OrderItems;
use App\Repositories\OrderItemsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OrderItemsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrderItemsRepository
     */
    protected $orderItemsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->orderItemsRepo = \App::make(OrderItemsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_order_items()
    {
        $orderItems = factory(OrderItems::class)->make()->toArray();

        $createdOrderItems = $this->orderItemsRepo->create($orderItems);

        $createdOrderItems = $createdOrderItems->toArray();
        $this->assertArrayHasKey('id', $createdOrderItems);
        $this->assertNotNull($createdOrderItems['id'], 'Created OrderItems must have id specified');
        $this->assertNotNull(OrderItems::find($createdOrderItems['id']), 'OrderItems with given id must be in DB');
        $this->assertModelData($orderItems, $createdOrderItems);
    }

    /**
     * @test read
     */
    public function test_read_order_items()
    {
        $orderItems = factory(OrderItems::class)->create();

        $dbOrderItems = $this->orderItemsRepo->find($orderItems->id);

        $dbOrderItems = $dbOrderItems->toArray();
        $this->assertModelData($orderItems->toArray(), $dbOrderItems);
    }

    /**
     * @test update
     */
    public function test_update_order_items()
    {
        $orderItems = factory(OrderItems::class)->create();
        $fakeOrderItems = factory(OrderItems::class)->make()->toArray();

        $updatedOrderItems = $this->orderItemsRepo->update($fakeOrderItems, $orderItems->id);

        $this->assertModelData($fakeOrderItems, $updatedOrderItems->toArray());
        $dbOrderItems = $this->orderItemsRepo->find($orderItems->id);
        $this->assertModelData($fakeOrderItems, $dbOrderItems->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_order_items()
    {
        $orderItems = factory(OrderItems::class)->create();

        $resp = $this->orderItemsRepo->delete($orderItems->id);

        $this->assertTrue($resp);
        $this->assertNull(OrderItems::find($orderItems->id), 'OrderItems should not exist in DB');
    }
}
