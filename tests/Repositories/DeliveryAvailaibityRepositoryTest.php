<?php namespace Tests\Repositories;

use App\Models\DeliveryAvailaibity;
use App\Repositories\DeliveryAvailaibityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DeliveryAvailaibityRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DeliveryAvailaibityRepository
     */
    protected $deliveryAvailaibityRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->deliveryAvailaibityRepo = \App::make(DeliveryAvailaibityRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_delivery_availaibity()
    {
        $deliveryAvailaibity = factory(DeliveryAvailaibity::class)->make()->toArray();

        $createdDeliveryAvailaibity = $this->deliveryAvailaibityRepo->create($deliveryAvailaibity);

        $createdDeliveryAvailaibity = $createdDeliveryAvailaibity->toArray();
        $this->assertArrayHasKey('id', $createdDeliveryAvailaibity);
        $this->assertNotNull($createdDeliveryAvailaibity['id'], 'Created DeliveryAvailaibity must have id specified');
        $this->assertNotNull(DeliveryAvailaibity::find($createdDeliveryAvailaibity['id']), 'DeliveryAvailaibity with given id must be in DB');
        $this->assertModelData($deliveryAvailaibity, $createdDeliveryAvailaibity);
    }

    /**
     * @test read
     */
    public function test_read_delivery_availaibity()
    {
        $deliveryAvailaibity = factory(DeliveryAvailaibity::class)->create();

        $dbDeliveryAvailaibity = $this->deliveryAvailaibityRepo->find($deliveryAvailaibity->id);

        $dbDeliveryAvailaibity = $dbDeliveryAvailaibity->toArray();
        $this->assertModelData($deliveryAvailaibity->toArray(), $dbDeliveryAvailaibity);
    }

    /**
     * @test update
     */
    public function test_update_delivery_availaibity()
    {
        $deliveryAvailaibity = factory(DeliveryAvailaibity::class)->create();
        $fakeDeliveryAvailaibity = factory(DeliveryAvailaibity::class)->make()->toArray();

        $updatedDeliveryAvailaibity = $this->deliveryAvailaibityRepo->update($fakeDeliveryAvailaibity, $deliveryAvailaibity->id);

        $this->assertModelData($fakeDeliveryAvailaibity, $updatedDeliveryAvailaibity->toArray());
        $dbDeliveryAvailaibity = $this->deliveryAvailaibityRepo->find($deliveryAvailaibity->id);
        $this->assertModelData($fakeDeliveryAvailaibity, $dbDeliveryAvailaibity->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_delivery_availaibity()
    {
        $deliveryAvailaibity = factory(DeliveryAvailaibity::class)->create();

        $resp = $this->deliveryAvailaibityRepo->delete($deliveryAvailaibity->id);

        $this->assertTrue($resp);
        $this->assertNull(DeliveryAvailaibity::find($deliveryAvailaibity->id), 'DeliveryAvailaibity should not exist in DB');
    }
}
