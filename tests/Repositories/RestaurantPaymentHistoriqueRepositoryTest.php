<?php namespace Tests\Repositories;

use App\Models\RestaurantPaymentHistorique;
use App\Repositories\RestaurantPaymentHistoriqueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RestaurantPaymentHistoriqueRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RestaurantPaymentHistoriqueRepository
     */
    protected $restaurantPaymentHistoriqueRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->restaurantPaymentHistoriqueRepo = \App::make(RestaurantPaymentHistoriqueRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_restaurant_payment_historique()
    {
        $restaurantPaymentHistorique = factory(RestaurantPaymentHistorique::class)->make()->toArray();

        $createdRestaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepo->create($restaurantPaymentHistorique);

        $createdRestaurantPaymentHistorique = $createdRestaurantPaymentHistorique->toArray();
        $this->assertArrayHasKey('id', $createdRestaurantPaymentHistorique);
        $this->assertNotNull($createdRestaurantPaymentHistorique['id'], 'Created RestaurantPaymentHistorique must have id specified');
        $this->assertNotNull(RestaurantPaymentHistorique::find($createdRestaurantPaymentHistorique['id']), 'RestaurantPaymentHistorique with given id must be in DB');
        $this->assertModelData($restaurantPaymentHistorique, $createdRestaurantPaymentHistorique);
    }

    /**
     * @test read
     */
    public function test_read_restaurant_payment_historique()
    {
        $restaurantPaymentHistorique = factory(RestaurantPaymentHistorique::class)->create();

        $dbRestaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepo->find($restaurantPaymentHistorique->id);

        $dbRestaurantPaymentHistorique = $dbRestaurantPaymentHistorique->toArray();
        $this->assertModelData($restaurantPaymentHistorique->toArray(), $dbRestaurantPaymentHistorique);
    }

    /**
     * @test update
     */
    public function test_update_restaurant_payment_historique()
    {
        $restaurantPaymentHistorique = factory(RestaurantPaymentHistorique::class)->create();
        $fakeRestaurantPaymentHistorique = factory(RestaurantPaymentHistorique::class)->make()->toArray();

        $updatedRestaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepo->update($fakeRestaurantPaymentHistorique, $restaurantPaymentHistorique->id);

        $this->assertModelData($fakeRestaurantPaymentHistorique, $updatedRestaurantPaymentHistorique->toArray());
        $dbRestaurantPaymentHistorique = $this->restaurantPaymentHistoriqueRepo->find($restaurantPaymentHistorique->id);
        $this->assertModelData($fakeRestaurantPaymentHistorique, $dbRestaurantPaymentHistorique->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_restaurant_payment_historique()
    {
        $restaurantPaymentHistorique = factory(RestaurantPaymentHistorique::class)->create();

        $resp = $this->restaurantPaymentHistoriqueRepo->delete($restaurantPaymentHistorique->id);

        $this->assertTrue($resp);
        $this->assertNull(RestaurantPaymentHistorique::find($restaurantPaymentHistorique->id), 'RestaurantPaymentHistorique should not exist in DB');
    }
}
