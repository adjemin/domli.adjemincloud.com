<?php namespace Tests\Repositories;

use App\Models\Orders;
use App\Repositories\OrdersRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OrdersRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrdersRepository
     */
    protected $ordersRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->ordersRepo = \App::make(OrdersRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_orders()
    {
        $orders = factory(Orders::class)->make()->toArray();

        $createdOrders = $this->ordersRepo->create($orders);

        $createdOrders = $createdOrders->toArray();
        $this->assertArrayHasKey('id', $createdOrders);
        $this->assertNotNull($createdOrders['id'], 'Created Orders must have id specified');
        $this->assertNotNull(Orders::find($createdOrders['id']), 'Orders with given id must be in DB');
        $this->assertModelData($orders, $createdOrders);
    }

    /**
     * @test read
     */
    public function test_read_orders()
    {
        $orders = factory(Orders::class)->create();

        $dbOrders = $this->ordersRepo->find($orders->id);

        $dbOrders = $dbOrders->toArray();
        $this->assertModelData($orders->toArray(), $dbOrders);
    }

    /**
     * @test update
     */
    public function test_update_orders()
    {
        $orders = factory(Orders::class)->create();
        $fakeOrders = factory(Orders::class)->make()->toArray();

        $updatedOrders = $this->ordersRepo->update($fakeOrders, $orders->id);

        $this->assertModelData($fakeOrders, $updatedOrders->toArray());
        $dbOrders = $this->ordersRepo->find($orders->id);
        $this->assertModelData($fakeOrders, $dbOrders->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_orders()
    {
        $orders = factory(Orders::class)->create();

        $resp = $this->ordersRepo->delete($orders->id);

        $this->assertTrue($resp);
        $this->assertNull(Orders::find($orders->id), 'Orders should not exist in DB');
    }
}
