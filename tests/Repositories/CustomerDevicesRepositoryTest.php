<?php namespace Tests\Repositories;

use App\Models\CustomerDevices;
use App\Repositories\CustomerDevicesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CustomerDevicesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CustomerDevicesRepository
     */
    protected $customerDevicesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->customerDevicesRepo = \App::make(CustomerDevicesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_customer_devices()
    {
        $customerDevices = factory(CustomerDevices::class)->make()->toArray();

        $createdCustomerDevices = $this->customerDevicesRepo->create($customerDevices);

        $createdCustomerDevices = $createdCustomerDevices->toArray();
        $this->assertArrayHasKey('id', $createdCustomerDevices);
        $this->assertNotNull($createdCustomerDevices['id'], 'Created CustomerDevices must have id specified');
        $this->assertNotNull(CustomerDevices::find($createdCustomerDevices['id']), 'CustomerDevices with given id must be in DB');
        $this->assertModelData($customerDevices, $createdCustomerDevices);
    }

    /**
     * @test read
     */
    public function test_read_customer_devices()
    {
        $customerDevices = factory(CustomerDevices::class)->create();

        $dbCustomerDevices = $this->customerDevicesRepo->find($customerDevices->id);

        $dbCustomerDevices = $dbCustomerDevices->toArray();
        $this->assertModelData($customerDevices->toArray(), $dbCustomerDevices);
    }

    /**
     * @test update
     */
    public function test_update_customer_devices()
    {
        $customerDevices = factory(CustomerDevices::class)->create();
        $fakeCustomerDevices = factory(CustomerDevices::class)->make()->toArray();

        $updatedCustomerDevices = $this->customerDevicesRepo->update($fakeCustomerDevices, $customerDevices->id);

        $this->assertModelData($fakeCustomerDevices, $updatedCustomerDevices->toArray());
        $dbCustomerDevices = $this->customerDevicesRepo->find($customerDevices->id);
        $this->assertModelData($fakeCustomerDevices, $dbCustomerDevices->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_customer_devices()
    {
        $customerDevices = factory(CustomerDevices::class)->create();

        $resp = $this->customerDevicesRepo->delete($customerDevices->id);

        $this->assertTrue($resp);
        $this->assertNull(CustomerDevices::find($customerDevices->id), 'CustomerDevices should not exist in DB');
    }
}
