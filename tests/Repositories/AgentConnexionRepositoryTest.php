<?php namespace Tests\Repositories;

use App\Models\AgentConnexion;
use App\Repositories\AgentConnexionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AgentConnexionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AgentConnexionRepository
     */
    protected $agentConnexionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->agentConnexionRepo = \App::make(AgentConnexionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_agent_connexion()
    {
        $agentConnexion = factory(AgentConnexion::class)->make()->toArray();

        $createdAgentConnexion = $this->agentConnexionRepo->create($agentConnexion);

        $createdAgentConnexion = $createdAgentConnexion->toArray();
        $this->assertArrayHasKey('id', $createdAgentConnexion);
        $this->assertNotNull($createdAgentConnexion['id'], 'Created AgentConnexion must have id specified');
        $this->assertNotNull(AgentConnexion::find($createdAgentConnexion['id']), 'AgentConnexion with given id must be in DB');
        $this->assertModelData($agentConnexion, $createdAgentConnexion);
    }

    /**
     * @test read
     */
    public function test_read_agent_connexion()
    {
        $agentConnexion = factory(AgentConnexion::class)->create();

        $dbAgentConnexion = $this->agentConnexionRepo->find($agentConnexion->id);

        $dbAgentConnexion = $dbAgentConnexion->toArray();
        $this->assertModelData($agentConnexion->toArray(), $dbAgentConnexion);
    }

    /**
     * @test update
     */
    public function test_update_agent_connexion()
    {
        $agentConnexion = factory(AgentConnexion::class)->create();
        $fakeAgentConnexion = factory(AgentConnexion::class)->make()->toArray();

        $updatedAgentConnexion = $this->agentConnexionRepo->update($fakeAgentConnexion, $agentConnexion->id);

        $this->assertModelData($fakeAgentConnexion, $updatedAgentConnexion->toArray());
        $dbAgentConnexion = $this->agentConnexionRepo->find($agentConnexion->id);
        $this->assertModelData($fakeAgentConnexion, $dbAgentConnexion->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_agent_connexion()
    {
        $agentConnexion = factory(AgentConnexion::class)->create();

        $resp = $this->agentConnexionRepo->delete($agentConnexion->id);

        $this->assertTrue($resp);
        $this->assertNull(AgentConnexion::find($agentConnexion->id), 'AgentConnexion should not exist in DB');
    }
}
