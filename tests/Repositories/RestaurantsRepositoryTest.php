<?php namespace Tests\Repositories;

use App\Models\Restaurants;
use App\Repositories\RestaurantsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RestaurantsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RestaurantsRepository
     */
    protected $restaurantsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->restaurantsRepo = \App::make(RestaurantsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_restaurants()
    {
        $restaurants = factory(Restaurants::class)->make()->toArray();

        $createdRestaurants = $this->restaurantsRepo->create($restaurants);

        $createdRestaurants = $createdRestaurants->toArray();
        $this->assertArrayHasKey('id', $createdRestaurants);
        $this->assertNotNull($createdRestaurants['id'], 'Created Restaurants must have id specified');
        $this->assertNotNull(Restaurants::find($createdRestaurants['id']), 'Restaurants with given id must be in DB');
        $this->assertModelData($restaurants, $createdRestaurants);
    }

    /**
     * @test read
     */
    public function test_read_restaurants()
    {
        $restaurants = factory(Restaurants::class)->create();

        $dbRestaurants = $this->restaurantsRepo->find($restaurants->id);

        $dbRestaurants = $dbRestaurants->toArray();
        $this->assertModelData($restaurants->toArray(), $dbRestaurants);
    }

    /**
     * @test update
     */
    public function test_update_restaurants()
    {
        $restaurants = factory(Restaurants::class)->create();
        $fakeRestaurants = factory(Restaurants::class)->make()->toArray();

        $updatedRestaurants = $this->restaurantsRepo->update($fakeRestaurants, $restaurants->id);

        $this->assertModelData($fakeRestaurants, $updatedRestaurants->toArray());
        $dbRestaurants = $this->restaurantsRepo->find($restaurants->id);
        $this->assertModelData($fakeRestaurants, $dbRestaurants->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_restaurants()
    {
        $restaurants = factory(Restaurants::class)->create();

        $resp = $this->restaurantsRepo->delete($restaurants->id);

        $this->assertTrue($resp);
        $this->assertNull(Restaurants::find($restaurants->id), 'Restaurants should not exist in DB');
    }
}
