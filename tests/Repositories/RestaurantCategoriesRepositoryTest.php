<?php namespace Tests\Repositories;

use App\Models\RestaurantCategories;
use App\Repositories\RestaurantCategoriesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RestaurantCategoriesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RestaurantCategoriesRepository
     */
    protected $restaurantCategoriesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->restaurantCategoriesRepo = \App::make(RestaurantCategoriesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_restaurant_categories()
    {
        $restaurantCategories = factory(RestaurantCategories::class)->make()->toArray();

        $createdRestaurantCategories = $this->restaurantCategoriesRepo->create($restaurantCategories);

        $createdRestaurantCategories = $createdRestaurantCategories->toArray();
        $this->assertArrayHasKey('id', $createdRestaurantCategories);
        $this->assertNotNull($createdRestaurantCategories['id'], 'Created RestaurantCategories must have id specified');
        $this->assertNotNull(RestaurantCategories::find($createdRestaurantCategories['id']), 'RestaurantCategories with given id must be in DB');
        $this->assertModelData($restaurantCategories, $createdRestaurantCategories);
    }

    /**
     * @test read
     */
    public function test_read_restaurant_categories()
    {
        $restaurantCategories = factory(RestaurantCategories::class)->create();

        $dbRestaurantCategories = $this->restaurantCategoriesRepo->find($restaurantCategories->id);

        $dbRestaurantCategories = $dbRestaurantCategories->toArray();
        $this->assertModelData($restaurantCategories->toArray(), $dbRestaurantCategories);
    }

    /**
     * @test update
     */
    public function test_update_restaurant_categories()
    {
        $restaurantCategories = factory(RestaurantCategories::class)->create();
        $fakeRestaurantCategories = factory(RestaurantCategories::class)->make()->toArray();

        $updatedRestaurantCategories = $this->restaurantCategoriesRepo->update($fakeRestaurantCategories, $restaurantCategories->id);

        $this->assertModelData($fakeRestaurantCategories, $updatedRestaurantCategories->toArray());
        $dbRestaurantCategories = $this->restaurantCategoriesRepo->find($restaurantCategories->id);
        $this->assertModelData($fakeRestaurantCategories, $dbRestaurantCategories->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_restaurant_categories()
    {
        $restaurantCategories = factory(RestaurantCategories::class)->create();

        $resp = $this->restaurantCategoriesRepo->delete($restaurantCategories->id);

        $this->assertTrue($resp);
        $this->assertNull(RestaurantCategories::find($restaurantCategories->id), 'RestaurantCategories should not exist in DB');
    }
}
