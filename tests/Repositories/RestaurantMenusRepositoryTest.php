<?php namespace Tests\Repositories;

use App\Models\RestaurantMenus;
use App\Repositories\RestaurantMenusRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RestaurantMenusRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RestaurantMenusRepository
     */
    protected $restaurantMenusRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->restaurantMenusRepo = \App::make(RestaurantMenusRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_restaurant_menus()
    {
        $restaurantMenus = factory(RestaurantMenus::class)->make()->toArray();

        $createdRestaurantMenus = $this->restaurantMenusRepo->create($restaurantMenus);

        $createdRestaurantMenus = $createdRestaurantMenus->toArray();
        $this->assertArrayHasKey('id', $createdRestaurantMenus);
        $this->assertNotNull($createdRestaurantMenus['id'], 'Created RestaurantMenus must have id specified');
        $this->assertNotNull(RestaurantMenus::find($createdRestaurantMenus['id']), 'RestaurantMenus with given id must be in DB');
        $this->assertModelData($restaurantMenus, $createdRestaurantMenus);
    }

    /**
     * @test read
     */
    public function test_read_restaurant_menus()
    {
        $restaurantMenus = factory(RestaurantMenus::class)->create();

        $dbRestaurantMenus = $this->restaurantMenusRepo->find($restaurantMenus->id);

        $dbRestaurantMenus = $dbRestaurantMenus->toArray();
        $this->assertModelData($restaurantMenus->toArray(), $dbRestaurantMenus);
    }

    /**
     * @test update
     */
    public function test_update_restaurant_menus()
    {
        $restaurantMenus = factory(RestaurantMenus::class)->create();
        $fakeRestaurantMenus = factory(RestaurantMenus::class)->make()->toArray();

        $updatedRestaurantMenus = $this->restaurantMenusRepo->update($fakeRestaurantMenus, $restaurantMenus->id);

        $this->assertModelData($fakeRestaurantMenus, $updatedRestaurantMenus->toArray());
        $dbRestaurantMenus = $this->restaurantMenusRepo->find($restaurantMenus->id);
        $this->assertModelData($fakeRestaurantMenus, $dbRestaurantMenus->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_restaurant_menus()
    {
        $restaurantMenus = factory(RestaurantMenus::class)->create();

        $resp = $this->restaurantMenusRepo->delete($restaurantMenus->id);

        $this->assertTrue($resp);
        $this->assertNull(RestaurantMenus::find($restaurantMenus->id), 'RestaurantMenus should not exist in DB');
    }
}
