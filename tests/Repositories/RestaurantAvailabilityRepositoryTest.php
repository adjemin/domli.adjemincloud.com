<?php namespace Tests\Repositories;

use App\Models\RestaurantAvailability;
use App\Repositories\RestaurantAvailabilityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RestaurantAvailabilityRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RestaurantAvailabilityRepository
     */
    protected $restaurantAvailabilityRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->restaurantAvailabilityRepo = \App::make(RestaurantAvailabilityRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_restaurant_availability()
    {
        $restaurantAvailability = factory(RestaurantAvailability::class)->make()->toArray();

        $createdRestaurantAvailability = $this->restaurantAvailabilityRepo->create($restaurantAvailability);

        $createdRestaurantAvailability = $createdRestaurantAvailability->toArray();
        $this->assertArrayHasKey('id', $createdRestaurantAvailability);
        $this->assertNotNull($createdRestaurantAvailability['id'], 'Created RestaurantAvailability must have id specified');
        $this->assertNotNull(RestaurantAvailability::find($createdRestaurantAvailability['id']), 'RestaurantAvailability with given id must be in DB');
        $this->assertModelData($restaurantAvailability, $createdRestaurantAvailability);
    }

    /**
     * @test read
     */
    public function test_read_restaurant_availability()
    {
        $restaurantAvailability = factory(RestaurantAvailability::class)->create();

        $dbRestaurantAvailability = $this->restaurantAvailabilityRepo->find($restaurantAvailability->id);

        $dbRestaurantAvailability = $dbRestaurantAvailability->toArray();
        $this->assertModelData($restaurantAvailability->toArray(), $dbRestaurantAvailability);
    }

    /**
     * @test update
     */
    public function test_update_restaurant_availability()
    {
        $restaurantAvailability = factory(RestaurantAvailability::class)->create();
        $fakeRestaurantAvailability = factory(RestaurantAvailability::class)->make()->toArray();

        $updatedRestaurantAvailability = $this->restaurantAvailabilityRepo->update($fakeRestaurantAvailability, $restaurantAvailability->id);

        $this->assertModelData($fakeRestaurantAvailability, $updatedRestaurantAvailability->toArray());
        $dbRestaurantAvailability = $this->restaurantAvailabilityRepo->find($restaurantAvailability->id);
        $this->assertModelData($fakeRestaurantAvailability, $dbRestaurantAvailability->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_restaurant_availability()
    {
        $restaurantAvailability = factory(RestaurantAvailability::class)->create();

        $resp = $this->restaurantAvailabilityRepo->delete($restaurantAvailability->id);

        $this->assertTrue($resp);
        $this->assertNull(RestaurantAvailability::find($restaurantAvailability->id), 'RestaurantAvailability should not exist in DB');
    }
}
