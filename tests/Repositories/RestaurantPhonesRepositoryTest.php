<?php namespace Tests\Repositories;

use App\Models\RestaurantPhones;
use App\Repositories\RestaurantPhonesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RestaurantPhonesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RestaurantPhonesRepository
     */
    protected $restaurantPhonesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->restaurantPhonesRepo = \App::make(RestaurantPhonesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_restaurant_phones()
    {
        $restaurantPhones = factory(RestaurantPhones::class)->make()->toArray();

        $createdRestaurantPhones = $this->restaurantPhonesRepo->create($restaurantPhones);

        $createdRestaurantPhones = $createdRestaurantPhones->toArray();
        $this->assertArrayHasKey('id', $createdRestaurantPhones);
        $this->assertNotNull($createdRestaurantPhones['id'], 'Created RestaurantPhones must have id specified');
        $this->assertNotNull(RestaurantPhones::find($createdRestaurantPhones['id']), 'RestaurantPhones with given id must be in DB');
        $this->assertModelData($restaurantPhones, $createdRestaurantPhones);
    }

    /**
     * @test read
     */
    public function test_read_restaurant_phones()
    {
        $restaurantPhones = factory(RestaurantPhones::class)->create();

        $dbRestaurantPhones = $this->restaurantPhonesRepo->find($restaurantPhones->id);

        $dbRestaurantPhones = $dbRestaurantPhones->toArray();
        $this->assertModelData($restaurantPhones->toArray(), $dbRestaurantPhones);
    }

    /**
     * @test update
     */
    public function test_update_restaurant_phones()
    {
        $restaurantPhones = factory(RestaurantPhones::class)->create();
        $fakeRestaurantPhones = factory(RestaurantPhones::class)->make()->toArray();

        $updatedRestaurantPhones = $this->restaurantPhonesRepo->update($fakeRestaurantPhones, $restaurantPhones->id);

        $this->assertModelData($fakeRestaurantPhones, $updatedRestaurantPhones->toArray());
        $dbRestaurantPhones = $this->restaurantPhonesRepo->find($restaurantPhones->id);
        $this->assertModelData($fakeRestaurantPhones, $dbRestaurantPhones->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_restaurant_phones()
    {
        $restaurantPhones = factory(RestaurantPhones::class)->create();

        $resp = $this->restaurantPhonesRepo->delete($restaurantPhones->id);

        $this->assertTrue($resp);
        $this->assertNull(RestaurantPhones::find($restaurantPhones->id), 'RestaurantPhones should not exist in DB');
    }
}
