<?php namespace Tests\Repositories;

use App\Models\OrderDeliveries;
use App\Repositories\OrderDeliveriesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OrderDeliveriesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrderDeliveriesRepository
     */
    protected $orderDeliveriesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->orderDeliveriesRepo = \App::make(OrderDeliveriesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_order_deliveries()
    {
        $orderDeliveries = factory(OrderDeliveries::class)->make()->toArray();

        $createdOrderDeliveries = $this->orderDeliveriesRepo->create($orderDeliveries);

        $createdOrderDeliveries = $createdOrderDeliveries->toArray();
        $this->assertArrayHasKey('id', $createdOrderDeliveries);
        $this->assertNotNull($createdOrderDeliveries['id'], 'Created OrderDeliveries must have id specified');
        $this->assertNotNull(OrderDeliveries::find($createdOrderDeliveries['id']), 'OrderDeliveries with given id must be in DB');
        $this->assertModelData($orderDeliveries, $createdOrderDeliveries);
    }

    /**
     * @test read
     */
    public function test_read_order_deliveries()
    {
        $orderDeliveries = factory(OrderDeliveries::class)->create();

        $dbOrderDeliveries = $this->orderDeliveriesRepo->find($orderDeliveries->id);

        $dbOrderDeliveries = $dbOrderDeliveries->toArray();
        $this->assertModelData($orderDeliveries->toArray(), $dbOrderDeliveries);
    }

    /**
     * @test update
     */
    public function test_update_order_deliveries()
    {
        $orderDeliveries = factory(OrderDeliveries::class)->create();
        $fakeOrderDeliveries = factory(OrderDeliveries::class)->make()->toArray();

        $updatedOrderDeliveries = $this->orderDeliveriesRepo->update($fakeOrderDeliveries, $orderDeliveries->id);

        $this->assertModelData($fakeOrderDeliveries, $updatedOrderDeliveries->toArray());
        $dbOrderDeliveries = $this->orderDeliveriesRepo->find($orderDeliveries->id);
        $this->assertModelData($fakeOrderDeliveries, $dbOrderDeliveries->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_order_deliveries()
    {
        $orderDeliveries = factory(OrderDeliveries::class)->create();

        $resp = $this->orderDeliveriesRepo->delete($orderDeliveries->id);

        $this->assertTrue($resp);
        $this->assertNull(OrderDeliveries::find($orderDeliveries->id), 'OrderDeliveries should not exist in DB');
    }
}
