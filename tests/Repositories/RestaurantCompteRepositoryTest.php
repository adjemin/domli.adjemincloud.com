<?php namespace Tests\Repositories;

use App\Models\RestaurantCompte;
use App\Repositories\RestaurantCompteRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RestaurantCompteRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RestaurantCompteRepository
     */
    protected $restaurantCompteRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->restaurantCompteRepo = \App::make(RestaurantCompteRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_restaurant_compte()
    {
        $restaurantCompte = factory(RestaurantCompte::class)->make()->toArray();

        $createdRestaurantCompte = $this->restaurantCompteRepo->create($restaurantCompte);

        $createdRestaurantCompte = $createdRestaurantCompte->toArray();
        $this->assertArrayHasKey('id', $createdRestaurantCompte);
        $this->assertNotNull($createdRestaurantCompte['id'], 'Created RestaurantCompte must have id specified');
        $this->assertNotNull(RestaurantCompte::find($createdRestaurantCompte['id']), 'RestaurantCompte with given id must be in DB');
        $this->assertModelData($restaurantCompte, $createdRestaurantCompte);
    }

    /**
     * @test read
     */
    public function test_read_restaurant_compte()
    {
        $restaurantCompte = factory(RestaurantCompte::class)->create();

        $dbRestaurantCompte = $this->restaurantCompteRepo->find($restaurantCompte->id);

        $dbRestaurantCompte = $dbRestaurantCompte->toArray();
        $this->assertModelData($restaurantCompte->toArray(), $dbRestaurantCompte);
    }

    /**
     * @test update
     */
    public function test_update_restaurant_compte()
    {
        $restaurantCompte = factory(RestaurantCompte::class)->create();
        $fakeRestaurantCompte = factory(RestaurantCompte::class)->make()->toArray();

        $updatedRestaurantCompte = $this->restaurantCompteRepo->update($fakeRestaurantCompte, $restaurantCompte->id);

        $this->assertModelData($fakeRestaurantCompte, $updatedRestaurantCompte->toArray());
        $dbRestaurantCompte = $this->restaurantCompteRepo->find($restaurantCompte->id);
        $this->assertModelData($fakeRestaurantCompte, $dbRestaurantCompte->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_restaurant_compte()
    {
        $restaurantCompte = factory(RestaurantCompte::class)->create();

        $resp = $this->restaurantCompteRepo->delete($restaurantCompte->id);

        $this->assertTrue($resp);
        $this->assertNull(RestaurantCompte::find($restaurantCompte->id), 'RestaurantCompte should not exist in DB');
    }
}
