<?php namespace Tests\Repositories;

use App\Models\RequestDeliveryPayment;
use App\Repositories\RequestDeliveryPaymentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RequestDeliveryPaymentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RequestDeliveryPaymentRepository
     */
    protected $requestDeliveryPaymentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->requestDeliveryPaymentRepo = \App::make(RequestDeliveryPaymentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_request_delivery_payment()
    {
        $requestDeliveryPayment = factory(RequestDeliveryPayment::class)->make()->toArray();

        $createdRequestDeliveryPayment = $this->requestDeliveryPaymentRepo->create($requestDeliveryPayment);

        $createdRequestDeliveryPayment = $createdRequestDeliveryPayment->toArray();
        $this->assertArrayHasKey('id', $createdRequestDeliveryPayment);
        $this->assertNotNull($createdRequestDeliveryPayment['id'], 'Created RequestDeliveryPayment must have id specified');
        $this->assertNotNull(RequestDeliveryPayment::find($createdRequestDeliveryPayment['id']), 'RequestDeliveryPayment with given id must be in DB');
        $this->assertModelData($requestDeliveryPayment, $createdRequestDeliveryPayment);
    }

    /**
     * @test read
     */
    public function test_read_request_delivery_payment()
    {
        $requestDeliveryPayment = factory(RequestDeliveryPayment::class)->create();

        $dbRequestDeliveryPayment = $this->requestDeliveryPaymentRepo->find($requestDeliveryPayment->id);

        $dbRequestDeliveryPayment = $dbRequestDeliveryPayment->toArray();
        $this->assertModelData($requestDeliveryPayment->toArray(), $dbRequestDeliveryPayment);
    }

    /**
     * @test update
     */
    public function test_update_request_delivery_payment()
    {
        $requestDeliveryPayment = factory(RequestDeliveryPayment::class)->create();
        $fakeRequestDeliveryPayment = factory(RequestDeliveryPayment::class)->make()->toArray();

        $updatedRequestDeliveryPayment = $this->requestDeliveryPaymentRepo->update($fakeRequestDeliveryPayment, $requestDeliveryPayment->id);

        $this->assertModelData($fakeRequestDeliveryPayment, $updatedRequestDeliveryPayment->toArray());
        $dbRequestDeliveryPayment = $this->requestDeliveryPaymentRepo->find($requestDeliveryPayment->id);
        $this->assertModelData($fakeRequestDeliveryPayment, $dbRequestDeliveryPayment->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_request_delivery_payment()
    {
        $requestDeliveryPayment = factory(RequestDeliveryPayment::class)->create();

        $resp = $this->requestDeliveryPaymentRepo->delete($requestDeliveryPayment->id);

        $this->assertTrue($resp);
        $this->assertNull(RequestDeliveryPayment::find($requestDeliveryPayment->id), 'RequestDeliveryPayment should not exist in DB');
    }
}
