<?php namespace Tests\Repositories;

use App\Models\Otps;
use App\Repositories\OtpsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OtpsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OtpsRepository
     */
    protected $otpsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->otpsRepo = \App::make(OtpsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_otps()
    {
        $otps = factory(Otps::class)->make()->toArray();

        $createdOtps = $this->otpsRepo->create($otps);

        $createdOtps = $createdOtps->toArray();
        $this->assertArrayHasKey('id', $createdOtps);
        $this->assertNotNull($createdOtps['id'], 'Created Otps must have id specified');
        $this->assertNotNull(Otps::find($createdOtps['id']), 'Otps with given id must be in DB');
        $this->assertModelData($otps, $createdOtps);
    }

    /**
     * @test read
     */
    public function test_read_otps()
    {
        $otps = factory(Otps::class)->create();

        $dbOtps = $this->otpsRepo->find($otps->id);

        $dbOtps = $dbOtps->toArray();
        $this->assertModelData($otps->toArray(), $dbOtps);
    }

    /**
     * @test update
     */
    public function test_update_otps()
    {
        $otps = factory(Otps::class)->create();
        $fakeOtps = factory(Otps::class)->make()->toArray();

        $updatedOtps = $this->otpsRepo->update($fakeOtps, $otps->id);

        $this->assertModelData($fakeOtps, $updatedOtps->toArray());
        $dbOtps = $this->otpsRepo->find($otps->id);
        $this->assertModelData($fakeOtps, $dbOtps->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_otps()
    {
        $otps = factory(Otps::class)->create();

        $resp = $this->otpsRepo->delete($otps->id);

        $this->assertTrue($resp);
        $this->assertNull(Otps::find($otps->id), 'Otps should not exist in DB');
    }
}
