<?php namespace Tests\Repositories;

use App\Models\PaymentMethodes;
use App\Repositories\PaymentMethodesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PaymentMethodesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PaymentMethodesRepository
     */
    protected $paymentMethodesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->paymentMethodesRepo = \App::make(PaymentMethodesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_payment_methodes()
    {
        $paymentMethodes = factory(PaymentMethodes::class)->make()->toArray();

        $createdPaymentMethodes = $this->paymentMethodesRepo->create($paymentMethodes);

        $createdPaymentMethodes = $createdPaymentMethodes->toArray();
        $this->assertArrayHasKey('id', $createdPaymentMethodes);
        $this->assertNotNull($createdPaymentMethodes['id'], 'Created PaymentMethodes must have id specified');
        $this->assertNotNull(PaymentMethodes::find($createdPaymentMethodes['id']), 'PaymentMethodes with given id must be in DB');
        $this->assertModelData($paymentMethodes, $createdPaymentMethodes);
    }

    /**
     * @test read
     */
    public function test_read_payment_methodes()
    {
        $paymentMethodes = factory(PaymentMethodes::class)->create();

        $dbPaymentMethodes = $this->paymentMethodesRepo->find($paymentMethodes->id);

        $dbPaymentMethodes = $dbPaymentMethodes->toArray();
        $this->assertModelData($paymentMethodes->toArray(), $dbPaymentMethodes);
    }

    /**
     * @test update
     */
    public function test_update_payment_methodes()
    {
        $paymentMethodes = factory(PaymentMethodes::class)->create();
        $fakePaymentMethodes = factory(PaymentMethodes::class)->make()->toArray();

        $updatedPaymentMethodes = $this->paymentMethodesRepo->update($fakePaymentMethodes, $paymentMethodes->id);

        $this->assertModelData($fakePaymentMethodes, $updatedPaymentMethodes->toArray());
        $dbPaymentMethodes = $this->paymentMethodesRepo->find($paymentMethodes->id);
        $this->assertModelData($fakePaymentMethodes, $dbPaymentMethodes->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_payment_methodes()
    {
        $paymentMethodes = factory(PaymentMethodes::class)->create();

        $resp = $this->paymentMethodesRepo->delete($paymentMethodes->id);

        $this->assertTrue($resp);
        $this->assertNull(PaymentMethodes::find($paymentMethodes->id), 'PaymentMethodes should not exist in DB');
    }
}
