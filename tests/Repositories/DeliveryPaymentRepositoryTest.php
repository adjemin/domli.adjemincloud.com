<?php namespace Tests\Repositories;

use App\Models\DeliveryPayment;
use App\Repositories\DeliveryPaymentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DeliveryPaymentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DeliveryPaymentRepository
     */
    protected $deliveryPaymentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->deliveryPaymentRepo = \App::make(DeliveryPaymentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_delivery_payment()
    {
        $deliveryPayment = factory(DeliveryPayment::class)->make()->toArray();

        $createdDeliveryPayment = $this->deliveryPaymentRepo->create($deliveryPayment);

        $createdDeliveryPayment = $createdDeliveryPayment->toArray();
        $this->assertArrayHasKey('id', $createdDeliveryPayment);
        $this->assertNotNull($createdDeliveryPayment['id'], 'Created DeliveryPayment must have id specified');
        $this->assertNotNull(DeliveryPayment::find($createdDeliveryPayment['id']), 'DeliveryPayment with given id must be in DB');
        $this->assertModelData($deliveryPayment, $createdDeliveryPayment);
    }

    /**
     * @test read
     */
    public function test_read_delivery_payment()
    {
        $deliveryPayment = factory(DeliveryPayment::class)->create();

        $dbDeliveryPayment = $this->deliveryPaymentRepo->find($deliveryPayment->id);

        $dbDeliveryPayment = $dbDeliveryPayment->toArray();
        $this->assertModelData($deliveryPayment->toArray(), $dbDeliveryPayment);
    }

    /**
     * @test update
     */
    public function test_update_delivery_payment()
    {
        $deliveryPayment = factory(DeliveryPayment::class)->create();
        $fakeDeliveryPayment = factory(DeliveryPayment::class)->make()->toArray();

        $updatedDeliveryPayment = $this->deliveryPaymentRepo->update($fakeDeliveryPayment, $deliveryPayment->id);

        $this->assertModelData($fakeDeliveryPayment, $updatedDeliveryPayment->toArray());
        $dbDeliveryPayment = $this->deliveryPaymentRepo->find($deliveryPayment->id);
        $this->assertModelData($fakeDeliveryPayment, $dbDeliveryPayment->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_delivery_payment()
    {
        $deliveryPayment = factory(DeliveryPayment::class)->create();

        $resp = $this->deliveryPaymentRepo->delete($deliveryPayment->id);

        $this->assertTrue($resp);
        $this->assertNull(DeliveryPayment::find($deliveryPayment->id), 'DeliveryPayment should not exist in DB');
    }
}
