<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('customers', 'CustomersAPIController');

Route::resource('food_categories', 'FoodCategoriesAPIController');

Route::resource('otps', 'OtpsAPIController');

Route::resource('restaurants', 'RestaurantsAPIController');

Route::resource('delivery_types', 'DeliveryTypesAPIController');

Route::resource('vehicle_types', 'VehicleTypesAPIController');

Route::resource('delivery_adresses', 'DeliveryAdressesAPIController');

Route::resource('order_histories', 'OrderHistoriesAPIController');

Route::resource('order_items', 'OrderItemsAPIController');

Route::resource('restaurant_phones', 'RestaurantPhonesAPIController');

Route::resource('restaurant_menuses', 'RestaurantMenusAPIController');

Route::resource('restaurant_categories', 'RestaurantCategoriesAPIController');

Route::resource('restaurant_users', 'RestaurantUsersAPIController');

Route::resource('menuses', 'MenusAPIController');

Route::resource('food', FoodAPIController::class);

Route::resource('food_parts', 'FoodPartsAPIController');


Route::resource('restaurant_availabilities', 'RestaurantAvailabilityAPIController');
/**
 * Register and login route for delivery
 * not protected with jwt
 */
Route::post('/delivery/register', [\App\Http\Controllers\API\DeliveryAPIController::class, 'store']);
Route::post('/delivery/login', [\App\Http\Controllers\API\DeliveryAPIController::class, 'login']);
Route::post('/delivery/checkUser', [\App\Http\Controllers\API\DeliveryAPIController::class, 'checkUser']);
Route::post('/delivery/checkToken', [\App\Http\Controllers\API\DeliveryAPIController::class, 'checkToken']);
Route::get('/delivery/order/{order_id}/detail', [\App\Http\Controllers\API\DeliveryAPIController::class, 'getOrderDetails']);
Route::get('/delivery/{userId}/notifications', [\App\Http\Controllers\API\DeliveryAPIController::class, 'getNotifications']);
Route::put('/delivery/notifications/{userId}', [\App\Http\Controllers\API\DeliveryAPIController::class, 'putNotifications']);

Route::post('delivery_password_forgotten', [\App\Http\Controllers\API\DeliveryAPIController::class, 'deliveryPasswordForgotten']);
Route::post('delivery_verify_code_password_forgotten', [\App\Http\Controllers\API\DeliveryAPIController::class, 'checkCodeForPasswordReset']);
Route::post('delivery_rest_password_forgotten', [\App\Http\Controllers\API\DeliveryAPIController::class, 'restPassword']);
Route::post('delivery_edit_password', [\App\Http\Controllers\API\DeliveryAPIController::class, 'editPassword']);
    
/**
 * protected route with jwt
 * for more detail https://jwt-auth.readthedocs.io/
 * I use custom guard "deliverer_api" in config.auth
 */
Route::group([
    'middleware' => 'auth.jwt',
    'guard' =>  'deliverer_api'
], function ($router) {
    Route::post('/delivery/profile/update', [\App\Http\Controllers\API\DeliveryAPIController::class, 'update']);
    Route::post('/delivery/profile/logout', [\App\Http\Controllers\API\DeliveryAPIController::class, 'logout']);
    Route::post('/delivery/changeOnlineStatus', [\App\Http\Controllers\API\DeliveryAPIController::class, 'changeOnlineStatus']);
    Route::get('/delivery/deliveryAssignement', [\App\Http\Controllers\API\DeliveryAPIController::class, 'deliveryAssignement']);
    Route::get('/delivery/deliveryPayment', [\App\Http\Controllers\API\DeliveryAPIController::class, 'deliveryPayment']);
    Route::get('/delivery/deliveryPaymentPaid', [\App\Http\Controllers\API\DeliveryAPIController::class, 'deliveryPaymentPaid']);
    Route::get('/delivery/deliveryAssignementAccepted', [\App\Http\Controllers\API\DeliveryAPIController::class, 'deliveryAssignementAccepted']);
    Route::post('/delivery/acceptOrRejectAssignement/{id}', [\App\Http\Controllers\API\DeliveryAPIController::class, 'acceptOrRejectAssignement']);
    Route::post('/delivery/changeJobStatus/{id}', [\App\Http\Controllers\API\DeliveryAPIController::class, 'changeJobStatus']);
    Route::post('/delivery/sendRequest', [\App\Http\Controllers\API\DeliveryAPIController::class, 'sendRequest']);
    Route::post('/delivery_edit_picture', [\App\Http\Controllers\API\DeliveryAPIController::class, 'delivery_edit_picture']);
    Route::get('delivery/request/{id}/historique',[\App\Http\Controllers\API\DeliveryAPIController::class, 'getHistorique']);


    // Route::post('delivery_password_forgotten', [\App\Http\Controllers\API\DeliveryAPIController::class, '']);

    Route::resource('jobs', 'JobAPIController');
    
    Route::resource('job_historiques', 'JobHistoriqueAPIController');
    
    Route::resource('agent_connexions', 'AgentConnexionAPIController');
    
    Route::resource('order_assignements', 'OrderAssignementAPIController');
    
    Route::resource('restaurant_payments', 'RestaurantPaymentAPIController');
    
    Route::resource('delivery_payments', 'DeliveryPaymentAPIController');
    
    Route::resource('vehicles', 'VehicleAPIController');

    Route::resource('delivery_availaibities', 'DeliveryAvailaibityAPIController');

    Route::resource('customer_devices', 'CustomerDevicesAPIController');

    Route::resource('payment_methodes', 'PaymentMethodesAPIController');

    Route::resource('orders', 'OrdersAPIController');

    Route::resource('order_deliveries', 'OrderDeliveriesAPIController');

    Route::resource('deliveries', 'DeliveryAPIController');
    
    Route::resource('order_histories', 'OrderHistoriesAPIController');

    Route::resource('order_items', 'OrderItemsAPIController');

    Route::resource('restaurant_comptes', 'RestaurantCompteAPIController');
    
    Route::resource('delivery_comptes', 'DeliveryCompteAPIController');
    
    Route::resource('delivery_payment_historiques', 'DeliveryPaymentHistoriqueAPIController');
    
    Route::resource('restaurant_payment_historiques', 'RestaurantPaymentHistoriqueAPIController');
    
    Route::resource('request_restaurant_payments', 'RequestRestaurantPaymentAPIController');
    
    Route::resource('request_delivery_payments', 'RequestDeliveryPaymentAPIController');
    
    Route::resource('contacts', 'ContactAPIController');
});




Route::resource('password_deliveries', 'PasswordDeliveryAPIController');