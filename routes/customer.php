<?php

use App\Http\Controllers\CustomerFrontController;
// use App\Models\balance;
use App\Models\ClapayNumber;
use App\Models\Customer;
use App\Models\PhoneNumberAccount;
use App\Models\SyntaxPayment;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

// Route::get('/home', function () {
//     $users[] = Auth::user();
//     $users[] = Auth::guard()->user();
//     $users[] = Auth::guard('customer')->user();

//     // $customer = Auth::guard('customer')->user();


// })->name('home');

// Route::get('/signup', function () {
//     return view('customer.signUp');
// })->name('signup');

// Route::get('/signinCustomer', function () {
//     return view('customer.login');
// })->name('signin');

// Route::get('/profile', [CustomerFrontOfficeController::class, 'showProfile'])->name('profilePage');

// Route::get('/profile', [CustomerFrontOfficeController::class, 'showProfile'])->name('profilePage');

// Route::get('/historique', [CustomerFrontOfficeController::class, 'customerRequestIndex'])->name('historiquePage');

// Route::get('/historique/detail/{id}', [CustomerFrontOfficeController::class, 'customerRequestShow'])->name('historiqueDetailPage');

// Route::get('customer_request', [CustomerFrontOfficeController::class, 'showMakeRequestForm'])->name('makeRequestForm');
// Route::post('customer_request/store', [CustomerFrontOfficeController::class, 'makeRequestStore'])->name('makeRequestStore');
// Route::post('find_jobber', [CustomerFrontOfficeController::class, 'findJobber'])->name('findJobber');

// Route::get('customer_rating/create/{id}', [CustomerFrontOfficeController::class, 'showCustomerRatingForm'])->name('showCustomerRatingForm');
// Route::post('customer_rating/store', [CustomerFrontOfficeController::class, 'storeCustomerRatingForm'])->name('storeCustomerRatingForm');

// Route::get('customer_chat', [CustomerFrontOfficeController::class, 'showChat']);
// Route::post('customer_send_message', [CustomerFrontOfficeController::class, 'sendMessage'])->name('sendMessage');
