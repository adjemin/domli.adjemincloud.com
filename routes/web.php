<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\RestaurantFrontController;
use App\Http\Controllers\CustomerFrontOfficeController;
use App\Http\Controllers\CustomerAuth\LoginController;



// Home page
Route::get('/test', function () {
    return view('customer.homePage.test');
});
Route::get('/domlii', [\App\Http\Controllers\DomliiController::class, 'index'])->name('home.index');
Route::get('/connexion', [\App\Http\Controllers\ConnexionController::class, 'index'])->name('connexion.index');
Route::get('/food-espace', [\App\Http\Controllers\FoodEspaceController::class, 'index'])->name('food_espace.index');
Route::get('/variete', [\App\Http\Controllers\VarieteController::class, 'index'])->name('variete.index');
Route::get('/', [App\Http\Controllers\HomePageController::class, 'index'])->name('homepage');
Route::get('/welcome', function () {
    return view('welcome');
});
Route::get('/accueil', [App\Http\Controllers\HomePageController::class, 'index'])->name('homepage');

Route::post('order-change_state/{order_number}', [App\Http\Controllers\OrdersController::class, 'change_status']);
/*
============================================================================================
Customer interface routes
============================================================================================
*/

Route::group(['prefix' => 'customer'], function () {
    Route::get('/login', [App\Http\Controllers\CustomerAuth\LoginController::class, 'showLoginForm'])->name('customer.login');
    Route::post('/login', [App\Http\Controllers\CustomerAuth\LoginController::class, 'login'])->name('customerLogin');

    Route::post('/logout', [App\Http\Controllers\CustomerAuth\LoginController::class, 'logout'])->name('logout');

    // Route::get('create_account/customer', [App\Http\Controllers\CustomerFrontOfficeController::class, 'showCustomerRegisterForm'])->name('customerRegisterForm');

    Route::get('/register', [App\Http\Controllers\CustomerAuth\RegisterController::class, 'showRegistrationForm'])->name('register');

    Route::post('/register', [App\Http\Controllers\CustomerAuth\RegisterController::class, 'storeCustomerAccount']);

    //->middleware('verified');

    Route::get('/home', [App\Http\Controllers\CustomerAuth\RegisterController::class, 'showHomePage'])->name('customerHome')->middleware('customer');

    Route::post('/password/email', 'CustomerAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');

    // Route::post('/password/reset', 'CustomerAuth\ResetPasswordController@reset')->name('password.email');

    // Route::get('/password/reset', 'CustomerAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');

    // Route::get('/password/reset/{token}', 'CustomerAuth\ResetPasswordController@showResetForm');

    Route::get('/orders/{order_number}', [App\Http\Controllers\CustomerFrontOfficeController::class, 'showOrderFromOrderNumber']);
    Route::get('/profile', [App\Http\Controllers\CustomerFrontOfficeController::class, 'showProfile'])->name('profilePage');
    Route::post('/profile/update', [App\Http\Controllers\CustomerFrontOfficeController::class, 'update'])->name('profileUpdate');
    Route::get('/orders', [App\Http\Controllers\CustomerFrontOfficeController::class, 'showProfile'])->name('profilePage');

    // Route::get('/order/{id}', [App\Http\Controllers\CustomerFrontOfficeController::class, 'showProfile'])->name('showOrder');

    Route::get('/cart', [CartController::class, 'index'])->name('customerCart');

    Route::delete('/cart/distroy/{rowId}', [CartController::class, 'distroyCart'])->name('distroyCart');
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', function () {
        return redirect('/admin/login');
    });

    Route::get('/login', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm']);
    // Route::get('/register', [App\Http\Controllers\Auth\RegisterController::class, 'showRegistrationForm']);
    Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'login']);
    Route::post('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout']);
    // Route::post('/register', [App\Http\Controllers\Auth\RegisterController::class, 'register']);
    Route::get('/password/reset', [App\Http\Controllers\Auth\ResetPasswordController::class, 'showResetForm']);
    Route::post('/password/reset', [App\Http\Controllers\Auth\ResetPasswordController::class, 'reset']);
    Route::post('/password/reset/{token}', [App\Http\Controllers\Auth\ResetPasswordController::class, 'showResetForm']);
    Route::get('/password/email', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'showLinkRequestForm']);
    Route::post('/password/email', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'sendResetLinkEmail']);

    Route::group(['middleware' => 'auth'], function () {
        Route::post('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout']);
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('admin-home-index');
        Route::get('/foodCategories', [App\Http\Controllers\FoodCategoriesController::class, 'index'])->name('admin-foodCategories-index');
        Route::post('/foodCategories', [App\Http\Controllers\FoodCategoriesController::class, 'store'])->name('admin-foodCategories-store');
        Route::get('/foodCategories/create', [App\Http\Controllers\FoodCategoriesController::class, 'create'])->name('admin-foodCategories-create');
        Route::get('/foodCategories/{id}', [App\Http\Controllers\FoodCategoriesController::class, 'show'])->name('admin-foodCategories-show');
        Route::get('/foodCategories/{id}/edit', [App\Http\Controllers\FoodCategoriesController::class, 'edit'])->name('admin-foodCategories-edit');
        Route::put('/foodCategories/{id}', [App\Http\Controllers\FoodCategoriesController::class, 'update'])->name('admin-foodCategories-update');
        Route::delete('/foodCategories/{id}', [App\Http\Controllers\FoodCategoriesController::class, 'destroy'])->name('admin-foodCategories-delete');
        Route::get('/orders', [App\Http\Controllers\OrdersController::class, 'index'])->name('admin-orders-index');
        Route::get('/food', [App\Http\Controllers\FoodController::class, 'index'])->name('admin-food-index');
        Route::get('/foodParts', [App\Http\Controllers\FoodPartsController::class, 'index'])->name('admin-foodParts-index');
        Route::get('/customers', [App\Http\Controllers\CustomersController::class, 'index'])->name('admin-customers-index');
        Route::get('/customers/{id}', [App\Http\Controllers\CustomersController::class, 'show'])->name('admin-customers-show');
        Route::post('/customers/desable-or-enable/{id}', [App\Http\Controllers\CustomersController::class, 'change_state'])->name('admin-customers-change_state');
        Route::post('/restaurants/desable-or-enable/{id}', [App\Http\Controllers\RestaurantsController::class, 'change_state'])->name('admin-restaurants-change_state');
        Route::post('/delivery/desable-or-enable/{id}', [App\Http\Controllers\DeliveryController::class, 'change_state'])->name('admin-delivery-change_state');
        Route::post('/vehicle/desable-or-enable/{id}', [App\Http\Controllers\VehicleController::class, 'change_state'])->name('admin-vehicle-change_state');
        Route::get('/delivery/{delivery_id}/orders', [App\Http\Controllers\OrdersController::class, 'delivery'])->name('backoffice-delivery-order-index');
        Route::get('/delivery/{delivery_id}/history', [App\Http\Controllers\DeliveryPaymentHistoriqueController::class, 'delivery'])->name('backoffice-delivery-payment-history-index');
        Route::get('/delivery/{delivery_id}/request', [App\Http\Controllers\RequestDeliveryPaymentController::class, 'delivery'])->name('backoffice-delivery-payment-request-index');
        Route::get('/delivery/{delivery_id}/order_assignements', [App\Http\Controllers\OrderAssignementController::class, 'delivery'])->name('backoffice-delivery-orderassignement-index');
        Route::delete('/customers/{id}', [App\Http\Controllers\CustomersController::class, 'destroy'])->name('admin-customers-delete');
        Route::get('/customers', [App\Http\Controllers\CustomersController::class, 'index'])->name('admin-customers-index');
        Route::get('/restaurants', [App\Http\Controllers\RestaurantsController::class, 'index'])->name('admin-restaurants-index');
        Route::get('/restaurants/{id}', [App\Http\Controllers\RestaurantsController::class, 'show'])->name('admin-restaurants-show');
        Route::get('/deliverers', [App\Http\Controllers\DeliveryController::class, 'index'])->name('admin-deliverers-index');
        Route::get('/orders', [App\Http\Controllers\OrdersController::class, 'index'])->name('admin-orders-index');
        Route::get('/orders/{id}', [App\Http\Controllers\OrdersController::class, 'show'])->name('admin-orders-show');
        Route::delete('/orders/{id}', [App\Http\Controllers\OrdersController::class, 'show'])->name('admin-orders-delete');

        Route::get('/restaurant/{restaurant_id}/foods', [App\Http\Controllers\FoodController::class, 'foodsByRestaurant'])->name('admin-restaurant-food-index');
        Route::get('/restaurant/{restaurant_id}/foods/create', [App\Http\Controllers\FoodController::class, 'create'])->name('admin-restaurant-food-create');
        Route::post('/restaurant/{restaurant_id}/foods/create', [App\Http\Controllers\FoodController::class, 'store'])->name('admin-restaurant-food-store');

        Route::get('/restaurant/{restaurant_id}/category', [App\Http\Controllers\RestaurantCategoryController::class, 'categorie'])->name('admin-restaurant-categorie-index');
        Route::get('restaurant/{restaurant_id}/category/create', [App\Http\Controllers\RestaurantCategoryController::class, 'create'])->name('admin-restaurant-categorie-create');


        Route::get('/restaurant/{restaurant_id}/menu', [App\Http\Controllers\RestaurantMenusController::class, 'menusByRestaurant'])->name('admin-restaurant-menu-index');
        Route::get('/restaurant/{restaurant_id}/menu/create', [App\Http\Controllers\RestaurantMenusController::class, 'create'])->name('admin-restaurant-menu-create');
        Route::post('/restaurant/{restaurant_id}/menu/create', [App\Http\Controllers\RestaurantMenusController::class, 'store'])->name('admin-restaurant-menu-store');
        Route::put('/restaurant/{restaurant_id}/menu/update', [App\Http\Controllers\RestaurantMenusController::class, 'update'])->name('admin-restaurant-menu-update');


        Route::get('/restaurant/{restaurant_id}/{food_id}/food_part/', [App\Http\Controllers\FoodPartsController::class, 'food'])->name('admin-restaurant-foodPart-index');
        Route::get('/restaurant/{restaurant_id}/{food_id}/food_part/create', [App\Http\Controllers\FoodPartsController::class, 'create'])->name('admin-restaurant-foodPart-create');
        Route::post('{food_id}/food_part/create', [App\Http\Controllers\FoodPartsController::class, 'store'])->name('admin-restaurant-foodPart-store');

        Route::get('/restaurant/{restaurant_id}/{food_part_id}/food_part_item', [App\Http\Controllers\FoodPartItemController::class, 'food_part'])->name('admin-restaurant-foodPartItem-store');
        Route::get('/restaurant/{restaurant_id}/{food_part_id}/food_part_item/create', [App\Http\Controllers\FoodPartItemController::class, 'create'])->name('admin-restaurant-foodPartItem-store');
        Route::post('/restaurant/{restaurant_id}/{food_part_id}/food_part_item/create', [App\Http\Controllers\FoodPartItemController::class, 'store'])->name('admin-restaurant-foodPartItem-store');
    });
});

/*
=============================================================================================================
Restaurants  route
=============================================================================================================
*/
Route::get('/backOffice/restaurants/{id}', [App\Http\Controllers\RestaurantsController::class, 'show'])->middleware('restaurant');
Route::post('/food/update/stock/{food_id}', [App\Http\Controllers\FoodController::class, 'stock']);

Route::post('/restaurant/logout', [App\Http\Controllers\RestaurantAuth\LoginController::class, 'logout']);
Route::group(['middleware' => 'restaurant'], function () {
    Route::get('/restaurant/{restaurant_id}/orders', [App\Http\Controllers\OrdersController::class, 'restaurant'])->name('backoffice-restaurant-order-index');
    Route::get('/restaurants/{restaurant_id}/edit', [App\Http\Controllers\RestaurantsController::class, 'edit'])->name('backoffice-restaurants-edit');

    Route::get('/restaurant/{restaurant_id}/foods', [App\Http\Controllers\FoodController::class, 'foodsByRestaurant'])->name('backoffice-restaurant-food-index');
    Route::get('/restaurant/{restaurant_id}/foods/create', [App\Http\Controllers\FoodController::class, 'create'])->name('backoffice-restaurant-food-create');
    Route::post('/restaurant/{restaurant_id}/foods/create', [App\Http\Controllers\FoodController::class, 'store'])->name('backoffice-restaurant-food-store');

    Route::get('/restaurant/{restaurant_id}/category', [App\Http\Controllers\RestaurantCategoryController::class, 'categorie'])->name('backoffice-restaurant-categorie-index');
    Route::get('restaurant/{restaurant_id}/category/create', [App\Http\Controllers\RestaurantCategoryController::class, 'create'])->name('backoffice-restaurant-categorie-create');


    Route::get('/restaurant/{restaurant_id}/menu', [App\Http\Controllers\RestaurantMenusController::class, 'menusByRestaurant'])->name('backoffice-restaurant-menu-index');
    Route::get('/restaurant/{restaurant_id}/menu/create', [App\Http\Controllers\RestaurantMenusController::class, 'create'])->name('backoffice-restaurant-menu-create');
    Route::post('/restaurant/{restaurant_id}/menu/create', [App\Http\Controllers\RestaurantMenusController::class, 'store'])->name('backoffice-restaurant-menu-store');


    Route::get('/restaurant/{restaurant_id}/{food_id}/food_part/', [App\Http\Controllers\FoodPartsController::class, 'food'])->name('backoffice-restaurant-foodPart-index');
    Route::get('/restaurant/{restaurant_id}/{food_id}/food_part/create', [App\Http\Controllers\FoodPartsController::class, 'create'])->name('backoffice-restaurant-foodPart-create');
    Route::get('/restaurant/{restaurant_id}/{food_id}/food_part/show', [App\Http\Controllers\FoodPartsController::class, 'show'])->name('backoffice-restaurant-foodPart-show');
    Route::get('/restaurant/{restaurant_id}/{food_id}/food_part/edit', [App\Http\Controllers\FoodPartsController::class, 'edit'])->name('backoffice-restaurant-foodPart-edit');
    Route::delete('/restaurant/{restaurant_id}/{food_id}/food_part/delete', [App\Http\Controllers\FoodPartsController::class, 'destroy'])->name('backoffice-restaurant-foodPart-delete');
    Route::post('{food_id}/food_part/create', [App\Http\Controllers\FoodPartsController::class, 'store'])->name('backoffice-restaurant-foodPart-store');

    Route::get('/restaurant/{restaurant_id}/{food_part_id}/food_part_item', [App\Http\Controllers\FoodPartItemController::class, 'food_part'])->name('backoffice-restaurant-foodPartItem-store');
    Route::get('/restaurant/{restaurant_id}/{food_part_id}/food_part_item/create', [App\Http\Controllers\FoodPartItemController::class, 'create'])->name('backoffice-restaurant-foodPartItem-store');
    Route::post('/restaurant/{restaurant_id}/{food_part_id}/food_part_item/create', [App\Http\Controllers\FoodPartItemController::class, 'store'])->name('backoffice-restaurant-foodPartItem-store');
    Route::get('/restaurant/{restaurant_id}/{id}/orders', [App\Http\Controllers\OrdersController::class, 'showRestaurant'])->name('backoffice-orders-show');

    // Route::get('/restaurant/{restaurant_id}/menu', [App\Http\Controllers\RestaurantMenusController::class, 'menusByRestaurant']);
    // Route::get('_restaurants_create/{restaurant_id}', [App\Http\Controllers\RestaurantMenusController::class, 'create']);
    // Route::post('menu_restaurants_create/{restaurant_id}', [App\Http\Controllers\RestaurantMenusController::class, 'store']);
});

Route::post('/register/restaurant', [\App\Http\Controllers\RestaurantAuth\RegisterController::class, 'storeRestaurantAccount'])->name('restaurant_register');

Route::post('/register/login', [App\Http\Controllers\RestaurantAuth\LoginController::class, 'login'])->name('restaurant_login');


Route::get('/restaurants', [\App\Http\Controllers\RestaurantFrontController::class, 'index'])->name('restaurants');

// Route::get('/restaurants/categorie/{slug}', [App\Http\Controllers\RestaurantFrontController::class, 'showCategorie'])->name('restaurantsCategorie');

Route::get('/restaurants/categorie', [App\Http\Controllers\RestaurantFrontController::class, 'showCategorie'])->name('searchRestaurantsCategorie');

Route::get('page/restaurant/{uuid}', [RestaurantFrontController::class, 'show'])->name('restaurant');

Route::get('page/restaurants/categories', [RestaurantFrontController::class, 'showCategorieRestaurant'])->name('CategorieRestaurants');

// Route::get('page/restaurant/{id}', App\Http\Livewire\RestaurantFront::class)->name('restaurant');

Route::get('restaurant/password/reset', [App\Http\Controllers\RestaurantAuth\ResetPasswordController::class, 'showResetForm']);
Route::post('restaurant/password/reset', [App\Http\Controllers\RestaurantAuth\ResetPasswordController::class, 'reset']);
Route::post('restaurant/password/reset/{token}', [App\Http\Controllers\RestaurantAuth\ResetPasswordController::class, 'showResetForm']);
Route::get('restaurant/password/email', [App\Http\Controllers\RestaurantAuth\ForgotPasswordController::class, 'showLinkRequestForm']);
Route::post('restaurant/password/email', [App\Http\Controllers\RestaurantAuth\ForgotPasswordController::class, 'sendResetLinkEmail']);

Route::get('customer/password/reset', [App\Http\Controllers\CustomerAuth\ResetPasswordController::class, 'showResetForm']);
Route::post('customer/password/reset', [App\Http\Controllers\CustomerAuth\ResetPasswordController::class, 'reset']);
Route::post('customer/password/reset/{token}', [App\Http\Controllers\CustomerAuth\ResetPasswordController::class, 'showResetForm']);
Route::get('customer/password/email', [App\Http\Controllers\CustomerAuth\ForgotPasswordController::class, 'showLinkRequestForm']);
Route::post('customer/password/email', [App\Http\Controllers\CustomerAuth\ForgotPasswordController::class, 'sendResetLinkEmail']);

/*
=============================================================================================================
Back office restaurant route
=============================================================================================================
*/

Route::group(['prefix' => 'backOffice'], function () {
    Route::resource('customers', App\Http\Controllers\CustomersController::class);

    Route::resource('foodCategories', App\Http\Controllers\FoodCategoriesController::class);

    Route::resource('coupons', App\Http\Controllers\CouponController::class);

    Route::resource('otps', App\Http\Controllers\OtpsController::class);

    Route::resource('restaurants', App\Http\Controllers\RestaurantsController::class);

    Route::resource('deliveryTypes', App\Http\Controllers\DeliveryTypesController::class);

    Route::resource('vehicleTypes', App\Http\Controllers\VehicleTypesController::class);

    Route::resource('customerDevices', App\Http\Controllers\CustomerDevicesController::class);

    Route::resource('paymentMethodes', App\Http\Controllers\PaymentMethodesController::class);

    Route::resource('orders', App\Http\Controllers\OrdersController::class);

    //     Route::resource('orderDeliveries', App\Http\Controllers\OrderDeliveriesController::class);

    Route::resource('deliveries', App\Http\Controllers\DeliveryController::class);

    Route::resource('deliveryAdresses', App\Http\Controllers\DeliveryAdressesController::class);

    Route::resource('orderHistories', App\Http\Controllers\OrderHistoriesController::class);

    Route::resource('orderItems', App\Http\Controllers\OrderItemsController::class);

    Route::resource('restaurantPhones', App\Http\Controllers\RestaurantPhonesController::class);

    Route::resource('restaurantMenuses', App\Http\Controllers\RestaurantMenusController::class);

    Route::resource('foodCategories', App\Http\Controllers\FoodCategoriesController::class);

    Route::resource('restaurantCategories', App\Http\Controllers\RestaurantCategoriesController::class);

    Route::resource('restaurantUsers', App\Http\Controllers\RestaurantUsersController::class);

    Route::resource('menuses', App\Http\Controllers\MenusController::class);

    Route::resource('food', App\Http\Controllers\FoodController::class);

    Route::resource('foodParts', App\Http\Controllers\FoodPartsController::class);

    Route::resource('foodPartItems', App\Http\Controllers\FoodPartItemController::class);

    Route::resource('restaurantAvailabilities', App\Http\Controllers\RestaurantAvailabilityController::class);
});

Route::get('/login/restaurant', function () {
    return view('frontOffice.restaurant.homePage.index');
})->name('restaurantHome');

Route::get('/restaurant/congratulation', function () {
    return view('frontOffice.restaurant.pages.congratulate');
})->name('restaurantCongrate');

Route::get('/restaurant/verify/{token}', [App\Http\Controllers\RestaurantAuth\RegisterController::class, 'verifyRestaurant'])->name('verifyRestaurant');

Route::get('/restaurant/page/confirm', [App\Http\Controllers\RestaurantAuth\RegisterController::class, 'restaurantConfirmView'])->name('restaurantConfirmView');

Route::get('/restaurant/next/step/{uuid}', [App\Http\Controllers\RestaurantAuth\RegisterController::class, 'restaurantLastRegister'])->name('restaurantLastRegister');


Route::post('/restaurant/update', [App\Http\Controllers\RestaurantAuth\RegisterController::class, 'updateRestaurant'])->name('updateRestaurant');

Route::get('/restaurant/waitting', [App\Http\Controllers\RestaurantAuth\RegisterController::class, 'afterRegistrationView'])->name('afterRegistrationView');

/*
=============================================================================================================
Order and checkout route
=============================================================================================================
*/

// Order route
Route::post('/get/food/data', [App\Http\Controllers\FoodController::class, 'getFoodData'])->name('createOrder');

Route::get('/create/order', [App\Http\Controllers\OrderController::class, 'index'])->name('createOrder');

Route::post('/create/order/notAuth', [App\Http\Controllers\OrderController::class, 'notAuthIndex'])->name('createOrderNotAuth');

Route::post('/store/order', [App\Http\Controllers\OrderController::class, 'store'])->name('storeOrder');

// Payment form
Route::post('/checkout', [App\Http\Controllers\checkoutController::class, 'index'])->name('checkout');

Route::post('/changelocale', [\App\Http\Controllers\TranslationController::class, 'changeLocale'])->name('changelocale');


// Route::get('/restaurant', function () {
//     return view('frontOffice.pages.restaurantSingle');
// })->name('restaurant.show');

// Customer login page
// Route::get('/customer/login', [App\Http\Controllers\CustomerAuth\LoginController::class, 'showLoginForm'])->name('customer.login');

// Route::post('/customer/login', [App\Http\Controllers\CustomerAuth\LoginController::class, 'guard'])->name('customer.store');
// Route::get('/customer/login', function () {
//     return view('customer.Auth.login');
// })->name('login');


// Auth::routes(['verify' => true]);

Route::get('/domli-admin', [App\Http\Controllers\HomeController::class, 'index'])->middleware('verified');

Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->middleware('verified');

// Route::get('/home', 'HomeController@index')->middleware('verified');


/*
============================================================================================
Customer route
============================================================================================
*/
Route::get('/partenaire/restaurants', function () {
    return view('customer.pages.pagerestau');
});


Route::get('/otp', function () {
    return view('frontOffice.pages.otpPhonePage');
})->name('otp');

Route::get('/about', function () {
    return view('frontOffice.pages.about');
})->name('aboutUs');

Route::get('/page/contact', function () {
    return view('frontOffice.pages.contact');
})->name('contactUs');

// Route::get('/order', function () {
//     return view('frontOffice.pages.orderDetail');
// })->name('orderDetail');
// /*
// ============================================================================================
// delivery route
// ============================================================================================
// */
// //

Route::get('/delivery/home', function () {
    return view('customer.homePage.test');
});

Route::get('/delivery/chooseMoveMeans', function () {
    return view('deliverer.pages.chooseMoveMeans');
})->name('chooseMoveMeans');

Route::get('/delivery/lastRegisterStep', function () {
    return view('deliverer.pages.lastRegisterStep');
})->name('lastRegister');

Route::get('/deliverer/profileAuth', function () {
    return view('deliverer.pages.profileAuth');
    // return view('frontOffice.delivery.pages.profileAuth');
})->name('profileAuth');

Route::get('/delivery/offers', function () {
    return view('deliverer.pages.offerDetail');
})->name('offers');

Route::get('/delivery/otp', function () {
    return view('deliverer.pages.otpPhonePage');
})->name('deliveryOtpPage');

Route::get('/delivery/offer', function () {
    return view('deliverer.pages.offerDetail');
})->name('deliveryOffer');


Route::get('/customer/verify/{token}', [App\Http\Controllers\CustomerAuth\RegisterController::class, 'verifyCustomer'])->name('verifyCusomer');
Route::get('/delivery/verify/{token}', [App\Http\Controllers\DelivererAuth\RegisterController::class, 'verifyDeliverer'])->name('verifyDeliverer');

Route::get('/domli/congratulate', function () {
    return view('customer.pages.congratulate');
})->name('congratulate');


Route::get('/domli/congratulate/delivery', function () {
    return view('deliverer.pages.congratulate');
})->name('congratulateDeliverer');

// Email note verified page
// Route::get('/customer/waitpage', function () {
//     return view('customer.pages.waitpage');
// });


// Cart route

route::get('/food/detail/{uuid}', [App\Http\Controllers\CartController::class, 'foodOrderDetail'])->name('foodOrderDetail');

route::post('/add/food/', [App\Http\Controllers\CartController::class, 'store'])->name('cartStore');
route::post('/add/discount/', [App\Http\Controllers\CartController::class, 'addDiscount'])->name('addDiscount');

route::get('/cart/destroy', [App\Http\Controllers\CartController::class, 'destroy'])->name('emptyCart');

Route::group(['prefix' => 'deliverer'], function () {

    Route::get('/login', [App\Http\Controllers\DelivererAuth\LoginController::class, 'index'])->name('delivererLogin');

    Route::post('/login', [App\Http\Controllers\DelivererAuth\LoginController::class, 'login'])->name('delivery_login_post');

    Route::post('/logout', [App\Http\Controllers\DelivererAuth\LoginController::class, 'logout'])->name('delivery_logout');

    Route::get('/register', [App\Http\Controllers\DelivererAuth\RegisterController::class, 'showRegistrationForm'])->name('delivery_register_get');

    Route::post(
        '/register',
        [App\Http\Controllers\DelivererAuth\RegisterController::class, 'register']
    )->name('delivery_register_post');

    Route::post('/password/email', [App\Http\Controllers\DelivererAuth\ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.request');

    Route::post('/password/reset', [App\Http\Controllers\DelivererAuth\ResetPasswordController::class, 'reset'])->name('password.email');

    Route::get('/password/reset', [App\Http\Controllers\DelivererAuth\ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.reset');

    Route::get('/password/reset/{token}', [App\Http\Controllers\DelivererAuth\ResetPasswordController::class, 'showResetForm']);
});


// OAuth Routes
Route::get('/auth/{provider}', [App\Http\Controllers\CustomerAuth\LoginController::class, 'redirectToProvider']);

Route::get('/auth/{provider}/callback', [App\Http\Controllers\CustomerAuth\LoginController::class, 'handleProviderCallback']);

Route::post('/search/restaurants', [App\Http\Controllers\RestaurantFrontController::class, 'searchRestaurant']);
Route::post('/search/foods', [App\Http\Controllers\RestaurantFrontController::class, 'searchFood']);

Route::get('policy/politic', function () {
    return view('customer.home');
});

// Route::get('/backOffice/restaurants', [App\Http\Controllers\RestaurantsController::class, 'index'])->middleware('auth');


// Route::get('/waitpage', function () {
//     return view('customer.pages.waitpage');
// })->name('wait');


Route::resource('vehicles', \App\Http\Controllers\VehicleController::class);

Route::resource('deliveryAvailaibities', \App\Http\Controllers\DeliveryAvailaibityController::class);

Route::resource('jobs', \App\Http\Controllers\JobController::class);

Route::resource('jobHistoriques', \App\Http\Controllers\JobHistoriqueController::class);

Route::resource('agentConnexions', \App\Http\Controllers\AgentConnexionController::class);

Route::resource('orderAssignements', \App\Http\Controllers\OrderAssignementController::class);

Route::resource('restaurantPayments', \App\Http\Controllers\RestaurantPaymentController::class);

Route::resource('deliveryPayments', \App\Http\Controllers\DeliveryPaymentController::class);

Route::resource('restaurantComptes', \App\Http\Controllers\RestaurantCompteController::class);

Route::resource('deliveryComptes', \App\Http\Controllers\DeliveryCompteController::class);

Route::resource('deliveryPaymentHistoriques', \App\Http\Controllers\DeliveryPaymentHistoriqueController::class);

Route::resource('restaurantPaymentHistoriques', \App\Http\Controllers\RestaurantPaymentHistoriqueController::class);
Route::get('/restaurant/{restaurant_id}/restaurantPaymentHistoriques', [\App\Http\Controllers\RestaurantPaymentHistoriqueController::class, 'restaurant'])->name('restaurantPaymentHistoriquesAdmin');

Route::resource('requestRestaurantPayments', \App\Http\Controllers\RequestRestaurantPaymentController::class);
Route::get('/restaurant/{restaurant_id}/requestRestaurantPayments', [\App\Http\Controllers\RequestRestaurantPaymentController::class, 'restaurant'])->name('requestRestaurantPaymentsAdmin');

Route::resource('requestDeliveryPayments', \App\Http\Controllers\RequestDeliveryPaymentController::class);

Route::resource('contacts', \App\Http\Controllers\ContactController::class);

Route::resource('passwordDeliveries', 'PasswordDeliveryController');
